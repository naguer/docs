# LPI DevOps Tools Engineer Certification



## Machine Deployment

### Vagrant

Vagrant is a tool for building and managing vm environments. Use provisioning tools as shell scripts, Chef, or Ansible. Out of the box providers: VirtualBox, Hyper-V, Docker

* Three ways that Vagrant will try and detect a provider:

  * Execute vagrant up with the --provider flag.
  * Use the VAGRANT_DEFAULT_PROVIDER environmental variable in your Vagrantfile. Example: ENV['VAGRANT_DEFAULT_PROVIDER'] = ''
  * Vagrant will go through all of the config.vm.provider calls in the Vagrantfile and try each in order.

* Other commands:

  * vagrant provision: runs any configure provisioners
  * vagrant reload: runs a halt followed by and up
  * vagrant status: state of the machines

* Multiple Machines in Vagrant with different providers:

  ```
  Vagrant.configure("2") do |config|
  	config.vm.define "web" do |web|
  		web.vm.box = "ubuntu/trusty64"
  		web.vm.provision "shell" do |shell|
  			shell.line = "apt update -y"
          end
      end
      config.vm.define "db" do |db|
  		web.vm.box = "ubuntu/trusty64"
  		web.vm.provision "puppet" do |puppet|
  			puppet.manifiest_path = "puppet/manifiest"
  			...continue...
          end
      end
  ```

* Example VagrantFile with docker image my_app that is pushed to Docker Hub your_company/my_app. You need to create a docker file to create a local environment for my_app

  ```
  Vagrant.configure("2") do |config| 
  	config.vm.provision "docker" do |d| 
		d.build_image "/your_company/my_app" 
  	end 
  end
  ```
  
  

### Vagrant Boxes

* Vagrant Boxes are packages for Vagrant environments, if you create a Vagrant box for Virtualbox you need to install VirtualBox Guest Additions, Docker provider does not require a Vagrant Box
* Vagrant Box Commands:
  * vagrant box add "ADDRESS"
  * vagrant box list: List all boxes installed
  * vagrant box outdated: Show box outdated
  * vagrant box update: Update box outdated
  * vagrant box prune: remove old versions
  * vagrant box remove
  * vagrant box repackage "NAME" "PROVIDER" "VERSION": This command repackages the given box and puts it in the current directory so you can redistribute it. In others words this create a new Box file from a Box imported into Vagrant.
  * Configure vagrant to automatically check box updates:
    `config vm.box_check_update = false`
* package.box files: 
  * metadata.json / info.json: name of the box, description, the available versions, providers (info about the box)
  *  others: box.ovf, Vagrantfile, box-disk001.vmdk



### Packer and Cloud Init

- Creating identical machine images for multiple platforms in parallel
- Packer does not replace configuration management
- Templates are JSON files. 
- Template components:
  - Builders: Responsible for creating machines and generating images. Examples: Amazon-ami, Docker, HyperV, VirtualBox, Vmware
  - Description: Optional, desc of what our template does
  - min_packer_version: Optional,
  - post-processors: They run after the image is built by the builder and provisioned by the provisioner. Examples: Amazon import, Checksum, Docker tag, Docker push, Google Compute Image Exporter, Shell, Vagrant, Vsphere
  - provisioners: Ansible, puppet, shell scripts, etc
  - variables: When we do a packer build, we can pass variable and runtime, this make more flexible
- Packer Commands:
  - packer build: Runs all the builds in order and generate a set of artifacts
  
    eg: packer build --var 'docker_repository=linuxacademy/express' --var 'docker_tag=1.2.5' packer.json
  
  - packer fix: Finds backwards incompatible parts and brings them up to date
  
  - packer inspect: Reads a template and outputs the various components that the template defines
  
  - packer validate: Validate the syntax and configuration of a template
  
  - packer build --changes: Build argument to overwrite the metadata of a source image
- Cloud Init: Multi Distribution package that handles early initialization of a cloud instance
  - Comes installed in the Ubuntu Cloud Images and images available on Ec2, azure, etc
  - Use Cases:
    - Setting a default locale
    - Setting an instance hostname
    - Generating instance SSH private keys
    - Adding SSH keys to a users .ssh/authorized_keys
    - Setting up ephemeral mount points
  - Commands:
    - cloud-init init: Initialize cloud-init and performs initial modules
    - cloud-init modules: Activates modules using a giving configuration key
    - cloud-init single: Runs a single module
    - cloud-init dhclient-hook: Runs the dhclient hook to record network info
    - cloud-init features: List defined features
    - cloud-init analyze: Analyzes cloud-init logs and data
    - cloud-init devel: Runs development tools
    - cloud-init collect-logs: Collects and tar all cloud-init debug info
    - cloud-init clean: Removes logs and artifacts so cloud-init can re-run
    - cloud-init status: Reports cloud-init status or wait on completion
  - Formats:
    - user-data script: Typically used by those who just want to execute a shell script, begins with #! or Content-Type: text/x-shellscript when using a MIME archive
    - include file: This content is an include file, begins with #include or Content-Type.....
    - Cloud config Data: This is the simplest way to accomplish some things via user-data, using cloud config sintax the user can specify certain things in a human friendly format. Begins with #cloud-config or Content-Type.....
    - Upstart Job: Content is placed into a file in /etc/init, and will be consumed by upstart as any other upstart job. Begins with #upstart-job or .......
    - Cloud Boothook: This content is boothook data. It is stored in a file under /var/lib/cloud and the executed immediately. Begin with #cloud-boothhook or ....
    - Part Handler: It contains custom code for either supporting new mime-types in multipart user data or overriding the existing handlers for supported mime-types. Begins with #part-handler or .....





## Configuration Management

### Configuration Management with Ansible, Puppet, and Chef

- Configuration Management

  - Resolves configuration drift
  - Maintaining a desired state
  - The process of systematically handling changes to a system
  - Maintains integrity of a system over time
  - Idempotent Behavior, is an operation that can be applied multiple times without changing the result beyond the initial application
  - Configuration management tools will avoid repeating task
  - The desired state is maintained even if you run it multiple times

- Why use configuration management?

  - Quick provisioning of new servers
  - Quick recovery from Critical Events (provision new servers for example)
  - Version control for the server environment
  - Replicated environments
  - Infrastructure because disposable

- Puppet

  - Is Designed to manage the configuration of unix and windows declaratively

  - Describes system resources and their state using the Puppet DSL

  - The Puppet DSL is based on ruby

  - Resources types are used to manage system resources

  - Resources types are declared in manifest files

  - **Commands:**
    
    - apply: manages systems without needing to contact a Puppet master server.
    - agent: manages systems, with the help of a Puppet master.
    - cert: helps manage puppet built in certificate authority
    - module: is a multi-purpose tool for working with Puppet modules
    - resource: lets you interactively inspect and manipulate resources
    - parser: lets you validate puppet code to make sure it contains no syntax error
    
  - Example code snippet

    ```
    user { ‘username’:
      ensure     => present,
      uid        => ‘102’,
      gid        => ‘wheel’,
      shell      => ‘/bin/bash’,
      home       => ‘/home/username’,
      managehome => true,
    }
    ```

- Chef

  - Configuration management tool written in Ruby, uses pure Ruby DSL

  - Uses a client-server model

  - It utilizes a declarative approach to configuration management

  - Resources are idempotent

  - Use resources to describe your infrastructure

  - **Chef testing tools: You need install the Chef DK package**
- Cookstyle: is a linting tool based on the RuboCop Ruby linting tool
    - Foodcritic
    - ChefSpec
    - **InSpec: is a framework for testing and auditing your applications and infrastructure**. InSpec provides a DSL for writing compliance tests that can be run locally or remotely using SSH or WinRM.
    - Test Kitchen
    
- A Chef recipe is a file that groups related resources
  
- Chef cookbook provides structure to your recipes
  
- **The knife command-line tool that provides an interface between a local chef-repo and the Chef server.**
  
- chef-client is an agent that runs nodes managed by Chef
  
  - The agent will bring the node into the expected state. Steps performed by chef-client.
      - Registering and authenticating the node with the Chef server
      - Building the node object
      - Synchronizing cookbooks
      - Taking the appropriate and required actions to configure the node
      - Looking for exceptions and notifications
    
  - chef-server-ctl

    - is used to:
- Status, show the status of all services available to the Chef server
      - service-list, display a list of all available services
    - Start and stop individual services
      - Reconfigure the Chef server
      - Gather Chef server log files
  - Backup and restore Chef server data
    - chef-server-ctl restore PATH_TO_BACKUP
- chef-server-ctl backup-recover
    
    - chef-server-ctl cleanse
- chef-server-ctl gather-logs
    
    - chef-server-ctl ha-status
- chef-server-ctl show-config
    
    - chef-server-ctl restart SERVICE_NAME
- chef-server-ctl service-list
    
    - chef-server-ctl start SERVICE_NAME
- chef-server-ctl status
    
    - chef-server-ctl stop SERVICE_NAME
- Chef-solo: A command that executes chef-client to converge cookbooks in a way that does not require the Chef server. Uses chef-clients Chef local mode. Does not support centralized distribution of cookbooks or a centralized API that interacts with and integrates infrastructure components or authentication or authorization.
    - Cookbooks: A cookbook is the fundamental unit of configuration and policy distribution. A cookbook defines a scenario and contains everything is required to support that scenario:

      - Recipes that specify the resources to use and the order in which they are to be applied
  - Attribute values
      - File distributions
  - Templates
      - Extensions to Chef, such as custom resources and libraries
- Cookbooks Commands:
  
  - knife cookbook
      - knife cookbook generate COOKBOOK_NAME (options)
  - knife cookbook delete COOKBOOK_NAME (cookbook_version) (options)
      - knife cookbook download COOKBOOK_NAME (cookbook_version) (options)
  - knife cookbook list (options)
      - knife cookbook metadata (options)
    - knife cookbook show COOKBOOK_NAME
      - knife cookbook upload (COOKBOOK_NAME) (options)
    
  
- Ansible

  - Opensource software that automates software provisioning, configuration management, and application deployment, is Agentless. Have playbooks, is like manifest in puppet or recipes with chef. Use YAML
  - Commands:
    - ansible `host-pattern`: Execute a single task playbook in a set of host
    - ansible-config `view|dump|list`: View, edit and manage ansible configuration
    - ansible-console `host-pattern`: REPL console for executing Ansible tasks
    - ansible-doc: Plugin documentation tool
    - ansible-galaxy: This command manages Ansibles roles in shared repositories. The default is galaxy.ansible.com
    - ansible-inventory: Used to display or dump the configured inventory as Ansible sees it
    - ansible-playbook: Run Ansible playbooks, executing the defined tasks on the targeted hosts
    - ansible-pull: Pull playbooks from a VCS repo and executes them for the local host
    - ansible-vault: Encrypt/decrypt utility for Ansible data files
  - Ansible inventory: Default path /etc/ansible/hosts. Is an inventory of servers, you can create groups, like webservers: server1, server2. etc. With -i you can define another host file. You can write this file in two ways, one in yaml, and the other also is simple.
  - Configuration file: Changes can be made and used in a configuration file which will be searched for in the following order:
    ANSIBLE_CONFIG (env variable if set), second ansible.cfg (in the current directory), third .ansible.cfg (in the home directory). And the last is in /etc/ansible/ansible.cfg

- Ansible Vault: stores sensitive data such as passwords or keys in encrypted files. Any file in Ansible can be encrypted by Vault. The ansible-vault CLI tool is used to edit files. If you need to execute a playbook with a pass in vault you need te execute: `ansible-playbook --ask-vault-pass --vault-password-file`

  - Commands:

    - Creating Encrypted Files: ansible-vault create foo.yml
    - Editing Encrypted Files: ansible-vault edit foo.yml
    - Rekeying Encrypted files (change pass): ansible-vault rekey foo.yml bar.yml
    - Encrypting Unencrypted Files: ansible-vault encrypt foo.yml bar.yml
    - Decrypting Encrypted Files: ansible-vault dencrypt foo.yml bar.yml
    - Viewing Encrypted Files: ansible-vault view foo.yml bar.yml baz.yml
    - Create encrypted variables to embed in yaml: ansible-vault encrypt string --vault-id a_password_file `foobar` --name `the_secret`

    

## Container Management

### Docker and Kubernetes

* **Docker** is designed to make it easier to create, deploy and run apps by using containers. 
  * Docker Engine is a client-server application:
    * The server runs as a daemon process
    * REST API
    * CLI client, the most common way to talk with docker
  * Commands:
    * `docker attach`: Attach local standard input, output and error streams to a running container. When you detach the container shut down
    * `docker build`: Build an image from a Dockerfile
    * `docker exec`: Run a command in a running container
    * `docker images`: List Images
    * `docker info`: Display system-wide information
    * `docker inspect`: Return low-level information about Docker objects
    * `docker logs`: Fetch the logs of a container
    * `docker network`: Manage Networks
    * `docker node`: Manage Swarm nodes
    * `docker ps`: List running containers
    * `docker pull`: Pull an image or a repository from a registry
    * `docker push`: Push an image or a repository to a registry
    * `docker restart`: Restart one or more containers
    * `docker rm`: Remove one or more containers
    * `docker rmi`: Remove one or more images
    * `docker run`: Run a command in a new container
    * `docker start`: Start one or more stopped containers
    * `docker stop`: Stop one or more running containers
    * `docker swarm`: Manage Swarm, initialize swarm, etc
    * `docker volume`: Manage volumes
* **Docker Images** is built up from a series of layers, each layer represents an instruction in the images Dockerfile. Each layer except the very last one is read-only, data changes are stored in this writable layer. Each instruction on a DockerFile create a new layer
  * `docker build . -t nah/static:latest`: Build your images (before you need to create a Dockerfile)
  * `docker images`: List your images
  * `docker run -d --name=static-site -p 80:80 la/static:latest`: Run your container
  * `docker ps`: View your running container
* **Docker Volumes** is for managing persistent data with Docker. You can use volumes, or mounts for this.
  * Commands:
    * `docker run -d --name=nginx --mount type=bind,source=nginx-vol,destination=/var/www,readonly nah/static:latest`: Mount volume in read only mode 
    
      (The 'bind' type will allow the container access to the underlying host operating system from the indicated target directory in the container filesystem)
    
    * `docker volume inspect`: check your created volumes
    
    * `docker volume ls`: List your volumes
    
    * `docker volume rm $NAME`: remove your volumes
* **Docker Networks** you can connect dockers together or with others computers.
  Docker Networks Drivers for our docker hosts and containers determine their behavior on each host as well as accessibility and routing to them.
  
  * Network Driver Types:
    * Bridge: Usually used when your application run in a standalone container, and it need to communicate with other things
    * Host: Also this is going to use for standalone containers
    * Overlay: Connect multiple docker daemon together and enable swarm service to communicate  with each other. You can also use to communicate 2 alone containers with deferents docker daemons
    * Macvlan: Allow you to assign a mac address to a container, appear like is on a physical device on your network
    * None: Disable networking
  * Bridge Network is a link layer device with forwards traffics between network segments, this can be a hardware device or a software device. Allow containers connected to the same bridge network to communicate a same time provides isolation from other containers not connected to the same bridge network. A default bridge network is created:
    * Creating user-defined custom bridge network is also possible
    * User-defined bridge network are superior to the default bridge network
    * Provides automatic DNS resolution between containers
    * Linked containers in a  user defined network is not going to share environment variables.
  * Overlay Networks creates a distributed network among multiple Docker daemon hosts. Allows containers connected to the network to communicate securely
  * Host Networks is the actual host network of the docker server, if you use this network in a docker containers is gonna be *no isolation* from the docker host. For example a container which bind to port 80 it will be available on port 80 on the hosts IP address
  * Macvlan Network this assigns a MAC address to each container virtual network interface. 
    It gives the appearance of being on a physical network interface
  * Commands:
    * `docker network create $NAME`: Create a new network
    * `docker network ls`: List the networks
    * `docker network inspect $NAME`: We can inspect the network for view more detailed information
    * `docker run -d --name=my-nginx --network my-net -p 8080:80 nginx:latest`: Run a container with the network `my-net`
* **DockerFiles** is a set of instructions for automatically building Docker images, the instructions are not case-sensitivity, but is better in UPPERCASE to distinguish them from arguments more easily. The instructions are run in order. Dockerfiles MUST start with a `FROM` instruction.
  Most important instructions:
  
  * FROM: Initializes a new build stage and sets the Base image for subsequent instructions
  * RUN: Executes any commands in a new layer on top of the current image and commit the results
  * CMD: Provides defaults for an executing container
  * LABEL: Adds metadata to an image
  * MAINTAINER: Sets the Author filed of the generated images
  * EXPOSE: Informs Docker that the container listener on the specified network port at runtime
  * ENV: Set the environment variables
  * ADD: Copies new files, directories, or remote files to the Docker image
  * COPY: Copies new files or directories (but not remote files) to the Docker Container (not the docker image it self)
  * ENTRYPOINT: Allows you to configure a container that will run as an executable
  * VOLUME: Creates a mount point
  * USER: Sets the username
  * WORKDIR: Sets the working directory (You can use multiple times in DF)
  * ARG: Defines a variable that users can pass at build-time
* **Kubernetes** is opensource container orchestration tools, is developed by Google, es container runtime-agnostic. Features provide everything needed for deploying containerized applications
  * Container Deployments & Rollout Control
  
  * Resource Bin Packing: Define min and max compute resource (cpu / mem)
  
  * Built-in Service Discovery & Autoscaling
  
  * Heterogenous Cluster: You can run Kubernetes everywhere, vms, cloudservers, on-premise servers
  
  * Persistent Storage: Out the box, you can use for example amazon EBS, GCP Persistent disks, etc
  
  * HA Features: Cluster featuring support, is one cluster is down, containers automatically move to another cluster
  
  * **Pods** the smallest unit of deployment, a Pod runs containers. Each Pod has its own IP addess and shares a PID namespace, network and hostname.
  
    * `kubectl get pods`: Get a pod lists
    * `kubectl create -f pod.yml`: Create a resource from a yml file
    * `kubectl delete pod nginx`: Delete a Nginx Pod
  
  * **ReplicaSets** is a set of pod templates that describes a set of Pod replicas. The ReplicaSet ensures that a specified number of Pod replicas are running at any time, If you need delete pods, you need to delete the RS. *ReplicaSet is the new version of Replication Controller*
    Commands:
  
    * `kubectl get replicasets`: Get a ReplicaSet list
    * `kubectl scale --replicas=4 replicaset/replicaset-demo`: Change the quantity of pods in a ReplicaSet
    * `kubectl delete replicaset replicaset-demo`
  
  * **Deployments** is for lifecycles, the provide the same replication functions (through replica sets) and also the ability to rollout changes and roll them back if it is necessary. I'd recommend using deployments over RCs as they are much more powerful.
    Commands:
  
    `kubectl get replicasets`
  
    `kubectl get deployments`
  
    `kubectl scale --replicas=4 deployment/nginx-deployment`
  
    `kubectl delete deployment nginx-deployment`
  
     

### Docker Compose

* Docker Compose Commands:

  * `docker-compose version`
  * `docker-compose build` take a dockerfile and build an image from that
  * `docker-compose up` start the services in the foreground
  * `docker-compose up -d ` start the services in the background
  * `docker-compose stop` stop the containers
  * `docker-compose restart` restart the services
  * `docker-compose ps` show the containers status
  * `docker-compose rm -f` delete the stopped containers 
  * `docker-compose images` list the docker images
  * `docker-compose logs` show the running containers information
  * `docker-compose pause` pause the running containers, you can start again with `unpause`
  * `docker-compose down` stop and remove the containers, remove the network, remove the images and volumes
  * `docker compose config` validate the docker-compose.yml syntax

* Docker Compose File example

  ```
  version: '3.3'
  services:
     db:
       image: mysql:5.7
       volumes:
         - db_data:/var/lib/mysql
       restart: always
       environment:
         MYSQL_ROOT_PASSWORD: somewordpress
         MYSQL_DATABASE: wordpress
         MYSQL_USER: wordpress
         MYSQL_PASSWORD: wordpress
     wordpress:
       depends_on:
         - db
       image: wordpress:latest
       ports:
         - "8000:80"
       restart: always
       environment:
         WORDPRESS_DB_HOST: db:3306
         WORDPRESS_DB_USER: wordpress
         WORDPRESS_DB_PASSWORD: wordpress
         WORDPRESS_DB_NAME: wordpress
  volumes:
      db_data: {}
  ```

  



### Docker Swarm And Docker Machine

* **Docker Swarm** is a clustering and scheduling tool for Docker containers. It allows you to establish and manage clusters of Docker nodes. Provides redundancy for failover, orchestration, deployment and service management.

  Feature Highlights:

  * Cluster management integrated with Docker Engine
  * Decentralized design: Various docker servers can connect to a Docker Swarm
  * Declarative service model
  * Scaling
  * Desired state reconciliation: For example if we lost 3 containers, Docker Swarm is going to recreate those 3 containers on a different worker node
  * Multi-host networking: Automatically assign addresses to the containers on the overlay network. Each container in the swarm is assign a unique DNS this allow load balancing of running containers
  * Service discovery
  * Load balancing
  * Secure by default
  * Rolling updates: Node per node updates. If the deployments go wrong, we can go back and use a previous version

  Swarm are multiple docker hosts running in *swarm* mode. Nodes can be managers, workers or both. Creating a service defines its optimal state (number of replicas, network, storage, expose ports). Docker works to maintain that desired state

  Components:

  * Nodes: Docker engine instance that participates in the swarm. Two types of nodes:
    * Manager nodes: The manager dispatch task to the worker nodes. Also perform orchestration and cluster management to make sure the swarm is running in his desired state. For default manager are configured to be workers, but you can change this
    * Worker nodes: Receiving and executing tasks.
  * Services and tasks:
    * Services: Is a task definition,
    * Replicated services: We can specify the number of tasks that we want to have replicated
    * Global services: The Swarm run a task for the service on every available node in the cluster
    * Task: They are docker containers that are executing a command. Is the atomic scaling unit for swarm
  * Loadbalacing:
    * Ingress load balancing: To expose a service and make available externally
    * PublishedPort
    * Internal load balancing
  * Commands:
    * `docker swarm init --advertise-addr $IP`  Initialize a swarm
    * `docker swarm join --token $TOKEN $IP`  Add worker server to the Swarm
    * `docker node ls` List the nodes
    * `docker node inspect $NODE` Inspect specified node
    * `docker swarm leave` Leave the swarm
    * `docker swarm unlock` Unlock swarm (if it has been locked)
    * `docker swarm update` For make changes in the swarm
    * `docker service create` Create a new service
    * `docker service create --name myservice --replicas=5 httpd` Create a service called 'myservice' with 5 replicas from the 'httpd' image
    * `docker service scale` Scale on or multiple replicated services
    * `docker service rm` Remove one or more services
    * `--mount type=volume,destination=/path/in/container` This will allow the container to use a volume within the service mounted to the path indicated

* **Docker Machine** is a tool that lets you install Docker Engine on a virtual host and manage the host with docker-machine commands. You can install DM in virtual hosts, your local system, servers in your data center, and Cloud providers. DM have two important uses cases:

  * I have an older desktop system and want to run Docker on Mac or Windows
  * I want to provision Docker host on remote system

  With Docker Machine Drivers we can use different services, like AWS, Azure, Digital Ocean, GCE, Generic (your computer), Hyper-V, OpenStack, Oracle VirtualBox and Vsphere

  Commands:

  * `docker-machine create --driver=virtualbox vbox-test` create a new docker machine
  * `eval $(docker-machine env vbox-test)` configure the default node
  * `docker-machine active` show the active node
  * `docke-machine config $NODE_NAME` This will show configuration information regarding security and keys, but not a full JSON listing for configuration
  * `docker-machine inspect $NODE_NAME` Get the complete configuration of an active machine in JSON
  * `docker-machine ls` list all docker-machine
  * `docker-machine active` show the active 'machines' running on your instance
  * `docker-machine provision $NODE_NAME`
  * `docker-machine restart $NODE_NAME` 
  * `docker-machine ssh $NODE_NAME`

  

## Software Engineering

### Modern Software Development

* **RESTful APIs** is REpresentational State Transfer is an architectural style that allow computers communicate with others. The most important point of rest is they are *stateless* (separation of client and server), this means you can go and change the code on your client side and that is not gonna go an impact anything on the server side. And likewise you can make changes to your server side code and its not go on impact your client. For this, they need a common format of being able to communicate with one another. For example, if we change our frontend code, this does not impact our backend code.

  Requests and Responses: Rest requires that a client make a request to the server. 
  Send a Request:

  * HTTP verb
    * GET Reads data
    * POST Creates resources
    * PUT Update resource, need ID
    * DELETE Removes resource, need ID
  * Header: The clients send the type of content that it is able to receive
    * Accept
    * MIME
      * Type/subtype
      * Application/json
      * Application/xml
  * Resource path: Requests must contain a path to a resource. Path should be the plural for example `/customers`. Append an id to the path when accessing a single resource `/customers/:id`
  * Message Body (optional)

  Get a Response: (The server need)

   * Content type

   * Response/Status Code

     	* 200 OK: Successful request
      * 201 CREATED: A resource has been created, for example using POST
         * 202 ACCEPTED: The request has been accepted but it hasn't been completed
     * 204 NO CONTENT: Successful HTTP request, where nothing is being returned in the response body
     	* 400 BAD REQUEST: The request wasn't understood by the server due to malformed syntax
     	* 401 UNAUTHORIZED: Either the authentication header is missing or it contains invalid credentials
     	* 403 FORBIDDEN: The client does not have permission to access this resource
     	* 404 NOT FOUND: A resource matching the request doesn't exist
     	* 405 METHOD NOT ALLOWED: The requested operation is not supported on the specified Artifact type by the Services API (for example you do a UPDATED, and its not supported)
     	* 409 CONFLICT: This status code may be returned if you try to update an object while it is undergoing an asynchronous operation
      * 500 INTERNAL SERVER ERROR: An unhanded exception occurred on the server

   * REST Example:
     Request

     ```
     GET /customer/123
     Accept: application/json
     ```

     Response

     ```
     Status Code: 200 (OK)
     Content-type: application/json
     {
     	"customer":(
     	"id":123,
     	"first_name": "Bob")
     }
     ```

* **Service Oriented Architecture** (SOA) it's an approach used to create an architecture based of the use of services (microservices based in SOA). Is an approach to distributed systems architecture, this services are: *Loosely coupled services, Standard interface and protocol, and Seamless cross-platform integration.* 
  This services communicates over an enterprise service bus (ESB)
  A service haces for properties:

  * It logically represents a business activity with a specified outcome (e.g. processing credit cards, providing weather data or consolidating a report)
  * It is self-contained
  * It is a black box for its consumer
  * it may consist of other underlying services

  SOA Principles:

  * Standardized service contract
  * Service autonomy
  * Service discovery
  * Service reusability
  * Service encapsulation

  SOA Manifesto:

  * Business value is given more importance than technical strategy
  * Strategic goals are given more importance than project-specific benefits
  * Intrinsic inter-operability is given more importance than custom integration
  * Shared services are given more importance than specific-purpose implementations
  * Flexibility is given more importance than optimization
  * Evolutionary refinement is given more importance than pursuit of initial perfection

  Monolithic vs. SOA vs. Microservices

  |   Monolithic    |      SOA       | Microservices  |
  | :-------------: | :------------: | :------------: |
  |   Single Unit   | Coarse-grained |  Fine-grained  |
  | Tightly coupled | Loosely couple | Loosely couple |

* **Microservices** breaks an application up into a collection of small, loosely-coupled services (The opposite of monolithic architecture). Protocols in microservices should be lightweight like rest, and also is very popular for microservices messages queues.
  Services in microservices are independent: codebase, running independently process, built independently, deployed independently, scaled independently

  Microservices Advantages:

  * Modularity
  * Flexibility: each microservices can be written in different language, always you should use the best tool for the case.
  * Scalability: you can scale individual part of your application.
  * Maintainability: individual teams work in microservices, they are more focuses 
  * Continuous refactoring
  * Enable continuous integration and delivery

* **Agile** is a set of values and principles, help breakdown silos organizations so teams can we work together. Adaptive planning with evolutionary development, early delivery and continuous improvement. Rapid and flexible response to change
  The Agile Manifesto says 

  * Individuals and interactions more than processes and tools
  * Breakdown barriers and make more relationships
  * Customer collaboration is more important than contract negotiation
  * Responding to Change more than following a plan

* Test Driven Development is a software development process that relies on the repetition of a very short development cycle.

  * Writes an automated test case that defines a desired function
  * Produces the minimum amount of code to pass that test
  * Refactor the new code to acceptable standards

  TDD Steps:

  * Write tests, run failing tests, write code, run passing tests and refactor code, repeat.



### CI/CD and Jenkins

* **Continuous Integration** is a critical part of any DevOps pipeline, is the practice of frequently merging code changes done by developers. In this practice the code is merged constantly throughout the day, this is a company with the execution of automated test to verify the build, the cycle is gonna start over again and repeat -> You write code, check it in, it gets tested, gets merge, it gets test.
  A developer commits a code change, the CI server sees the change and automatically performs a build, and a after that automated test are executed against the build, developers are notified if the build fails.

  With CI we can:

  * Early detection of certain types of bugs
  * Eliminate the scramble to integrate just before a big release
  * Make frequent releases possible (and smaller)
  * Make continuous testing possible
  * Encourages good coding practices

* **Continuous Delivery and Continuous Deployment**, Continuous delivery (CD) is the practice of continuously maintaining code in a deployable state in other hand Continuous Deployment is the practice of frequently deploying small code changes to production, also code is always in a deployable state. Code is deployed to prod frequently.

  Code goes through a series of stages:

  * automated build
  * automated testing
  * manual acceptance testing
    The result is a deployable artifact or package, after that we deploy a prod automated. If the deploy fail can be rolled back using an automated process

  Advantages of CICD:

  * Faster time-to-market
  * Fewer problems caused by the deployment process, more smaller frequents releases
  * Lower risk because the smaller changes
  * Reliable rollbacks
  * Fearless deployments, its a routine, is easy

* **Jenkins** is a open source automation tool, can be used to automate:

  * Building
  * Testing
  * Delivering
  * Deploying Software

  Supported version control tools like CVS, Subversion, git, Mercurial, Perforce, RTC.

  Important Concepts:

  * Artifact: is an immutable file that is generated by a build or pipeline when Jenkins runs
  * Core: is the primary Jenkins application, is the Jenkins war file, this provides the basic web ui and configuration and foundation for all the plugins
  * Downstream:  A downstream job is a configured project that is triggered as part of a execution of pipeline.
  * Fingerprint: Is a Hash, is unique, and we use this to track the usage of an artifact
  * Master: Is the master node of Jenkins
  * Node: Is capable of executing pipelines or projects
  * Project: Is a user configured description of work in Jenkins
  * Pipeline: Is a user defined model of a CD pipeline, is like a project but this time it's gonna built for continuous delivery.
  * Plugin: With plugins we can extend the Jenkins functionality
  * Stage: Is a part of a pipeline, and is used to go and define distinct subsets of the entire pipeline
  * Step: Is a single task, task tell Jenkins what to do inside of a pipeline or project
  * Upstream: An upstream job is a configured project that triggers a project as of its execution
  * Workspace: Is a disposable directory on the file system of the node that is executing the work (pipeline or project)
  
* **Artifact Repositories** are tools designed to optimize the download and storage of binary files. A binary repository is a software repository for packages, artifacts and their corresponding metadata. 
  Store binary files produced by an organization itself:

  * Product releases
  * Nightly product builds
  * Third party binaries

  Build Tools: Jenkins, Bamboo, TeamCity, Maven, Gradle, Ivy, Travis CI, Circle CI, TFS

  Support for Orchestration Tools: Chef, Kubernetes, Helm, Mesos, Puppet, Docker Swarm

  Storage: S3, Google Cloud Storage, Azure Blog Storage

  Support for: Maven/Java

  Popular Artifacts Repositories:

   * Artifactory
      * Package support for: 
        - Docker: you can have a private docker registry
        - YUM/APT: for example approved packages
        - NPM: you can have your own NPM registry, for example a remote proxy for NPM, if NPM goes down you can use your local NPM registry
        - Maven: you can store your Maven artifacts
        - NuGet: Is like a Ruby Gem server or py server for Python. This is for .NET
        - Ruby:/Python repository for your Gems or proxy remote
        - Chef: you can have your own Chef Supermarket
        - Puppet: Same with Forge.
        - Git: Mostly for large files storage
        - PHP: For PHP packages
        - Bower: Go packages
   * Nexus
      * Package support for: Maven/Java, NPM, NuGet, RubyGems, Docker, APT, YUM, Gradle, Ant, Ivy

  



### Git

* Uses a distributed architecture, Git takes a snapshots of the current state for every commit (not is delta-based), if a file not change, git use a old snapshot. Git uses checksums to maintain data integrity, this means you cant lose information or get corrupted files without get knowing about it. Nearly all of Git actions only add data

* **Git file states:**

  * Committed: The data is safely stored and your get database
  * Modified: If you have changes that have not yet been committed,
  * Staged: Commit and go to the new commit snapshot (select files for you next commit is the staging area)

* **Commands:**

  * `git push origin master` master is the current branch, and the origin is the git repository

  * `git remote -v` show the remote gits repository

  * `git logs` show commits, author, date, and files

  * `git show` show the changes of the last commit with diff

  * `git diff FILE` show the local changes of a file

  * `git commit --amend -m` for change the commit message

  * `git rm --cached FILE` If you need remove a stage file in the index, a file add with git add

  * `git reset --hard` delete files they aren't committed, delete all changes after the last commit. This command delete the information forever

  * `git revert HEAD` is like the last commands, but with logs/history. With head you revert the last commit, you can change the commit id

  * `git stash` help you to temporarily but safely store your uncommitted local changes - and leave you with a clean working copy. Is recommended or even required: when merging branches, when pulling from a remote, or simply when checking out a different branch.

  * `git branch dev` creates a new branch called dev. `git branch` list the branches

  * `git checkout dev` change to dev branch

  * `git checkout -b test`create the branch test, and change to test branch, is a shortcut

  * `git merge feature_branch` on the branch DEV, this merge feature to dev, after that do `git push origin dev`. Merging takes the contents of the feature branch and integrates it with the master branch. As a result, only the master branch is changed. The feature branch history remains same.

  * `git branch -d feature_branch` delete branch

  * `git rebase new_branch` on the branch DEV, is similar to merge. When you do rebase a feature branch onto master, you move the base of the feature branch to master branch ending point.

  * **When to make a rebase? When to Merge?**

    If the feature branch you are getting changes from is shared with other developers, rebasing is not recommended, because the rebasing process will create inconsistent repositories. For individuals, rebasing makes a lot of sense.

    If you want to see the history completely same as it happened, you should use merge. **Merge preserves history whereas rebase rewrites it**
  
* **Dealing with Conflicts** (merge conflicts)

* **Git Submodules** There are times when you need to include a vendor library in your code. You can accomplish this with submodules. 

  `git submodule add http://$REPO.git`

  To remove a submodule: `git rm --cached $REPO` `git rm .gitmodules`

* **Tagging in Git** are a fixed point in history pointed to a commit or branch in a point of history.

  `git tag 1.0` ->`git push origin v1.0`



### Deploying Code to Production

* **Immutable Servers**, in this approach we replace the server, don't update it. We don't making configuration changes on a running server, the idea is to go and create a new one and then replace it. For this we need to prebaking images with everything they need (application, configuration with dependencies). Images can be tested, failed images are rejected, and if passes, then deploy to prod. We can use CI/CD tools to automate deploys, and use a deployment strategy like blue-green or canary 
  Immutable servers VS server with configuration management:

  * With immutable servers, the servers are disposable
  * With CM, we thing our servers they are persistent (if the server fail, we need to make troubleshooting)
  * With Immutable servers the changes applied to the base image, with CM we make the changes in running systems
  * With immutable servers we can test our images, with CM we can't

* **Blue-Green Deployments** is a technique that reduces downtime and risk by staging a new production environment that simultaneously runs along side the older production environment. Blue is the older version and Green is the new version. Traffic is routed to both Blue and Green. Once validated, Blue is taken offline and traffic is only routed to the Green environment.

  * Steps:

  1. Deploy out the green version of your application.
  2. Verify that the green deploy works by checking the route in a browser.
  3. Map the route of the blue app to the green app.
  4. Verify that things are working properly by checking the route of the blue app in a browser.
  5. Once verified that the green app is accessible using the route from the blue app, unmap the route to the blue app.
  6. Verify that traffic solely routes to the green deploy.
  7. Deprovision the blue app if you are using cloud servers. At this point, the green deploy becomes the blue deploy.

* **Canary Deployments** is like Blue-Green, but we can assign percents, for example, the 10% of our users use the new version, and the 90% of our users use the old version, if everything is OK, we move all the users to the new version.

  * Steps: 

  1. All traffic is routed to the old version of the application.
  2. Most traffic is routed to the old version of the application and a segment of the user base is routed to the new version of the application.
  3. Once vetted all traffic is routed to the new version of the application.



### Standard Components and Platforms for Software

* **Content Delivery Network** is a collection o proxies servers located all over the world that is used to help facilitate the delivery of your content faster. Content like images, JavaScript and static files. To minimize the distance between the visitors and your website’s server, a CDN stores a cached version of its content in multiple geographical locations (a.k.a., points of presence, or PoPs). Each PoP contains a number of caching servers responsible for content delivery to visitors within its proximity.
  
  * Benefits: Uptime reliability, Improving website load times, reducing bandwidth costs, increasing content availability and redundancy, improving security (ant ddos)
  * Push VS Pull CDNs:
    * Push: Content is distributed proactively to edge servers in the CND locations. The visitors requests goes to the closest points of Presence (PoP) location rather than the origin server. You are responsible for pushing your content, this makes if you do any chance to a image, or video, you are responsible for going and update.
    * Pull: The end-user sends the requests that pulls content from nearest edge server, the CDN pulls the content from origin and caches it. The only negative is for the first user the content need to request to the origin server.
* **Cloud Foundry** is PaaS (Manages only applications and data) multi cloud originally developed by Vmware. Is IaaS platform agnostic (aws, google, vmware, openstack, etc). You can build, test, deploy your apps easily, is Open source, commercial product or through a hosting provider. The cli is supported on every OS. Support any language or framework, also supports docker images.
  
  * Commands:
    * `cf SUB_COMMNAND`
    * `cf push APP_NAME --random ruoute`  push your app to CloudFoundry
    * `cf apps` list your applications
    * `cf app APP_NAME` more information about an application
  * Manifest file for the application resource, and the number of instances.
* **Openstack** is an Opensource IAAS for Cloud Computing, controls large pools of Compute, storage and networking
  
  * Components: (REVIEW!!!)
    * Computer (Nova): Fabric controller, is the main part, can work with many virtualization technologies
    * Networking (Neutron)
    * Block Storage (Cinder)
    * Identity (Keystone): Like IAM
    * Image (Glance): Image registry
    * Object Storage (Swift): Like Drive or Dropbox
    * Dashboard (Horizon)
    * Orchestration (Heat): Is like cloudformation
    * Workflow (Mistral): Workflow Service
    * Telemetry (Ceilometer): you can generate bills based of utilization, have an API for integrate with external billing system
    * Database (Trove): Like RDS
    * Elastic map reduce (Sahara) Hadoop Cluster
    * Bare metal (Ironic)
    * Messaging (Zaqar): Is great for Web and mobile apps
    * Shared file System (Manila)
    * DNS (Designate)
    * Search (Searchlight) is like ELK
    * Key manager (barbican)
    * Container orchestration (Magnum)
    * Root Cause Analysis (Vitrage)
    * Rule-bases alarm actions (Aodh)
    
    

### Cross Site Scripting, ACID and CAP Theorem

* **Cross Site Scripting** is the most common of injection attack (47% of the vulnerabilities). Malicious scripts are injected into trusted websites, and then they send the link to a victim and therefore execute this malicious script.

  * Reflected XSS Vulnerability: the attack sends a link to a victim -> the victim clicks the link and visits the site -> the vulnerability loads a script from an external site into the target page -> The script has full access to the browser DOM environment (include any kind of cookies) -> The script performs a malicious action as the signed-in user

  * Stored XSS Vulnerability: This is more dangerous than a reflect vulnerability, because:

    * The attack stored XSS attack and therefore it can be automated

    * A victim in a stored XSS attack doesn't have to take any action

      Malicious data is stored in a database or some other storage. When a visitor visits the site the malicious action is executed.

  * How to mitigate XSS vulnerabilites:

    * HttpOnly Flag: Prevent Javascript from accessing cookies
    * Validate your data: For ex: 
      * Is the data an integer?
      * is the data numbers and dashes e.g. a credit card date filed?
    * Escape and sanitize your data:

  Injection Theory: When an attacker's attempt to send data to an application in a way that will change the meaning of commands being sent to an interpreter.

*  **CORS Headers** Cross-origin resource sharing (CORS) is a mechanism that allows restricted resources on a web page to be requested from another domain. 

  Pre-flight request: Is used to check if request is allowed, the following HTTP requests methods use a preflight request PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH (don't use this POST/GET)
  A cross-origin HTTP request is made when a resource has a different origin (domain name, protocol, and port), Importants Headers:

  * Access-Control-Allow-Origin is the most important header for this, its going to indicate whether or not the responses can be shared
  * Access-Control-Allow-Credentials indicate if the request can be exposed when the credential flag is true to allow headers
  * Access-Control-Allow-Headers is gonna be used with a pre flight requests, indicate the request can be used when making the actual request
  * Access-Control-Allow-Methods is a pre flight requests, tell the client what methods are allow (put, delete, etc)
  * Access-Control-Expose-Headers is what headers can be exposed as part of response and gonna list them by their name
  * Access-Control-Max-Age this is how long a pre flight request can be cached
  * Access-Control-Request-Headers is also gonna be used with a pre flight request, lets the server know which headers will be used
  * Access-Control-Request-Method is also gonna be used with a pre flight request and let the server know which http methods will be used when the actual request is made
  * Origin indicate where the fetch originally came from

* **CSRF Tokens** is  Cross-Site Request Forgery, is a type of attack that tricks the victims browser (client side) and execute some malicious request. The victim issues a unexpected request to the web server (like transfer founds, or changing your email address).
  This require a little of social engineering and a link, for e.g. if you are logged into your bank suddenly the attacker can transfer your money.

  Anti-CSRF tokens are used to prevent attackers issuing requests by the victim, a token is sent as a hidden field in the form and another is sent as a Set-Cookie in the header of the response. The two tokens are validated on the server.

* **CAP Theorem** also named Brewer's theorem. You only can guarantee two  of tree 

  * Consistency: Every read receives the most recent write or an error
  * Availability: Every request receives a (non-error) response:
    * No guarantee that it contains the most recent write
  * Partition tolerance: System continues to operate despite an arbitrary number of messages being dropped (or delayed) by the network between nodes (for e.g one node in Virginia and the other in Oregon)

* **ACID and BASE** this deal with databases, acid is more with relational databases and base with no sql

  * Acid: Database transactions are a single logical unit of work (access and modify data). 
    Transactions need follow properties to maintain consistency in a database. ACID
    * Atomicity: A transaction is ALL of NOTHING, there is no midway point (Abort or commits)
    * Consistency: The correct state of the db before and after a transaction
    * Isolation: This ensures that multiple transactions can occur without leading to any kind of consistency in the database state
    * Durability: This make sure things are written a disc and therefore are persistent, even in the case of some failure
  * Base: NoSQL Databases (much looser than ACID guarantees)
    * Basically Available: The db appears to work most of the time, for example if you have 3 nodes and 2 are down, you still have one node up, the system is basically available.
    * Soft State: The storage dont have the be right consistent and replicas dont have to be mutually consistent all the time
    * Eventual Consistency: Eventually all going to go and get caught up, eventually if you have replicas all be going to be updated



## Deploying Code to Production

### Prometheus and Logstash

* **Prometheus** is a monitoring platform, member of Cloud Native Computing Foundation. It collects metrics by doing and monitoring targets by scraping metrics from an HTTP endpoint on the target system. Can also trigger alerts if some condition is observed to be true. Prometheus store it’s data as times series data. In Prometheus every time series data has a metric name and a Label.
  
  * Features:
    * Use a multi-dimensional data model winch allows Prometheus to go and store all data as time series every time series is going to be uniquely identified by it metrics name and a set of keys.
    * Prometheus has its own querying language, its a functional expression language, lets you go an aggregate time series data in real time. You can show the data as a graph
    * Prometheus doesn't rely on distribute storage, it uses single server nodes that are autonomous
    * For time-series collection use a pull model over HTTP
    * Pushing time-series is supported via an intermediary gateway
    * Targets discovered via service discovery or static configuration
    * Multiple modes of graphing and dashboard support
  * Exporters: Prometheus was developed for monitoring web-services, for this we need to use Exporters
    * blackbox_exporter: collect metrics on a TCP port and have it imported into Prometheus
    * consul_exporter
    * graphite_exporter
    * haproxy_exporter
    * memcached_exporter
    * mysqld_exporter
    * node_exporter: exposes a wide variety of hardware- and kernel-related metrics.
    * statsd_exporter
  * Node Exporter: Has a configurable set of collectors for gathering various types of host-based metrics, monitor the metrics for:
    * Disk IO
    * CPU Load
    * Network Statistics
    * Much more
  * Pushgateway
    * Intermediary service that push metrics from jobs which cannot be scraped
    * Only recommend for certain limited cases
    * Great for ephemeral and batch jobs:
      * These kinds of jobs may not exist long enough to be scraped
      * Push their metrics to a Pushgateway
  * Alertmanager
    * Handles alerts sent by client applications such as the Prometheus server
    * Routes Alerts: Mail, PagerDuty, OpsGenie
    * Grouping: You can create groups for alerts, with this if you have for example an outage, you only receive a single notification
    * Inhibitions, this is for suppress notification for certain kinds of alerts, for example if a database is down, you can disable the alerts dependencies
    * Silences: is a way to mute alerts for given time
  * Grafana is an opensource visualization tool, is not part of Prometheus, is a separate tool. Support different data stores, like Graphite, InfluxDB, Elasticsearch, Prometheus. Grafana can support Prometheus queries. You can create an edit dashboards
  
* **Logstash** is an open source tool for collecting, parsing, and storing logs for future use. Is also part of the Elastic stack. 
  Logstash event processing pipeline 3 stages:

  * inputs: generate events. The most common inputs are files, syslogs, redis, beats
  * filters: modify the events. Filters more commons, grok, mutate, drop, clone, geoip
    In Grok syntax is the name of the pattern that will match the text (is like grep), semantic in Grok is the field name to assign the value of the matched text
  * outputs: ship them somewhere else. More commons are Elasticsearch, file, graphite, statds

  Filebeats is a lightweight shipper for forwarding and centralizing log data, you need to install the file beat client in your server, and filebeat monitors the log files or locations that you specify, collect log events, and forward it to its destination for indexing. Filebeats have 2 components, inputs and harvesters. Harvesters are responsible for read the content of the log file line by line. 
  Beats use Heartbeat for ping your infrastructure. 
  Harvesters are configured to read a single line by default but can be configured to read multiline events such as stacktraces.
  Syslog messages and parses according to the RFC3164 format.
  
  The extensive Plugin Ecosystem is what makes Logstash so extensible.
  
  Libbeat is responsible for aggregates the events and sends the aggregated data to an output in filebeat
  
  Semantic is the name of the pattern that will match your text in gork.