# DevOps Tools Engineer Practice Exam A

1. Which of the following git commands is used to manage files in a repository? (Choose two correct answers.)
   1. **git rm**
   2. git cp
   3. **git mv**
   4. git move
   5. git copy
   
   

2. What implications does container virtualization have for DevOps? (Choose two answers.)
   1. **Containers decouple the packaging of an application from its infrastructure.**
   2. Containers require developers to have detailed knowledge of their IT infrastructure.
   3. **Containers let developers test their software under production conditions.**
   4. Containers complicate the deployment of software and require early deployment tests.
   5. Containers require application specific adjustment to the container platform.



3. The file index.php, which is being maintained in a git repository, was changed locally and contains an error. If the error has not been committed to the repository yet, which of the following git commands reverts the local copy of index.php to the latest committed version in the current branch?

   1. git lastver – index.php
   2. **git revert – index.php**
   3. git checkout – index.php
   4. git clean – index.php
   5. git repair – index.php

   

4. Which of the following statements are true about Jenkins? (Choose two correct answers.)

   1. Jenkins is specific to Java based applications.

   2. **Jenkins can delegate tasks to slave nodes.**

   3. Jenkins only works on local files and cannot use SCM repositories.

   4. Jenkins’ functionality is determined by plugins.

   5. **Jenkins includes a series of integrated testing suites.**

      

5. What happens when a merge conflict occurs in git? (Choose two correct answers.)

   1. The conflicting files remain unchanged in the local repository.
   2. Conflict markers are added to the files.
   3. A new branch containing the remote changes is created.
   4. **The affected files are flagged as conflicting.**
   5. **The newest version is placed in the local repository.**

   

6. Which Ansible modules can be used to change the contents of a file? (Choose three correct answers.)

   1. **lineinfile**
   2. **replace**
   3. **patch**
   4. insert
   5. modify

   

7. What is the Puppet equivalent to an Ansible Playbook called?

   1. **A Puppet Catalog**
   2. A Puppet Playbook
   3. A Puppet Factsheet
   4. A Puppet Declaration
   5. A Puppet Manifest

   

8. Which of the following Ansible tasks copies the file example.txt to a manage system?

   1. rsync:
      example.txt
      /tmp/example.txt src:
      dst:
   2. **copy:**
      **example.txt**
      **/tmp/example.txt src:**
      **dest:**
   3. retrieve:
      example.txt
      /tmp/example.txt src:
      dest:
   4.  cp:
      source:
      example.txt
      dst: /tmp/example.txt
   5. transfer:
      src:
      example.txt
      dest: /tmp/example.txt

   

9. Which of the following commands lists the cookbooks available on a Chef server?

   1. kitchen cookbook list
   2. chef-client cookbook list
   3. **chef-server cookbook list**
   4. chef-solo cookbook list
   5. knife cookbook list

   

10. An Ansible variable file contains the following content:
    `myapp:
    option1: one`
    Which of the following strings can be used to reference the defined variable? (Choose two correct answers).

    1. **myapp(option1);**
    2. option1@myapp
    3. myapp[‘option1’]
    4. myapp.option1
    5. **myapp{{option1}}**

    
    
11. What statement is true regarding the Swarm service created by the following command?
    `docker service create --name myweb --network webnet --mode global nginx`

    1. It runs exactly one time in the Swarm and cannot be scaled.
    2. It runs exactly once on each node in a Swarm.
    3. **It runs on one node by default and can be scaled to an arbitrary number of replicas.**
    4. It runs on all nodes which provide the network webnet.
    5. It runs only on those nodes which support the network type global.
    

    
12. Which of the following mechanisms are used for service discovery in a container environment? (Choose two correct answers.)

    1. The container platform offers a command like docker service discover which should be run within a container.
    2. **The container platform sets environment variables containing service information within the containers.**
    3. The container platform lists localhost ports assigned to containers in each container’s /etc/services file.
    4. The container platform mounts the sockets for all available services into the container’s file systems.
    5. **The container platforms maintains DNS records which point to containers offering a specific service.**

    

13. Which of the following commands lists the nodes in a Docker Swam cluster?

    1. docker-swarm listnodes
    2. docker engine ls
    3. **docker node ls**
    4. docker machine ls
    5. docker swarm nodes

    

14. Which of the following container names could have been assigned automatically by Docker?

    1. docker-c00001
    2. 2.0.17.172
    3. container
    4. c0023817
    5. **clever_ritchie**

    

15. Which elements exist on the highest level of the definition of every Kubernetes Objects? (Specify the name of one of the elements, without any values.)

    -> **POD**

    

16. How is a Docker container image retrieved from a Docker registry?

    1. Docker retrieves a ZIP archive which is extracted into the container’s root file system
    2. **Multiple stacked images are retrieved and layered on top of each other.**
       **A flat hard disk image is downloaded once per container and mounted as the root file system.**
    3. The registry merger all components of the image into one file which is shipped to Docker.
    4. The container is built from an ISO file along with a configuration for an unattended installation.

    

17. Given the following excerpt of a Dockerfile:
    `Run apt-get –y update && apt-get install –y fortunes && apt-get clean`
    Why are the multiple apt-get commands combined in one RUN statement instead of using multiple RUN statements?

    1. To prevent the commands from running in parallel because Docker executes all RUN statements in their own container at the same time.
    2. To ensure the execution order of the commands because Docker might evaluate the statements of a Dockerfile in any order.
    3. To avoid the creation of unnecessary images because Docker creates a new image for each RUN statement.
    4. **To execute both commands in the same container instance and void Docker to reset the container to the original base image.**
    5. To execute the apt-get install command only if the apt-get update command was successful because Docker does not check the success of RUN statements.

    

18. Which of the following tasks are completed by docker-compose down when it is used with additional parameters? (Choose two correct answers.)

    1. **Delete all volumes defined in the composer file.**
    2. Delete all containers defined in the composer file.
    3. **Delete all networks defined in the composer file.**
    4. Delete all images used in the composer file from the Docker nodes.
    5. Delete all images built from the composer file from their registry.

    

19. The following command is issued on two docker nodes:
    `docker network create --driver bridge isolated_nw`
    Afterwards, one container is started at each node with the parameter ``--network=isolated_nw`. It turns out that the containers can not interact with each other.
    What must be done in order to allow the containers to interact with each other? (Choose two correct answers.)

    1. Use a host network instead of a bridged network.
    2. Add the option --inter-container to the docker network create command.
    3. **Start the containers on the same node.**
    4. **Change the --network parameter of docker create to --network=isolated_nw,nofence.**
    5. Use an overlay network instead of a bridged network.

    

20. If a Dockerfile references the container’s base image without a specific version tag, which tag of that image is used to create the container?

    1. **latest**
    2. default
    3. current
    4. nightly lts

    

21. A Dockerfile contains the statements:
    `COPY data/ /data/
    VOLUME /data`
    What happens when the resulting container is started without any additional volume configuration? (Choose two correct answers.)

    1. Files existing in /data/ in the image are not available in the running container.
    2. Changes to files within /data/ affect the Docker image and all other containers derived from it.
    3. **Existing files from /data/ in the image are copied to the new volume.**
    4. An error is raised because /data/ already contains data when the volume is mounted.
    5. **A new volume is created and mounted to /data/ within the new container.**

    

22. Which section of the Prometheus configuration defines which nodes are monitored?

    1. **scrape_config**
    2. targets
    3. rules
    4. listener
    5. nodes

    

23. Which of the log messages below matches the following Logstash grok filter?
    `grok { match => [“message”, “%{SYSLOGBASE} new node
    %{IPORHOST:node}” ] }`

    1. Jun 30 00:36:49 headnode: new node 198.51.100.103 at clustermanager:12353
    2. Jun 30 00:36:49 headnode clustermanager[12353]: new node 198.51.100.103
    3. **Jun 30 00:36:49 headnode clustermanager[198.51.100.103]: new node**
    4. %{SYSLOG-FROM:headnode clustermanager[12353]} new node 198.51.100.103
    5. clustermanager[12353]: Jun 30 00:36:49 headnode new node 198.51.100.103
    

    
24. How does Prometheus gather information about monitored hosts and services?
    
    1. It implements the ICMP and SNMP protocols to ping and query remote services.
    2. It opens a webhook where monitored applications have to submit various metrics.
    3. It uses HTTP to retrieve JSON encoded metrics from the monitored objects.
    4. It queries a relational database for metrics written to the database by monitored applications.
    5. **It runs scripts on the Prometheus server which perform tests and return various metrics.**
    
    
    
25. Which of the following statements is true about load balancers?

    1. Load balancers are a security risk because they obfuscate the origin of connections.
    2. **Load balancer help to improve the availability and scalability of a service.**
    3. Load balancers are a single point of failure because they cannot be deployed redundantly.
    4. Load balancer require access to private keys in order to be able to forward HTTPS traffic.
    5. Load balancers cannot use connection content, such as HTTP cookies, to route traffic.

    

26. Which security issues exist for most publicly available Vagrant boxes? (Choose three correct answers.)

    1. **They accept SSH logins from the user vagrant with the password vagrant.**
    2. **They accept SSH logins from the user vagrant with a publicly available SSH key pair.**
    3. **The vagrant user can use sudo to obtain root privileges without additional authentication.**
    4. Their whole file system, including configuration files, is writable by any user, including vagrant.
    5. They export their file system via NFS with full write permissions without any additional restrictions.

    

27. What must be the first line of a plain text user-data configuration containing YAML configuration for cloud-init?

    1. cloud-config:
    2. --- cloud-config
    3. #!/usr/bin/cloud-init
    4. [cloud-config]
    5. **#cloud-config**

    

28. How is cloud-init integrated with a managed system image?

    1. cloud-init provides the cloud-init-worker command which has to be invoked periodically within the running instance.
    2. cloud-init provides systemd units which must be included in several stages of the booting process of the instance.
    3. cloud-init provides its own startup mechanism which replaces the instance’s original init system, such as systemd.
    4. **cloud-init provides a Linux kernel module that must be included and loaded in the instance’s initramfs.**
    5. cloud-init provides the cloud-init-daemon service which is launched during startup and keeps the instance in sync with the desired configuration.

    

29. Which of the following elements are presents in a Vagrant box file? (Choose two correct answers.)

    1. A Vagrant guest configuration file that is used to create instances of the box.
    2. **Configuration files for provisioners such as Ansible.**
    3. The installer for the Vagrant version which is required to run the box.
    4. **A metadata file describing the box and its requirements.**
    5. A base file system image in a format supported by the provider of the box.

    

30. Which of the following properties apply to a content delivery network? (Choose three correct answers.)

    1. CDNs require all elements of a web site to be served by the same CDN.
    2. CDNs can stream large media files such as movies or music to clients.
    3. **CDNs are present in multiple locations to serve content close to clients.**
    4. **CDNs serve huge numbers of clients with high bandwidth and low latency.**
    5. **CDNs forward all requests to a backend server and never store content locally.**
    
31. Which of the following Ansible tasks copies the file example.txt to a manage system?

    1. rsync:
       src: example.txt
       dst: /tmp/example.txt 
    2. **copy:**
       **src: example.txt**
       **dest: /tmp/example.txt**
    3. retrieve:
       src: example.txt
       dest: /tmp/example.txt
    4. cp:
       source: example.txt
       dst: /tmp/example.txt
    5. transfer:
       src: example.txt
       dest: /tmp/example.txt

    

32. Which of the following commands lists the cookbooks available on a Chef server?

    1. kitchen cookbook list
    2. chef-client cookbook list
    3. **chef-server cookbook list**
    4. chef-solo cookbook list
    5. knife cookbook list

    

33. An Ansible variable file contains the following content:
    `myapp:
    option1: one`
    Which of the following strings can be used to reference the defined variable? (Choose two correct answers).

    1. **myapp(option1);**
    2. option1@myapp
    3. myapp[‘option1’] 
    4. myapp.option1
    5. **myapp{{option1}}**

34. Which Ansible keyword is used in a playbook to store the result (i.e. return code) of a task in a variable?

    1. **register**
    2. return
    3. output
    4. result
    5. set_fact

    

35. Which of the following functions are provided by the Ansible apt module? (Choose two correct answers.)

    1. Update an installed package to the latest version.
    2. **Update the list of available packages from configured repositories.**
    3. Re-compile an installed package from the source code.
    4. Add the URL of a new repository to the package manager configuration.
    5. **Install a dpkg based Linux distribution on an empty target system.**

    

36. What statement is true regarding the Swarm service created by the following command?
    `docker service create --name myweb --network webnet --mode global nginx`

    1. It runs exactly one time in the Swarm and cannot be scaled.
    2. It runs exactly once on each node in a Swarm.
    3. **It runs on one node by default and can be scaled to an arbitrary number of replicas.**
    4. It runs on all nodes which provide the network webnet.
    5. It runs only on those nodes which support the network type global.

    

37. Which of the following mechanisms are used for service discovery in a container environment? (Choose two correct answers.)

    1. The container platform offers a command like docker service discover which should be run within a container.
    2. **The container platform sets environment variables containing service information within the containers.**
    3. The container platform lists localhost ports assigned to containers in each container’s /etc/services file.
    4. The container platform mounts the sockets for all available services into the container’s file systems.
    5. **The container platforms maintains DNS records which point to containers offering a specific service.**

    

38. Which of the statements below are true about the volume created by the following command? (Choose two correct answers.)
    `docker run –v /data –ti debian`

    1. The new /data volume contains a copy of the complete container’s base image.
    2. The volume containing the container’s rootfile system is retained until the /data volume is deleted.
    3. The /data volume is discarded when the container terminates.
    4. **The /data volume can be attached to another Docker container.**
    5. **If the command is run a second time, another volume for /data is created.**

    

39. Which of the following commands lists the nodes in a Docker Swam cluster?

    1. docker-swarm listnodes
    2. docker engine ls
    3. **docker node ls**
    4. docker machine ls
    5. docker swarm nodes

    

40. Which of the following container names could have been assigned automatically by Docker?

    1. docker-c00001
    2. 2.0.17.172
    3. container
    4. c0023817
    5. **clever_ritchie**

    

41. How is a Docker container image retrieved from a Docker registry?

    1. Docker retrieves a ZIP archive which is extracted into the container’s root file system.
    2. **Multiple stacked images are retrieved and layered on top of each other.**
    3. A flat hard disk image is downloaded once per container and mounted as the root file system.
    4. The registry merger all components of the image into one file which is shipped to Docker.
    5. The container is built from an ISO file along with a configuration for an unattended installation.

    

42. Given the following excerpt of a Dockerfile:
    `Run apt-get –y update && apt-get install –y fortunes && apt-get clean`
    Why are the multiple apt-get commands combined in one RUN statement instead of using multiple RUN statements?

    1. To prevent the commands from running in parallel because Docker executes all RUN statements in their own container at the same time.	
    2. To ensure the execution order of the commands because Docker might evaluate the statements of a Dockerfile in any order.
    3. To avoid the creation of unnecessary images because Docker creates a new image for each RUN statement.
    4. **To execute both commands in the same container instance and void Docker to reset the container to the original base image.**
    5. To execute the apt-get install command only if the apt-get update command was successful because Docker does not check the success of RUN statements.

    

43. Which of the following tasks are completed by docker-compose down when it is used with additional parameters? (Choose two correct answers.)

    1. **Delete all volumes defined in the composer file.**
    2. Delete all containers defined in the composer file.
    3. **Delete all networks defined in the composer file.**
    4. Delete all images used in the composer file from the Docker nodes.
    5. Delete all images built from the composer file from their registry.

    

44. The following command is issued on two docker nodes:
    `docker network create --driver bridge isolated_nw`
    Afterwards, one container is started at each node with the parameter `--network=isolated_nw`. It turns out that the containers can not interact with each other.
    What must be done in order to allow the containers to interact with each other? (Choose two correct answers.)

    1. Use a host network instead of a bridged network.
    2. Add the option --inter-container to the docker network create command.
    3. **Start the containers on the same node.**
    4. **Change the --network parameter of docker create to --network=isolated_nw,nofence.**
    5. Use an overlay network instead of a bridged network.

    

45. If a Dockerfile references the container’s base image without a specific version tag, which tag of that image is used to create the container?

    1. **latest**
    2. default
    3. current
    4. nightly
    5. lts
