# English Tenses 



There are three main verb tenses in English: *present, past* and *future*. The present, past and future tenses are divided into four **aspects**: the *simple, progressive, perfect* and *perfect progressive*.

There are 12 major verb tenses that English learners should know.

English has only two ways of forming a tense from the verb alone: the past and the present. For example, *we drove* and *we drive*.

To form other verb tenses, you have to add a form of *have, be* or *will* in front of the verb. These are called helping, or **auxiliary verbs**.

| VERB TENSES         | past                                                         | present                                                      | future                                                       |
| ------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Simple              | It snowed yesterday.**Simple past verb**                     | It snows every winter.**Simple present verb**                | It is going to snow tonight.It will snow this winter.**Will / be going to + simple present** |
| Progressive         | It was snowing when I drove to work.**Was/were + -ing verb** | It is snowing.**Am/is/are+-ing verb**                        | It will be snowing by the time I get home.**Will be + -ing verb** |
| Perfect             | It had already snowed before I left.**Had + past participle verb** | I have driven in snow many times.**Have/has + past participle verb** | It will have snowed 6 inches by the end of the day.**Will have + past participle verb** |
| Perfect progressive | It had been snowing for two days before it stopped.**Had been + -ing verb + for/since** | It has been snowing all month long.**Has/have + -ing verb + for/since** | It will have been snowing for three days by the time it stops.**Will have been + -ing verb + for/sinc** |

English verb tenses give many details about time and action such as:

*Is the action finished? How long did the action happen? Was the action repeated? Did the action happen at a known or unknown time? Is the action a habit? Is the action planned or **spontaneous**?*

It is difficult to think about time **distinctions** that do not exist in your own language. So, it can take many years for English learners to master verb tenses.

Let’s get started. We are going to give examples of all 12 verb tenses using the verb *drive*.

**Simple Tenses**

We’ll start with the simple tenses. These are probably the first tenses you learned in English. Simple tenses usually refer to a single action. In general, simple tenses express facts and situations that existed in the past, exist in the present, or will exist in the future.

Simple present: *I drive home every day.*

Simple past: *I drove home yesterday.*

Simple future: *I will drive home later.*

**Progressive (Continuous) Tenses**

Let’s go on to the progressive tenses. We use progressive tenses to talk about unfinished events. Progressive tenses are also called *continuous* tenses.

Past progressive: *I was driving when you called.*

Present progressive: *I am driving now.*

Future progressive: *I will be driving when you call.*

**Perfect Tenses**

Now let’s look at the perfect tenses. Perfect tenses cause the most confusion. To put it simply, they express the idea that one event happens before another event.

There are many tricky exceptions with the perfect tenses, which we will discuss in a future episode. The adverbs *never, yet* and *already* are common in perfect tenses.

Present perfect: *I have driven that road.*

Past perfect: *I had already driven that road in the past.*

Future perfect: *I will have driven 200 miles by tomorrow.*

**Perfect Progressive Tenses**

Finally, let’s look at the perfect progressive tenses. Generally, perfect progressive tenses express duration, or *how long*? Perfect progressive tenses usually include the adverbs *for* or *since*.

Present perfect progressive: *I have been driving since this morning.*

Past perfect progressive: *I had been driving for three hours before I stopped to get gas.*

Future perfect progressive: *I will have been driving for five hours by the time I arrive.*

Don’t worry if you don’t understand everything yet. Here are some recommendations we have for learning verb tenses.

**Adverbs are your friends**

First, think of adverbs as your friends. Adverbs of time offer valuable clues about the correct verb tense.

Let’s use the adverb *ago*. *Ago* is only used in the simple past as in, “I left home three years ago.” The adverb *ago* is never used in the present perfect. Certain adverbs occur with certain verb tenses.

**Keep it simple**

English learners sometimes try to impress people by using complex verb tenses. You often have a choice of several verb tenses. When you do, always choose the simplest one. It will be clearer for your listener, and there is less chance of making a mistake.



Often when someone asks a question, you can respond in the same verb tense. We’ll ask a question in each verb tense. Give an answer in the same tense, then listen to our answer.

Ready?

> \1. Did you get enough sleep last night? (simple past)
>
> Yes, I slept well.
>
> \2. Do you shower every day? (simple present)
> Yes, I shower every day.
>
> \3. Are you going to study tonight? (simple future)
> Yes, I’m going to study tonight.
>
> \4. What were you doing when I called you last night? (past progressive)
> I was eating dinner when you called me last night.
>
> \5. What are you doing right now? (present progressive)
> I am practicing verb tenses right now.
>
> \6. What will you be doing at midnight on New Year’s Eve? (future progressive)
> I will be celebrating the New Year with my friends.
>
> \7. Had you ever tried skiing before today? (past perfect)
> Yes, I had already done it several times before.
>
> \8. Have you ever broken the law? (present perfect)
> No, I have never broken the law.
>
> \9. Will you have gotten married by the time you turn 30? (future perfect)
> No, I will not have gotten married by the time I turn 30.
>
> \10. How long had you been smoking before you quit? (past perfect progressive)
> I had been smoking for two years before I quit.
>
> \11. How long have you been waiting for the bus? (present perfect progressive)
> I have been waiting for the bus for 20 minutes.
>
> \12. How long will you have been working before you retire? (future perfect progressive)
> I will have been working for 30 years before I retire.



## THE 5 MOST COMMON ENGLISH VERB TENSES

1. Simple Present %57,5
2. Simple Past %19,7
3. Simple Future %8,5
4. Present Perfect %6.0
5. Present Progressive %5.1

Some quick insights from the top 5:

- The simple present accounts for more than half of the verbs in English speech
- The 5 most common verb tenses total up to over 95% of usage
- The simple tenses are the top three verb tenses



## Simple Present

The simple present tense is one of several forms of present tense in English. It is used to describe **habits, unchanging situations, general truths, and fixed arrangements**. The simple present tense is simple to form. Just use the base form of the verb: (I take, you take, we take, they take).
The 3rd person singular takes an -s at the end. (he takes, she takes)

#### THE SIMPLE PRESENT TENSE IS USED:

- To express habits, general truths, repeated actions or unchanging situations, emotions and wishes:
  **I smoke** (habit); **I work in London** (unchanging situation); **London is a large city** (general truth)
- To give instructions or directions:
  **You walk** for two hundred meters, then **you turn** left.
- To express fixed arrangements, present or future:
  Your exam **starts** at 09.00
- To express future time, after some conjunctions: **after, when, before, as soon as, until:**
  **He'll give it to you when you come next Saturday.**

**Be careful! The simple present is not used to express actions happening now.**

#### EXAMPLES

- **For habits**
  He drinks tea at breakfast.
  She only eats fish.
  They watch television regularly.
- **For repeated actions or events**
  We catch the bus every morning.
  It rains every afternoon in the hot season.
  They drive to Monaco every summer.
- **For general truths**
  Water freezes at zero degrees.
  The Earth revolves around the Sun.
  Her mother is Peruvian.

- **For instructions or directions**
  Open the packet and pour the contents into hot water.
  You take the No.6 bus to Watney and then the No.10 to Bedford.
- **For fixed arrangements**
  His mother arrives tomorrow.
  Our holiday starts on the 26th March
- **With future constructions**
  She'll see you before she leaves.
  We'll give it to her when she arrives.

#### FORMING THE SIMPLE PRESENT TENSE: TO THINK

| Affirmative | Interrogative   | Negative           |
| :---------- | :-------------- | :----------------- |
| I think     | Do I think?     | I do not think     |
| You think   | Do you think?   | You do not think   |
| He thinks   | Does he think?  | He does not think  |
| She thinks  | Does she think? | She does not think |
| It thinks   | Does it think?  | It does not think  |
| We think    | Do we think?    | We do not think.   |
| They think  | Do they think?  | They do not think. |

#### NOTES ON THE SIMPLE PRESENT, THIRD PERSON SINGULAR

- In the third person singular the verb **always ends in -s**:
  *he want**s**, she need**s**, he give**s**, she think**s.***
- Negative and question forms use DOES (= the third person of the auxiliary 'DO') + the infinitive of the verb.
  *He want**s** ice cream. **Does** he want strawberry? He **does** not want vanilla.*
- Verbs ending in **-y** : the third person changes the **-y** to **-ies**:
  *fly --> fl**ies**, cry --> cr**ies***
  **Exception**: if there is a vowel before the -**y**:
  *play --> play**s**, pray --> pray**s***
- Add **-es** to verbs ending in:**-ss, -x, -sh, -ch**:
  *he pass**es,** she catch**es,** he fix**es,** it push**es***

##### EXAMPLES

- **He goes** to school every morning.
- **She understands** English.
- **It mixes** the sand and the water.
- **He tries** very hard.
- **She enjoys** playing the piano.



## Simple Past

#### DEFINITION OF THE SIMPLE PAST TENSE

The simple past tense, sometimes called the preterite, is used to talk about a **completed action** in a time **before now**. The simple past is the basic form of past tense in English. The time of the action can be in the recent past or the distant past and action duration is not important.

##### EXAMPLES

- John Cabot **sailed** to America in 1498.
- My father **died** last year.
- He **lived** in Fiji in 1976.
- We **crossed** the Channel yesterday.

You always use the simple past when you say **when** something happened, so it is associated with certain past time expressions

- **frequency**: *often, sometimes, always*
  I sometimes **walked** home at lunchtime.
  I often **brought** my lunch to school.
- **a definite point in time**: *last week, when I was a child, yesterday, six weeks ago*
  We **saw** a good film *last week*.
  *Yesterday*, I **arrived** in Geneva.
  She **finished** her work at *seven o'clock*
  I **went** to the theatre *last night*
- **an indefinite point in time**: *the other day, ages ago, a long time ago*
  People **lived** in caves a *long time ago*.
  She **played** the piano *when she was a child*.

**Note:** the word *ago* is a useful way of expressing the distance into the past. It is placed **after** the period of time: *a week ago, three years ago, a minute ago*.

Be Careful: The simple past in English may look like a tense in your own language, but the meaning may be different.

#### FORMING THE SIMPLE PAST TENSE

###### PATTERNS OF SIMPLE PAST TENSE FOR REGULAR VERBS

| **Affirmative**            |             |                           |
| -------------------------- | ----------- | ------------------------- |
| Subject                    | + verb + ed |                           |
| I                          | skipped.    |                           |
| **Negative**               |             |                           |
| Subject                    | + did not   | + infinitive without *to* |
| They                       | didn't      | go.                       |
| **Interrogative**          |             |                           |
| Did                        | + subject   | + infinitive without *to* |
| Did                        | she         | arrive?                   |
| **Interrogative negative** |             |                           |
| Did not                    | + subject   | + infinitive without *to* |
| Didn't                     | you         | play?                     |

###### TO WALK

| **Affirmative** | **Negative**     | **Interrogative** |
| :-------------- | :--------------- | :---------------- |
| I walked        | I didn't walk    | Did I walk?       |
| You walked      | You didn't walk  | Did you walk?     |
| He walked       | He didn't walk   | Did he walk?      |
| We walked       | We didn't walk   | Did we walk?      |
| They walked     | They didn't walk | Did they walk?    |

###### SIMPLE PAST TENSE OF TO BE, TO HAVE, TO DO

| Subject       | Verb   |          |        |
| :------------ | :----- | -------- | ------ |
|               | **Be** | **Have** | **Do** |
| **I**         | was    | had      | did    |
| **You**       | were   | had      | did    |
| **He/She/It** | was    | had      | did    |
| **We**        | were   | had      | did    |
| **You**       | were   | had      | did    |
| **They**      | were   | had      | did    |

#### NOTES ON AFFIRMATIVE, NEGATIVE, & INTERROGATIVE FORMS

###### AFFIRMATIVE

The affirmative of the simple past tense is simple.

- I **was** in Japan last year
- She **had** a headache yesterday.
- We **did** our homework last night.

###### NEGATIVE AND INTERROGATIVE

For the negative and interrogative simple past form of *"to do"* as an ordinary verb, use the auxiliary *"did",* e.g. We **didn't do** our homework last night.
The negative of *"have"* in the simple past is usually formed using the auxiliary *"did"*, but sometimes by simply adding *not* or the contraction *"n't"*.

The interrogative form of *"have"* in the simple past normally uses the auxiliary *"did".*

##### EXAMPLES

- They **weren't** in Rio last summer.
- We **didn't have** any money.
- We **didn't have** time to visit the Eiffel Tower.
- We **didn't do** our exercises this morning.
- **Were** they in Iceland last January?
- **Did you have** a bicycle when you were young?
- **Did you do** much climbing in Switzerland?

**Note:** For the negative and interrogative form of **all** verbs in the simple past, always use the auxiliary **'did''.**

#### SIMPLE PAST, IRREGULAR VERBS

Some verbs are irregular in the simple past. Here are the most common ones.

###### TO GO

- He **went** to a club last night.
- **Did he go** to the cinema last night?
- He **didn't go** to bed early last night.

###### TO GIVE

- We **gave** her a doll for her birthday.
- They **didn't give** John their new address.
- **Did Barry give** you my passport?

###### TO COME

- My parents **came** to visit me last July.
- We **didn't come** because it was raining.
- **Did he come** to your party last week?

https://www.ef.com/wwen/english-resources/english-grammar/simple-past-tense/

https://www.englishpage.com/verbpage/simplepast.html



## Simple Future

#### FUNCTIONS OF THE SIMPLE FUTURE TENSE

The simple future refers to a time later than now, and expresses facts or certainty. In this case there is no 'attitude'.

**The simple future is used:**

- To predict a future event:
  It **will rain** tomorrow.
- With I or We, to express a spontaneous decision:
  **I'll pay** for the tickets by credit card.
- To express willingness:
  **I'll do** the washing-up.
  **He'll carry** your bag for you.
- In the negative form, to express unwillingness:
  The baby **won't eat** his soup.
  I **won't leave** until I've seen the manager!
- With I in the interrogative form using "shall", to make an offer:
  **Shall I open** the window?
- With we in the interrogative form using "shall", to make a suggestion:
  **Shall we go** to the cinema tonight?
- With I in the interrogative form using "shall", to ask for advice or instructions:
  What **shall I tell** the boss about this money?
- With you, to give orders:
  You **will do** exactly as I say.
- With you in the interrogative form, to give an invitation:
  **Will you come** to the dance with me?
  **Will you marry** me?

**Note:**In modern English **will** is preferred to **shall**. Shall is mainly used with **I** and **we** to make an offer or suggestion, or to ask for advice (see examples above). With the other persons (you, he, she, they) shall is only used in literary or poetic situations, e.g. *"With rings on her fingers and bells on her toes, She **shall have** music wherever she goes."*

#### FORMING THE SIMPLE FUTURE

The simple future tense is composed of two parts: *will / shall* + the infinitive without *to*

| Subject                    | will     | infinitive without to |
| :------------------------- | :------- | :-------------------- |
| **Affirmative**            |          |                       |
| I                          | will     | go                    |
| I                          | shall    | go                    |
| **Negative**               |          |                       |
| They                       | will not | see                   |
| They                       | won't    | see                   |
| **Interrogative**          |          |                       |
| Will                       | she      | ask?                  |
| **Interrogative negative** |          |                       |
| Won't                      | they     | try?                  |

###### CONTRACTIONS

I will = I'll
We will = we'll
You will = you'll
He will = he'll
She will = she'll
They will = they'll
Will not = won't

The form "it will" is not normally shortened.

#### TO SEE: SIMPLE FUTURE TENSE

| Affirmative       | Negative       | Interrogative  | Interrogative Negative |
| :---------------- | :------------- | :------------- | :--------------------- |
| **I** will see    | I won't see    | Will I see?    | Won't I see?           |
| ***I** shall see  |                | *Shall I see?  |                        |
| **You** will see  | You won't see  | Will you see?  | Won't you see?         |
| **He** will see   | He won't see   | Will he see?   | Won't he see?          |
| **We** will see   | We won't see   | Will we see?   | Won't we see?          |
| ***We** shall see |                | *Shall we see? |                        |
| **They** will see | They won't see | Will they see? | Won't they see?        |

***Shall** is dated, but it is still commonly used instead of "will" with the affirmative or interrogative forms of **I** and **we** in certain cases (see above).



## Present Perfect

#### DEFINITION OF THE PRESENT PERFECT TENSE

The present perfect is used to indicate a link between the present and the past. The time of the action is **before now but not specified**, and we are often more interested in the **result** than in the action itself.

**BE CAREFUL!** There may be a verb tense in your language with a similar form, but the meaning is probably NOT the same.

###### THE PRESENT PERFECT IS USED TO DESCRIBE

- An action or situation that started in the past and continues in the present. *I **have lived** in Bristol since 1984* (= and I still do.)
- An action performed during a period that has not yet finished. *She **has been** to the cinema twice this week* (= and the week isn't over yet.)
- A repeated action in an unspecified period between the past and now. *We **have visited** Portugal several times.*
- An action that was completed in the very recent past, expressed by 'just'. *I **have just finished** my work.*
- An action when the time is not important. *He **has read** 'War and Peace'.* (= the result of his reading is important)

**Note:** When we want to give or ask details about when, where, who, we use the simple past. Read more about [choosing between the present perfect and the simple past tenses](https://www.ef.com/wwen/english-resources/english-grammar/present-perfect-vs-simple-past/).

###### ACTIONS STARTED IN THE PAST AND CONTINUING IN THE PRESENT

- They ***haven't lived*** here for years.
- She ***has worked*** in the bank for five years.
- We ***have had*** the same car for ten years.
- ***Have you played*** the piano since you were a child?

###### WHEN THE TIME PERIOD REFERRED TO HAS NOT FINISHED

- ***I have worked*** hard ***this week***.
- It ***has rained*** a lot ***this year***.
- We ***haven't seen*** her ***today***.

###### ACTIONS REPEATED IN AN UNSPECIFIED PERIOD BETWEEN THE PAST AND NOW.

- They ***have seen*** that film six times
- It ***has happened*** several times already.
- She ***has visited*** them frequently.
- We ***have eaten*** at that restaurant many times.

###### ACTIONS COMPLETED IN THE VERY RECENT PAST (+JUST)

- ***Have you just finished*** work?
- I ***have just eaten***.
- We ***have just seen*** her.
- ***Has he just left***?

###### WHEN THE PRECISE TIME OF THE ACTION IS NOT IMPORTANT OR NOT KNOWN

- Someone ***has eaten** my soup*!
- ***Have you seen*** 'Gone with the Wind'?
- ***She's studied*** Japanese, Russian, and English.

Read more about [using the present perfect with the words "ever", "never", "already", and "yet"](https://www.ef.com/english-resources/english-grammar/present-perfect-ever-never-already-yet/), and about [using the present perfect with the words "for" and "since"](https://www.ef.com/english-resources/english-grammar/present-perfect-and/).

#### FORMING THE PRESENT PERFECT

The present perfect of any verb is composed of two elements : the appropriate form of the auxiliary verb **to have** (present tense), plus the past participle of the main verb. The past participle of a regular verb is **base+ed**, e.g. *played, arrived, looked*. For irregular verbs, see the **Table of irregular verbs** in the section called **'Verbs'**.

| **Affirmative**            |                   |                     |
| -------------------------- | ----------------- | ------------------- |
| **Subject**                | **to have**       | **past participle** |
| She                        | has               | visited.            |
| **Negative**               |                   |                     |
| **Subject**                | **to have + not** | **past participle** |
| She                        | has not (hasn't)  | visited.            |
| **Interrogative**          |                   |                     |
| **to have**                | **subject**       | **past participle** |
| Has                        | she               | visited?            |
| **Negative interrogative** |                   |                     |
| **to have + not**          | **subject**       | **past participle** |
| Hasn't                     | she               | visited?            |

##### TO WALK, PRESENT PERFECT

|                            |                        |                         |
| :------------------------- | :--------------------- | :---------------------- |
| Affirmative                | Negative               | Interrogative           |
| **I** have walked          | I haven't walked       | Have I walked?          |
| **You** have walked        | You haven't walked.    | Have you walked?        |
| **He, she, it** has walked | He, she, hasn't walked | Has he, she, it walked? |
| **We** have walked         | We haven't walked      | Have we walked?         |
| **You** have walked        | You haven't walked     | Have you walked?        |
| **They** have walked       | They haven't walked    | Have they walked?       |



## Present Continuous / Progressive 

#### FORMING THE PRESENT CONTINUOUS

The present continuous of any verb is composed of two parts - *the present tense of the verb to be + the present participle of the main verb.*

(The form of the present participle is: *base+ing, e.g. talking, playing, moving, smiling)*

| **Affirmative**   |                   |                  |
| ----------------- | ----------------- | ---------------- |
| Subject           | + **to be**       | **+ base + ing** |
| She               | is                | talking.         |
| **Negative**      |                   |                  |
| Subject           | **+ to be + not** | **+ base + ing** |
| She               | is not (isn't)    | talking          |
| **Interrogative** |                   |                  |
| **to be**         | **+ subject**     | **+ base + ing** |
| Is                | she               | talking?         |

#### EXAMPLES: TO GO, PRESENT CONTINUOUS

|                          |                         |                       |
| :----------------------- | :---------------------- | :-------------------- |
| Affirmative              | Negative                | Interrogative         |
| **I** am going           | I am not going          | Am I going?           |
| **You** are going        | You aren't going.       | Are you going?        |
| **He, she, it** is going | He, she, it isn't going | Is he, she, it going? |
| **We** are going         | We aren't going         | Are we going?         |
| **You** are going        | You aren't going        | Are you going?        |
| **They** are going       | They aren't going       | Are they going?       |

**Note**: alternative negative contractions: *I'm not going, you're not going, he's not going etc.*

#### FUNCTIONS OF THE PRESENT CONTINUOUS

As with all tenses in English, the **speaker's attitude** is as important as the time of the action or event. When someone uses the present continuous, they are thinking about something that is ***unfinished or incomplete***

##### THE PRESENT CONTINUOUS IS USED:

- to describe an action that is going on at this moment: ***You are using** the Internet*. ***You are studying** English grammar.*
- to describe an action that is going on during this period of time or a trend: ***Are you still working** for the same company? More and more people **are becoming** vegetarian.*
- to describe an action or event in the future, which has already been planned or prepared: ***We're going** on holiday tomorrow*. ***I'm meeting** my boyfriend tonight*. ***Are they visiting*** you next winter?
- to describe a temporary event or situation: *He usually plays the drums, but **he's playing** bass guitar tonight*. *The weather forecast was good, but **it's raining** at the moment.*
- with "always, forever, constantly", to describe and emphasise a continuing series of repeated actions: *Harry and Sally **are always arguing**! **You're constantly complaining** about your mother-in-law!*

**BE CAREFUL!** Some verbs are not usually used in the continuous form

#### VERBS THAT ARE NOT USUALLY USED IN THE CONTINUOUS FORM

The verbs in the list below are normally used in the simple form because they refer to **states**, rather than actions or processes.

###### SENSES / PERCEPTION

- to feel*
- to hear
- to see*
- to smell
- to taste

###### OPINION

- to assume
- to believe
- to consider
- to doubt
- to feel (= to think)
- to find (= to consider)
- to suppose
- to think*

###### MENTAL STATES

- to forget
- to imagine
- to know
- to mean
- to notice
- to recognise
- to remember
- to understand

###### EMOTIONS / DESIRES

- to envy
- to fear
- to dislike
- to hate
- to hope
- to like
- to love
- to mind
- to prefer
- to regret
- to want
- to wish

###### MEASUREMENT

- to contain
- to cost
- to hold
- to measure
- to weigh

###### OTHERS

- to look (=resemble)
- to seem
- to be *(in most cases)*
- to have *(when it means "to possess")**

##### EXCEPTIONS

Perception verbs (see, hear, feel, taste, smell) are often used with *can: I can see...* These verbs may be used in the continuous form but with a different meaning

- *This coat **feels** nice and warm.* (your perception of the coat's qualities)
- ***John's feeling** much better now* (his health is improving)
- *She **has** three dogs and a cat.* (possession)
- ***She's having** supper.* (She's eating)
- *I can **see** Anthony in the garden* (perception)
- ***I'm seeing** Anthony later* (We are planning to meet)



_________________



IN limited region of space, limited bordes. ARGENTINA tiene fronteras. Limited scoop

ON is for surface (superficies), or places, the most general. Any sources of surfaces, they have dont limitated.
AT the smalls.

*TIME IN AT ON*

General thinks that have been limited in time, like a month, a year, **limited scoped.** WE USE **IN**

**A**T : Very specifiet, we use, the hour, at 2:00.  PIN POINT PLACES AND PIN POINT TIMES

ON: The biggest date, the month, the day,



_-----------------_

Past continuos and simple past.

The boy ____ cake when his mother
came into the room.
a was eat
b eats
c was eating
d has eating



It's sound -> IS OK! 
it's sound me -> is BAD! 



DIVorced SE PRONUNCIA DIVORST

married -marriTD

E FINALES EN INGLES NO SUENAN.

16 BAD to go

Would you like SIEMPRE SEGUIDO DE *TO*

17 BAD i am flying (Present continuos)

18 i'll PRONUNCIACION AIL Ambrella

's raining.





_____



