# Tenses: Present Simple



#### Exercises

* https://www.perfect-english-grammar.com/present-simple-exercise-7.html
* http://www.focus.olsztyn.pl/en-exercise-on-present-simple.html
* http://www.focus.olsztyn.pl/en-simple-present-exercises.html



Exercises PDF answers:

Open the brackets in Present Simple:

1. ask
2. listens
3. drink
4. help
5. understands
6. start
7. are
8. drinks
9. lives
   

Rewrite the sentences, starting with SHE:

1. She speaks Spanish
2. She teaches Maths at school
3. She travels by car every day
4. She goes to work by the underground
   

Make questions and give shorts answers:

1. Does he smoke? Yes, he does
2. Do they live in a house? No, they don't
3. Does your brother visit you often? Yes, he does
4. Do you drive to work? Yes, i do
5. Does he drive a Ford? Yes, he does
6. Do you meet often? No, we don't
7. Does she live with her parents? Yes, she does
8. Does he watch TV a lot? No, he doesn't
9. Do they speak to each other? Yes, they do



Make the sentences negative:

1. I don't speak German
2. She doesn't travel by train
3. He doesn't wash the dishes every afternoon
4. I don't like to talk to you
5. We don't live in London
6. It doesn't rain every day
7. We don't like listening to the radio
8. He doesn't speak 4 foreign languages
9. She isn't a good programmer



Open the brackets in Present Simple:

1. Do you work here?
2. I don't like travelling by car
3. The TV programme starts at 5
4. The office opens at 7 a.m
5. Does she like watermelon?
6. Do your sister know the truth?
7. We travel a lot
8. The course ends at the end of june

