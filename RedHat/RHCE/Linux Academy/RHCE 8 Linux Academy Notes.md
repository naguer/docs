# RHCE 8 Linux Academy Notes

### Understand Core Components of Ansible

#### Inventories

Inventories are what Ansible uses to locate and run against multiple Hosts.

Default location /etc/ansible/hosts, can be modified in /etc/ansible/ansible.cfg, you can pass specified file using the `-i`. The file contain individual hosts, groups of hosts, groups of groups, and host and group level variables. We can write the file in two types, INI-based inventory file or Yaml-based

```
webservers:
  hosts:
    alpha.example.org:
    beta.example.org:
      ansible_host:               192.168.200.122
    192.168.1.100:
    192.168.1.110:
    www[001:006].example.com:
```

#### Modules

Modules are essentially tools for particular tasks. Modules can take, and usually do take, parameters. 
Modules return JSON. Run modules from the command line or within a playbook. Ansible ships with a significant amount of modules by default. Custom modules can be written.

#### Variables

Variables names should only contain letters, numbers and underscores. Variables should always start with a letter. There are three main scopes for variables; Global, Hosts, Play.
They are typically used for configuration values and various parameters. Variables can store the return value of executed commands. Variables may also be dictionaries. Ansible provides a number of predefined variables. 

```
- hosts: all
  vars:
    hello: world
  tasks:
  - name: Ansible Basic Variable Example
    debug:
      msg: "{{ hello }}"
```

#### Facts

Facts provide certain information about a given target host. Facts are automatically discovered by Ansible whet it reaches out to a hosts. Facts can be disabled. Facts can be cached for use in playbook executions

```
aksarav@middlewareinventory:~$ ansible app -m setup -i ansible_hosts 
mwiapp01 | SUCCESS => {
"ansible_facts": {
"ansible_all_ipv4_addresses": [
"10.0.2.15", 
"192.168.60.4"
], 
"ansible_architecture": "x86_64", 
"ansible_bios_version": "VirtualBox", 
}, 
"ansible_date_time": {
"date": "2018-09-16", 
```

#### Plays and Playbooks

The goal of a play is to map a group of hosts to some well-defined roles. A play can consist of one or more tasks which make calls to Ansible modules. A playbook is a series of plays.

```
  - name: Playbook
    hosts: webservers
    become: yes
    become_user: root
    tasks:
      - name: ensure apache is at the latest version
        yum:
          name: httpd
          state: latest
      - name: ensure apache is running
        service:
          name: httpd
          state: started
```

#### Configuration Files

Possible locations of Ansible configs files (in order)

	* ANSIBLE_CONFIG (env variable)
	* ansible.cfg (in the currect directory)
	* ~/.ansible.cfg (in the home directory)
	* /etc/ansible/ansible.cfg

A configuration file will not automatically load if it is in a world-writable directory. 

Common Ansible configuration:

* `ansible-config` to view configuration
  * `list` print all configuration options
  * `dump` dumps configuration
  * `view` view the file
* Commonly used settings:
  * `inventory` specifies the default inventory file
  * `roles_path` sets paths to search in for roles
  * `forks` specifies the amount of hosts configured by Ansible at the same time 
  * `ansible_managed` text inserted into templates which indicate that file is managed by Ansible and changes will be overwritten



### Install and Configure an Ansible Control Node

#### Install Required Packages

```
git clone git://github.com/ansible/ansible.git --single-branch --branch stable-2.8
cd ansible
source ./hacking/env-setup
pip2.7 install --user -r ./requirements.txt
ansible 127.0.0.1 -m ping
```

#### Create a Static Host Inventory File

An inventory is a list of groups and variables. Multiple inventory files may be specified using a directory. Inventory files may be specified in INI or YAML format.
Best practices:

* Variables should be stored in YAML files located relative to the inventory file.
* Host and group variables should be stored in the host_vars and group_vars directories respectively (directories must be created).
* Variable files should be named after the host or group for which they contain variables (files may end in .yml or .yaml)

```
webservers:
  hosts:
    alpha.example.org
    	ansible_port: 5556
  children:
  	webservers:
  		hosts:
    		192.168.1.110
    	vars:
    		httpd_port: 8080
```

#### Create a Configuration File

/etc/ansible/ansible.cfg

```
[defaults]
# some basic default values...
Si y#inventory      = /etc/ansible/hosts
#library        = /usr/share/my_modules/
#module_utils   = /usr/share/my_module_utils/
#remote_tmp     = ~/.ansible/tmp
#local_tmp      = ~/.ansible/tmp
#plugin_filters_cfg = /etc/ansible/plugin_filters.yml
#forks          = 5
....
```



### Configure Ansible Managed Nodes







### Script Administration Tasks







### Create Ansible Plays and Playbooks







### Use Ansible Modules for System Administration Tasks







### Create and Use Templates to Create Customized Configuration Files





### Create and Work with Roles





### Managing Parallelism





### Protect Sensitive Data in Playbooks with Ansible Vault





### Ansible Documentation