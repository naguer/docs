# Linux Automation with Ansible



## Introduction

### Red Hat System Administration III: Linux Automation with Ansible

*Red Hat System Administration III: Linux Automation with Ansible* (RH294) is intended for Linux system administrators and developers who need to automate provisioning, configuration, application deployment, and orchestration.

Students will learn how to install and configure Ansible on a management workstation and prepare managed hosts for automation. Students will write Ansible Playbooks to automate tasks, and run them to ensure servers are correctly deployed and configured. Examples of approaches to automate common Linux system administration tasks will be explored.



### Course Objectives

- Install and configure Ansible or Red Hat Ansible Engine on a control node.
- Create and manage inventories of managed hosts, and prepare them for Ansible automation.
- Run individual ad hoc automation tasks from the command line.
- Write Ansible Playbooks to consistently automate multiple tasks and apply them to managed hosts.
- Parameterize playbooks using variables and facts, and protect sensitive data with Ansible Vault.
- Write and reuse existing Ansible roles to simplify playbook creation and reuse code.
- Automate common Red Hat Enterprise Linux system administration tasks using Ansible.



### Audience

- Linux system administrators, DevOps engineers, infrastructure automation engineers, and systems design engineers responsible for automation of configuration management, consistent and repeatable application deployment, provisioning and deployment of development, testing, and production servers, and integration with DevOps CI/CD workflows.



### Prerequisites

- Red Hat Certified System Administrator (EX200/RHCSA) certification or equivalent Red Hat Enterprise Linux knowledge and experience.



## Chapter 1. Introducing Ansible

- [Automating Linux Administration with Ansible](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch01)
- [Quiz: Automating Linux Administration with Ansible](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch01s02)
- [Installing Ansible](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch01s03)
- [Guided Exercise: Installing Ansible](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch01s04)
- [Summary](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch01s05)

**Abstract**



| **Goal**       | Describe the fundamental Ansible concepts and how it is used, and install Red Hat Ansible Engine. |
| -------------- | ------------------------------------------------------------ |
| **Objectives** | Describe the motivation for automating Linux administration tasks with Ansible, fundamental Ansible concepts, and Ansible’s basic architecture. Install Ansible on a control node and describe the distinction between community Ansible and Red Hat Ansible Engine. |
| **Sections**   | Automating Linux Administration with Ansible (and Quiz)Installing Ansible (and Guided Exercise) |



### Automating Linux Administration with Ansible

#### Objective

After completing this section, you should be able to describe the motivation for automating Linux administration tasks with Ansible, fundamental Ansible concepts, and Ansible's basic architecture.

#### Automation and Linux System Administration

For many years, most system administration and infrastructure management has relied on manual tasks performed through graphical or command-line user interfaces. System administrators often work from checklists, other documentation, or a memorized routine to perform standard tasks.

This approach is error-prone. It is easy for a system administrator to skip a step or perform a step mistakenly. Often there is limited verification that the steps were performed properly or that they result in the expected outcome.

Furthermore, by managing each server manually and independently, it is very easy for many servers that are supposed to be identical in configuration to be different in minor (or major) ways. This can make maintenance more difficult and introduce errors or instability into the IT environment.

*Automation* can help avoid the problems caused by manual system administration and infrastructure management. As a system administrator, you can use it to ensure that all your systems are quickly and correctly deployed and configured. This allows you to automate the repetitive tasks in your daily schedule, freeing up your time and allowing you to focus on more critical things. For your organization, this means you can more quickly roll out the next version of an application or updates to a service.

**Infrastructure as Code**

A good automation system allows you to implement *Infrastructure as Code* practices. Infrastructure as Code means that you can use a machine-readable automation language to define and describe the state you want your IT infrastructure to be in. Ideally, this automation language should also be very easy for humans to read, because then you can easily understand what the state is and make changes to it. This code is then applied to your infrastructure to ensure that it is actually in that state.

If the automation language is represented as simple text files, it can easily be managed in a version control system like software code. The advantage of this is that every change can be checked into the version control system, so you have a history of the changes you make over time. If you want to revert to an earlier known-good configuration, you simply can check out that version of the code and apply it to your infrastructure.

This builds a foundation to help you follow best practices in DevOps. Developers can define their desired configuration in the automation language. Operators can review those changes more easily to provide feedback, and use that automation to reproducibly ensure that systems are in the state expected by the developers.

**Mitigating Human Error**

By reducing the tasks performed manually on servers using automation of tasks and Infrastructure as Code practices, your servers will be in consistent configurations more often. This means that you need to become accustomed to making changes through updating your automation code, rather than manually applying them to your servers. Otherwise, you run the risk of losing manually-applied changes the next time you apply changes through your automation.

Automation allows you to use code review, peer review by multiple subject matter experts, and documentation of the procedure by the automation itself to reduce your operational risks.

Ultimately, you can enforce that changes to your IT infrastructure must be made through automation in order to mitigate human error.

#### What is Ansible?

*Ansible* is an open source automation platform. It is a *simple automation language* that can perfectly describe an IT application infrastructure in *Ansible Playbooks* . It is also an *automation engine* that runs Ansible Playbooks.

Ansible can manage powerful automation tasks and can adapt to many different workflows and environments. At the same time, new users of Ansible can very quickly use it to become productive.

**Ansible Is Simple**

Ansible Playbooks provide human-readable automation. This means that playbooks are automation tools that are also easy for humans to read, comprehend, and change. No special coding skills are required to write them. Playbooks execute tasks in order. The simplicity of playbook design makes them usable by every team, which allows people new to Ansible to get productive quickly.

**Ansible Is Powerful**

You can use Ansible to deploy applications, for configuration management, for workflow automation, and for network automation. Ansible can be used to orchestrate the entire application life cycle.

**Ansible Is Agentless**

Ansible is built around an *agentless architecture* . Typically, Ansible connects to the hosts it manages using OpenSSH or WinRM and runs tasks, often (but not always) by pushing out small programs called *Ansible modules* to those hosts. These programs are used to put the system in a specific desired state. Any modules that are pushed are removed when Ansible is finished with its tasks. You can start using Ansible almost immediately because no special agents need to be approved for use and then deployed to the managed hosts. Because there are no agents and no additional custom security infrastructure, Ansible is more efficient and more secure than other alternatives.

Ansible has a number of important strengths:

- *Cross platform support* : Ansible provides agentless support for Linux, Windows, UNIX, and network devices, in physical, virtual, cloud, and container environments.
- *Human-readable automation* : Ansible Playbooks, written as YAML text files, are easy to read and help ensure that everyone understands what they will do.
- *Perfect description of applications* : Every change can be made by Ansible Playbooks, and every aspect of your application environment can be described and documented.
- *Easy to manage in version control* : Ansible Playbooks and projects are plain text. They can be treated like source code and placed in your existing version control system.
- *Support for dynamic inventories* : The list of machines that Ansible manages can be dynamically updated from external sources in order to capture the correct, current list of all managed servers all the time, regardless of infrastructure or location.
- *Orchestration that integrates easily with other systems* : HP SA, Puppet, Jenkins, Red Hat Satellite, and other systems that exist in your environment can be leveraged and integrated into your Ansible workflow.

#### Ansible: The Language of DevOps





Figure 1.1: Ansible across the application life cycle

Communication is the key to DevOps. Ansible is the first automation language that can be read and written across IT. It is also the only automation engine that can automate the application life cycle and continuous delivery pipeline from start to finish.

#### Ansible Concepts and Architecture

There are two types of machines in the Ansible architecture: *control nodes* and *managed hosts* . Ansible is installed and run from a control node, and this machine also has copies of your Ansible project files. A control node could be an administrator's laptop, a system shared by a number of administrators, or a server running Red Hat Ansible Tower.

Managed hosts are listed in an *inventory* , which also organizes those systems into groups for easier collective management. The inventory can be defined in a static text file, or dynamically determined by scripts that get information from external sources.

Instead of writing complex scripts, Ansible users create high-level *plays* to ensure a host or group of hosts are in a particular state. A play performs a series of *tasks* on the hosts, in the order specified by the play. These plays are expressed in YAML format in a text file. A file that contains one or more plays is called a *playbook* .

Each task runs a *module* , a small piece of code (written in Python, PowerShell, or some other language), with specific arguments. Each module is essentially a tool in your toolkit. Ansible ships with hundreds of useful modules that can perform a wide variety of automation tasks. They can act on system files, install software, or make API calls.

When used in a task, a module generally ensures that some particular aspect of the machine is in a particular state. For example, a task using one module may ensure that a file exists and has particular permissions and contents, while a task using a different module may make certain that a particular file system is mounted. If the system is not in that state, the task should put it in that state. If the system is already in that state, it does nothing. If a task fails, Ansible's default behavior is to abort the rest of the playbook for the hosts that had a failure.

Tasks, plays, and playbooks are designed to be *idempotent* . This means that you can safely run a playbook on the same hosts multiple times. When your systems are in the correct state, the playbook makes no changes when you run it. This means that you should be able to run a playbook on the same hosts multiple times safely. When your systems are in the correct state the playbook should make no changes when you run it. There are a handful of modules that you can use to run arbitrary commands. However, you must use those modules with care to ensure that they run in an idempotent way.

Ansible also uses *plug-ins* . Plug-ins are code that you can add to Ansible to extend it and adapt it to new uses and platforms.

The Ansible architecture is agentless. Typically, when an administrator runs an Ansible Playbook or an ad hoc command, the control node connects to the managed host using SSH (by default) or WinRM. This means that clients do not need to have an Ansible-specific agent installed on managed hosts, and do not need to permit special network traffic to some nonstandard port.

*Red Hat Ansible Tower* is an enterprise framework to help you control, secure, and manage your Ansible automation at scale. You can use it to control who has access to run playbooks on which hosts, share the use of SSH credentials without allowing users to transfer or see their contents, log all of your Ansible jobs, and manage inventory, among many other things. It provides a web-based user interface (web UI) and a RESTful API. It is not a core part of Ansible, but a separate product that helps you use Ansible more effectively with a team or at a large scale.





Figure 1.2: Ansible architecture

#### The Ansible Way

**Complexity Kills Productivity**

Simpler is better. Ansible is designed so that its tools are simple to use and automation is simple to write and read. You should take advantage of this to strive for simplification in how you create your automation.

**Optimize For Readability**

The Ansible automation language is built around simple, declarative, text-based files that are easy for humans to read. Written properly, Ansible Playbooks can clearly document your workflow automation.

**Think Declaratively**

Ansible is a *desired-state engine* . It approaches the problem of how to automate IT deployments by expressing them in terms of the state that you want your systems to be in. Ansible's goal is to put your systems into the desired state, only making changes that are necessary. Trying to treat Ansible like a scripting language is not the right approach.





Figure 1.3: Ansible provides complete automation

#### Use Cases

Unlike some other tools, Ansible combines and unites orchestration with configuration management, provisioning, and application deployment in one easy-to-use platform.

Some use cases for Ansible include:

- Configuration Management

  Centralizing configuration file management and deployment is a common use case for Ansible, and it is how many power users are first introduced to the Ansible automation platform.

- Application Deployment

  When you define your application with Ansible, and manage the deployment with Red Hat Ansible Tower, teams can effectively manage the entire application life cycle from development to production.

- Provisioning

  Applications have to be deployed or installed on systems. Ansible and Red Hat Ansible Tower can help streamline the process of provisioning systems, whether you are PXE booting and kickstarting bare-metal servers or virtual machines, or creating virtual machines or cloud instances from templates. Applications have to be deployed or installed on systems.

- Continuous Delivery

  Creating a CI/CD pipeline requires coordination and buy-in from numerous teams. You cannot do it without a simple automation platform that everyone in your organization can use. Ansible Playbooks keep your applications properly deployed (and managed) throughout their entire life cycle.

- Security and Compliance

  When your security policy is defined in Ansible Playbooks, scanning and remediation of site-wide security policies can be integrated into other automated processes. Instead of being an afterthought, it is an integral part of everything that is deployed.

- Orchestration

  Configurations alone do not define your environment. You need to define how multiple configurations interact, and ensure the disparate pieces can be managed as a whole.

**REFERENCES**

[Ansible](https://www.ansible.com/)

[How Ansible Works](https://www.ansible.com/how-ansible-works)



------

###  Installing Ansible

#### Objectives

After completing this section, you should be able to install Ansible on a control node and describe the distinction between community Ansible and Red Hat Ansible Engine.

#### Ansible or Red Hat Ansible Automation?

Red Hat provides Ansible software in special channels as a convenience to Red Hat Enterprise Linux subscribers, and you can use these software packages normally.

However, if you want formal support for Ansible and its modules, Red Hat offers a special subscription for this, Red Hat Ansible Automation. This subscription includes support for Ansible itself, as Red Hat Ansible Engine. This adds formal technical support with SLAs and a published scope of coverage for Ansible and its core modules. More information on the scope of this support is available at[ *Red Hat Ansible Engine Life Cycle* ](https://access.redhat.com/support/policy/updates/ansible-engine).

#### Control Nodes

Ansible is simple to install. The Ansible software only needs to be installed on the control node (or nodes) from which Ansible will be run. Hosts that are managed by Ansible do not need to have Ansible installed. This installation involves relatively few steps and has minimal requirements.

The control node should be a Linux or UNIX system. Microsoft Windows is not supported as a control node, although Windows systems can be managed hosts.

Python 3 (version 3.5 or later) or Python 2 (version 2.7 or later) needs to be installed on the control node.

**IMPORTANT**

If you are running Red Hat Enterprise Linux 8, Ansible 2.8 can automatically use the platform-python package that supports system utilities that use Python. You do not need to install the python36 or python27 package from AppStream.

```
[root@controlnode ~]#yum list installed platform-python
Loaded plugins: langpacks, search-disabled-repos
Installed Packages
platform-python.x86_64      3.6.8-2.el8_0     @anaconda
```

Information on how to install the ansible software package on a Red Hat Enterprise Linux system is available in the Knowledgebase article[ *How Do I Download and Install Red Hat Ansible Engine?* ](https://access.redhat.com/articles/3174981).

Ansible is under rapid upstream development, and therefore Red Hat Ansible Engine has a rapid life cycle. More information on the current life cycle is available at[ https://access.redhat.com/support/policy/updates/ansible-engine ](https://access.redhat.com/support/policy/updates/ansible-engine).

Red Hat provides the`ansible-2.8-for-rhel-8-x86_64-rpms`channel for Red Hat Enterprise Linux 8. You can also get the latest update of the Red Hat Ansible Engine 2 major release for RHEL 8 in the`ansible-2-for-rhel-8-x86_64-rpms`channel.

You can use these channels to install Ansible with limited support with standard Red Hat Enterprise Linux subscriptions. If you need more comprehensive Ansible support, you can purchase full Red Hat Ansible Engine subscriptions and associate them with your systems before enabling the channels, as discussed in the Knowledgebase article.

If you have a Red Hat Ansible Engine subscription, the installation procedure for Red Hat Ansible Engine 2 is as follows:

**WARNING**

You do not need to run these steps in your classroom environment.

1. Register your system to Red Hat Subscription Manager.

   ```
   [root@host ~]# subscription-manager register
   ```

2. Set a role for your system.

   ```
   [root@host ~]# subscription-manager role --set="Red Hat Enterprise Linux Server"
   ```

3. Attach your Red Hat Ansible Engine subscription. This command helps you find your Red Hat Ansible Engine subscription:

   ```
   [root@host ~]# subscription-manager list --available
   ```

4. Use the pool ID of the subscription to attach the pool to the system.

   ```
   [root@host ~]# subscription-manager attach --pool=<engine-subscription-pool>
   ```

5. Enable the Red Hat Ansible Engine repository.

   ```
   [root@host ~]# subscription-manager repos \
   > --enable ansible-2-for-rhel-8-x86_64-rpms
   ```

6. Install Red Hat Ansible Engine.

   ```
   [root@host ~]# yum install ansible
   ```

If you are using the version with limited support provided with your Red Hat Enterprise Linux subscription, use the following procedure:

1. Enable the Red Hat Ansible Engine repository.

   ```
   [root@host ~]# subscription-manager refresh
   [root@host ~]# subscription-manager repos \
   > --enable ansible-2-for-rhel-8-x86_64-rpms
   ```

2. Install Red Hat Ansible Engine.

   ```
   [root@host ~]# yum install ansible
   ```

#### Managed Hosts

One of the benefits of Ansible is that managed hosts do not need to have a special agent installed. The Ansible control node connects to managed hosts using a standard network protocol to ensure that the systems are in the specified state.

Managed hosts might have some requirements depending on how the control node connects to them and what modules it will run on them.

Linux and UNIX managed hosts need to have Python 2 (version 2.6 or later) or Python 3 (version 3.5 or later) installed for most modules to work.

For Red Hat Enterprise Linux 8, you may be able to depend on the platform-python package. You can also enable and install the python36 application stream (or the python27 application stream).

```
[root@host ~]#yum module install python36
```

If SELinux is enabled on the managed hosts, you also need to make sure the python3-libselinux package is installed before using modules that are related to any copy, file, or template functions. (Note that if the other Python components are installed, you can use Ansible modules such as`yum`or`package`to ensure that this package is also installed.)

**IMPORTANT**

Some package names may be different in Red Hat Enterprise Linux 7 and earlier because of the ongoing migration to Python 3.

For Red Hat Enterprise Linux 7 and earlier, install the python package, which provides Python 2. Instead of python3-libselinux , you will need to make sure the libselinux-python package is installed.

Some modules might have their own additional requirements. For example, the`dnf`module, which can be used to install packages on current Fedora systems, requires the python3-dnf package ( python-dnf in RHEL 7).

**NOTE**

Some modules do not need Python. For example, arguments passed to the Ansible`raw`module are run directly through the configured remote shell instead of going through the module subsystem. This can be useful for managing devices that do not have Python available or cannot have Python installed, or for bootstrapping Python onto a system that does not have it.

However, the`raw`module is difficult to use in a safely idempotent way. If you can use a normal module instead, it is generally better to avoid using`raw`and similar command modules. This is discussed further later in the course.

**Microsoft Windows-based Managed Hosts**

Ansible includes a number of modules that are specifically designed for Microsoft Windows systems. These are listed in the[ Windows Modules ](https://docs.ansible.com/ansible/latest/modules/list_of_windows_modules.html)section of the Ansible module index.

Most of the modules specifically designed for Microsoft Windows managed hosts require PowerShell 3.0 or later on the managed host rather than Python. In addition, the managed hosts need to have PowerShell remoting configured. Ansible also requires at least .NET Framework 4.0 or later to be installed on Windows managed hosts.

This course uses Linux-based managed hosts in its examples, and does not go into great depth on the specific differences and adjustments needed when managing Microsoft Windows-based managed hosts. More information is available on the Ansible web site at[ https://docs.ansible.com/ansible/latest/user_guide/windows.html ](https://docs.ansible.com/ansible/latest/user_guide/windows.html).

**Managed Network Devices**

You can also use Ansible automation to configure managed network devices such as routers and switches. Ansible includes a large number of modules specifically designed for this purpose. This includes support for Cisco IOS, IOS XR, and NX-OS; Juniper Junos; Arista EOS; and VyOS-based networking devices, among others.

You can write Ansible Playbooks for network devices using the same basic techniques that you use when writing playbooks for servers. Because most network devices cannot run Python, Ansible runs network modules on the control node, not on the managed hosts. Special connection methods are also used to communicate with network devices, typically using either CLI over SSH, XML over SSH, or API over HTTP(S).

This course does not cover automation of network device management in any depth. For more information on this topic, see[ *Ansible for Network Automation* ](https://docs.ansible.com/ansible/latest/network/index.html)on the Ansible community website, or attend our alternative course[ *Ansible for Network Automation* (DO457) ](https://www.redhat.com/en/services/training/do457-ansible-network-automation).

**REFERENCES**

**ansible** (1) man page

[*Top Support Policies for Red Hat Ansible Automation*](https://access.redhat.com/ansible-top-support-policies)

[*Installation Guide — Ansible Documentation*](http://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

[*Windows Guides — Ansible Documentation*](https://docs.ansible.com/ansible/latest/user_guide/windows.html)

[*Ansible for Networking Automation — Ansible Documentation*](https://docs.ansible.com/ansible/latest/network/index.html)



------

### Guided Exercise: Installing Ansible

In this exercise, you will install Ansible on a control node running Red Hat Enterprise Linux.

**Outcome**

You should be able to install Ansible on a control node.

Log in to`workstation`as`student`using`student`as the password, and run **lab intro-install start** . This start script configures the control node.

```
[student@workstation ~]$lab intro-install start
```

1. Install Ansible on`workstation`so that it can serve the control node.

   ```
   [student@workstation ~]$sudo yum install ansible[sudo] password for student:
   Loaded plugins: langpacks, search-disabled-repos
   Resolving Dependencies
   --> Running transaction check
   ...output omitted...Is this ok [y/d/N]:y...output omitted...
   ```

2. Verify that Ansible is installed on the system. Execute the **ansible** command with the`--version`option.

   ```
   [student@workstation ~]$ansible --version
   ansible 2.8.0
     config file = /etc/ansible/ansible.cfg
     configured module search path = ['/home/student/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
     ansible python module location = /usr/lib/python3.6/site-packages/ansible
     executable location = /usr/bin/ansible
     python version = 3.6.8 (default, Apr  3 2019, 17:26:03) [GCC 8.2.1 20180905 (Red Hat 8.2.1-3)]
   ```

3. Verify the ansible_python_version on the localhost by using the setup module.

   ```
   [student@workstation ~]$ansible -m setup localhost | grep ansible_python_version
       "ansible_python_version": "3.6.8",
   ```

**Finish**

On`workstation`, run the **lab intro-install finish** script to clean up this exercise.

```
[student@workstation ~]$lab intro-install finish
```

This concludes the guided exercise.



------

###  Summary

In this chapter, you learned:

- Automation is a key tool to mitigate human error and quickly ensure that your IT infrastructure is in a consistent, correct state.
- Ansible is an open source automation platform that can adapt to many different workflows and environments.
- Ansible can be used to manage many different types of systems, including servers running Linux, Microsoft Windows, or UNIX, and network devices.
- Ansible Playbooks are human-readable text files that describe the desired state of an IT infrastructure.
- Ansible is built around an agentless architecture in which Ansible is installed on a control node and clients do not need any special agent software.
- Ansible connects to managed hosts using standard network protocols such as SSH, and runs code or commands on the managed hosts to ensure that they are in the state specified by Ansible.





## Chapter 2. Deploying Ansible

- [Building an Ansible Inventory](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch02)
- [Guided Exercise: Building an Ansible Inventory](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch02s02)
- [Managing Ansible Configuration Files](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch02s03)
- [Guided Exercise: Managing Ansible Configuration Files](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch02s04)
- [Running Ad Hoc Commands](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch02s05)
- [Guided Exercise: Running Ad Hoc Commands](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch02s06)
- [Lab: Deploying Ansible](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch02s07)
- [Summary](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch02s08)



**Abstract**

| **Goal**       | Configure Ansible to manage hosts and run ad hoc Ansible commands. |
| -------------- | ------------------------------------------------------------ |
| **Objectives** | Describe Ansible inventory concepts and manage a static inventory file.Describe where Ansible configuration files are located, how Ansible selects them, and edit them to apply changes to default settings.Run a single Ansible automation task using an ad hoc command and explain some use cases for ad hoc commands. |
| **Sections**   | Building an Ansible Inventory (and Guided Exercise)Managing Ansible Configuration Files (and Guided Exercise)Running Ad Hoc Commands (and Guided Exercise) |
| **Lab**        | Deploying Ansible                                            |



### Building an Ansible Inventory

#### Objectives

After completing this section, you should be able to describe Ansible inventory concepts and manage a static inventory file.

#### Defining the Inventory

An *inventory* defines a collection of hosts that Ansible will manage. These hosts can also be assigned to *groups* , which can be managed collectively. Groups can contain child groups, and hosts can be members of multiple groups. The inventory can also set variables that apply to the hosts and groups that it defines.

Host inventories can be defined in two different ways. A *static* host inventory can be defined by a text file. A *dynamic* host inventory can be generated by a script or other program as needed, using external information providers.

#### Specifying Managed Hosts with a Static Inventory

A static inventory file is a text file that specifies the managed hosts that Ansible targets. You can write this file using a number of different formats, including INI-style or YAML. The INI-style format is very common and will be used for most examples in this course.

**NOTE**

There are multiple static inventory formats supported by Ansible. In this section, we are focusing on the most common one, INI-style format.

In its simplest form, an INI-style static inventory file is a list of host names or IP addresses of managed hosts, each on a single line:

```
web1.example.com
web2.example.com
db1.example.com
db2.example.com
192.0.2.42
```

Normally, however, you organize managed hosts into *host groups* . Host groups allow you to more effectively run Ansible against a collection of systems. In this case, each section starts with a host group name enclosed in square brackets ([]). This is followed by the host name or an IP address for each managed host in the group, each on a single line.

In the following example, the host inventory defines two host groups:`webservers`and`db-servers`.

```
[webservers]
web1.example.com
web2.example.com
192.0.2.42

[db-servers]
db1.example.com
db2.example.com
```

Hosts can be in multiple groups. In fact, recommended practice is to organize your hosts into multiple groups, possibly organized in different ways depending on the role of the host, its physical location, whether it is in production or not, and so on. This allows you to easily apply Ansible plays to specific hosts.

```
[webservers]
web1.example.com
web2.example.com
192.0.2.42

[db-servers]
db1.example.com
db2.example.com

[east-datacenter]
web1.example.com
db1.example.com

[west-datacenter]
web2.example.com
db2.example.com

[production]
web1.example.com
web2.example.com
db1.example.com
db2.example.com

[development]
192.0.2.42
```

**IMPORTANT**

Two host groups always exist:

- The`all`host group contains every host explicitly listed in the inventory.
- The`ungrouped`host group contains every host explicitly listed in the inventory that is not a member of any other group.

**Defining Nested Groups**

Ansible host inventories can include groups of host groups. This is accomplished by creating a host group name with the`:children`suffix. The following example creates a new group called`north-america`, which includes all hosts from the`usa`and`canada`groups.

```
[usa]
washington1.example.com
washington2.example.com

[canada]
ontario01.example.com
ontario02.example.com

[north-america:children]
canada
usa
```

A group can have both managed hosts and child groups as members. For example, in the previous inventory you could add a`[north-america]`section that has its own list of managed hosts. That list of hosts would be merged with the additional hosts that the`north-america`group inherits from its child groups.

**Simplifying Host Specifications with Ranges**

You can specify ranges in the host names or IP addresses to simplify Ansible host inventories. You can specify either numeric or alphabetic ranges. Ranges have the following syntax:

```
[START:END]
```

Ranges match all values from*START*to*END*, inclusively. Consider the following examples:

- 192.168.[4:7].[0:255] matches all IPv4 addresses in the 192.168.4.0/22 network (192.168.4.0 through 192.168.7.255).
- server[01:20].example.com matches all hosts named server01.example.com through server20.example.com.
- [a:c].dns.example.com matches hosts named a.dns.example.com, b.dns.example.com, and c.dns.example.com.
- 2001:db8::[a:f] matches all IPv6 addresses from 2001:db8::a through 2001:db8::f.

If leading zeros are included in numeric ranges, they are used in the pattern. The second example above does not match`server1.example.com`but does match`server07.example.com`. To illustrate this, the following example uses ranges to simplify the`[usa]`and`[canada]`group definitions from the earlier example:

```
[usa]
washington[1:2].example.com

[canada]
ontario[01:02].example.com
```

**Verifying the Inventory**

When in doubt, use the **ansible** command to verify a machine's presence in the inventory:

```
[user@controlnode ~]$ansible washington1.example.com --list-hosts
  hosts (1):
    washington1.example.com
[user@controlnode ~]$ansible washington01.example.com --list-hosts
 [WARNING]: provided hosts list is empty, only localhost is available

  hosts (0):
```

You can run the following command to list all hosts in a group:

```
[user@controlnode ~]$ansible canada --list-hosts
  hosts (2):
    ontario01.example.com
    ontario02.example.com
```

**IMPORTANT**

If the inventory contains a host and a host group with the same name, the **ansible** command prints a warning and targets the host. The host group is ignored.

There are various ways to deal with this situation, the easiest being to ensure that host groups do not use the same names as hosts in the inventory.

**Overriding the Location of the Inventory**

The`/etc/ansible/hosts`file is considered the system's default static inventory file. However, normal practice is not to use that file but to define a different location for inventory files in your Ansible configuration file. This is covered in the next section.

The **ansible** and **ansible-playbook** commands that you use to run Ansible ad hoc commands and playbooks later in the course can also specify the location of an inventory file on the command line with the`--inventory*PATHNAME*`or`-i*PATHNAME*`option, where`PATHNAME`is the path to the desired inventory file.

**Defining Variables in the Inventory**

Values for variables used by playbooks can be specified in host inventory files. These variables only apply to specific hosts or host groups. Normally it is better to define these *inventory variables* in special directories and not directly in the inventory file. This topic is discussed in more depth elsewhere in the course.

#### Describing a Dynamic Inventory

Ansible inventory information can also be dynamically generated, using information provided by external databases. The open source community has written a number of dynamic inventory scripts that are available from the upstream Ansible project. If those scripts do not meet your needs, you can also write your own.

For example, a dynamic inventory program could contact your Red Hat Satellite server or Amazon EC2 account, and use information stored there to construct an Ansible inventory. Because the program does this when you run Ansible, it can populate the inventory with up-to-date information provided by the service as new hosts are added and old hosts are removed.

This topic is discussed in more detail later in the course.

**REFERENCES**

[Inventory: Ansible Documentation](http://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)



------

### Guided Exercise: Building an Ansible Inventory

In this exercise, you will create a new static inventory containing hosts and groups.

**Outcomes**

You should be able to create default and custom static inventories.

Log in to`workstation`as`student`using`student`as the password.

On`workstation`, run the **lab deploy-inventory start** command. This start script ensures that the managed hosts,`servera`,`serverb`,`serverc`, and`serverd`, are reachable on the network.

```
[student@workstation ~]$lab deploy-inventory start
```

1. Modify`/etc/ansible/hosts`to include`servera.lab.example.com`as a managed host.

   1. Add`servera.lab.example.com`to the end of the default inventory file,`/etc/ansible/hosts`.

      ```
      [student@workstation ~]$sudo vim /etc/ansible/hosts...output omitted...
      ## db-[99:101]-node.example.com
      
      servera.lab.example.com
      ```

   2. Continue editing the`/etc/ansible/hosts`inventory file by adding a`[webservers]`group to the bottom of the file with`serverb.lab.example.com`server as a group member. Save and exit when complete.

      ```
      ...output omitted...
      ## db-[99:101]-node.example.com
      
      servera.lab.example.com
      
      [webservers]
      serverb.lab.example.com
      ```

2. Verify the managed hosts in the`/etc/ansible/hosts`inventory file.

   1. Use the **ansible all --list-hosts** command to list all managed hosts in the default inventory file.

      ```
      [student@workstation ~]$ansible all --list-hosts
        hosts (2):
          servera.lab.example.com
          serverb.lab.example.com
      ```

   2. Use the **ansible** `ungrouped``--list-hosts`command to list only managed hosts that do not belong to a group.

      ```
      [student@workstation ~]$ansible ungrouped --list-hosts
        hosts (1):
          servera.lab.example.com
      ```

   3. Use the **ansible** `webservers``--list-hosts`command to list only managed hosts that belong to the`webservers`group.

      ```
      [student@workstation ~]$ansible webservers --list-hosts
        hosts (1):
          serverb.lab.example.com
      ```

3. Create a custom static inventory file named`inventory`in the`/home/student/deploy-inventory`working directory.

   Information about your four managed hosts is listed in the following table. You will assign each host to multiple groups for management purposes based on the purpose of the host, the city where it is located, and the deployment environment to which it belongs.

   In addition, groups for US cities (Raleigh and Mountain View) must be set up as children of the group`us`so that hosts in the United States can be managed as a group.

   

   **Table 2.1. Server Inventory Specifications**

   | Host name                 | Purpose    | Location      | Environment |
   | :------------------------ | :--------- | :------------ | :---------- |
   | `servera.lab.example.com` | Web server | Raleigh       | Development |
   | `serverb.lab.example.com` | Web server | Raleigh       | Testing     |
   | `serverc.lab.example.com` | Web server | Mountain View | Production  |
   | `serverd.lab.example.com` | Web server | London        | Production  |

   1. Create the`/home/student/deploy-inventory`working directory, and change into it.

      ```
      [student@workstation ~]$mkdir ~/deploy-inventory[student@workstation ~]$cd ~/deploy-inventory[student@workstation deploy-inventory]$
      ```

   2. Create an`inventory`file in the`/home/student/deploy-inventory`working directory. Use the Server Inventory Specifications table as a guide. Edit the`inventory`file and add the following content:

      ```
      [webservers]
      server[a:d].lab.example.com
      
      [raleigh]
      servera.lab.example.com
      serverb.lab.example.com
      
      [mountainview]
      serverc.lab.example.com
      
      [london]
      serverd.lab.example.com
      
      [development]
      servera.lab.example.com
      
      [testing]
      serverb.lab.example.com
      
      [production]
      serverc.lab.example.com
      serverd.lab.example.com
      
      [us:children]
      raleigh
      mountainview
      ```

4. Use variations of the **ansible** *host-or-group*`-i inventory``--list-hosts`command to verify the managed hosts and groups in the custom`/home/student/deploy-inventory/inventory`inventory file.

   

   **IMPORTANT**

   Your **ansible** command must include the`-i inventory`option. This makes **ansible** use your`inventory`file in the current working directory instead of the system`/etc/ansible/hosts`inventory file.

   1. Use the **ansible** `all``-i inventory``--list-hosts`command to list all managed hosts.

      ```
      [student@workstation deploy-inventory]$ansible all -i inventory --list-hosts
        hosts (4):
          servera.lab.example.com
          serverb.lab.example.com
          serverc.lab.example.com
          serverd.lab.example.com
      ```

   2. Use the **ansible** `ungrouped``-i inventory``--list-hosts`command to list all managed hosts listed in the inventory file but are not part of a group. There are no ungrouped managed hosts in this inventory file.

      ```
      [student@workstation deploy-inventory]$ansible ungrouped -i inventory \>--list-hosts
       [WARNING]: No hosts matched, nothing to do
      
        hosts (0):
      ```

   3. Use the **ansible** `development``-i inventory``--list-hosts`command to list all managed hosts listed in the`development`group.

      ```
      [student@workstation deploy-inventory]$ansible development -i inventory \>--list-hosts
        hosts (1):
          servera.lab.example.com
      ```

   4. Use the **ansible** `testing``-i inventory``--list-hosts`command to list all managed hosts listed in the`testing`group.

      ```
      [student@workstation deploy-inventory]$ansible testing -i inventory \>--list-hosts
        hosts (1):
          serverb.lab.example.com
      ```

   5. Use the **ansible** `production``-i inventory``--list-hosts`command to list all managed hosts listed in the`production`group.

      ```
      [student@workstation deploy-inventory]$ansible production -i inventory \>--list-hosts
        hosts (2):
          serverc.lab.example.com
          serverd.lab.example.com
      ```

   6. Use the **ansible** `us``-i inventory``--list-hosts`command to list all managed hosts listed in the`us`group.

      ```
      [student@workstation deploy-inventory]$ansible us -i inventory --list-hosts
        hosts (3):
          servera.lab.example.com
          serverb.lab.example.com
          serverc.lab.example.com
      ```

   7. You are encouraged to experiment with other variations to confirm managed host entries in the custom inventory file.

**Finish**

On`workstation`, run the **lab deploy-inventory finish** script to clean up this exercise.

```
[student@workstation ~]$lab deploy-inventory finish
```

This concludes the guided exercise.



------

### Running Ad Hoc Commands

#### Objectives

After completing this section, you should be able to run a single Ansible automation task using an ad hoc command and explain some use cases for ad hoc commands.

#### Running Ad Hoc Commands with Ansible

An *ad hoc command* is a way of executing a single Ansible task quickly, one that you do not need to save to run again later. They are simple, online operations that can be run without writing a playbook.

Ad hoc commands are useful for quick tests and changes. For example, you can use an ad hoc command to make sure that a certain line exists in the`/etc/hosts`file on a group of servers. You could use another ad hoc command to efficiently restart a service on many different machines, or to ensure that a particular software package is up-to-date.

Ad hoc commands are very useful for quickly performing simple tasks with Ansible. They do have their limits, and in general you will want to use Ansible Playbooks to realize the full power of Ansible. In many situations, however, ad hoc commands are exactly the tool you need to perform simple tasks quickly.

**Running Ad Hoc Commands**

Use the **ansible** command to run ad hoc commands:

```
ansiblehost-pattern-mmodule[-a'module arguments'] [-iinventory]
```

The *host-pattern* argument is used to specify the managed hosts on which the ad hoc command should be run. It could be a specific managed host or host group in the inventory. You have already seen this used in conjunction with the`--list-hosts`option, which shows you which hosts are matched by a particular host pattern. You have also already seen that you can use the`-i`option to specify a different inventory location to use than the default in the current Ansible configuration file.

The`-m`option takes as an argument the name of the *module* that Ansible should run on the targeted hosts. Modules are small programs that are executed to implement your task. Some modules need no additional information, but others need additional arguments to specify the details of their operation. The`-a`option takes a list of those arguments as a quoted string.

One of the simplest ad hoc commands uses the`ping`module. This module does not do an ICMP ping, but checks to see if you can run Python-based modules on managed hosts. For example, the following ad hoc command determines whether all managed hosts in the inventory can run standard modules:

```
[user@controlnode ~]$ansible all -m ping
servera.lab.example.com | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "ping": "pong"
}
```

**Performing Tasks with Modules Using Ad Hoc Commands**

Modules are the tools that ad hoc commands use to accomplish tasks. Ansible provides hundreds of modules which do different things. You can usually find a tested, special-purpose module that does what you need as part of the standard installation.

The **ansible-doc** `-l`command lists all modules installed on a system. You can use **ansible-doc** to view the documentation of particular modules by name, and find information about what arguments the modules take as options. For example, the following command displays documentation for the`ping`module:

```
[user@controlnode ~]$ansible-doc ping
> PING    (/usr/lib/python3.6/site-packages/ansible/modules/system/ping.py)

        A trivial test module, this module always returns `pong' on successful contact. It does not make sense in playbooks, but it is useful from `/usr/bin/ansible' to
        verify the ability to login and that a usable Python is configured. This is NOT ICMP ping, this is just a trivial test module that requires Python on the
        remote-node. For Windows targets, use the [win_ping] module instead. For Network targets, use the [net_ping] module instead.

  * This module is maintained by The Ansible Core Team
OPTIONS (= is mandatory):

- data
        Data to return for the `ping' return value.
        If this parameter is set to `crash', the module will cause an exception.
        [Default: pong]
        type: str


SEE ALSO:
      * Module net_ping
           The official documentation on the net_ping module.
           https://docs.ansible.com/ansible/latest/modules/net_ping_module.html
      * Module win_ping
           The official documentation on the win_ping module.
           https://docs.ansible.com/ansible/latest/modules/win_ping_module.html


AUTHOR: Ansible Core Team, Michael DeHaan
        METADATA:
          status:
          - stableinterface
          supported_by: core
        

EXAMPLES:

# Test we can logon to 'webservers' and execute python with json lib.
# ansible webservers -m ping

# Example from an Ansible Playbook
- ping:

# Induce an exception to see what happens
- ping:
    data: crash


RETURN VALUES:

ping:
    description: value provided with the data parameter
    returned: success
    type: str
    sample: pong
```

To learn more about modules, access the online Ansible documentation at[ http://docs.ansible.com/ansible/latest/modules/modules_by_category.html ](http://docs.ansible.com/ansible/latest/modules/modules_by_category.html).

The following table lists a number of useful modules as examples. Many others exist.



**Table 2.3. Ansible Modules**

| Module category          | Modules                                                      |
| :----------------------- | :----------------------------------------------------------- |
| Files modules            | `copy`: Copy a local file to the managed host`file`: Set permissions and other properties of files`lineinfile`: Ensure a particular line is or is not in a file`synchronize`: Synchronize content using **rsync** |
| Software package modules | `package`: Manage packages using autodetected package manager native to the operating system`yum`: Manage packages using the YUM package manager`apt`: Manage packages using the APT package manager`dnf`: Manage packages using the DNF package manager`gem`: Manage Ruby gems`pip`: Manage Python packages from PyPI |
| System modules           | `firewalld`: Manage arbitrary ports and services using **firewalld**`reboot`: Reboot a machine`service`: Manage services`user`: Add, remove, and manage user accounts |
| Net Tools modules        | `get_url`: Download files over HTTP, HTTPS, or FTP`nmcli`: Manage networking`uri`: Interact with web services |



Most modules take arguments. You can find the list of arguments available for a module in the module's documentation. Ad hoc commands pass arguments to modules using the`-a`option. When no argument is needed, omit the`-a`option from the ad hoc command. If multiple arguments need to be specified, supply them as a quoted space-separated list.

For example, the following ad hoc command uses the`user`module to ensure that the`newbie`user exists and has UID 4000 on`servera.lab.example.com`:

```
[user@controlnode ~]$ansible -m user -a 'name=newbie uid=4000 state=present' \>servera.lab.example.com
servera.lab.example.com | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": true,
    "comment": "",
    "createhome": true,
    "group": 4000,
    "home": "/home/newbie",
    "name": "newbie",
    "shell": "/bin/bash",
    "state": "present",
    "system": false,
    "uid": 4000
}
```

Most modules are *idempotent* , which means that they can be run safely multiple times, and if the system is already in the correct state, they do nothing. For example, if you run the previous ad hoc command again, it should report no change:

```
[user@controlnode ~]$ansible -m user -a 'name=newbie uid=4000 state=present' \>servera.lab.example.com
servera.lab.example.com | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "append": false,
    "changed": false
    "comment": "",
    "group": 4000,
    "home": "/home/newbie",
    "move_home": false,
    "name": "newbie",
    "shell": "/bin/bash",
    "state": "present",
    "uid": 4000
}
```

**Running Arbitrary Commands on Managed Hosts**

The`command`module allows administrators to run arbitrary commands on the command line of managed hosts. The command to be run is specified as an argument to the module using the`-a`option. For example, the following command runs the **hostname** command on the managed hosts referenced by the`mymanagedhosts`host pattern.

```
[user@controlnode ~]$ansible mymanagedhosts -m command -a /usr/bin/hostname
host1.lab.example.com | CHANGED | rc=0 >>
host1.lab.example.com
host2.lab.example.com | CHANGED | rc=0 >>
host2.lab.example.com
```

The previous ad hoc command example returned two lines of output for each managed host. The first line is a status report, showing the name of the managed host that the ad hoc operation ran on, as well as the outcome of the operation. The second line is the output of the command executed remotely using the Ansible`command`module.

For better readability and parsing of ad hoc command output, administrators might find it useful to have a single line of output for each operation performed on a managed host. Use the`-o`option to display the output of Ansible ad hoc commands in a single line format.

```
[user@controlnode ~]$ansible mymanagedhosts -m command -a /usr/bin/hostname -o
host1.lab.example.com | CHANGED | rc=0 >> (stdout) host1.lab.example.com
host2.lab.example.com | CHANGED | rc=0 >> (stdout) host2.lab.example.com
```

The`command`module allows administrators to quickly execute remote commands on managed hosts. These commands are not processed by the shell on the managed hosts. As such, they cannot access shell environment variables or perform shell operations, such as redirection and piping.

**NOTE**

If an ad hoc command does not specify which module to use with the`-m`option, Red Hat Ansible Engine uses the`command`module by default.

For situations where commands require shell processing, administrators can use the`shell`module. Like the`command`module, you pass the commands to be executed as arguments to the module in an ad hoc command. Ansible then executes the command remotely on the managed hosts. Unlike the`command`module, the commands are processed through a shell on the managed hosts. Therefore, shell environment variables are accessible and shell operations such as redirection and piping are also available for use.

The following example illustrates the difference between the`command`and`shell`modules. If you try to execute the built-in Bash command **set** with these two modules, it only succeeds with the`shell`module.

```
[user@controlnode ~]$ansible localhost -m command -a set
localhost | FAILED | rc=2 >>
[Errno 2] No such file or directory
[user@controlnode ~]$ansible localhost -m shell -a set
localhost | CHANGED | rc=0 >>
BASH=/bin/sh
BASHOPTS=cmdhist:extquote:force_fignore:hostcomplete:interact
ive_comments:progcomp:promptvars:sourcepath
BASH_ALIASES=()
...output omitted...
```

Both`command`and`shell`modules require a working Python installation on the managed host. A third module,`raw`, can run commands directly using the remote shell, bypassing the module subsystem. This is useful when managing systems that cannot have Python installed (for example, a network router). It can also be used to install Python on a host.

**IMPORTANT**

In most circumstances, it is a recommended practice that you avoid the`command`,`shell`, and`raw`"run command" modules.

Most other modules are idempotent and can perform change tracking automatically. They can test the state of systems and do nothing if those systems are already in the correct state. By contrast, it is much more complicated to use "run command" modules in a way that is idempotent. Depending upon them makes it harder for you to be confident that rerunning an ad hoc command or playbook would not cause an unexpected failure. When a`shell`or`command`module runs, it typically reports a`CHANGED`status based on whether it thinks it affected machine state.

There are times when "run command" modules are valuable tools and a good solution to a problem. If you do need to use them, it is probably best to try to use the`command`module first, resorting to`shell`or`raw`modules only if you need their special features.

#### Configuring Connections for Ad Hoc Commands

The directives for managed host connections and privilege escalation can be configured in the Ansible configuration file, and they can also be defined using options in ad hoc commands. When defined using options in ad hoc commands, they take precedence over the directive configured in the Ansible configuration file. The following table shows the analogous command-line options for each configuration file directive.



**Table 2.4. Ansible Command-line Options**

| Configuration file directives | Command-line option        |
| :---------------------------- | :------------------------- |
| `inventory`                   | `-i`                       |
| `remote_user`                 | `-u`                       |
| `become`                      | `--become` , `-b`          |
| `become_method`               | `--become-method`          |
| `become_user`                 | `--become-user`            |
| `become_ask_pass`             | `--ask-become-pass` , `-K` |



Before configuring these directives using command-line options, their currently defined values can be determined by consulting the output of **ansible** `--help`.

```
[user@controlnode ~]$ansible --help...output omitted...
  -b, --become          run operations with become (nopasswd implied)
  --become-method=BECOME_METHOD
                        privilege escalation method to use (default=sudo),
                        valid choices: [ sudo | su | pbrun | pfexec | runas |
                        doas ]
  --become-user=BECOME_USER
...output omitted...
  -u REMOTE_USER, --user=REMOTE_USER
                        connect as this user (default=None)
```



**REFERENCES**

**ansible** (1) man page

[Working with Patterns: Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/intro_patterns.html)

[Introduction to Ad-Hoc Commands: Ansible Documentation](http://docs.ansible.com/ansible/latest/user_guide/intro_adhoc.html)

[Module Index: Ansible Documentation](http://docs.ansible.com/ansible/latest/modules/modules_by_category)

[command - Executes a command on a remote node: Ansible Documentation](http://docs.ansible.com/ansible/latest/modules/command_module.html)

[shell - Execute commands in nodes: Ansible Documentation](http://docs.ansible.com/ansible/latest/modules/shell_module.html)



------

### Guided Exercise: Running Ad Hoc Commands

In this exercise, you will execute ad hoc commands on multiple managed hosts.

**Outcomes**

You should be able to execute commands on managed hosts on an ad hoc basis using privilege escalation.

You will execute ad hoc commands on`workstation`and`servera`using the`devops`user account. This account has the same **sudo** configuration on both`workstation`and`servera`.

Log in to`workstation`as`student`using`student`as the password.

On`workstation`, run the **lab deploy-adhoc start** command. This script ensures that the managed host`servera`is reachable on the network. It also creates and populates the`/home/student/deploy-adhoc`working directory with materials used in this exercise.

```
[student@workstation ~]$lab deploy-adhoc start
```

1. Determine the **sudo** configuration for the`devops`account on both`workstation`and`servera`.

   1. Determine the **sudo** configuration for the`devops`account that was configured when`workstation`was built. Enter`student`if prompted for the password for the`student`account.

      ```
      [student@workstation ~]$sudo -l -U devops...output omitted...
      User devops may run the following commands on workstation:
          (ALL) NOPASSWD: ALL
      ```

      Note that the user has full **sudo** privileges but does not require password authentication.

   2. Determine the **sudo** configuration for the`devops`account that was configured when`servera`was built.

      ```
      [student@workstation ~]$ssh devops@servera.lab.example.com[devops@servera ~]$sudo -l...output omitted...
      User devops may run the following commands on servera:
          (ALL) NOPASSWD: ALL
      [devops@servera ~]$exit
      ```

      Note that the user has full **sudo** privileges but does not require password authentication.

2. Change directory to`/home/student/deploy-adhoc`and examine the contents of the`ansible.cfg`and`inventory`files.

   ```
   [student@workstation ~]$cd ~/deploy-adhoc[student@workstation deploy-adhoc]$cat ansible.cfg
   [defaults]
   inventory=inventory
   [student@workstation deploy-adhoc]$cat inventory
   [control_node]
   localhost
   
   [intranetweb]
   servera.lab.example.com
   ```

   The configuration file uses the directory's`inventory`file as the Ansible inventory. Note that Ansible is not yet configured to use privilege escalation.

3. Using the`all`host group and the`ping`module, execute an ad hoc command that ensures all managed hosts can run Ansible modules using Python.

   ```
   [student@workstation deploy-adhoc]$ansible all -m ping
   servera.lab.example.com | SUCCESS => {
       "ansible_facts": {
           "discovered_interpreter_python": "/usr/libexec/platform-python"
       },
       "changed": false,
       "ping": "pong"
   }
   localhost | SUCCESS => {
       "ansible_facts": {
           "discovered_interpreter_python": "/usr/libexec/platform-python"
       },
       "changed": false,
       "ping": "pong"
   }
   ```

4. Using the`command`module, execute an ad hoc command on`workstation`to identify the user account that Ansible uses to perform operations on managed hosts. Use the`localhost`host pattern to connect to`workstation`for the ad hoc command execution. Because you are connecting locally,`workstation`is both the control node and managed host.

   ```
   [student@workstation deploy-adhoc]$ansible localhost -m command -a 'id'
   localhost | CHANGED | rc=0 >>
   uid=1000(student) gid=1000(student) groups=1000(student),10(wheel) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
   ```

   Notice that the ad hoc command was performed on the managed host as the`student`user.

5. Execute the previous ad hoc command on`workstation`but connect and perform the operation with the`devops`user account by using the`-u`option.

   ```
   [student@workstation deploy-adhoc]$ansible localhost -m command -a 'id' -u devops
   localhost | CHANGED | rc=0 >>
   uid=1001(devops) gid=1001(devops) groups=1001(devops) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
   ```

   Notice that the ad hoc command was performed on the managed host as the`devops`user.

6. Using the`copy`module, execute an ad hoc command on`workstation`to change the contents of the`/etc/motd`file so that it consists of the string "Managed by Ansible" followed by a newline. Execute the command using the`devops`account, but do not use the`--become`option to switch to`root`. The ad hoc command should fail due to lack of permissions.

   ```
   [student@workstation deploy-adhoc]$ansible localhost -m copy \>-a 'content="Managed by Ansible\n" dest=/etc/motd' -u devops
   localhost | FAILED! => {
       "ansible_facts": {
           "discovered_interpreter_python": "/usr/libexec/platform-python"
       },
       "changed": false,
       "checksum": "4458b979ede3c332f8f2128385df4ba305e58c27",
       "msg": "Destination /etc not writable"
   }
   ```

   The ad hoc command failed because the`devops`user does not have permission to write to the file.

7. Run the command again using privilege escalation. You could fix the settings in the`ansible.cfg`file, but for this example just use appropriate command-line options of the **ansible** command.

   Using the`copy`module, execute the previous command on`workstation`to change the contents of the`/etc/motd`file so that it consists of the string "Managed by Ansible" followed by a newline. Use the`devops`user to make the connection to the managed host, but perform the operation as the`root`user using the`--become`option. The use of the`--become`option is sufficient because the default value for the`become_user`directive is set to`root`in the`/etc/ansible/ansible.cfg`file.

   ```
   [student@workstation deploy-adhoc]$ansible localhost -m copy \>-a 'content="Managed by Ansible\n" dest=/etc/motd' -u devops --become
   localhost | CHANGED => {
       "ansible_facts": {
           "discovered_interpreter_python": "/usr/libexec/platform-python"
       },
       "changed": true,
       "checksum": "4458b979ede3c332f8f2128385df4ba305e58c27",
       "dest": "/etc/motd",
       "gid": 0,
       "group": "root",
       "md5sum": "65a4290ee5559756ad04e558b0e0c4e3",
       "mode": "0644",
       "owner": "root",
       "secontext": "system_u:object_r:etc_t:s0",
       "size": 19,
       "src": "/home/devops/.ansible/tmp/ansible-tmp-1558954193.0260043-131348380629718/source",
       "state": "file",
       "uid": 0
   }
   ```

   Note that the command succeeded this time because the ad hoc command was executed with privilege escalation.

8. Run the previous ad hoc command again on all hosts using the`all`host group. This ensures that`/etc/motd`on both`workstation`and`servera`consist of the text “ `Managed by Ansible` ” .

   ```
   [student@workstation deploy-adhoc]$ansible all -m copy \>-a 'content="Managed by Ansible\n" dest=/etc/motd' -u devops --become
   servera.lab.example.com | CHANGED => {
       "ansible_facts": {
           "discovered_interpreter_python": "/usr/libexec/platform-python"
       },
       "changed": true,
       "checksum": "4458b979ede3c332f8f2128385df4ba305e58c27",
       "dest": "/etc/motd",
       "gid": 0,
       "group": "root",
       "md5sum": "65a4290ee5559756ad04e558b0e0c4e3",
       "mode": "0644",
       "owner": "root",
       "secontext": "system_u:object_r:etc_t:s0",
       "size": 19,
       "src": "/home/devops/.ansible/tmp/ansible-tmp-1558954250.7893758-136255396678462/source",
       "state": "file",
       "uid": 0
   }
   localhost | SUCCESS => {
       "ansible_facts": {
           "discovered_interpreter_python": "/usr/libexec/platform-python"
       },
       "changed": false,
       "checksum": "4458b979ede3c332f8f2128385df4ba305e58c27",
       "dest": "/etc/motd",
       "gid": 0,
       "group": "root",
       "mode": "0644",
       "owner": "root",
       "path": "/etc/motd",
       "secontext": "system_u:object_r:etc_t:s0",
       "size": 19,
       "state": "file",
       "uid": 0
   }
   ```

   You should see`SUCCESS`for`localhost`and`CHANGED`for`servera`. However,`localhost`should report`"changed": false`because the file is already in the correct state. Conversely,`servera`should report`"changed": true`because the ad hoc command updated the file to the correct state.

9. Using the`command`module, execute an ad hoc command to run **cat /etc/motd** to verify that the contents of the file have been successfully modified on both`workstation`and`servera`. Use the`all`host group and the`devops`user to specify and make the connection to the managed hosts. You do not need privilege escalation for this command to work.

   ```
   [student@workstation deploy-adhoc]$ansible all -m command \>-a 'cat /etc/motd' -u devops
   servera.lab.example.com | CHANGED | rc=0 >>
   Managed by Ansible
   
   localhost | CHANGED | rc=0 >>
   Managed by Ansible
   ```

**Finish**

On`workstation`, run the **lab deploy-adhoc finish** script to clean up this exercise.

```
[student@workstation ~]$lab deploy-adhoc finish
```

This concludes the guided exercise.



------

### Lab: Deploying Ansible     

​      **Performance Checklist**     

In this lab, you will configure an Ansible control  node for connections to inventory hosts and use ad hoc commands to  perform actions on managed hosts.

​      **Outcomes**     

You should be able to configure a control node to run ad hoc commands on managed hosts.

You will use Ansible to manage a number of hosts from`workstation.lab.example.com`as the`student`user.         You will set up a project directory containing an`ansible.cfg`file with specific defaults, and an`inventory`directory containing an inventory file.

You will use ad hoc commands to ensure the`/etc/motd`file on all managed hosts consists of specified content.

Log in to`workstation`as`student`using`student`as the password.

On`workstation`, run the       **lab deploy-review start**      command.         This script ensures that the managed hosts are reachable on the network.

```
[student@workstation ~]$lab deploy-review start
```

1. Verify that the        ansible       package is installed on the control node, and run the         **ansible**        `--version`command.

   1. Verify that the          ansible         package is installed.

      ```
      [student@workstation ~]$yum list installed ansible
      Installed Packages
      ansible.noarch    2.8.0-1.el8ae     @rhel-8-server-ansible-2.8-rpms
      ```

   2. Run the           **ansible**          `--version`command to confirm the version of Ansible that is installed.

      ```
      [student@workstation ~]$ansible --version
      ansible 2.8.0
        config file = /etc/ansible/ansible.cfg
        configured module search path = ['/home/student/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
        ansible python module location = /usr/lib/python3.6/site-packages/ansible
        executable location = /usr/bin/ansible
        python version = 3.6.8 (default, Apr  3 2019, 17:26:03) [GCC 8.2.1 20180905 (Red Hat 8.2.1-3)]
      ```

   

In the`student`user's home directory on`workstation`,`/home/student`, create a new directory named`deploy-review`.           Change to that directory.

```
[student@workstation ~]$mkdir ~/deploy-review[student@workstation ~]$cd ~/deploy-review[student@workstation deploy-review]$
```



Create an`ansible.cfg`file in the`deploy-review`directory, which you will use to set the following Ansible defaults:

- Connect to managed hosts as the`devops`user.
- Use the`inventory`subdirectory to contain the inventory file.
- Disable privilege escalation by default.               If privilege escalation is enabled from the command line, configure default settings to have Ansible use the            **sudo**           method to switch to the`root`user account.               Ansible should not prompt for the`devops`login password or the            **sudo**           password.

The managed hosts have been configured with a`devops`user who can log in using SSH key-based authentication and can run any command as`root`using the         **sudo**        command without a password.

1. Use a text editor to create the`/home/student/deploy-review/ansible.cfg`file.               Create a`[defaults]`section.               Add a`remote_user`directive to have Ansible use the`devops`user when connecting to managed hosts.               Add an`inventory`directive to configure Ansible to use the`/home/student/deploy-review/inventory`directory as the default location for the inventory file.

   ```
   [defaults]
   remote_user = devops
   inventory = inventory
   ```

2. In the`/home/student/deploy-review/ansible.cfg`file, create the`[privilege_escalation]`section and add the following entries to disable privilege escalation.               Set the privilege escalation method to use the`root`account with           **sudo**          , without password authentication.

   ```
   [privilege_escalation]
   become = False
   become_method = sudo
   become_user = root
   become_ask_pass = False
   ```

3. The completed`ansible.cfg`file should read as follows:

   ```
   [defaults]
   remote_user = devops
   inventory = inventory
   
   [privilege_escalation]
   become = False
   become_method = sudo
   become_user = root
   become_ask_pass = False
   ```

   Save your work and exit the editor.



Create the`/home/student/deploy-review/inventory`directory.

Download the`http://materials.example.com/labs/deploy-review/inventory`file and save it as a static inventory file named`/home/student/deploy-review/inventory/inventory`.

1. Create the`/home/student/deploy-review/inventory`directory.

   ```
   [student@workstation deploy-review]$mkdir inventory
   ```

2. Download the`http://materials.example.com/labs/deploy-review/inventory`file to the`/home/student/deploy-review/inventory`directory.

   ```
   [student@workstation deploy-review]$wget -O inventory/inventory \>http://materials.example.com/labs/deploy-review/inventory
   ```

3. Inspect the contents of the`/home/student/deploy-review/inventory/inventory`file.

   ```
   [student@workstation deploy-review]$cat inventory/inventory
   [internetweb]
   serverb.lab.example.com
   
   [intranetweb]
   servera.lab.example.com
   serverc.lab.example.com
   serverd.lab.example.com
   ```



Execute the         **id**        command as an ad hoc command that targets the`all`host group to verify that`devops`is the remote user and that privilege escalation is disabled by default.

```
[student@workstation deploy-review]$ansible all -m command -a 'id'
serverb.lab.example.com | CHANGED | rc=0 >>
uid=1001(devops) gid=1001(devops) groups=1001(devops) context=unconfined_u:
 unconfined_r:unconfined_t:s0-s0:c0.c1023

serverc.lab.example.com | CHANGED | rc=0 >>
uid=1001(devops) gid=1001(devops) groups=1001(devops) context=unconfined_u:
 unconfined_r:unconfined_t:s0-s0:c0.c1023

servera.lab.example.com | CHANGED | rc=0 >>
uid=1001(devops) gid=1001(devops) groups=1001(devops) context=unconfined_u:
 unconfined_r:unconfined_t:s0-s0:c0.c1023

serverd.lab.example.com | CHANGED | rc=0 >>
uid=1001(devops) gid=1001(devops) groups=1001(devops) context=unconfined_u:
unconfined_r:unconfined_t:s0-s0:c0.c1023
```

Your results may be returned in a different order.



Execute an ad hoc command, targeting the`all`host group, that uses the`copy`module to modify the contents of the`/etc/motd`file on all hosts.

Use the`copy`module's`content`option to ensure that the`/etc/motd`file consists of the string`This server is managed by Ansible.\n`as a single line.           (The`\n`used with the`content`option causes the module to put a newline at the end of the string.)

You must request privilege escalation from the command line to make this work with your current`ansible.cfg`defaults.

```
[student@workstation deploy-review]$ansible all -m copy \>-a 'content="This server is managed by Ansible.\n" dest=/etc/motd' --become
serverd.lab.example.com | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": true,
    "checksum": "93d304488245bb2769752b95e0180607effc69ad",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "md5sum": "af74293c7b2a783c4f87064374e9417a",
    "mode": "0644",
    "owner": "root",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 35,
    "src": "/home/devops/.ansible/tmp/ansible-tmp-1558954517.7426903-24998924904061/source",
    "state": "file",
    "uid": 0
}
servera.lab.example.com | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": true,
    "checksum": "93d304488245bb2769752b95e0180607effc69ad",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "md5sum": "af74293c7b2a783c4f87064374e9417a",
    "mode": "0644",
    "owner": "root",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 35,
    "src": "/home/devops/.ansible/tmp/ansible-tmp-1558954517.7165847-103324013882266/source",
    "state": "file",
    "uid": 0
}
serverc.lab.example.com | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": true,
    "checksum": "93d304488245bb2769752b95e0180607effc69ad",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "md5sum": "af74293c7b2a783c4f87064374e9417a",
    "mode": "0644",
    "owner": "root",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 35,
    "src": "/home/devops/.ansible/tmp/ansible-tmp-1558954517.75727-94151722302122/source",
    "state": "file",
    "uid": 0
}
serverb.lab.example.com | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": true,
    "checksum": "93d304488245bb2769752b95e0180607effc69ad",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "md5sum": "af74293c7b2a783c4f87064374e9417a",
    "mode": "0644",
    "owner": "root",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 35,
    "src": "/home/devops/.ansible/tmp/ansible-tmp-1558954517.6649802-53313238077104/source",
    "state": "file",
    "uid": 0
}
```



If you run the same ad hoc command again, you should see that the`copy`module detects that the files are already correct and so they are not changed.           Look for the ad hoc command to report`SUCCESS`and the line`"changed": false`for each managed host.

```
[student@workstation deploy-review]$ansible all -m copy \>-a 'content="This server is managed by Ansible.\n" dest=/etc/motd' --become
serverb.lab.example.com | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "checksum": "93d304488245bb2769752b95e0180607effc69ad",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "mode": "0644",
    "owner": "root",
    "path": "/etc/motd",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 35,
    "state": "file",
    "uid": 0
}
serverc.lab.example.com | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "checksum": "93d304488245bb2769752b95e0180607effc69ad",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "mode": "0644",
    "owner": "root",
    "path": "/etc/motd",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 35,
    "state": "file",
    "uid": 0
}
serverd.lab.example.com | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "checksum": "93d304488245bb2769752b95e0180607effc69ad",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "mode": "0644",
    "owner": "root",
    "path": "/etc/motd",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 35,
    "state": "file",
    "uid": 0
}
servera.lab.example.com | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/libexec/platform-python"
    },
    "changed": false,
    "checksum": "93d304488245bb2769752b95e0180607effc69ad",
    "dest": "/etc/motd",
    "gid": 0,
    "group": "root",
    "mode": "0644",
    "owner": "root",
    "path": "/etc/motd",
    "secontext": "system_u:object_r:etc_t:s0",
    "size": 35,
    "state": "file",
    "uid": 0
}
```



To confirm this another way, run an ad hoc command that targets the`all`host group, using the`command`module to execute the         **cat /etc/motd**        command.           Output from the         **ansible**        command should display the string`This server is managed by Ansible.`for all hosts.           You do not need privilege escalation for this ad hoc command.

```
[student@workstation deploy-review]$ansible all -m command -a 'cat /etc/motd'
serverb.lab.example.com | CHANGED | rc=0 >>
This server is managed by Ansible.

servera.lab.example.com | CHANGED | rc=0 >>
This server is managed by Ansible.

serverd.lab.example.com | CHANGED | rc=0 >>
This server is managed by Ansible.

serverc.lab.example.com | CHANGED | rc=0 >>
This server is managed by Ansible.
```



1. Run         **lab**        `deploy-review``grade`on`workstation`to check your work.

   ```
   [student@workstation deploy-review]$lab deploy-review grade
   ```

​      **Finish**     

On`workstation`, run the       **lab**      `deploy-review``finish`script to clean up the resources created in this lab.

```
[student@workstation ~]$lab deploy-review finish
```

This concludes the lab.



------

###  Summary

In this chapter, you learned:

- Any system upon which Ansible is installed and which has access to the required configuration files and playbooks to manage remote systems ( *managed hosts* ) is called a *control node* .
- Managed hosts are defined in the *inventory* . Host patterns are used to reference managed hosts defined in an inventory.
- Inventories can be static files or dynamically generated by a program from an external source, such as a directory service or cloud management system.
- Ansible looks for its configuration file in a number of places in order of precedence. The first configuration file found is used; all others are ignored.
- The **ansible** command is used to perform *ad hoc commands* on managed hosts.
- Ad hoc commands determine the operation to perform through the use of *modules* and their arguments, and can make use of Ansible's *privilege escalation* features.





## Chapter 3. Implementing Playbooks

- [Writing and Running Playbooks](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch03)
- [Guided Exercise: Writing and Running Playbooks](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch03s02)
- [Implementing Multiple Plays](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch03s03)
- [Guided Exercise: Implementing Multiple Plays](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch03s04)
- [Lab: Implementing Playbooks](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch03s05)
- [Summary](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch03s06)



**Abstract**

| **Goal**       | Write a simple Ansible Playbook and run it to automate tasks on multiple hosts. |
| -------------- | ------------------------------------------------------------ |
| **Objectives** | Write a basic Ansible Playbook and run it using the **ansible-playbook** command.Write a playbook that uses multiple plays and per-play privilege escalation.Effectively use **ansible-doc** to learn how to use new modules to implement tasks for a play. |
| **Sections**   | Writing and Running Playbooks (and Guided Exercise)Implementing Multiple Plays (and Guided Exercise) |
| **Lab**        | Implementing Playbooks                                       |



### Writing and Running Playbooks

#### Objectives

After completing this section, you should be able to write a basic Ansible Playbook and run it using the **ansible-playbook** command.

#### Ansible Playbooks and Ad Hoc Commands

Ad hoc commands can run a single, simple task against a set of targeted hosts as a one-time command. The real power of Ansible, however, is in learning how to use playbooks to run multiple, complex tasks against a set of targeted hosts in an easily repeatable manner.

A *play* is an ordered set of tasks run against hosts selected from your inventory. A *playbook* is a text file containing a list of one or more plays to run in a specific order.

Plays allow you to change a lengthy, complex set of manual administrative tasks into an easily repeatable routine with predictable and successful outcomes. In a playbook, you can save the sequence of tasks in a play into a human-readable and immediately runnable form. The tasks themselves, because of the way in which they are written, document the steps needed to deploy your application or infrastructure.

#### Formatting an Ansible Playbook

To help you understand the format of a playbook, review this ad hoc command from a previous chapter:

```
[student@workstation ~]$ansible -m user -a "name=newbie uid=4000 state=present" \>servera.lab.example.com
```

This can be rewritten as a single task play and saved in a playbook. The resulting playbook appears as follows:



**Example 3.1. A Simple Playbook**

```
---
- name: Configure important user consistently
  hosts: servera.lab.example.com
  tasks:
    - name: newbie exists with UID 4000
      user:
        name: newbie
        uid: 4000
        state: present
```



A playbook is a text file written in YAML format, and is normally saved with the extension`yml`. The playbook uses indentation with space characters to indicate the structure of its data. YAML does not place strict requirements on how many spaces are used for the indentation, but there are two basic rules.

- Data elements at the same level in the hierarchy (such as items in the same list) must have the same indentation.
- Items that are children of another item must be indented more than their parents.

You can also add blank lines for readability.

**IMPORTANT**

Only the space character can be used for indentation; tab characters are not allowed.

If you use the **vi** text editor, you can apply some settings which might make it easier to edit your playbooks. For example, you can add the following line to your`$HOME/.vimrc`file, and when **vi** detects that you are editing a YAML file, it performs a 2-space indentation when you press the **Tab** key and autoindents subsequent lines.

```
autocmd FileType yaml setlocal ai ts=2 sw=2 et
```

A playbook begins with a line consisting of three dashes (`---`) as a start of document marker. It may end with three dots (`...`) as an end of document marker, although in practice this is often omitted.

In between those markers, the playbook is defined as a list of plays. An item in a YAML list starts with a single dash followed by a space. For example, a YAML list might appear as follows:

```
- apple
- orange
- grape
```

In[ Example 3.1, “A Simple Playbook” ](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch03), the line after`---`begins with a dash and starts the first (and only) play in the list of plays.

The play itself is a collection of key-value pairs. Keys in the same play should have the same indentation. The following example shows a YAML snippet with three keys. The first two keys have simple values. The third has a list of three items as a value.

```
  name: just an example
  hosts: webservers
  tasks:
    - first
    - second
    - third
```

The original example play has three keys,`name`,`hosts`, and`tasks`, because these keys all have the same indentation.

The first line of the example play starts with a dash and a space (indicating the play is the first item of a list), and then the first key, the`name`attribute. The`name`key associates an arbitrary string with the play as a label. This identifies what the play is for. The`name`key is optional, but is recommended because it helps to document your playbook. This is especially useful when a playbook contains multiple plays.

```
- name: Configure important user consistently
```

The second key in the play is a`hosts`attribute, which specifies the hosts against which the play's tasks are run. Like the argument for the **ansible** command, the`hosts`attribute takes a host pattern as a value, such as the names of managed hosts or groups in the inventory.

```
  hosts: servera.lab.example.com
```

Finally, the last key in the play is the`tasks`attribute, whose value specifies a list of tasks to run for this play. This example has a single task, which runs the`user`module with specific arguments (to ensure user`newbie`exists and has UID 4000).

```
  tasks:
    - name: newbie exists with UID 4000
      user:
        name: newbie
        uid: 4000
        state: present
```

The`tasks`attribute is the part of the play that actually lists, in order, the tasks to be run on the managed hosts. Each task in the list is itself a collection of key-value pairs.

In this example, the only task in the play has two keys:

- `name`is an optional label documenting the purpose of the task. It is a good idea to name all your tasks to help document the purpose of each step of the automation process.
- `user`is the module to run for this task. Its arguments are passed as a collection of key-value pairs, which are children of the module (`name`,`uid`, and`state`).

The following is another example of a`tasks`attribute with multiple tasks, using the`service`module to ensure that several network services are enabled to start at boot:

```
  tasks:
    - name: web server is enabled
      service:
        name: httpd
        enabled: true

    - name: NTP server is enabled
      service:
        name: chronyd
        enabled: true

    - name: Postfix is enabled
      service:
        name: postfix
        enabled: true
```

**IMPORTANT**

The order in which the plays and tasks are listed in a playbook is important, because Ansible runs them in the same order.

The playbooks you have seen so far are basic examples, and you will see more sophisticated examples of what you can do with plays and tasks as this course continues.

#### Running Playbooks

The **ansible-playbook** command is used to run playbooks. The command is executed on the control node and the name of the playbook to be run is passed as an argument:

```
[student@workstation ~]$ansible-playbook site.yml
```

When you run the playbook, output is generated to show the play and tasks being executed. The output also reports the results of each task executed.

The following example shows the contents of a simple playbook, and then the result of running it.

```
[student@workstation playdemo]$cat webserver.yml
---
- name: play to setup web server
  hosts: servera.lab.example.com
  tasks:
  - name: latest httpd version installed
    yum:
      name: httpd
      state: latest
...output omitted...[student@workstation playdemo]$ansible-playbook webserver.yml

PLAY [play to setup web server] ************************************************

TASK [Gathering Facts] *********************************************************
ok: [servera.lab.example.com]

TASK [latest httpd version installed] ******************************************
changed: [servera.lab.example.com]

PLAY RECAP *********************************************************************
servera.lab.example.com    : ok=2    changed=1    unreachable=0    failed=0
```

Note that the value of the`name`key for each play and task is displayed when the playbook is run. (The`Gathering Facts`task is a special task that the`setup`module usually runs automatically at the start of a play. This is covered later in the course.) For playbooks with multiple plays and tasks, setting`name`attributes makes it easier to monitor the progress of a playbook's execution.

You should also see that the`latest httpd version installed`task is`changed`for`servera.lab.example.com`. This means that the task changed something on that host to ensure its specification was met. In this case, it means that the httpd package probably was not installed or was not the latest version.

In general, tasks in Ansible Playbooks are idempotent, and it is safe to run a playbook multiple times. If the targeted managed hosts are already in the correct state, no changes should be made. For example, assume that the playbook from the previous example is run again:

```
[student@workstation playdemo]$ansible-playbook webserver.yml

PLAY [play to setup web server] ************************************************

TASK [Gathering Facts] *********************************************************
ok: [servera.lab.example.com]

TASK [latest httpd version installed] ******************************************
ok: [servera.lab.example.com]

PLAY RECAP *********************************************************************
servera.lab.example.com    : ok=2    changed=0    unreachable=0    failed=0
```

This time, all tasks passed with status`ok`and no changes were reported.

**Increasing Output Verbosity**

The default output provided by the **ansible-playbook** command does not provide detailed task execution information. The **ansible-playbook -v** command provides additional information, with up to four total levels.



**Table 3.1. Configuring the Output Verbosity of Playbook Execution**

| Option  | Description                                                  |
| :------ | :----------------------------------------------------------- |
| `-v`    | The task results are displayed.                              |
| `-vv`   | Both task results and task configuration are displayed       |
| `-vvv`  | Includes information about connections to managed hosts      |
| `-vvvv` | Adds extra verbosity options to the connection plug-ins, including users being used in the managed hosts to execute scripts, and what scripts have been executed |



**Syntax Verification**

Prior to executing a playbook, it is good practice to perform a verification to ensure that the syntax of its contents is correct. The **ansible-playbook** command offers a`--syntax-check`option that you can use to verify the syntax of a playbook. The following example shows the successful syntax verification of a playbook.

```
[student@workstation ~]$ansible-playbook --syntax-check webserver.yml

playbook: webserver.yml
```

When syntax verification fails, a syntax error is reported. The output also includes the approximate location of the syntax issue in the playbook. The following example shows the failed syntax verification of a playbook where the space separator is missing after the`name`attribute for the play.

```
[student@workstation ~]$ansible-playbook --syntax-check webserver.yml
ERROR! Syntax Error while loading YAML.
  mapping values are not allowed in this context

The error appears to have been in ...output omitted... line 3, column 8, but may
be elsewhere in the file depending on the exact syntax problem.

The offending line appears to be:

- name:play to setup web server
  hosts: servera.lab.example.com
       ^ here
```

**Executing a Dry Run**

You can use the`-C`option to perform a *dry run* of the playbook execution. This causes Ansible to report what changes would have occurred if the playbook were executed, but does not make any actual changes to managed hosts.

The following example shows the dry run of a playbook containing a single task for ensuring that the latest version of httpd package is installed on a managed host. Note that the dry run reports that the task would effect a change on the managed host.

```
[student@workstation ~]$ansible-playbook -C webserver.yml

PLAY [play to setup web server] ************************************************

TASK [Gathering Facts] *********************************************************
ok: [servera.lab.example.com]

TASK [latest httpd version installed] ******************************************
changed: [servera.lab.example.com]

PLAY RECAP *********************************************************************
servera.lab.example.com    : ok=2    changed=1    unreachable=0    failed=0
```

**REFERENCES**

`ansible-playbook`(1) man page

[Intro to Playbooks — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html)

[Playbooks — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html)

[Check Mode ("Dry Run") — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_checkmode.html)



------

###  Guided Exercise: Writing and Running Playbooks

In this exercise, you will write and run an Ansible Playbook.

**Outcomes**

You should be able to write a playbook using basic YAML syntax and Ansible Playbook structure, and successfully run it with the **ansible-playbook** command.

Log in to`workstation`as`student`using`student`as the password.

On`workstation`, run the **lab playbook-basic start** command. This function ensures that the managed hosts,`serverc.lab.example.com`and`serverd.lab.example.com`are reachable on the network. It also ensures that the correct Ansible configuration file and inventory file are installed on the control node.

```
[student@workstation ~]$lab playbook-basic start
```

The`/home/student/playbook-basic`working directory has been created on`workstation`for this exercise. This directory has already been populated with an`ansible.cfg`configuration file, and also an`inventory`inventory file, which defines a`web`group that includes both managed hosts listed above as members.

In this directory, use a text editor to create a playbook named`site.yml`. This playbook contains one play, which should target members of the`web`host group. The playbook should use tasks to ensure that the following conditions are met on the managed hosts:

- The httpd package is present, using the`yum`module.
- The local`files/index.html`file is copied to`/var/www/html/index.html`on each managed host, using the`copy`module.
- The`httpd`service is started and enabled, using the`service`module.

You can use the **ansible-doc** command to help you understand the keywords needed for each of the modules.

After the playbook is written, verify its syntax and then use **ansible-playbook** to run the playbook to implement the configuration.

1. Change to the`/home/student/playbook-basic`directory.

   ```
   [student@workstation ~]$cd ~/playbook-basic[student@workstation playbook-basic]$
   ```

2. Use a text editor to create a new playbook called`/home/student/playbook-basic/site.yml`. Start writing a play that targets the hosts in the`web`host group.

   1. Create and open`~/playbook-basic/site.yml`. The first line of the file should be three dashes to indicate the start of the playbook.

      ```
      ---
      ```

   2. The next line starts the play. It needs to start with a dash and a space before the first keyword in the play. Name the play with an arbitrary string documenting the play's purpose, using the`name`keyword.

      ```
      ---
      - name: Install and start Apache HTTPD
      ```

   3. Add a`hosts`keyword-value pair to specify that the play run on hosts in the inventory's`web`host group. Make sure that the`hosts`keyword is indented two spaces so it aligns with the`name`keyword in the preceding line.

      The complete`site.yml`file should now appear as follows:

      ```
      ---
      - name: Install and start Apache HTTPD
        hosts: web
      ```

3. Continue to edit the`/home/student/playbook-basic/site.yml`file, and add a`tasks`keyword and the three tasks for your play that were specified in the instructions.

   1. Add a`tasks`keyword indented by two spaces (aligned with the`hosts`keyword) to start the list of tasks. Your file should now appear as follows:

      ```
      ---
      - name: Install and start Apache HTTPD
        hosts: web
        tasks:
      ```

   2. Add the first task. Indent by four spaces, and start the task with a dash and a space, and then give the task a name, such as`httpd package is present`. Use the`yum`module for this task. Indent the module keywords two more spaces; set the package name to`httpd`and the package state to`present`. The task should appear as follows:

      ```
          - name: httpd package is present
            yum:
              name: httpd
              state: present
      ```

   3. Add the second task. Match the format of the previous task, and give the task a name, such as`correct index.html is present`. Use the`copy`module. The module keywords should set the`src`key to`files/index.html`and the`dest`key to`/var/www/html/index.html`. The task should appear as follows:

      ```
          - name: correct index.html is present
            copy:
              src: files/index.html
              dest: /var/www/html/index.html
      ```

   4. Add the third task to start and enable the`httpd`service. Match the format of the previous two tasks, and give the new task a name, such as`httpd is started`. Use the`service`module for this task. Set the`name`key of the service to`httpd`, the`state`key to`started`, and the`enabled`key to`true`. The task should appear as follows:

      ```
          - name: httpd is started
            service:
              name: httpd
              state: started
              enabled: true
      ```

   5. Your entire`site.yml`Ansible Playbook should match the following example. Make sure that the indentation of your play's keywords, the list of tasks, and each task's keywords are all correct.

      ```
      ---
      - name: Install and start Apache HTTPD
        hosts: web
        tasks:
          - name: httpd package is present
            yum:
              name: httpd
              state: present
      
          - name: correct index.html is present
            copy:
              src: files/index.html
              dest: /var/www/html/index.html
      
          - name: httpd is started
            service:
              name: httpd
              state: started
              enabled: true
      ```

      Save the file and exit your text editor.

4. Before running your playbook, run the **ansible-playbook --syntax-check site.yml** command to verify that its syntax is correct. If it reports any errors, correct them before moving to the next step. You should see output similar to the following:

   ```
   [student@workstation playbook-basic]$ansible-playbook --syntax-check site.yml
   
   playbook: site.yml
   ```

5. Run your playbook. Read through the output generated to ensure that all tasks completed successfully.

   ```
   [student@workstation playbook-basic]$ansible-playbook site.yml
   
   PLAY [Install and start Apache HTTPD] ******************************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [serverd.lab.example.com]
   ok: [serverc.lab.example.com]
   
   TASK [httpd package is present] ************************************************
   changed: [serverd.lab.example.com]
   changed: [serverc.lab.example.com]
   
   TASK [correct index.html is present] *******************************************
   changed: [serverd.lab.example.com]
   changed: [serverc.lab.example.com]
   
   TASK [httpd is started] ********************************************************
   changed: [serverd.lab.example.com]
   changed: [serverc.lab.example.com]
   
   PLAY RECAP *********************************************************************
   serverc.lab.example.com    : ok=4    changed=3    unreachable=0    failed=0      
   serverd.lab.example.com    : ok=4    changed=3    unreachable=0    failed=0
   ```

6. If all went well, you should be able to run the playbook a second time and see all tasks complete with no changes to the managed hosts.

   ```
   [student@workstation playbook-basic]$ansible-playbook site.yml
   
   PLAY [Install and start Apache HTTPD] ******************************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [serverd.lab.example.com]
   ok: [serverc.lab.example.com]
   
   TASK [httpd package is present] ************************************************
   ok: [serverd.lab.example.com]
   ok: [serverc.lab.example.com]
   
   TASK [correct index.html is present] *******************************************
   ok: [serverc.lab.example.com]
   ok: [serverd.lab.example.com]
   
   TASK [httpd is started] ********************************************************
   ok: [serverd.lab.example.com]
   ok: [serverc.lab.example.com]
   
   PLAY RECAP *********************************************************************
   serverc.lab.example.com    : ok=4    changed=0    unreachable=0    failed=0      
   serverd.lab.example.com    : ok=4    changed=0    unreachable=0    failed=0
   ```

7. Use the **curl** command to verify that both`serverc`and`serverd`are configured as an HTTPD server.

   ```
   [student@workstation playbook-basic]$curl serverc.lab.example.com
   This is a test page.
   [student@workstation playbook-basic]$curl serverd.lab.example.com
   This is a test page.
   ```

**Finish**

On`workstation`, run the **lab playbook-basic finish** script to clean up the resources created in this exercise.

```
[student@workstation ~]$lab playbook-basic finish
```

This concludes the guided exercise.



------

### Implementing Multiple Plays

#### Objectives

After completing this section, you should be able to:

- Write a playbook that uses multiple plays and per-play privilege escalation.
- Effectively use **ansible-doc** to learn how to use new modules to implement tasks for a play.

#### Writing Multiple Plays

A playbook is a YAML file containing a list of one or more plays. Remember that a single play is an ordered list of tasks to execute against hosts selected from the inventory. Therefore, if a playbook contains multiple plays, each play may apply its tasks to a separate set of hosts.

This can be very useful when orchestrating a complex deployment which may involve different tasks on different hosts. You can write a playbook that runs one play against one set of hosts, and when that finishes runs another play against another set of hosts.

Writing a playbook that contains multiple plays is very straightforward. Each play in the playbook is written as a top-level list item in the playbook. Each play is a list item containing the usual play keywords.

The following example shows a simple playbook with two plays. The first play runs against`web.example.com`, and the second play runs against`database.example.com`.

```
---
# This is a simple playbook with two plays

- name: first play
  hosts: web.example.com
  tasks:
    - name: first task
      yum:
        name: httpd
        status: present

    - name: second task
      service:
        name: httpd
        enabled: true

- name: second play
  hosts: database.example.com
  tasks:
    - name: first task
      service:
        name: mariadb
        enabled: true
```

#### Remote Users and Privilege Escalation in Plays

Plays can use different remote users or privilege escalation settings for a play than what is specified by the defaults in the configuration file. These are set in the play itself at the same level as the`hosts`or`tasks`keywords.

**User Attributes**

Tasks in playbooks are normally executed through a network connection to the managed hosts. As with ad hoc commands, the user account used for task execution depends on various keywords in the Ansible configuration file,`/etc/ansible/ansible.cfg`. The user that runs the tasks can be defined by the`remote_user`keyword. However, if privilege escalation is enabled, other keywords such as`become_user`can also have an impact.

If the remote user defined in the Ansible configuration for task execution is not suitable, it can be overridden by using the`remote_user`keyword within a play.

```
remote_user: remoteuser
```

**Privilege Escalation Attributes**

Additional keywords are also available to define privilege escalation parameters from within a playbook. The`become`boolean keyword can be used to enable or disable privilege escalation regardless of how it is defined in the Ansible configuration file. It can take`yes`or`true`to enable privilege escalation, or`no`or`false`to disable it.

```
become: true
```

If privilege escalation is enabled, the`become_method`keyword can be used to define the privilege escalation method to use during a specific play. The example below specifies that **sudo** be used for privilege escalation.

```
become_method: sudo
```

Additionally, with privilege escalation enabled, the`become_user`keyword can define the user account to use for privilege escalation within the context of a specific play.

```
become_user: privileged_user
```

The following example demonstrates the use of these keywords in a play:

```
- name: /etc/hosts is up to date
  hosts: datacenter-west
  remote_user: automation
  become: yes

  tasks:
    - name: server.example.com in /etc/hosts
      lineinfile:
        path: /etc/hosts
        line: '192.0.2.42 server.example.com server'
        state: present
```

#### Finding Modules for Tasks

**Module Documentation**

The large number of modules packaged with Ansible provides administrators with many tools for common administrative tasks. Earlier in this course, we discussed the Ansible documentation website at[ http://docs.ansible.com ](http://docs.ansible.com/). The *Module Index* on the website is an easy way to browse the list of modules shipped with Ansible. For example, modules for user and service management can be found under *Systems Modules* and modules for database administration can be found under *Database Modules* .

For each module, the Ansible documentation website provides a summary of its functions and instructions on how each specific function can be invoked with options to the module. The documentation also provides useful examples that show you how to use each module and how to set their keywords in a task.

You have already worked with the **ansible-doc** command to look up information about modules installed on the local system. As a review, to see a list of the modules available on a control node, run the **ansible-doc -l** command. This displays a list of module names and a synopsis of their functions.

```
[student@workstation modules]$ansible-doc -l
a10_server              Manage A10 Networks ... devices' server object.
a10_server_axapi3       Manage A10 Networks ... devices
a10_service_group       Manage A10 Networks ... devices' service groups.
a10_virtual_server      Manage A10 Networks ... devices' virtual servers.
...output omitted...
zfs_facts               Gather facts about ZFS datasets.
znode                   Create, ... and update znodes using ZooKeeper
zpool_facts             Gather facts about ZFS pools.
zypper                  Manage packages on SUSE and openSUSE
zypper_repository       Add and remove Zypper repositories
```

Use the **ansible-doc [module name]**command to display detailed documentation for a module. Like the Ansible documentation website, the command provides a synopsis of the module's function, details of its various options, and examples. The following example shows the documentation displayed for the`yum`module.

```
[student@workstation modules]$ansible-doc yum
> YUM    (/usr/lib/python3.6/site-packages/ansible/modules/packaging/os/yum.py)

        Installs, upgrade, downgrades, removes, and lists packages and groups with the `yum' package manager. This module only works on Python 2. If you require Python
        3 support see the [dnf] module.

  * This module is maintained by The Ansible Core Team
  * note: This module has a corresponding action plugin.

OPTIONS (= is mandatory):

- allow_downgrade
        Specify if the named package and version is allowed to downgrade a maybe already installed higher version of that package. Note that setting
        allow_downgrade=True can make this module behave in a non-idempotent way. The task could end up with a set of packages that does not match the complete list of
        specified packages to install (because dependencies between the downgraded package and others can cause changes to the packages which were in the earlier
        transaction).
        [Default: no]
        type: bool
        version_added: 2.4

- autoremove
        If `yes', removes all "leaf" packages from the system that were originally installed as dependencies of user-installed packages but which are no longer required
        by any such package. Should be used alone or when state is `absent'
        NOTE: This feature requires yum >= 3.4.3 (RHEL/CentOS 7+)
        [Default: no]
        type: bool
        version_added: 2.7

- bugfix
        If set to `yes', and `state=latest' then only installs updates that have been marked bugfix related.
        [Default: no]
        version_added: 2.6

- conf_file
        The remote yum configuration file to use for the transaction.
        [Default: (null)]
        version_added: 0.6

- disable_excludes
        Disable the excludes defined in YUM config files.
        If set to `all', disables all excludes.
        If set to `main', disable excludes defined in [main] in yum.conf.
        If set to `repoid', disable excludes defined for given repo id.
        [Default: (null)]
        version_added: 2.7

- disable_gpg_check
        Whether to disable the GPG checking of signatures of packages being installed. Has an effect only if state is `present' or `latest'.
        [Default: no]
        type: bool
        version_added: 1.2

- disable_plugin
        `Plugin' name to disable for the install/update operation. The disabled plugins will not persist beyond the transaction.
        [Default: (null)]
        version_added: 2.5

- disablerepo
        `Repoid' of repositories to disable for the install/update operation. These repos will not persist beyond the transaction. When specifying multiple repos,
        separate them with a `","'.
        As of Ansible 2.7, this can alternatively be a list instead of `","' separated string
        [Default: (null)]
```

The **ansible-doc** command also offers the`-s`option, which produces example output that can serve as a model for how to use a particular module in a playbook. This output can serve as a starter template, which can be included in a playbook to implement the module for task execution. Comments are included in the output to remind administrators of the use of each option. The following example shows this output for the`yum`module.

```
[student@workstation ~]$ansible-doc -s yum
- name: Manages packages with the `yum' package manager
  yum:
      allow_downgrade:       # Specify if the named package ...
      autoremove:            # If `yes', removes all "leaf" packages ...
      bugfix:                # If set to `yes', ...
      conf_file:             # The remote yum configuration file ...
      disable_excludes:      # Disable the excludes ...
      disable_gpg_check:     # Whether to disable the GPG ...
      disable_plugin:        # `Plugin' name to disable ...
      disablerepo:           # `Repoid' of repositories ...
      download_only:         # Only download the packages, ...
      enable_plugin:         # `Plugin' name to enable ...
      enablerepo:            # `Repoid' of repositories to enable ...
      exclude:               # Package name(s) to exclude ...
      installroot:           # Specifies an alternative installroot, ...
      list:                  # Package name to run ...
      name:                  # A package name or package specifier ...
      releasever:            # Specifies an alternative release ...
      security:              # If set to `yes', ...
      skip_broken:           # Skip packages with ...
      state:                 # Whether to install ... or remove ... a package.
      update_cache:          # Force yum to check if cache ...
      update_only:           # When using latest, only update ...
      use_backend:           # This module supports `yum' ...
      validate_certs:        # This only applies if using a https url ...
```

**Module Maintenance**

Ansible ships with a large number of modules that can be used for many tasks. The upstream community is very active, and these modules may be in different stages of development. The **ansible-doc** documentation for the module is expected to specify who maintains that module in the upstream Ansible community, and what its development status is. This is indicated in the`METADATA`section at the end of the output of **ansible-doc** for that module.

The`status`field records the development status of the module:

- `stableinterface`: The module's keywords are stable, and every effort will be made not to remove keywords or change their meaning.
- `preview`: The module is in technology preview, and might be unstable, its keywords might change, or it might require libraries or web services that are themselves subject to incompatible changes.
- `deprecated`: The module is deprecated, and will no longer be available in some future release.
- `removed`: The module has been removed from the release, but a stub exists for documentation purposes to help former users migrate to new modules.

**NOTE**

The`stableinterface`status only indicates that a module's interface is stable, it does not rate the module's code quality.

The`supported_by`field records who maintains the module in the upstream Ansible community. Possible values are:

- `core`: Maintained by the "core" Ansible developers upstream, and always included with Ansible.
- `curated`: Modules submitted and maintained by partners or companies in the community. Maintainers of these modules must watch for any issues reported or pull requests raised against the module. Upstream "core" developers review proposed changes to curated modules after the community maintainers have approved the changes. Core committers also ensure that any issues with these modules due to changes in the Ansible engine are remediated. These modules are currently included with Ansible, but might be packaged separately at some point in the future.
- `community`: Modules not supported by the core upstream developers, partners, or companies, but maintained entirely by the general open source community. Modules in this category are still fully usable, but the response rate to issues is purely up to the community. These modules are also currently included with Ansible, but will probably be packaged separately at some point in the future.

The upstream Ansible community has an issue tracker for Ansible and its integrated modules at[ https://github.com/ansible/ansible/issues ](https://github.com/ansible/ansible/issues).

Sometimes, a module does not exist for something you want to do. As an end user, you can also write your own private modules, or get modules from a third party. Ansible searches for custom modules in the location specified by the`ANSIBLE_LIBRARY`environment variable, or if that is not set, by a`library`keyword in the current Ansible configuration file. Ansible also searches for custom modules in the`./library`directory relative to the playbook currently being run.

```
library = /usr/share/my_modules
```

Information on writing modules is beyond the scope of this course. Documentation on how to do this is available at[ https://docs.ansible.com/ansible/latest/dev_guide/developing_modules.html ](https://docs.ansible.com/ansible/latest/dev_guide/developing_modules.html).

**IMPORTANT**

Use the **ansible-doc** command to find and learn how to use modules for your tasks.

When possible, try to avoid the`command`,`shell`, and`raw`modules in playbooks, even though they might seem simple to use. Because these take arbitrary commands, it is very easy to write non-idempotent playbooks with these modules.

For example, the following task using the`shell`module is not idempotent. Every time the play is run, it rewrites`/etc/resolv.conf`even if it already consists of the line`nameserver 192.0.2.1`.

```
- name: Non-idempotent approach with shell module
  shell: echo "nameserver 192.0.2.1" > /etc/resolv.conf
```

There are several ways to write tasks using the`shell`module idempotently, and sometimes making those changes and using`shell`is the best approach. A quicker solution may be to use **ansible-doc** to discover the`copy`module and use that to get the desired effect.

The following example does not rewrite the`/etc/resolv.conf`file if it already consists of the correct content:

```
- name: Idempotent approach with copy module
  copy:
    dest: /etc/resolv.conf
    content: "nameserver 192.0.2.1\n"
```

The`copy`module tests to see if the state has already been met, and if so, it makes no changes. The`shell`module allows a lot of flexibility, but also requires more attention to ensure that it runs idempotently.

Idempotent playbooks can be run repeatedly to ensure systems are in a particular state without disrupting those systems if they already are.

#### Playbook Syntax Variations

The last part of this chapter investigates some variations of YAML or Ansible Playbook syntax that you might encounter.

**YAML Comments**

Comments can also be used to aid readability. In YAML, everything to the right of the number or hash symbol (#) is a comment. If there is content to the left of the comment, precede the number symbol with a space.

```
# This is a YAML comment
some data # This is also a YAML comment
```

**YAML Strings**

Strings in YAML do not normally need to be put in quotation marks even if there are spaces contained in the string. You can enclose strings in either double quotes or single quotes.

```
this is a string
'this is another string'
"this is yet another a string"
```

There are two ways to write multiline strings. You can use the vertical bar (|) character to denote that newline characters within the string are to be preserved.

```
include_newlines: |
        Example Company
        123 Main Street
        Atlanta, GA 30303
```

You can also write multiline strings using the greater-than (>) character to indicate that newline characters are to be converted to spaces and that leading white spaces in the lines are to be removed. This method is often used to break long strings at space characters so that they can span multiple lines for better readability.

```
fold_newlines: >
        This is an example
        of a long string,
        that will become
        a single sentence once folded.
```

**YAML Dictionaries**

You have seen collections of key-value pairs written as an indented block, as follows:

```
  name: svcrole
  svcservice: httpd
  svcport: 80
```

Dictionaries can also be written in an inline block format enclosed in curly braces, as follows:

```
  {name: svcrole, svcservice: httpd, svcport: 80}
  
```

In most cases the inline block format should be avoided because it is harder to read. However, there is at least one situation in which it is more commonly used. The use of *roles* is discussed later in this course. When a playbook includes a list of roles, it is more common to use this syntax to make it easier to distinguish roles included in a play from the variables being passed to a role.

**YAML Lists**

You have also seen lists written with the normal single-dash syntax:

```
  hosts:
    - servera
    - serverb
    - serverc
```

Lists also have an inline format enclosed in square braces, as follows:

```
hosts: [servera, serverb, serverc]
```

You should avoid this syntax because it is usually harder to read.

**Obsolete key=value Playbook Shorthand**

Some playbooks might use an older shorthand method to define tasks by putting the key-value pairs for the module on the same line as the module name. For example, you might see this syntax:

```
  tasks:
    - name: shorthand form
      service: name=httpd enabled=true state=started
```

Normally you would write the same task as follows:

```
  tasks:
    - name: normal form
      service:
        name: httpd
        enabled: true
        state: started
```

You should generally avoid the shorthand form and use the normal form.

The normal form has more lines, but it is easier to work with. The task's keywords are stacked vertically and easier to differentiate. Your eyes can run straight down the play with less left-to-right motion. Also, the normal syntax is native YAML; the shorthand form is not. Syntax highlighting tools in modern text editors can help you more effectively if you use the normal format than if you use the shorthand format.

You might see this syntax in documentation and older playbooks from other people, and the syntax does still function.

**REFERENCES**

**ansible-playbook** (1) and **ansible-doc** (1) man pages

[*Intro to Playbooks — Ansible Documentation*](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html)

[*Playbooks — Ansible Documentation*](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html)

[*Developing Modules — Ansible Documentation*](https://docs.ansible.com/ansible/latest/dev_guide/developing_modules.html)

[*Module Support — Ansible Documentation*](https://docs.ansible.com/ansible/latest/user_guide/modules_support.html)

[*YAML Syntax — Ansible Documentation*](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html)



------

###  Guided Exercise: Implementing Multiple Plays

In this exercise, you will create a playbook containing multiple plays, then use it to perform configuration tasks on managed hosts.

**Outcomes**

You should be able to construct and execute a playbook to manage configuration and perform administration of a managed host.

Log in to`workstation`as`student`using`student`as the password.

On`workstation`, run the **lab playbook-multi start** command. This function ensures that the managed host,`servera.lab.example.com`, is reachable on the network. It also ensures that the correct Ansible configuration file and inventory file are installed on the control node.

```
[student@workstation ~]$lab playbook-multi start
```

1. A working directory,`/home/student/playbook-multi`, has been created on`workstation`for the Ansible project. The directory has already been populated with an`ansible.cfg`configuration file and an inventory file,`inventory`. The managed host,`servera.lab.example.com`, is already defined in this inventory file. Create a new playbook,`/home/student/playbook-multi/intranet.yml`, and add the lines needed to start the first play. It should target the managed host`servera.lab.example.com`and enable privilege escalation.

   1. Change directory into the`/home/student/playbook-multi`working directory.

      ```
      [student@workstation ~]$cd ~/playbook-multi[student@workstation playbook-multi]$
      ```

   2. Create and open a new playbook,`/home/student/playbook-multi/intranet.yml`, and add a line consisting of three dashes to the beginning of the file to indicate the start of the YAML file.

      ```
      ---
      ```

   3. Add the following line to the`/home/student/playbook-multi/intranet.yml`file to denote the start of a play with a name of`Enable intranet services`.

      ```
      - name: Enable intranet services
      ```

   4. Add the following line to indicate that the play applies to the`servera.lab.example.com`managed host. Be sure to indent the line with two spaces (aligning with the`name`keyword above it) to indicate that it is part of the first play.

      ```
        hosts: servera.lab.example.com
      ```

   5. Add the following line to enable privilege escalation. Be sure to indent the line with two spaces (aligning with the keywords above it) to indicate it is part of the first play.

      ```
        become: yes
      ```

   6. Add the following line to define the beginning of the`tasks`list. Indent the line with two spaces (aligning with the keywords above it) to indicate that it is part of the first play.

      ```
        tasks:
      ```

2. As the first task in the first play, define a task that ensures that the httpd and firewalld packages are up to date.

   Be sure to indent the first line of the task with four spaces. Under the`tasks`keyword in the first play, add the following lines.

   ```
       - name: latest version of httpd and firewalld installed
         yum:
           name:
             - httpd
             - firewalld
           state: latest
   ```

   The first line provides a descriptive name for the task. The second line is indented with six spaces and calls the`yum`module. The next line is indented eight spaces and is a`name`keyword. It specifies which packages the`yum`module should ensure are up-to-date. The`yum`module's`name`keyword (which is different from the task name) can take a list of packages, which is indented ten spaces on the two following lines. After the list, the 8-space indented`state`keyword specifies that the`yum`module should ensure that the latest version of the packages is installed.

3. Add a task to the first play's list that ensures that the correct content is in`/var/www/html/index.html`.

   Add the following lines to define the content for`/var/www/html/index.html`. Be sure to indent the first line with four spaces.

   ```
       - name: test html page is installed
         copy:
           content: "Welcome to the example.com intranet!\n"
           dest: /var/www/html/index.html
   ```

   The first entry provides a descriptive name for the task. The second entry is indented with six spaces and calls the`copy`module. The remaining entries are indented with eight spaces and pass the necessary arguments to ensure that the correct content is in the web page.

4. Define two more tasks in the play to ensure that the`firewalld`service is running and will start on boot, and will allow connections to the`httpd`service.

   1. Add the following lines to ensure that the`firewalld`service is enabled and running. Be sure to indent the first line with four spaces.

      ```
          - name: firewalld enabled and running
            service:
              name: firewalld
              enabled: true
              state: started
      ```

      The first entry provides a descriptive name for the task. The second entry is indented with eight spaces and calls the`service`module. The remaining entries are indented with ten spaces and pass the necessary arguments to ensure that the firewalld service is enabled and started.

   2. Add the following lines to ensure that`firewalld`allows HTTP connections from remote systems. Be sure to indent the first line with four spaces.

      ```
          - name: firewalld permits access to httpd service
            firewalld:
              service: http
              permanent: true
              state: enabled
              immediate: yes
      ```

      The first entry provides a descriptive name for the task. The second entry is indented with six spaces and calls the`firewalld`module. The remaining entries are indented with eight spaces and pass the necessary arguments to ensure that remote HTTP connections are permanently allowed.

5. Add a final task to the first play that ensures that the`httpd`service is running and will start at boot.

   Add the following lines to ensure that the`httpd`service is enabled and running. Be sure to indent the first line with four spaces.

   ```
       - name: httpd enabled and running
         service:
           name: httpd
           enabled: true
           state: started
   ```

   The first entry provides a descriptive name for the task. The second entry is indented with six spaces and calls the`service`module. The remaining entries are indented with eight spaces and pass the necessary arguments to ensure that the`httpd`service is enabled and running.

6. In`/home/student/playbook-multi/intranet.yml`, define a second play targeted at`localhost`which will test the intranet web server. It does not need privilege escalation.

   1. Add the following line to define the start of a second play. Note that there is no indentation.

      ```
      - name: Test intranet web server
      ```

   2. Add the following line to indicate that the play applies to the`localhost`managed host. Be sure to indent the line with two spaces to indicate that it is contained by the second play.

      ```
        hosts: localhost
      ```

   3. Add the following line to disable privilege escalation. Be sure to align the indentation with the`hosts`keyword above it.

      ```
        become: no
      ```

   4. Add the following line to the`/home/student/playbook-multi/intranet.yml`file to define the beginning of the`tasks`list. Be sure to indent the line with two spaces to indicate that it is contained by the second play.

      ```
        tasks:
      ```

7. Add a single task to the second play, and use the`uri`module to request content from`http://servera.lab.example.com`. The task should verify a return HTTP status code of`200`. Configure the task to place the returned content in the task results variable.

   Add the following lines to create the task for verifying the web service from the control node. Be sure to indent the first line with four spaces.

   ```
       - name: connect to intranet web server
         uri:
           url: http://servera.lab.example.com
           return_content: yes
           status_code: 200
   ```

   The first line provides a descriptive name for the task. The second line is indented with six spaces and calls the`uri`module. The remaining lines are indented with eight spaces and pass the necessary arguments to execute a query for web content from the control node to the managed host and verify the status code received. The`return_content`keyword ensures that the server's response is added to the task results.

8. Verify that the final`/home/student/playbook-multi/intranet.yml`playbook reflects the following structured content, then save and close the file.

   ```
   ---
   - name: Enable intranet services
     hosts: servera.lab.example.com
     become: yes
     tasks:
       - name: latest version of httpd and firewalld installed
         yum:
           name:
             - httpd
             - firewalld
           state: latest
   
       - name: test html page is installed
         copy:
           content: "Welcome to the example.com intranet!\n"
           dest: /var/www/html/index.html
   
       - name: firewalld enabled and running
         service:
           name: firewalld
           enabled: true
           state: started
   
       - name: firewalld permits access to httpd service
         firewalld:
           service: http
           permanent: true
           state: enabled
           immediate: yes
   
       - name: httpd enabled and running
         service:
           name: httpd
           enabled: true
           state: started
   
   - name: Test intranet web server
     hosts: localhost
     become: no
     tasks:
       - name: connect to intranet web server
         uri:
           url: http://servera.lab.example.com
           return_content: yes
           status_code: 200
   ```

9. Run the **ansible-playbook --syntax-check** command to verify the syntax of the`/home/student/playbook-multi/intranet.yml`playbook.

   ```
   [student@workstation playbook-multi]$ansible-playbook --syntax-check intranet.yml
   
   playbook: intranet.yml
   ```

10. Execute the playbook using the`-v`option to output detailed results for each task. Read through the output generated to ensure that all tasks completed successfully. Verify that an HTTP GET request to`http://servera.lab.example.com`provides the correct content.

    ```
    [student@workstation playbook-multi]$ansible-playbook -v intranet.yml...output omitted...
    
    PLAY [Enable intranet services] *************************************************
    
    TASK [Gathering Facts] **********************************************************
    ok: [servera.lab.example.com]
    
    TASK [latest version of httpd and firewalld installed] **************************
    changed: [servera.lab.example.com] => {"changed": true, ...output omitted...
    
    TASK [test html page is installed] **********************************************
    changed: [servera.lab.example.com] => {"changed": true, ...output omitted...
    
    TASK [firewalld enabled and running] ********************************************
    ok: [servera.lab.example.com] => {"changed": false, ...output omitted...
    
    TASK [firewalld permits http service] *******************************************
    changed: [servera.lab.example.com] => {"changed": true, ...output omitted...
    
    TASK [httpd enabled and running] ************************************************
    changed: [servera.lab.example.com] => {"changed": true, ...output omitted...
    
    PLAY [Test intranet web server] *************************************************
    
    TASK [Gathering Facts] **********************************************************
    ok: [localhost]
    
    TASK [connect to intranet web server] *******************************************
    ok: [localhost] => {"accept_ranges": "bytes", "changed": false, "connection": "cl
    ose", "content": "Welcome to the example.com intranet!\n", "content_length":
     "37", "content_type": "text/html; charset=UTF-8", "cookies": {}, "cookies_string
    ": "", "date": "...output omitted...", "etag": "\"25-5790ddbcc5a48\"",
     "last_modified": "...output omitted...", "msg": "OK (37 bytes)", "redir
    ected": false, "server": "Apache/2.4.6 (Red Hat Enterprise Linux)",
    "status": 200, "url": "http://servera.lab.example.com"}
    
    PLAY RECAP **********************************************************************
    localhost                  : ok=2    changed=0    unreachable=0    failed=0    
    servera.lab.example.com    : ok=6    changed=4    unreachable=0    failed=0
    ```

**Finish**

On`workstation`, run the **lab playbook-multi finish** command to clean up the resources created in this exercise.

```
[student@workstation ~]$lab playbook-multi finish
```

This concludes the guided exercise.



------

###  Lab: Implementing Playbooks

**Performance Checklist**

In this lab, you will configure and perform administrative tasks on managed hosts using a playbook.

**Outcomes**

You should be able to construct and execute a playbook to install, configure, and verify the status of web and database services on a managed host.

Log in to`workstation`as`student`using`student`as the password.

On`workstation`, run the **lab playbook-review start** command. This function ensures that the managed host,`serverb.lab.example.com`, is reachable on the network. It also ensures that the correct Ansible configuration file and inventory file are installed on the control node.

```
[student@workstation ~]$lab playbook-review start
```

A working directory,`/home/student/playbook-review`, has been created on`workstation`for the Ansible project. The directory has already been populated with an`ansible.cfg`configuration file and an`inventory`file. The managed host,`serverb.lab.example.com`, is already defined in this inventory file.

**NOTE**

The playbook used by this lab is very similar to the one you wrote in the preceding guided exercise in this chapter. If you do not want to create this lab's playbook from scratch, you can use that exercise's playbook as a starting point for this lab.

If you do, be careful to target the correct hosts and change the tasks to match the instructions for this exercise.

1. Create a new playbook,`/home/student/playbook-review/internet.yml`, and add the necessary entries to start a first play named`Enable internet services`and specify its intended managed host,`serverb.lab.example.com`. Add an entry to enable privilege escalation, and one to start a task list.

   1. Add the following entry to the beginning of`/home/student/playbook-review/internet.yml`to begin the YAML format.

      ```
      ---
      ```

   2. Add the following entry to denote the start of a play with a name of`Enable internet services`.

      ```
      - name: Enable internet services
      ```

   3. Add the following entry to indicate that the play applies to the`serverb`managed host.

      ```
        hosts: serverb.lab.example.com
      ```

   4. Add the following entry to enable privilege escalation.

      ```
        become: yes
      ```

   5. Add the following entry to define the beginning of the`tasks`list.

      ```
        tasks:
      ```

   

2. Add the required entries to the`/home/student/playbook-review/internet.yml`file to define a task that installs the latest versions of firewalld , httpd , mariadb-server , php , and php-mysqlnd packages.

   ```
       - name: latest version of all required packages installed
         yum:
           name:
             - firewalld
             - httpd
             - mariadb-server
             - php
             - php-mysqlnd
           state: latest
   ```

   

3. Add the necessary entries to the`/home/student/playbook-review/internet.yml`file to define the firewall configuration tasks. They should ensure that the`firewalld`service is`enabled`and`running`, and that access is allowed to the`httpd`service.

   ```
       - name: firewalld enabled and running
         service:
           name: firewalld
           enabled: true
           state: started
   
       - name: firewalld permits http service
         firewalld:
           service: http
           permanent: true
           state: enabled
           immediate: yes
   ```

   

4. Add the necessary tasks to ensure the`httpd`and`mariadb`services are`enabled`and`running`.

   ```
       - name: httpd enabled and running
         service:
           name: httpd
           enabled: true
           state: started
   
       - name: mariadb enabled and running
         service:
           name: mariadb
           enabled: true
           state: started
   ```

   

5. Add the necessary entries to define the final task for generating web content for testing. Use the`get_url`module to copy`http://materials.example.com/labs/playbook-review/index.php`to`/var/www/html/`on the managed host.

   ```
       - name: test php page is installed
         get_url:
           url: "http://materials.example.com/labs/playbook-review/index.php"
           dest: /var/www/html/index.php
           mode: 0644
   ```

   

6. In`/home/student/playbook-review/internet.yml`, define another play for the task to be performed on the control node. This play will test access to the web server that should be running on the`serverb`managed host. This play does not require privilege escalation, and will run on the`localhost`managed host.

   1. Add the following entry to denote the start of a second play with a name of`Test internet web server`.

      ```
      - name: Test internet web server
      ```

   2. Add the following entry to indicate that the play applies to the`localhost`managed host.

      ```
        hosts: localhost
      ```

   3. Add the following line after the`hosts`keyword to disable privilege escalation for the second play.

      ```
        become: no
      ```

   4. Add an entry to the`/home/student/playbook-review/internet.yml`file to define the beginning of the`tasks`list.

      ```
        tasks:
      ```

   

7. Add a task that tests the web service running on`serverb`from the control node using the`uri`module. Check for a return status code of`200`.

   ```
       - name: connect to internet web server
         uri:
           url: http://serverb.lab.example.com
           status_code: 200
   ```

   

8. Verify the syntax of the`internet.yml`playbook.

   ```
   [student@workstation playbook-review]$ansible-playbook --syntax-check \>internet.yml
   
   playbook: internet.yml
   ```

   

9. Use the **ansible-playbook** command to run the playbook. Read through the output generated to ensure that all tasks completed successfully.

   ```
   [student@workstation playbook-review]$ansible-playbook internet.yml
   PLAY [Enable internet services] ************************************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [serverb.lab.example.com]
   
   TASK [latest version of all required packages installed] ***********************
   changed: [serverb.lab.example.com]
   
   TASK [firewalld enabled and running] *******************************************
   ok: [serverb.lab.example.com]
   
   TASK [firewalld permits http service] ******************************************
   changed: [serverb.lab.example.com]
   
   TASK [httpd enabled and running] ***********************************************
   changed: [serverb.lab.example.com]
   
   TASK [mariadb enabled and running] *********************************************
   changed: [serverb.lab.example.com]
   
   TASK [test php page installed] *************************************************
   changed: [serverb.lab.example.com]
   
   PLAY [Test internet web server] ************************************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [localhost]
   
   TASK [connect to internet web server] ******************************************
   ok: [localhost]
   
   PLAY RECAP *********************************************************************
   localhost                  : ok=2    changed=0    unreachable=0    failed=0
   serverb.lab.example.com    : ok=7    changed=5    unreachable=0    failed=0
   ```

   

**Evaluation**

Grade your work by running the **lab playbook-review grade** command from your`workstation`machine. Correct any reported failures and rerun the script until successful.

```
[student@workstation ~]$lab playbook-review grade
```

**Finish**

On`workstation`, run the **lab playbook-review finish** script to clean up the resources created in this lab.

```
[student@workstation ~]$lab playbook-review finish
```

This concludes the lab.



------

### Summary

In this chapter, you learned:

- A *play* is an ordered list of tasks, which runs against hosts selected from the inventory.
- A *playbook* is a text file that contains a list of one or more plays to run in order.
- Ansible Playbooks are written in YAML format.
- YAML files are structured using space indentation to represent the data hierarchy.
- Tasks are implemented using standardized code packaged as Ansible *modules* .
- The **ansible-doc** command can list installed modules, and provide documentation and example code snippets of how to use them in playbooks.
- The **ansible-playbook** command is used to verify playbook syntax and run playbooks.



## Chapter 4. Managing Variables and Facts

- [Managing Variables](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch04)
- [Guided Exercise: Managing Variables](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch04s02)
- [Managing Secrets](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch04s03)
- [Guided Exercise: Managing Secrets](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch04s04)
- [Managing Facts](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch04s05)
- [Guided Exercise: Managing Facts](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch04s06)
- [Lab: Managing Variables and Facts](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch04s07)
- [Summary](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch04s08)



**Abstract**

| **Goal**       | Write playbooks that use variables to simplify management of the playbook, and facts to reference information about the managed hosts. |
| -------------- | ------------------------------------------------------------ |
| **Objectives** | Create and reference variables that affect particular hosts or host groups, the play, or the global environment, and describe how variable precedence works.Encrypt sensitive variables using Ansible Vault, and run playbooks that reference Vault-encrypted variable files.Reference data about managed hosts using Ansible facts, and configure custom facts on managed hosts. |
| **Sections**   | Managing Variables (and Guided Exercise)Managing Secrets (and Guided Exercise)Managing Facts (and Guided Exercise) |
| **Lab**        | Managing Variables and Facts                                 |



### Managing Variables

#### Objectives

After completing this section, you should be able to create and reference variables in playbooks, affecting particular hosts or host groups, the play, or the global environment, and to describe how variable precedence works.

#### Introduction to Ansible Variables

Ansible supports variables that can be used to store values that can then be reused throughout files in an Ansible project. This can simplify the creation and maintenance of a project and reduce the number of errors.

Variables provide a convenient way to manage dynamic values for a given environment in your Ansible project. Examples of values that variables might contain include:

- Users to create
- Packages to install
- Services to restart
- Files to remove
- Archives to retrieve from the internet

#### Naming Variables

Variable names must start with a letter, and they can only contain letters, numbers, and underscores.

The following table illustrates the difference between invalid and valid variable names.



**Table 4.1. Examples of Invalid and Valid Ansible Variable Names**

| Invalid variable names | Valid variable names              |
| :--------------------- | :-------------------------------- |
| `web server`           | `web_server`                      |
| `remote.file`          | `remote_file`                     |
| `1st file`             | `file_1``file1`                   |
| `remoteserver$1`       | `remote_server_1``remote_server1` |



#### Defining Variables

Variables can be defined in a variety of places in an Ansible project. However, this can be simplified to three basic scope levels:

- *Global scope*: Variables set from the command line or Ansible configuration
- *Play scope*: Variables set in the play and related structures
- *Host scope*: Variables set on host groups and individual hosts by the inventory, fact gathering, or registered tasks

If the same variable name is defined at more than one level, the level with the highest precedence wins. A narrow scope takes precedence over a wider scope: variables defined by the inventory are overridden by variables defined by the playbook, which are overridden by variables defined on the command line.

A detailed discussion of variable precedence is available in the Ansible documentation, a link to which is provided in the References at the end of this section.

#### Variables in Playbooks

Variables play an important role in Ansible Playbooks because they ease the management of variable data in a playbook.

**Defining Variables in Playbooks**

When writing playbooks, you can define your own variables and then invoke those values in a task. For example, a variable named `web_package` can be defined with a value of `httpd`. A task can then call the variable using the `yum` module to install the httpd package.

Playbook variables can be defined in multiple ways. One common method is to place a variable in a `vars` block at the beginning of a playbook:

```
- hosts: all
  vars:
    user: joe
    home: /home/joe
```

It is also possible to define playbook variables in external files. In this case, instead of using a `vars` block in the playbook, the `vars_files` directive may be used, followed by a list of names for external variable files relative to the location of the playbook:

```
- hosts: all
  vars_files:
    - vars/users.yml
```

The playbook variables are then defined in that file or those files in YAML format:

```
user: joe
home: /home/joe
```

**Using Variables in Playbooks**

After variables have been declared, administrators can use the variables in tasks. Variables are referenced by placing the variable name in double curly braces ({{ }}). Ansible substitutes the variable with its value when the task is executed.

```
vars:
  user: joe

tasks:
  # This line will read: Creates the user joe
  - name: Creates the user {{ user }}
    user:
      # This line will create the user named Joe
      name: "{{ user }}"
```

**IMPORTANT**

When a variable is used as the first element to start a value, quotes are mandatory. This prevents Ansible from interpreting the variable reference as starting a YAML dictionary. The following message appears if quotes are missing:

```
yum:
     name: {{ service }}
            ^ here
We could be wrong, but this one looks like it might be an issue with
missing quotes.  Always quote template expression brackets when they
start a value. For instance:

    with_items:
      - {{ foo }}

Should be written as:

    with_items:
      - "{{ foo }}"
```

#### Host Variables and Group Variables

Inventory variables that apply directly to hosts fall into two broad categories: *host variables* apply to a specific host, and *group variables* apply to all hosts in a host group or in a group of host groups. Host variables take precedence over group variables, but variables defined by a playbook take precedence over both.

One way to define host variables and group variables is to do it directly in the inventory file. This is an older approach and not preferred, but you may still encounter it.

- Defining the `ansible_user` host variable for `demo.example.com`:

  ```
  [servers]
  demo.example.com  ansible_user=joe
  ```

- Defining the `user` group variable for the `servers` host group.

  ```
  [servers]
  demo1.example.com
  demo2.example.com
  
  [servers:vars]
  user=joe
  ```

- Defining the `user` group variable for the `servers` group, which consists of two host groups each with two servers.

  ```
  [servers1]
  demo1.example.com
  demo2.example.com
  
  [servers2]
  demo3.example.com
  demo4.example.com
  
  [servers:children]
  servers1
  servers2
  
  [servers:vars]
  user=joe
  ```

Some disadvantages of this approach are that it makes the inventory file more difficult to work with, it mixes information about hosts and variables in the same file, and uses an obsolete syntax.

**Using Directories to Populate Host and Group Variables**

The preferred approach to defining variables for hosts and host groups is to create two directories, `group_vars` and `host_vars`, in the same working directory as the inventory file or directory. These directories contain files defining group variables and host variables, respectively.

**IMPORTANT**

The recommended practice is to define inventory variables using `host_vars` and `group_vars` directories, and not to define them directly in the inventory files.

To define group variables for the `servers` group, you would create a YAML file named `group_vars/servers`, and then the contents of that file would set variables to values using the same syntax as in a playbook:

```
user: joe
```

Likewise, to define host variables for a particular host, create a file with a name matching the host in the `host_vars` directory to contain the host variables.

The following examples illustrate this approach in more detail. Consider a scenario where there are two data centers to manage and the data center hosts are defined in the `~/project/inventory` inventory file:

```
[admin@station project]$ cat ~/project/inventory
[datacenter1]
demo1.example.com
demo2.example.com

[datacenter2]
demo3.example.com
demo4.example.com

[datacenters:children]
datacenter1
datacenter2
```

- If you need to define a general value for all servers in both data centers, set a group variable for the `datacenters` host group:

  ```
  [admin@station project]$ cat ~/project/group_vars/datacenters
  package: httpd
  ```

- If the value to define varies for each data center, set a group variable for each data center host group:

  ```
  [admin@station project]$ cat ~/project/group_vars/datacenter1
  package: httpd
  [admin@station project]$ cat ~/project/group_vars/datacenter2
  package: apache
  ```

- If the value to be defined varies for each host in every data center, then define the variables in separate host variable files:

  ```
  [admin@station project]$ cat ~/project/host_vars/demo1.example.com
  package: httpd
  [admin@station project]$ cat ~/project/host_vars/demo2.example.com
  package: apache
  [admin@station project]$ cat ~/project/host_vars/demo3.example.com
  package: mariadb-server
  [admin@station project]$ cat ~/project/host_vars/demo4.example.com
  package: mysql-server
  ```

The directory structure for the example project, `project`, if it contained all the example files above, would appear as follows:

```
project
├── ansible.cfg
├── group_vars
│   ├── datacenters
│   ├── datacenters1
│   └── datacenters2
├── host_vars
│   ├── demo1.example.com
│   ├── demo2.example.com
│   ├── demo3.example.com
│   └── demo4.example.com
├── inventory
└── playbook.yml
```

#### Overriding Variables from the Command Line

Inventory variables are overridden by variables set in a playbook, but both kinds of variables may be overridden through arguments passed to the **ansible** or **ansible-playbook** commands on the command line. Variables set on the command line are called *extra variables*.

Extra variables can be useful when you need to override the defined value for a variable for a one-off run of a playbook. For example:

```
[user@demo ~]$ ansible-playbook main.yml -e "package=apache"
```

#### Using Arrays as Variables

Instead of assigning configuration data that relates to the same element (a list of packages, a list of services, a list of users, and so on), to multiple variables, administrators can use *arrays*. One consequence of this is that an array can be browsed.

For example, consider the following snippet:

```
user1_first_name: Bob
user1_last_name: Jones
user1_home_dir: /users/bjones
user2_first_name: Anne
user2_last_name: Cook
user2_home_dir: /users/acook
```

This could be rewritten as an array called `users`:

```
users:
  bjones:
    first_name: Bob
    last_name: Jones
    home_dir: /users/bjones
  acook:
    first_name: Anne
    last_name: Cook
    home_dir: /users/acook
```

You can then use the following variables to access user data:

```
# Returns 'Bob'
users.bjones.first_name

# Returns '/users/acook'
users.acook.home_dir
```

Because the variable is defined as a Python *dictionary*, an alternative syntax is available.

```
# Returns 'Bob'
users['bjones']['first_name']

# Returns '/users/acook'
users['acook']['home_dir']
```

**IMPORTANT**

The dot notation can cause problems if the key names are the same as names of Python methods or attributes, such as `discard`, `copy`, `add`, and so on. Using the brackets notation can help avoid conflicts and errors.

Both syntaxes are valid, but to make troubleshooting easier, Red Hat recommends that you use one syntax consistently in all files throughout any given Ansible project.

#### Capturing Command Output with Registered Variables

Administrators can use the `register` statement to capture the output of a command. The output is saved into a temporary variable that can be used later in the playbook for either debugging purposes or to achieve something else, such as a particular configuration based on a command's output.

The following playbook demonstrates how to capture the output of a command for debugging purposes:

```
---
- name: Installs a package and prints the result
  hosts: all
  tasks:
    - name: Install the package
      yum:
        name: httpd
        state: installed
      register: install_result

    - debug: var=install_result
```

When you run the playbook, the `debug` module is used to dump the value of the `install_result` registered variable to the terminal.

```
[user@demo ~]$ ansible-playbook playbook.yml
PLAY [Installs a package and prints the result] ****************************

TASK [setup] ***************************************************************
ok: [demo.example.com]

TASK [Install the package] *************************************************
ok: [demo.example.com]

TASK [debug] ***************************************************************
ok: [demo.example.com] => {
    "install_result": {
        "changed": false,
        "msg": "",
        "rc": 0,
        "results": [
            "httpd-2.4.6-40.el7.x86_64 providing httpd is already installed"
        ]
    }
}

PLAY RECAP *****************************************************************
demo.example.com    : ok=3    changed=0    unreachable=0    failed=0
```

**REFERENCES**

[*Inventory — Ansible Documentation*](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)

[*Variables — Ansible Documentation*](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html)

[*Variable Precedence: Where Should I Put A Variable?*](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable)

[*YAML Syntax — Ansible Documentation*](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html)



------

###  Guided Exercise: Managing Variables

In this exercise, you will define and use variables in a playbook.

**Outcomes**

You should be able to:



- Define variables in a playbook.
- Create tasks that use defined variables.



Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab data-variables start** command. This function creates the `data-variables` working directory, and populates it with an Ansible configuration file and host inventory.

```
[student@workstation ~]$ lab data-variables start
```

1. On `workstation`, as the `student` user, change into the `/home/student/data-variables` directory.

   ```
   [student@workstation ~]$ cd ~/data-variables
   [student@workstation data-variables]$ 
   ```

2. Over the next several steps, you will create a playbook that installs the Apache web server and opens the ports for the service to be reachable. The playbook queries the web server to ensure it is up and running.

   Create the `playbook.yml` playbook and define the following variables in the `vars` section:

   

   **Table 4.2. Variables**

   | Variable           | Description                            |
   | :----------------- | :------------------------------------- |
   | `web_pkg`          | Web server package to install.         |
   | `firewall_pkg`     | Firewall package to install.           |
   | `web_service`      | Web service to manage.                 |
   | `firewall_service` | Firewall service to manage.            |
   | `python_pkg`       | Required package for the `uri` module. |
   | `rule`             | The service name to open.              |

   ```
   ---
   - name: Deploy and start Apache HTTPD service
     hosts: webserver
     vars:
       web_pkg: httpd
       firewall_pkg: firewalld
       web_service: httpd
       firewall_service: firewalld
       python_pkg: python3-PyMySQL
       rule: http
   ```

3. Create the `tasks` block and create the first task, which should use the `yum` module to make sure the latest versions of the required packages are installed.

   ```
     tasks:
       - name: Required packages are installed and up to date
         yum:
           name:
             - "{{ web_pkg }}"
             - "{{ firewall_pkg }}"
             - "{{ python_pkg }}"
           state: latest
   ```

**NOTE**

   You can use **ansible-doc yum** to review the syntax for the `yum` module. The syntax shows that its `name` directive can take a list of packages that the module should work with, so that you do not need separate tasks to makes sure each package is up-to-date.

4. Create two tasks to make sure that the `httpd` and `firewalld` services are started and enabled.

   ```
       - name: The {{ firewall_service }} service is started and enabled
         service:
           name: "{{ firewall_service }}"
           enabled: true
           state: started
   
       - name: The {{ web_service }} service is started and enabled
         service:
           name: "{{ web_service }}"
           enabled: true
           state: started
   ```

   **NOTE**

   The `service` module works differently from the `yum` module, as documented by **ansible-doc service**. Its `name` directive takes the name of exactly one service to work with.

   You can write a single task that ensures both services are started and enabled, using the `loop` keyword covered later in this course.

5. Add a task that ensures specific content exists in the `/var/www/html/index.html` file.

   ```
       - name: Web content is in place
         copy:
           content: "Example web content"
           dest: /var/www/html/index.html
   ```

6. Add a task that uses the `firewalld` module to ensure the firewall ports are open for the `firewalld` service named in the `rule` variable.

   ```
       - name: The firewall port for {{ rule }} is open
         firewalld:
           service: "{{ rule }}"
           permanent: true
           immediate: true
           state: enabled
   ```

7. Create a new play that queries the web service to ensure everything has been correctly configured. It should run on `localhost`. Because of that Ansible fact, Ansible does not have to change identity, so set the `become` module to `false`. You can use the `uri` module to check a URL. For this task, check for a status code of 200 to confirm the web server on `servera.lab.example.com` is running and correctly configured.

   ```
   - name: Verify the Apache service
     hosts: localhost
     become: false
     tasks:
       - name: Ensure the webserver is reachable
         uri:
           url: http://servera.lab.example.com
           status_code: 200
   ```

8. When completed, the playbook should appear as follows. Review the playbook and confirm that both plays are correct.

   ```
   ---
   - name: Deploy and start Apache HTTPD service
     hosts: webserver
     vars:
       web_pkg: httpd
       firewall_pkg: firewalld
       web_service: httpd
       firewall_service: firewalld
       python_pkg: python3-PyMySQL
       rule: http
   
     tasks:
       - name: Required packages are installed and up to date
         yum:
           name:
             - "{{ web_pkg  }}"
             - "{{ firewall_pkg }}"
             - "{{ python_pkg }}"
           state: latest
   
       - name: The {{ firewall_service }} service is started and enabled
         service:
           name: "{{ firewall_service }}"
           enabled: true
           state: started
   
       - name: The {{ web_service }} service is started and enabled
         service:
           name: "{{ web_service }}"
           enabled: true
           state: started
   
       - name: Web content is in place
         copy:
           content: "Example web content"
           dest: /var/www/html/index.html
   
       - name: The firewall port for {{ rule }} is open
         firewalld:
           service: "{{ rule }}"
           permanent: true
           immediate: true
           state: enabled
   
   - name: Verify the Apache service
     hosts: localhost
     become: false
     tasks:
       - name: Ensure the webserver is reachable
         uri:
           url: http://servera.lab.example.com
           status_code: 200
   ```

9. Before you run the playbook, use the **ansible-playbook --syntax-check** command to verify its syntax. If it reports any errors, correct them before moving to the next step. You should see output similar to the following:

   ```
   [student@workstation data-variables]$ ansible-playbook --syntax-check playbook.yml
   
   playbook: playbook.yml
   ```

10. Use the **ansible-playbook** command to run the playbook. Watch the output as Ansible installs the packages, starts and enables the services, and ensures the web server is reachable.

    ```
    [student@workstation data-variables]$ ansible-playbook playbook.yml
    
    PLAY [Deploy and start Apache HTTPD service] ***********************************
    
    TASK [Gathering Facts] *********************************************************
    ok: [servera.lab.example.com]
    
    TASK [Required packages are installed and up to date] **************************
    changed: [servera.lab.example.com]
    
    TASK [The firewalld service is started and enabled] ****************************
    ok: [servera.lab.example.com]
    
    TASK [The httpd service is started and enabled] ********************************
    changed: [servera.lab.example.com]
    
    TASK [Web content is in place] *************************************************
    changed: [servera.lab.example.com]
    
    TASK [The firewall port for http is open] **************************************
    changed: [servera.lab.example.com]
    
    PLAY [Verify the Apache service] ***********************************************
    
    TASK [Gathering Facts] *********************************************************
    ok: [localhost]
    
    TASK [Ensure the webserver is reachable] ***************************************
    ok: [localhost]
    
    PLAY RECAP *********************************************************************
    localhost                  : ok=2    changed=0    unreachable=0    failed=0
    servera.lab.example.com    : ok=6    changed=4    unreachable=0    failed=0
    ```

**Finish**

On `workstation`, run the **lab data-variables finish** script to clean up this exercise.

```
[student@workstation ~]$ lab data-variables finish
```

This concludes the guided exercise.



------

### Managing Secrets

#### Objectives

After completing this section, you should be able to encrypt sensitive variables using Ansible Vault, and run playbooks that reference Vault-encrypted variable files.

#### Introducing Ansible Vault

Ansible may need access to sensitive data such as passwords or API keys in order to configure managed hosts. Normally, this information might be stored as plain text in inventory variables or other Ansible files. In that case, however, any user with access to the Ansible files or a version control system which stores the Ansible files would have access to this sensitive data. This poses an obvious security risk.

Ansible Vault, which is included with Ansible, can be used to encrypt and decrypt any structured data file used by Ansible. To use Ansible Vault, a command-line tool named **ansible-vault** is used to create, edit, encrypt, decrypt, and view files. Ansible Vault can encrypt any structured data file used by Ansible. This might include inventory variables, included variable files in a playbook, variable files passed as arguments when executing the playbook, or variables defined in Ansible roles.

**IMPORTANT**

Ansible Vault does not implement its own cryptographic functions but rather uses an external Python toolkit. Files are protected with symmetric encryption using AES256 with a password as the secret key. Note that the way this is done has not been formally audited by a third party.

**Creating an Encrypted File**

To create a new encrypted file, use the **ansible-vault create filename** command. The command prompts for the new vault password and then opens a file using the default editor, **vi**. You can set and export the `EDITOR` environment variable to specify a different default editor by setting and exporting. For example, to set the default editor to **nano**, **export EDITOR=nano**.

```
[student@demo ~]$ ansible-vault create secret.yml
New Vault password: redhat
Confirm New Vault password: redhat
```

Instead of entering the vault password through standard input, you can use a vault password file to store the vault password. You need to carefully protect this file using file permissions and other means.

```
[student@demo ~]$ ansible-vault create --vault-password-file=vault-pass secret.yml
```

The cipher used to protect files is AES256 in recent versions of Ansible, but files encrypted with older versions may still use 128-bit AES.

**Viewing an Encrypted File**

You can use the **ansible-vault view filename** command to view an Ansible Vault-encrypted file without opening it for editing.

```
[student@demo ~]$ ansible-vault view secret1.yml
Vault password: secret
less 458 (POSIX regular expressions)
Copyright (C) 1984-2012 Mark Nudelman

less comes with NO WARRANTY, to the extent permitted by law.
For information about the terms of redistribution,
see the file named README in the less distribution.
Homepage: http://www.greenwoodsoftware.com/less
my_secret: "yJJvPqhsiusmmPPZdnjndkdnYNDjdj782meUZcw"
```

**Editing an Existing Encrypted File**

To edit an existing encrypted file, Ansible Vault provides the **ansible-vault edit filename** command. This command decrypts the file to a temporary file and allows you to edit it. When saved, it copies the content and removes the temporary file.

```
[student@demo ~]$ ansible-vault edit secret.yml
Vault password: redhat
```

**NOTE**

The **edit** subcommand always rewrites the file, so you should only use it when making changes. This can have implications when the file is kept under version control. You should always use the **view** subcommand to view the file's contents without making changes.

**Encrypting an Existing File**

To encrypt a file that already exists, use the **ansible-vault encrypt filename** command. This command can take the names of multiple files to be encrypted as arguments.

```
[student@demo ~]$ ansible-vault encrypt secret1.yml secret2.yml
New Vault password: redhat
Confirm New Vault password: redhat
Encryption successful
```

Use the `--output=OUTPUT_FILE` option to save the encrypted file with a new name. You can only use one input file with the `--output` option.

**Decrypting an Existing File**

An existing encrypted file can be permanently decrypted by using the **ansible-vault decrypt filename** command. When decrypting a single file, you can use the `--output` option to save the decrypted file under a different name.

```
[student@demo ~]$ ansible-vault decrypt secret1.yml --output=secret1-decrypted.yml
Vault password: redhat
Decryption successful
```

**Changing the Password of an Encrypted File**

You can use the **ansible-vault rekey filename** command to change the password of an encrypted file. This command can rekey multiple data files at once. It prompts for the original password and then the new password.

```
[student@demo ~]$ ansible-vault rekey secret.yml
Vault password: redhat
New Vault password: RedHat
Confirm New Vault password: RedHat
Rekey successful
```

When using a vault password file, use the `--new-vault-password-file` option:

```
[student@demo ~]$ ansible-vault rekey \
> --new-vault-password-file=NEW_VAULT_PASSWORD_FILE secret.yml
```

**Playbooks and Ansible Vault**

To run a playbook that accesses files encrypted with Ansible Vault, you need to provide the encryption password to the **ansible-playbook** command. If you do not provide the password, the playbook returns an error:

```
[student@demo ~]$ ansible-playbook site.yml
ERROR: A vault password must be specified to decrypt vars/api_key.yml
```

To provide the vault password to the playbook, use the `--vault-id` option. For example, to provide the vault password interactively, use `--vault-id @prompt` as illustrated in the following example:

```
[student@demo ~]$ ansible-playbook --vault-id @prompt site.yml
Vault password (default): redhat
```

**IMPORTANT**

If you are using a release of Ansible earlier than version 2.4, you need to use the `--ask-vault-pass` option to interactively provide the vault password. You can still use this option if all vault-encrypted files used by the playbook were encrypted with the same password.

```
[student@demo ~]$ ansible-playbook --ask-vault-pass site.yml
Vault password: redhat
```

Alternatively, you can use the `--vault-password-file` option to specify a file that stores the encryption password in plain text. The password should be a string stored as a single line in the file. Because that file contains the sensitive plain text password, it is vital that it be protected through file permissions and other security measures.

```
[student@demo ~]$ ansible-playbook --vault-password-file=vault-pw-file site.yml
```

You can also use the `ANSIBLE_VAULT_PASSWORD_FILE` environment variable to specify the default location of the password file.

**IMPORTANT**

Starting with Ansible 2.4, you can use multiple Ansible Vault passwords with **ansible-playbook**. To use multiple passwords, pass multiple `--vault-id` or `--vault-password-file` options to the **ansible-playbook** command.

```
[student@demo ~]$ ansible-playbook \
> --vault-id one@prompt --vault-id two@prompt site.yml
Vault password (one): 
Vault password (two): 
...output omitted...
```

The vault IDs `one` and `two` preceding `@prompt` can be anything and you can even omit them entirely. If you use the `--vault-id *id*` option when you encrypt a file with **ansible-vault** command, however, when you run **ansible-playbook** then the password for the matching ID is tried before any others. If it does not match, the other passwords you provided will be tried next. The vault ID `@prompt` with no ID is actually shorthand for `default@prompt`, which means to prompt for the password for vault ID `default`.

**Recommended Practices for Variable File Management**

To simplify management, it makes sense to set up your Ansible project so that sensitive variables and all other variables are kept in separate files. The files containing sensitive variables can then be protected with the **ansible-vault** command.

Remember that the preferred way to manage group variables and host variables is to create directories at the playbook level. The `group_vars` directory normally contains variable files with names matching host groups to which they apply. The `host_vars` directory normally contains variable files with names matching host names of managed hosts to which they apply.

However, instead of using files in `group_vars` or `host_vars`, you also can use directories for each host group or managed host. Those directories can then contain multiple variable files, all of which are used by the host group or managed host. For example, in the following project directory for `playbook.yml`, members of the `webservers` host group uses variables in the `group_vars/webservers/vars` file, and `demo.example.com` uses the variables in both `host_vars/demo.example.com/vars` and `host_vars/demo.example.com/vault`:

```
.
├── ansible.cfg
├── group_vars
│   └── webservers
│       └── vars
├── host_vars
│   └── demo.example.com
│       ├── vars
│       └── vault
├── inventory
└── playbook.yml
```

In this scenario, the advantage is that most variables for `demo.example.com` can be placed in the `vars` file, but sensitive variables can be kept secret by placing them separately in the `vault` file. Then the administrator can use **ansible-vault** to encrypt the `vault` file, while leaving the `vars` file as plain text.

There is nothing special about the file names being used in this example inside the `host_vars/demo.example.com` directory. That directory could contain more files, some encrypted by Ansible Vault and some which are not.

Playbook variables (as opposed to inventory variables) can also be protected with Ansible Vault. Sensitive playbook variables can be placed in a separate file which is encrypted with Ansible Vault and which is included in the playbook through a `vars_files` directive. This can be useful, because playbook variables take precedence over inventory variables.

If you are using multiple vault passwords with your playbook, make sure that each encrypted file is assigned a vault ID, and that you enter the matching password with that vault ID when running the playbook. This ensures that the correct password is selected first when decrypting the vault-encrypted file, which is faster than forcing Ansible to try all the vault passwords you provided until it finds the right one.

**REFERENCES**

`ansible-playbook`(1) and `ansible-vault`(1) man pages

[*Vault — Ansible Documentation*](https://docs.ansible.com/ansible/latest/user_guide/playbooks_vault.html)

[*Variables and Vaults — Ansible Documentation*](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#best-practices-for-variables-and-vaults)



------

### Guided Exercise: Managing Secrets

In this exercise, you will encrypt sensitive variables with Ansible Vault to protect them, and then run a playbook that uses those variables.

**Outcomes**

You should be able to:

- Execute a playbook using variables defined in an encrypted file.



Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab data-secret start** command. This script ensures that Ansible is installed on `workstation` and creates a working directory for this exercise. This directory includes an inventory file that points to `servera.lab.example.com` as a managed host, which is part of the `devservers` group.

```
[student@workstation ~]$ lab data-secret start
```

1. On `workstation`, as the `student` user, change to the `/home/student/data-secret` working directory.

   ```
   [student@workstation ~]$ cd ~/data-secret
   [student@workstation data-secret]$ 
   ```

2. Edit the contents of the provided encrypted file, `secret.yml`. The file can be decrypted using `redhat` as the password. Uncomment the `username` and `pwhash` variable entries.

   1. Edit the encrypted file `/home/student/data-secret/secret.yml`. Provide a password of `redhat` for the vault when prompted. The encrypted file opens in the default editor, vim.

      ```
      [student@workstation data-secret]$ ansible-vault edit secret.yml
      Vault password: redhat
      ```

   2. Uncomment the two variable entries, then save the file and exit the editor. They should appear as follows:

      ```
      username: ansibleuser1
      pwhash: $6$jf...uxhP1
      ```

3. Create a playbook named `/home/student/data-secret/create_users.yml` that uses the variables defined in the `/home/student/data-secret/secret.yml` encrypted file.

   Configure the playbook to use the `devservers` host group. Run this playbook as the `devops` user on the remote managed host. Configure the playbook to create the `ansibleuser1` user defined by the `username` variable. Set the user's password using the password hash stored in the `pwhash` variable.

   ```
   ---
   - name: create user accounts for all our servers
     hosts: devservers
     become: True
     remote_user: devops
     vars_files:
       - secret.yml
     tasks:
       - name: Creating user from secret.yml
         user:
           name: "{{ username }}"
           password: "{{ pwhash }}"
   ```

4. Use the **ansible-playbook --syntax-check** command to verify the syntax of the `create_users.yml` playbook. Use the `--ask-vault-pass` option to prompt for the vault password, which decrypts `secret.yml`. Resolve any syntax errors before you continue.

   ```
   [student@workstation data-secret]$ ansible-playbook --syntax-check \
   > --ask-vault-pass create_users.yml
   Vault password (default): redhat
   
   playbook: create_users.yml
   ```

   **NOTE**

   Instead of using `--ask-vault-pass`, you can use the newer `--vault-id @prompt` option to do the same thing.

5. Create a password file named `vault-pass` to use for the playbook execution instead of asking for a password. The file must contain the plain text `redhat` as the vault password. Change the permissions of the file to `0600`.

   ```
   [student@workstation data-secret]$ echo 'redhat' > vault-pass
   [student@workstation data-secret]$ chmod 0600 vault-pass
   ```

6. Execute the Ansible Playbook using the `vault-pass` file, to create the `ansibleuser1` user on a remote system using the passwords stored as variables in the `secret.yml` Ansible Vault encrypted file.

   ```
   [student@workstation data-secret]$ ansible-playbook \
   > --vault-password-file=vault-pass create_users.yml
   
   PLAY [create user accounts for all our servers] ********************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [servera.lab.example.com]
   
   TASK [Creating users from secret.yml] ******************************************
   changed: [servera.lab.example.com]
   
   PLAY RECAP *********************************************************************
   servera.lab.example.com    : ok=2    changed=1    unreachable=0    failed=0
   ```

7. Verify that the playbook ran correctly. The user `ansibleuser1` should exist and have the correct password on `servera.lab.example.com`. Test this by using **ssh** to log in as that user on `servera.lab.example.com`. The password for `ansibleuser1` is `redhat`. To make sure that SSH only tries to authenticate by password and not by an SSH key, use the `-o PreferredAuthentications=password` option when you log in.

   Log off from `servera` when you have successfully logged in.

   ```
   [student@workstation data-secret]$ ssh -o PreferredAuthentications=password \
   > ansibleuser1@servera.lab.example.com
   ansibleuser1@servera.lab.example.com's password: redhat
   Activate the web console with: systemctl enable --now cockpit.socket
   
   [ansibleuser1@servera ~]$ exit
   logout
   Connection to servera.lab.example.com closed.
   ```

**Finish**

On `workstation`, run the **lab data-secret finish** script to clean up this exercise.

```
[student@workstation ~]$ lab data-secret finish
```

This concludes the guided exercise.



------

### Managing Facts

#### Objectives

After completing this section, you should be able to reference data about managed hosts using Ansible facts, and configure custom facts on managed hosts.

#### Describing Ansible Facts

Ansible *facts* are variables that are automatically discovered by Ansible on a managed host. Facts contain host-specific information that can be used just like regular variables in plays, conditionals, loops, or any other statement that depends on a value collected from a managed host.

Some of the facts gathered for a managed host might include:

- The host name
- The kernel version
- The network interfaces
- The IP addresses
- The version of the operating system
- Various environment variables
- The number of CPUs
- The available or free memory
- The available disk space

Facts are a convenient way to retrieve the state of a managed host and to determine what action to take based on that state. For example:

- A server can be restarted by a conditional task which is run based on a fact containing the managed host's current kernel version.
- The MySQL configuration file can be customized depending on the available memory reported by a fact.
- The IPv4 address used in a configuration file can be set based on the value of a fact.

Normally, every play runs the `setup` module automatically before the first task in order to gather facts. This is reported as the `Gathering Facts` task in Ansible 2.3 and later, or simply as `setup` in older versions of Ansible. By default, you do not need to have a task to run `setup` in your play. It is normally run automatically for you.

One way to see what facts are gathered for your managed hosts is to run a short playbook that gathers facts and uses the `debug` module to print the value of the `ansible_facts` variable.

```
- name: Fact dump
  hosts: all
  tasks:
    - name: Print all facts
      debug:
        var: ansible_facts
```

When you run the playbook, the facts are displayed in the job output:

```
[user@demo ~]$ ansible-playbook facts.yml

PLAY [Fact dump] ***************************************************************

TASK [Gathering Facts] *********************************************************
ok: [demo1.example.com]

TASK [Print all facts] *********************************************************
ok: [demo1.example.com] => {
    "ansible_facts": {
        "all_ipv4_addresses": [
            "172.25.250.10"
        ],
        "all_ipv6_addresses": [
           "fe80::5054:ff:fe00:fa0a"
        ],
        "ansible_local": {},
        "apparmor": {
            "status": "disabled"
        },
        "architecture": "x86_64",
        "bios_date": "01/01/2011",
        "bios_version": "0.5.1",
        "cmdline": {
            "BOOT_IMAGE": "/boot/vmlinuz-3.10.0-327.el7.x86_64",
            "LANG": "en_US.UTF-8",
            "console": "ttyS0,115200n8",
            "crashkernel": "auto",
            "net.ifnames": "0",
            "no_timer_check": true,
            "ro": true,
            "root": "UUID=2460ab6e-e869-4011-acae-31b2e8c05a3b"
        },
...output omitted...
```

The playbook displays the content of the `ansible_facts` variable in JSON format as a hash/dictionary of variables. You can browse the output to see what facts are gathered, to find facts that you might want to use in your plays.

The following table shows some facts which might be gathered from a managed node and may be useful in a playbook:



**Table 4.3. Examples of Ansible Facts**

| Fact                                        | Variable                                                     |
| :------------------------------------------ | :----------------------------------------------------------- |
| Short host name                             | `ansible_facts['hostname']`                                  |
| Fully qualified domain name                 | `ansible_facts['fqdn']`                                      |
| Main IPv4 address (based on routing)        | `ansible_facts['default_ipv4']['address']`                   |
| List of the names of all network interfaces | `ansible_facts['interfaces']`                                |
| Size of the `/dev/vda1` disk partition      | `ansible_facts['devices']['vda']['partitions']['vda1']['size']` |
| List of DNS servers                         | `ansible_facts['dns']['nameservers']`                        |
| Version of the currently running kernel     | `ansible_facts['kernel']`                                    |



**NOTE**

Remember that when a variable's value is a hash/dictionary, there are two syntaxes that can be used to retrieve the value. To take two examples from the preceding table:

- `ansible_facts['default_ipv4']['address']` can also be written `ansible_facts.default_ipv4.address`
- `ansible_facts['dns']['nameservers']` can also be written `ansible_facts.dns.nameservers`

When a fact is used in a playbook, Ansible dynamically substitutes the variable name for the fact with the corresponding value:

```
---
- hosts: all
  tasks:
  - name: Prints various Ansible facts
    debug:
      msg: >
        The default IPv4 address of {{ ansible_facts.fqdn }}
        is {{ ansible_facts.default_ipv4.address }}
```

The following output shows how Ansible was able to query the managed node and dynamically use the system information to update the variable. Facts can also be used to create dynamic groups of hosts that match particular criteria.

```
[user@demo ~]$ ansible-playbook playbook.yml
PLAY ***********************************************************************

TASK [Gathering Facts] *****************************************************
ok: [demo1.example.com]

TASK [Prints various Ansible facts] ****************************************
ok: [demo1.example.com] => {
    "msg": "The default IPv4 address of demo1.example.com is
            172.25.250.10"
}

PLAY RECAP *****************************************************************
demo1.example.com    : ok=2    changed=0    unreachable=0    failed=0
```

#### Ansible Facts Injected as Variables

Before Ansible 2.5, facts were injected as individual variables prefixed with the string `ansible_` instead of being part of the `ansible_facts` variable. For example, the `ansible_facts['distribution']` fact would have been called `ansible_distribution`.

Many older playbooks still use facts injected as variables instead of the new syntax that is namespaced under the `ansible_facts` variable. You can use an ad hoc command to run the `setup` module to print the value of all facts in this form. In the following example, an ad hoc command is used to run the `setup` module on the managed host `demo1.example.com`:

```
[user@demo ~]$ ansible demo1.example.com -m setup
demo1.example.com | SUCCESS => {
    "ansible_facts": {
        "ansible_all_ipv4_addresses": [
            "172.25.250.10"
        ],
        "ansible_all_ipv6_addresses": [
            "fe80::5054:ff:fe00:fa0a"
        ],
        "ansible_apparmor": {
            "status": "disabled"
        },
        "ansible_architecture": "x86_64",
        "ansible_bios_date": "01/01/2011",
        "ansible_bios_version": "0.5.1",
        "ansible_cmdline": {
            "BOOT_IMAGE": "/boot/vmlinuz-3.10.0-327.el7.x86_64",
            "LANG": "en_US.UTF-8",
            "console": "ttyS0,115200n8",
            "crashkernel": "auto",
            "net.ifnames": "0",
            "no_timer_check": true,
            "ro": true,
            "root": "UUID=2460ab6e-e869-4011-acae-31b2e8c05a3b"
        }
...output omitted...
```

The following table compares the old and new fact names.



**Table 4.4. Comparison of Selected Ansible Fact Names**

| ansible_facts form                                           | Old fact variable form                                 |
| :----------------------------------------------------------- | :----------------------------------------------------- |
| `ansible_facts['hostname']`                                  | `ansible_hostname`                                     |
| `ansible_facts['fqdn']`                                      | `ansible_fqdn`                                         |
| `ansible_facts['default_ipv4']['address']`                   | `ansible_default_ipv4['address']`                      |
| `ansible_facts['interfaces']`                                | `ansible_interfaces`                                   |
| `ansible_facts['devices']['vda']['partitions']['vda1']['size']` | `ansible_devices['vda']['partitions']['vda1']['size']` |
| `ansible_facts['dns']['nameservers']`                        | `ansible_dns['nameservers']`                           |
| `ansible_facts['kernel']`                                    | `ansible_kernel`                                       |



**IMPORTANT**

Currently, Ansible recognizes both the new fact naming system (using `ansible_facts`) and the old pre-2.5 “facts injected as separate variables” naming system.

You can turn off the old naming system by setting the `inject_facts_as_vars` parameter in the `[default]` section of the Ansible configuration file to `false`. The default setting is currently `true`.

The default value of `inject_facts_as_vars` will probably change to `false` in a future version of Ansible. If it is set to `false`, you can only reference Ansible facts using the new `ansible_facts.*` naming system. In that case, attempts to reference facts through the old namespace results in the following error:

```
...output omitted...
TASK [Show me the facts] *************************************************
fatal: [demo.example.com]: FAILED! => {"msg": "The task includes an option with an undefined variable. The error was: 'ansible_distribution' is undefined\n\nThe error appears to have been in
 '/home/student/demo/playbook.yml': line 5, column 7, but may\nbe elsewhere in the file depending on the exact syntax problem.\n\nThe offending line appears to be:\n\n  tasks:\n    - name: Show me the facts\n      ^ here\n"}
...output omitted...
```

#### Turning off Fact Gathering

Sometimes, you do not want to gather facts for your play. There are a couple of reasons why this might be the case. It might be that you are not using any facts and want to speed up the play or reduce load caused by the play on the managed hosts. It might be that the managed hosts cannot run the `setup` module for some reason, or need to install some prerequisite software before gathering facts.

To disable fact gathering for a play, set the `gather_facts` keyword to `no`:

```
---
- name: This play gathers no facts automatically
  hosts: large_farm
  gather_facts: no
```

Even if `gather_facts: no` is set for a play, you can manually gather facts at any time by running a task that uses the `setup` module:

```
  tasks:
    - name: Manually gather facts
      setup:
...output omitted...
```

#### Creating Custom Facts

Administrators can create *custom facts* which are stored locally on each managed host. These facts are integrated into the list of standard facts gathered by the `setup` module when it runs on the managed host. These allow the managed host to provide arbitrary variables to Ansible which can be used to adjust the behavior of plays.

Custom facts can be defined in a static file, formatted as an INI file or using JSON. They can also be executable scripts which generate JSON output, just like a dynamic inventory script.

Custom facts allow administrators to define certain values for managed hosts, which plays might use to populate configuration files or conditionally run tasks. Dynamic custom facts allow the values for these facts, or even which facts are provided, to be determined programmatically when the play is run.

By default, the `setup` module loads custom facts from files and scripts in each managed host's `/etc/ansible/facts.d` directory. The name of each file or script must end in `.fact` in order to be used. Dynamic custom fact scripts must output JSON-formatted facts and must be executable.

This is an example of a static custom facts file written in INI format. An INI-formatted custom facts file contains a top level defined by a section, followed by the key-value pairs of the facts to define:

```
[packages]
web_package = httpd
db_package = mariadb-server

[users]
user1 = joe
user2 = jane
```

The same facts could be provided in JSON format. The following JSON facts are equivalent to the facts specified by the INI format in the preceding example. The JSON data could be stored in a static text file or printed to standard output by an executable script:

```
{
  "packages": {
    "web_package": "httpd",
    "db_package": "mariadb-server"
  },
  "users": {
    "user1": "joe",
    "user2": "jane"
  }
}
```

**NOTE**

Custom fact files cannot be in YAML format like a playbook. JSON format is the closest equivalent.

Custom facts are stored by the `setup` module in the `ansible_facts.ansible_local` variable. Facts are organized based on the name of the file that defined them. For example, assume that the preceding custom facts are produced by a file saved as `/etc/ansible/facts.d/custom.fact` on the managed host. In that case, the value of `ansible_facts.ansible_local['custom']['users']['user1']` is `joe`.

You can inspect the structure of your custom facts by running the `setup` module on the managed hosts with an ad hoc command.

```
[user@demo ~]$ ansible demo1.example.com -m setup
demo1.example.com | SUCCESS => {
    "ansible_facts": {
...output omitted...
        "ansible_local": {
            "custom": {
                "packages": {
                    "db_package": "mariadb-server",
                    "web_package": "httpd"
                },
                "users": {
                    "user1": "joe",
                    "user2": "jane"
                }
            }
        },
...output omitted...
    },
    "changed": false
}
```

Custom facts can be used the same way as default facts in playbooks:

```
[user@demo ~]$ cat playbook.yml
---
- hosts: all
  tasks:
  - name: Prints various Ansible facts
    debug:
      msg: >
           The package to install on {{ ansible_facts['fqdn'] }}
           is {{ ansible_facts['ansible_local']['custom']['packages']['web_package'] }}

[user@demo ~]$ ansible-playbook playbook.yml
PLAY ***********************************************************************

TASK [Gathering Facts] *****************************************************
ok: [demo1.example.com]

TASK [Prints various Ansible facts] ****************************************
ok: [demo1.example.com] => {
    "msg": "The package to install on demo1.example.com  is httpd"
}

PLAY RECAP *****************************************************************
demo1.example.com    : ok=2    changed=0    unreachable=0    failed=0
```

#### Using Magic Variables

Some variables are not facts or configured through the `setup` module, but are also automatically set by Ansible. These *magic variables* can also be useful to get information specific to a particular managed host.

Four of the most useful are:

- `hostvars`

  Contains the variables for managed hosts, and can be used to get the values for another managed host's variables. It does not include the managed host's facts if they have not yet been gathered for that host.

- `group_names`

  Lists all groups the current managed host is in.

- `groups`

  Lists all groups and hosts in the inventory.

- `inventory_hostname`

  Contains the host name for the current managed host as configured in the inventory. This may be different from the host name reported by facts for various reasons.

There are a number of other “magic variables” as well. For more information, see https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable. One way to get insight into their values is to use the `debug` module to report on the contents of the `hostvars` variable for a particular host:

```
[user@demo ~]$ ansible localhost -m debug -a 'var=hostvars["localhost"]'
localhost | SUCCESS => {
    "hostvars[\"localhost\"]": {
        "ansible_check_mode": false,
        "ansible_connection": "local",
        "ansible_diff_mode": false,
        "ansible_facts": {},
        "ansible_forks": 5,
        "ansible_inventory_sources": [
            "/home/student/demo/inventory"
        ],
        "ansible_playbook_python": "/usr/bin/python2",
        "ansible_python_interpreter": "/usr/bin/python2",
        "ansible_verbosity": 0,
        "ansible_version": {
            "full": "2.7.0",
            "major": 2,
            "minor": 7,
            "revision": 0,
            "string": "2.7.0"
        },
        "group_names": [],
        "groups": {
            "all": [
                "serverb.lab.example.com"
            ],
            "ungrouped": [],
            "webservers": [
                "serverb.lab.example.com"
            ]
        },
        "inventory_hostname": "localhost",
        "inventory_hostname_short": "localhost",
        "omit": "__omit_place_holder__18d132963728b2cbf7143dd49dc4bf5745fe5ec3",
        "playbook_dir": "/home/student/demo"
    }
}
```

#### REFERENCES

[*setup - Gathers facts about remote hosts — Ansible Documentation*](https://docs.ansible.com/ansible/latest/modules/setup_module.html)

[*Local Facts (Facts.d) — Variables — Ansible Documentation*](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#local-facts-facts-d)



------

### Guided Exercise: Managing Facts

In this exercise, you will gather Ansible facts from a managed host and use them in plays.

**Outcomes**

You should be able to:



- Gather facts from a host.
- Create tasks that use the gathered facts.



Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab data-facts start** command. This script creates the working directory, `data-facts`, and populates it with an Ansible configuration file and host inventory.

```
[student@workstation ~]$ lab data-facts start
```

1. On `workstation`, as the `student` user, change into the `/home/student/data-facts` directory.

   ```
   [student@workstation ~]$ cd ~/data-facts
   [student@workstation data-facts]$ 
   ```

2. The Ansible `setup` module retrieves facts from systems. Run an ad hoc command to retrieve the facts for all servers in the `webserver` group. The output displays all the facts gathered for `servera.lab.example.com` in JSON format. Review some of the variables displayed.

   ```
   [student@workstation data-facts]$ ansible webserver -m setup
   ...output omitted...
   servera.lab.example.com | SUCCESS => {
       "ansible_facts": {
           "ansible_all_ipv4_addresses": [
               "172.25.250.10"
           ],
           "ansible_all_ipv6_addresses": [
               "fe80::2937:3aa3:ea8d:d3b1"
           ],
   ...output omitted...
   ```

3. On `workstation`, create a fact file named `/home/student/data-facts/custom.fact`. The fact file defines the package to install and the service to start on `servera`. The file should read as follows:

   ```
   [general]
   package = httpd
   service = httpd
   state = started
   enabled = true
   ```

4. Create the `setup_facts.yml` playbook to make the `/etc/ansible/facts.d` remote directory and to save the `custom.fact` file to that directory.

   ```
   ---
   - name: Install remote facts
     hosts: webserver
     vars:
       remote_dir: /etc/ansible/facts.d
       facts_file: custom.fact
     tasks:
       - name: Create the remote directory
         file:
           state: directory
           recurse: yes
           path: "{{ remote_dir }}"
       - name: Install the new facts
         copy:
           src: "{{ facts_file }}"
           dest: "{{ remote_dir }}"
   ```

5. Run an ad hoc command with the `setup` module. Search for the `ansible_local` section in the output. There should not be any custom facts at this point.

   ```
   [student@workstation data-facts]$ ansible webserver -m setup 
   servera.lab.example.com | SUCCESS => {
       "ansible_facts": {
   ...output omitted...
           "ansible_local": {}
   ...output omitted...
       },
       "changed": false
   }
   ```

6. Before running the playbook, verify its syntax is correct by running **ansible-playbook --syntax-check**. If it reports any errors, correct them before moving to the next step. You should see output similar to the following:

   ```
   [student@workstation data-facts]$ ansible-playbook --syntax-check setup_facts.yml
   
   playbook: setup_facts.yml
   ```

7. Run the `setup_facts.yml` playbook.

   ```
   [student@workstation data-facts]$ ansible-playbook setup_facts.yml
   
   PLAY [Install remote facts] ****************************************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [servera.lab.example.com]
   
   TASK [Create the remote directory] *********************************************
   changed: [servera.lab.example.com]
   
   TASK [Install the new facts] ***************************************************
   changed: [servera.lab.example.com]
   
   PLAY RECAP *********************************************************************
   servera.lab.example.com    : ok=3    changed=2    unreachable=0    failed=0
   ```

8. It is now possible to create the main playbook that uses both default and user facts to configure `servera`. Over the next several steps, you will add to the playbook file. Create the playbook `playbook.yml` with the following:

   ```
   ---
   - name: Install Apache and starts the service
     hosts: webserver
   ```

9. Continue editing the `playbook.yml` file by creating the first task that installs the httpd package. Use the user fact for the name of the package.

   ```
     tasks:
       - name: Install the required package
         yum:
           name: "{{ ansible_facts['ansible_local']['custom']['general']['package'] }}"
           state: latest
   ```

10. Create another task that uses the custom fact to start the `httpd` service.

    ```
        - name: Start the service
          service:
            name: "{{ ansible_facts['ansible_local']['custom']['general']['service'] }}"
            state: "{{ ansible_facts['ansible_local']['custom']['general']['state'] }}"
            enabled: "{{ ansible_facts['ansible_local']['custom']['general']['enabled'] }}"
    ```

11. When completed with all the tasks, the full playbook should look like the following. Review the playbook and ensure all the tasks are defined.

    ```
    ---
    - name: Install Apache and starts the service
      hosts: webserver
    
      tasks:
        - name: Install the required package
          yum:
            name: "{{ ansible_facts['ansible_local']['custom']['general']['package'] }}"
            state: latest
    
        - name: Start the service
          service:
            name: "{{ ansible_facts['ansible_local']['custom']['general']['service'] }}"
            state: "{{ ansible_facts['ansible_local']['custom']['general']['state'] }}"
            enabled: "{{ ansible_facts['ansible_local']['custom']['general']['enabled'] }}"
    ```

12. Before running the playbook, use an ad hoc command to verify the `httpd` service is not currently running on `servera`.

    ```
    [student@workstation data-facts]$ ansible servera.lab.example.com -m command \
    > -a 'systemctl status httpd'
    servera.lab.example.com | FAILED | rc=4 >>
    Unit httpd.service could not be found.non-zero return code
    ```

13. Verify the syntax of the playbook by running **ansible-playbook --syntax-check**. If it reports any errors, correct them before moving to the next step. You should see output similar to the following:

    ```
    [student@workstation data-facts]$ ansible-playbook --syntax-check playbook.yml
    
    playbook: playbook.yml
    ```

14. Run the playbook using the **ansible-playbook** command. Watch the output as Ansible installs the package and then enables the service.

    ```
    [student@workstation data-facts]$ ansible-playbook playbook.yml
    
    PLAY [Install Apache and start the service] ************************************
    
    TASK [Gathering Facts] *********************************************************
    ok: [servera.lab.example.com]
    
    TASK [Install the required package] ********************************************
    changed: [servera.lab.example.com]
    
    TASK [Start the service] *******************************************************
    changed: [servera.lab.example.com]
    
    PLAY RECAP *********************************************************************
    servera.lab.example.com    : ok=3    changed=2    unreachable=0    failed=0
    ```

15. Use an ad hoc command to execute **systemctl** to determine whether the `httpd` service is now running on `servera`.

    ```
    [student@workstation data-facts]$ ansible servera.lab.example.com -m command \
    > -a 'systemctl status httpd'
    servera.lab.example.com | CHANGED | rc=0 >>
    ● httpd.service - The Apache HTTP Server
       Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
       Active: active (running) since Mon 2019-05-27 07:50:55 EDT; 50s ago
         Docs: man:httpd.service(8)
     Main PID: 11603 (httpd)
       Status: "Running, listening on: port 80"
        Tasks: 213 (limit: 4956)
       Memory: 24.1M
       CGroup: /system.slice/httpd.service
    ...output omitted...
    ```

**Finish**

On `workstation`, run the **lab data-facts finish** script to clean up this exercise.

```
[student@workstation ~]$ lab data-facts finish
```

This concludes the guided exercise.



------

### Lab: Managing Variables and Facts

**Performance Checklist**

In this lab, you will write and run an Ansible Playbook that uses variables, secrets, and facts.

**Outcomes**

You should be able to define variables and use facts in a playbook, as well as use variables defined in an encrypted file.

Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab data-review start** command. The script creates the `/home/student/data-review` working directory and populates it with an Ansible configuration file and host inventory. The managed host `serverb.lab.example.com` is defined in this inventory as a member of the `webserver` host group. A developer has asked you to write an Ansible Playbook to automate the setup of a web server environment on `serverb.lab.example.com`, which controls user access to its website using basic authentication.

The `files` subdirectory contains:

- A `httpd.conf` configuration file for the Apache web service for basic authentication
- A `.htaccess` file, used to control access to the web server's document root directory
- A `htpasswd` file containing credentials for permitted users

```
[student@workstation ~]$ lab data-review start
```

1. In the working directory, create the `playbook.yml` playbook and add the `webserver` host group as the managed host. Define the following play variables:

   

   **Table 4.5. Variables**

   | Variable         | Values                         |
   | :--------------- | :----------------------------- |
   | `firewall_pkg`   | `firewalld`                    |
   | `firewall_svc`   | `firewalld`                    |
   | `web_pkg`        | `httpd`                        |
   | `web_svc`        | `httpd`                        |
   | `ssl_pkg`        | `mod_ssl`                      |
   | `httpdconf_src`  | `files/httpd.conf`             |
   | `httpdconf_dest` | `/etc/httpd/conf/httpd.conf`   |
   | `htaccess_src`   | `files/.htaccess`              |
   | `secrets_dir`    | `/etc/httpd/secrets`           |
   | `secrets_src`    | `files/htpasswd`               |
   | `secrets_dest`   | `"{{ secrets_dir }}/htpasswd"` |
   | `web_root`       | `/var/www/html`                |

   1. Change to the `/home/student/data-review` working directory.

      ```
      [student@workstation ~]$ cd ~/data-review
      [student@workstation data-review]$ 
      ```

   2. Create the `playbook.yml` playbook file and edit it in a text editor. The beginning of the file should appear as follows:

      ```
      ---
      - name: install and configure webserver with basic auth
        hosts: webserver
        vars:
          firewall_pkg: firewalld
          firewall_svc: firewalld
          web_pkg: httpd
          web_svc: httpd
          ssl_pkg: mod_ssl
          httpdconf_src: files/httpd.conf
          httpdconf_dest: /etc/httpd/conf/httpd.conf
          htaccess_src: files/.htaccess
          secrets_dir: /etc/httpd/secrets
          secrets_src: files/htpasswd
          secrets_dest: "{{ secrets_dir }}/htpasswd"
          web_root: /var/www/html
      ```

   

2. Add a `tasks` section to the play. Write a task that ensures the latest version of the necessary packages are installed. These packages are defined by the `firewall_pkg`, `web_pkg`, and `ssl_pkg` variables.

   1. Define the beginning of the `tasks` section by adding the following line to the playbook:

      ```
        tasks:
      ```

   2. Add the following lines to the playbook to define a task that uses the `yum` module to install the required packages.

      ```
          - name: latest version of necessary packages installed
            yum:
              name:
                - "{{ firewall_pkg }}"
                - "{{ web_pkg }}"
                - "{{ ssl_pkg }}"
              state: latest
      ```

   

3. Add a second task to the playbook that ensures that the file specified by the `httpdconf_src` variable has been copied (with the `copy` module) to the location specified by the `httpdconf_dest` variable on the managed host. The file should be owned by the `root` user and the `root` group. Also set 0644 as the file permissions.

   Add the following lines to the playbook to define a task that uses the `copy` module to copy the contents of the file defined by the `httpdconf_src` variable to the location specified by the `httpdconf_dest` variable.

   ```
       - name: configure web service
         copy:
           src: "{{ httpdconf_src }}"
           dest: "{{ httpdconf_dest }}"
           owner: root
           group: root
           mode: 0644
   ```

   

4. Add a third task that uses the `file` module to create the directory specified by the `secrets_dir` variable on the managed host. This directory holds the password files used for the basic authentication of web services. The file should be owned by the `apache` user and the `apache` group. Set 0500 as the file permissions.

   Add the following lines to the playbook to define a task that uses the `file` module to create the directory defined by the `secrets_dir` variable.

   ```
       - name: secrets directory exists
         file:
           path: "{{ secrets_dir }}"
           state: directory
           owner: apache
           group: apache
           mode: 0500
   ```

   

5. Add a fourth task that uses the `copy` module to place a htpasswd file, used for basic authentication of web users. The source should be defined by the `secrets_src` variable. The destination should be defined by the `secrets_dest` variable. The file should be owned by the `apache` user and group. Set `0400` as the file permissions.

   ```
       - name: htpasswd file exists
         copy:
           src: "{{ secrets_src }}"
           dest: "{{ secrets_dest }}"
           owner: apache
           group: apache
           mode: 0400
   ```

   

6. Add a fifth task that uses the `copy` module to create a `.htaccess` file in the document root directory of the web server. Copy the file specified by the `htaccess_src` variable to `{{ web_root }}/.htaccess`. The file should be owned by the `apache` user and the `apache` group. Set 0400 as the file permissions.

   Add the following lines to the playbook to define a task which uses the `copy` module to create the `.htaccess` file using the file defined by the `htaccess_src` variable.

   ```
       - name: .htaccess file installed in docroot
         copy:
           src: "{{ htaccess_src }}"
           dest: "{{ web_root }}/.htaccess"
           owner: apache
           group: apache
           mode: 0400
   ```

   

7. Add a sixth task that uses the `copy` module to create the web content file `index.html` in the directory specified by the `web_root` variable. The file should contain the message “*HOSTNAME* (*IPADDRESS*) has been customized by Ansible.”, where `HOSTNAME` is the fully-qualified host name of the managed host and `IPADDRESS` is its IPv4 IP address. Use the `content` option to the `copy` module to specify the content of the file, and Ansible facts to specify the host name and IP address.

   Add the following lines to the playbook to define a task that uses the `copy` module to create the `index.html` file in the directory defined by the `web_root` variable. Populate the file with the content specified using the `ansible_facts['fqdn']` and `ansible_facts['default_ipv4']['address']` Ansible facts retrieved from the managed host.

   ```
       - name: create index.html
         copy:
           content: "{{ ansible_facts['fqdn'] }} ({{ ansible_facts['default_ipv4']['address'] }}) has been customized by Ansible.\n"
           dest: "{{ web_root }}/index.html"
   ```

   

8. Add a seventh task that uses the `service` module to enable and start the firewall service on the managed host.

   Add the following lines to the playbook to define a task that uses the `service` module to enable and start the firewall service.

   ```
       - name: firewall service enabled and started
         service:
           name: "{{ firewall_svc }}"
           state: started
           enabled: true
   ```

   

9. Add an eighth task that uses the `firewalld` module to allow the `https` service needed for users to access web services on the managed host. This firewall change should be permanent and should take place immediately.

   Add the following lines to the playbook to define a task that uses the `firewalld` module to open the HTTPS port for the web service.

   ```
       - name: open the port for the web server
         firewalld:
           service: https
           state: enabled
           immediate: true
           permanent: true
   ```

   

10. Add a final task that uses the `service` module to enable and start the web service on the managed host for all configuration changes to take effect. The name of the web service is defined by the `web_svc` variable.

    ```
        - name: web service enabled and started
          service:
            name: "{{ web_svc }}"
            state: started
            enabled: true
    ```

    

11. Define a second play targeted at `localhost` which will test authentication to the web server. It does not need privilege escalation. Define a variable named `web_user` with the value `guest`.

    1. Add the following line to define the start of a second play. Note that there is no indentation.

       ```
       - name: test web server with basic auth
       ```

    2. Add the following line to indicate that the play applies to the `localhost` managed host.

       ```
         hosts: localhost
       ```

    3. Add the following line to disable privilege escalation.

       ```
         become: no
       ```

    4. Add the following lines to define a variables list and the `web_user` variable.

       ```
         vars:
           web_user: guest
       ```

    

12. Add a directive to the play that adds additional variables from a variable file named `vars/secret.yml`. This file contains a variable named `web_pass` that specifies the password for the web user. You will create this file later in the lab.

    Define the start of the task list.

    1. Using the `vars_files` keyword, add the following lines to the playbook to instruct Ansible to use variables found in the `vars/secret.yml` variable file.

       ```
         vars_files:
           - vars/secret.yml
       ```

    2. Add the following line to define the beginning of the `tasks` list.

       ```
         tasks:
       ```

    

13. Add two tasks to the second play.

    The first uses the `uri` module to request content from `https://serverb.lab.example.com` using basic authentication. Use the `web_user` and `web_pass` variables to authenticate to the web server. Note that the certificate presented by `serverb` will not be trusted, so you will need to avoid certificate validation. The task should verify a return HTTP status code of `200`. Configure the task to place the returned content in the task results variable. Register the task result in a variable.

    The second task uses the `debug` module to print the content returned from the web server.

    1. Add the following lines to create the task for verifying the web service from the control node. Be sure to indent the first line with four spaces.

       ```
           - name: connect to web server with basic auth
             uri:
               url: https://serverb.lab.example.com
               validate_certs: no
               force_basic_auth: yes
               user: "{{ web_user }}"
               password: "{{ web_pass }}"
               return_content: yes
               status_code: 200
             register: auth_test
       ```

    2. Create the second task using the debug module. The content returned from the web server is added to the registered variable as the key `content`.

       ```
           - debug:
               var: auth_test.content
       ```

    3. The completed playbook should appear as follows:

       ```
       ---
       - name: install and configure webserver with basic auth
         hosts: webserver
         vars:
           firewall_pkg: firewalld
           firewall_svc: firewalld
           web_pkg: httpd
           web_svc: httpd
           ssl_pkg: mod_ssl
           httpdconf_src: files/httpd.conf
           httpdconf_dest: /etc/httpd/conf/httpd.conf
           htaccess_src: files/.htaccess
           secrets_dir: /etc/httpd/secrets
           secrets_src: files/htpasswd
           secrets_dest: "{{ secrets_dir }}/htpasswd"
           web_root: /var/www/html
         tasks:
           - name: latest version of necessary packages installed
             yum:
               name:
                 - "{{ firewall_pkg }}"
                 - "{{ web_pkg }}"
                 - "{{ ssl_pkg }}"
               state: latest
       
           - name: configure web service
             copy:
               src: "{{ httpdconf_src }}"
               dest: "{{ httpdconf_dest }}"
               owner: root
               group: root
               mode: 0644
       
           - name: secrets directory exists
             file:
               path: "{{ secrets_dir }}"
               state: directory
               owner: apache
               group: apache
               mode: 0500
       
           - name: htpasswd file exists
             copy:
               src: "{{ secrets_src }}"
               dest: "{{ secrets_dest }}"
               owner: apache
               group: apache
               mode: 0400
       
           - name: .htaccess file installed in docroot
             copy:
               src: "{{ htaccess_src }}"
               dest: "{{ web_root }}/.htaccess"
               owner: apache
               group: apache
               mode: 0400
       
           - name: create index.html
             copy:
               content: "{{ ansible_facts['fqdn'] }} ({{ ansible_facts['default_ipv4']['address'] }}) has been customized by Ansible.\n"
               dest: "{{ web_root }}/index.html"
       
           - name: firewall service enable and started
             service:
               name: "{{ firewall_svc }}"
               state: started
               enabled: true
       
           - name: open the port for the web server
             firewalld:
               service: https
               state: enabled
               immediate: true
               permanent: true
       
           - name: web service enabled and started
             service:
               name: "{{ web_svc }}"
               state: started
               enabled: true
       
       - name: test web server with basic auth
         hosts: localhost
         become: no
         vars:
           - web_user: guest
         vars_files:
           - vars/secret.yml
         tasks:
           - name: connect to web server with basic auth
             uri:
               url: https://serverb.lab.example.com
               validate_certs: no
               force_basic_auth: yes
               user: "{{ web_user }}"
               password: "{{ web_pass }}"
               return_content: yes
               status_code: 200
             register: auth_test
       
           - debug:
               var: auth_test.content
       ```

    4. Save and close the `playbook.yml` file.

    

14. Create a file encrypted with Ansible Vault, named `vars/secret.yml`. Use the password `redhat` to encrypt it. It should set the `web_pass` variable to `redhat`, which will be the web user's password.

    1. Create a subdirectory named `vars` in the working directory.

       ```
       [student@workstation data-review]$ mkdir vars
       ```

    2. Create the encrypted variable file, `vars/secret.yml`, using Ansible Vault. Set the password for the encrypted file to `redhat`.

       ```
       [student@workstation data-review]$ ansible-vault create vars/secret.yml
       New Vault password: redhat
       Confirm New Vault password: redhat
       ```

    3. Add the following variable definition to the file.

       ```
       web_pass: redhat
       ```

    4. Save and close the file.

    

15. Run the `playbook.yml` playbook. Verify that content is successfully returned from the web server, and that it matches what was configured in an earlier task.

    1. Before running the playbook, verify that its syntax is correct by running **ansible-playbook --syntax-check**. Use the `--ask-vault-pass` to be prompted for the vault password. Enter `redhat` when prompted for the password. If it reports any errors, correct them before moving to the next step. You should see output similar to the following:

       ```
       [student@workstation data-review]$ ansible-playbook --syntax-check \
       > --ask-vault-pass playbook.yml
       Vault password: redhat
       
       playbook: playbook.yml
       ```

    2. Using the **ansible-playbook** command, run the playbook with the `--ask-vault-pass` option. Enter `redhat` when prompted for the password.

       ```
       [student@workstation data-review]$ ansible-playbook playbook.yml --ask-vault-pass
       Vault password: redhat
       PLAY [Install and configure webserver with basic auth] *********************
       
       ...output omitted...
       
       TASK [connect to web server with basic auth] ***********************************
       ok: [localhost]
       
       TASK [debug] *******************************************************************
       ok: [localhost] => {
           "auth_test.content": "serverb.lab.example.com (172.25.250.11) has been customized by Ansible.\n"
       }
       
       PLAY RECAP *********************************************************************
       localhost                  : ok=3    changed=0    unreachable=0    failed=0
       serverb.lab.example.com    : ok=10   changed=8    unreachable=0    failed=0
       ```

    

**Evaluation**

Run the **lab data-review grade** command on *workstation* to confirm success on this exercise. Correct any reported failures and rerun the script until successful.

```
[student@workstation ~]$ lab data-review grade
```

**Finish**

On `workstation`, run the **lab data-review finish** command to clean up this exercise.

```
[student@workstation ~]$ lab data-review finish
```

This concludes the lab.



------

### Summary

In this chapter, you learned:

- Ansible *variables* allow administrators to reuse values across files in an entire Ansible project.
- Variables can be defined for hosts and host groups in the inventory file.
- Variables can be defined for playbooks by using facts and external files. They can also be defined on the command line.
- The `register` keyword can be used to capture the output of a command in a variable.
- Ansible Vault is one way to protect sensitive data such as password hashes and private keys for deployment using Ansible Playbooks.
- Ansible *facts* are variables that are automatically discovered by Ansible from a managed host.



## Chapter 5. Implementing Task Control

- [Writing Loops and Conditional Tasks](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch05)
- [Guided Exercise: Writing Loops and Conditional Tasks](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch05s02)
- [Implementing Handlers](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch05s03)
- [Guided Exercise: Implementing Handlers](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch05s04)
- [Handling Task Failure](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch05s05)
- [Guided Exercise: Handling Task Failure](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch05s06)
- [Lab: Implementing Task Control](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch05s07)
- [Summary](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch05s08)

**Abstract**



| **Goal**       | Manage task control, handlers, and task errors in Ansible Playbooks. |
| -------------- | ------------------------------------------------------------ |
| **Objectives** | Implement loops to write efficient tasks to control when to run tasks.Implement a task that runs only when another task changes the managed host.Control what happens when a task fails, and what conditions cause a task to fail. |
| **Sections**   | Writing Loops and Conditional Tasks (and Guided Exercise)Implementing Handlers (and Guided Exercise)Handling Task Failure (and Guided Exercise) |
| **Lab**        | Implementing Task Control                                    |



### Writing Loops and Conditional Tasks

#### Objectives

After completing this section, you should be able to use loops to write efficient tasks, and use conditions to control when to run tasks.

#### Task Iteration with Loops

Using loops saves administrators from the need to write multiple tasks that use the same module. For example, instead of writing five tasks to ensure five users exist, you can write one task that iterates over a list of five users to ensure they all exist.

Ansible supports iterating a task over a set of items using the `loop` keyword. You can configure loops to repeat a task using each item in a list, the contents of each of the files in a list, a generated sequence of numbers, or using more complicated structures. This section covers simple loops that iterate over a list of items. Consult the documentation for more advanced looping scenarios.

**Simple Loops**

A simple loop iterates a task over a list of items. The `loop` keyword is added to the task, and takes as a value the list of items over which the task should be iterated. The loop variable `item` holds the value used during each iteration.

Consider the following snippet that uses the `service` module twice in order to ensure two network services are running:

```
- name: Postfix is running
  service:
    name: postfix
    state: started

- name: Dovecot is running
  service:
    name: dovecot
    state: started
```

These two tasks can be rewritten to use a simple loop so that only one task is needed to ensure both services are running:

```
- name: Postfix and Dovecot are running
  service:
    name: "{{ item }}"
    state: started
  loop:
    - postfix
    - dovecot
```

The list used by `loop` can be provided by a variable. In the following example, the variable `mail_services` contains the list of services that need to be running.

```
vars:
  mail_services:
    - postfix
    - dovecot

tasks:
  - name: Postfix and Dovecot are running
    service:
      name: "{{ item }}"
      state: started
    loop: "{{ mail_services }}"
```

**Loops over a List of Hashes or Dictionaries**

The `loop` list does not need to be a list of simple values. In the following example, each item in the list is actually a hash or a dictionary. Each hash or dictionary in the example has two keys, `name` and `groups`, and the value of each key in the current `item` loop variable can be retrieved with the `item.name` and `item.groups` variables, respectively.

```
- name: Users exist and are in the correct groups
  user:
    name: "{{ item.name }}"
    state: present
    groups: "{{ item.groups }}"
  loop:
    - name: jane
      groups: wheel
    - name: joe
      groups: root
```

The outcome of the preceding task is that the user `jane` is present and a member of the group `wheel`, and that the user `joe` is present and a member of the group `root`.

**Earlier-Style Loop Keywords**

Before Ansible 2.5, most playbooks used a different syntax for loops. Multiple loop keywords were provided, which were prefixed with `with_`, followed by the name of an Ansible look-up plug-in (an advanced feature not covered in detail in this course). This syntax for looping is very common in existing playbooks, but will probably be deprecated at some point in the future.

A few examples are listed in the table below:



**Table 5.1. Earlier-Style Ansible Loops**

| Loop keyword    | Description                                                  |
| :-------------- | :----------------------------------------------------------- |
| `with_items`    | Behaves the same as the `loop` keyword for simple lists, such as a list of strings or a list of hashes/dictionaries. Unlike `loop`, if lists of lists are provided to `with_items`, they are flattened into a single-level list. The loop variable `item` holds the list item used during each iteration. |
| `with_file`     | This keyword requires a list of control node file names. The loop variable `item` holds the content of a corresponding file from the file list during each iteration. |
| `with_sequence` | Instead of requiring a list, this keyword requires parameters to generate a list of values based on a numeric sequence. The loop variable `item` holds the value of one of the generated items in the generated sequence during each iteration. |



An example of `with_items` in a playbook is shown below:

```
  vars:
    data:
      - user0
      - user1
      - user2
  tasks:
    - name: "with_items"
      debug:
        msg: "{{ item }}"
      with_items: "{{ data }}"
```

**IMPORTANT**

Since Ansible 2.5, the recommended way to write loops is to use the `loop` keyword.

However, you should still understand the old syntax, especially `with_items`, because it is widely used in existing playbooks. You are likely to encounter playbooks and roles that continue to use `with_*` keywords for looping.

Any task using the old syntax can be converted to use `loop` in conjunction with Ansible filters. You do not need to know how to use Ansible filters to do this. There is a good reference on how to convert the old loops to the new syntax, as well as examples of how to loop over items that are not simple lists, in the Ansible documentation in the section [Migrating from with_X to loop](https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html#migrating-from-with-x-to-loop) of the *Ansible User Guide*.

You will likely encounter tasks from older playbooks that contain `with_*` keywords.

Advanced looping techniques are beyond the scope of this course. All iteration tasks in this course can be implemented with either the `with_items` or the `loop` keyword.

**Using Register Variables with Loops**

The `register` keyword can also capture the output of a task that loops. The following snippet shows the structure of the register variable from a task that loops:

```
[student@workstation loopdemo]$ cat loop_register.yml
---
- name: Loop Register Test
  gather_facts: no
  hosts: localhost
  tasks:
    - name: Looping Echo Task
      shell: "echo This is my item: {{ item }}"
      loop:
        - one
        - two
      register: echo_results

    - name: Show echo_results variable
      debug:
        var: echo_results
```

| [![1](https://rol.redhat.com/rol/static/roc/Common_Content/images/1.svg)](https://rol.redhat.com/rol/app/#control-flow-register-echoresults) | The `echo_results` variable is registered.                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [![2](https://rol.redhat.com/rol/static/roc/Common_Content/images/2.svg)](https://rol.redhat.com/rol/app/#control-flow-debug-echoresults) | The contents of the `echo_results` variable are displayed to the screen. |

Running the above playbook yields the following output:

```
[student@workstation loopdemo]$ ansible-playbook loop_register.yml
PLAY [Loop Register Test] ****************************************************

TASK [Looping Echo Task] *****************************************************
...output omitted...
TASK [Show echo_results variable] ********************************************
ok: [localhost] => {
    "echo_results": {
        "changed": true,
        "msg": "All items completed",
        "results": [
            {
                "_ansible_ignore_errors": null,
                ...output omitted...
                "changed": true,
                "cmd": "echo This is my item: one",
                "delta": "0:00:00.011865",
                "end": "2018-11-01 16:32:56.080433",
                "failed": false,
                ...output omitted...
                "item": "one",
                "rc": 0,
                "start": "2018-11-01 16:32:56.068568",
                "stderr": "",
                "stderr_lines": [],
                "stdout": "This is my item: one",
                "stdout_lines": [
                    "This is my item: one"
                ]
            },
            {
                "_ansible_ignore_errors": null,
                ...output omitted...
                "changed": true,
                "cmd": "echo This is my item: two",
                "delta": "0:00:00.011142",
                "end": "2018-11-01 16:32:56.828196",
                "failed": false,
                ...output omitted...
                "item": "two",
                "rc": 0,
                "start": "2018-11-01 16:32:56.817054",
                "stderr": "",
                "stderr_lines": [],
                "stdout": "This is my item: two",
                "stdout_lines": [
                    "This is my item: two"
                ]
            }
        ]
    }
}
...output omitted...
```

| [![1](https://rol.redhat.com/rol/static/roc/Common_Content/images/1.svg)](https://rol.redhat.com/rol/app/#control-echoresults-start) | The `{` character indicates that the start of the `echo_results` variable is composed of key-value pairs. |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [![2](https://rol.redhat.com/rol/static/roc/Common_Content/images/2.svg)](https://rol.redhat.com/rol/app/#control-flow-register-left) | The `results` key contains the results from the previous task. The `[` character indicates the start of a list. |
| [![3](https://rol.redhat.com/rol/static/roc/Common_Content/images/3.svg)](https://rol.redhat.com/rol/app/#control-flow-register-one-start) | The start of task metadata for the first item (indicated by the `item` key). The output of the **echo** command is found in the `stdout` key. |
| [![4](https://rol.redhat.com/rol/static/roc/Common_Content/images/4.svg)](https://rol.redhat.com/rol/app/#control-flow-register-two-start) | The start of task result metadata for the second item.       |
| [![5](https://rol.redhat.com/rol/static/roc/Common_Content/images/5.svg)](https://rol.redhat.com/rol/app/#control-flow-register-right) | The `]` character indicates the end of the `results` list.   |

In the above, the `results` key contains a list. Below, the playbook is modified such that the second task iterates over this list:

```
[student@workstation loopdemo]$ cat new_loop_register.yml
---
- name: Loop Register Test
  gather_facts: no
  hosts: localhost
  tasks:
    - name: Looping Echo Task
      shell: "echo This is my item: {{ item }}"
      loop:
        - one
        - two
      register: echo_results

    - name: Show stdout from the previous task.
      debug:
        msg: "STDOUT from previous task: {{ item.stdout }}"
      loop: "{{ echo_results['results'] }}"
```

After executing the above playbook, the output is:

```
PLAY [Loop Register Test] ****************************************************

TASK [Looping Echo Task] *****************************************************
...output omitted...

TASK [Show stdout from the previous task.] ***********************************
ok: [localhost] => (item={...output omitted...}) => {
    "msg": "STDOUT from previous task: This is my item: one"
}
ok: [localhost] => (item={...output omitted...}) => {
    "msg": "STDOUT from previous task: This is my item: two"
}
...output omitted...
```

#### Running Tasks Conditionally

Ansible can use *conditionals* to execute tasks or plays when certain conditions are met. For example, a conditional can be used to determine available memory on a managed host before Ansible installs or configures a service.

Conditionals allow administrators to differentiate between managed hosts and assign them functional roles based on the conditions that they meet. Playbook variables, registered variables, and Ansible facts can all be tested with conditionals. Operators to compare strings, numeric data, and Boolean values are available.

The following scenarios illustrate the use of conditionals in Ansible:

- A hard limit can be defined in a variable (for example, `min_memory`) and compared against the available memory on a managed host.
- The output of a command can be captured and evaluated by Ansible to determine whether or not a task completed before taking further action. For example, if a program fails, then a batch is skipped.
- Use Ansible facts to determine the managed host network configuration and decide which template file to send (for example, network bonding or trunking).
- The number of CPUs can be evaluated to determine how to properly tune a web server.
- Compare a registered variable with a predefined variable to determine if a service changed. For example, test the MD5 checksum of a service configuration file to see if the service is changed.

**Conditional Task Syntax**

The `when` statement is used to run a task conditionally. It takes as a value the condition to test. If the condition is met, the task runs. If the condition is not met, the task is skipped.

One of the simplest conditions that can be tested is whether a Boolean variable is true or false. The `when` statement in the following example causes the task to run only if `run_my_task` is true:

```
---
- name: Simple Boolean Task Demo
  hosts: all
  vars:
    run_my_task: true

  tasks:
    - name: httpd package is installed
      yum:
        name: httpd
      when: run_my_task
```

The next example is a bit more sophisticated, and tests whether the `my_service` variable has a value. If it does, the value of `my_service` is used as the name of the package to install. If the `my_service` variable is not defined, then the task is skipped without an error.

```
---
- name: Test Variable is Defined Demo
  hosts: all
  vars:
    my_service: httpd

  tasks:
    - name: "{{ my_service }} package is installed"
      yum:
        name: "{{ my_service }}"
      when: my_service is defined
```

The following table shows some of the operations that administrators can use when working with conditionals:



**Table 5.2. Example Conditionals**

| Operation                                                    | Example                                     |
| :----------------------------------------------------------- | :------------------------------------------ |
| Equal (value is a string)                                    | `ansible_machine == "x86_64"`               |
| Equal (value is numeric)                                     | `max_memory == 512`                         |
| Less than                                                    | `min_memory < 128`                          |
| Greater than                                                 | `min_memory > 256`                          |
| Less than or equal to                                        | `min_memory <= 256`                         |
| Greater than or equal to                                     | `min_memory >= 512`                         |
| Not equal to                                                 | `min_memory != 512`                         |
| Variable exists                                              | `min_memory is defined`                     |
| Variable does not exist                                      | `min_memory is not defined`                 |
| Boolean variable is `true`. The values of `1`, `True`, or `yes` evaluate to `true`. | `memory_available`                          |
| Boolean variable is `false`. The values of `0`, `False`, or `no` evaluate to `false`. | `not memory_available`                      |
| First variable's value is present as a value in second variable's list | `ansible_distribution in supported_distros` |



The last entry in the preceding table might be confusing at first. The following example illustrates how it works.

In the example, the `ansible_distribution` variable is a fact determined during the `Gathering Facts` task, and identifies the managed host's operating system distribution. The variable `supported_distros` was created by the playbook author, and contains a list of operating system distributions that the playbook supports. If the value of `ansible_distribution` is in the `supported_distros` list, the conditional passes and the task runs.

```
---
- name: Demonstrate the "in" keyword
  hosts: all
  gather_facts: yes
  vars:
    supported_distros:
      - RedHat
      - Fedora
  tasks:
    - name: Install httpd using yum, where supported
      yum:
        name: http
        state: present
      when: ansible_distribution in supported_distros
```

**IMPORTANT**

Notice the indentation of the `when` statement. Because the `when` statement is not a module variable, it must be placed outside the module by being indented at the top level of the task.

A task is a YAML hash/dictionary, and the `when` statement is simply one more key in the task like the task's name and the module it uses. A common convention places any `when` keyword that might be present after the task's name and the module (and module arguments).

**Testing Multiple Conditions**

One `when` statement can be used to evaluate multiple conditionals. To do so, conditionals can be combined with either the `and` or `or` keywords, and grouped with parentheses.

The following snippets show some examples of how to express multiple conditions.

- If a conditional statement should be met when either condition is true, then you should use the `or` statement. For example, the following condition is met if the machine is running either Red Hat Enterprise Linux or Fedora:

  ```
  when: ansible_distribution == "RedHat" or ansible_distribution == "Fedora"
  ```

- With the `and` operation, both conditions have to be true for the entire conditional statement to be met. For example, the following condition is met if the remote host is a Red Hat Enterprise Linux 7.5 host, and the installed kernel is the specified version:

  ```
  when: ansible_distribution_version == "7.5" and ansible_kernel == "3.10.0-327.el7.x86_64"
  ```

  The `when` keyword also supports using a list to describe a list of conditions. When a list is provided to the `when` keyword, all of the conditionals are combined using the `and` operation. The example below demonstrates another way to combine multiple conditional statements using the `and` operator:

  ```
  when:
    - ansible_distribution_version == "7.5"
    - ansible_kernel == "3.10.0-327.el7.x86_64"
  ```

  This format improves readability, a key goal of well-written Ansible Playbooks.

- More complex conditional statements can be expressed by grouping conditions with parentheses. This ensures that they are correctly interpreted.

  For example, the following conditional statement is met if the machine is running either Red Hat Enterprise Linux 7 or Fedora 28. This example uses the greater-than character (>) so that the long conditional can be split over multiple lines in the playbook, to make it easier to read.

  ```
  when: >
      ( ansible_distribution == "RedHat" and
        ansible_distribution_major_version == "7" )
      or
      ( ansible_distribution == "Fedora" and
      ansible_distribution_major_version == "28" )
  ```

#### Combining Loops and Conditional Tasks

You can combine loops and conditionals.

In the following example, the mariadb-server package is installed by the `yum` module if there is a file system mounted on `/` with more than 300 MB free. The `ansible_mounts` fact is a list of dictionaries, each one representing facts about one mounted file system. The loop iterates over each dictionary in the list, and the conditional statement is not met unless a dictionary is found representing a mounted file system where both conditions are true.

```
- name: install mariadb-server if enough space on root
  yum:
    name: mariadb-server
    state: latest
  loop: "{{ ansible_mounts }}"
  when: item.mount == "/" and item.size_available > 300000000
```

**IMPORTANT**

When you use `when` with `loop` for a task, the `when` statement is checked for each item.

Here is another example that combines conditionals and register variables. The following annotated playbook restarts the `httpd` service only if the `postfix` service is running:

```
---
- name: Restart HTTPD if Postfix is Running
  hosts: all
  tasks:
    - name: Get Postfix server status
      command: /usr/bin/systemctl is-active postfix 
      ignore_errors: yes
      register: result

    - name: Restart Apache HTTPD based on Postfix status
      service:
        name: httpd
        state: restarted
      when: result.rc == 0
```

| [![1](https://rol.redhat.com/rol/static/roc/Common_Content/images/1.svg)](https://rol.redhat.com/rol/app/#task-control-command) | Is Postfix running or not?                                   |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [![2](https://rol.redhat.com/rol/static/roc/Common_Content/images/2.svg)](https://rol.redhat.com/rol/app/#task-control-errors) | If it is not running and the command fails, do not stop processing. |
| [![3](https://rol.redhat.com/rol/static/roc/Common_Content/images/3.svg)](https://rol.redhat.com/rol/app/#task-control-result) | Saves information on the module's result in a variable named `result`. |
| [![4](https://rol.redhat.com/rol/static/roc/Common_Content/images/4.svg)](https://rol.redhat.com/rol/app/#task-control-evaluation) | Evaluates the output of the Postfix task. If the exit code of the **systemctl** command is 0, then Postfix is active and this task restarts the `httpd` service. |

**REFERENCES**

[Loops — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html)

[Tests — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_tests.html)

[Conditionals — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html)

[What Makes A Valid Variable Name — Variables — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#what-makes-a-valid-variable-name)



__________

### Guided Exercise: Writing Loops and Conditional Tasks

In this exercise, you will write a playbook containing tasks that have conditionals and loops.

**Outcomes**

You should be able to:

- Implement Ansible conditionals using the `when` keyword.
- Implement task iteration using the `loop` keyword in conjunction with conditionals.

Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab control-flow start** command. This script creates the working directory, `/home/student/control-flow`.

```
[student@workstation ~]$ lab control-flow start
```

1. On `workstation.lab.example.com`, change to the `/home/student/control-flow` project directory.

   ```
   [student@workstation ~]$ cd ~/control-flow
   [student@workstation control-flow]$ 
   ```

2. The lab script created an Ansible configuration file as well as an inventory file. This inventory file contains the server `servera.lab.example.com` in the `database_dev` host group, and the `serverb.lab.example.com` in the `database_prod` host group. Review the file before proceeding.

   ```
   [student@workstation control-flow]$ cat inventory
   [database_dev]
   servera.lab.example.com
   
   [database_prod]
   serverb.lab.example.com
   ```

3. Create the `playbook.yml` playbook, which contains a play with two tasks. Use the `database_dev` host group. The first task installs the MariaDB required packages, and the second task ensures that the MariaDB service is running.

   1. Open the playbook in a text editor. Define the variable `mariadb_packages` with two values: `mariadb-server`, and `python3-PyMySQL`. The playbook uses the variable to install the required packages. The file should read as follows:

      ```
      ---
      - name: MariaDB server is running
        hosts: database_dev
        vars:
          mariadb_packages:
            - mariadb-server
            - python3-PyMySQL
      ```

   2. Define a task that uses the `yum` module and the variable `mariadb_packages`. The task installs the required packages. The task should read as follows:

      ```
        tasks:
          - name: MariaDB packages are installed
            yum:
              name: "{{ item }}"
              state: present
            loop: "{{ mariadb_packages }}"
      ```

   3. Define a second task to start the `mariadb` service. The task should read as follows:

      ```
          - name: Start MariaDB service
            service:
              name: mariadb
              state: started
              enabled: true
      ```

4. Run the playbook and watch the output of the play.

   ```
   [student@workstation control-flow]$ ansible-playbook playbook.yml
   
   PLAY [MariaDB server is running] ***********************************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [servera.lab.example.com]
   
   TASK [MariaDB packages are installed] ******************************************
   changed: [servera.lab.example.com] => (item=mariadb-server)
   changed: [servera.lab.example.com] => (item=python3-PyMySQL)
   
   TASK [Start MariaDB service] ***************************************************
   changed: [servera.lab.example.com]
   
   PLAY RECAP *********************************************************************
   servera.lab.example.com    : ok=3    changed=2    unreachable=0    failed=0
   ```

5. Update the first task to execute only if the managed host uses Red Hat Enterprise Linux as its operating system. Update the play to use the `database_prod` host group. The task should read as follows:

   ```
   - name: MariaDB server is running
     hosts: database_prod
     vars:
   ...output omitted...
     tasks:
       - name: MariaDB packages are installed
         yum:
           name: "{{ item }}"
           state: present
         loop: "{{ mariadb_packages }}"
         when: ansible_distribution == "RedHat"
   ```

6. Verify that the managed hosts in the `database_prod` host group use Red Hat Enterprise Linux as its operating system.

   ```
   [student@workstation control-flow]$ ansible database_prod -m command \
   > -a 'cat /etc/redhat-release' -u devops --become
   serverb.lab.example.com | CHANGED | rc=0 >>
   Red Hat Enterprise Linux release 8.0 (Ootpa)
   ```

7. Run the playbook again and watch the output of the play.

   ```
   [student@workstation control-flow]$ ansible-playbook playbook.yml
   
   PLAY [MariaDB server is running] ***********************************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [serverb.lab.example.com]
   
   TASK [MariaDB packages are installed] ******************************************
   changed: [serverb.lab.example.com] => (item=mariadb-server)
   changed: [serverb.lab.example.com] => (item=python3-PyMySQL)
   
   TASK [Start MariaDB service] ***************************************************
   changed: [serverb.lab.example.com]
   
   PLAY RECAP *********************************************************************
   serverb.lab.example.com    : ok=3    changed=2    unreachable=0    failed=0
   ```

   Ansible executes the task because `serverb.lab.example.com` uses Red Hat Enterprise Linux.

**Finish**

On `workstation`, run the **lab** `control-flow` `finish` script to clean up the resources created in this exercise.

```
[student@workstation ~]$ lab control-flow finish
```

This concludes the guided exercise.



_________________

### Implementing Handlers

#### Objectives

After completing this section, you should be able to implement a task that runs only when another task changes the managed host.

#### Ansible Handlers

Ansible modules are designed to be *idempotent*. This means that in a properly written playbook, the playbook and its tasks can be run multiple times without changing the managed host unless they need to make a change to get the managed host to the desired state.

However, sometimes when a task does make a change to the system, a further task may need to be run. For example, a change to a service's configuration file may then require that the service be reloaded so that the changed configuration takes effect.

*Handlers* are tasks that respond to a notification triggered by other tasks. Tasks only notify their handlers when the task changes something on a managed host. Each handler has a globally unique name and is triggered at the end of a block of tasks in a playbook. If no task notifies the handler by name then the handler will not run. If one or more tasks notify the handler, the handler will run exactly once after all other tasks in the play have completed. Because handlers are tasks, administrators can use the same modules in handlers that they would use for any other task. Normally, handlers are used to reboot hosts and restart services.

Handlers can be considered as *inactive* tasks that only get triggered when explicitly invoked using a `notify` statement. The following snippet shows how the Apache server is only restarted by the `restart apache` handler when a configuration file is updated and notifies it:

```
tasks:
  - name: copy demo.example.conf configuration template
    template:
      src: /var/lib/templates/demo.example.conf.template
      dest: /etc/httpd/conf.d/demo.example.conf
    notify:
      - restart apache

handlers:
  - name: restart apache
    service:
      name: httpd
      state: restarted
```

| [![1](https://rol.redhat.com/rol/static/roc/Common_Content/images/1.svg)](https://rol.redhat.com/rol/app/#ansible-handlers-task) | The task that notifies the handler.                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [![2](https://rol.redhat.com/rol/static/roc/Common_Content/images/2.svg)](https://rol.redhat.com/rol/app/#ansible-handlers-notify) | The `notify` statement indicates the task needs to trigger a handler. |
| [![3](https://rol.redhat.com/rol/static/roc/Common_Content/images/3.svg)](https://rol.redhat.com/rol/app/#ansible-handlers-notify-name) | The name of the handler to run.                              |
| [![4](https://rol.redhat.com/rol/static/roc/Common_Content/images/4.svg)](https://rol.redhat.com/rol/app/#ansible-handlers-section) | The `handlers` keyword indicates the start of the list of handler tasks. |
| [![5](https://rol.redhat.com/rol/static/roc/Common_Content/images/5.svg)](https://rol.redhat.com/rol/app/#ansible-handlers-name) | The name of the handler invoked by tasks.                    |
| [![6](https://rol.redhat.com/rol/static/roc/Common_Content/images/6.svg)](https://rol.redhat.com/rol/app/#ansible-handlers-module-name) | The module to use for the handler.                           |

In the previous example, the `restart apache` handler triggers when notified by the `template` task that a change happened. A task may call more than one handler in its `notify` section. Ansible treats the `notify` statement as an array and iterates over the handler names:

```
tasks:
  - name: copy demo.example.conf configuration template
    template:
      src: /var/lib/templates/demo.example.conf.template
      dest: /etc/httpd/conf.d/demo.example.conf
    notify:
      - restart mysql
      - restart apache

handlers:
  - name: restart mysql
    service:
      name: mariadb
      state: restarted

  - name: restart apache
    service:
      name: httpd
      state: restarted
```

#### Describing the Benefits of Using Handlers

As discussed in the Ansible documentation, there are some important things to remember about using handlers:

- Handlers always run in the order specified by the `handlers` section of the play. They do not run in the order in which they are listed by `notify` statements in a task, or in the order in which tasks notify them.
- Handlers normally run after all other tasks in the play complete. A handler called by a task in the `tasks` part of the playbook will not run until *all* tasks under `tasks` have been processed. (There are some minor exceptions to this.)
- Handler names exist in a per-play namespace. If two handlers are incorrectly given the same name, only one will run.
- Even if more than one task notifies a handler, the handler only runs once. If no tasks notify it, a handler will not run.
- If a task that includes a `notify` statement does not report a `changed` result (for example, a package is already installed and the task reports `ok`), the handler is not notified. The handler is skipped unless another task notifies it. Ansible notifies handlers only if the task reports the `changed` status.

**IMPORTANT**

Handlers are meant to perform an extra action when a task makes a change to a managed host. They should not be used as a replacement for normal tasks.

**REFERENCES**

[Intro to Playbooks — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html)



________________________

### Guided Exercise: Implementing Handlers

In this exercise, you will implement handlers in playbooks.

**Outcomes**

You should be able to define handlers in playbooks and notify them for configuration change.

Run **lab control-handlers start** on `workstation` to configure the environment for the exercise. This script creates the `/home/student/control-handlers` project directory and downloads the Ansible configuration file and the host inventory file needed for the exercise. The project directory also contains a partially complete playbook, `configure_db.yml`.

```
[student@workstation ~]$ lab control-handlers start
```

1. On `workstation.lab.example.com`, open a new terminal and change to the `/home/student/control-handlers` project directory.

   ```
   [student@workstation ~]$ cd ~/control-handlers
   [student@workstation control-handlers]$ 
   ```

2. In that directory, use a text editor to edit the `configure_db.yml` playbook file. This playbook installs and configures a database server. When the database server configuration changes, the playbook triggers a restart of the database service and configures the database administrative password.

   1. Using a text editor, review the `configure_db.yml` playbook. It begins with the initialization of some variables:

      ```
      ---
      - name: MariaDB server is installed
        hosts: databases
        vars:
          db_packages: 
            - mariadb-server
            - python3-PyMySQL
          db_service: mariadb 
          resources_url: http://materials.example.com/labs/control-handlers 
          config_file_url: "{{ resources_url }}/my.cnf.standard" 
          config_file_dst: /etc/my.cnf 
        tasks:
      ```

      | [![1](https://rol.redhat.com/rol/static/roc/Common_Content/images/1.svg)](https://rol.redhat.com/rol/app/#control-handlers-practice-db-packages) | `db_packages` defines the name of the packages to install for the database service. |
      | ------------------------------------------------------------ | ------------------------------------------------------------ |
      | [![2](https://rol.redhat.com/rol/static/roc/Common_Content/images/2.svg)](https://rol.redhat.com/rol/app/#control-handlers-practice-db-service) | `db_service` defines the name of the database service.       |
      | [![3](https://rol.redhat.com/rol/static/roc/Common_Content/images/3.svg)](https://rol.redhat.com/rol/app/#control-handlers-practice-resources-url) | `resources_url` represents the URL for the resource directory where remote configuration files are located. |
      | [![4](https://rol.redhat.com/rol/static/roc/Common_Content/images/4.svg)](https://rol.redhat.com/rol/app/#control-handlers-practice-config-file-url) | `config_file_url` represents the URL of the database configuration file to install. |
      | [![5](https://rol.redhat.com/rol/static/roc/Common_Content/images/5.svg)](https://rol.redhat.com/rol/app/#control-handlers-practice-config-file-dst) | `config_file_dst`: Location of the installed configuration file on the managed hosts. |

   2. In the `configure_db.yml` file, define a task that uses the `yum` module to install the required database packages as defined by the `db_packages` variable. If the task changes the system, the database was not installed, and you need to notify the `set db password` handler to set your initial database user and password. Remember that the handler task, if it is notified, will not run until every task in the `tasks` section has run.

      The task should read as follows:

      ```
        tasks:
          - name: "{{ db_packages }} packages are installed"
            yum:
              name: "{{ db_packages }}"
              state: present
            notify:
              - set db password
      ```

   3. Add a task to start and enable the database service. The task should read as follows:

      ```
          - name: Make sure the database service is running
            service:
              name: "{{ db_service }}"
              state: started
              enabled: true
      ```

   4. Add a task to download `my.cnf.standard` to `/etc/my.cnf` on the managed host, using the `get_url` module. Add a condition that notifies the `restart db service` handler to restart the database service after a configuration file change. The task should read:

      ```
          - name: The {{ config_file_dst }} file has been installed
            get_url:
              url: "{{ config_file_url }}"
              dest: "{{ config_file_dst }}"
              owner: mysql
              group: mysql
              force: yes
            notify:
              - restart db service
      ```

   5. Add the `handlers` keyword to define the start of the handler tasks. Define the first handler, `restart db service`, which restarts the `mariadb` service. It should read as follows:

      ```
        handlers:
          - name: restart db service
            service:
              name: "{{ db_service }}"
              state: restarted
      ```

   6. Define the second handler, `set db password`, which sets the administrative password for the database service. The handler uses the `mysql_user` module to perform the command. The handler should read as follows:

      ```
          - name: set db password
            mysql_user:
              name: root
              password: redhat
      ```

   When completed, the playbook should appear as follows:

   ```
   ---
   - name: MariaDB server is installed
     hosts: databases
     vars:
       db_packages:
         - mariadb-server
         - python3-PyMySQL
       db_service: mariadb
       resources_url: http://materials.example.com/labs/control-handlers
       config_file_url: "{{ resources_url }}/my.cnf.standard"
       config_file_dst: /etc/my.cnf
     tasks:
       - name: "{{ db_packages }} packages are installed"
         yum:
           name: "{{ db_packages }}"
           state: present
         notify:
           - set db password
   
       - name: Make sure the database service is running
         service:
           name: "{{ db_service }}"
           state: started
           enabled: true
   
       - name: The {{ config_file_dst }} file has been installed
         get_url:
           url: "{{ config_file_url }}"
           dest: "{{ config_file_dst }}"
           owner: mysql
           group: mysql
           force: yes
         notify:
           - restart db service
   
     handlers:
       - name: restart db service
         service:
           name: "{{ db_service }}"
           state: restarted
   
       - name: set db password
         mysql_user:
           name: root
           password: redhat
   ```

3. Before running the playbook, verify that its syntax is correct by running **ansible-playbook** with the `--syntax-check` option. If it reports any errors, correct them before moving to the next step. You should see output similar to the following:

   ```
   [student@workstation control-handlers]$ ansible-playbook configure_db.yml \
   > --syntax-check
   
   playbook: configure_db.yml
   ```

4. Run the `configure_db.yml` playbook. The output shows that the handlers are being executed.

   ```
   [student@workstation control-handlers]$ ansible-playbook configure_db.yml
   
   PLAY [Installing MariaDB server] *********************************************
   
   TASK [Gathering Facts] *******************************************************
   ok: [servera.lab.example.com]
   
   TASK [['mariadb-server', 'python3-PyMySQL'] packages are installed] **********
   changed: [servera.lab.example.com]
   
   TASK [Make sure the database service is running] *****************************
   changed: [servera.lab.example.com]
   
   TASK [The /etc/my.cnf file has been installed] *******************************
   changed: [servera.lab.example.com]
   
   RUNNING HANDLER [restart db service] *****************************************
   changed: [servera.lab.example.com]
   
   RUNNING HANDLER [set db password] ********************************************
   changed: [servera.lab.example.com]
   
   PLAY RECAP *******************************************************************
   servera.lab.example.com    : ok=6    changed=5    unreachable=0    failed=0
   ```

5. Run the playbook again.

   ```
   [student@workstation control-handlers]$ ansible-playbook configure_db.yml
   
   PLAY [Installing MariaDB server] *********************************************
   
   TASK [Gathering Facts] *******************************************************
   ok: [servera.lab.example.com]
   
   TASK [['mariadb-server', 'python3-PyMySQL'] packages are installed] **********
   ok: [servera.lab.example.com]
   
   TASK [Make sure the database service is running] *****************************
   ok: [servera.lab.example.com]
   
   TASK [The /etc/my.cnf file has been installed] *******************************
   ok: [servera.lab.example.com]
   
   PLAY RECAP *******************************************************************
   servera.lab.example.com    : ok=4    changed=0    unreachable=0    failed=0
   ```

   This time the handlers are skipped. In the event that the remote configuration file is changed in the future, executing the playbook would trigger the `restart db service` handler but not the `set db password` handler.

**Finish**

On `workstation`, run the **lab** `control-handlers` `finish` script to clean up the resources created in this exercise.

```
[student@workstation ~]$ lab control-handlers finish
```

This concludes the guided exercise.



____

### Handling Task Failure

#### Objectives

After completing this section, you should be able to control what happens when a task fails, and what conditions cause a task to fail.

#### Managing Task Errors in Plays

Ansible evaluates the return code of each task to determine whether the task succeeded or failed. Normally, when a task fails Ansible immediately aborts the rest of the play on that host, skipping all subsequent tasks.

However, sometimes you might want to have play execution continue even if a task fails. For example, you might expect that a particular task could fail, and you might want to recover by running some other task conditionally. There are a number of Ansible features that can be used to manage task errors.

**Ignoring Task Failure**

By default, if a task fails, the play is aborted. However, this behavior can be overridden by ignoring failed tasks. You can use the `ignore_errors` keyword in a task to accomplish this.

The following snippet shows how to use `ignore_errors` in a task to continue playbook execution on the host even if the task fails. For example, if the notapkg package does not exist then the yum module fails, but having `ignore_errors` set to `yes` allows execution to continue.

```
- name: Latest version of notapkg is installed
  yum:
    name: notapkg
    state: latest
  ignore_errors: yes
```

**Forcing Execution of Handlers after Task Failure**

Normally when a task fails and the play aborts on that host, any handlers that had been notified by earlier tasks in the play will not run. If you set the `force_handlers: yes` keyword on the play, then notified handlers are called even if the play aborted because a later task failed.

The following snippet shows hows to use the `force_handlers` keyword in a play to force execution of the handler even if a task fails:

```
---
- hosts: all
  force_handlers: yes
  tasks:
    - name: a task which always notifies its handler
      command: /bin/true
      notify: restart the database

    - name: a task which fails because the package doesn't exist
      yum:
        name: notapkg
        state: latest

  handlers:
    - name: restart the database
      service:
        name: mariadb
        state: restarted
```

**NOTE**

Remember that handlers are notified when a task reports a `changed` result but are not notified when it reports an `ok` or `failed` result.

**Specifying Task Failure Conditions**

You can use the `failed_when` keyword on a task to specify which conditions indicate that the task has failed. This is often used with command modules that may successfully execute a command, but the command's output indicates a failure.

For example, you can run a script that outputs an error message and use that message to define the failed state for the task. The following snippet shows how the `failed_when` keyword can be used in a task:

```
tasks:
  - name: Run user creation script
    shell: /usr/local/bin/create_users.sh
    register: command_result
    failed_when: "'Password missing' in command_result.stdout"
```

The `fail` module can also be used to force a task failure. The above scenario can alternatively be written as two tasks:

```
tasks:
  - name: Run user creation script
    shell: /usr/local/bin/create_users.sh
    register: command_result
    ignore_errors: yes

  - name: Report script failure
    fail:
      msg: "The password is missing in the output"
    when: "'Password missing' in command_result.stdout"
```

You can use the `fail` module to provide a clear failure message for the task. This approach also enables delayed failure, allowing you to run intermediate tasks to complete or roll back other changes.

**Specifying When a Task Reports “Changed” Results**

When a task makes a change to a managed host, it reports the `changed` state and notifies handlers. When a task does not need to make a change, it reports `ok` and does not notify handlers.

The `changed_when` keyword can be used to control when a task reports that it has changed. For example, the `shell` module in the next example is being used to get a Kerberos credential which will be used by subsequent tasks. It normally would always report `changed` when it runs. To suppress that change, `changed_when: false` is set so that it only reports `ok` or `failed`.

```
  - name: get Kerberos credentials as "admin"
    shell: echo "{{ krb_admin_pass }}" | kinit -f admin
    changed_when: false
```

The following example uses the `shell` module to report `changed` based on the output of the module that is collected by a registered variable:

```
tasks:
  - shell:
      cmd: /usr/local/bin/upgrade-database
    register: command_result
    changed_when: "'Success' in command_result.stdout"
    notify:
      - restart_database

handlers:
  - name: restart_database
     service:
       name: mariadb
       state: restarted
```

**Ansible Blocks and Error Handling**

In playbooks, *blocks* are clauses that logically group tasks, and can be used to control how tasks are executed. For example, a task block can have a `when` keyword to apply a conditional to multiple tasks:

```
- name: block example
  hosts: all
  tasks:
    - name: installing and configuring Yum versionlock plugin 
      block:
      - name: package needed by yum
        yum:
          name: yum-plugin-versionlock
          state: present
      - name: lock version of tzdata
        lineinfile:
          dest: /etc/yum/pluginconf.d/versionlock.list
          line: tzdata-2016j-1
          state: present
      when: ansible_distribution == "RedHat"
```

Blocks also allow for error handling in combination with the `rescue` and `always` statements. If any task in a block fails, tasks in its `rescue` block are executed in order to recover. After the tasks in the block clause run, as well as the tasks in the rescue clause if there was a failure, then tasks in the `always` clause run. To summarize:

- `block`: Defines the main tasks to run.
- `rescue`: Defines the tasks to run if the tasks defined in the `block` clause fail.
- `always`: Defines the tasks that will always run independently of the success or failure of tasks defined in the `block` and `rescue` clauses.

The following example shows how to implement a block in a playbook. Even if tasks defined in the `block` clause fail, tasks defined in the `rescue` and `always` clauses are executed.

```
  tasks:
    - name: Upgrade DB
      block:
        - name: upgrade the database
          shell:
            cmd: /usr/local/lib/upgrade-database
      rescue:
        - name: revert the database upgrade
          shell:
            cmd: /usr/local/lib/revert-database
      always:
        - name: always restart the database
          service:
            name: mariadb
            state: restarted
```

The `when` condition on a `block` clause also applies to its `rescue` and `always` clauses if present.

**REFERENCES**

[Error Handling in Playbooks — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_error_handling.html)

[Error Handling — Blocks — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_blocks.html#error-handling)



__________________

### Guided Exercise: Handling Task Failure

In this exercise, you will explore different ways to handle task failure in an Ansible Playbook.

**Outcomes**

You should be able to:

- Ignore failed commands during the execution of playbooks.
- Force execution of handlers.
- Override what constitutes a failure in tasks.
- Override the `changed` state for tasks.
- Implement `block`, `rescue`, and `always` in playbooks.

On `workstation`, run the lab start script to confirm the environment is ready for the lab to begin. This script creates the working directory, `/home/student/control-errors`.

```
[student@workstation ~]$ lab control-errors start
```

1. On `workstation.lab.example.com`, change to the `/home/student/control-errors` project directory.

   ```
   [student@workstation ~]$ cd ~/control-errors
   [student@workstation control-errors]$ 
   ```

2. The lab script created an Ansible configuration file as well as an inventory file that contains the server `servera.lab.example.com` in the `databases` group. Review the file before proceeding.

3. Create the `playbook.yml` playbook, which contains a play with two tasks. Write the first task with a deliberate error to cause failure.

   1. Open the playbook in a text editor. Define three variables: `web_package` with a value of `http`, `db_package` with a value of `mariadb-server`, and `db_service` with a value of `mariadb`. These variables will be used to install the required packages and start the server.

      The `http` value is an intentional error in the package name. The file should read as follows:

      ```
      ---
      - name: Task Failure Exercise
        hosts: databases
        vars:
          web_package: http
          db_package: mariadb-server
          db_service: mariadb
      ```

   2. Define two tasks that use the `yum` module and the two variables, `web_package` and `db_package`. The tasks will install the required packages. The tasks should read as follows:

      ```
        tasks:
          - name: Install {{ web_package }} package
            yum:
              name: "{{ web_package }}"
              state: present
      
          - name: Install {{ db_package }} package
            yum:
              name: "{{ db_package }}"
              state: present
      ```

4. Run the playbook and watch the output of the play.

   ```
   [student@workstation control-errors]$ ansible-playbook playbook.yml
   
   PLAY [Task Failure Exercise] ***************************************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [servera.lab.example.com]
   
   TASK [Install http package] ****************************************************
   fatal: [servera.lab.example.com]: FAILED! => {"changed": false, "failures": ["No package http available."], "msg": "Failed to install some of the specified packages", "rc": 1, "results": []}
   
   PLAY RECAP *********************************************************************
   servera.lab.example.com    : ok=1    changed=0    unreachable=0    failed=1 
   ```

   The task failed because there is no existing package called `http`. Because the first task failed, the second task was not run.

5. Update the first task to ignore any errors by adding the `ignore_errors` keyword. The tasks should read as follows:

   ```
     tasks:
       - name: Install {{ web_package }} package
         yum:
           name: "{{ web_package }}"
           state: present
         ignore_errors: yes
   
       - name: Install {{ db_package }} package
         yum:
           name: "{{ db_package }}"
           state: present
   ```

6. Run the playbook again and watch the output of the play.

   ```
   [student@workstation control-errors]$ ansible-playbook playbook.yml
   
   PLAY [Task Failure Exercise] ***************************************************
   
   TASK [Gathering Facts] *********************************************************
   ok: [servera.lab.example.com]
   
   TASK [Install http package] ****************************************************
   fatal: [servera.lab.example.com]: FAILED! => {"changed": false, "failures": ["No package http available."], "msg": "Failed to install some of the specified packages", "rc": 1, "results": []}
   ...ignoring
   
   TASK [Install mariadb-server package] ******************************************
   changed: [servera.lab.example.com]
   
   PLAY RECAP *********************************************************************
   servera.lab.example.com    : ok=3    changed=1    unreachable=0    failed=0
   ```

   Despite the fact that the first task failed, Ansible executed the second one.

7. In this step, you will set up a `block` keyword so you can experiment with how they work.

   1. Update the playbook by nesting the first task in a `block` clause. Remove the line that sets `ignore_errors: yes`. The block should read as follows:

      ```
          - name: Attempt to set up a webserver
            block:
              - name: Install {{ web_package }} package
                yum:
                  name: "{{ web_package }}"
                  state: present
      ```

   2. Nest the task that installs the mariadb-server package in a `rescue` clause. The task will execute if the task listed in the `block` clause fails. The block clause should read as follows:

      ```
            rescue:
              - name: Install {{ db_package }} package
                yum:
                  name: "{{ db_package }}"
                  state: present
      ```

   3. Finally, add an `always` clause to start the database server upon installation using the `service` module. The clause should read as follows:

      ```
            always:
              - name: Start {{ db_service }} service
                service:
                  name: "{{ db_service }}"
                  state: started
      ```

   4. The completed task section should read as follows:

      ```
        tasks:
          - name: Attempt to set up a webserver
            block:
              - name: Install {{ web_package }} package
                yum:
                  name: "{{ web_package }}"
                  state: present
            rescue:
              - name: Install {{ db_package }} package
                yum:
                  name: "{{ db_package }}"
                  state: present
            always:
              - name: Start {{ db_service }} service
                service:
                  name: "{{ db_service }}"
                  state: started
      ```

8. Now run the playbook again and observe the output.

   1. Run the playbook. The task in the block that makes sure `web_package` is installed fails, which causes the task in the `rescue` block to run. Then the task in the `always` block runs.

      ```
      [student@workstation control-errors]$ ansible-playbook playbook.yml
      
      PLAY [Task Failure Exercise] ***************************************************
      
      TASK [Gathering Facts] *********************************************************
      ok: [servera.lab.example.com]
      
      TASK [Install http package] ****************************************************
      fatal: [servera.lab.example.com]: FAILED! => {"changed": false, "failures": ["No package http available."], "msg": "Failed to install some of the specified packages", "rc": 1, "results": []}
      
      TASK [Install mariadb-server package] ******************************************
      ok: [servera.lab.example.com]
      
      TASK [Start mariadb service] ***************************************************
      changed: [servera.lab.example.com]
      
      PLAY RECAP *********************************************************************
      servera.lab.example.com    : ok=3    changed=1    unreachable=0    failed=1
      ```

   2. Edit the playbook, correcting the value of the `web_package` variable to read `httpd`. That will cause the task in the block to succeed the next time you run the playbook.

      ```
        vars:
          web_package: httpd
          db_package: mariadb-server
          db_service: mariadb
      ```

   3. Run the playbook again. This time, the task in the block does not fail. This causes the task in the `rescue` section to be ignored. The task in the `always` will still run.

      ```
      [student@workstation control-errors]$ ansible-playbook playbook.yml
      
      PLAY [Task Failure Exercise] ***************************************************
      
      TASK [Gathering Facts] *********************************************************
      ok: [servera.lab.example.com]
      
      TASK [Install httpd package] ***************************************************
      changed: [servera.lab.example.com]
      
      TASK [Start mariadb service] ***************************************************
      ok: [servera.lab.example.com]
      
      PLAY RECAP *********************************************************************
      servera.lab.example.com    : ok=3    changed=1    unreachable=0    failed=0
      ```

9. This step explores how to control the condition that causes a task to be reported as “changed” to the managed host.

   1. Edit the playbook to add two tasks to the start of the play, preceding the `block`. The first task uses the `command` module to run the **date** command and register the result in the `command_result` variable. The second task uses the `debug` module to print the standard output of the first task's command.

      ```
        tasks:
          - name: Check local time
            command: date
            register: command_result
      
          - name: Print local time
            debug:
              var: command_result.stdout
      ```

   2. Run the playbook. You should see that the first task, which runs the `command` module, reports `changed`, even though it did not change the remote system; it only collected information about the time. That is because the `command` module cannot tell the difference between a command that collects data and a command that changes state.

      ```
      [student@workstation control-errors]$ ansible-playbook playbook.yml
      
      PLAY [Task Failure Exercise] ***************************************************
      
      TASK [Gathering Facts] *********************************************************
      ok: [servera.lab.example.com]
      
      TASK [Check local time] ********************************************************
      changed: [servera.lab.example.com]
      
      TASK [Print local time] ********************************************************
      ok: [servera.lab.example.com] => {
          "command_result.stdout": "mié mar 27 08:07:08 EDT 2019"
      }
      
      TASK [Install httpd package] ***************************************************
      ok: [servera.lab.example.com]
      
      TASK [Start mariadb service] ***************************************************
      ok: [servera.lab.example.com]
      
      PLAY RECAP *********************************************************************
      servera.lab.example.com    : ok=5    changed=1    unreachable=0    failed=0
      ```

      If you run the playbook again, the `Check local time` task returns `changed` again.

   3. That `command` task should not report `changed` every time it runs because it is not changing the managed host. Because you know that the task will never change a managed host, add the line `changed_when: false` to the task to suppress the change.

      ```
        tasks:
          - name: Check local time
            command: date
            register: command_result
            changed_when: false
      
          - name: Print local time
            debug:
              var: command_result.stdout
      ```

   4. Run the playbook again and notice that the task now reports `ok`, but the task is still being run and is still saving the time in the variable.

      ```
      [student@workstation control-errors]$ ansible-playbook playbook.yml
      
      PLAY [Task Failure Exercise] ***************************************************
      
      TASK [Gathering Facts] *********************************************************
      ok: [servera.lab.example.com]
      
      TASK [Check local time] ********************************************************
      ok: [servera.lab.example.com]
      
      TASK [Print local time] ********************************************************
      ok: [servera.lab.example.com] => {
          "command_result.stdout": "mié mar 27 08:08:36 EDT 2019"
      }
      
      TASK [Install httpd package] ***************************************************
      ok: [servera.lab.example.com]
      
      TASK [Start mariadb service] ***************************************************
      ok: [servera.lab.example.com]
      
      PLAY RECAP *********************************************************************
      servera.lab.example.com    : ok=5    changed=0    unreachable=0    failed=0
      ```

10. As a final exercise, edit the playbook to explore how the `failed_when` keyword interacts with tasks.

    1. Edit the `Install {{ web_package }} package` task so that it reports as having failed when `web_package` has the value `httpd`. Because this is the case, the task will report failure when you run the play.

       Be careful with your indentation to make sure the keyword is correctly set on the task.

       ```
           - block:
               - name: Install {{ web_package }} package
                 yum:
                   name: "{{ web_package }}"
                   state: present
                 failed_when: web_package == "httpd"
       ```

    2. Run the playbook.

       ```
       [student@workstation control-errors]$ ansible-playbook playbook.yml
       
       PLAY [Task Failure Exercise] ***************************************************
       
       TASK [Gathering Facts] *********************************************************
       ok: [servera.lab.example.com]
       
       TASK [Check local time] ********************************************************
       ok: [servera.lab.example.com]
       
       TASK [Print local time] ********************************************************
       ok: [servera.lab.example.com] => {
           "command_result.stdout": "mié mar 27 08:09:35 EDT 2019"
       }
       
       TASK [Install httpd package] ***************************************************
       fatal: [servera.lab.example.com]: FAILED! => {"changed": false, "failed_when_result": true, "msg": "Nothing to do", "rc": 0, "results": ["Installed: httpd"]}
       
       TASK [Install mariadb-server package] ******************************************
       ok: [servera.lab.example.com]
       
       TASK [Start mariadb service] ***************************************************
       ok: [servera.lab.example.com]
       
       PLAY RECAP *********************************************************************
       servera.lab.example.com    : ok=5    changed=0    unreachable=0    failed=1
       ```

       Look carefully at the output. The `Install httpd package` task *reports*that it failed, but it actually ran and made sure the package is installed first. The `failed_when` keyword changes the status the task reports *after*the task runs; it does not change the behavior of the task itself.

       However, the reported failure might change the behavior of the rest of the play. Because that task was in a block and reported that it failed, the `Install mariadb-server package` task in the block's `rescue` section was run.

**Finish**

On `workstation`, run the **lab** `control-errors` `finish` script to clean up the resources created in this exercise.

```
[student@workstation ~]$ lab control-errors finish
```

This concludes the guided exercise.



______________________________

### Lab: Implementing Task Control

**Performance Checklist**

In this lab, you will install the Apache web server and secure it using `mod_ssl`. You will use conditions, handlers, and task failure handling in your playbook to deploy the environment.

**Outcomes**

You should be able to define conditionals in Ansible Playbooks, set up loops that iterate over elements, define handlers in playbooks, and handle task errors.

Log in as the `student` user on `workstation` and run **lab** `control-review` `start`. This script ensures that the managed host, `serverb`, is reachable on the network. It also ensures that the correct Ansible configuration file and inventory are installed on the control node.

```
[student@workstation ~]$ lab control-review start
```

1. On `workstation`, change to the `/home/student/control-review` project directory.

   ```
   [student@workstation ~]$ cd ~/control-review
   [student@workstation control-review]$ 
   ```

   

2. The project directory contains a partially completed playbook, `playbook.yml`. Using a text editor, add a task that uses the `fail` module under the `#Fail Fast Message` comment. Be sure to provide an appropriate name for the task. This task should only be executed when the remote system does not meet the minimum requirements.

   The minimum requirements for the remote host are listed below:

   - Has at least the amount of RAM specified by the `min_ram_mb` variable. The `min_ram_mb` variable is defined in the `vars.yml` file and has a value of `256`.
   - Is running Red Hat Enterprise Linux.

   The completed task matches:

   ```
     tasks:
       #Fail Fast Message
       - name: Show Failed System Requirements Message
         fail:
           msg: "The {{ inventory_hostname }} did not meet minimum reqs."
         when: >
           ansible_memtotal_mb < min_ram_mb or
           ansible_distribution != "RedHat"
   ```

   

3. Add a single task to the playbook under the `#Install all Packages` comment to install the latest version of any missing packages. Required packages are specified by the `packages` variable, which is defined in the `vars.yml` file.

   The task name should be `Ensure required packages are present`.

   The completed task matches:

   ```
       #Install all Packages
       - name: Ensure required packages are present
         yum:
           name: "{{ packages }}"
           state: latest
   ```

   

4. Add a single task to the playbook under the `#Enable and start services` comment to start all services. All services specified by the `services` variable, which is defined in the `vars.yml` file, should be started and enabled. Be sure to provide an appropriate name for the task.

   The completed task matches:

   ```
       #Enable and start services
       - name: Ensure services are started and enabled
         service:
           name: "{{ item }}"
           state: started
           enabled: yes
         loop: "{{ services }}"
   ```

   

5. Add a task block to the playbook under the `#Block of config tasks` comment. This block contains two tasks:

   - A task to ensure the directory specified by the `ssl_cert_dir` variable exists on the remote host. This directory stores the web server's certificates.

   - A task to copy all files specified by the `web_config_files` variable to the remote host. Examine the structure of the `web_config_files` variable in the `vars.yml` file. Configure the task to copy each file to the correct destination on the remote host.

     This task should trigger the `restart web service` handler if any of these files are changed on the remote server.

   Additionally, a debug task is executed if either of the two tasks above fail. In this case, the task prints the message: `One or more of the configuration changes failed, but the web service is still active.`.

   Be sure to provide an appropriate name for all tasks.

   The completed task block matches below:

   ```
       #Block of config tasks
       - name: Setting up the SSL cert directory and config files
         block: 
           - name: Create SSL cert directory
             file:
               path: "{{ ssl_cert_dir }}"
               state: directory
   
           - name: Copy Config Files
             copy:
               src: "{{ item.src }}"
               dest: "{{ item.dest }}"
             loop: "{{ web_config_files }}"
             notify: restart web service
   
         rescue:
           - name: Configuration Error Message
             debug:
               msg: >
                 One or more of the configuration
                 changes failed, but the web service
                 is still active.
   ```

   

6. The playbook configures the remote host to listen for standard HTTPS requests. Add a single task to the playbook under the `#Configure the firewall` comment to configure `firewalld`.

   This task should ensure that the remote host allows standard HTTP and HTTPS connections. These configuration changes should be effective immediately and persist after a system reboot. Be sure to provide an appropriate name for the task.

   The completed task matches:

   ```
       #Configure the firewall
       - name: ensure web server ports are open
         firewalld:
           service: "{{ item }}"
           immediate: true
           permanent: true
           state: enabled
         loop:
           - http
           - https
   ```

   

7. Define the `restart web service` handler.

   When triggered, this task should restart the web service defined by the `web_service` variable, defined in the `vars.yml` file.

   A `handlers` section is added to the end of the playbook:

   ```
     handlers:
       - name: restart web service
         service:
           name: "{{ web_service }}"
           state: restarted
   ```

   The completed playbook contains:

   ```
   ---
   - name: Playbook Control Lab
     hosts: webservers
     vars_files: vars.yml
     tasks:
       #Fail Fast Message
       - name: Show Failed System Requirements Message
         fail:
           msg: "The {{ inventory_hostname }} did not meet minimum reqs."
         when: >
           ansible_memtotal_mb < min_ram_mb or
           ansible_distribution != "RedHat"
   
       #Install all Packages
       - name: Ensure required packages are present
         yum:
           name: "{{ packages }}"
           state: latest
   
       #Enable and start services
       - name: Ensure services are started and enabled
         service:
           name: "{{ item }}"
           state: started
           enabled: yes
         loop: "{{ services }}"
   
       #Block of config tasks
       - name: Setting up the SSL cert directory and config files 
         block:
           - name: Create SSL cert directory
             file:
               path: "{{ ssl_cert_dir }}"
               state: directory
   
           - name: Copy Config Files
             copy:
               src: "{{ item.src }}"
               dest: "{{ item.dest }}"
             loop: "{{ web_config_files }}"
             notify: restart web service
   
         rescue:
           - name: Configuration Error Message
             debug:
               msg: >
                 One or more of the configuration
                 changes failed, but the web service
                 is still active.
   
       #Configure the firewall
       - name: ensure web server ports are open
         firewalld:
           service: "{{ item }}"
           immediate: true
           permanent: true
           state: enabled
         loop:
           - http
           - https
   
     #Add handlers
     handlers:
       - name: restart web service
         service:
           name: "{{ web_service }}"
           state: restarted
   ```

   

8. From the project directory, `~/control-review`, run the `playbook.yml` playbook. The playbook should execute without errors, and trigger the execution of the handler task.

   ```
   [student@workstation control-review]$ ansible-playbook playbook.yml
   
   PLAY [Playbook Control Lab] **************************************************
   
   TASK [Gathering Facts] *******************************************************
   ok: [serverb.lab.example.com]
   
   TASK [Show Failed System Requirements Message] *******************************
   skipping: [serverb.lab.example.com]
   
   TASK [Ensure required packages are present] **********************************
   changed: [serverb.lab.example.com]
   
   TASK [Ensure services are started and enabled] *******************************
   changed: [serverb.lab.example.com] => (item=httpd)
   ok: [serverb.lab.example.com] => (item=firewalld)
   
   TASK [Create SSL cert directory] *********************************************
   changed: [serverb.lab.example.com]
   
   TASK [Copy Config Files] *****************************************************
   changed: [serverb.lab.example.com] => (item={'src': 'server.key', 'dest': '/etc/httpd/conf.d/ssl'})
   changed: [serverb.lab.example.com] => (item={'src': 'server.crt', 'dest': '/etc/httpd/conf.d/ssl'})
   changed: [serverb.lab.example.com] => (item={'src': 'ssl.conf', 'dest': '/etc/httpd/conf.d'})
   changed: [serverb.lab.example.com] => (item={'src': 'index.html', 'dest': '/var/www/html'})
   
   TASK [ensure web server ports are open] **************************************
   changed: [serverb.lab.example.com] => (item=http)
   changed: [serverb.lab.example.com] => (item=https)
   
   RUNNING HANDLER [restart web service] ****************************************
   changed: [serverb.lab.example.com]
   
   PLAY RECAP *******************************************************************
   serverb.lab.example.com    : ok=7    changed=6    unreachable=0    failed=0
   ```

   

9. Verify that the web server now responds to HTTPS requests, using the self-signed custom certificate to encrypt the connection. The web server response should match the string `Configured for both HTTP and HTTPS.`

   ```
   [student@workstation control-review]$ curl -k -vvv https://serverb.lab.example.com
   * About to connect() to serverb.lab.example.com port 443 (#0)
   *   Trying 172.25.250.11...
   * Connected to serverb.lab.example.com (172.25.250.11) port 443 (#0)
   * Initializing NSS with certpath: sql:/etc/pki/nssdb
   * skipping SSL peer certificate verification
   * SSL connection using TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
   * Server certificate:
   ...output omitted...
   * 	start date: Nov 13 15:52:18 2018 GMT
   * 	expire date: Aug 09 15:52:18 2021 GMT
   * 	common name: serverb.lab.example.com
   ...output omitted...
   < Accept-Ranges: bytes
   < Content-Length: 36
   < Content-Type: text/html; charset=UTF-8
   <
   Configured for both HTTP and HTTPS.
   * Connection #0 to host serverb.lab.example.com left intact
   ```

   

**Evaluation**

Run the **lab control-review grade** command on `workstation` to confirm success on this exercise. Correct any reported failures and rerun the script until successful.

```
[student@workstation ~]$ lab control-review grade
```

**Finish**

Run the **lab** `control-review` `finish` command to clean up after the lab.

```
[student@workstation ~]$ lab control-review finish
```

This concludes the lab.



______

###  Summary

In this chapter, you learned:

- Loops are used to iterate over a set of values, for example, a simple list of strings, or a list of either hashes or dictionaries.
- Conditionals are used to execute tasks or plays only when certain conditions have been met.
- Handlers are special tasks that execute at the end of the play if notified by other tasks.
- Handlers are only notified when a task reports that it changed something on a managed host.
- Tasks are configured to handle error conditions by ignoring task failure, forcing handlers to be called even if the task failed, mark a task as failed when it succeeded, or override the behavior that causes a task to be marked as changed.
- Blocks are used to group tasks as a unit and to execute other tasks depending upon whether or not all the tasks in the block succeed.



______

## Chapter 6. Deploying Files to Managed Hosts

- [Modifying and Copying Files to Hosts](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch06)
- [Guided Exercise: Modifying and Copying Files to Hosts](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch06s02)
- [Deploying Custom Files with Jinja2 Templates](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch06s03)
- [Guided Exercise: Deploying Custom Files with Jinja2 Templates](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch06s04)
- [Lab: Deploying Files to Managed Hosts](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch06s05)
- [Summary](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch06s06)

**Abstract**

| **Goal**       | Deploy, manage, and adjust files on hosts managed by Ansible. |
| -------------- | ------------------------------------------------------------ |
| **Objectives** | Create, install, edit, and remove files on managed hosts, and manage permissions, ownership, SELinux context, and other characteristics of those files.Customize files using Jinja2 templates, and deploy them to managed hosts. |
| **Sections**   | Modifying and Copying Files to Hosts (and Guided Exercise)Deploying Custom Files with Jinja2 Templates (and Guided Exercise) |
| **Lab**        | Deploying Files to Managed Hosts                             |



### Modifying and Copying Files to Hosts

#### Objectives

After completing this section, you should be able to create, install, edit, and remove files on managed hosts, and manage permissions, ownership, SELinux context, and other characteristics of those files.

#### Describing Files Modules

Red Hat Ansible Engine ships with a large collection of modules (the “module library”) that are developed as part of the upstream Ansible project. To make it easier to organize, document, and manage them, they are organized into groups based on function in the documentation and when installed on a system.

The `Files` modules library includes modules allowing you to accomplish most tasks related to Linux file management, such as creating, copying, editing, and modifying permissions and other attributes of files. The following table provides a list of frequently used file management modules:



**Table 6.1. Commonly Used Files Modules**

| Module name   | Module description                                           |
| :------------ | :----------------------------------------------------------- |
| `blockinfile` | Insert, update, or remove a block of multiline text surrounded by customizable marker lines. |
| `copy`        | Copy a file from the local or remote machine to a location on a managed host. Similar to the `file` module, the `copy` module can also set file attributes, including SELinux context. |
| `fetch`       | This module works like the `copy` module, but in reverse. This module is used for fetching files from remote machines to the control node and storing them in a file tree, organized by host name. |
| `file`        | Set attributes such as permissions, ownership, SELinux contexts, and time stamps of regular files, symlinks, hard links, and directories. This module can also create or remove regular files, symlinks, hard links, and directories. A number of other file-related modules support the same options to set attributes as the `file` module, including the `copy` module. |
| `lineinfile`  | Ensure that a particular line is in a file, or replace an existing line using a back-reference regular expression. This module is primarily useful when you want to change a single line in a file. |
| `stat`        | Retrieve status information for a file, similar to the Linux **stat** command. |
| `synchronize` | A wrapper around the **rsync** command to make common tasks quick and easy. The `synchronize` module is not intended to provide access to the full power of the **rsync** command, but does make the most common invocations easier to implement. You may still need to call the **rsync** command directly via the `run command` module depending on your use case. |



#### Automation Examples with Files Modules

Creating, copying, editing, and removing files on managed hosts are common tasks that you can implement using modules from the `Files` modules library. The following examples show ways that you can use these modules to automate common file management tasks.

**Ensuring a File Exists on Managed Hosts**

Use the `file` module to touch a file on managed hosts. This works like the **touch** command, creating an empty file if it does not exist, and updating its modification time if it does exist. In this example, in addition to touching the file, Ansible ensures that the owning user, group, and permissions of the file are set to specific values.

```
- name: Touch a file and set permissions
  file:
    path: /path/to/file
    owner: user1
    group: group1
    mode: 0640
    state: touch
```

Example outcome:

```
$ ls -l file
-rw-r-----  user1 group1 0 Nov 25 08:00 file
```

**Modifying File Attributes**

You can use the `file` module to ensure that a new or existing file has the correct permissions or SELinux type as well.

For example, the following file has retained the default SELinux context relative to a user's home directory, which is not the desired context.

```
$ ls -Z samba_file
-rw-r--r-- owner group unconfined_u:object_r:user_home_t:s0 samba_file
```

The following task ensures that the SELinux context type attribute of the `samba_file` file is the desired `samba_share_t` type. This behavior is similar to the Linux **chcon** command.

```
- name: SELinux type is set to samba_share_t
  file:
    path: /path/to/samba_file
    setype: samba_share_t
```

Example outcome:

```
$ ls -Z samba_file
-rw-r--r--  owner group unconfined_u:object_r:samba_share_t:s0 samba_file
```

**NOTE**

File attribute parameters are available in multiple file management modules. Run the **ansible-doc file** and **ansible-doc copy** commands for additional information.

**Making SELinux File Context Changes Persistent**

The `file` module acts like **chcon** when setting file contexts. Changes made with that module could be unexpectedly undone by running **restorecon**. After using `file` to set the context, you can use `sefcontext` from the collection of `System` modules to update the SELinux policy like **semanage fcontext**.

```
- name: SELinux type is persistently set to samba_share_t
  sefcontext:
    target: /path/to/samba_file
    setype: samba_share_t
    state: present
```

Example outcome:

```
$ ls -Z samba_file
-rw-r--r--  owner group unconfined_u:object_r:samba_share_t:s0 samba_file
```

**IMPORTANT**

The `sefcontext` module updates the default context for the target in the SELinux policy, but does not change the context on existing files.

**Copying and Editing Files on Managed Hosts**

In this example, the `copy` module is used to copy a file located in the Ansible working directory on the control node to selected managed hosts.

By default this module assumes that `force: yes` is set. That forces the module to overwrite the remote file if it exists but contains different contents from the file being copied. If `force: no` is set, then it only copies the file to the managed host if it does not already exist.

```
- name: Copy a file to managed hosts
  copy:
    src: file
    dest: /path/to/file
```

To retrieve files from managed hosts use the `fetch` module. This could be used to retrieve a file such as an SSH public key from a reference system before distributing it to other managed hosts.

```
- name: Retrieve SSH key from reference host
  fetch:
    src: "/home/{{ user }}/.ssh/id_rsa.pub
    dest: "files/keys/{{ user }}.pub"
```

To ensure a specific single line of text exists in an existing file, use the `lineinfile` module:

```
- name: Add a line of text to a file
  lineinfile:
    path: /path/to/file
    line: 'Add this line to the file'
    state: present
```

To add a block of text to an existing file, use the `blockinfile` module:

```
- name: Add additional lines to a file
  blockinfile:
    path: /path/to/file
    block: |
      First line in the additional block of text
      Second line in the additional block of text
    state: present
```

**NOTE**

When using the `blockinfile` module, commented block markers are inserted at the beginning and end of the block to ensure idempotency.

```
# BEGIN ANSIBLE MANAGED BLOCK
First line in the additional block of text
Second line in the additional block of text
# END ANSIBLE MANAGED BLOCK
```

You can use the `marker` parameter to the module to help ensure that the right comment character or text is being used for the file in question.

**Removing a File from Managed Hosts**

A basic example to remove a file from managed hosts is to use the `file` module with the `state: absent` parameter. The `state` parameter is optional to many modules. You should always make your intentions clear whether you want `state: present` or `state: absent` for several reasons. Some modules support other options as well. It is possible that the default could change at some point, but perhaps most importantly, it makes it easier to understand the state the system should be in based on your task.

```
- name: Make sure a file does not exist on managed hosts
  file:
    dest: /path/to/file
    state: absent
```

**Retrieving the Status of a File on Managed Hosts**

The `stat` module retrieves facts for a file, similar to the Linux **stat** command. Parameters provide the functionality to retrieve file attributes, determine the checksum of a file, and more.

The `stat` module returns a hash dictionary of values containing the file status data, which allows you to refer to individual pieces of information using separate variables.

The following example registers the results of a `stat` module and then prints the MD5 checksum of the file that it checked. (The more modern SHA256 algorithm is also available; MD5 is being used here for easier legibility.)

```
- name: Verify the checksum of a file
  stat:
    path: /path/to/file
    checksum_algorithm: md5
  register: result

- debug
    msg: "The checksum of the file is {{ result.stat.checksum }}"
```

The outcome should be similar to the following:

```
TASK [Get md5 checksum of a file] *****************************************
ok: [hostname]

TASK [debug] **************************************************************
ok: [hostname] => {
    "msg": "The checksum of the file is 5f76590425303022e933c43a7f2092a3"
}
```

Information about the values returned by the `stat` module are documented by **ansible-doc**, or you can register a variable and display its contents to see what is available:

```
- name: Examine all stat output of /etc/passwd
  hosts: localhost

  tasks:
    - name: stat /etc/passwd
      stat:
        path: /etc/passwd
      register: results

    - name: Display stat results
      debug:
        var: results
```

**Synchronizing Files Between the Control Node and Managed Hosts**

The `synchronize` module is a wrapper around the **rsync** tool, which simplifies common file management tasks in your playbooks. The **rsync** tool must be installed on both the local and remote host. By default, when using the `synchronize` module, the “local host” is the host that the synchronize task originates on (usually the control node), and the “destination host” is the host that `synchronize` connects to.

The following example synchronizes a file located in the Ansible working directory to the managed hosts:

```
- name: synchronize local file to remote files
  synchronize:
    src: file
    dest: /path/to/file
```

There are many ways to use the `synchronize` module and its many parameters, including synchronizing directories. Run the **ansible-doc synchronize** command for additional parameters and playbook examples.

**REFERENCES**

**ansible-doc**(1), **chmod**(1), **chown**(1), **rsync**(1), **stat**(1) and **touch**(1) man pages

[*Files modules*](https://docs.ansible.com/ansible/latest/modules/list_of_files_modules.html)



____________

### Guided Exercise: Modifying and Copying Files to Hosts

In this exercise, you will use standard Ansible modules to create, install, edit, and remove files on managed hosts and manage the permissions, ownership, and SELinux contexts of those files.

**Outcomes**

You should be able to:

- Retrieve files from managed hosts, by host name, and store them locally.
- Create playbooks that use common file management modules such as `copy`, `file`, `lineinfile`, and `blockinfile`.

Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab file-manage start** command. The script creates the `file-manage` project directory, and downloads the Ansible configuration file and the host inventory file needed for the exercise.

```
[student@workstation ~]$ lab file-manage start
```

1. As the `student` user on `workstation`, change to the `/home/student/file-manage` working directory. Create a playbook called `secure_log_backups.yml` in the current working directory. Configure the playbook to use the `fetch` module to retrieve the `/var/log/secure` log file from each of the managed hosts and store them on the control node. The playbook should create the `secure-backups` directory with subdirectories named after the host name of each managed host. Store the backup files in their respective subdirectories.

   1. Navigate to the `/home/student/file-manage` working directory.

      ```
      [student@workstation ~]$ cd ~/file-manage
      [student@workstation file-manage]$
      ```

   2. Create the `secure_log_backups.yml` playbook with initial content:

      ```
      ---
      - name: Use the fetch module to retrieve secure log files
        hosts: all
        remote_user: root
      ```

   3. Add a task to the `secure_log_backups.yml` playbook that retrieves the `/var/log/secure` log file from the managed hosts and stores it in the `~/file-manage/secure-backups` directory. The `fetch` module creates the `~/file-manage/secure-backups` directory if it does not exist. Use the `flat: no` parameter to ensure the default behavior of appending the host name, path, and file name to the destination:

      ```
        tasks:
          - name: Fetch the /var/log/secure log file from managed hosts
            fetch:
              src: /var/log/secure
              dest: secure-backups
              flat: no
      ```

   4. Before running the playbook, run the **ansible-playbook --syntax-check secure_log_backups.yml** command to verify its syntax. Correct any errors before moving to the next step.

      ```
      [student@workstation file-manage]$ ansible-playbook --syntax-check \
      > secure_log_backups.yml
      
      playbook: secure_log_backups.yml
      ```

   5. Run **ansible-playbook secure_log_backups.yml** to execute the playbook:

      ```
      [student@workstation file-manage]$ ansible-playbook secure_log_backups.yml
      PLAY [Use the fetch module to retrieve secure log files] ******************
      
      TASK [Gathering Facts] ****************************************************
      ok: [servera.lab.example.com]
      ok: [serverb.lab.example.com]
      
      TASK [Fetch the /var/log/secure file from managed hosts] ******************
      changed: [serverb.lab.example.com]
      changed: [servera.lab.example.com]
      
      PLAY RECAP ****************************************************************
      servera.lab.example.com    : ok=2    changed=1    unreachable=0    failed=0
      serverb.lab.example.com    : ok=2    changed=1    unreachable=0    failed=0
      ```

   6. Verify the playbook results:

      ```
      [student@workstation file-manage]$ tree -F secure-backups
      secure-backups
      ├── servera.lab.example.com/
      │   └── var/
      │       └── log/
      │           └── secure
      └── serverb.lab.example.com/
          └── var/
              └── log/
                  └── secure
      ```

2. Create the `copy_file.yml` playbook in the current working directory. Configure the playbook to copy the `/home/student/file-manage/files/users.txt` file to all managed hosts as the root user.

   1. Add the following initial content to the `copy_file.yml` playbook:

      ```
      ---
      - name: Using the copy module
        hosts: all
        remote_user: root
      ```

   2. Add a task to use the `copy` module to copy the `/home/student/file-manage/files/users.txt` file to all managed hosts. Use the `copy` module to set the following parameters for the `users.txt` file:

      

      **Table 6.2.** 

      | Parameter | Values                   |
      | :-------- | :----------------------- |
      | `src`     | `files/users.txt`        |
      | `dest`    | `/home/devops/users.txt` |
      | `owner`   | `devops`                 |
      | `group`   | `devops`                 |
      | `mode`    | `u+rw,g-wx,o-rwx`        |
      | `setype`  | `samba_share_t`          |

      ```
        tasks:
          - name: Copy a file to managed hosts and set attributes
            copy:
              src: files/users.txt
              dest: /home/devops/users.txt
              owner: devops
              group: devops
              mode: u+rw,g-wx,o-rwx
              setype: samba_share_t
      ```

   3. Use the **ansible-playbook --syntax-check copy_file.yml** command to verify the syntax of the `copy_file.yml` playbook.

      ```
      [student@workstation file-manage]$ ansible-playbook --syntax-check copy_file.yml
      
      playbook: copy_file.yml
      ```

   4. Run the playbook:

      ```
      [student@workstation file-manage]$ ansible-playbook copy_file.yml
      PLAY [Using the copy module] *******************************************
      
      TASK [Gathering Facts] *************************************************
      ok: [serverb.lab.example.com]
      ok: [servera.lab.example.com]
      
      TASK [Copy a file to managed hosts and set attributes] *****************
      changed: [servera.lab.example.com]
      changed: [serverb.lab.example.com]
      
      PLAY RECAP *************************************************************
      servera.lab.example.com : ok=2    changed=1    unreachable=0    failed=0
      serverb.lab.example.com : ok=2    changed=1    unreachable=0    failed=0
      ```

   5. Use an ad hoc command to execute the **ls -Z** command as user `devops` to verify the attributes of the `users.txt` file on the managed hosts.

      ```
      [student@workstation file-manage]$ ansible all -m command -a 'ls -Z' -u devops
      servera.lab.example.com | CHANGED | rc=0 >>
      unconfined_u:object_r:samba_share_t:s0 users.txt
      
      serverb.lab.example.com | CHANGED | rc=0 >>
      unconfined_u:object_r:samba_share_t:s0 users.txt
      ```

3. In a previous step, the `samba_share_t` SELinux type field was set for the `users.txt` file. However, it is now determined that default values should be set for the SELinux file context.

   Create a playbook called `selinux_defaults.yml` in the current working directory. Configure the playbook to use the `file` module to ensure the default SELinux context for `user`, `role`, `type`, and `level` fields.

   **NOTE**

   In the real world you would also edit `copy_file.yml` and remove the `setype` keyword.

   1. Create the `selinux_defaults.yml` playbook:

      ```
      ---
      - name: Using the file module to ensure SELinux file context
        hosts: all
        remote_user: root
        tasks:
          - name:  SELinux file context is set to defaults
            file:
              path: /home/devops/users.txt
              seuser: _default
              serole: _default
              setype: _default
              selevel: _default
      ```

   2. Use the **ansible-playbook --syntax-check selinux_defaults.yml** command to verify the syntax of the `selinux_defaults.yml` playbook.

      ```
      [student@workstation file-manage]$ ansible-playbook --syntax-check \
      > selinux_defaults.yml
      
      playbook: selinux_defaults.yml
      ```

   3. Run the playbook:

      ```
      [student@workstation file-manage]$ ansible-playbook selinux_defaults.yml
      PLAY [Using the file module to ensure SELinux file context] ************
      
      TASK [Gathering Facts] *************************************************
      ok: [serverb.lab.example.com]
      ok: [servera.lab.example.com]
      
      TASK [SELinux file context is set to defaults] *************************
      changed: [serverb.lab.example.com]
      changed: [servera.lab.example.com]
      
      PLAY RECAP *************************************************************
      servera.lab.example.com : ok=2    changed=1    unreachable=0    failed=0
      serverb.lab.example.com : ok=2    changed=1    unreachable=0    failed=0
      ```

   4. Use an ad hoc command to execute the **ls -Z** command as user `devops` to verify the default file attributes of `unconfined_u:object_r:user_home_t:s0`.

      ```
      [student@workstation file-manage]$ ansible all -m command -a 'ls -Z' -u devops
      servera.lab.example.com | CHANGED | rc=0 >>
      unconfined_u:object_r:user_home_t:s0 users.txt
      
      serverb.lab.example.com | CHANGED | rc=0 >>
      unconfined_u:object_r:user_home_t:s0 users.txt
      ```

4. Create a playbook called `add_line.yml` in the current working directory. Configure the playbook to use the `lineinfile` module to append the line `This line was added by the lineinfile module.` to the `/home/devops/users.txt` file on all managed hosts.

   1. Create the `add_line.yml` playbook:

      ```
      ---
      - name: Add text to an existing file
        hosts: all
        remote_user: devops
        tasks:
          - name: Add a single line of text to a file
            lineinfile:
              path: /home/devops/users.txt
              line: This line was added by the lineinfile module.
              state: present
      ```

   2. Use **ansible-playbook --syntax-check add_line.yml** command to verify the syntax of the `add_line.yml` playbook.

      ```
      [student@workstation file-manage]$ ansible-playbook --syntax-check add_line.yml
      
      playbook: add_line.yml
      ```

   3. Run the playbook:

      ```
      [student@workstation file-manage]$ ansible-playbook add_line.yml
      PLAY [Add text to an existing file] ************************************
      
      TASK [Gathering Facts] *************************************************
      ok: [serverb.lab.example.com]
      ok: [servera.lab.example.com]
      
      TASK [Add a single line of text to a file] *****************************
      changed: [servera.lab.example.com]
      changed: [serverb.lab.example.com]
      
      PLAY RECAP *************************************************************
      servera.lab.example.com : ok=2    changed=1    unreachable=0    failed=0
      serverb.lab.example.com : ok=2    changed=1    unreachable=0    failed=0
      ```

   4. Use the `command` module with the **cat** option, as the `devops` user, to verify the content of the `users.txt` file on the managed hosts.

      ```
      [student@workstation file-manage]$ ansible all -m command \
      > -a 'cat users.txt' -u devops
      serverb.lab.example.com | CHANGED | rc=0 >>
      This line was added by the lineinfile module.
      
      servera.lab.example.com | CHANGED | rc=0 >>
      This line was added by the lineinfile module.
      ```

5. Create a playbook called `add_block.yml` in the current working directory. Configure the playbook to use the `blockinfile` module to append the following block of text to the `/home/devops/users.txt` file on all managed hosts.

   ```
   This block of text consists of two lines.
   They have been added by the blockinfile module.
   ```

   1. Create the `add_block.yml` playbook:

      ```
      ---
      - name: Add block of text to a file
        hosts: all
        remote_user: devops
        tasks:
          - name: Add a block of text to an existing file
            blockinfile:
              path: /home/devops/users.txt
              block: |
                This block of text consists of two lines.
                They have been added by the blockinfile module.
              state: present
      ```

   2. Use the **ansible-playbook --syntax-check add_block.yml** command to verify the syntax of the `add_block.yml` playbook.

      ```
      [student@workstation file-manage]$ ansible-playbook --syntax-check add_block.yml
      
      playbook: add_block.yml
      ```

   3. Run the playbook:

      ```
      [student@workstation file-manage]$ ansible-playbook add_block.yml
      
      PLAY [Add block of text to a file] ****************************************
      
      TASK [Gathering Facts] ****************************************************
      ok: [serverb.lab.example.com]
      ok: [servera.lab.example.com]
      
      TASK [Add a block of text to an existing file] ****************************
      changed: [servera.lab.example.com]
      changed: [serverb.lab.example.com]
      
      PLAY RECAP ****************************************************************
      servera.lab.example.com    : ok=2    changed=1    unreachable=0    failed=0
      serverb.lab.example.com    : ok=2    changed=1    unreachable=0    failed=0
      ```

   4. Use the `command` module with the **cat** command to verify the correct content of the `/home/devops/users.txt` file on the managed host.

      ```
      [student@workstation file-manage]$ ansible all -m command \
      > -a 'cat users.txt' -u devops
      serverb.lab.example.com | CHANGED | rc=0 >>
      This line was added by the lineinfile module.
      # BEGIN ANSIBLE MANAGED BLOCK
      This block of text consists of two lines.
      They have been added by the blockinfile module.
      # END ANSIBLE MANAGED BLOCK
      
      servera.lab.example.com | CHANGED | rc=0 >>
      This line was added by the lineinfile module.
      # BEGIN ANSIBLE MANAGED BLOCK
      This block of text consists of two lines.
      They have been added by the blockinfile module.
      # END ANSIBLE MANAGED BLOCK
      ```

6. Create a playbook called `remove_file.yml` in the current working directory. Configure the playbook to use the `file` module to remove the `/home/devops/users.txt` file from all managed hosts.

   1. Create the `remove_file.yml` playbook:

      ```
      ---
      - name: Use the file module to remove a file
        hosts: all
        remote_user: devops
        tasks:
          - name: Remove a file from managed hosts
            file:
              path: /home/devops/users.txt
              state: absent
      ```

   2. Use the **ansible-playbook --syntax-check remove_file.yml** command to verify the syntax of the `remove_file.yml` playbook.

      ```
      [student@workstation file-manage]$ ansible-playbook --syntax-check remove_file.yml
      
      playbook: remove_file.yml
      ```

   3. Run the playbook:

      ```
      [student@workstation file-manage]$ ansible-playbook remove_file.yml
      PLAY [Use the file module to remove a file] *******************************
      
      TASK [Gathering Facts] ****************************************************
      ok: [serverb.lab.example.com]
      ok: [servera.lab.example.com]
      
      TASK [Remove a file from managed hosts] ***********************************
      changed: [serverb.lab.example.com]
      changed: [servera.lab.example.com]
      
      PLAY RECAP ****************************************************************
      servera.lab.example.com    : ok=2    changed=1    unreachable=0    failed=0
      serverb.lab.example.com    : ok=2    changed=1    unreachable=0    failed=0
      ```

   4. Use an ad hoc command to execute the **ls -l** command to confirm that the `users.txt` file no longer exists on the managed hosts.

      ```
      [student@workstation file-manage]$ ansible all -m command -a 'ls -l'
      serverb.lab.example.com | CHANGED | rc=0 >>
      total 0
      
      servera.lab.example.com | CHANGED | rc=0 >>
      total 0
      ```

**Finish**

On `workstation`, run the **lab file-manage finish** script to clean up this exercise.

```
[student@workstation ~]$ lab file-manage finish
```

This concludes the guided exercise.



____



### Deploying Custom Files with Jinja2 Templates

#### Objectives

After completing this section, you should be able to use Jinja2 templates to deploy customized files to managed hosts.

#### Templating Files

Red Hat Ansible Engine has a number of modules that can be used to modify existing files. These include `lineinfile` and `blockinfile`, among others. However, they are not always easy to use effectively and correctly.

A much more powerful way to manage files is to *template* them. With this method, you can write a template configuration file that is automatically customized for the managed host when the file is deployed, using Ansible variables and facts. This can be easier to control and is less error-prone.

#### Introduction to Jinja2

Ansible uses the Jinja2 templating system for template files. Ansible also uses Jinja2 syntax to reference variables in playbooks, so you already know a little bit about how to use it.

**Using Delimiters**

Variables and logic expressions are placed between tags, or delimiters. For example, Jinja2 templates use `{% *EXPR* %}` for expressions or logic (for example, loops), while `{{ *EXPR* }}` are used for outputting the results of an expression or a variable to the end user. The latter tag, when rendered, is replaced with a value or values, and are seen by the end user. Use `{# *COMMENT* #}` syntax to enclose comments that should not appear in the final file.

In the following example, the first line includes a comment that will not be included in the final file. The variable references in the second line are replaced with the values of the system facts being referenced.

```
{# /etc/hosts line #}
{{ ansible_facts['default_ipv4']['address'] }}    {{ ansible_facts['hostname'] }}
```

#### Building a Jinja2 template

A Jinja2 template is composed of multiple elements: data, variables, and expressions. Those variables and expressions are replaced with their values when the Jinja2 template is rendered. The variables used in the template can be specified in the `vars` section of the playbook. It is possible to use the managed hosts' facts as variables on a template.

**NOTE**

Remember that the facts associated with a managed host can be obtained using the **ansible system_hostname -i inventory_file -m setup** command.

The following example shows how to create a template for `/etc/ssh/sshd_config` with variables and facts retrieved by Ansible from managed hosts. When the associated playbook is executed, any facts are replaced by their values in the managed host being configured.

**NOTE**

A file containing a Jinja2 template does not need to have any specific file extension (for example, `.j2`). However, providing such a file extension may make it easier for you to remember that it is a template file.

```
# {{ ansible_managed }}
# DO NOT MAKE LOCAL MODIFICATIONS TO THIS FILE AS THEY WILL BE LOST

Port {{ ssh_port }}
ListenAddress {{ ansible_facts['default_ipv4']['address'] }}

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

SyslogFacility AUTHPRIV

PermitRootLogin {{ root_allowed }}
AllowGroups {{ groups_allowed }}

AuthorizedKeysFile /etc/.rht_authorized_keys .ssh/authorized_keys

PasswordAuthentication {{ passwords_allowed }}

ChallengeResponseAuthentication no

GSSAPIAuthentication yes
GSSAPICleanupCredentials no

UsePAM yes

X11Forwarding yes
UsePrivilegeSeparation sandbox

AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

Subsystem sftp	/usr/libexec/openssh/sftp-server
```

**Deploying Jinja2 Templates**

Jinja2 templates are a powerful tool to customize configuration files to be deployed on the managed hosts. When the Jinja2 template for a configuration file has been created, it can be deployed to the managed hosts using the `template` module, which supports the transfer of a local file on the control node to the managed hosts.

To use the `template` module, use the following syntax. The value associated with the `src` key specifies the source Jinja2 template, and the value associated with the `dest` key specifies the file to be created on the destination hosts.

```
tasks:
  - name: template render
    template:
      src: /tmp/j2-template.j2
      dest: /tmp/dest-config-file.txt
```

**NOTE**

The `template` module also allows you to specify the owner (the user that owns the file), group, permissions, and SELinux context of the deployed file, just like the `file` module. It can also take a `validate` option to run an arbitrary command (such as **visudo** `-c`) to check the syntax of a file for correctness before copying it into place.

For more details, see **ansible-doc template**.

#### **Managing Templated Files**

To avoid having system administrators modify files deployed by Ansible, it is a good practice to include a comment at the top of the template to indicate that the file should not be manually edited.

One way to do this is to use the “Ansible managed” string set in the `ansible_managed` directive. This is not a normal variable but can be used as one in a template. The `ansible_managed` directive is set in the `ansible.cfg` file:

```
ansible_managed = Ansible managed
```

To include the `ansible_managed` string inside a Jinja2 template, use the following syntax:

```
{{ ansible_managed }}
```

#### **Control Structures**

You can use Jinja2 control structures in template files to reduce repetitive typing, to enter entries for each host in a play dynamically, or conditionally insert text into a file.

**Using Loops**

Jinja2 uses the `for` statement to provide looping functionality. In the following example, the `user` variable is replaced with all the values included in the `users` variable, one value per line.

```
{% for user in users %}
      {{ user }}
{% endfor %}
```

The following example template uses a `for` statement to run through all the values in the `users` variable, replacing `myuser` with each value, except when the value is `root`.

```
{# for statement #}
{% for myuser in users if not myuser == "root" %}
User number {{ loop.index }} - {{ myuser }}
{% endfor %}
```

The `loop.index` variable expands to the index number that the loop is currently on. It has a value of 1 the first time the loop executes, and it increments by 1 through each iteration.

As another example, this template also uses a `for` statement, and assumes a `myhosts` variable has been defined in the inventory file being used. This variable would contain a list of hosts to be managed. With the following `for` statement, all hosts in the `myhosts` group from the inventory would be listed in the file.

```
{% for myhost in groups['myhosts'] %}
{{ myhost }}
{% endfor %}
```

For a more practical example, you can use this to generate an `/etc/hosts` file from host facts dynamically. Assume that you have the following playbook:

```
- name: /etc/hosts is up to date
  hosts: all
  gather_facts: yes
  tasks:
    - name: Deploy /etc/hosts
      template:
        src: templates/hosts.j2
        dest: /etc/hosts
```

The following three-line `templates/hosts.j2` template constructs the file from all hosts in the group `all`. (The middle line is extremely long in the template due to the length of the variable names.) It iterates over each host in the group to get three facts for the `/etc/hosts` file.

```
{% for host in groups['all'] %}
{{ hostvars['host']['ansible_facts']['default_ipv4']['address'] }} {{ hostvars['host']['ansible_facts']['fqdn'] }} {{ hostvars['host']['ansible_facts']['hostname'] }}
{% endfor %}
```

**Using Conditionals**

Jinja2 uses the `if` statement to provide conditional control. This allows you to put a line in a deployed file if certain conditions are met.

In the following example, the value of the `result` variable is placed in the deployed file only if the value of the `finished` variable is `True`.

```
{% if finished %}
{{ result }}
{% endif %}
```

**IMPORTANT**

You can use Jinja2 loops and conditionals in Ansible templates, but not in Ansible Playbooks.

#### **Variable Filters**

Jinja2 provides filters which change the output format for template expressions (for example, to JSON). There are filters available for languages such as YAML and JSON. The `to_json` filter formats the expression output using JSON, and the `to_yaml` filter formats the expression output using YAML.

```
{{ output | to_json }}
{{ output | to_yaml }}
```

Additional filters are available, such as the `to_nice_json` and `to_nice_yaml` filters, which format the expression output in either JSON or YAML human readable format.

```
{{ output | to_nice_json }}
{{ output | to_nice_yaml }}
```

Both the `from_json` and `from_yaml` filters expect strings in either JSON or YAML format, respectively, to parse them.

```
{{ output | from_json }}
{{ output | from_yaml }}
```

#### **Variable Tests**

The expressions used with `when` clauses in Ansible Playbooks are Jinja2 expressions. Built-in Ansible tests used to test return values include `failed`, `changed`, `succeeded`, and `skipped`. The following task shows how tests can be used inside of conditional expressions.

```
tasks:
...output omitted...
  - debug: msg="the execution was aborted"
    when: returnvalue is failed
```

**REFERENCES**

[template - Templates a file out to a remote server — Ansible Documentation](https://docs.ansible.com/ansible/latest/modules/template_module.html)

[Variables — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html)

[Filters — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html)



------

### Guided Exercise: Deploying Custom Files with Jinja2 Templates

In this exercise, you will create a simple template file that your playbook will use to install a customized Message of the Day file on each managed host.

**Outcomes**

You should be able to:

- Build a template file.
- Use the template file in a playbook.

Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab file-template start** command. This script ensures that Ansible is installed on `workstation`, creates the `/home/student/file-template` directory, and downloads the `ansible.cfg` file into that directory.

```
[student@workstation ~]$ lab file-template start
```

**NOTE**

All the files used during this exercise are available for reference on `workstation` in the `/home/student/file-template/files` directory.

1. On `workstation`, navigate to the `/home/student/file-template` working directory. Create the `inventory` file in the current working directory. This file configures two groups: `webservers` and `workstations`. Include the `servera.lab.example.com` system in the `webservers` group, and the `workstation.lab.example.com` system in the `workstations` group.

   ```
   [webservers]
   servera.lab.example.com
   
   [workstations]
   workstation.lab.example.com
   ```

2. Create a template for the Message of the Day and include it in the `motd.j2` file in the current working directory. Include the following variables and facts in the template:

   - `ansible_facts['fqdn']`, to insert the FQDN of the managed host.
   - `ansible_facts['distribution']` and `ansible_facts['distribution_version']`, to provide distribution information.
   - `system_owner`, for the system owner's email. This variable needs to be defined with an appropriate value in the `vars` section of the playbook template.

   ```
   This is the system {{ ansible_facts['fqdn'] }}.
   This is a {{ ansible_facts['distribution'] }} version {{ ansible_facts['distribution_version'] }} system.
   Only use this system with permission.
   You can request access from {{ system_owner }}.
   ```

3. Create a playbook file named `motd.yml` in the current working directory. Define the `system_owner` variable in the `vars` section, and include a task for the `template` module that maps the `motd.j2` Jinja2 template to the remote file `/etc/motd` on the managed hosts. Set the owner and group to `root`, and the mode to 0644.

   ```
   ---
   - name: configure SOE
     hosts: all
     remote_user: devops
     become: true
     vars:
       - system_owner: clyde@example.com
     tasks:
       - name: configure /etc/motd
         template:
           src: motd.j2
           dest: /etc/motd
           owner: root
           group: root
           mode: 0644
   ```

4. Before running the playbook, use the **ansible-playbook --syntax-check** command to verify the syntax. If it reports any errors, correct them before moving to the next step. You should see output similar to the following:

   ```
   [student@workstation file-template]$ ansible-playbook --syntax-check motd.yml
   
   playbook: motd.yml
   ```

5. Run the `motd.yml` playbook.

   ```
   [student@workstation file-template]$ ansible-playbook motd.yml
   PLAY [all] ******************************************************************
   
   TASK [Gathering Facts] ******************************************************
   ok: [servera.lab.example.com]
   ok: [workstation.lab.example.com]
   
   TASK [template] *************************************************************
   changed: [servera.lab.example.com]
   changed: [workstation.lab.example.com]
   
   PLAY RECAP ******************************************************************
   servera.lab.example.com      : ok=2    changed=1    unreachable=0    failed=0
   workstation.lab.example.com  : ok=2    changed=1    unreachable=0    failed=0
   ```

6. Log in to `servera.lab.example.com` as the `devops` user to verify that the MOTD is correctly displayed when logged in. Log off when you have finished.

   ```
   [student@workstation file-template]$ ssh devops@servera.lab.example.com
   This is the system servera.lab.example.com.
   This is a RedHat version 8.0 system.
   Only use this system with permission.
   You can request access from clyde@example.com.
   ...output omitted...
   [devops@servera ~]# exit
   Connection to servera.lab.example.com closed.
   ```

**Finish**

Run the **lab file-template finish** command to clean up after the exercise.

```
[student@workstation ~]$ lab file-template finish
```

This concludes the guided exercise.



------

### Lab: Deploying Files to Managed Hosts

**Performance Checklist**

In this lab, you will run a playbook that creates a customized file on your managed hosts by using a Jinja2 template.

**Outcomes**

You should be able to:

- Build a template file.
- Use the template file in a playbook.



Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab file-review start** command. This ensures that Ansible is installed on `workstation`, creates the `/home/student/file-review` directory, and downloads the `ansible.cfg` file into that directory. It also downloads the `motd.yml`, `motd.j2`, `issue`, and `inventory` files into the `/home/student/file-review/files` directory.

```
[student@workstation ~]$ lab file-review start
```

**NOTE**

All files used in this exercise are available on `workstation` in the `/home/student/file-review/files` directory.

1. Create an inventory file named `inventory` in the `/home/student/file-review` directory. This inventory file defines the `servers` group, which has the `serverb.lab.example.com` managed host associated with it.

   1. On workstation, change to the `/home/student/file-review` directory.

      ```
      [student@workstation ~]$ cd ~/file-review/
      ```

   2. Create the `inventory` file in the current directory. This file configures one group called `servers`. Include the `serverb.lab.example.com` system in the `servers` group.

      ```
      [servers]
      serverb.lab.example.com
      ```

   

2. Identify the facts on `serverb.lab.example.com` that show the total amount of system memory, and the number of processors.

   Use the `setup` module to get a list of all the facts for the `serverb.lab.example.com` managed host. The `ansible_processor_count` and `ansible_memtotal_mb` facts provide information about the resource limits of the managed host.

   ```
   [student@workstation file-review]$ ansible serverb.lab.example.com -m setup
   serverb.lab.example.com | SUCCESS => {
       "ansible_facts": {
   ...output omitted...
   	"ansible_processor_count": 1,
   ...output omitted...
   	"ansible_memtotal_mb": 821,
   ...output omitted...
       },
       "changed": false
   }
   ```

   

3. Create a template for the Message of the Day, named `motd.j2`, in the current directory. When the `devops` user logs in to `serverb.lab.example.com`, a message should display that shows the system's total memory and processor count. Use the `ansible_facts['memtotal_mb']` and `ansible_facts['processor_count']` facts to provide the system resource information for the message.

   ```
   System total memory: {{ ansible_facts['memtotal_mb'] }} MiB.
   System processor count: {{ ansible_facts['processor_count'] }}
   ```

   

4. Create a new playbook file called `motd.yml` in the current directory. Using the `template` module, configure the `motd.j2` Jinja2 template file previously created to map to the file `/etc/motd` on the managed hosts. This file has the `root` user as owner and group, and its permissions are 0644. Using the `stat` and `debug` modules, create tasks to verify that `/etc/motd` exists on the managed hosts and displays the file information for `/etc/motd`. Use the `copy` module to place `files/issue` into the `/etc/` directory on the managed host, use the same ownership and permissions as `/etc/motd`. Use the `file` module to ensure that `/etc/issue.net` is a symbolic link to `/etc/issue` on the managed host. Configure the playbook so that it uses the `devops` user, and sets the `become` parameter to `true`.

   ```
   ---
   - name: Configure system
     hosts: all
     remote_user: devops
     become: true
     tasks:
       - name: Configure a custom /etc/motd
         template:
           src: motd.j2
           dest: /etc/motd
           owner: root
           group: root
           mode: 0644
   
       - name: Check file exists
         stat:
           path: /etc/motd
         register: motd
   
       - name: Display stat results
         debug:
           var: motd
   
       - name: Copy custom /etc/issue file
         copy:
           src: files/issue
           dest: /etc/issue
           owner: root
           group: root
           mode: 0644
   
       - name: Ensure /etc/issue.net is a symlink to /etc/issue
         file:
           src: /etc/issue
           dest: /etc/issue.net
           state: link
           owner: root
           group: root
           force: yes
   ```

   

5. Run the playbook included in the `motd.yml` file.

   1. Before you run the playbook, use the **ansible-playbook --syntax-check** command to verify its syntax. If it reports any errors, correct them before moving to the next step. You should see output similar to the following:

      ```
      [student@workstation file-review]$ ansible-playbook --syntax-check motd.yml
      
      playbook: motd.yml
      ```

   2. Run the playbook included in the `motd.yml` file.

      ```
      [student@workstation file-review]$ ansible-playbook motd.yml
      
      PLAY [Configure system] ****************************************************
      
      TASK [Gathering Facts] *****************************************************
      ok: [serverb.lab.example.com]
      
      TASK [Configure a custom /etc/motd] ****************************************
      changed: [serverb.lab.example.com]
      
      TASK [Check file exists] ***************************************************
      ok: [serverb.lab.example.com]
      
      TASK [Display stat results] ************************************************
      ok: [serverb.lab.example.com] => {
          "motd": {
              "changed": false,
              "failed": false,
      ...output omitted...
      
      TASK [Copy custom /etc/issue file] *****************************************
      changed: [serverb.lab.example.com]
      
      TASK [Ensure /etc/issue.net is a symlink to /etc/issue] ********************
      changed: [serverb.lab.example.com]
      
      PLAY RECAP *****************************************************************
      serverb.lab.example.com    : ok=6    changed=3    unreachable=0    failed=0
      ```

   

6. Check that the playbook included in the `motd.yml` file has been executed correctly.

   Log in to `serverb.lab.example.com` as the `devops` user, and verify that the `/etc/motd` and `/etc/issue` contents are displayed when logging in. Log off when you have finished.

   ```
   [student@workstation file-review]$ ssh devops@serverb.lab.example.com
   *------------------------------- PRIVATE SYSTEM -----------------------------*
   *   Access to this computer system is restricted to authorised users only.   *
   *                                                                            *
   *      Customer information is confidential and must not be disclosed.       *
   *----------------------------------------------------------------------------*
   System total memory: 821 MiB.
   System processor count: 1
   Activate the web console with: systemctl enable --now cockpit.socket
   
   Last login: Thu Apr 25 22:09:33 2019 from 172.25.250.9
   [devops@serverb ~]$ logout
   ```

   

**Evaluation**

On `workstation`, run the **lab file-review grade** script to confirm success on this exercise.

```
[student@workstation ~]$ lab file-review grade
```

**Finish**

On `workstation`, run the **lab file-review finish** script to clean up after the lab.

```
[student@workstation ~]$ lab file-review finish
```

This concludes the guided exercise.



------

### Summary

In this chapter, you learned:

- The `Files` modules library includes modules that allow you to accomplish most tasks related to file management, such as creating, copying, editing, and modifying permissions and other attributes of files.
- You can use Jinja2 templates to dynamically construct files for deployment.
- A Jinja2 template is usually composed of two elements: variables and expressions. Those variables and expressions are replaced with values when the Jinja2 template is rendered.
- Jinja2 filters transform template expressions from one kind or format of data into another.



____

## Chapter 7. Managing Large Projects

- [Selecting Hosts with Host Patterns](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07)
- [Guided Exercise: Selecting Hosts with Host Patterns](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07s02)
- [Managing Dynamic Inventories](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07s03)
- [Guided Exercise: Managing Dynamic Inventories](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07s04)
- [Configuring Parallelism](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07s05)
- [Guided Exercise: Configuring Parallelism](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07s06)
- [Including and Importing Files](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07s07)
- [Guided Exercise: Including and Importing Files](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07s08)
- [Lab: Managing Large Projects](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07s09)
- [Summary](https://rol.redhat.com/rol/app/courses/rh294-8.0/pages/ch07s10)

**Abstract**

| **Goal**       | Write playbooks that are optimized for larger and more complex projects. |
| -------------- | ------------------------------------------------------------ |
| **Objectives** | Write sophisticated host patterns to efficiently select hosts for a play or ad hoc command.Describe the nature and purpose of dynamic inventories and then install and use an existing script as an Ansible dynamic inventory source.Tune the number of simultaneous connections that Ansible opens to managed hosts and how Ansible processes groups of managed hosts through the play’s tasks.Manage large playbooks by importing or including playbooks and tasks from external files, either unconditionally or based on a conditional test. |
| **Sections**   | Selecting Hosts with Host Patterns (and Guided Exercise)Managing Dynamic Inventories (and Guided Exercise)Configuring Parallelism (and Guided Exercise)Including and Importing Files (and Guided Exercise) |
| **Lab**        | Managing Large Projects                                      |



### Selecting Hosts with Host Patterns

#### Objectives

After completing this section, you will be able to write sophisticated host patterns to efficiently select hosts for a play or ad hoc command.

#### Referencing Inventory Hosts

*Host patterns* are used to specify the hosts to target by a play or ad hoc command. In its simplest form, the name of a managed host or a host group in the inventory is a host pattern that specifies that host or host group.

You have already used host patterns in this course. In a play, the `hosts` directive specifies the managed hosts to run the play against. For an ad hoc command, provide the host pattern as a command line argument to the **ansible** command.

It is usually easier to control what hosts a play targets by carefully using host patterns and having appropriate inventory groups, instead of setting complex conditionals on the play's tasks. Therefore, it is important to have a robust understanding of host patterns.

The following example inventory is used throughout this section to illustrate host patterns.

```
[student@controlnode ~]$ cat myinventory
web.example.com
data.example.com

[lab]
labhost1.example.com
labhost2.example.com

[test]
test1.example.com
test2.example.com

[datacenter1]
labhost1.example.com
test1.example.com

[datacenter2]
labhost2.example.com
test2.example.com

[datacenter:children]
datacenter1
datacenter2

[new]
192.168.2.1
192.168.2.2
```

To demonstrate how host patterns are resolved, you will execute an Ansible Playbook, `playbook.yml`, using different host patterns to target different subsets of managed hosts from this example inventory.

**Managed Hosts**

The most basic host pattern is the name for a single managed host listed in the inventory. This specifies that the host will be the only one in the inventory that will be acted upon by the **ansible** command.

When the playbook runs, the first `Gathering Facts` task should run on all managed hosts that match the host pattern. A failure during this task can cause the managed host to be removed from the play.

If an IP address is listed explicitly in the inventory, instead of a host name, then it can be used as a host pattern. If the IP address is not listed in the inventory, then you cannot use it to specify the host even if the IP address resolves to that host name in the DNS.

The following example shows how a host pattern can be used to reference an IP address contained in an inventory.

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: 192.168.2.1
...output omitted...

[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [192.168.2.1]
...output omitted...
```

**NOTE**

One problem with referring to managed hosts by IP address in the inventory is that it can be hard to remember which IP address matches which host for your plays and ad hoc commands. However, you may have to specify the host by IP address for connection purposes if the host does not have a resolvable host name.

It is possible to point an alias at a particular IP address in your inventory by setting the `ansible_host` host variable. For example, you could have a host in your inventory named `dummy.example`, and then direct connections using that name to the IP address 192.168.2.1 by creating a `host_vars/dummy.example` file containing the following host variable:

```
ansible_host: 192.168.2.1
```

**Specifying Hosts Using a Group**

You have already used inventory host groups as host patterns. When a group name is used as a host pattern, it specifies that Ansible will act on the hosts that are members of the group.

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: lab
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost1.example.com]
ok: [labhost2.example.com]
...output omitted...
```

Remember that there is a special group named `all` that matches all managed hosts in the inventory.

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: all
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost2.example.com]
ok: [test2.example.com]
ok: [web.example.com]
ok: [data.example.com]
ok: [labhost1.example.com]
ok: [192.168.2.1]
ok: [test1.example.com]
ok: [192.168.2.2]
```

There is also a special group named `ungrouped`, which includes all managed hosts in the inventory that are not members of any other group:

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: ungrouped
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [web.example.com]
ok: [data.example.com]
```

**Matching Multiple Hosts with Wildcards**

Another method of accomplishing the same thing as the `all` host pattern is to use the asterisk (*) wildcard character, which matches any string. If the host pattern is just a quoted asterisk, then all hosts in the inventory will match.

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: '*'
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost2.example.com]
ok: [test2.example.com]
ok: [web.example.com]
ok: [data.example.com]
ok: [labhost1.example.com]
ok: [192.168.2.1]
ok: [test1.example.com]
ok: [192.168.2.2]
```

**IMPORTANT**

Some characters that are used in host patterns also have meaning for the shell. This can be a problem when using host patterns to run ad hoc commands from the command line with **ansible**. It is a recommended practice to enclose host patterns used on the command line in single quotes to protect them from unwanted shell expansion.

Likewise, if you are using any special wildcards or list characters in an Ansible Playbook, then you must put your host pattern in single quotes to ensure it is parsed correctly.

```
---
  hosts: '!test1.example.com,development'
```

The asterisk character can also be used to match any managed hosts or groups that contain a particular substring.

For example, the following wildcard host pattern matches all inventory names that end in `.example.com`:

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: '*.example.com'
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost1.example.com]
ok: [test1.example.com]
ok: [labhost2.example.com]
ok: [test2.example.com]
ok: [web.example.com]
ok: [data.example.com]
```

The following example uses a wildcard host pattern to match the names of hosts or host groups that start with `192.168.2.`:

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: '192.168.2.*'
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [192.168.2.1]
ok: [192.168.2.2]
```

The next example uses a wildcard host pattern to match the names of hosts or host groups that begin with `datacenter`.

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: 'datacenter*'
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost1.example.com]
ok: [test1.example.com]
ok: [labhost2.example.com]
ok: [test2.example.com]
```

**IMPORTANT**

The wildcard host patterns match all inventory names, hosts, and host groups. They do not distinguish between names that are DNS names, IP addresses, or groups, which can lead to some unexpected matches.

For example, compare the results of specifying the `datacenter*` host pattern from the preceding example with the results of the `data*` host pattern based on the example inventory:

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: 'data*'
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost1.example.com]
ok: [test1.example.com]
ok: [labhost2.example.com]
ok: [test2.example.com]
ok: [data.example.com]
```

**Lists**

Multiple entries in an inventory can be referenced using logical lists. A comma-separated list of host patterns matches all hosts that match any of those host patterns.

If you provide a comma-separated list of managed hosts, then all those managed hosts will be targeted:

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: labhost1.example.com,test2.example.com,192.168.2.2
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost1.example.com]
ok: [test2.example.com]
ok: [192.168.2.2]
```

If you provide a comma-separated list of groups, then all hosts in any of those groups will be targeted:

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: lab,datacenter1
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost1.example.com]
ok: [labhost2.example.com]
ok: [test1.example.com]
```

You can also mix managed hosts, host groups, and wildcards, as shown below:

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: lab,data*,192.168.2.2
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost1.example.com]
ok: [labhost2.example.com]
ok: [test1.example.com]
ok: [test2.example.com]
ok: [data.example.com]
ok: [192.168.2.2]
```

**NOTE**

The colon character (:) can be used instead of a comma. However, the comma is the preferred separator, especially when working with IPv6 addresses as managed host names. You may see the colon syntax in older examples.

If an item in a list starts with an ampersand character (&), then hosts must match that item in order to match the host pattern. It operates similarly to a logical AND.

For example, based on our example inventory, the following host pattern matches machines in the `lab` group only if they are also in the `datacenter1` group:

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: lab,&datacenter1
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost1.example.com]
```

You could also specify that machines in the `datacenter1` group match only if they are in the `lab` group with the host patterns `&lab,datacenter1` or `datacenter1,&lab`.

You can exclude hosts that match a pattern from a list by using the exclamation point or "bang" character (!) in front of the host pattern. This operates like a logical NOT.

This example matches all hosts defined in the `datacenter` group, except `test2.example.com` based on the example inventory:

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: datacenter,!test2.example.com
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [labhost1.example.com]
ok: [test1.example.com]
ok: [labhost2.example.com]
```

The pattern `'!test2.example.com,datacenter'` could have been used in the preceding example to achieve the same result.

The final example shows the use of a host pattern that matches all hosts in the test inventory, except the managed hosts in the `datacenter1` group.

```
[student@controlnode ~]$ cat playbook.yml
---
- hosts: all,!datacenter1
...output omitted...
[student@controlnode ~]$ ansible-playbook playbook.yml

PLAY [Test Host Patterns] **************************************************

TASK [Gathering Facts] *****************************************************
ok: [web.example.com]
ok: [data.example.com]
ok: [labhost2.example.com]
ok: [test2.example.com]
ok: [192.168.2.1]
ok: [192.168.2.2]
```

**REFERENCES**

[Working with Patterns — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/intro_patterns.html)

[Working with Inventory — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)



____________________

### Guided Exercise: Selecting Hosts with Host Patterns

In this exercise, you will explore how to use host patterns to specify hosts from the inventory for plays or ad hoc commands. You will be provided with several example inventories to explore host patterns.

**Outcomes**

You will be able to use different host patterns to access various hosts in an inventory.

Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab projects-host start** command. The script creates the `projects-host` project directory, and then downloads the Ansible configuration file and the host inventory file needed for this exercise.

```
[student@workstation ~]$ lab projects-host start
```

1. On `workstation`, change to the working directory for the exercise, `/home/student/projects-host`, and review the contents of the directory.

   ```
   [student@workstation ~]$ cd ~/projects-host
   [student@workstation projects-host]$
   ```

   1. List the contents of the directory.

      ```
      [student@workstation projects-host]$ ls
      ansible.cfg inventory1 inventory2 playbook.yml
      ```

   2. Inspect the example inventory file, `inventory1`. Notice how the inventory is organized. Explore which hosts and groups are in the inventory, and which domains are used.

      ```
      srv1.example.com
      srv2.example.com
      s1.lab.example.com
      s2.lab.example.com
      
      [web]
      jupiter.lab.example.com
      saturn.example.com
      
      [db]
      db1.example.com
      db2.example.com
      db3.example.com
      
      [lb]
      lb1.lab.example.com
      lb2.lab.example.com
      
      [boston]
      db1.example.com
      jupiter.lab.example.com
      lb2.lab.example.com
      
      [london]
      db2.example.com
      db3.example.com
      file1.lab.example.com
      lb1.lab.example.com
      
      [dev]
      web1.lab.example.com
      db3.example.com
      
      [stage]
      file2.example.com
      db2.example.com
      
      [prod]
      lb2.lab.example.com
      db1.example.com
      jupiter.lab.example.com
      
      [function:children]
      web
      db
      lb
      city
      
      [city:children]
      boston
      london
      environments
      
      [environments:children]
      dev
      stage
      prod
      new
      
      [new]
      172.25.252.23
      172.25.252.44
      172.25.252.32
      ```

   3. Inspect the example inventory file, `inventory2`. Notice how the inventory is organized. Explore which hosts and groups are in the inventory, and which domains are used.

      ```
      workstation.lab.example.com
      
      [london]
      servera.lab.example.com
      
      [berlin]
      serverb.lab.example.com
      
      [tokyo]
      serverc.lab.example.com
      
      [atlanta]
      serverd.lab.example.com
      
      [europe:children]
      london
      berlin
      ```

   4. Lastly, inspect the contents of the playbook, `playbook.yml`. Notice how the playbook uses the `debug` module to display the name of each managed host.

      ```
      ---
      - name: Resolve host patterns
        hosts:
        tasks:
          - name: Display managed host name
            debug:
              msg: "{{ inventory_hostname }}"
      ```

2. Using an ad hoc command, determine if the `db1.example.com` server is present in the `inventory1` inventory file.

   ```
   [student@workstation projects-host]$ ansible db1.example.com -i inventory1 \
   > --list-hosts
     hosts (1):
       db1.example.com
   ```

3. Using an ad hoc command, reference an IP address contained in the `inventory1` inventory with a host pattern.

   ```
   [student@workstation projects-host]$ ansible 172.25.252.44 -i inventory1 \
   > --list-hosts
    hosts (1):
       172.25.252.44
   ```

4. With an ad hoc command, use the `all` group to list all managed hosts in the `inventory1` inventory file.

   ```
   [student@workstation projects-host]$ ansible all -i inventory1 --list-hosts
     hosts (17):
       srv1.example.com
       srv2.example.com
       s1.lab.example.com
       s2.lab.example.com
       jupiter.lab.example.com
       saturn.example.com
       db1.example.com
       db2.example.com
       db3.example.com
       lb1.lab.example.com
       lb2.lab.example.com
       file1.lab.example.com
       web1.lab.example.com
       file2.example.com
       172.25.252.23
       172.25.252.44
       172.25.252.32
   ```

5. With an ad hoc command, use the asterisk (*) character to list all hosts that end in `.example.com` in the `inventory1` inventory file.

   ```
   [student@workstation projects-host]$ ansible '*.example.com' -i inventory1 \
   > --list-hosts
     hosts (14):
       jupiter.lab.example.com
       saturn.example.com
       db1.example.com
       db2.example.com
       db3.example.com
       lb1.lab.example.com
       lb2.lab.example.com
       file1.lab.example.com
       web1.lab.example.com
       file2.example.com
       srv1.example.com
       srv2.example.com
       s1.lab.example.com
       s2.lab.example.com
   ```

6. As you can see in the output of the preceeding command, there are 14 hosts in the `*.example.com` domain. Modify the host pattern in the previous ad hoc command so that hosts in the `*.lab.example.com` domain are ignored.

   ```
   [student@workstation projects-host]$ ansible '*.example.com, !*.lab.example.com' \
   > -i inventory1 --list-hosts
     hosts (7):
       saturn.example.com
       db1.example.com
       db2.example.com
       db3.example.com
       file2.example.com
       srv1.example.com
       srv2.example.com
   ```

7. Without accessing the groups in the `inventory1` inventory file, use an ad hoc command to list these three hosts: `lb1.lab.example.com`, `s1.lab.example.com`, and `db1.example.com`.

   ```
   [student@workstation projects-host]$ ansible \
   > lb1.lab.example.com,s1.lab.example.com,db1.example.com -i inventory1 \
   > --list-hosts
     hosts (3):
       lb1.lab.example.com
       s1.lab.example.com
       db1.example.com
   ```

8. Use a wildcard host pattern in an ad hoc command to list hosts that start with a `172.25.` IP address in the `inventory1` inventory file.

   ```
   [student@workstation projects-host]$ ansible '172.25.*' -i inventory1 --list-hosts
     hosts (3):
       172.25.252.23
       172.25.252.44
       172.25.252.32
   ```

9. Use a host pattern in an ad hoc command to list all hosts in the `inventory1` inventory file that start with the letter "s."

   ```
   [student@workstation projects-host]$ ansible 's*' -i inventory1 --list-hosts
     hosts (7):
       saturn.example.com
       srv1.example.com
       srv2.example.com
       s1.lab.example.com
       s2.lab.example.com
       file2.example.com
       db2.example.com
   ```

   Notice the `file2.example.com` and `db2.example.com` hosts in the output of the preceding command. They appear in the list because they are both members of a group called `stage`, which also begins with the letter "s."

10. Using a list and wildcard host patterns in an ad hoc command, list all hosts in the `inventory1` inventory in the `prod` group, those hosts with an IP address beginning with `172`, and hosts that contain `lab` in their name.

    ```
    [student@workstation projects-host]$ ansible 'prod,172*,*lab*' -i inventory1 \
    > --list-hosts
      hosts (11):
        lb2.lab.example.com
        db1.example.com
        jupiter.lab.example.com
        172.25.252.23
        172.25.252.44
        172.25.252.32
        lb1.lab.example.com
        file1.lab.example.com
        web1.lab.example.com
        s1.lab.example.com
        s2.lab.example.com
    ```

11. Use an ad hoc command to list all hosts that belong to both the `db` and `london` groups.

    ```
    [student@workstation projects-host]$ ansible 'db,&london' -i inventory1 \
    > --list-hosts
      hosts (2):
        db2.example.com
        db3.example.com
    ```

12. Modify the `hosts` value in the `playbook.yml` file so that all servers in the `london` group are targeted. Execute the playbook using the `inventory2` inventory file.

    ```
    ...output omitted...
      hosts: london
    ...output omitted...
    ```

    ```
    [student@workstation projects-host]$ ansible-playbook -i inventory2 playbook.yml
    ...output omitted...
    TASK [Gathering Facts] ************************************************
    ok: [servera.lab.example.com]
    ...output omitted...
    ```

13. Modify the `hosts` value in the `playbook.yml` file so that all servers in the `europe` nested group are targeted. Execute the playbook using the `inventory2` inventory file.

    ```
    ...output omitted...
      hosts: europe
    ...output omitted...
    ```

    ```
    [student@workstation projects-host]$ ansible-playbook -i inventory2 playbook.yml
    ...output omitted...
    TASK [Gathering Facts] ************************************************
    ok: [servera.lab.example.com]
    ok: [serverb.lab.example.com]
    ...output omitted...
    ```

14. Modify the `hosts` value in the `playbook.yml` file so that all servers that do not belong to any group are targeted. Execute the playbook using the `inventory2` inventory file.

    ```
    ...output omitted...
      hosts: ungrouped
    ...output omitted...
    ```

    ```
    [student@workstation projects-hosts]$ ansible-playbook -i inventory2 playbook.yml
    ...output omitted...
    TASK [Gathering Facts] ************************************************
    ok: [workstation.lab.example.com]
    ...output omitted...
    ```

**Finish**

On `workstation`, run the **lab projects-host finish** script to clean up this exercise.

```
[student@workstation ~]$ lab projects-host finish
```

This concludes the guided exercise.



------

### Managing Dynamic Inventories

#### Objectives

After completing this section, you will be able to describe what dynamic inventories are, as well as install and use an existing script as an Ansible dynamic inventory source.

#### Generating Inventories Dynamically

The static inventory files you have worked with so far are easy to write, and are convenient for managing small infrastructures. When working with numerous machines, however, or in an environment where machines come and go very quickly, it can be hard to keep the static inventory files up-to-date.

Most large IT environments have systems that keep track of which hosts are available and how they are organized. For example, there might be an external directory service maintained by a monitoring system such as Zabbix, or on FreeIPA or Active Directory servers. Installation servers such as Cobbler, or management services such as Red Hat Satellite, might track deployed bare-metal systems. Similarly, cloud services such as Amazon Web Services EC2 or an OpenStack deployment, or virtual machine infrastructures based on VMware or Red Hat Virtualization, might be sources of information about the instances and virtual machines that come and go.

Ansible supports *dynamic inventory* scripts that retrieve current information from these types of sources whenever Ansible executes, allowing the inventory to be updated in real time. These scripts are executable programs that collect information from some external source and output the inventory in JSON format.

Dynamic inventory scripts are used just like static inventory text files. The location of the inventory is specified either directly in the current `ansible.cfg` file, or using the `-i` option. If the inventory file is executable, then it is treated as a dynamic inventory program and Ansible attempts to run it to generate the inventory. If the file is not executable, then it is treated as a static inventory.

**NOTE**

The inventory location can be configured in the `ansible.cfg` configuration file with the `inventory` parameter. By default, it is configured to `/etc/ansible/hosts`.

#### Contributed Scripts

A number of existing dynamic inventory scripts were contributed to the Ansible project by the open source community. They are not included in the ansible package or officially supported by Red Hat. These scripts are available from the Ansible GitHub site at `https://github.com/ansible/ansible/tree/devel/contrib/inventory`.

Some data sources, or platforms, that are targeted by contributed dynamic inventory scripts include:

- Private cloud platforms, such as Red Hat OpenStack Platform.
- Public cloud platforms, such as Rackspace Cloud, Amazon Web Services EC2, and Google Compute Engine.
- Virtualization platforms, such as Red Hat Virtualization (oVirt), and VMware vSphere.
- Platform-as-a-Service solutions, such as OpenShift Container Platform.
- Life-cycle management tools, such as Foreman (with Red Hat Satellite 6 or stand-alone) and Spacewalk (upstream of Red Hat Satellite 5).
- Hosting providers, such as Digital Ocean and Linode.

Each script may have its own dependencies and requirements in order to function. The contributed scripts are mostly written in Python, but that is not a requirement for dynamic inventory scripts.

#### Writing Dynamic Inventory Programs

If a dynamic inventory script does not exist for the directory system or infrastructure in use, you can write a custom dynamic inventory program. The custom program can be written in any programming language, but must return inventory information in JSON format when passed appropriate options.

The **ansible-inventory** command can be a helpful tool for learning how to author Ansible inventories in JSON format. You can use the **ansible-inventory** command, available since Ansible 2.4, to view an inventory file in JSON format.

To display the contents of the inventory file in JSON format, run the **ansible-inventory --list** command. You can use the `-i` option to specify the location of the inventory file to process, or just use the default inventory set by the current Ansible configuration.

The following example demonstrates using the **ansible-inventory** command to process an INI-style inventory file and output it in JSON format.

```
[student@workstation projects-host]$ cat inventory
workstation1.lab.example.com

[webservers]
web1.lab.example.com
web2.lab.example.com

[databases]
db1.lab.example.com
db2.lab.example.com

[student@workstation projects-host]$ ansible-inventory -i inventory --list
{
    "_meta": {
        "hostvars": {
            "db1.lab.example.com": {},
            "db2.lab.example.com": {},
            "web1.lab.example.com": {},
            "web2.lab.example.com": {},
            "workstation1.lab.example.com": {}
        }
    },
    "all": {
        "children": [
            "databases",
            "ungrouped",
            "webservers"
        ]
    },
    "databases": {
        "hosts": [
            "db1.lab.example.com",
            "db2.lab.example.com"
        ]
    },
    "ungrouped": {
        "hosts": [
            "workstation1.lab.example.com"
        ]
    },
    "webservers": {
        "hosts": [
            "web1.lab.example.com",
            "web2.lab.example.com"
        ]
    }
}
```

If you want to write your own dynamic inventory script, more detailed information is available at [Developing Dynamic Inventory Sources](http://docs.ansible.com/ansible/dev_guide/developing_inventory.html) in the *Ansible Developer Guide*. The following is a brief overview.

Start the script with an appropriate interpreter line (for example, `#!/usr/bin/python`) and should be executable so that Ansible can run it.

When passed the `--list` option, the script must print a JSON-encoded hash/dictionary of all the hosts and groups in the inventory.

In its simplest form, a group can be a list of managed hosts. In this example of the JSON-encoded output from an inventory script, `webservers` is a host group that has `web1.lab.example.com` and `web2.lab.example.com` as managed hosts in the group. The `databases` host group includes the `db1.lab.example.com` and `db2.lab.example.com` hosts as members.

```
[student@workstation ~]$ ./inventoryscript --list
{
  "webservers"  : [ "web1.lab.example.com", "web2.lab.example.com" ],
  "databases"  : [ "db1.lab.example.com", "db2.lab.example.com" ]
}
```

Alternatively, each group's value can be a JSON hash/dictionary containing a list of each managed host, any child groups, and any group variables that might be set. The next example shows the JSON-encoded output for a more complex dynamic inventory. The `boston` group has two child groups (`backup` and `ipa`), three managed hosts of its own, and a group variable set (`example_host: false`).

```
{
   "webservers" : [
      "web1.demo.example.com",
      "web2.demo.example.com"
   ],
   "boston" : {
      "children" : [
         "backup",
         "ipa"
      ],
      "vars" : {
         "example_host" : false
      },
      "hosts" : [
         "server1.demo.example.com",
         "server2.demo.example.com",
         "server3.demo.example.com"
      ]
   },
   "backup" : [
      "server4.demo.example.com"
   ],
   "ipa" : [
      "server5.demo.example.com"
   ],
   "_meta" : {
      "hostvars" : {
        "server5.demo.example.com": {
          "ntpserver": "ntp.demo.example.com",
          "dnsserver": "dns.demo.example.com"
        }
      }
   }
}
```

The script should also support the `--host *managed-host*` option. This option must print a JSON hash/dictionary consisting of variables which are associated with that host, or an empty JSON hash/dictionary.

```
[student@workstation ~]$ ./inventoryscript --host server5.demo.example.com
{
    "ntpserver" : "ntp.demo.example.com",
    "dnsserver" : "dns.demo.example.com"
}
```

**NOTE**

When called with the `--host *hostname*` option, the script must print a JSON hash/dictionary of the variables for the specified host. An empty JSON hash or dictionary may be printed if there are no variables provided.

Optionally, if the `--list` option returns a top-level element called `_meta`, it is possible to return all host variables in one script call, which improves script performance. In that case, `--host` calls are not made.

See [Developing Dynamic Inventory Sources](http://docs.ansible.com/ansible/developing_inventory.html) for more information.

#### Managing Multiple Inventories

Ansible supports the use of multiple inventories in the same run. If the location of the inventory is a directory (whether set by the `-i` option, the value of the `inventory` parameter, or in some other way), then all inventory files included in the directory, either static or dynamic, are combined to determine the inventory. The executable files within that directory are used to retrieve dynamic inventories, and the other files are used as static inventories.

Inventory files should not depend on other inventory files or scripts in order to resolve. For example, if a static inventory file specifies that a particular group should be a child of another group, then it also needs to have a placeholder entry for that group, even if all members of that group come from the dynamic inventory. Consider the `cloud-east` group in the following example:

```
[cloud-east]

[servers]
test.demo.example.com

[servers:children]
cloud-east
```

This ensures that no matter the order in which inventory files are parsed, all of them are internally consistent.

**NOTE**

The order in which inventory files are parsed is not specified by the documentation. Currently, when multiple inventory files exist, they are parsed in alphabetical order. If one inventory source depends on information from another inventory source, then the order in which they are loaded may determine if the inventory file works as expected or throws an error. Therefore, it is important to make sure that all files are self-consistent to avoid unexpected errors.

Ansible ignores files in an inventory directory if they end with certain suffixes. This can be controlled with the `inventory_ignore_extensions` directive in the Ansible configuration file. More information is available in the Ansible documentation.

**REFERENCES**

[Working With Dynamic Inventory: Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/intro_dynamic_inventory.html)

[Developing Dynamic Inventory: Ansible Documentation](https://docs.ansible.com/ansible/latest/dev_guide/developing_inventory.html)



------

### Guided Exercise: Managing Dynamic Inventories

In this exercise, you will install custom scripts that dynamically generate a list of inventory hosts.

**Outcomes**

You will be able to install and use existing dynamic inventory scripts.

Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab projects-inventory start** command. It ensures Ansible is installed on `workstation` and also creates the `/home/student/projects-inventory` working directory for this exercise.

```
[student@workstation ~]$ lab projects-inventory start
```

1. On `workstation`, change to the working directory for the exercise, `/home/student/projects-inventory`. View the contents of the `ansible.cfg` Ansible configuration file. The configuration file sets the inventory location to `inventory`.

   ```
   [defaults]
   inventory = inventory
   ```

2. Create the `/home/student/projects-inventory/inventory` directory.

   ```
   [student@workstation projects-inventory]$ mkdir inventory
   ```

3. From `http://materials.example.com/labs/projects-inventory/` download the `inventorya.py`, `inventoryw.py`, and `hosts` files to your `/home/student/projects-inventory/inventory` directory. Both of the files ending in `.py` are scripts that generate dynamic inventories, and the third file is a static inventory.

   - The `inventorya.py` script provides the `webservers` group, which includes the `servera.lab.example.com` host.
   - The `inventoryw.py` script provides the `workstation.lab.example.com` host.
   - The `hosts` static inventory file defines the `servers` group, which is a parent group of the `webservers` group.

   ```
   [student@workstation projects-inventory]$ wget \
   > http://materials.example.com/labs/projects-inventory/inventorya.py \
   > -O inventory/inventorya.py
   [student@workstation projects-inventory]$ wget \
   > http://materials.example.com/labs/projects-inventory/inventoryw.py \
   > -O inventory/inventoryw.py
   [student@workstation projects-inventory]$ wget \
   > http://materials.example.com/labs/projects-inventory/hosts \
   > -O inventory/hosts
   ```

4. Using the **ansible** command with the **inventorya.py** script as the inventory, list the managed hosts associated with the `webservers` group. An error relating to the permissions of `inventorya.py` is raised.

   ```
   [student@workstation projects-inventory]$ ansible -i inventory/inventorya.py \
   > webservers --list-hosts
    [WARNING]:  * Failed to parse /home/student/projects-inventory/inventory/inventorya.py with script plugin: problem running /home/student/projects-inventory/inventory/inventorya.py --list ([Errno 13] Permission denied)
   
    [WARNING]:  * Failed to parse /home/student/projects-inventory/inventory/inventorya.py with ini plugin: /home/student/projects-inventory/inventory/inventorya.py:3: Expected key=value host variable assignment, got: subprocess
   
    [WARNING]: Unable to parse /home/student/projects-inventory/inventory/inventorya.py as an inventory source
   
    [WARNING]: No inventory was parsed, only implicit localhost is available
   
    [WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'
   
    [WARNING]: Could not match supplied host pattern, ignoring: webservers
   
     hosts (0):
   ```

5. Check the current permissions of the `inventorya.py` script, and change them to 755.

   ```
   [student@workstation projects-inventory]$ ls -la inventory/inventorya.py
   -rw-rw-r--. 1 student student 639 Apr 29 14:20 inventory/inventorya.py
   [student@workstation projects-inventory]$ chmod 755 inventory/inventorya.py
   ```

6. Change the permissions of the `inventoryw.py` script to 755.

   ```
   [student@workstation projects-inventory]$ chmod 755 inventory/inventoryw.py
   ```

7. Check the current output of the `inventorya.py` script using the `--list` parameter. The hosts associated with the `webservers` group are displayed.

   ```
   [student@workstation projects-inventory]$ inventory/inventorya.py --list
   {"webservers": {"hosts": ["servera.lab.example.com"], "vars": { }}}
   ```

8. Check the current output of the `inventoryw.py` script using the `--list` parameter. The `workstation.lab.example.com` host is displayed.

   ```
   [student@workstation projects-inventory]$ inventory/inventoryw.py --list
   {"all": {"hosts": ["workstation.lab.example.com"], "vars": { }}}
   ```

9. Check the `servers` group definition in the `/home/student/projects-inventory/inventory/hosts` file. The `webservers` group defined in the dynamic inventory is configured as a child of the `servers` group.

   ```
   [student@workstation projects-inventory]$ cat inventory/hosts
   [servers:children]
   webservers
   ```

10. Run the following command to verify the list of hosts in the `webservers` group. An error about the `webservers` group being undefined is raised.

    ```
    [student@workstation projects-inventory]$ ansible webservers --list-hosts
     [WARNING]:  * Failed to parse /home/student/projects-inventory/inventory/hosts with yaml plugin: Syntax Error while loading YAML.   found unexpected ':'  The error appears to have been in '/home/student/projects-inventory/inventory/hosts': line 1, column 9, but may be elsewhere in the file depending on the exact syntax problem.  The offending line appears to be:   [servers:children]         ^ here
    
     [WARNING]:  * Failed to parse /home/student/projects-inventory/inventory/hosts with ini plugin: /home/student/projects-inventory/inventory/hosts:2: Section [servers:children] includes undefined group: webservers
    
     [WARNING]: Unable to parse /home/student/projects-inventory/inventory/hosts as an inventory source
    
      hosts (1):
        servera.lab.example.com
    ```

11. To avoid this problem, the static inventory must have a placeholder entry that defines an empty `webservers` host group. It is important for the static inventory to define any host group it references, because it is possible that it could dynamically disappear from the external source, causing this error.

    Edit the `/home/student/projects-inventory/inventory/hosts` file so it contains the following content:

    ```
    [webservers]
    
    [servers:children]
    webservers
    ```

    **IMPORTANT**

    If the dynamic inventory script that provides the host group is named so that it sorts before the static inventory references it, then you will not see this error. However, if the host group ever disappears from the dynamic inventory, and you do not have a placeholder, the static inventory will be referencing a missing host group and the error will break the parsing of the inventory.

12. Rerun the following command to verify the list of hosts in the `webservers` group. It should work without any errors.

    ```
    [student@workstation projects-inventory]$ ansible webservers --list-hosts
      hosts (1):
        servera.lab.example.com
    ```

**Finish**

On `workstation`, run the **lab projects-inventory finish** command to clean up this exercise.

```
[student@workstation ~]$ lab projects-inventory finish
```

This concludes the guided exercise.



------

### Configuring Parallelism

#### Objectives

After completing this section, you will be able to tune the number of simultaneous connections that Ansible opens to managed hosts, and how Ansible processes groups of managed hosts through the play's tasks.

#### Configure Parallelism in Ansible Using Forks

When Ansible processes a playbook, it runs each play in order. After determining the list of hosts for the play, Ansible runs through each task in order. Normally, all hosts must successfully complete a task before any host starts the next task in the play.

In theory, Ansible could simultaneously connect to all hosts in the play for each task. This works fine for small lists of hosts. But, if the play targets hundreds of hosts, this can put a heavy load on the control node.

The maximum number of simultaneous connections that Ansible makes is controlled by the `forks` parameter in the Ansible configuration file. It is set to `5` by default, which can be verified using one of the following.

```
[student@demo ~]$ grep forks ansible.cfg
forks          = 5
[student@demo ~]$ ansible-config dump |grep -i forks
DEFAULT_FORKS(default) = 5
[student@demo ~]$ ansible-config list |grep -i forks
DEFAULT_FORKS:
  description: Maximum number of forks Ansible will use to execute tasks on target
  - {name: ANSIBLE_FORKS}
  - {key: forks, section: defaults}
  name: Number of task forks
```

For example, assume an Ansible control node is configured with the default value of five forks and the play has ten managed hosts. Ansible will execute the first task in the play on the first five managed hosts, followed by a second round of execution of the first task on the other five managed hosts. After the first task has been executed on all the managed hosts, Ansible will proceed with executing the next task across all the managed hosts in groups of five hosts at a time. Ansible will do this with each task in turn until the play ends.

The default for `forks` is set to be very conservative. If your control node is managing Linux hosts, most tasks will run on the managed hosts and the control node has less load. In this case, you can usually set `forks` to a much higher value, possibly closer to 100, and see performance improvements.

If your playbooks run a lot of code on the control node, then you should raise the fork limit judiciously. If you use Ansible to manage network routers and switches, then most of those modules run on the control node and not on the network device. Because of the higher load this places on the control node, its capacity to support increases in the number of forks will be significantly lower than for a control node managing only Linux hosts.

You can override the default setting for `forks` from the command line in the Ansible configuration file. Both the **ansible** and the **ansible-playbook** commands offer the `-f` or `--forks` options to specify the number of forks to use.

#### Managing Rolling Updates

Normally, when Ansible runs a play, it makes sure that all managed hosts have completed each task before starting the next task. After all managed hosts have completed all tasks, then any notified handlers are run.

However, running all tasks on all hosts can lead to undesirable behavior. For example, if a play updates a cluster of load balanced web servers, it might need to take each web server out of service while the update takes place. If all the servers are updated in the same play, they could all be out of service at the same time.

One way to avoid this problem is to use the `serial` keyword to run the hosts through the play in batches. Each batch of hosts will be run through the entire play before the next batch is started.

In the example below, Ansible executes the play on two managed hosts at a time, until all managed hosts have been updated. Ansible begins by executing the tasks in the play on the first two managed hosts. If either or both of those two hosts notified the handler, then Ansible runs the handler as needed for those two hosts. When the play execution is complete on these two managed hosts, Ansible repeats the process on the next two managed hosts. Ansible continues to run the play in this way until all managed hosts have been updated.

```
---
- name: Rolling update
  hosts: webservers
  serial: 2
  tasks:
  - name: latest apache httpd package is installed
    yum:
      name: httpd
      state: latest
    notify: restart apache

  handlers:
  - name: restart apache
    service:
      name: httpd
      state: restarted
```

Suppose the `webservers` group in the previous example contains five web servers that reside behind a load balancer. With the `serial` parameter set to `2`, the play will run up to two web servers at a time. Thus, a majority of the five web servers will always be available.

In contrast, in the absence of the `serial` keyword, the play execution and resulting handler execution would occur across all five web servers at the same time. This would probably lead to a service outage because web services on all the web servers would be restarted at the same time.

**IMPORTANT**

For certain purposes, each batch of hosts counts as if it were a full play running on a subset of hosts. This means that if an entire batch fails, the play fails, which causes the entire playbook run to fail.

In the previous scenario with `serial: 2` set, if something is wrong and the play fails for the first two hosts processed, then the playbook will abort and the remaining three hosts will not be run through the play. This is a useful feature because only a subset of the servers would be unavailable, leaving the service degraded rather than down.

The `serial` keyword can also be specified as a percentage. This percentage is applied to the total number of hosts in the play to determine the rolling update batch size. Regardless of the percentage, the number of hosts per pass will always be 1 or greater.

**REFERENCES**

[Rolling Update Batch Size — Delegation, Rolling Updates, and Local Actions — Ansible Documentation](http://docs.ansible.com/ansible/playbooks_delegation.html#rolling-update-batch-size)

[Ansible Performance Tuning (For Fun and Profit)](https://www.ansible.com/blog/ansible-performance-tuning)



------

### Guided Exercise: Configuring Parallelism

In this exercise, you will explore the effects of different serial and forks directives on how a play is processed by Ansible.

**Outcomes**

You will be able to tune parallel and serial executions of a playbook across multiple managed hosts.

Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab projects-parallelism start** command. The script checks that Ansible is installed on `workstation`, and then creates the directory structure and associated files for the lab environment.

```
[student@workstation ~]$ lab projects-parallelism start
```

1. Change to the `/home/student/projects-parallelism` directory. Examine the contents of the project directory to become familiar with the project files.

   1. Examine the contents of the `ansible.cfg` file. Note that the inventory file is set to `inventory`. Note also that the `forks` parameter is set to `4`.

      ```
      [defaults]
      inventory=inventory
      remote_user=devops
      forks=4
      ...output omitted...
      ```

   2. Examine the contents of the `inventory` file. Note that it contains a host group, `webservers`, which contains four hosts.

      ```
      [webservers]
      servera.lab.example.com
      serverb.lab.example.com
      serverc.lab.example.com
      serverd.lab.example.com
      ```

   3. Examine the contents of the `playbook.yml` file. The playbook executes on the `webservers` host group, ensures that the latest httpd package is installed, and that the `httpd` service is enabled and started.

      ```
      ---
          - name: Update web server
            hosts: webservers
      
            tasks:
              - name: Lastest httpd package installed
                  yum:
                    name: httpd
                    state: latest
                  notify:
                    - Restart httpd
      
            handlers:
              - name: Restart httpd
                service:
                  name: httpd
                  enabled: yes
                  state: restarted
      ```

   4. Finally, examine the contents of the `remove_apache.yml` file. The playbook executes on the `webservers` host group, ensures that the `httpd` service is disabled and stopped, and then ensures that the httpd package is not installed.

      ```
      ---
      - hosts: webservers
        tasks:
          - name: Disable httpd service
            service:
              name: httpd
              enabled: no
              state: stopped
          - name: Remove httpd package
            yum:
              name: httpd
              state: absent
      ```

2. Execute the `playbook.yml` playbook, using the **time** command to determine how long it takes for the playbook to run. Watch the playbook as it runs. Note how Ansible performs each task on all four hosts at the same time.

   ```
   [student@workstation projects-parallelism]$ time ansible-playbook playbook.yml
   PLAY [Update apache] *******************************************************
   
   TASK [Gathering Facts] *****************************************************
   ok: [servera.lab.example.com]
   ok: [serverd.lab.example.com]
   ok: [serverb.lab.example.com]
   ok: [serverc.lab.example.com]
   
   ...output omitted...
   
   real    0m22.701s
   user    0m23.275s
   sys     0m2.637s
   ```

3. Execute the `remove_apache.yml` playbook to stop and disable the `httpd` service and to remove the httpd package.

   ```
   [student@workstation projects-parallelism]$ ansible-playbook remove_apache.yml
   ```

4. Change the value of the `forks` parameter to `1` in `ansible.cfg`.

   ```
   [defaults]
   inventory=inventory
   remote_user=devops
   forks=1
   ...output omitted...
   ```

5. Re-execute the `playbook.yml` playbook, using the **time** command to determine how long it takes for the playbook to run. Watch the playbook as it runs. Note that this time Ansible performs each task on one host at a time. Also note how decreasing the number of forks caused the playbook execution to take longer than before.

   ```
   [student@workstation projects-parallelism]$ time ansible-playbook playbook.yml
   
   PLAY [Update apache] *******************************************************
   
   TASK [Gathering Facts] *****************************************************
   ok: [serverb.lab.example.com]
   ok: [servera.lab.example.com]
   ok: [serverc.lab.example.com]
   ok: [serverd.lab.example.com]
   
   ...output omitted...
   
   real    0m37.853s
   user    0m22.414s
   sys     0m4.749s
   ```

6. Execute the `remove_apache.yml` playbook to stop and disable the `httpd` service and to remove the httpd package.

   ```
   [student@workstation projects-parallelism]$ ansible-playbook remove_apache.yml
   ```

7. Set the value of the `forks` parameter to `2` in `ansible.cfg`.

   ```
   [defaults]
   inventory=inventory
   remote_user=devops
   forks=2
   ...output omitted...
   ```

8. Add the following `serial` parameter to the play in the `playbook.yml` playbook so that the play only executes on two hosts at a time. The beginning of the playbook should display as follows:

   ```
   ---
   - name: Update web server
     hosts: webservers
     serial: 2
   ```

9. Re-execute the `playbook.yml` playbook. Watch the playbook as it runs. Note how Ansible executes the entire play on just two hosts before re-executing the play on the two remaining hosts.

   ```
   [student@workstation projects-parallelism]$ ansible-playbook playbook.yml
   
   PLAY [Update apache] *******************************************************
   
   TASK [Gathering Facts] *****************************************************
   ok: [servera.lab.example.com]
   ok: [serverb.lab.example.com]
   
   TASK [Latest version of apache installed] **********************************
   changed: [servera.lab.example.com]
   changed: [serverb.lab.example.com]
   
   ...output omitted...
   
   PLAY [Update apache] *******************************************************
   
   TASK [Gathering Facts] *****************************************************
   ok: [serverc.lab.example.com]
   ok: [serverd.lab.example.com]
   
   TASK [Latest version of apache installed] **********************************
   changed: [serverd.lab.example.com]
   changed: [serverc.lab.example.com]
   
   ...output omitted...
   ```

10. Execute the `remove_apache.yml` playbook to stop and disable the `httpd` service and to remove the httpd package.

    ```
    [student@workstation projects-parallelism]$ ansible-playbook remove_apache.yml
    ```

11. Set the value of the `forks` parameter back to `4` in `ansible.cfg`.

    ```
    [defaults]
    inventory=inventory
    remote_user=devops
    forks=4
    ...output omitted...
    ```

12. Set the `serial` parameter in the `playbook.yml` playbook to `3`. The beginning of the playbook should display as follows:

    ```
    ---
    - name: Update web server
      hosts: webservers
      serial: 3
    ```

13. Re-execute the `playbook.yml` playbook. Watch the playbook as it runs. Note how Ansible executes the entire play on just three hosts and then re-executes the play on the one remaining host.

    ```
    [student@workstation projects-parallelism]$ ansible-playbook playbook.yml
    
    PLAY [Update apache] *******************************************************
    
    TASK [Gathering Facts] *****************************************************
    ok: [servera.lab.example.com]
    ok: [serverb.lab.example.com]
    ok: [serverc.lab.example.com]
    
    TASK [Latest version of apache installed] **********************************
    changed: [servera.lab.example.com]
    changed: [serverb.lab.example.com]
    changed: [serverc.lab.example.com]
    
    ...output omitted...
    
    PLAY [Update apache] *******************************************************
    
    TASK [Gathering Facts] *****************************************************
    ok: [serverd.lab.example.com]
    
    TASK [Latest version of apache installed] **********************************
    changed: [serverd.lab.example.com]
    
    ...output omitted...
    ```

**Finish**

On `workstation`, run the **lab projects-parallelism finish** command to clean up this exercise.

```
[student@workstation ~]$ lab projects-parallelism finish
```

This concludes the guided exercise.



------

###  Including and Importing Files

#### Objectives

After completing this section, you will be able to manage large playbooks by importing or including other playbooks or tasks from external files, either unconditionally or based on a conditional test.

#### Managing Large Playbooks

When a playbook gets long or complex, you can divide it up into smaller files to make it easier to manage. You can combine multiple playbooks into a main playbook modularly, or insert lists of tasks from a file into a play. This can make it easier to reuse plays or sequences of tasks in different projects.

#### Including or Importing Files

There are two operations that Ansible can use to bring content into a playbook. You can *include* content, or you can *import* content.

When you include content, it is a *dynamic* operation. Ansible processes included content during the run of the playbook, as content is reached.

When you import content, it is a *static* operation. Ansible preprocesses imported content when the playbook is initially parsed, before the run starts.

#### Importing Playbooks

The `import_playbook` directive allows you to import external files containing lists of plays into a playbook. In other words, you can have a master playbook that imports one or more additional playbooks.

Because the content being imported is a complete playbook, the `import_playbook` feature can only be used at the top level of a playbook and cannot be used inside a play. If you import multiple playbooks, then they will be imported and run in order.

A simple example of a master playbook that imports two additional playbooks is shown below:

```
- name: Prepare the web server
  import_playbook: web.yml

- name: Prepare the database server
  import_playbook: db.yml
```

You can also interleave plays in your master playbook with imported playbooks.

```
- name: Play 1
  hosts: localhost
  tasks:
    - debug:
        msg: Play 1

- name: Import Playbook
  import_playbook: play2.yml
```

In the preceding example, the `Play 1` runs first, followed by the plays imported from the `play2.yml` playbook.

#### Importing and Including Tasks

You can import or include a list of tasks from a task file into a play. A task file is a file that contains a flat list of tasks:

```
[admin@node ~]$ cat webserver_tasks.yml
- name: Installs the httpd package
  yum:
    name: httpd
    state: latest

- name: Starts the httpd service
  service:
    name: httpd
    state: started
```

**Importing Task Files**

You can statically import a task file into a play inside a playbook by using the `import_tasks` feature. When you import a task file, the tasks in that file are directly inserted when the playbook is parsed. The location of `import_tasks` in the playbook controls where the tasks are inserted and the order in which multiple imports are run.

```
---
- name: Install web server
  hosts: webservers
  tasks:
  - import_tasks: webserver_tasks.yml
```

When you import a task file, the tasks in that file are directly inserted when the playbook is parsed. Because `import_tasks` statically imports the tasks when the playbook is parsed, there are some effects on how it works.

- When using the `import_tasks` feature, conditional statements set on the import, such as `when`, are applied to each of the tasks that are imported.
- You cannot use loops with the `import_tasks` feature.
- If you use a variable to specify the name of the file to import, then you cannot use a host or group inventory variable.

**Including Task Files**

You can also dynamically include a task file into a play inside a playbook by using the `include_tasks` feature.

```
---
- name: Install web server
  hosts: webservers
  tasks:
  - include_tasks: webserver_tasks.yml
```

The `include_tasks` feature does not process content in the playbook until the play is running and that part of the play is reached. The order in which playbook content is processed impacts how the include tasks feature works.

- When using the `include_tasks` feature, conditional statements such as `when` set on the include determine whether or not the tasks are included in the play at all.
- If you run **ansible-playbook --list-tasks** to list the tasks in the playbook, then tasks in the included task files are not displayed. The tasks that include the task files are displayed. By comparison, the `import_tasks` feature would not list tasks that import task files, but instead would list the individual tasks from the imported task files.
- You cannot use **ansible-playbook --start-at-task** to start playbook execution from a task that is in an included task file.
- You cannot use a `notify` statement to trigger a handler name that is in an included task file. You can trigger a handler in the main playbook that includes an entire task file, in which case all tasks in the included file will run.

**NOTE**

You can find a more detailed discussion of the differences in behavior between `import_tasks` and `include_tasks` when conditionals are used at ["Conditionals"](https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html#applying-when-to-roles-imports-and-includes) in the *Ansible User Guide*.

**Use Cases for Task Files**

Consider the following examples where it might be useful to manage sets of tasks as external files separate from the playbook:

- If new servers require complete configuration, then administrators could create various sets of tasks for creating users, installing packages, configuring services, configuring privileges, setting up access to a shared file system, hardening the servers, installing security updates, and installing a monitoring agent. Each of these sets of tasks could be managed through a separate self-contained task file.
- If servers are managed collectively by the developers, the system administrators, and the database administrators, then every organization can write its own task file which can then be reviewed and integrated by the system manager.
- If a server requires a particular configuration, then it can be integrated as a set of tasks that are executed based on a conditional. In other words, including the tasks only if specific criteria are met.
- If a group of servers need to run a particular task or set of tasks, then the tasks might only be run on a server if it is part of a specific host group.

**Managing Task Files**

You can create a dedicated directory for task files, and save all task files in that directory. Then your playbook can simply include or import task files from that directory. This allows construction of a complex playbook while making it easy to manage its structure and components.

#### Defining Variables for External Plays and Tasks

The incorporation of plays or tasks from external files into playbooks using Ansible's import and include features greatly enhance the ability to reuse tasks and playbooks across an Ansible environment. To maximize the possibility of reuse, these task and play files should be as generic as possible. Variables can be used to parameterize play and task elements to expand the application of tasks and plays.

For example, the following task file installs the package needed for a web service, and then enables and starts the necessary service.

```
---
  - name: Install the httpd package
    yum:
      name: httpd
      state: latest
  - name: Start the httpd service
    service:
      name: httpd
      enabled: true
      state: started
```

If you parameterize the package and service elements as shown in the following example, then the task file can also be used for the installation and administration of other software and their services, rather than being useful for web service only.

```
---
  - name: Install the {{ package }} package
    yum:
      name: "{{ package }}"
      state: latest
  - name: Start the {{ service }} service
    service:
      name: "{{ service }}"
      enabled: true
      state: started
```

Subsequently, when incorporating the task file into a playbook, define the variables to use for the task execution as follows:

```
...output omitted...
  tasks:
    - name: Import task file and set variables
      import_tasks: task.yml
      vars:
        package: httpd
        service: service
```

Ansible makes the passed variables available to the tasks imported from the external file.

You can use the same technique to make play files more reusable. When incorporating a play file into a playbook, pass the variables to use for the play execution as follows:

```
...output omitted...
- name: Import play file and set the variable
  import_playbook: play.yml
  vars:
    package: mariadb
```

**IMPORTANT**

Earlier versions of Ansible used an `include` feature to include both playbooks and task files, depending on context. This functionality is being deprecated for a number of reasons.

Prior to Ansible 2.0, `include` operated like a static import. In Ansible 2.0 it was changed to operate dynamically, but this created some limitations. In Ansible 2.1 it became possible for `include` to be dynamic or static depending on task settings, which was confusing and error-prone. There were also issues with ensuring that `include` worked correctly in all contexts.

Thus, `include` was replaced in Ansible 2.4 with new directives such as `include_tasks`, `import_tasks`, and `import_playbook`. You might find examples of `include` in older playbooks, but you should avoid using it in new ones.

**REFERENCES**

[Including and Importing — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_includes.html)

[Creating Reusable Playbooks — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse.html)

[Conditionals — Ansible Documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html)



------

### Guided Exercise: Including and Importing Files

In this exercise, you will include and import playbooks and tasks in a top-level Ansible Playbook.

**Outcomes**

You will be able to include task and playbook files in playbooks.

Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab projects-file start** command. The script creates the working directory, `/home/student/projects-file`, and associated project files.

```
[student@workstation ~]$ lab projects-file start
```

1. On `workstation`, as the `student` user, change to the `/home/student/projects-file` directory.

   ```
   [student@workstation ~]$ cd ~/projects-file
   [student@workstation projects-file]$ 
   ```

2. Review the contents of the three files in the `tasks` subdirectory.

   1. Review the contents of the `tasks/environment.yml` file. The file contains tasks for package installation and service administration.

      ```
      [student@workstation projects-file]$ cat tasks/environment.yml
      ---
        - name: Install the {{ package }} package
          yum:
            name: "{{ package }}"
            state: latest
        - name: Start the {{ service }} service
          service:
            name: "{{ service }}"
            enabled: true
            state: started
      ```

   2. Review the contents of the `tasks/firewall.yml` file. The file contains tasks for installation, administration, and configuration of firewall software.

      ```
      [student@workstation projects-file]$ cat tasks/firewall.yml
      ---
        - name: Install the firewall
          yum:
            name: "{{ firewall_pkg }}"
            state: latest
      
        - name: Start the firewall
          service:
            name: "{{ firewall_svc }}"
            enabled: true
            state: started
      
        - name: Open the port for {{ rule }}
          firewalld:
            service: "{{ item }}"
            immediate: true
            permanent: true
            state: enabled
          loop: "{{ rule }}"
      ```

   3. Review the contents of the `tasks/placeholder.yml` file. This file contains a task for populating a placeholder web content file.

      ```
      [student@workstation projects-file]$ cat tasks/placeholder.yml
      ---
        - name: Create placeholder file
          copy:
            content: "{{ ansible_facts['fqdn'] }} has been customized using Ansible.\n"
            dest: "{{ file }}"
      ```

3. Review the contents of the `test.yml` file in the `plays` subdirectory. This file contains a play which tests connections to a web service.

   ```
   ---
   - name: Test web service
     hosts: localhost
     become: no
     tasks:
       - name: connect to internet web server
         uri:
           url: "{{ url }}"
           status_code: 200
   ```

4. Create a playbook named `playbook.yml`. Define the first play with the name `Configure web server`. The play should execute against the `servera.lab.example.com` managed host defined in the `inventory` file. The beginning of the file should look like the following:

   ```
   ---
   - name: Configure web server
     hosts: servera.lab.example.com
   ```

5. In the `playbook.yml` playbook, define the tasks section with three sets of tasks. Include the first set of tasks from the `tasks/environment.yml` tasks file. Define the necessary variables to install the httpd package and to enable and start the `httpd` service. Import the second set of tasks from the `tasks/firewall.yml` tasks file. Define the necessary variables to install the firewalld package to enable and start the `firewalld` service, and to allow `http` connections. Import the third task set from the `tasks/placeholder.yml` task file.

   1. Create the tasks section in the first play by adding the following entry to the `playbook.yml` playbook.

      ```
        tasks:
      ```

   2. Include the first set of tasks from `tasks/environment.yml` using the `include_tasks` feature. Set the `package` and `service` variables to `httpd`. Set the `svc_state` variable to `started`.

      ```
          - name: Include the environment task file and set the variables
            include_tasks: tasks/environment.yml
            vars:
              package: httpd
              service: httpd
            when: ansible_facts['os_family'] == 'RedHat'
      ```

   3. Import the second set of tasks from `tasks/firewall.yml` using the `import_tasks` feature. Set the `firewall_pkg` and `firewall_svc` variables to `firewalld`. Set the `rule` variable to `http`.

      ```
          - name: Import the firewall task file and set the variables
            import_tasks: tasks/firewall.yml
            vars:
              firewall_pkg: firewalld
              firewall_svc: firewalld
              rule:
                - http
                - https
      ```

   4. Import the last task set from `tasks/placeholder.yml` using the `import_tasks` feature. Set the `file` variable to `/var/www/html/index.html`.

      ```
          - name: Import the placeholder task file and set the variable
            import_tasks: tasks/placeholder.yml
            vars:
              file: /var/www/html/index.html
      ```

6. Add a second and final play to the `playbook.yml` playbook using the contents of the `plays/test.yml` playbook.

   1. Add a second play to the `playbook.yml` playbook to validate the web server installation. Import the play from `plays/test.yml`. Set the `url` variable to `http://servera.lab.example.com`.

      ```
      - name: Import test play file and set the variable
        import_playbook: plays/test.yml
        vars:
          url: 'http://servera.lab.example.com'
      ```

   2. Your playbook should look like the following after the changes are complete:

      ```
      ---
      - name: Configure web server
        hosts: servera.lab.example.com
      
        tasks:
          - name: Include the environment task file and set the variables
            include_tasks: tasks/environment.yml
            vars:
              package: httpd
              service: httpd
            when: ansible_facts['os_family'] == 'RedHat'
      
          - name: Import the firewall task file and set the variables
            import_tasks: tasks/firewall.yml
            vars:
              firewall_pkg: firewalld
              firewall_svc: firewalld
              rule:
                - http
                - https
      
          - name: Import the placeholder task file and set the variable
            import_tasks: tasks/placeholder.yml
            vars:
              file: /var/www/html/index.html
      
      - name: Import test play file and set the variable
        import_playbook: plays/test.yml
        vars:
          url: 'http://servera.lab.example.com'
      ```

   3. Save the changes to the `playbook.yml` playbook.

7. Before running the playbook, verify its syntax is correct by running **ansible-playbook --syntax-check**. If errors are reported, correct them before moving to the next step.

   ```
   [student@workstation projects-file]$ ansible-playbook playbook.yml --syntax-check
   
   playbook: playbook.yml
   ```

8. Execute the `playbook.yml` playbook. The output of the playbook shows the import of the task and play files.

   ```
   [student@workstation projects-file]$ ansible-playbook playbook.yml
   
   PLAY [Configure web server] ***********************************************
   
   TASK [Gathering Facts] ****************************************************
   ok: [servera.lab.example.com]
   
   TASK [Install the httpd package] ******************************************
   changed: [servera.lab.example.com]
   
   TASK [Start the httpd service] ********************************************
   changed: [servera.lab.example.com]
   
   TASK [Install the firewall] ***********************************************
   ok: [servera.lab.example.com]
   
   TASK [Start the firewall] *************************************************
   ok: [servera.lab.example.com]
   
   TASK [Open the port for http] *********************************************
   changed: [servera.lab.example.com]
   
   TASK [Create placeholder file] ********************************************
   changed: [servera.lab.example.com]
   
   PLAY [Test web service] ***************************************************
   
   TASK [Gathering Facts] ****************************************************
   ok: [localhost]
   
   TASK [connect to internet web server] *************************************
   ok: [localhost]
   
   PLAY RECAP ****************************************************************
   localhost                  : ok=2    changed=0    unreachable=0    failed=0
   servera.lab.example.com    : ok=8    changed=4    unreachable=0    failed=0
   ```

**Finish**

On `workstation`, run the **lab projects-file finish** script to clean up this exercise.

```
[student@workstation ~]$ lab projects-file finish
```

This concludes the guided exercise.



________________

### Lab: Managing Large Projects

**Performance Checklist**

In this lab, you will modify a large playbook to be easier to manage by using host patterns, includes, imports, and a dynamic inventory. You will also tune how the playbook is processed by Ansible.

**Outcomes**

You can simplify managed host references in a playbook by specifying host patterns against a dynamic inventory. You should also be able to restructure a playbook so that tasks are imported from external task files and tune the playbook for rolling updates.

**Instructions**

You have inherited a playbook from the previous administrator. The playbook is used to configure a web service on `servera.lab.example.com`, `serverb.lab.example.com`, `serverc.lab.example.com`, and `serverd.lab.example.com`. The playbook also configures the firewall on the four managed hosts so that web traffic is allowed.

Log in to `workstation` as `student` using `student` as the password.

On `workstation`, run the **lab projects-review start** command. This setup script ensures that the managed hosts are reachable on the network. It also ensures that the correct Ansible configuration file, inventory file, and playbook are installed on the control node in the `/home/student/projects-review` directory.

```
[student@workstation ~]$ lab projects-review start
```

Make the following changes to the `playbook.yml` playbook file so that it is easier to manage, and tune it so that future executions use rolling updates to prevent all four web servers from being unavailable at the same time.

1. Simplify the list of managed hosts in the playbook by using a wildcard host pattern. Use the `inventory/inventory.py` dynamic inventory script to verify the wildcard host pattern.

   1. Change directory to the `/home/student/projects-review` working directory. Review the `ansible.cfg` configuration file to determine the location of the inventory file. The inventory is defined as the `inventory` subdirectory and this subdirectory contains an `inventory.py` dynamic inventory script.

      ```
      [student@workstation ~]$ cd ~/projects-review
      [student@workstation projects-review]$ cat ansible.cfg
      [defaults]
      inventory = inventory
      ...output omitted...
      [student@workstation projects-review]$ ll
      total 16
      -rw-rw-r--. 1 student student   33 Dec 19 00:48 ansible.cfg
      drwxrwxr-x. 2 student student 4096 Dec 18 22:35 files
      drwxrwxr-x. 2 student student 4096 Dec 19 01:18 inventory
      -rw-rw-r--. 1 student student  959 Dec 18 23:48 playbook.yml
      [student@workstation projects-review]$ ll inventory/
      total 4
      -rwxrwxr-x. 1 student student 612 Dec 19 01:18 inventory.py
      ```

   2. Make the `inventory/inventory.py` dynamic inventory script executable, and then execute the dynamic inventory script with the `--list` option to display the full list of hosts in the inventory.

      ```
      [student@workstation projects-review]$ chmod 755 inventory/inventory.py
      [student@workstation projects-review]$ inventory/inventory.py --list
      {"all": {"hosts": ["servera.lab.example.com", "serverb.lab.example.com", "serverc.lab.example.com", "serverd.lab.example.com", "workstation.lab.example.com"], "vars": { }}}
      ```

   3. Verify that the host pattern `server*.lab.example.com` correctly identifies the four managed hosts that are targeted by the `playbook.yml` playbook.

      ```
      [student@workstation projects-review]$ ansible server*.lab.example.com \
      > --list-hosts
        hosts (4):
          serverb.lab.example.com
          serverd.lab.example.com
          servera.lab.example.com
          serverc.lab.example.com
      ```

   4. Replace the host list in the `playbook.yml` playbook with the `server*.lab.example.com` host pattern.

      ```
      ---
      - name: Install and configure web service
        hosts: server*.lab.example.com
      ...output omitted...
      ```

   

2. Restructure the playbook so that the first three tasks in the playbook are kept in an external task file located at `tasks/web_tasks.yml`. Use the `import_tasks` feature to incorporate this task file into the playbook.

   1. Create the `tasks` subdirectory.

      ```
      [student@workstation projects-review]$ mkdir tasks
      ```

   2. Place the contents of the first three tasks in the `playbook.yml` playbook into the `tasks/web_tasks.yml` file. The task file should contain the following content:

      ```
      ---
      - name: Install httpd
        yum:
          name: httpd
          state: latest
      
      - name: Enable and start httpd
        service:
          name: httpd
          enabled: true
          state: started
      
      - name: Tuning configuration installed
        copy:
          src: files/tune.conf
          dest: /etc/httpd/conf.d/tune.conf
          owner: root
          group: root
          mode: 0644
        notify:
          - restart httpd
      ```

   3. Remove the first three tasks from the `playbook.yml` playbook and put the following lines in their place to import the `tasks/web_tasks.yml` task file.

      ```
          - name: Import the web_tasks.yml task file
            import_tasks: tasks/web_tasks.yml
      ```

   

3. Restructure the playbook so that the fourth, fifth, and sixth tasks in the playbook are kept in an external task file located at `tasks/firewall_tasks.yml`. Use the `import_tasks` feature to incorporate this task file into the playbook.

   1. Place the contents of the three remaining tasks in the `playbook.yml` playbook into the `tasks/firewall_tasks.yml` file. The task file should contain the following content.

      ```
      ---
      - name: Install firewalld
        yum:
          name: firewalld
          state: latest
      
      - name: Enable and start the firewall
        service:
          name: firewalld
          enabled: true
          state: started
      
      - name: Open the port for http
        firewalld:
          service: http
          immediate: true
          permanent: true
          state: enabled
      ```

   2. Remove the remaining three tasks from the `playbook.yml` playbook and put the following lines in their place to import the `tasks/firewall_tasks.yml` task file.

      ```
          - name: Import the firewall_tasks.yml task file
            import_tasks: tasks/firewall_tasks.yml
      ```

   

4. There is some duplication of tasks between the `tasks/web_tasks.yml` and `tasks/firewall_tasks.yml` files. Move the tasks that install packages and enable services into a new file named `tasks/install_and_enable.yml` and update them to use variables. Replace the original tasks with `import_tasks` statements, passing in appropriate variable values.

   1. Copy the yum and service tasks from `tasks/web_tasks.yml` into a new file named `tasks/install_and_enable.yml`.

      ```
      ---
      - name: Install httpd
        yum:
          name: httpd
          state: latest
      
      - name: Enable and start httpd
        service:
          name: httpd
          enabled: true
          state: started
      ```

   2. Replace the package and service names in `tasks/install_and_enable.yml` with the variables `package` and `service`.

      ```
      ---
      - name: Install {{ package }}
        yum:
          name: "{{ package }}"
          state: latest
      
      - name: Enable and start {{ service }}
        service:
          name: "{{ service }}"
          enabled: true
          state: started
      ```

   3. Replace the yum and service tasks in `tasks/web_tasks.yml` and `tasks/firewall_tasks.yml` with `import_tasks` statements.

      ```
      ---
      - name: Install and start httpd
        import_tasks: install_and_enable.yml
        vars:
          package: httpd
          service: httpd
      ```

      ```
      ---
      - name: Install and start firewalld
        import_tasks: install_and_enable.yml
        vars:
          package: firewalld
          service: firewalld
      ```

   

5. Because the handler for restarting the httpd service could be triggered if there are future changes to the `files/tune.conf` file, implement the rolling update feature in the `playbook.yml` playbook and set the rolling update batch size to two hosts.

   1. Add the `serial` parameter to the `playbook.yml` playbook.

      ```
      ---
      - name: Install and configure web service
        hosts: server*.lab.example.com
        serial: 2
      ...output omitted...
      ```

   

6. Verify the changes to the `playbook.yml` playbook were correctly made and then execute the playbook.

   1. Verify that the `playbook.yml` playbook contains the following contents.

      ```
      ---
      - name: Install and configure web service
        hosts: server*.lab.example.com
        serial: 2
      
        tasks:
          - name: Import the web_tasks.yml task file
            import_tasks: tasks/web_tasks.yml
      
          - name: Import the firewall_tasks.yml task file
            import_tasks: tasks/firewall_tasks.yml
      
        handlers:
          - name: restart httpd
            service:
              name: httpd
              state: restarted
      ```

   2. Execute the playbook with **ansible-playbook --syntax-check** to verify the playbook contains no syntax errors. If errors are present, correct them before preceding.

      ```
      [student@workstation projects-review]$ ansible-playbook playbook.yml \
      > --syntax-check
      playbook: playbook.yml
      ```

   3. Execute the playbook. The playbook should execute against the host as a rolling update with a batch size of two managed hosts.

      ```
      [student@workstation projects-review]$ ansible-playbook playbook.yml
      
      PLAY [Install and configure web service] ***********************************
      
      TASK [Gathering Facts] *****************************************************
      ok: [serverd.lab.example.com]
      ok: [serverb.lab.example.com]
      
      TASK [Install httpd] *******************************************************
      changed: [serverd.lab.example.com]
      changed: [serverb.lab.example.com]
      
      TASK [Enable and start httpd] **********************************************
      changed: [serverd.lab.example.com]
      changed: [serverb.lab.example.com]
      
      TASK [Tuning configuration installed] **************************************
      changed: [serverb.lab.example.com]
      changed: [serverd.lab.example.com]
      
      TASK [Install firewalld] ***************************************************
      ok: [serverb.lab.example.com]
      ok: [serverd.lab.example.com]
      
      TASK [Enable and start firewalld] ******************************************
      ok: [serverd.lab.example.com]
      ok: [serverb.lab.example.com]
      
      TASK [Open the port for http] **********************************************
      changed: [serverd.lab.example.com]
      changed: [serverb.lab.example.com]
      
      RUNNING HANDLER [restart httpd] ********************************************
      changed: [serverb.lab.example.com]
      changed: [serverd.lab.example.com]
      
      PLAY [Install and configure web service] ***********************************
      
      TASK [Gathering Facts] *****************************************************
      ok: [serverc.lab.example.com]
      ok: [servera.lab.example.com]
      
      TASK [Install httpd] *******************************************************
      changed: [serverc.lab.example.com]
      changed: [servera.lab.example.com]
      
      TASK [Enable and start httpd] **********************************************
      changed: [serverc.lab.example.com]
      changed: [servera.lab.example.com]
      
      TASK [Tuning configuration installed] **************************************
      changed: [servera.lab.example.com]
      changed: [serverc.lab.example.com]
      
      TASK [Install firewalld] ***************************************************
      ok: [servera.lab.example.com]
      ok: [serverc.lab.example.com]
      
      TASK [Enable and start firewalld] ******************************************
      ok: [servera.lab.example.com]
      ok: [serverc.lab.example.com]
      
      TASK [Open the port for http] **********************************************
      changed: [servera.lab.example.com]
      changed: [serverc.lab.example.com]
      
      RUNNING HANDLER [restart httpd] ********************************************
      changed: [serverc.lab.example.com]
      changed: [servera.lab.example.com]
      
      PLAY RECAP *****************************************************************
      servera.lab.example.com    : ok=8    changed=2    unreachable=0    failed=0
      serverb.lab.example.com    : ok=8    changed=5    unreachable=0    failed=0
      serverc.lab.example.com    : ok=8    changed=5    unreachable=0    failed=0
      serverd.lab.example.com    : ok=8    changed=5    unreachable=0    failed=0
      ```

   

**Evaluation**

Run the **lab projects-review grade** command from `workstation` to confirm success on this exercise. Correct any reported failures and rerun the script until successful.

```
[student@workstation ~]$ lab projects-review grade
```

**Finish**

On `workstation`, run the **lab projects-review finish** script to clean up the resources created in this lab.

```
[student@workstation ~]$ lab projects-review finish
```

This concludes the lab.



________

###  Summary

In this chapter, you learned:

- Host patterns are used to specify the managed hosts to be targeted by plays or ad hoc commands.
- Dynamic inventory scripts can be used to generate dynamic lists of managed hosts from directory services or other sources external to Ansible.
- The `forks` parameter in the Ansible configuration file sets the maximum number of parallel connections to managed hosts.
- The `serial` parameter can be used to implement rolling updates across managed hosts by defining the number of managed hosts in each rolling update batch.
- You can use the `import_playbook` feature to incorporate external play files into playbooks.
- You can use the `include_tasks` or `import_tasks` features to incorporate external task files into playbooks.



