# Red Hat Certified System Administrator 8

## Understand and Use Essential Tools

* **Input/Output Redirection** 

  Output Redirection: The most common use of output redirection is collect the output of a command into a file. For example:

  ```
  ls -al > listing
  ```

  You can also *append* data to a file, for example if you want to add a line to the file message, you can do:

  ```
  echo "The last line" >> /var/log/message
  ```

  Input Redirection:  For example, you feed the command `sort` with the file `listing`

  ```
  sort < listing
  ```

  Both redirection: Redirect the file `listing` to feed the command `sort` and generate a new file `sorted_list` with the output

  ```
  sort < listing > sorted_list
  ```

  Error Redirection: you can change how error are displayed as well

  ```
  myscript.sh 2> errors
  ```

  The 2 in the redirect specifies to redirect stderr rather than just stdout. With this command, normal output will still be displayed on the screen but any errors will be written to a file named `errors`. If you would like, you can combine both stdout and stderr redirection to write all output (and errors) to a file

  ```
  myscript.sh > allout.txt 2>&1
  ```

  

* **Using grep and Regular Expressions**

  Using grep as part of a pipe:

  ```
  find . -name *.txt | grep taxes
  ```

  -i: insensitive
  -r: recursive
  -v: reversing
  -w: Only the specified word, not the pattern match

  Grep regular expressions:

  * **^** Match expression at the start of a line `^A`

  * **$** Match expression at the end of a line `A$`

  * \\ Turn off the special meaning of the next character `\^`

    For example `grep '^\*' example` print the match `*test` in a file

  * Match any one of the enclosed characters, as in `[aeiou]` and use a hyphen for a range, as in `[0-9]`
    For example `grep '^[%*]' example` print the match `%test` and `*test`

    Other example `grep '^[a-zA-Z]' example` print all that beginning with a letter

  * **^** Match any one character except those enclosed in `[ ]`, as in `[^0-9]`

    Example `grep '^[^a-zA-Z]' example` print all that NOT begging with a letter

  * **.** Match a single character of any value, except end of line
    `grep '^.i' example` match all words with the first letter is any and second is "i" `pip`, `file`

  * \* Match zero or more of the preceding character or expression

  * **\\{x,y\\}** Match x to y occurrences of the preceding

    e.g `grep '^%\{1,4\}' example` print all matching start with a % from 1 to 4 occurrences, if a word start with %%%%% don't match

  * **\\{x\\}** Match exactly x occurrences of the preceding

    e.g `grep '^1\{4\}' example` print all matching start with four `1`, if you word have five `1` don't match 
    e.g `grep 'i\{2\}' example` print two `i` in a row like `aiio`, `aaaii`

    e.g `grep 'i\{2,\}' example` print two `i` in a row to any number like `aiio`, `aaaiii`

    

    Other examples:

    Show lines containing only `linux`

    ```
    echo linux ; echo linuxTest | grep '^linux$' 
    ```

    Show lines starting with `^s` \ escapes the `^`

    ```
    echo ^s | grep '\^s'
    ```

    Print for either `Linux` or `linux`

    ```
    echo linux ; echo Linux | grep '[lL]inux'
    ```

    Print for BOB, Bob, BOb or BoB

    ```
    ➜  ~ cat bob.txt | grep 'B[oO][Bb]' 
    BOB
    Bob
    BOb
    BoB
    ```

    Print blank lines

    ```
    grep '^$' file
    ```

    Search for pairs of numerics digits

    ```
    grep --color '[0-9][0-9]' pairnumbers.txt
    ```

    Remove all comments and blank lines

    ```
    grep -v -e '^ *#' -e '^$' testfile.txt
    ```

  

* **Accessing Remote Systems with ssh**
  Importants files:

  * id_rsa: private key
  * id_rsa.pub: public key
  * know_hosts: storage of the server fingerprints

  X11 Forwarding `ssh -x user@server`

  

* **Logging in and Switching Users**

  A user environment is a collection of directories, files and setting that set up how things look whenn a user logs in, what commands are run, and what that user access to. By default the `su` command retains the environment of the user who ran the command. So if user Lucy has a different `$PATH` just typing `su lucy` won't set your path up the way hers is. For that to happen you need to use the `-` argument to `su`. So your command would be `su - lucy` if you wanted to inherit Lucy's `$PATH`

  

  Single Command with `su -c`: If all you want to do is run a single command as the substitute user, you can run `su -c fdisk`. In this instance you don't really need to passs the `-` argument because your intent is not to get a shell, just to run a single command

  

* **Compressing and Decompressing Files**

Nothing

* **Creating and Manipulating Files**

Common `find` flags: 
f: file | d:directory | l:link 

```
find . -type f -name httpd.conf
find . -type -d -name html
find . -type -l -name redhat-release
```



* **Linking Files**

Nothing

* **File Permissions**

  SUID and SGID

  ```
  ➜  ~ ls -l /usr/bin/passwd 
  -rwSr-xr-x 1 root root 63944 Jul 16 13:48 /usr/bin/passwd
  ```

  That means that the SUID bit has been set, this is anyone runs that file, its actually running that command/binary with the user owner of the file.
  SUID dont work on script (anything starting with !#/bin/bash for example) for security and technical issues. 

  For SGID is similar but with groups for example `--X--S--X`

  

  Sticky Bit: If you come across a directory that has permission like `sewxrwxrwt` that means the Sticky Bit is set. Files can be written in this directory by anyone, but only the owner can remove their files.

  ```
  ➜  ~ ls -ld /tmp
  drwxrwxrwt 31 root root 20480 Oct 31 00:00 /tmp
  ```

* **File Permissions: umask**

  Default permissions are fine, but it would be tedious to change them for every new file created. We can hide a permission from the default view by setting mask. So if we set a mask of 222 we get the following: 
  666 (default)
  222 (mask)
  444 (result)

  *Umask is not persistent*

  View actual umask

  ```
  ➜  ~ umask
  022
  ```

  If you want to change to make umask persistent for all user you need to edit /etc/profile

* **Using System Documentation**

To see a list of all man pages with `nfs` in the title you can use `whatis nfs`

After of that you can use `man`

```
➜  ~ whatis nfs
nfs (5)              - fstab format and options for the nfs file systems
➜  ~ man 5 nfs
```

Or you can search descriptions of man pages using `apropos nfs` to get a clue of what man page to look. 
This show a lot of man pages (every mention of the word)

## Operate Running Systems

## Configure Local Storage

## Create and Configure File Systems

## Deploy, Configure, and Maintain Systems

## Manage Users and Groups

## Manage Security

## RHCSA 8 Updates

