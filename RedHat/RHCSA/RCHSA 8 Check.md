

#### Chapter 3. Managing Local Users and Groups



##### Managing Local Group Accounts | SKIP

* Use the **usermod -aG** command to add a user to a supplementary group.
  (The use of the `-a` option makes **usermod** function in *append* mode. Without `-a`, the user will be removed from any of their current supplementary groups that are not included in the `-G` option's list.)

```
sudo usermod -aG group01 user03
```

* Use the **usermod -g** command to change a user's primary group.

```
[user01@host ~]$ sudo usermod -g group01 user02
```

* The **groupadd** command creates groups. The `-g` option specifies a particular GID for the group to use. Without options the **groupadd** command uses the next available GID from the range specified in the `/etc/login.defs` file while creating the groups. The `-r` option creates a system group using a GID from the range of valid system GIDs listed in the `/etc/login.defs`

```
[user01@host ~]$ sudo groupadd -g 10000 group01
```

* The **groupmod** command changes the properties of an existing group. The `-n` option specifies a new name for the group.

```
[user01@host ~]$ sudo groupmod -n group0022 group02
```

The **-g** option specifies a new GID.

```
[user01@host ~]$ sudo groupmod -g 20000 group0022
```



##### Managing User Passwords | SKIP

The **chage -d 0 user03** command forces the `user03` user to update its password on the next login.

The **chage -E 2019-08-05 user03** command causes the `user03` user's account to expire on 2019-08-05 (in YYYY-MM-DD format).

* The **date** command can be used to calculate a date in the future. Determine a date 180 days in the future. Use the format `%F` with the **date** command to get the exact value.

```
[student@servera ~]$ date -d "+180 days" +%F
```

* The **usermod** command can lock an account with the `-L` option.

```
[user01@host ~]$ sudo usermod -L user03
```

If a user leaves the company, the administrator may lock and expire an account with a single **usermod** command. The date must be given as the number of days since 1970-01-01, or in the *YYYY-MM-DD* format.

```
[user01@host ~]$ sudo usermod -L -e 2019-10-05 user03
```

* Set the passwords to expire 180 days from the current date for all users. Use administrative rights to edit the configuration file.

1. Set `PASS_MAX_DAYS` to `180` in `/etc/login.defs`

* `/etc/skel` directory with default home files when you create a new user
* `/etc/default/useradd` contain the default user parameters when you create a new user (shell, home, group)

##### ACL

There are two types of ACLs: *access ACLs* and *default ACLs*. An access ACL is the access control list for a specific file or directory. A default ACL can only be associated with a directory; if a file within the directory does not have an access ACL, it uses the rules of the default ACL for the directory. Default ACLs are optional.

The `getfacl` command is used to read FS ACLs.

The `setfacl` command is used to modify or set ACLs on a file or directory.

examples:

`setfacl -m g:avengers:rwx devteam` permission rwx to the group avengers on directory devteam

`setfacl -m u:tony:rw info.log` permission rw to the user tony on file info.log

`setfacl -x g:avengers devteam` remove the permissions

`setfacl -b devteam` remove the acl

To set a default ACL, add `d:` before the rule and specify a directory instead of a file name.

For example, to set the default ACL for the `/share/` directory to read and execute for users not in the user group (an access ACL for an individual file can override it):

```
# setfacl -m d:o:rx /share
```



#### Chapter 4. Controlling Access to Files

##### Managing Default Permissions and File Access

**Table 4.1. Effects of Special Permissions on Files and Directories**

| Special permission | Effect on files                                              | Effect on directories                                        |
| :----------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| `u+s` (suid)       | File executes as the user that owns the file, not the user that ran the file. | No effect.                                                   |
| `g+s` (sgid)       | File executes as the group that owns the file.               | Files newly created in the directory have their group owner set to match the group owner of the directory. |
| `o+t` (sticky)     | No effect.                                                   | Users with write access to the directory can only remove files that they own; they cannot remove or force saves to files owned by other users. |

- Symbolically: setuid = `u+s`; setgid = `g+s`; sticky = `o+t`
- Numerically (fourth preceding digit): setuid = 4; setgid = 2; sticky = 1

**Examples**

- Add the setgid bit on `directory`:

  ```
  [user@host ~]# chmod g+s directory
  ```

- Set the setgid bit and add read/write/execute permissions for user and group, with no access for others, on `directory`:

  ```
  [user@host ~]# chmod 2770 directory
  ```

* **Default File Permissions**

  If you create a new directory, the operating system starts by assigning it octal permissions 0777 (`drwxrwxrwx`). If you create a new regular file, the operating system assignes it octal permissions 0666 (`-rw-rw-rw-`). You always have to explicitly add execute permission to a regular file. This makes it harder for an attacker to compromise a network service so that it creates a new file and immediately executes it as a program.

  Use the **umask** command with a single numeric argument to change the umask of the current shell. The numeric argument should be an octal value corresponding to the new umask value. You can omit any leading zeros in the umask.

  The system's default umask values for Bash shell users are defined in the `/etc/profile` and `/etc/bashrc` files. Users can override the system defaults in the `.bash_profile` and `.bashrc` files in their home directories.

  

#### Chapter 5. Managing SELinux Security



##### **Controlling SELinux File Contexts** | SKIP

- Manage the SELinux policy rules that determine the default context for files and directories using the **semanage fcontext** command.

  ```
  semanage fcontext -a -t httpd_sys_content_t '/virtual(/.*)?'
  ```

- Apply the context defined by the SELinux policy to files and directories using the **restorecon** command. Most used ` restorecon -Rv`

- The type context for files and directories normally found in `/var/www/html` is `httpd_sys_content_t`. The contexts for files and directories normally found in `/tmp` and `/var/tmp` is `tmp_t`

- The most common extended regular expression used in `fcontext` rules is `(/.*)?`, which means “optionally, match a / followed by any number of characters”. It matches the directory listed before the expression and everything in that directory recursively.

- To ensure that you have the tools to manage SELinux contexts, install the `policycoreutil` package and the `policycoreutil-python` package if needed. These contain the **restorecon** command and **semanage** command, respectively.

Normally for add httpd context to a directory:

```
semanage fcontext -a -t httpd_sys_content_t '/custom(/.*)?'
restorecon -Rv /custom
```

To troubleshoot SELinux, you can put it temporarily in permissive mode by using **setenforce 0**

The semanage command is not the easiest command to remember. Fortunately, it has some excellent man pages. Type man semanage and use G to go all the way down to the bottom of the man page. You’ll now see the “See Also” section, which mentions semanage-fcontext, which is about managing file context with semanage. Open this man page using man semanage-fcontext, type /examples



##### Adjusting SELinux Policy with Booleans

SELinux booleans are switches that change the behavior of the SELinux policy. SELinux booleans are rules that can be enabled or disabled. They can be used by security administrators to tune the policy to make selective adjustments.

The SELinux man pages, provided with the *selinux-policy-doc* package, describe the purpose of the available booleans. The **man -k '_selinux'** command lists these man pages.

Commands useful for managing SELinux booleans include **getsebool -a**, which lists booleans and their state, and **setsebool** which modifies booleans. **setsebool -P** modifies the SELinux policy to make the modification persistent. And **semanage boolean -l** reports on whether or not a boolean is persistent, along with a short description of the boolean.

To list booleans in which the current state differs from the default state, run **semanage boolean -l -C**.

* To enable the Apache feature that permits users to publish web content from their home directories, you must edit the `/etc/httpd/conf.d/userdir.conf` configuration file. Comment out the line that sets `UserDir` to `disabled` and uncomment the line that sets `UserDir` to `public_html`. **http://servera/~student/index.html**



##### Investigating and Resolving SELinux Issues

* The most common SELinux issue is an incorrect file context. This can occur when a file is created in a location with one file context and moved into a place where a different context is expected. In most cases, running **restorecon** will correct the issue. 
* Another remedy for overly restrictive access could be the adjustment of a Boolean. For example, the `ftpd_anon_write` boolean controls whether anonymous FTP users can upload files. You must turn this boolean on to permit anonymous FTP users to upload files to a server. Adjusting booleans requires more care because they can have a broad impact on system security.
* First grep `sealert` in  `/var/log/messages ` or`/var/log/audit/audit.log` copy the id problem an execute `sealert -l $ID`, this show a possible fix. Look in the *Raw Audit Messages* of the `sealert` reveals the target file that is the problem. To search the /var/log/audit.log file use the ausearch command, the -m searches on the message type, the -ts searches based on time `ausearch -m AVC -ts recent`
* touch `./autorelabel` if you want to back to the first selinux files state. 
* install `setroubleshoot-server` package if you don't have sealert command



#### Chapter 6. Tuning System Performance

##### Adjusting Tuning Profiles | SKIP

|          Command           |               Description                |
| :------------------------: | :--------------------------------------: |
|      tuned-adm active      | View the currently active tuning profile |
|       tuned-adm list       |    List all availabe tunning profiles    |
| tuned-adm profile $PROFILE |       Switch to different profile        |
|    tuned-adm recommend     |        Recommend a tuning profile        |



#### Chapter 7. Installing and Updating Software Packages



##### Installing and Updating Software Packages with Yum | SKIP

| Task                                          | Command                         |
| :-------------------------------------------: | :-----------------------------: |
| List installed and available packages by name | yum list [NAME-PATTERN]     |
| List installed and available groups           | yum group list              |
| Search for a package by keyword               | yum search KEYWORD          |
| Show details of a package                     | yum info PACKAGENAME        |
| Show all added repositories | yum repolist all |
| Install a package                             | yum install PACKAGENAME     |
| Install a package group                       | yum group install GROUPNAME |
| Update all packages                           | yum update                  |
| Remove a package                              | yum remove PACKAGENAME      |
| Display transaction history                   | yum history                 |



##### Enabling Yum Software Repositories

|              Task               |                  Command                   |
| :-----------------------------: | :----------------------------------------: |
| View all available repositories |              yum repolist all              |
| Enable or disable repositories  | **yum-config-manager --enable REPOSITORY** |
|           Import Key            |            rpm --import URL_KEY            |

Create Yum repositories in /etc/yum.repos.d

```
[user@host ~]$ yum-config-manager --add-repo="http://dl.fedoraproject.org/pub/epel/8/x86_64/"
Loaded plugins: langpacks
adding repo from: http://dl.fedoraproject.org/pub/epel/8/x86_64/

[dl.fedoraproject.org_pub_epel_8_x86_64_]
name=added from: http://dl.fedoraproject.org/pub/epel/8/x86_64/
baseurl=http://dl.fedoraproject.org/pub/epel/8/x86_64/
enabled=1
```

We can add GPG key and location, for example: 

```
[EPEL]
name=EPEL 8
baseurl=http://dl.fedoraproject.org/pub/epel/8/x86_64/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
```

(!) man yum.conf for view the syntax

##### Managing Package Module Streams

The AppStream (aka Modules) It allows admins to install different versions of popular software, in addition to different profiles for different packages (for example, a minimal `httpd` server). 

**Modules**

A module is a set of RPM packages that are a consistent set that belong together. Typically, this is organized around a specific version of a software application or programming language. A typical module can contain packages with an application, packages with the application’s specific dependency libraries, packages with documentation for the application, and packages with helper utilities.

**Module Streams**

Each module can have one or more module streams, which hold different versions of the content. Each of the streams receives updates independently. Think of the module stream as a virtual repository in the Application Stream physical repository.

For each module, only one of its streams can be enabled and provide its packages.

**Module Profiles**

Each module can have one or more profiles. A profile is a list of certain packages to be installed together for a particular use-case such as for a server, client, development, minimal install, or other.

|                             Task                             |                      Command                      |
| :----------------------------------------------------------: | :-----------------------------------------------: |
|                  List of available modules                   |                  yum module list                  |
|        List the module streams for a specific module         |               yum module list perl                |
|                     Details of a module                      |               yum module info perl                |
|    Install a module using the default stream and profiles    |              yum module install perl              |
|  Install a module using specified stream, in this case `10`  | yum module install postgresql:10 or httpd/minimal |
| The **@** notation informs **yum** that the argument is a module (Same Before) |                 yum install @perl                 |
| Remove an installed module, also removes all packages installed from this module. |              yum module remove perl               |
|              Reset the module and its streams.               |               yum module reset perl               |
|                  Disable the module stream                   |              yum module disable perl              |

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled

Use the **yum module install** command to install the *python36* module. Use the `name:stream/profile` syntax to install the *python36* module from the `3.6` stream and the `common` profile.

```
yum module install python36:3.6/common
```

Install *apache HTTP Server* module from the `2.4` stream and the `common` profile.

```
yum module install httpd:2.4/common
```



#### Chapter 8. Managing Basic Storage



##### Adding Partitions, File Systems, and Persistent Mounts

| Task                                                         | Command                                           |
| ------------------------------------------------------------ | ------------------------------------------------- |
| The following command to write an MBR disk label to a disk   | parted /dev/vdb mklabel msdos                     |
| To write a GPT disk label                                    | parted /dev/vdb mklabel gpt                       |
| Use the **mkpart** subcommand to create a new primary or extended partition | parted /dev/vdb mkpart                            |
| This does not create the file system on the partition; it is only an indication of the partition type | parted /dev/vdb mkpart xfs                        |
| To get the list of the supported file-system types           | parted /dev/vdb help mkpart                       |
| Creating MBR Partitions                                      | parted /dev/vdb mkpart primary xfs 2048s 1000MB   |
| Creating GPG Partitions (userdata is the name of the partition) | parted /dev/vdb mkpart usersdata xfs 2048s 1000MB |
| This command waits for the system to detect the new partition | udevadm settle                                    |

The **mklabel** subcommand wipes the existing partition table. Only use **mklabel** when the intent is to reuse the disk without regard to the existing data. If a new label changes the partition boundaries, all data in existing file systems will become inaccessible.

* For situations where you need more than four partitions on an MBR-partitioned disk, create three primary partitions and one extended partition. This extended partition serves as a container within which you can create multiple logical partitions.

* Specify the sector on the disk that the new partition starts on.

  ```
  Start? 2048s
  ```

  Notice the `s` suffix to provide the value in sectors. You can also use the `MiB`, `GiB`, `TiB`, `MB`, `GB`, or `TB` suffixes. If you do not provide a suffix, `MB` is the default. **parted** may round the value you provide to satisfy disk constraints. With most disks, a start sector that is a multiple of 2048 is a safe assumption.

  Specify the disk sector where the new partition should end.

  ```
  End? 1000MB
  ```

  With **parted**, you cannot directly provide the size of your partition, but you can quickly compute it with the following formula:

  ```
  Size = End - Start
  ```

* Use the **mkpart** subcommand to start creating the new partition.

  With the GPT scheme, each partition is given a name.

  ```
  (parted) mkpart
  Partition name?  []? usersdata
  ```

* Creating FS: use the **mkfs.xfs** command to apply an XFS file system to a block device. For ext4, use **mkfs.ext4**.

* /etc/fstab example

  ```
  UUID=7a20315d-ed8b-4e75-a5b6-24ff9e1f9838   /dbdata  xfs   defaults   0 0
  systemctl daemon-reload
  ```

you can use the **findmnt --verify** command to control the `/etc/fstab` file.



**Create a 2GB GPT Partition on `/dev/xvdf`**

We need to use `gdisk /dev/xvdf` to create our partition.

- Enter `n` to create a new partition.
- Accept the default for the partition number.
- Accept the default for the starting sector.
- For the ending sector we type `+2G` to create a 2 Gigabyte partition.
- Accept the default partition type.
- Enter `w` to write the partition information

Once all that is complete, run `partprobe` to finalize these settings.



##### Managing Swap Space | SKIP

Creating a Swap Partition

```
parted /dev/sdb mkpart swap1 linux-swap 1001MB 1257MB
mkswap /dev/sdb2
fstab -> UUID=39e2667a-9458-42fe-9665-c5c854605881   swap   swap   defaults   0 0
fstab -> UUID=fbd7fa60-b781-44a8-961b-37ac3ef572bf   swap   swap   pri=10    0 0
systemctl daemon-reload
swapon -a && swapon --show
```



#### Chapter 9. Controlling Services and the Boot Process



##### Controlling System Services | SKIP

**Table 9.3. Useful Service Management Commands**

|                             Task                             |               Command                |
| :----------------------------------------------------------: | :----------------------------------: |
|        View detailed information about a unit state.         |      **systemctl status UNIT**       |
|             Stop a service on a running system.              |       **systemctl stop UNIT**        |
|             Start a service on a running system.             |       **systemctl start UNIT**       |
|            Restart a service on a running system.            |      **systemctl restart UNIT**      |
|     Reload the configuration file of a running service.      |      **systemctl reload UNIT**       |
| Completely disable a service from being started, both manually and at boot. |       **systemctl mask UNIT**        |
|               Make a masked service available.               |      **systemctl unmask UNIT**       |
|          Configure a service to start at boot time.          |      **systemctl enable UNIT**       |
|        Disable a service from starting at boot time.         |      **systemctl disable UNIT**      |
|    List units required and wanted by the specified unit.     | **systemctl list-dependencies UNIT** |



##### Selecting the Boot Target

|       Target        |                           Purpose                            |
| :-----------------: | :----------------------------------------------------------: |
| `graphical.target`  | System supports multiple users, graphical- and text-based logins. |
| `multi-user.target` |   System supports multiple users, text-based logins only.    |
|   `rescue.target`   |  **sulogin** prompt, basic system initialization completed.  |
| `emergency.target`  | **sulogin** prompt, `initramfs` pivot complete, and system root mounted on `/` read only (THIS FOR TROUBLESHOTTING) |

| Task                                       | Command                                  |
| ------------------------------------------ | ---------------------------------------- |
| List the available targets                 | systemctl list-units --type=target --all |
| Switch automatically to a different target | systemctl isolate multi-user.target      |
| Get default boot target                    | systemctl get-default                    |
| Change the boot target                     | systemctl set-default graphical.target   |

For example, to boot the system into a rescue shell where you can change the system configuration with almost no services running, append the following option to the kernel command line from the boot loader.

Append `systemd.unit=*target*.target`. For example, `systemd.unit=emergency.target`.



##### Resetting the Root Password | SKIP

```
Append rd.break in grub
mount -o remount,rw /sysroot
chroot /sysroot
passwd root
touch /.autorelabel
```



#### Chapter 10. Managing Networking

(!) Remember the nmcli-examples man page

(!) Run **nmcli con up / nmcli con reload**  after manually changing the contents of the /etc/sysconfig/ifcfg files.

(!) REMEMBER `nmtui` command

(!) Notice that while adding a network connection you use **ip4**, but while modifying parameters for an existing connection, you often use **ipv4** instead. This is not a typo; it is just how it works.

##### Validating Network Configuration

| Task                                                         | Command                      |
| ------------------------------------------------------------ | ---------------------------- |
| List all network interfaces available on your system         | ip link show                 |
| View device and address information (ipv4/6)                 | ip addr show ens3            |
| Show statistics about network performance                    | ip -s link show ens3         |
| The **ping6** command is the IPv6 version of ping            | ping6                        |
| Show the IPv6 routing table                                  | ip -6 route                  |
| To trace the path that network traffic takes to reach a remote host through multiple routers (is like traceroute, but traceroute command is not installed by default) | tracepath URL                |
| The **tracepath6** and **traceroute -6** commands are the equivalent to **tracepath** and **traceroute** for IPv6. | tracepath6 2001:db8:0:2::451 |
| The **ss** command is meant to replace the older tool **netstat**, For **ss**, both IPv4 and IPv6 connections are displayed. | ss -ta                       |



##### Configuring Networking from the Command Line

| Command                         | Purpose                                                      |
| :------------------------------ | :----------------------------------------------------------- |
| **nmcli dev status**            | Show the NetworkManager status of all network interfaces.    |
| **nmcli con show**              | List all connections.                                        |
| **nmcli con show name**         | List the current settings for the connection *name*.         |
| **nmcli con add con-name name** | Add a new connection named *name*.                           |
| **nmcli con mod name**          | Modify the connection *name*.                                |
| **nmcli con reload**            | Reload the configuration files (useful after they have been edited by hand). |
| **nmcli con up name**           | Activate the connection *name*.                              |
| **nmcli dev dis dev**           | Deactivate and disconnect the current connection on the network interface *dev*. |
| **nmcli con del name**          | Delete the connection *name* and its configuration file.     |

To set the IPv6 address to `2001:db8:0:1::a00:1/64` and default gateway to `2001:db8:0:1::1` for the connection `static-ens3`:

```
[root@host ~]# nmcli con mod static-ens3 ipv6.address 2001:db8:0:1::a00:1/64 \
ipv6.gateway 2001:db8:0:1::1
```

This final example creates an `eno2` connection for the `eno2` device with static IPv6 and IPv4 addresses, using the IPv6 address and network prefix `2001:db8:0:1::c000:207/64` and default IPv6 gateway `2001:db8:0:1::1`, and the IPv4 address and network prefix `192.0.2.7/24` and default IPv4 gateway `192.0.2.1`, but still autoconnects at startup and saves its configuration into `/etc/sysconfig/network-scripts/ifcfg-eno2`. Due to screen size limitations, terminate the first line with a shell `\` escape and complete the command on the next line.

```
nmcli con add con-name eno2 type ethernet ifname eno2 \
ipv6.address 2001:db8:0:1::c000:207/64 ipv6.gateway 2001:db8:0:1::1 \
ipv4.address 192.0.2.7/24 ipv4.gateway 192.0.2.1
```

Create a static connection:

```
nmcli con add con-name "static-addr" ifname enX \
type ethernet ipv4.method manual \
ipv4.address 172.25.250.10/24 ipv4.gateway 172.25.250.254
Other:
nmcli con add con-name "static" ifname eth0 autoconnect no type ethernet ip4 10.0.0.10/24 gw4 10.0.0.1
```

 When the connection is added, you use **ip4** and **gw4** (without a *v*).

**nmcli con mod "static" ipv4.addresses "10.0.0.20/24" 10.0.0.100** changes the current IP address and default gateway on your network connection.

Create a DHCP connection:

```
nmcli con add con-name dhcp type ethernet ifname ens33 ipv4.method auto
```

Add DNS settings:

```
nmcli con mod "static-addr" ipv4.dns 172.25.250.254
nmcli con mod "static-addr" +ipv4.dns 8.8.8.8
systemctl restart network
```

Disable the original connection from autostarting at boot.

```
nmcli con mod "Wired connection 1" connection.autoconnect no
```

Add new IP:

```
nmcli con mod "lab" +ipv4.addresses 10.0.1.1/24
```



##### Editing Network Configuration Files

By default, changes made with **nmcli con mod name** are automatically saved to `/etc/sysconfig/network-scripts/ifcfg-*name*`. That file can also be manually edited with a text editor. After doing so, run **nmcli con reload** so that NetworkManager reads the configuration changes.

**Table 10.2. Comparison of nm-settings and ifcfg-\* Directives**

| **nmcli con mod**                             | `ifcfg-* file`                                          | Effect                                                       |
| :-------------------------------------------- | :------------------------------------------------------ | :----------------------------------------------------------- |
| `ipv4.method manual`                          | `BOOTPROTO=none`                                        | IPv4 addresses configured statically.                        |
| `ipv4.method auto`                            | `BOOTPROTO=dhcp`                                        | Looks for configuration settings from a DHCPv4 server. If static addresses are also set, will not bring those up until we have information from DHCPv4. |
| `ipv4.addresses "192.0.2.1/24 192.0.2.254"`   | `IPADDR0=192.0.2.1` `PREFIX0=24` `GATEWAY0=192.0.2.254` | Sets static IPv4 address, network prefix, and default gateway. If more than one is set for the connection, then instead of 0, the `ifcfg-*` directives end with 1, 2, 3 and so on. |
| `ipv4.dns 8.8.8.8`                            | `DNS0=8.8.8.8`                                          | Modify `/etc/resolv.conf` to use this `nameserver`.          |
| `ipv4.dns-search example.com`                 | `DOMAIN=example.com`                                    | Modify `/etc/resolv.conf` to use this domain in the `search` directive. |
| `ipv4.ignore-auto-dns true`                   | `PEERDNS=no`                                            | Ignore DNS server information from the DHCP server.          |
| `ipv6.method manual`                          | `IPV6_AUTOCONF=no`                                      | IPv6 addresses configured statically.                        |
| `ipv6.method auto`                            | `IPV6_AUTOCONF=yes`                                     | Configures network settings using SLAAC from router advertisements. |
| `ipv6.method dhcp`                            | `IPV6_AUTOCONF=no` `DHCPV6C=yes`                        | Configures network settings by using DHCPv6, but not SLAAC.  |
| `ipv6.addresses "2001:db8::a/64 2001:db8::1"` | `IPV6ADDR=2001:db8::a/64` `IPV6_DEFAULTGW=2001:db8::1`  | Sets static IPv6 address, network prefix, and default gateway. If more than one address is set for the connection, `IPV6_SECONDARIES` takes a double-quoted list of space-delimited address/prefix definitions. |
| `ipv6.dns ...`                                | `DNS0= ...`                                             | Modify `/etc/resolv.conf` to use this nameserver. Exactly the same as IPv4. |
| `ipv6.dns-search example.com`                 | `DOMAIN=example.com`                                    | Modify `/etc/resolv.conf` to use this domain in the `search` directive. Exactly the same as IPv4. |
| `ipv6.ignore-auto-dns true`                   | `IPV6_PEERDNS=no`                                       | Ignore DNS server information from the DHCP server.          |
| `connection.autoconnect yes`                  | `ONBOOT=yes`                                            | Automatically activate this connection at boot.              |
| `connection.id ens3`                          | `NAME=ens3`                                             | The name of this connection.                                 |
| `connection.interface-name ens3`              | `DEVICE=ens3`                                           | The connection is bound to the network interface with this name. |
| `802-3-ethernet.mac-address . . .`            | `HWADDR= ...`                                           | The connection is bound to the network interface with this MAC address. |

**Table 10.3. IPv4 Configuration Options for ifcfg File**

|                           *Static*                           |    *Dynamic*     |                           *Either*                           |
| :----------------------------------------------------------: | :--------------: | :----------------------------------------------------------: |
| `BOOTPROTO=none`<br />`IPADDR0=172.25.250.10`<br />`PREFIX0=24`<br />`GATEWAY0=172.25.250.254`<br />`DEFROUTE=yes`<br />`DNS1=172.25.254.254` | `BOOTPROTO=dhcp` | `DEVICE=ens3`<br />`NAME="static-ens3"`<br />`ONBOOT=yes`<br />`UUID=f3e8(...)ad3e`<br />`USERCTL=yes` |

After modifying the configuration files, run **nmcli con reload** to make NetworkManager read the configuration changes. The interface still needs to be restarted for changes to take effect.

```
[root@host ~]# nmcli con reload
[root@host ~]# nmcli con down "static-ens3"
[root@host ~]# nmcli con up "static-ens3"
```



##### Configuring Host Names and Name Resolution | SKIP

```
hostnamectl set-hostname host@example.com
hostnamectl status
```

Remember **/etc/nsswitch.conf**

To add the DNS server with IPv6 IP address `2001:4860:4860::8888` to the list of nameservers to use with the connection `static-ens3`:

```
[root@host ~]# nmcli con mod static-ens3 +ipv6.dns 2001:4860:4860::8888
```

DHCP automatically rewrites the `/etc/resolv.conf` file as interfaces are started unless you specify `PEERDNS`=`no` in the relevant interface configuration files. Set this using the **nmcli** command.

```
[root@host ~]# nmcli con mod "static-ens3" ipv4.ignore-auto-dns yes
```

PERSISTENT CHANGE HOSTNAME -> `hostnamectl set-hostname host@example.com`
TMP CHANGE HOSTNAME -> `hostname testname`



#### Chapter 11. Analyzing and Storing Logs



##### Rsyslog

For example, the following line in /etc/rsyslog.conf would record messages sent to the `authpriv` facility at any priority to the file `/var/log/secure`:

```
authpriv.*                  /var/log/secure
```

Other example, create the `/etc/rsyslog.d/debug.conf` file with the necessary entries to redirect all log messages having the `debug` priority to `/var/log/messages-debug`. You may use the **vim /etc/rsyslog.d/debug.conf** command to create the file with the following content (The left side of each line indicates the facility and severity of the syslog)

(!)  man 5 rsyslog.conf

```
*.debug /var/log/messages-debug
```

Sending Syslog Messages Manually

```
logger -p local7.notice "Log entry created on host
```

```
logger -p notice hello
grep hello /var/log/messages
```



##### Storing the System Journal Permanently

```
mkdir /var/log/journal
echo "persistent|volatile|auto" >> /etc/systemd/journald.conf
systemctl restart systemd-journald
journalctl -b 1 (first system boot)
journalctl -b (current system boot)
journalctl --since 07:01:00 --until 07:31:00 (30 min period)
other example: jornalctl -u httpd
```

**Storage=persistent** The journal will be stored on disk in the directory /var/log/journal. This directory will be created automatically if it doesn’t exist. Or use "auto" but you need to create /var/log/journal directory. Type **chown root:systemd-journal /var/log/journal**, followed by **chmod 2755 /var/log/journal**.



##### Setting Local Clocks and Time Zones

If you need to know what timezone select in specified location

```
tzselect
```

Set date and time with TZ

```
timedatectl
timedatectl list-timezones
timedatectl set-timezone America/Phoenix
timedatectl set-time 9:00:00 (only with ntp off)
timedatectl set-ntp ON
```



##### Configuring and Monitoring Chronyd

```
echo "server classroom.example.com iburst" >> /etc/chrony.conf
systemctl restart chronyd
chronyc sources -v
```



#### Chapter 12. Implementing Advanced Storage Features



##### Creating Logical Volumes

```
yum -y install lmv2
parted -s /dev/vdb mkpart primary 1MiB 769MiB
parted -s /dev/vdb set 1 lvm on
udevadm settle
pvcreate /dev/vdb1
vgcreate vg01 /dev/vdb1
lvcreate -n lv01 -L 700M vg01
mkfs -t xfs /dev/vg01/lv01
```

- **lvcreate -L 128M**: Size the logical volume to exactly 128 MiB.
- **lvcreate -l 128** : Size the logical volume to exactly 128 extents. The total number of bytes depends on the size of the physical extent block on the underlying physical volume.

Exercise: Create a New Logical Volume (LV-A) with a Size of 30 Extents that Belongs to the Volume Group VG-A (with a PE Size of 32M). After Creating the Volume, Configure the Server to Mount It Persistently on `/mnt`.

Then we need to create the Volume Group named VG-A with a 32M physical extent size:

```
vgcreate VG-A /dev/vdc -s 32m
```

Finally, we create the Logical Volume named LV-A with 30 extents:

```
lvcreate -n LV-A -l 30 VG-A
```



##### Extending Logical Volumes

```
[root@host ~]# vgextend vg01 /dev/vdb3
```

**Reducing a Volume Group**

```
pvmove /dev/vdb3
```

This command moves the PEs from `/dev/vdb3` to other PVs with free PEs in the same VG. Alternatively, **pvremove** can be used to permanently stop using the device as a PV.

```
vgreduce vg01 /dev/vdb3
```

**Extend the logical volume**.

| Command                      | Results                                                      |
| :--------------------------- | :----------------------------------------------------------- |
| **lvextend -l 128**          | Resize the logical volume to *exactly* 128 extents in size.  |
| **lvextend -l +128**         | Add 128 extents to the current size of the logical volume.   |
| **lvextend -L 128M**         | Resize the logical volume to *exactly* 128 MiB.              |
| **lvextend -L +128M**        | Add 128 MiB to the current size of the logical volume.       |
| **lvextend -l +50%FREE**     | Add 50 percent of the current free space in the VG to the LV. |
| **lvextend -r -l +100%FREE** | The -r flag will resize the FS, if possible (don't use with VDO) |

**Extend the file system**.

xfs with mount point

```
xfs_growfs /mnt/data
```

ext4 with lv name

```
resize2fs /dev/vg01/lv01
```

swap (previously unmount)

```
mkswap /dev/vg01/lv02
```



##### Managing Layered Storage with Stratis

```
yum install stratis-cli stratisd
systemctl enable --now stratisd
stratis pool create pool1 /dev/vdb
stratis pool list
stratis pool add-data pool1 /dev/vdc
stratis blockdev list pool1
stratis filesystem create pool1 filesystem1
stratis filesystem list
fstab: UUID=31b9...8c55 /dir1 xfs defaults,x-systemd.requires=stratisd.service 0 0
stratis filesystem snapshot pool1 filesystem1 fs1-snap1
```

The `x-systemd.requires=stratisd.service` mount option delays mounting the file system until after `systemd` starts the `stratisd.service` during the boot process. 
NOTE: Failure to use the `x-systemd.requires=stratisd.service` mount option in `/etc/fstab` for the Stratis file system will result in the machine booting to `emergency.target` on the next reboot.



##### Compressing and Deduplicating Storage with VDO

```
yum install vdo kmod-kvdo
systemctl enable --now vdo
vdo create --name=vdo1 --device=/dev/vdd --vdoLogicalSize=50G
mkfs.xfs -K /dev/mapper/vdo1 
or -> mkfs.ext4 -E nodiscard /dev/mapper/vdo1
udevadm settle
mkdir /vdo && mount /dev/mapper/vdo1 /vdo
fstab: UUID=31b9...8c55 /dev/mapper/vdo xfs defaults,_netdev,x-systemd.device-timeout=0,x-systemd.requires=vdo.service 0 0
vdstats
vdo status --name=vdo1
vdo list
vdo start
```

Whatever fs we use we need to make sure that we don't discard blocks when making the FS.



**Extending Virtual Disks (LVM/VDO)**

After extend the LV do:

```
lvextend -l +100%FREE /dev/mapper/vdoDev-vdoLV 
vdo growPhysycal --name=vdo1
vdostats --human-readeable ; df -h | grep vdo
vdo growlogical --name=vdo1 --vdoLogicalSize=180G
xfs_growfs /mnt/vdo
df -h | grep vdo
```

You can't shrink the physical size of a VDO volume

#### Chapter 13. Scheduling Future Tasks

.---------------- minute (0 - 59)

|  .------------- hour (0 - 23)

|  |  .---------- day of month (1 - 31)

|  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...

|  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue ...

|  |  |  |  |

- - - - - user-name  command

crontab -e -u derek

The purpose of `/etc/anacrontab` is to make sure that important jobs always run. Check /var/spool/anacron



##### Cleaning Temporary Files with a Systemd Timer

To ensure that long-running systems do not fill up their disks with stale data, a `systemd` timer unit called `systemd-tmpfiles-clean.timer` triggers `systemd-tmpfiles-clean.service` on a regular interval, which executes the **systemd-tmpfiles --clean** command.

```
systemctl cat systemd-tmpfiles-clean.timer 
(/lib/systemd/system/systemd-tmpfiles-clean.timer)
systemctl daemon-reload
systemctl enable --now systemd-tmpfiles-clean.timer
```



**Cleaning Temporary Files Manually**

The format of the configuration files for **systemd-tmpfiles** is detailed in the **tmpfiles.d**(5) manual 



#### Chapter 14. Accessing Network-Attached Storage



##### Mounting NFS Shares

```
yum install nfs-utils
systemctl start rpcbind
showmount -e SERVER_IP
mount -t nfs -o rw,sync serverb:/share mountpoint
fstab -> serverb:/share  /mountpoint  nfs  rw,soft  0 0 
```



##### The **nfsconf** Tool

Use the **nfsconf** tool to get, set, or unset NFS configuration parameters. /etc/nfs.conf

```
nfsconf --set nfsd vers4.2 y
```

**Configure an NFSv4-only Client**

You can configure an NFSv4-only client by setting the following values on the `/etc/nfs.conf` configuration file.

Start by disabling UDP and other NFSv2 and NFSv3 related keys:

```
nfsconf --set nfsd udp n
nfsconf --set nfsd vers2 n
nfsconf --set nfsd vers3 n
```

Enable TCP, and NFSv4, related keys.

```
nfsconf --set nfsd tcp y
nfsconf --set nfsd vers4 y
nfsconf --set nfsd vers4.0 y
nfsconf --set nfsd vers4.1 y
nfsconf --set nfsd vers4.2 y
```



##### Automount

```
yum install autofs
vim /etc/auto.master.d/demo.autofs -> /shares  /etc/auto.demo
vim /etc/auto.demo -> work  -rw,sync  serverb:/shares/work
systemctl enable --now autofs
```

This entry uses the `/shares` directory as the base for indirect automounts. The `/etc/auto.demo` file contains the mount details. Use an absolute file name. The `auto.demo` file needs to be created before starting the `autofs` service.

Auto.master file:

```
auto.master -> /nfs /etc/auto.misc --timeout 60
```

The first field is the local mount point. The second field is the location of the map file on the local filesystems. the third field is optional, but can be used for different mount options, like a timeout.
The service creates the /misc directory. You don't need to create

Map file:

```
autonah -rw,soft,intr nfs.server.com:/nfsexport
```

The first field is the name of the local mount point, the second are mount options, the third is the location of the export. You can view the files with `ls /nfs/autonah`

**Direct maps**

```
/etc/auto.master -> /-  /etc/auto.direct
/etc/auto.direct -> /mnt/docs  -rw,sync  serverb:/shares/docs
```

In this example the `/mnt` directory exists, and it is not managed by `autofs`. The full directory `/mnt/docs` will be created and removed automatically by the `autofs` service.

**Indirect Wildcard Maps**

```
/etc/auto.demo -> *  -rw,sync  serverb:/shares/&
```

The mount point (or key) is an asterisk character (*), and the subdirectory on the source location is an ampersand character (&). Everything else in the entry is the same.

When a user attempts to access `/shares/work`, the key `*` (which is `work` in this example) replaces the ampersand in the source location and `serverb:/shares/work` is mounted. As with the indirect example, the `work` directory is created and removed automatically by `autofs`.



#### Chapter 15. Managing Network Security

```
yum install firewalld firewall-config
```

There are two config ares inside firewalld, runtime or permanent, runtime doesn't persist through reboots or firewalld restart/reload

##### **Pre-defined Zones**

Firewalld has pre-defined zones, each of which you can customize. By default, all zones permit any incoming traffic which is part of a communication initiated by the system, and all outgoing traffic. 

**Table 15.1. Default Configuration of Firewalld Zones**

| Zone name  | Default configuration                                        |
| :--------- | :----------------------------------------------------------- |
| `trusted`  | Allow all incoming traffic.                                  |
| `home`     | Reject incoming traffic unless related to outgoing traffic or matching the `ssh`, `mdns`, `ipp-client`, `samba-client`, or `dhcpv6-client` pre-defined services. |
| `internal` | Reject incoming traffic unless related to outgoing traffic or matching the `ssh`, `mdns`, `ipp-client`, `samba-client`, or `dhcpv6-client` pre-defined services (same as the `home` zone to start with). |
| `work`     | Reject incoming traffic unless related to outgoing traffic or matching the `ssh`, `ipp-client`, or `dhcpv6-client` pre-defined services. |
| `public`   | Reject incoming traffic unless related to outgoing traffic or matching the `ssh` or `dhcpv6-client` pre-defined services. *The default zone for newly added network interfaces.* |
| `external` | Reject incoming traffic unless related to outgoing traffic or matching the `ssh` pre-defined service. Outgoing IPv4 traffic forwarded through this zone is *masqueraded* to look like it originated from the IPv4 address of the outgoing network interface. |
| `dmz`      | Reject incoming traffic unless related to outgoing traffic or matching the `ssh` pre-defined service. |
| `block`    | Reject all incoming traffic unless related to outgoing traffic. |
| `drop`     | Drop all incoming traffic unless related to outgoing traffic (do not even respond with ICMP errors). |

##### **Pre-defined Services**

Firewalld has a number of pre-defined services. These service definitions help you identify particular network services to configure. Instead of having to research relevant ports for the `samba-client` service, for example, specify the pre-built `samba-client` service to configure the correct ports and protocols. 

**Table 15.2. Selected Pre-defined Firewalld Services**

| Service name    | Configuration                                                |
| :-------------- | :----------------------------------------------------------- |
| `ssh`           | Local SSH server. Traffic to 22/tcp                          |
| `dhcpv6-client` | Local DHCPv6 client. Traffic to 546/udp on the fe80::/64 IPv6 network |
| `ipp-client`    | Local IPP printing. Traffic to 631/udp.                      |
| `samba-client`  | Local Windows file and print sharing client. Traffic to 137/udp and 138/udp. |
| `mdns`          | Multicast DNS (mDNS) local-link name resolution. Traffic to 5353/udp to the 224.0.0.251 (IPv4) or ff02::fb (IPv6) multicast addresses. |

**Configuring the Firewall from the Command Line**

Note that unless otherwise specified, almost all commands will work on the *runtime* configuration, unless the `--permanent` option is specified. If the `--permanent` option is specified, you must activate the setting by also running the **firewall-cmd --reload** command, which reads the current permanent configuration and applies it as the new runtime configuration. Many of the commands listed take the `--zone=*ZONE*` option to determine which zone they affect. Where a netmask is required, use CIDR notation, such as 192.168.1/24.

| **firewall-cmd** commands                      | Explanation                                                  |
| :--------------------------------------------- | :----------------------------------------------------------- |
| **--get-default-zone**                         | Query the current default zone.                              |
| **--set-default-zone=ZONE**                    | Set the default zone. This changes both the runtime and the permanent configuration. |
| **--get-zones**                                | List all available zones.                                    |
| **--get-active-zones**                         | List all zones currently in use (have an interface or source tied to them), along with their interface and source information. |
| **--add-source=CIDR [--zone=ZONE]**            | Route all traffic coming from the IP address or network/netmask to the specified zone. If no `--zone=` option is provided, the default zone is used. |
| **--remove-source=CIDR [--zone=ZONE]**         | Remove the rule routing all traffic from the zone coming from the IP address or network/netmask network. If no `--zone=` option is provided, the default zone is used. |
| **--add-interface=INTERFACE [--zone=ZONE]**    | Route all traffic coming from `*INTERFACE*` to the specified zone. If no `--zone=` option is provided, the default zone is used. |
| **--change-interface=INTERFACE [--zone=ZONE]** | Associate the interface with `*ZONE*` instead of its current zone. If no `--zone=` option is provided, the default zone is used. |
| **--list-all [--zone=ZONE]**                   | List all configured interfaces, sources, services, and ports for `*ZONE*`. If no `--zone=` option is provided, the default zone is used. |
| **--list-all-zones**                           | Retrieve all information for all zones (interfaces, sources, ports, services). |
| **--add-service=SERVICE [--zone=ZONE]**        | Allow traffic to `*SERVICE*`. If no `--zone=` option is provided, the default zone is used. |
| **--add-port=PORT/PROTOCOL [--zone=ZONE]**     | Allow traffic to the `*PORT/PROTOCOL*` port(s). If no `--zone=` option is provided, the default zone is used. |
| **--remove-service=SERVICE [--zone=ZONE]**     | Remove `*SERVICE*` from the allowed list for the zone. If no `--zone=` option is provided, the default zone is used. |
| **--remove-port=PORT/PROTOCOL [--zone=ZONE]**  | Remove the `*PORT/PROTOCOL*` port(s) from the allowed list for the zone. If no `--zone=` option is provided, the default zone is used. |
| **--reload**                                   | Drop the runtime configuration and apply the persistent configuration. |

The example commands below set the default zone to `dmz`, assign all traffic coming from the `192.168.0.0/24` network to the `internal` zone, and open the network ports for the `mysql` service on the `internal` zone.

```
firewall-cmd --get-zones
firewall-cmd --get-default-zone
firewall-cmd --set-default-zone=dmz
firewall-cmd --permanent --zone=internal --add-source=192.168.0.0/24
firewall-cmd --permanent --zone=internal --add-service=mysql
firewall-cmd --reload
```

##### 

Another example, add apache access to home zone

```
firewall-cmd --zone=home --list-all   		  -> or --list-services
firewall-cmd --zone=home --add-service=http   -> or port permanent
firewall-cmd --zone=home --add-port=80/tcp --permanent
firewall-cmd --reload
firewall-cmd --zone=home --list-all 
firewall-cmd --add-source=192.168.0.0/24
```

(if is default zone you don't need zone)



__________

### Recommendations

https://www.redhat.com/en/services/training/ex200-red-hat-certified-system-administrator-rhcsa-exam

- RCHSA 8 VDO, Stratis thin disks and tuned settings.  and Yum Modules, Application Streams, without LDAP

  *Configure disk compression* => VDO

  *Manage layered storage* => Stratis

  *Work with package module streams* => Application Streams

- A common test taking technique is to start with the questions you know how to do well first--this is good advice generally, but some of the questions build on each-other (i.e. "make X partion of Z size" and then later on "on X partion, make directory Y".) If you skipped ahead to the easier "make a directory" question, then you may do it incorrectly or not understand where it needs to go since it was based on a previous question. So my advice? Try really hard to do the question in order, and only skip it if you really don't know.

- Use man -K a lot. Not lowercase k, UPPERCASE. That searches through the body of every manpage. 

- yum provides and rpm -qx a MUST.

- `ps axo command,pid,%cpi --sort=%cpu | tail` List process with more consumed cpu

- `yum install bash-completion`

- `/etc/nswitch` controls the order that resources are checked for resolution. -> systemctl restart network

- If you use fdisk/gdisk don't forget execute a `partprobe` after create the partition

- Set up the NFS Share

  First, install the required package by running `yum install nfs-utils -y`

  We need to create the mount point that will be served out with `mkdir /nfs`.

  Edit `/etc/exports`, adding a line that looks like `/nfs *(rw)`.

  Run `chmod 777 /nfs` to make sure it's writable.

  Run `exportfs -a` to export the directory we've configured in `/etc/exports`

  Now we need to start the required services with:

  ```
  systemctl start {rpcbind,nfs-server,rpc-statd,nfs-idmapd}
  ```

  On the client, we should install the required package by running:

  ```
  yum install nfs-utils -y
  ```

  Then we can create the mount point with `mkdir /mnt/nfs`.

  Run `showmount -e <NFS_SERVER_IP>` .

  Start the required service with:

  ```
  systemctl start rpcbind
  ```

  And finally we can run `mount -t nfs <NFS_SERVER_IP>:/nfs /mnt/nfs`.

New objectives RHCSA v8: 

- Adjust process scheduling
- Preserve system journals
- Configure disk compression
- Manage layered storage
- Work with package module streams
- Configure superuser access
- Restrict network access using firewall-cmd/firewall

Objective that are gone:

- KVM
- ACL
- Find / Locate
- CIFS - "Mount and unmount network file systems using NFS" (previous version included CIFS)
- Update kernel
- Configure system to use existing authentication service



Write a complete yum.repo (name, baseurl,enabled,gpgcheck,gpgkey)



- **Before the exam: Arrive early! You need at least 15 minutes to set up, though it’s recommended you have 30 minutes in case you run into technical issues.**
- **Read the exam introduction because it includes important information about the exam environment.**
- **Read through all the exam tasks before starting any of them.**
- **Begin with starting/enabling the relevant services, then enable firewall services/ports that are needed. Once all of that is done, you don’t have to think about it for the rest of the exam and can focus on the tasks.**
- **After enabling services and changing firewall settings, do any tasks that have to do with LVM or disk partitioning. This way, if you make any major mistakes that require to restart the exam, you don’t have to redo as much work as you would otherwise.**
- **After disk partitioning related tasks, start doing tasks that are easy for you first. This will give you more time at the end to focus on hardest tasks that may require you to read man pages and other documentation.**
- **Don’t know what to do? Use man -K [search term]. The uppercase is intentional—it allows you to search through all manpages. Lowercase ‘k’ only searches the title and descriptions of a manpage.**



1. Make sure that your server boots and you have root access to it.
2. Configure networking in the way it is supposed to work.
3. Configure any repositories that you need.
4. Install and enable all services that need to be up and running at the end of the exam.
5. Work on all storage-related tasks.
6. Create all required user and group accounts.
7. Set permissions.
8. Make sure SELinux is working and configured as required.
9. Work on everything else.



| Command                        | Task                                          |
| ------------------------------ | --------------------------------------------- |
| chrt -p #PID                   | To view a process current settings            |
| chrt -f -p #PRIO #PID          | To make a process use FIFO                    |
| chrt -r -p #PRIO #PID          | To make a process use RR                      |
| chrt -o -p #PRIO #PID          | To make a process use OTHER                   |
| chrt -f #PRIO /path/to/command | To start a process with a different scheduler |

Stratis

```
UUID=a1f0b64a-4ebb-4d4e-9543-b1d79f600283 /fs1 xfs defaults,x-systemd.requires=stratisd.service 0 0
systemctl daemon-reload
```

Vdo

```
/dev/mapper/vdo-name mount-point xfs defaults,_netdev,x-systemd.device-timeout=0,x-systemd.requires=vdo.service 0 0
```

