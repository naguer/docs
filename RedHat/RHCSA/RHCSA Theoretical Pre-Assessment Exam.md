# Theoretical Pre-Assessment Exam

The RHCSA exam is a 100% practical exam. Therefore, you need to work on actual configuration tasks, and you must deliver a working configuration at the end of the exam. Therefore, passing this practical exam requires that you have a working knowledge of RHEL 8. This chapter helps you check whether you have the requisite knowledge.

In the following pre-exam theoretical exam, you are asked how you would approach some essential tasks. The purpose is to check for yourself whether you are on the right track. You do not have to provide a detailed step-by-step procedure. You just need to know what needs to be done. For instance, if the question asks how to set the appropriate SELinux context type on a nondefault web server document root, you know what you need to know if you say “check the semanage-fcontext man page.” If you do not have the answers to any of these questions, you know that you need to do additional studying on those topics.

In this theoretical pre-assessment exam, some key elements are covered. This test is *not* 100% comprehensive; it just focuses on some of the most essential skills.

1. You need to create a shared group environment where members of the group sales can easily share permissions with one another. Which approach would you suggest?
2. You need to change the hostname of the computer to something else and do it persistently. How would you do that?
3. On your disk, you have to create a logical volume with a size of 500 MB and the name my_lv. You do not have LVM volumes yet. List the steps to be taken to create the logical volume and mount it as an Ext4 file system on the /data directory. Also ensure that the extent size this logical volume uses is 8 MiB.
4. On the logical volume created in step 3, you need to set an ACL that gives members of the account group read and execute permissions. All other permission settings can be left as they are. How would you do this?
5. While booting, your server gives an error and shows “Enter root password for maintenance mode.” What is the most likely explanation for this problem?
6. You need to access a repository that is available on ftp://server.example.com/pub/repofiles. How would you do this?
7. You want to manually edit the network configuration by modifying the relevant configuration file for the eth0 interface. What is the name of this file? Do you also need to do something else to make sure that the configuration is not changed back again automatically? Which service needs to be restarted to make the changes effective?
8. What configuration line would you add to which configuration file to schedule a cron job that executes the command **logger it is 2 AM** at 2 a.m. on every weekday? (You need to exclude Saturday and Sunday.)
9. You have configured your web server to listen at port 8082, and now it doesn’t start anymore. How do you troubleshoot?
10. You have access to the server console, but you do not have the root password to log in to that server. Describe step by step what you would do to get access to the server by changing the password of user root.
11. Describe exactly what you need to do to automatically mount home directories for LDAP users. The home directories are on nfs://server.example.com/home/ldapusers, and they should be automounted at /home/ldapusers on your local machine.
12. You need to install the RPM package that contains the file sealert, but you have no clue what the name of this package is. What is the best way to find the package name?
13. You have just downloaded a new kernel file from an FTP server; the update is not available in a repository. How do you use it to update your kernel in a way that allows you to install the new kernel but still keep the old kernel available for booting as a backup in case things go wrong?
14. You are trying to find relevant man pages that match the keyword user. You type **man -k user** but get the “nothing appropriate” message. How can you fix this?
15. How do you add a user to a new secondary group with the name sales without modifying the existing (secondary) group assignments?
16. How would you create a 5-GiB VDO volume with the name vdodata and mount it automatically on /vdodata?
17. How would you configure timesync, such that your server is synchronizing time with server10.example.com?
18. How would you check the recommended tuned profile and set it as the default profile?