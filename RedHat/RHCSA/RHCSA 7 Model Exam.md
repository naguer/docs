 Exam Times:

RHCSA: Two and a half hours.

Pass Scores:

Total 300 points. Pass at 210 points.

\---------------------------------------------------------------------------------------------------------------------------

**Password Crack**

mount -o

remount,rw /sysroot/, remount r,w permission;

chroot /sysroot/

passwd

/.autorelabel

\-----------------------------------------------------------------------------------------

**Config Ip - Set Hostname**

Hostname: station.network12.example.com

IP: 172.24.12.10

Mask: 255.255.255.0

Gateway: 172.24.12.254

DNS: 172.24.12.0

NameServer: server.network12.example.com 172.24.12.100

Systemctl isolate graphical.target

**Nmtui** (interfaz grafica podemos configurar la conexión ya que por NMCLI no me dejo hacer todos los cambios como por ejemplo el gateway)

hostnamectl set-hostname server.network12.example.com

nmcli con mod eth0 ipv4.method manual

nmcli con mod eth0 ipv4.address 172.24.12.10/24 ipv4.gateway 172.24.12.254 Ipv4.dns 172.24.12.0, 172.24.12.254  

por las dudas agregue al archivos hosts  

172.24.12.100 server.network12.example.com



  



  



  

___________________

**Configure SELINUX to make it work in enforcing mode**

```
# setenforce 1  
```

```
vim /etc/selinux/config  

selinux=enforcing
```

\--------------------------------------------------------------------------------------------------------

**Configure a default software repository for your system**

Vim /etc/yum.repos.d/base.repo

```
[base]
Name= baserhel7
Baseurl= http://server.network12.example.com/pub/x86_64/
Gpgcheck=0
Enabled=1
```

Yum clean all

Yum repolist

\------------------------------------------------------------------------------------------------------------

2 Adjust the size of the Logical Volume

Adjust the size of the vo Logical Volume, its file system size should be 250M.  

Sin perder información.

Fijarse si es para aumentar o decrecer el volumen de esa unidad

 RECOMENDARIA HACER BACKUP DE INFO POR LAS DUDAS

AUMENTAR:

df -h

lvextend -L +100M /dev/vg0/vo

xfs_growfs /mnt/data  

resize2fs /dev/vg0/vo    

DISMINUIR

Umount /mnt/data

E2ckfs -f /dev/vg/vo

Resize2fs / dev/vg/vo 250MB

Lvreduce /dev/vg/vo -L 250MB   

Mount -a  

\-------------------------------------------------------------------------------------------------------

\3. Create User Account

Create the following user, group and group membership: sharegrp  

User natasha, using sharegrp as a sub group

User Harry, also using sharegrp as a sub group

User sarah, can not access the SHELL which is interactive in the system, and is not a member of

sharegrp, natasha，harry，sarah password is redhat.

Solutions:

groupadd sharegrp

useradd natasha -aG sharegrp

useradd haryy -aG sharegrp

useradd sarah -s /sbin/nologin

Passwd username  

\---------------------------------------------------------------------------------------------------------------------

\4. Configure /var/tmp/fstab Permission

Copy the file /etc/fstab to /var/tmp/fstab. Configure var/tmp/fstab permissions as the following:

Owner of the file /var/tmp/fstab is Root, belongs to group shearegrp

File /var/tmp/fstab cannot be executed by any user

User natasha can read and write /var/tmp/fstab

User harry cannot read and write /var/tmp/fstab

All other users (present and future) can read var/tmp/fstab.

Solutions:

cp /etc/fstab /var/tmp/fstab

chown root:root  /var/tmp/fstab

Chmod 664 /var/tmp/fstab

setfacl -m u:natasha:rw- /var/tmp/fstab

 setfacl -m u:haryy:--- /var/tmp/fstab



  



  



  

5 Configure a cron Task

User natasha must configure a cron job, local time 14:23 runs and executes: */bin/echo hiya every

crontab -e -u natasha

23 14 * * * /bin/echo hiya

Systemctl enable crond

Systemcdl restart crond

\------------------------------------------------------------------------------------------------

\6. Create a Shared Directory

Create a shared directory /root/shared, make it has the following characteristics:

/root/shared  belongs to group sharegrp

This directory can be read and written by members of group sharegrp

Any files created in /root/shared, group automatically set as sharegrp.

Solutions:

mkdir /root/shared

chgrp -R sharegrp /root/shared

chmod 2770 /root/shared -R

\----------------------------------------------------------------------------------------------------------------------------

\7. Install the Kernel Upgrade

Istall suitable kernel updation from:

http://server.network12.example.com/pub/updates.

NUNCA LO PUDE DESCARGAR LA VERDAD NI IDEA

Following requirements must be met:

Updated kernel used as the default kernel of system start-up.

The original kernel is still valid and can be guided when system starts up.

Soultion:



  

rpm -ivh kernel-*.rpm

vi /boot/grub/grub.conf  

Some questions are: Install and upgrade the kernel as required. To ensure that grub2 is the

default item for startup.

TAMBIEN INTENTE AGREGE EL REPOSITORIO CON ESA DIRECCION Y TODO LO TOMA PERO NO ME INSTALABA EL KERNELL AGREGE EXCEPCIONES DE FIREWALL Y DEMAS Y NADA

\---------------------------------------------------------------------------------------------------------------------------

\8. Binding to an external validation server

System server.network12.example.com provides a LDAP validation service, your system should

bind to this service as required:

Base DN of validation service is dc=network12,dc=example,dc=com

LDAP is used for prviding account information and validation information

Conneting and using the certification of

http://server.network12.example.com/pub/EXAMPLE-CA-CERT.perm to encrypt

After the correct configuration, ldapuser1 can log into your system, it does not have HOME directory

until you finish autofs questions, ldapuser1 password is password.

Solutions:

yum -y install sssd authconfig-gtk autofs

authconfig-gtk LO CONFIGURAS Y LISTO

Comprobarlo con ssh localhost -l ldapuser1

Y te tiene que logear diciendo que no pudo crearte el directorio.

El CA es .perm no pasa nada.

\---------------------------------------------------------------------------------------------------------------

\9. Configure NTP

Configure NTP service, NTP server :http://server.network12.example.com   

Configure the client:

Yum -y install chrony

Vim /etc/chrony.conf

server http://server.network12.example.com  iburst

server 172.24.12.254  iburst

Timedatectl set-ntp true

systemctl enable chronyd

 systemctl restart chronyd

Validate: timedatectl status

LO HICE ASI PERO AL VALIDAD NUNCA ME DIO SINCRONIZADO

\---------------------------------------------------------------------------------------------------------------------------

10 Configure autofs

Configure the autofs automatically mount to the home directory of LDAP, as required:

server.network12.example.com use NFS to share the home to your system. This file system

contains a pre configured home directory of user ldapuserX.

Home directory of ldapuserX is:

server.domain11.example.com /netdir

Home directory of ldapuserX should automatically mount to the ldapuserX of the local

/netdir

Home directory’s write permissions must be availabe for users

ldapuser1’s password is password  

yum install -y autofs

mkdir /netdir

echo “/netdir /etc/ldapnetdir” >> /etc/auto.master.d/hola.autofs

echo “* -rw,sync 172.24.12.254:/netdir/&” >>/etc/ldapnetdir

systemctl start autofs

systemctl enable autofs

 ssh localhost -l ldapuser1

\11. Configure a user account

Create a user iar，uid is 3400. Password is redhat

Solutions:

useradd -u 3400 iar

 passwd iar

12 Add a swap partition  

Adding a extra 500M swap partition to your system, this swap partition should mount

automatically when the system starts up. Dont remove and modify the existing swap partitions on

your system.

COMANDOS UTILES lsblk blkid

fdisk  /dev/vda  

OJO FIJARSE SI ES LA 4TA PARTICION VAS A TENER QUE CREARLA COMO EXTENDIDA Y DESPUES CREAR UNA PARTICION PRIMARIA SOBRE LA EXTENDIDA Y DARLE EL FORMATO 82

Vgcreate vgswap /sdev/vda5

Lvcreate vgswap -n swap 100%FREE

mkswap /dev/vgswap/swap  

echo “UUID swap swap defaults 0 0” >> /etc/fstab

free -m

swapon -a

free -m

\-------------------------------------------------------------------------------------------------------------

\13. Search files

Find out files owned by jack, and copy them to directory /root/findresults

Solution:

mkdir /root/findfiles

find / -user jack -exec cp -a {} /root/findfiles/ \;

ls /root/findresults

\----------------------------------------------------------------------------------------------------------------

\14. Search a String

Find out all the columns that contains the string seismic within

/usr/share/dict/words, then copy all these columns to /root/lines.tx in original order, there is no blank

line, all columns must be the accurtae copy of the original columns.

Solutions:

grep seismic /usr/share/dict/words > /root/lines.txt

\-------------------------------------------------------------------------------------------------------------

\15. Create a backup

Create a backup file named /root/backup.tar.bz2, contains the content of /usr/local, tar must use gzip to compress.

Tar -cvzf /root/backup.tgz /usr/local

\---------------------------------------------------------------------------------------------------------------

\16. Create a logical volume

Create a new logical volume as required:

Name the logical volume as database, belongs to datastore of the vloume group, size is 50 PE.

Expansion size of each vloumes in volum group datastore is 16MB.

Use ext3 to format this new logical volume, this logical vloume should automatically mount to

/mnt/database

ACORDARSE QUE YA TENIAS CREADA LA EXTENDIDA ASI QUE PODES SEGUIR CREANDO PARTICIONE PRIMARIAS SIN PROBLEMA

fdisk  /dev/vda  

vgcreate datastore /dev/vdax –s 16M

lvcreate datastore  –n database – l 50

mkfs.ext3 /dev/datastore/database

mkdir /mnt/database

echo “/dev/datastore/database  /mnt/database ext3 defaults 0 0” >> /etc/fstab

mount -a

df –Th

\----------------------------------------------------------------------------------------------------------------------