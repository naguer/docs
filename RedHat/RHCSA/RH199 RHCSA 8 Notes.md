# Red Hat Certified System Administrator 8



## Chapter 1: Accessing Systems and Obtaining Support

**Using ssh-agent for Non-interactive Authentication**

If your SSH private key is protected with a passphrase, you normally have to enter the passphrase to use the private key for authentication. However, you can use a program called **ssh-agent** to temporarily cache the passphrase in memory. Then any time that you use SSH to log in to another system with the private key, **ssh-agent** will automatically provide the passphrase for you. This is convenient, and can improve security by providing fewer opportunities for someone "shoulder surfing" to see you type the passphrase in.

Once **ssh-agent** is running, you need to tell it the passphrase for your private key or keys. You can do this with the **ssh-add** command.

The following **ssh-add** commands add the private keys from `/home/*user*/.ssh/id_rsa` (the default) and `/home/*user*/.ssh/key-with-pass` files, respectively.

```
[operator1@serverb ~]$ eval $(ssh-agent)
Agent pid 21032
[operator1@serverb ~]$ ssh-add .ssh/key2
Enter passphrase for .ssh/key2: redhatpass
Identity added: .ssh/key2 (operator1@serverb.lab.example.com)
```

The preceding **eval** command started **ssh-agent** and configured this shell session to use it. You then used **ssh-add** to provide the unlocked private key to **ssh-agent**.

**WARNING**

If you do not specify the file where the key gets saved, the default file (`/home/*user*/.ssh/id_rsa`) is used. You have already used the default file name when generating SSH keys in the preceding step, so it is vital that you specify a non-default file, otherwise the existing SSH keys will be overwritten.

```
[operator1@serverb ~]$ ssh-keygen -f .ssh/key2
```

**Summary**

- The Bash shell is a command interpreter that prompts interactive users to specify Linux commands.
- Many commands have a `--help` option that displays a usage message or screen.
- SSH supports both password-based and key-based authentication.
- The **ssh-keygen** command generates an SSH key pair for authentication. The **ssh-copy-id** command exports the public key to remote systems.
- Red Hat Customer Portal provides you with access to documentation, downloads, optimization tools, support case management, and subscription and entitlement management for your Red Hat products.
- **redhat-support-tool** is a command-line tool to query Knowledgebase and work with support cases from the server's command line.
- Red Hat Insights is a SaaS-based predictive analytics tool to help you identify and remediate threats to your systems' security, performance, availability, and stability.



## Chapter 2. Navigating File Systems

| Location | Purpose                                                      |
| :------- | :----------------------------------------------------------- |
| `/usr`   | Installed software, shared libraries, include files, and read-only program data. Important subdirectories include:`/usr/bin`: User commands.`/usr/sbin`: System administration commands.`/usr/local`: Locally customized software. |
| `/etc`   | Configuration files specific to this system.                 |
| `/var`   | Variable data specific to this system that should persist between boots. Files that dynamically change, such as databases, cache directories, log files, printer-spooled documents, and website content may be found under `/var`. |
| `/run`   | Runtime data for processes started since the last boot. This includes process ID files and lock files, among other things. The contents of this directory are recreated on reboot. This directory consolidates `/var/run` and `/var/lock` from earlier versions of Red Hat Enterprise Linux. |
| `/home`  | *Home directories* are where regular users store their personal data and configuration files. |
| `/root`  | Home directory for the administrative superuser, `root`.     |
| `/tmp`   | A world-writable space for temporary files. Files which have not been accessed, changed, or modified for 10 days are deleted from this directory automatically. Another temporary directory exists, `/var/tmp`, in which files that have not been accessed, changed, or modified in more than 30 days are deleted automatically. |
| `/boot`  | Files needed in order to start the boot process.             |
| `/dev`   | Contains special *device files* that are used by the system to access hardware. |

**Hard Links and Soft Links**

It is possible to create multiple names that point to the same file. There are two ways to do this: by creating a *hard link* to the file, or by creating a *soft link* (sometimes called a *symbolic link*) to the file. Each has its advantages and disadvantages.



**Creating Hard Links**

Every file starts with a single hard link, from its initial name to the data on the file system. When you create a new hard link to a file, you create another name that points to that same data. The new hard link acts exactly like the original file name. Once created, you cannot tell the difference between the new hard link and the original name of the file.

You can find out if a file has multiple hard links with the **ls -l** command. One of the things it reports is each file's *link count*, the number of hard links the file has.

```
[user@host ~]$ pwd
/home/user
[user@host ~]$ ls -l newfile.txt
-rw-r--r--. 1 user user 0 Mar 11 19:19 newfile.txt
```

In the preceding example, the link count of `newfile.txt` is 1. It has exactly one absolute path, which is `/home/user/newfile.txt`.

If you want to find out whether two files are hard links of each other, one way is to use the `-i` option with the **ls** command to list the files' *inode number*. If the files are on the same file system (discussed in a moment) and their inode numbers are the same, the files are hard links pointing to the same data.

```
[user@host ~]$ ls -il newfile.txt /tmp/newfile-hlink2.txt
8924107 -rw-rw-r--. 2 user user 12 Mar 11 19:19 newfile.txt
8924107 -rw-rw-r--. 2 user user 12 Mar 11 19:19 /tmp/newfile-hlink2.txt
```

Even if the original file gets deleted, the contents of the file are still available as long as at least one hard link exists. Data is only deleted from storage when the last hard link is deleted.



**Limitations of Hard Links**

Hard links have some limitations. Firstly, hard links can only be used with regular files. You cannot use **ln** to create a hard link to a directory or special file.

Secondly, hard links can only be used if both files are on the same *file system*. The file-system hierarchy can be made up of multiple storage devices. Depending on the configuration of your system, when you change into a new directory, that directory and its contents may be stored on a different file system.



**Creating Soft Links**

The **ln -s** command creates a soft link, which is also called a "symbolic link." A soft link is not a regular file, but a special type of file that points to an existing file or directory.

Soft links have some advantages over hard links:

- They can link two files on different file systems.
- They can point to a directory or special file, not just a regular file.

One way to compare hard links and soft links that might help you understand how they work:

- A hard link points a name to data on a storage device
- A soft link points a name to another name, that points to data on a storage device



**Summary**

- Files on a Linux system are organized into a single inverted tree of directories, known as a file-system hierarchy.
- Absolute paths start with a / and specify the location of a file in the file-system hierarchy.
- Relative paths do not start with a / and specify the location of a file relative to the current working directory.
- Five key commands are used to manage files: **mkdir**, **rmdir**, **cp**, **mv**, and **rm**.
- Hard links and soft links are different ways to have multiple file names point to the same data.



## Chapter 3. Managing Local Users and Groups

By default, systems use the `/etc/passwd` file to store information about local users.

Each line in the `/etc/passwd` file contains information about one user. It is divided up into seven colon-separated fields. Here is an example of a line from `/etc/passwd`:

```
user01:x:1000:1000:User One:/home/user01:/bin/bash
```

| user01       | Username for this user (`user01`).                           |
| ------------ | ------------------------------------------------------------ |
| x            | The user's password used to be stored here in encrypted format. That has been moved to the `/etc/shadow` file, which will be covered later. This field should always be `x`. |
| 1000         | The UID number for this user account (`1000`).               |
| 1000         | The GID number for this user account's primary group (`1000`). Groups will be discussed later in this section. |
| User One     | The real name for this user (`User One`).                    |
| /home/user01 | The home directory for this user (`/home/user01`). This is the initial working directory when the shell starts and contains the user's data and configuration settings. |
| /bin/bash    | The default shell program for this user, which runs on login (**/bin/bash**). For a regular user, this is normally the program that provides the user's command-line prompt. A system user might use **/sbin/nologin** if interactive logins are not allowed for that user. |

Each line in the `/etc/group` file contains information about one group. Each group entry is divided into four colon-separated fields. Here is an example of a line from `/etc/group`:

```
group01:x:10000:user01,user02,user03
```

| group01                | Group name for this group (`group01`).                       |
| ---------------------- | ------------------------------------------------------------ |
| x                      | Obsolete group password field. This field should always be `x`. |
| 10000                  | The GID number for this group (`10000`).                     |
| user01, user02, user03 | A list of users who are members of this group as a supplementary group (`user01`, `user02`, `user03`). Primary (or default) and supplementary groups are discussed later in this section. |



**Primary Groups and Supplementary Groups**

Every user has exactly one primary group. For local users, this is the group listed by GID number in the `/etc/passwd` file. By default, this is the group that will own new files created by the user.

Normally, when you create a new regular user, a new group with the same name as that user is created. That group is used as the primary group for the new user, and that user is the only member of this *User Private Group*. It turns out that this helps make management of file permissions simpler, which will be discussed later in this course.

Users may also have *supplementary groups*. Membership in supplementary groups is determined by the `/etc/group` file. Users are granted access to files based on whether any of their groups have access. It doesn't matter if the group or groups that have access are primary or supplementary for the user.



**Gaining Superuser Access**

set up **sudo** to allow a user to run commands as another user without entering their password:

```
ansible  ALL=(ALL)  NOPASSWD:ALL
```

The `ALL=(ALL)` specifies that on any host that might have this file, `ansible` can run any command.



**Modifying Existing Users from the Command Line**

- The **usermod --help** command displays the basic options that can be used to modify an account. Some common options include:

  | **usermod** options:    | Usage                                                        |
  | :---------------------- | :----------------------------------------------------------- |
  | `-c, --comment COMMENT` | Add the user's real name to the comment field.               |
  | `-g, --gid GROUP`       | Specify the primary group for the user account.              |
  | `-G, --groups GROUPS`   | Specify a comma-separated list of supplementary groups for the user account. |
  | `-a, --append`          | Used with the `-G` option to add the supplementary groups to the user's current set of group memberships instead of replacing the set of supplementary groups with a new set. |
  | `-d, --home HOME_DIR`   | Specify a particular home directory for the user account.    |
  | `-m, --move-home`       | Move the user's home directory to a new location. Must be used with the **-d** option. |
  | `-s, --shell SHELL`     | Specify a particular login shell for the user account.       |
  | `-L, --lock`            | Lock the user account.                                       |
  | `-U, --unlock`          | Unlock the user account.                                     |



**Deleting Users from the Command Line**

- The **userdel username** command removes the details of `username` from `/etc/passwd`, but leaves the user's home directory intact.
- The **userdel -r username** command removes the details of `username` from `/etc/passwd` and also deletes the user's home directory.

The `root` user can use the **find / -nouser -o -nogroup** command to find all unowned files and directories.



**UID Ranges**

Specific UID numbers and ranges of numbers are used for specific purposes by Red Hat Enterprise Linux.

- *UID 0* is always assigned to the superuser account, `root`.
- *UID 1-200* is a range of "system users" assigned statically to system processes by Red Hat.
- *UID 201-999* is a range of "system users" used by system processes that do not own files on the file system. They are typically assigned dynamically from the available pool when the software that needs them is installed. Programs run as these "unprivileged" system users in order to limit their access to only the resources they need to function.
- *UID 1000+* is the range available for assignment to regular users.



**Managing Local Group Accounts**

- The `-r` option creates a system group using a GID from the range of valid system GIDs listed in the `/etc/login.defs` file. The `SYS_GID_MIN` and `SYS_GID_MAX` configuration items in `/etc/login.defs` define the range of system GIDs.

  ```
  [user01@host ~]$ sudo groupadd -r group02
  [user01@host ~]$ tail /etc/group
  ...output omitted...
  group01:x:10000:
  group02:x:988:
  ```



**Modifying Existing Groups from the Command Line**

- The **groupmod** command changes the properties of an existing group. The `-n` option specifies a new name for the group.

  ```
  [user01@host ~]$ sudo groupmod -n group0022 group02
  ```

The **-g** option specifies a new GID.

```
[user01@host ~]$ sudo groupmod -g 20000 group0022
```



**Changing Group Membership from the Command Line**

- The membership of a group is controlled with user management. Use the **usermod -g** command to change a user's primary group.

  ```
  [user01@host ~]$ id user02
  uid=1006(user02) gid=1008(user02) groups=1008(user02)
  [user01@host ~]$ sudo usermod -g group01 user02
  [user01@host ~]$ id user02
  uid=1006(user02) gid=10000(group01) groups=10000(group01)
  ```

- Use the **usermod -aG** command to add a user to a supplementary group.

  ```
  [user01@host ~]$ id user03
  uid=1007(user03) gid=1009(user03) groups=1009(user03)
  [user01@host ~]$ sudo usermod -aG group01 user03
  [user01@host ~]$ id user03
  uid=1007(user03) gid=1009(user03) groups=1009(user03),10000(group01)
  ```

  **IMPORTANT**

  The use of the `-a` option makes **usermod** function in *append* mode. Without `-a`, the user will be removed from any of their current supplementary groups that are not included in the `-G` option's list.



**Configuring Password Aging**

```
[user01@host ~]$ sudo chage -m 0 -M 90 -W 7 -I 14 user03
```

The preceding **chage** command uses the `-m`, `-M`, `-W`, and `-I` options to set the minimum age, maximum age, warning period, and inactivity period of the user's password, respectively.

The **chage -d 0 user03** command forces the `user03` user to update its password on the next login.

The **chage -l user03** command displays the password aging details of `user03`.

The **chage -E 2019-08-05 user03** command causes the `user03` user's account to expire on 2019-08-05 (in YYYY-MM-DD format).

**NOTE**

The **date** command can be used to calculate a date in the future. The `-u` option reports the time in UTC.

```
[user01@host ~]$ date -d "+45 days" -u
Thu May 23 17:01:20 UTC 2019
```

Determine a date 180 days in the future. Use the format `%F` with the **date** command to get the exact value.

```
[student@servera ~]$ date -d "+180 days" +%F
2019-07-24
```

Edit the password aging configuration items in the `/etc/login.defs` file to set the default password aging policies. The `PASS_MAX_DAYS` sets the default maximum age of the password. The `PASS_MIN_DAYS` sets the default minimum age of the password. The `PASS_WARN_AGE` sets the default warning period of the password. Any change in the default password aging policies will be effective for new users only. The existing users will continue to use the old password aging settings rather than the new ones.



**Restricting Access**

You can use the **chage** command to set account expiration dates. When that date is reached, the user cannot log in to the system interactively. The **usermod** command can lock an account with the `-L` option.

```
[user01@host ~]$ sudo usermod -L user03
[user01@host ~]$ su - user03
Password: redhat
su: Authentication failure
```

If a user leaves the company, the administrator may lock and expire an account with a single **usermod** command. The date must be given as the number of days since 1970-01-01, or in the *YYYY-MM-DD* format.

```
[user01@host ~]$ sudo usermod -L -e 2019-10-05 user03
```

The preceding **usermod** command uses the `-e` option to set the account expiry date for the given user account. The `-L` option locks the user's password.



**Summary**

In this chapter, you learned:

- There are three main types of user account: the superuser, system users, and regular users.
- A user must have a primary group and may be a member of one or more supplementary groups.
- The three critical files containing user and group information are `/etc/passwd`, `/etc/group`, and `/etc/shadow`.
- The **su** and **sudo** commands can be used to run commands as the superuser.
- The **useradd**, **usermod**, and **userdel** commands can be used to manage users.
- The **groupadd**, **groupmod**, and **groupdel** commands can be used to manage groups.
- The **chage** command can be used to configure and view password expiration settings for users.



## Chapter 4. Controlling Access to Files

**Changing File and Directory Permissions**

- *Who* is u, g, o, a *(for user, group, other, all)*
- *What* is +, -, = *(for add, remove, set exactly)*
- *Which* is r, w, x *(for read, write, execute)*

The **chmod** command supports the `-R` option to recursively set permissions on the files in an entire directory tree. When using the `-R` option, it can be useful to set permissions symbolically using the `X` option. This allows the execute (search) permission to be set on directories so that their contents can be accessed, without changing permissions on most files. Be cautious with the `X` option, however, because if a file has any execute permission set, `X` will set the specified execute permission on that file as well. For example, the following command recursively sets read and write access on `demodir` and all its children for their group owner, but only applies group execute permissions to directories and files that already have execute set for user, group, or other.

```
[root@host opt]# chmod -R g+rwX demodir
```



**Special Permissions**

*Special permissions* constitute a fourth permission type in addition to the basic user, group, and other types. As the name implies, these permissions provide additional access-related features over and above what the basic permission types allow. This section details the impact of special permissions, summarized in the table below.

 **Table 4.1. Effects of Special Permissions on Files and Directories**

| Special permission | Effect on files                                              | Effect on directories                                        |
| :----------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| `u+s` (suid)       | File executes as the user that owns the file, not the user that ran the file. | No effect.                                                   |
| `g+s` (sgid)       | File executes as the group that owns the file.               | Files newly created in the directory have their group owner set to match the group owner of the directory. |
| `o+t` (sticky)     | No effect.                                                   | Users with write access to the directory can only remove files that they own; they cannot remove or force saves to files owned by other users. |

The *setuid* permission on an executable file means that commands run as the user owning the file, not as the user that ran the command. One example is the **passwd** command:

```
[user@host ~]$ ls -l /usr/bin/passwd
-rwsr-xr-x. 1 root root 35504 Jul 16  2010 /usr/bin/passwd
```

In a long listing, you can identify the setuid permissions by a lowercase `s` where you would normally expect the `x` (owner execute permissions) to be. If the owner does not have execute permissions, this is replaced by an uppercase `S`.

The special permission *setgid* on a directory means that files created in the directory inherit their group ownership from the directory, rather than inheriting it from the creating user. This is commonly used on group collaborative directories to automatically change a file from the default private group to the shared group, or if files in a directory should be always owned by a specific group. An example of this is the `/run/log/journal` directory:

```
[user@host ~]$ ls -ld /run/log/journal
drwxr-sr-x. 3 root systemd-journal 60 May 18 09:15 /run/log/journal
```

If setgid is set on an executable file, commands run as the group that owns that file, not as the user that ran the command, in a similar way to setuid works. One example is the **locate** command:

```
[user@host ~]$ ls -ld /usr/bin/locate
-rwx--s--x. 1 root slocate 47128 Aug 12 17:17 /usr/bin/locate
```

In a long listing, you can identify the setgid permissions by a lowercase `s` where you would normally expect the `x` (group execute permissions) to be. If the group does not have execute permissions, this is replaced by an uppercase `S`.

Lastly, the *sticky bit* for a directory sets a special restriction on deletion of files. Only the owner of the file (and `root`) can delete files within the directory. An example is `/tmp`:

```
[user@host ~]$ ls -ld /tmp
drwxrwxrwt. 39 root root 4096 Feb  8 20:52 /tmp
```

In a long listing, you can identify the sticky permissions by a lowercase `t` where you would normally expect the `x` (other execute permissions) to be. If other does not have execute permissions, this is replaced by an uppercase `T`.



**Setting Special Permissions**

- Symbolically: setuid = `u+s`; setgid = `g+s`; sticky = `o+t`
- Numerically (fourth preceding digit): setuid = 4; setgid = 2; sticky = 1

**Examples**

- Add the setgid bit on `directory`:

  ```
  [user@host ~]# chmod g+s directory
  ```

- Set the setgid bit and add read/write/execute permissions for user and group, with no access for others, on `directory`:

  ```
  [user@host ~]# chmod 2770 directory
  ```



**Default File Permissions**

When you create a new file or directory, it is assigned initial permissions. There are two things that affect these initial permissions. The first is whether you are creating a regular file or a directory. The second is the current *umask*.

If you create a new directory, the operating system starts by assigning it octal permissions 0777 (`drwxrwxrwx`). If you create a new regular file, the operating system assignes it octal permissions 0666 (`-rw-rw-rw-`). You always have to explicitly add execute permission to a regular file. This makes it harder for an attacker to compromise a network service so that it creates a new file and immediately executes it as a program.

However, the shell session will also set a umask to further restrict the permissions that are initially set. This is an octal bitmask used to clear the permissions of new files and directories created by a process. If a bit is set in the umask, then the corresponding permission is cleared on new files. For example, the umask 0002 clears the write bit for other users. The leading zeros indicate the special, user, and group permissions are not cleared. A umask of 0077 clears all the group and other permissions of newly created files.

The **umask** command without arguments will display the current value of the shell's umask:

```
[user@host ~]$ umask
0002
```

Use the **umask** command with a single numeric argument to change the umask of the current shell. The numeric argument should be an octal value corresponding to the new umask value. You can omit any leading zeros in the umask.

The system's default umask values for Bash shell users are defined in the `/etc/profile` and `/etc/bashrc` files. Users can override the system defaults in the `.bash_profile` and `.bashrc` files in their home directories.

**umask Example**

The following example explains how the umask affects the permissions of files and directories. Look at the default umask permissions for both files and directories in the current shell. The owner and group both have read and write permission on files, and other is set to read. The owner and group both have read, write, and execute permissions on directories. The only permission for other is read.

```
[user@host ~]$ umask
0002
[user@host ~]$ touch default
[user@host ~]$ ls -l default.txt
-rw-rw-r--. 1 user user 0 May  9 01:54 default.txt
[user@host ~]$ mkdir default
[user@host ~]$ ls -ld default
drwxrwxr-x. 2 user user 0 May  9 01:54 default 
```

By setting the umask value to 0, the file permissions for other change from read to read and write. The directory permissions for other changes from read and execute to read, write, and execute.

```
[user@host ~]$ umask 0
[user@host ~]$ touch zero.txt
[user@host ~]$ ls -l zero.txt
-rw-rw-rw-. 1 user user 0 May  9 01:54 zero.txt
[user@host ~]$ mkdir zero
[user@host ~]$ ls -ld zero
drwxrwxrwx. 2 user user 0 May  9 01:54 zero 
```

To mask all file and directory permissions for other, set the umask value to 007.

```
[user@host ~]$ umask 007
[user@host ~]$ touch seven.txt
[user@host ~]$ ls -l seven.txt
-rw-rw----. 1 user user 0 May  9 01:55 seven.txt
[user@host ~]$ mkdir seven
[user@host ~]$ ls -ld seven
drwxrwx---. 2 user user 0 May  9 01:54 seven
```

A umask of 027 ensures that new files have read and write permissions for user and read permission for group. New directories have read and write access for group and no permissions for other.

```
[user@host ~]$ umask 027
[user@host ~]$ touch two-seven.txt
[user@host ~]$ ls -l two-seven.txt
-rw-r-----. 1 user user 0 May  9 01:55 two-seven.txt
[user@host ~]$ mkdir two-seven
[user@host ~]$ ls -ld two-seven
drwxr-x---. 2 user user 0 May  9 01:54 two-seven 
```

The default umask for users is set by the shell startup scripts. By default, if your account's UID is 200 or more and your username and primary group name are the same, you will be assigned a umask of 002. Otherwise, your umask will be 022.

**(!) IMPORTANT**

As `root`, you can change this by adding a shell startup script named `/etc/profile.d/local-umask.sh` that looks something like the output in this example:

```
[root@host ~]# cat /etc/profile.d/local-umask.sh
# Overrides default umask configuration
if [ $UID -gt 199 ] && [ "`id -gn`" = "`id -un`" ]; then
    umask 007
else
    umask 022
fi
```

The preceding example will set the umask to 007 for users with a UID greater than 199 and with a username and primary group name that match, and to 022 for everyone else. If you just wanted to set the umask for everyone to 022, you could create that file with just the following content:

```
# Overrides default umask configuration
umask 022
```

To ensure that global umask changes take effect you must log out of the shell and log back in. Until that time the umask configured in the current shell is still in effect.



**Summary**

In this chapter, you learned:

- Files have three categories to which permissions apply. A file is owned by a user, a single group, and other users. The most specific permission applies. User permissions override group permissions and group permissions override other permissions.
- The **ls** command with the `-l` option expands the file listing to include both the file permissions and ownership.
- The **chmod** command changes file permissions from the command line. There are two methods to represent permissions, symbolic (letters) and numeric (digits).
- The **chown** command changes file ownership. The `-R` option recursively changes the ownership of a directory tree.
- The **umask** command without arguments displays the current umask value of the shell. Every process on the system has a umask. The default umask values for Bash are defined in the `/etc/profile` and `/etc/bashrc` files.



## Chapter 5. Managing SELinux Security

### Changing the current SELinux mode

The SELinux subsystem provides tools to display and change modes. To determine the current SELinux mode, run the **getenforce** command. To set SELinux to a different mode, use the **setenforce** command:

```
[user@host ~]# getenforce
Enforcing
[user@host ~]# setenforce
usage:  setenforce [ Enforcing | Permissive | 1 | 0 ]
[user@host ~]# setenforce 0
[user@host ~]# getenforce
Permissive
```

Alternatively, you can set the SELinux mode at boot time by passing a parameter to the kernel: the kernel argument of **enforcing=0** boots the system into permissive mode; a value of **enforcing=1** sets enforcing mode. You can also disable SELinux completely by passing on the kernel parameter **selinux=0**. A value of **selinux=1** enables SELinux.

### Initial SELinux Context

On systems running SELinux, all processes and files are labeled. The label represents the security relevant information, known as the SELinux context.

New files typically inherit their SELinux context from the parent directory, thus ensuring that they have the proper context.

But this inheritance procedure can be undermined in two different ways. First, if you create a file in a different location from the ultimate intended location and then move the file, the file still has the SELinux context of the directory where it was created, not the destination directory. Second, if you copy a file preserving the SELinux context, as with the **cp -a** command, the SELinux context reflects the location of the original file.

The following example demonstrates inheritance and its pitfalls. Consider these two files created in `/tmp`, one moved to `/var/www/html` and the second one copied to the same directory. Note the SELinux contexts on the files. The file that was moved to the `/var/www/html` directory retains the file context for the `/tmp` directory. The file that was copied to the `/var/www/html` directory inherited SELinux context from the `/var/www/html` directory.

The **ls -Z** command displays the SELinux context of a file. Note the label of the file.

```
[root@host ~]# ls -Z /var/www/html/index.html
-rw-r--r--. root root unconfined_u:object_r:httpd_sys_content_t:s0 /var/www/html/index.html  
```

And the **ls -Zd** command displays the SELinux context of a directory:

```
[root@host ~]# ls -Zd /var/www/html/
drwxr-xr-x. root root system_u:object_r:httpd_sys_content_t:s0 /var/www/html/ 
```

Note that the `/var/www/html/index.html` has the same label as the parent directory `/var/www/html/`. Now, create files outside of the `/var/www/html` directory and note their file context:

```
[root@host ~]# touch /tmp/file1 /tmp/file2
[root@host ~]# ls -Z /tmp/file*
unconfined_u:object_r:user_tmp_t:s0 /tmp/file1
unconfined_u:object_r:user_tmp_t:s0 /tmp/file2
```

Move one of these files to the `/var/www/html` directory, copy another, and note the label of each:

```
[root@host ~]# mv /tmp/file1 /var/www/html/
[root@host ~]# cp /tmp/file2 /var/www/html/
[root@host ~]# ls -Z /var/www/html/file*
unconfined_u:object_r:user_tmp_t:s0 /var/www/html/file1
unconfined_u:object_r:httpd_sys_content_t:s0 /var/www/html/file2  
```

The moved file maintains its original label while the copied file inherits the label from the `/var/www/html` directory. `unconfined_u:` is the user, `object_r:` denotes the role, and `s0` is the level. A sensitivity level of 0 is the lowest possible sensitivity level.

### Changing the SELinux context of a file

Commands to change the SELinux context on files include **semanage fcontext**, **restorecon**, and **chcon**.

The preferred method to set the SELinux context for a file is to declare the default labeling for a file using the **semanage fcontext** command and then applying that context to the file using the **restorecon** command. This ensures that the labeling will be as desired even after a complete relabeling of the file system.

The **chcon** command changes SELinux contexts. **chcon** sets the security context on the file, stored in the file system. It is useful for testing and experimenting. However, it does not save context changes in the SELinux context database. When a **restorecon** command runs, changes made by the **chcon** command also do not survive. Also, if the entire file system is relabeled, the SELinux context for files changed using **chcon** are reverted.

The following screen shows a directory being created. The directory has a type value of `default_t`.

```
[root@host ~]# mkdir /virtual
[root@host ~]# ls -Zd /virtual
drwxr-xr-x. root root unconfined_u:object_r:default_t:s0 /virtual
```

The **chcon** command changes the file context of the `/virtual` directory: the type value changes to `httpd_sys_content_t`.

```
[root@host ~]# chcon -t httpd_sys_content_t /virtual
[root@host ~]# ls -Zd /virtual
drwxr-xr-x. root root unconfined_u:object_r:httpd_sys_content_t:s0 /virtual 
```

The **restorecon** command runs and the type value returns to the value of `default_t`. Note the `Relabeled` message.

```
[root@host ~]# restorecon -v /virtual
Relabeled /virtual from unconfined_u:object_r:httpd_sys_content_t:s0 to unconfined_u:object_r:default_t:s0
[root@host ~]# ls -Zd /virtual
drwxr-xr-x. root root unconfined_u:object_r:default_t:s0 /virtual
```

### Defining SELinux Default File Context Rules

The **semanage fcontext** command displays and modifies the rules that **restorecon** uses to set default file contexts. It uses extended regular expressions to specify the path and file names. The most common extended regular expression used in `fcontext` rules is `(/.*)?`, which means “optionally, match a / followed by any number of characters”. It matches the directory listed before the expression and everything in that directory recursively.

**Basic File Context Operations**

The following table is a reference for **semanage fcontext** options to add, remove or list SELinux file contexts.



**Table 5.1. semanage fcontext commands**

| option           | description                                  |
| :--------------- | :------------------------------------------- |
| **-a, --add**    | Add a record of the specified object type    |
| **-d, --delete** | Delete a record of the specified object type |
| **-l, --list**   | List records of the specified object type    |



To ensure that you have the tools to manage SELinux contexts, install the `policycoreutil` package and the `policycoreutil-python` package if needed. These contain the **restorecon** command and **semanage** command, respectively.

To ensure that all files in a directory have the correct file context run the **semanage fcontext -l** followed by the **restorecon** command. In the following example, note the file context of each file before and after the **semanage** and **restorecon** commands run.

```
[root@host ~]# ls -Z /var/www/html/file*
unconfined_u:object_r:user_tmp_t:s0 /var/www/html/file1  unconfined_u:object_r:httpd_sys_content_t:s0 /var/www/html/file2  
[root@host ~]# semanage fcontext -l
...output omitted...
/var/www(/.*)?       all files    system_u:object_r:httpd_sys_content_t:s0
...output omitted... 
[root@host; ~]# restorecon -Rv /var/www/
Relabeled /var/www/html/file1 from unconfined_u:object_r:user_tmp_t:s0 to unconfined_u:object_r:httpd_sys_content_t:s0
[root@host ~]# ls -Z /var/www/html/file*
unconfined_u:object_r:httpd_sys_content_t:s0 /var/www/html/file1  unconfined_u:object_r:httpd_sys_content_t:s0 /var/www/html/file2
```

The following example shows how to use **semanage** to add a context for a new directory.

```
[root@host ~]# mkdir /virtual
[root@host ~]# touch /virtual/index.html
[root@host ~]# ls -Zd /virtual/
drwxr-xr-x. root root unconfined_u:object_r:default_t:s0 /virtual/ 
[root@host ~]# ls -Z /virtual/
-rw-r--r--. root root unconfined_u:object_r:default_t:s0 index.html
[root@host ~]# semanage fcontext -a -t httpd_sys_content_t '/virtual(/.*)?'
[root@host ~]# restorecon -RFvv /virtual
[root@host ~]# ls -Zd /virtual/
drwxr-xr-x. root root system_u:object_r:httpd_sys_content_t:s0 /virtual/
[root@host ~]# ls -Z /virtual/
-rw-r--r--. root root system_u:object_r:httpd_sys_content_t:s0 index.html
```



### SELinux booleans

SELinux booleans are switches that change the behavior of the SELinux policy. SELinux booleans are rules that can be enabled or disabled. They can be used by security administrators to tune the policy to make selective adjustments.

The SELinux man pages, provided with the *selinux-policy-doc* package, describe the purpose of the available booleans. The **man -k '_selinux'** command lists these man pages.

Commands useful for managing SELinux booleans include **getsebool**, which lists booleans and their state, and **setsebool** which modifies booleans. **setsebool -P** modifies the SELinux policy to make the modification persistent. And **semanage boolean -l** reports on whether or not a boolean is persistent, along with a short description of the boolean.

Non-privileged users can run the **getsebool** command, but you must be a superuser to run **semanage boolean -l** and **setsebool -P**.

```
[user@host ~]$ getsebool -a
abrt_anon_write --> off
abrt_handle_event --> off
abrt_upload_watch_anon_write --> on
antivirus_can_scan_system --> off
antivirus_use_jit --> off
...output omitted...
[user@host ~]$ getsebool httpd_enable_homedirs
httpd_enable_homedirs --> off
[user@host ~]$ setsebool httpd_enable_homedirs on
Could not change active booleans. Please try as root: Permission denied
[user@host ~]$ sudo setsebool httpd_enable_homedirs on
[user@host ~]$ sudo semanage boolean -l | grep httpd_enable_homedirs
httpd_enable_homedirs          (on   ,  off)  Allow httpd to enable homedirs
[user@host ~]$ getsebool httpd_enable_homedirs
httpd_enable_homedirs --> on
```

The `-P` option writes all pending values to the policy, making them persistent across reboots. In the example that follows, note the values in parentheses: both are now set to `on`.

```
[user@host ~]$ setsebool -P httpd_enable_homedirs on
[user@host ~]$ sudo semanage boolean -l | grep httpd_enable_homedirs
httpd_enable_homedirs          (on   ,   on)  Allow httpd to enable homedirs 
```

To list booleans in which the current state differs from the default state, run **semanage boolean -l -C**.

```
[user@host ~]$ sudo semanage boolean -l -C
SELinux boolean                State  Default Description

cron_can_relabel               (off   ,   on)  Allow cron to can relabel
```



### Troubleshooting SELinux Issues

It is important to understand what actions you must take when SELinux prevents access to files on a server that you know should be accessible. Use the following steps as a guide to troubleshooting these issues:

1. Before thinking of making any adjustments, consider that SELinux may be doing its job correctly by prohibiting the attempted access. If a web server tries to access files in `/home`, this could signal a compromise of the service if web content is not published by users. If access should have been granted, then additional steps need to be taken to solve the problem.
2. The most common SELinux issue is an incorrect file context. This can occur when a file is created in a location with one file context and moved into a place where a different context is expected. In most cases, running **restorecon** will correct the issue. Correcting issues in this way has a very narrow impact on the security of the rest of the system.
3. Another remedy for overly restrictive access could be the adjustment of a Boolean. For example, the `ftpd_anon_write` boolean controls whether anonymous FTP users can upload files. You must turn this boolean on to permit anonymous FTP users to upload files to a server. Adjusting booleans requires more care because they can have a broad impact on system security.



### Monitoring SELinux Violations

Install the *setroubleshoot-server* package to send SELinux messages to `/var/log/messages`. **setroubleshoot-server** listens for audit messages in `/var/log/audit/audit.log` and sends a short summary to `/var/log/messages`. This summary includes *unique identifiers (UUID)* for SELinux violations that can be used to gather further information. The **sealert -l UUID** command is used to produce a report for a specific incident. Use **sealert -a /var/log/audit/audit.log** to produce reports for all incidents in that file.

Consider the following sample sequence of commands on a standard Apache web server:

```
[root@host ~]# touch /root/file3
[root@host ~]# mv /root/file3 /var/www/html
[root@host ~]# systemctl start httpd
[root@host ~]# curl http://localhost/file3
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>403 Forbidden</title>
</head><body>
<h1>Forbidden</h1>
<p>You don't have permission to access /file3
on this server.</p>
</body></html>
```

You expect the web server to deliver the contents of `file3` but instead it returns a `permission denied` error. Inspecting both `/var/log/audit/audit.log` and `/var/log/messages` reveals extra information about this error.

```
[root@host ~]# tail /var/log/audit/audit.log
...output omitted...
type=AVC msg=audit(1392944135.482:429): avc:  denied  { getattr } for
  pid=1609 comm="httpd" path="/var/www/html/file3" dev="vda1" ino=8980981
  scontext=system_u:system_r:httpd_t:s0
  tcontext=unconfined_u:object_r:admin_home_t:s0 tclass=file
...output omitted...
[root@host ~]# tail /var/log/messages
...output omitted...
Feb 20 19:55:42 host setroubleshoot: SELinux is preventing /usr/sbin/httpd
  from getattr access on the file . For complete SELinux messages. run
  sealert -l 613ca624-248d-48a2-a7d9-d28f5bbe2763
```

Both log files indicate that an SELinux denial is the culprit. The **sealert** command that is part of the output in `/var/log/messages` provides extra information, including a possible fix.

```
[root@host ~]# sealert -l 613ca624-248d-48a2-a7d9-d28f5bbe2763
SELinux is preventing /usr/sbin/httpd from getattr access on the file .

*****  Plugin catchall (100. confidence) suggests   **************************

If you believe that httpd should be allowed getattr access on the
  file by default.
Then you should report this as a bug.
You can generate a local policy module to allow this access.
Do
allow this access for now by executing:
# grep httpd /var/log/audit/audit.log | audit2allow -M mypol
# semodule -i mypol.pp


Additional Information:
Source Context                system_u:system_r:httpd_t:s0
Target Context                unconfined_u:object_r:admin_home_t:s0
Target Objects                 [ file ]
Source                        httpd
Source Path                   /usr/sbin/httpd
Port                          <Unknown>
Host                          servera
Source RPM Packages           httpd-2.4.6-14.el7.x86_64
Target RPM Packages
Policy RPM                    selinux-policy-3.12.1-124.el7.noarch
Selinux Enabled               True
Policy Type                   targeted
Enforcing Mode                Enforcing
Host Name                     servera
Platform                      Linux servera 3.10.0-84.el7.x86_64 #1
                              SMP Tue Feb 4 16:28:19 EST 2014 x86_64 x86_64
Alert Count                   2
First Seen                    2014-02-20 19:55:35 EST
Last Seen                     2014-02-20 19:55:35 EST
Local ID                      613ca624-248d-48a2-a7d9-d28f5bbe2763

Raw Audit Messages
type=AVC msg=audit(1392944135.482:429): avc:  denied  { getattr } for
  pid=1609 comm="httpd" path="/var/www/html/file3" dev="vda1" ino=8980981
  scontext=system_u:system_r:httpd_t:s0
  tcontext=unconfined_u:object_r:admin_home_t:s0 tclass=file

type=SYSCALL msg=audit(1392944135.482:429): arch=x86_64 syscall=lstat
  success=no exit=EACCES a0=7f9fed0edea8 a1=7fff7bffc770 a2=7fff7bffc770
  a3=0 items=0 ppid=1608 pid=1609 auid=4294967295 uid=48 gid=48 euid=48
  suid=48 fsuid=48 egid=48 sgid=48 fsgid=48 tty=(none) ses=4294967295
  comm=httpd exe=/usr/sbin/httpd subj=system_u:system_r:httpd_t:s0 key=(null)

Hash: httpd,httpd_t,admin_home_t,file,getattr 
```

**NOTE**

The `Raw Audit Messages` section reveals the target file that is the problem, `/var/www/html/file3`. Also, the target context, `tcontext`, does not look like it belongs with a web server. Use the **restorecon /var/www/html/file3** command to fix the file context. If there are other files that need to be adjusted, **restorecon** can recursively reset the context: **restorecon -R /var/www/**.

The `Raw Audit Messages` section of the **sealert** command contains information from `/var/log/audit.log`. To search the `/var/log/audit.log` file use the **ausearch** command. The `-m` searches on the message type. The `-ts` option searches based on time.

```
[root@host ~]# ausearch -m AVC -ts recent
----
time->Tue Apr  9 13:13:07 2019
type=PROCTITLE msg=audit(1554808387.778:4002): proctitle=2F7573722F7362696E2F6874747064002D44464F524547524F554E44
type=SYSCALL msg=audit(1554808387.778:4002): arch=c000003e syscall=49 success=no exit=-13 a0=3 a1=55620b8c9280 a2=10 a3=7ffed967661c items=0 ppid=1 pid=9340 auid=4294967295 uid=0 gid=0 euid=0 suid=0 fsuid=0 egid=0 sgid=0 fsgid=0 tty=(none) ses=4294967295 comm="httpd" exe="/usr/sbin/httpd" subj=system_u:system_r:httpd_t:s0 key=(null)
type=AVC msg=audit(1554808387.778:4002): avc:  denied  { name_bind } for  pid=9340 comm="httpd" src=82 scontext=system_u:system_r:httpd_t:s0 tcontext=system_u:object_r:reserved_port_t:s0 tclass=tcp_socket permissive=0
```



### Summary

In this chapter, you learned:

- The **getenforce** and **setenforce** commands are used to manage the SELinux mode of a system.
- The **semanage** command is used to manage SELinux policy rules. The **restorecon** command applies the context defined by the policy.
- Booleans are switches that change the behavior of the SELinux policy. They can be enabled or disabled and are used to tune the policy.
- The **sealert** displays useful information to help with SELinux troubleshooting.



## Chapter 6. Tuning System Performance

**Table 6.1. Fundamental Process Management Signals**

| Signal number | Short name | Definition         | Purpose                                                      |
| :------------ | :--------- | :----------------- | :----------------------------------------------------------- |
| 1             | HUP        | Hangup             | Used to report termination of the controlling process of a terminal. Also used to request process reinitialization (configuration reload) without termination. |
| 2             | INT        | Keyboard interrupt | Causes program termination. Can be blocked or handled. Sent by pressing INTR key sequence (**Ctrl**+**c**). |
| 3             | QUIT       | Keyboard quit      | Similar to SIGINT; adds a process dump at termination. Sent by pressing QUIT key sequence (**Ctrl**+**\**). |
| 9             | KILL       | Kill, unblockable  | Causes abrupt program termination. Cannot be blocked, ignored, or handled; always fatal. |
| 15 *default*  | TERM       | Terminate          | Causes program termination. Unlike SIGKILL, can be blocked, ignored, or handled. The “polite” way to ask a program to terminate; allows self-cleanup. |
| 18            | CONT       | Continue           | Sent to a process to resume, if stopped. Cannot be blocked. Even if handled, always resumes the process. |
| 19            | STOP       | Stop, unblockable  | Suspends the process. Cannot be blocked or handled.          |
| 20            | TSTP       | Keyboard stop      | Unlike SIGSTOP, can be blocked, ignored, or handled. Sent by pressing SUSP key sequence (**Ctrl**+**z**). |

The **kill** command sends a signal to a process by PID number. Despite its name, the kill command can be used for sending any signal, not just those for terminating programs. You can use the **kill -l** command to list the names and numbers of all available signals.

```
[user@host ~]$ kill -l
 1) SIGHUP      2) SIGINT      3) SIGQUIT     4) SIGILL      5) SIGTRAP
 6) SIGABRT     7) SIGBUS      8) SIGFPE      9) SIGKILL    10) SIGUSR1
11) SIGSEGV    12) SIGUSR2    13) SIGPIPE    14) SIGALRM    15) SIGTERM
16) SIGSTKFLT  17) SIGCHLD    18) SIGCONT    19) SIGSTOP    20) SIGTSTP
```

Use **pkill** to send a signal to one or more processes which match selection criteria. Selection criteria can be a command name, a processes owned by a specific user, or all system-wide processes. The **pkill** command includes advanced selection criteria:

- Command - Processes with a pattern-matched command name.
- UID - Processes owned by a Linux user account, effective or real.
- GID - Processes owned by a Linux group account, effective or real.
- Parent - Child processes of a specific parent process.
- Terminal - Processes running on a specific controlling terminal.

#### Logging Users Out Administratively

All user login sessions are associated with a terminal device (TTY). If the device name is of the form `pts/*N*`, it is a *pseudo-terminal* associated with a graphical terminal window or remote login session. If it is of the form `tty*N*`, the user is on a system console, alternate console, or other directly connected terminal device.

```
[user@host ~]$ w
 12:43:06 up 27 min,  5 users,  load average: 0.03, 0.17, 0.66
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
root     tty2                      12:26   14:58   0.04s  0.04s -bash
bob      tty3                      12:28   14:42   0.02s  0.02s -bash
user     pts/1    desk.example.com 12:41    2.00s  0.03s  0.03s w
[user@host ~]$ 
```

First identify the PID numbers to be killed using **pgrep**, which operates much like **pkill**, including using the same options, except that **pgrep** lists processes rather than killing them.

```
[root@host ~]# pgrep -l -u bob
6964 bash
6998 sleep
6999 sleep
7000 sleep
[root@host ~]# pkill -SIGKILL -u bob
[root@host ~]# pgrep -l -u bob
```



### Installing and enabling tuned

A minimal Red Hat Enterprise Linux 8 installation includes and enables the *tuned* package by default. To install and enable the package manually:

```
[root@host ~]$ yum install tuned
[root@host ~]$ systemctl enable --now tuned
Created symlink /etc/systemd/system/multi-user.target.wants/tuned.service → /usr/lib/systemd/system/tuned.service.
```

### Managing profiles from the command line

The **tuned-adm** command is used to change settings of the `tuned` daemon. The **tuned-adm** command can query current settings, list available profiles, recommend a tuning profile for the system, change profiles directly, or turn off tuning.

A system administrator identifies the currently active tuning profile with **tuned-adm active**.

```
[root@host ~]# tuned-adm active
Current active profile: virtual-guest
```

The **tuned-adm list** command lists all available tuning profiles, including both built-in profiles and custom tuning profiles created by a system administrator.

```
[root@host ~]# tuned-adm list
Available profiles:
- balanced
- desktop
- latency-performance
- network-latency
- network-throughput
- powersave
- sap
- throughput-performance
- virtual-guest
- virtual-host
Current active profile: virtual-guest
```

Use **tuned-adm profile profilename** to switch the active profile to a different one that better matches the system's current tuning requirements.

```
[root@host ~]$ tuned-adm profile throughput-performance
[root@host ~]$ tuned-adm active
Current active profile: throughput-performance
```

The **tuned-adm** command can recommend a tuning profile for the system. This mechanism is used to determine the default profile of a system after installation.

```
[root@host ~]$ tuned-adm recommend
virtual-guest
```

**NOTE**

The **tuned-adm recommend** output is based on various system characteristics, including whether the system is a virtual machine and other predefined categories selected during system installation.

To revert the setting changes made by the current profile, either switch to another profile or deactivate the tuned daemon. Turn off `tuned` tuning activity with **tuned-adm off**.

```
[root@host ~]$ tuned-adm off
[root@host ~]$ tuned-adm active
No current active profile.
```



#### Influencing Process Scheduling

The nice level values range from -20 (highest priority) to 19 (lowest priority). By default, processes inherit their nice level from their parent, which is usually 0. Higher nice levels indicate less priority (the process easily gives up its CPU usage), while lower nice levels indicate a higher priority (the process is less inclined to give up the CPU)

**Displaying Nice Levels from the Command Line**

```
[user@host ~]$ ps axo pid,comm,nice,cls --sort=-nice
```

The **nice** command can be used by all users to start commands with a default or higher nice level.

Use the `-n` option to apply a user-defined nice level to the starting process. The default is to add 10 to the process' current nice level. The following example starts a command as a background job with a user-defined nice value and displays the process's nice level:

```
[user@host ~]$ nice -n 15 sha1sum &
[1] 3521
[user@host ~]$ ps -o pid,comm,nice 3521
  PID COMMAND          NI
 3521 sha1sum          15
```

#### Changing the Nice Level of an Existing Process

The nice level of an existing process can be changed using the **renice** command. This example uses the PID identifier from the previous example to change from the current nice level of 15 to the desired nice level of 19.

```
[user@host ~]$ renice -n 19 3521
3521 (process ID) old priority 15, new priority 19
```

The **top** command can also be used to change the nice level on a process. From within the **top** interactive interface, press the `r` option to access the **renice** command, followed by the PID to be changed and the new nice level.

### Summary

In this chapter, you learned:

- A signal is a software interrupt that reports events to an executing program. The **kill**, **pkill**, and **killall** commands use signals to control processes.
- The `tuned` service will automatically modify device settings to meet specific system needs based on a pre-defined selected tuning profile.
- To revert all changes made to the system settings by a selected profile, either switch to another profile or deactivate the `tuned` service.
- A relative priority is assigned to a process to determine its CPU access. This priority is called the nice value of a process.
- The **nice** command assigns a priority to a process when it starts. The **renice** command modifies the priority of a running process.



## Chapter 7. Installing and Updating Software Packages



### Registration from the Command Line

Use the **subscription-manager**(8) to register a system without using a graphical environment. The **subscription-manager** command can automatically attach a system to the best-matched compatible subscriptions for the system.

- Register a system to a Red Hat account:

  ```
  [user@host ~]$ subscription-manager register --username=yourusername \
  --password=yourpassword
  ```

- View available subscriptions:

  ```
  [user@host ~]$ subscription-manager list --available | less
  ```

- Auto-attach a subscription:

  ```
  [user@host ~]$ subscription-manager attach --auto
  ```

- Alternatively, attach a subscription from a specific pool from the list of available subscriptions:

  ```
  [user@host ~]$ subscription-manager attach --pool=poolID
  ```

- View consumed subscriptions:

  ```
  [user@host ~]$ subscription-manager list --consumed
  ```

- Unregister a system:

  ```
  [user@host ~]$ subscription-manager unregister
  ```



### Installing and Updating Software Packages with Yum

**Finding Software with Yum**

- **yum help** displays usage information.

- **yum list** displays installed and available packages.

  ```
  [user@host ~]$ yum list 'http*'
  Available Packages
  http-parser.i686              2.8.0-2.el8                        rhel8-appstream
  http-parser.x86_64            2.8.0-2.el8                        rhel8-appstream
  ```

* **yum search KEYWORD** lists packages by keywords found in the name and summary fields only.

  To search for packages that have “web server” in their name, summary, and description fields, use **search all**:

```
[user@host ~]$ yum search all 'web server'
================= Summary & Description Matched: web server ====================
pcp-pmda-weblog.x86_64 : Performance Co-Pilot (PCP) metrics from web server logs
nginx.x86_64 : A high performance web server and reverse proxy server
```

*  **yum info PACKAGENAME** returns detailed information about a package, including the disk space needed for installation.

  To get information on the Apache HTTP Server:

```
[user@host ~]$ yum info httpd
Available Packages
Name         : httpd
Version      : 2.4.37
```

*  **yum provides PATHNAME** displays packages that match the path name specified (which often include wildcard characters).

To find packages that provide the `/var/www/html` directory, use:

```
[user@host ~]$ yum provides /var/www/html
httpd-filesystem-2.4.37-7.module+el8+2443+605475b7.noarch : The basic directory layout for the Apache HTTP server
Repo        : rhel8-appstream
Matched from:
Filename    : /var/www/html
```



**Installing and removing groups of software with yum**

Like **yum list**, the **yum group list** command shows the names of installed and available groups.

```
[user@host ~]$ yum group list
Available Environment Groups:
   Server with GUI
   Minimal Install
```

- **yum group info** displays information about a group. It includes a list of mandatory, default, and optional package names.

  ```
  [user@host ~]$ yum group info "RPM Development Tools"
  Group: RPM Development Tools
   Description: These tools include core development tools such rpmbuild.
    Mandatory Packages:
      redhat-rpm-config
      rpm-build
    Default Packages:
      rpmdevtools
    Optional Packages:
      rpmlint
  ```

- **yum group install** installs a group that installs its mandatory and default packages and the packages they depend on.

  ```
  [user@host ~]$ sudo yum group install "RPM Development Tools"
  ```



**Viewing transaction history**

- All install and remove transactions are logged in `/var/log/dnf.rpm.log`.

  ```
  [user@host ~]$ tail -5 /var/log/dnf.rpm.log
  2019-02-26T18:27:00Z SUBDEBUG Installed: rpm-build-4.14.2-9.el8.x86_64
  ```

* The **history undo** option reverses a transaction.

```
[user@host ~]$ sudo yum history undo 5
Undoing transaction 7, from Tue 26 Feb 2019 10:40:32 AM EST
    Install apr-1.6.3-8.el8.x86_64                              @rhel8-appstream
```

#### Summary of Yum Commands

Packages can be located, installed, updated, and removed by name or by package groups.

| Task:                                         | Command:                        |
| :-------------------------------------------- | :------------------------------ |
| List installed and available packages by name | **yum list [NAME-PATTERN]**     |
| List installed and available groups           | **yum group list**              |
| Search for a package by keyword               | **yum search KEYWORD**          |
| Show details of a package                     | **yum info PACKAGENAME**        |
| Install a package                             | **yum install PACKAGENAME**     |
| Install a package group                       | **yum group install GROUPNAME** |
| Update all packages                           | **yum update**                  |
| Remove a package                              | **yum remove PACKAGENAME**      |
| Display transaction history                   | **yum history**                 |



#### Enabling Yum Software Repositories

To view all available repositories:

```
[user@host ~]$ yum repolist all
...output omitted...
rhel-8-for-x86_64-appstream-debug-rpms   ... AppStream (Debug RPMs)  disabled
rhel-8-for-x86_64-appstream-rpms         ... AppStream (RPMs)        enabled: 5,045
```

The **yum-config-manager** command can be used to enable or disable repositories. To enable a repository, the command sets the `enabled` parameter to `1`. For example, the following command enables the `rhel-8-server-debug-rpms` repository:

```
[user@host ~]$ yum-config-manager --enable rhel-8-server-debug-rpms
Updating Subscription Management repositories.
============= repo: rhel-8-for-x86_64-baseos-debug-rpms ============
[rhel-8-for-x86_64-baseos-debug-rpms]
bandwidth = 0
baseurl = [https://cdn.redhat.com/content/dist/rhel8/8/x86_64/baseos/debug]
cachedir = /var/cache/dnf
```

**Creating Yum Repositories**

Create Yum repositories with the **yum-config-manager** command. The following command creates a file named `/etc/yum.repos.d/dl.fedoraproject.org_pub_epel_8_x86_64_.repo` with the output shown.

```
[user@host ~]$ yum-config-manager --add-repo="http://dl.fedoraproject.org/pub/epel/8/x86_64/"
Loaded plugins: langpacks
adding repo from: http://dl.fedoraproject.org/pub/epel/8/x86_64/

[dl.fedoraproject.org_pub_epel_8_x86_64_]
name=added from: http://dl.fedoraproject.org/pub/epel/8/x86_64/
baseurl=http://dl.fedoraproject.org/pub/epel/8/x86_64/
enabled=1
```

To define a repository, but not search it by default, insert the `enabled=0` parameter. Repositories can be enabled and disabled persistently with **yum-config-manager** command or temporarily with **yum** command options, `--enablerepo=*PATTERN*` and `--disablerepo=*PATTERN*`.



### Introduction to Application Stream

Red Hat Enterprise Linux 8 using a new technology called *Modularity*. Modularity allows a single repository to host multiple versions of an application's package and its dependencies.

Red Hat Enterprise Linux 8 content is distributed through two main software repositories: *BaseOS* and *Application Stream (AppStream)*.

**BaseOS**

The BaseOS repository provides the core operating system content for Red Hat Enterprise Linux as RPM packages. BaseOS components have a life cycle identical to that of content in previous Red Hat Enterprise Linux releases.

**Application Stream**

The Application Stream repository provides content with varying life cycles as both modules and traditional packages. Application Stream contains necessary parts of the system, as well as a wide range of applications previously available as a part of Red Hat Software Collections and other products and programs.

**BaseOS**

The BaseOS repository provides the core operating system content for Red Hat Enterprise Linux as RPM packages. BaseOS components have a life cycle identical to that of content in previous Red Hat Enterprise Linux releases.

**Application Stream**

The Application Stream repository provides content with varying life cycles as both modules and traditional packages. Application Stream contains necessary parts of the system, as well as a wide range of applications previously available as a part of Red Hat Software Collections and other products and programs.

The Application Stream repository contains two types of content: *Modules* and traditional RPM packages. A module describes a set of RPM packages that belong together. Modules can contain several streams to make multiple versions of applications available for installation. Enabling a module stream gives the system access to the RPM packages within that module stream.

### Modules

A module is a set of RPM packages that are a consistent set that belong together. Typically, this is organized around a specific version of a software application or programming language. A typical module can contain packages with an application, packages with the application’s specific dependency libraries, packages with documentation for the application, and packages with helper utilities.

**Module Streams**

Each module can have one or more module streams, which hold different versions of the content. Each of the streams receives updates independently. Think of the module stream as a virtual repository in the Application Stream physical repository.

For each module, only one of its streams can be enabled and provide its packages.

**Module Profiles**

Each module can have one or more profiles. A profile is a list of certain packages to be installed together for a particular use-case such as for a server, client, development, minimal install, or other.

Installing a particular module profile simply installs a particular set of packages from the module stream. You can subsequently install or uninstall packages normally. If you do not specify a profile, the module will install its *default profile*.

**Listing Modules**

To display a list of available modules, use **yum module list**:

```
[user@host ~]$ yum module list
Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)
Name                    Stream        Profiles   Summary
389-ds                  1.4           default    389 Directory Server (base)
ant                     1.10 [d]      common [d] Java build tool
container-tools         1.0 [d]       common [d] Common tools and dependencies for container runtimes
```

To list the module streams for a specific module and retrieve their status:

```
[user@host ~]$ yum module list perl
Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)
Name  Stream       Profiles             Summary
perl  5.24         common [d], minimal  Practical Extraction and Report Language
```

To display details of a module:

```
[user@host ~]$ yum module info perl
Name             : perl
Stream           : 5.24
Version          : 820190207164249
Context          : ee766497
Profiles         : common [d], minimal
```

**NOTE**

Without specifying a module stream, **yum module info** shows list of packages installed by default profile of a module using the default stream. Use the *module-name:stream* format to view a specific module stream. Add the `--profile` option to display information about packages installed by each of the module's profiles. For example:

```
[user@host ~]$ yum module info --profile perl:5.24
```

**Enabling Module Streams and Installing Modules**

Module streams must be enabled in order to install their module. To simplify this process, when a module is installed it enables its module stream if necessary. Module streams can be enabled manually using **yum module enable** and providing the name of the module stream.

**IMPORTANT**

Only one module stream may be enabled for a given module. Enabling an additional module stream will disable the original module stream.

Install a module using the default stream and profiles:

```
[user@host ~]$ sudo yum module install perl
Dependencies resolved.
================================================================================
 Package         Arch   Version      Repository                            Size
================================================================================
Installing group/module packages:
 perl            x86_64 4:5.26.3-416.el8
                                     rhel-8-for-x86_64-appstream-htb-rpms  72 k
Installing dependencies:
...output omitted...
Running transaction
  Preparing        :                                                        1/1
  Installing       : perl-Exporter-5.72-396.el8.noarch                    1/155
  Installing       : perl-Carp-1.42-396.el8.noarch                        2/155
```

The same results could have been accomplished by running **yum install @perl**. The **@** notation informs **yum** that the argument is a module name instead of a package name.

To verify the status of the module stream and the installed profile:

```
[user@host ~]$ yum module list perl
Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)
Name  Stream       Profiles             Summary
perl  5.24         common, minimal      Practical Extraction and Report Language
perl  5.26 [d][e]  common [i], minimal  Practical Extraction and Report Language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
```

To remove an installed module:

```
[user@host ~]$ sudo yum module remove perl
Dependencies resolved.
================================================================================
 Package                        ArchVersion            Repository                            Size
================================================================================
Removing:
 perl                           x86_644:5.26.3-416.el8   @rhel-8.0-for-x86_64-appstream-rpms 0
Removing unused dependencies:
...output omitted...
Running transaction
Preparing        :                                                        1/1
Erasing          : perl-4:5.26.3-416.el8.x86_64                         1/155
Erasing          : perl-CPAN-2.18-397.el8.noarch                        2/155
...output omitted...
Removed:
perl-4:5.26.3-416.el8.x86_64
dwz-0.12-9.el8.x86_64
...output omitted...
Complete!
```

After the module is removed, the module stream is still enabled. To verify the module stream is still enabled:

```
[user@host ~]$ yum module list perl
Red Hat Enterprise Linux 8 for x86_64 - AppStream (RPMs)
Name  Stream       Profiles             Summary
perl  5.24         common [d], minimal  Practical Extraction and Report Language
perl  5.26 [d][e]   common [d], minimal  Practical Extraction and Report Language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
  
```

To disable the module stream:

```
[user@host ~]$ sudo yum module disable perl
  ...output omitted...
Dependencies resolved.
=================================================================================
 Package           Arch             Version              Repository         Size
=================================================================================
Disabling module streams:
 perl                               5.26
Is this ok [y/N]: y
Complete!
```



## Summary

In this chapter, you learned:

- Red Hat Subscription Management provides tools to entitle machines to product subscriptions, get updates to software packages, and track information about support contracts and subscriptions used by the systems.
- Software is provided as RPM packages, which make it easy to install, upgrade, and uninstall software from the system.
- The **rpm** command can be used to query a local database to provide information about the contents of installed packages and install downloaded package files.
- **yum** is a powerful command-line tool that can be used to install, update, remove, and query software packages.
- Red Hat Enterprise Linux 8 uses Application Streams to provide a single repository to host multiple versions of an application's packages and its dependencies.



## Chapter 8. Managing Basic Storage

The **lsblk -fp** command lists the full path of the device, along with the UUIDs and mount points, as well as the type of file system in the partition. If the file system is not mounted, the mount point will be blank.

Mount the file system by the UUID of the file system.

```
[root@host ~]# mount UUID="46f543fd-78c9-4526-a857-244811be2d88" /mnt/data
```

**MBR Partitioning Scheme**

Since 1982, the *Master Boot Record (MBR)* partitioning scheme has dictated how disks are partitioned on systems running BIOS firmware. This scheme supports a maximum of four primary partitions. On Linux systems, with the use of extended and logical partitions, administrators can create a maximum of 15 partitions. Because partition size data is stored as 32-bit values, disks partitioned with the MBR scheme have a maximum disk and partition size of 2 TiB.

**GPT Partitioning Scheme**

For systems running *Unified Extensible Firmware Interface (UEFI)* firmware, GPT is the standard for laying out partition tables on physical hard disks. GPT is part of the UEFI standard and addresses many of the limitations that the old MBR-based scheme imposes.

A GPT provides a maximum of 128 partitions. Unlike an MBR, which uses 32 bits for storing logical block addresses and size information, a GPT allocates 64 bits for logical block addresses. This allows a GPT to accommodate partitions and disks of up to eight zebibytes (ZiB) or eight billion tebibytes.



### Managing Partitions with Parted

The **parted** command takes the device name of the whole disk as the first argument and one or more subcommands. The following example uses the **print** subcommand to display the partition table on the `/dev/vda` disk.

```
[root@host ~]# parted /dev/vda print
Model: Virtio Block Device (virtblk)
Disk /dev/vda: 53.7GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  10.7GB  10.7GB  primary  xfs          boot
 2      10.7GB  53.7GB  42.9GB  primary  xfs
```

If you do not provide a subcommand, **parted** opens an interactive session for issuing commands.

By default, **parted** displays all the sizes in powers of 10 (KB, MB, GB). You can change that default with the **unit** subcommand which accepts the following parameters:

- `s` for sector
- `B` for byte
- `MiB`, `GiB`, or `TiB` (powers of 2)
- `MB`, `GB`, or `TB` (powers of 10)

```
[root@host ~]# parted /dev/vda unit s print
Model: Virtio Block Device (virtblk)
Disk /dev/vda: 104857600s
Sector size (logical/physical): 512B/512B
```

**Writing the Partition Table on a New Disk**

To partition a new drive, you first have to write a disk label to it. The disk label indicates which partitioning scheme to use.

As the `root` user, use the following command to write an MBR disk label to a disk.

```
[root@host ~]# parted /dev/vdb mklabel msdos
```

To write a GPT disk label, use the following command.

```
[root@host ~]# parted /dev/vdb mklabel gpt
```

**Creating MBR Partitions**

```
[root@host ~]# parted /dev/vdb mkpart primary xfs 2048s 1000MB
```

**Creating GPT Partitions**

```
[root@host ~]# parted /dev/vdb mkpart usersdata xfs 2048s 1000MB
```

**Deleting Partitions**

```
[root@host ~]# parted /dev/vdb
(parted) print
Number  Start   End     Size   File system  Name       Flags
 1      1049kB  1000MB  999MB  xfs          usersdata
(parted) rm 1
```

**Persistently Mounting File Systems**

```
[root@host ~]# cat /etc/fstab
UUID=7a20315d-ed8b-4e75-a5b6-24ff9e1f9838   /dbdata  xfs   defaults   0 0
```

The *first field* specifies the device. This example uses the UUID to specify the device. File systems create and store the UUID in their super block at creation time. Alternatively, you could use the device file, such as `/dev/vdb1`.

The *second field* is the directory mount point, from which the block device will be accessible in the directory structure. The mount point must exist; if not, create it with the **mkdir** command.

The *third field* contains the file-system type, such as `xfs` or `ext4`.

The *fourth field* is the comma-separated list of options to apply to the device. `defaults` is a set of commonly used options. The **mount**(8) man page documents the other available options.

The *fifth field* is used by the **dump** command to back up the device. Other backup applications do not usually use this field.

The *last field*, the **fsck** order field, determines if the **fsck** command should be run at system boot to verify that the file systems are clean. The value in this field indicates the order in which **fsck** should run. For XFS file systems, set this field to `0` because XFS does not use **fsck** to check its file-system status. For ext4 file systems, set it to `1` for the root file system and `2` for the other ext4 file systems. This way, **fsck** processes the root file system first and then checks file systems on separate disks concurrently, and file systems on the same disk in sequence.

**(!) Important**

When you add or remove an entry in the `/etc/fstab` file, run the **systemctl daemon-reload** command, or reboot the server, for `systemd` to register the new configuration.

```
[root@host ~]# systemctl daemon-reload
```

### NOTE

Having an incorrect entry in `/etc/fstab` may render the machine non-bootable. Administrators should verify that the entry is valid by unmounting the new file system and using **mount /mountpoint**, which reads `/etc/fstab`, to remount the file system. If the **mount** command returns an error, correct it before rebooting the machine.

As an alternative, you can use the **findmnt --verify** command to control the `/etc/fstab` file.



### Managing Swap Space

**Formatting the Device**

The **mkswap** command applies a swap signature to the device. Unlike other formatting utilities, **mkswap** writes a single block of data at the beginning of the device, leaving the rest of the device unformatted so the kernel can use it for storing memory pages.

```
[root@host ~]# mkswap /dev/vdb2
Setting up swapspace version 1, size = 244 MiB (255848448 bytes)
no label, UUID=39e2667a-9458-42fe-9665-c5c854605881
```

Use **swapon** with the device as a parameter, or use **swapon -a** to activate all the swap spaces listed in the `/etc/fstab` file. Use the **swapon --show** and **free** commands to inspect the available swap spaces.

```
[root@host ~]# swapon /dev/vdb2
```

**Activating Swap Space Persistently**

```
UUID=39e2667a-9458-42fe-9665-c5c854605881   swap   swap   defaults   0 0
```

**Setting the Swap Space Priority**

By default, the system uses swap spaces in series, meaning that the kernel uses the first activated swap space until it is full, then it starts using the second swap space. However, you can define a priority for each swap space to force that order.

To set the priority, use the `pri` option in `/etc/fstab`. The kernel uses the swap space with the highest priority first. The default priority is -2.

The following example shows three swap spaces defined in `/etc/fstab`. The kernel uses the last entry first, with `pri=10`. When that space is full, it uses the second entry, with `pri=4`. Finally, it uses the first entry, which has a default priority of -2.

```
UUID=af30cbb0-3866-466a-825a-58889a49ef33   swap   swap   defaults  0 0
UUID=39e2667a-9458-42fe-9665-c5c854605881   swap   swap   pri=4     0 0
UUID=fbd7fa60-b781-44a8-961b-37ac3ef572bf   swap   swap   pri=10    0 0
```

Use **swapon --show** to display the swap space priorities.

When swap spaces have the same priority, the kernel writes to them in a round-robin fashion.

## Summary

In this chapter, you learned:

- The **mount** command allows the root user to manually mount a file system.
- **parted** can be used to add, modify, and remove partitions on disks with the MBR or the GPT partitioning scheme.
- XFS file systems are created on disk partitions using **mkfs.xfs**.
- To make file system mounts persistent, they must be added to `/etc/fstab`.
- Swap spaces are initialized using the **mkswap** command.



## Chapter 9. Controlling Services and the Boot Process

##### **Listing Service Units**

You use the **systemctl** command to explore the current state of the system. For example, the following command lists all currently loaded service units, paginating the output using **less**.

```
[root@host ~]# systemctl list-units --type=service
UNIT                               LOAD   ACTIVE SUB     DESCRIPTION
atd.service                        loaded active running Job spooling tools
auditd.service                     loaded active running Security Auditing Service
```

##### **Columns in the systemctl list-units Command Output**

- `UNIT`

  The service unit name.

- `LOAD`

  Whether `systemd` properly parsed the unit's configuration and loaded the unit into memory.

- `ACTIVE`

  The high-level activation state of the unit. This information indicates whether the unit has started successfully or not.

- `SUB`

  The low-level activation state of the unit. This information indicates more detailed information about the unit. The information varies based on unit type, state, and how the unit is executed.

- `DESCRIPTION`

  The short description of the unit.

By default, the **systemctl list-units --type=service** command lists only the service units with `active` activation states. The `--all` option lists all service units regardless of the activation states. Use the `--state=` option to filter by the values in the `LOAD`, `ACTIVE`, or `SUB` fields.

```
[root@host ~]# systemctl list-units --type=service --all
UNIT                          LOAD      ACTIVE   SUB     DESCRIPTION
  atd.service                 loaded    active   running Job spooling tools
  auditd.service              loaded    active   running Security Auditing ...
  auth-rpcgss-module.service  loaded    inactive dead    Kernel Module ...
```

The **systemctl** command without any arguments lists units that are both loaded and active.

```
[root@host ~]# systemctl
UNIT                                 LOAD   ACTIVE SUB       DESCRIPTION
proc-sys-fs-binfmt_misc.automount    loaded active waiting   Arbitrary...
sys-devices-....device               loaded active plugged   Virtio network...
```

##### **Viewing Service States**

View the status of a specific unit with **systemctl status name.type**. If the unit type is not provided, **systemctl** will show the status of a service unit, if one exists.

```
[root@host ~]# systemctl status sshd.service
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
```

##### Verifying the Status of a Service

The **systemctl** command provides methods for verifying the specific states of a service. For example, use the following command to verify that the a service unit is currently active (running):

```
[root@host ~]# systemctl is-active sshd.service
active
```

The command returns state of the service unit, which is usually `active` or `inactive`.

Run the following command to verify whether a service unit is enabled to start automatically during system boot:

```
[root@host ~]# systemctl is-enabled sshd.service
enabled
```

The command returns whether the service unit is enabled to start at boot time, which is usually `enabled` or `disabled`.

To verify whether the unit failed during startup, run the following command:

```
[root@host ~]# systemctl is-failed sshd.service
active
```

The command either returns `active` if it is properly running or `failed` if an error has occurred during startup. In case the unit was stopped, it returns `unknown` or `inactive`.

To list all the failed units, run the **systemctl --failed --type=service** command.

##### Starting and Stopping Services

To start a service, first verify that it is not running with **systemctl status**. Then, use the **systemctl start** command as the `root` user (using **sudo** if necessary). The example below shows how to start the `sshd.service` service:

```
[root@host ~]# systemctl start sshd.service
```

To stop a currently running service, use the `stop` argument with the **systemctl** command. The example below shows how to stop the `sshd.service` service:

```
[root@host ~]# systemctl stop sshd.service
```

##### Restarting and Reloading Services

During a restart of a running service, the service is stopped and then started. On the restart of service, the process ID changes and a new process ID gets associated during the startup. To restart a running service, use the `restart` argument with the **systemctl** command. The example below shows how to restart the `sshd.service` service:

```
[root@host ~]# systemctl restart sshd.service
```

Some services have the ability to reload their configuration files without requiring a restart. This process is called a *service reload*. Reloading a service does not change the process ID associated with various service processes. To reload a running service, use the `reload` argument with the **systemctl** command. The example below shows how to reload the `sshd.service` service after configuration changes:

```
[root@host ~]# systemctl reload sshd.service
```

##### Listing Unit Dependencies

The **systemctl list-dependencies UNIT** command displays a hierarchy mapping of dependencies to start the service unit. To list reverse dependencies (units that depend on the specified unit), use the `--reverse` option with the command.

```
[root@host ~]# systemctl list-dependencies sshd.service
sshd.service
● ├─system.slice
● ├─sshd-keygen.target
● │ ├─sshd-keygen@ecdsa.service
● │ ├─sshd-keygen@ed25519.service
● │ └─sshd-keygen@rsa.service
● └─sysinit.target
...output omitted...
```

##### Masking and Unmasking Services

At times, a system may have different services installed that are conflicting with each other. For example, there are multiple methods to manage mail servers (`postfix` and `sendmail`, for example). Masking a service prevents an administrator from accidentally starting a service that conflicts with others. Masking creates a link in the configuration directories to the `/dev/null` file which prevents the service from starting.

```
[root@host ~]# systemctl mask sendmail.service
Created symlink /etc/systemd/system/sendmail.service → /dev/null.
[root@host ~]# systemctl list-unit-files --type=service
UNIT FILE                                   STATE
...output omitted...
sendmail.service                            masked
...output omitted...
```

Attempting to start a masked service unit fails with the following output:

```
[root@host ~]# systemctl start sendmail.service
Failed to start sendmail.service: Unit sendmail.service is masked.
```

Use the **systemctl unmask** command to unmask the service unit.

```
[root@host ~]# systemctl unmask sendmail
Removed /etc/systemd/system/sendmail.service.
```



**Table 9.3. Useful Service Management Commands**

| Task                                                         | Command                              |
| :----------------------------------------------------------- | :----------------------------------- |
| View detailed information about a unit state.                | **systemctl status UNIT**            |
| Stop a service on a running system.                          | **systemctl stop UNIT**              |
| Start a service on a running system.                         | **systemctl start UNIT**             |
| Restart a service on a running system.                       | **systemctl restart UNIT**           |
| Reload the configuration file of a running service.          | **systemctl reload UNIT**            |
| Completely disable a service from being started, both manually and at boot. | **systemctl mask UNIT**              |
| Make a masked service available.                             | **systemctl unmask UNIT**            |
| Configure a service to start at boot time.                   | **systemctl enable UNIT**            |
| Disable a service from starting at boot time.                | **systemctl disable UNIT**           |
| List units required and wanted by the specified unit.        | **systemctl list-dependencies UNIT** |



##### Describing the Red Hat Enterprise Linux 8 Boot Process

Comment: **NOT CRITICAL** for the exam

Modern computer systems are complex combinations of hardware and software. Starting from an undefined, powered-down state to a running system with a login prompt requires a large number of pieces of hardware and software to work together. The following list gives a high-level overview of the tasks involved for a physical `x86_64` system booting Red Hat Enterprise Linux 8. The list for `x86_64` virtual machines is roughly the same, but the hypervisor handles some of the hardware-specific steps in software.

- The machine is powered on. The system firmware, either modern UEFI or older BIOS, runs a *Power On Self Test (POST)* and starts to initialize some of the hardware.

  Configured using the system BIOS or UEFI configuration screens that you typically reach by pressing a specific key combination, such as **F2**, early during the boot process.

- The system firmware searches for a bootable device, either configured in the UEFI boot firmware or by searching for a *Master Boot Record (MBR)* on all disks, in the order configured in the BIOS.

  Configured using the system BIOS or UEFI configuration screens that you typically reach by pressing a specific key combination, such as **F2**, early during the boot process.

- The system firmware reads a boot loader from disk and then passes control of the system to the boot loader. On a Red Hat Enterprise Linux 8 system, the boot loader is the *GRand Unified Bootloader version 2 (GRUB2)*.

  Configured using the **grub2-install** command, which installs GRUB2 as the boot loader on the disk.

- GRUB2 loads its configuration from the `/boot/grub2/grub.cfg` file and displays a menu where you can select which kernel to boot.

  Configured using the `/etc/grub.d/` directory, the `/etc/default/grub` file, and the **grub2-mkconfig** command to generate the `/boot/grub2/grub.cfg` file.

- After you select a kernel, or the timeout expires, the boot loader loads the kernel and *initramfs* from disk and places them in memory. An `initramfs` is an archive containing the kernel modules for all the hardware required at boot, initialization scripts, and more. On Red Hat Enterprise Linux 8, the `initramfs` contains an entire usable system by itself.

  Configured using the `/etc/dracut.conf.d/` directory, the **dracut** command, and the **lsinitrd** command to inspect the `initramfs` file.

- The boot loader hands control over to the kernel, passing in any options specified on the kernel command line in the boot loader, and the location of the `initramfs` in memory.

  Configured using the `/etc/grub.d/` directory, the `/etc/default/grub` file, and the **grub2-mkconfig** command to generate the `/boot/grub2/grub.cfg` file.

- The kernel initializes all hardware for which it can find a driver in the `initramfs`, then executes **/sbin/init** from the `initramfs` as PID 1. On Red Hat Enterprise Linux 8, `/sbin/init` is a link to `systemd`.

  Configured using the kernel `init=` command-line parameter.

- The `systemd` instance from the `initramfs` executes all units for the `initrd.target` target. This includes mounting the root file system on disk on to the `/sysroot` directory.

  Configured using `/etc/fstab`

- The kernel switches (pivots) the root file system from `initramfs` to the root file system in `/sysroot`. `systemd` then re-executes itself using the copy of `systemd` installed on the disk.

- `systemd` looks for a default target, either passed in from the kernel command line or configured on the system, then starts (and stops) units to comply with the configuration for that target, solving dependencies between units automatically. In essence, a `systemd` target is a set of units that the system should activate to reach the desired state. These targets typically start a text-based login or a graphical login screen.

  Configured using `/etc/systemd/system/default.target` and `/etc/systemd/system/`.

##### Selecting a Systemd Target

A `systemd` target is a set of `systemd` units that the system should start to reach a desired state. The following table lists the most important targets.



**Table 9.4. Commonly Used Targets**

| Target              | Purpose                                                      |
| :------------------ | :----------------------------------------------------------- |
| `graphical.target`  | System supports multiple users, graphical- and text-based logins. |
| `multi-user.target` | System supports multiple users, text-based logins only.      |
| `rescue.target`     | **sulogin** prompt, basic system initialization completed.   |
| `emergency.target`  | **sulogin** prompt, `initramfs` pivot complete, and system root mounted on `/` read only. |

To list the available targets, use the following command.

```
[user@host ~]$ systemctl list-units --type=target --all
  UNIT                      LOAD      ACTIVE   SUB    DESCRIPTION
  ---------------------------------------------------------------------------
  basic.target              loaded    active   active Basic System
  cryptsetup.target         loaded    active   active Local Encrypted Volumes
  emergency.target          loaded    inactive dead   Emergency Mode
  getty-pre.target          loaded    inactive dead   Login Prompts (Pre)
  getty.target              loaded    active   active Login Prompts
  graphical.target          loaded    inactive dead   Graphical Interface
...output omitted...
```

##### **Selecting a Target at Runtime**

On a running system, administrators can switch to a different target using the **systemctl isolate** command.

```
[root@host ~]# systemctl isolate multi-user.target
```

Isolating a target stops all services not required by that target (and its dependencies), and starts any required services not yet started.

Not all targets can be isolated. You can only isolate targets that have `AllowIsolate=yes` set in their unit files. For example, you can isolate the graphical target, but not the cryptsetup target.

```
[user@host ~]$ systemctl cat graphical.target
AllowIsolate=yes
[user@host ~]$ systemctl cat cryptsetup.target
```

##### **Setting a Default Target**

When the system starts, `systemd` activates the `default.target` target. Normally the default target in `/etc/systemd/system/` is a symbolic link to either `graphical.target` or `multi-user.target`. Instead of editing this symbolic link by hand, the **systemctl** command provides two subcommands to manage this link: **get-default** and **set-default**.

```
[root@host ~]# systemctl get-default
multi-user.target
[root@host ~]# systemctl set-default graphical.target
Removed /etc/systemd/system/default.target.
Created symlink /etc/systemd/system/default.target -> /usr/lib/systemd/system/graphical.target.
[root@host ~]# systemctl get-default
graphical.target
```

##### **Selecting a Different Target at Boot Time**

To select a different target at boot time, append the `systemd.unit=*target*.target` option to the kernel command line from the boot loader.

For example, to boot the system into a rescue shell where you can change the system configuration with almost no services running, append the following option to the kernel command line from the boot loader.

```
systemd.unit=rescue.target
```

This configuration change only affects a single boot, making it a useful tool for troubleshooting the boot process.

To use this method of selecting a different target, use the following procedure:

1. Boot or reboot the system.
2. Interrupt the boot loader menu countdown by pressing any key (except **Enter** which would initiate a normal boot).
3. Move the cursor to the kernel entry that you want to start.
4. Press **e** to edit the current entry.
5. Move the cursor to the line that starts with `linux`. This is the kernel command line.
6. Append `systemd.unit=*target*.target`. For example, `systemd.unit=emergency.target`.
7. Press **Ctrl**+**x** to boot with these changes.



##### Resetting the Root Password from the Boot Loader

To access that `root` shell, follow these steps:

1. Reboot the system.
2. Interrupt the boot loader countdown by pressing any key, except **Enter**.
3. Move the cursor to the kernel entry to boot.
4. Press **e** to edit the selected entry.
5. Move the cursor to the kernel command line (the line that starts with `linux`).
6. Append `rd.break console=tty0`. With that option, the system breaks just before the system hands control from the `initramfs` to the actual system.
7. Press **Ctrl**+**x** to boot with the changes.

At this point, the system presents a `root` shell, with the actual root file system on the disk mounted read-only on `/sysroot`. Because troubleshooting often requires modification to the root file system, you need to change the root file system to read/write. The following step shows how the `remount,rw` option to the **mount** command remounts the file system with the new option (`rw`) set.

To reset the `root` password from this point, use the following procedure:

1. Remount `/sysroot` as read/write.

   ```
   switch_root:/# mount -o remount,rw /sysroot
   ```

2. Switch into a **chroot** jail, where `/sysroot` is treated as the root of the file-system tree.

   ```
   switch_root:/# chroot /sysroot
   ```

3. Set a new `root` password.

   ```
   sh-4.4# passwd root
   ```

4. Configure the system to automatically perform a full SELinux relabel after boot. This is necessary because the **passwd** command recreates the `/etc/shadow` file without an SELinux context.

   ```
   sh-4.4# touch /.autorelabel
   ```

5. Type **exit** twice. The first command exits the **chroot** jail, and the second command exits the `initramfs` debug shell.

At this point, the system continues booting, performs a full SELinux relabel, and then reboots again.



##### Inspecting Logs

Looking at the logs of previously failed boots can be useful. If the system journals are persistent across reboots, you can use the **journalctl** tool to inspect those logs.

Remember that by default, the system journals are kept in the `/run/log/journal` directory, which means the journals are cleared when the system reboots. To store journals in the `/var/log/journal` directory, which persists across reboots, set the `Storage` parameter to `persistent` in `/etc/systemd/journald.conf`.

```
[root@host ~]# vim /etc/systemd/journald.conf
...output omitted...
[Journal]
Storage=persistent
...output omitted...
[root@host ~]# systemctl restart systemd-journald.service
```

To inspect the logs of a previous boot, use the `-b` option of **journalctl**. Without any arguments, the `-b` option only displays messages since the last boot. With a negative number as an argument, it displays the logs of previous boots.

```
[root@host ~]# journalctl -b -1 -p err
```

This command shows all messages rated as an error or worse from the previous boot.

##### **Using the Emergency and Rescue Targets**

By appending either `systemd.unit=rescue.target` or `systemd.unit=emergency.target` to the kernel command line from the boot loader, the system spawns into a rescue or emergency shell instead of starting normally. Both of these shells require the `root` password.



##### Diagnosing and Fixing File System Issues

Errors in `/etc/fstab` and corrupt file systems can stop a system from booting. In most cases, `systemd` drops to an emergency repair shell that requires the `root` password.

The following table lists some common errors and their results.



**Table 9.5. Common File System Issues**

| Problem                                               | Result                                                       |
| :---------------------------------------------------- | :----------------------------------------------------------- |
| Corrupt file system                                   | `systemd` attempts to repair the file system. If the problem is too severe for an automatic fix, the system drops the user to an emergency shell. |
| Nonexistent device or UUID referenced in `/etc/fstab` | `systemd` waits for a set amount of time, waiting for the device to become available. If the device does not become available, the system drops the user to an emergency shell after the timeout. |
| Nonexistent mount point in `/etc/fstab`               | The system drops the user to an emergency shell.             |
| Incorrect mount option specified in `/etc/fstab`      | The system drops the user to an emergency shell.             |

In all cases, administrators can also use the emergency target to diagnose and fix the issue, because no file systems are mounted before the emergency shell is displayed.

**NOTE**

When using the emergency shell to address file-system issues, do not forget to run **systemctl daemon-reload** after editing `/etc/fstab`. Without this reload, `systemd` may continue using the old version.

The `nofail` option in an entry in the `/etc/fstab` file permits the system to boot even if the mount of that file system is not successful. *Do not* use this option under normal circumstances. With `nofail`, an application can start with its storage missing, with possibly severe consequences.



##### Summary

In this chapter, you learned:

- Use the **systemctl status** command to determine the status of system daemons and network services started by `systemd`.
- The **systemctl list-dependencies** command lists all service units upon which a specific service unit depends.
- **systemctl reboot** and **systemctl poweroff** reboot and power down a system, respectively.
- **systemctl isolate desired.target** switches to a new target at runtime.
- **systemctl get-default** and **systemctl set-default** can be used to query and set the default target.
- Use `rd.break` on the kernel command-line to interrupt the boot process before control is handed over from the `initramfs`. The root file system is mounted read-only under `/sysroot`.
- The `emergency.target` target can be used to diagnose and fix file system issues.



## Chapter 10. Managing Networking

##### **Identifying Network Interfaces**

The **ip link** command will list all network interfaces available on your system:

```
[user@host ~]$ ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: ens3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:00:00:0a brd ff:ff:ff:ff:ff:ff
3: ens4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:00:00:1e brd ff:ff:ff:ff:ff:ff
```

In the preceding example, the server has three network interfaces: `lo`, which is the loopback device that is connected to the server itself, and two Ethernet interfaces, `ens3` and `ens4`.

##### **Displaying IP Addresses**

Use the **ip** command to view device and address information. A single network interface can have multiple IPv4 or IPv6 addresses.

```
[user@host ~]$ ip addr show ens3
```

##### **Displaying Performance Statistics**

The **ip** command may also be used to show statistics about network performance. Counters for each network interface can be used to identify the presence of network issues. The counters record statistics for things like the number of received (RX) and transmitted (TX) packets, packet errors, and packets that were dropped.

```
[user@host ~]$ ip -s link show ens3
```

##### **Displaying Performance Statistics**

The **ip** command may also be used to show statistics about network performance. Counters for each network interface can be used to identify the presence of network issues. The counters record statistics for things like the number of received (RX) and transmitted (TX) packets, packet errors, and packets that were dropped.

```
[user@host ~]$ ip -s link show ens3
```

##### **Displaying the Routing Table**

Use the **ip** command with the `route` option to show routing information.

```
[user@host ~]$ ip route
default via 192.0.2.254 dev ens3 proto static metric 1024
192.0.2.0/24 dev ens3 proto kernel scope link src 192.0.2.2
10.0.0.0/8 dev ens4 proto kernel scope link src 10.0.0.11
```

This shows the IPv4 routing table. All packets destined for the `10.0.0.0/8` network are sent directly to the destination through the device `ens4`. All packets destined for the `192.0.2.0/24` network are sent directly to the destination through the device `ens3`. All other packets are sent to the default router located at `192.0.2.254`, and also through device `ens3`.

Add the `-6` option to show the IPv6 routing table:

```
[user@host ~]$ ip -6 route
unreachable ::/96 dev lo  metric 1024  error -101
unreachable ::ffff:0.0.0.0/96 dev lo  metric 1024  error -101
2001:db8:0:1::/64 dev ens3  proto kernel  metric 256
unreachable 2002:a00::/24 dev lo  metric 1024  error -101
```

##### **Tracing Routes Taken by Traffic**

To trace the path that network traffic takes to reach a remote host through multiple routers, use either **traceroute** or **tracepath**. This can identify whether an issue is with one of your routers or an intermediate one. Both commands use UDP packets to trace a path by default; however, many networks block UDP and ICMP traffic. The **traceroute** command has options to trace the path with UDP (default), ICMP (**-I**), or TCP (**-T**) packets. Typically, however, the **traceroute** command is not installed by default.

```
[user@host ~]$ tracepath access.redhat.com
...output omitted...
 4:  71-32-28-145.rcmt.qwest.net                          48.853ms asymm  5
 5:  dcp-brdr-04.inet.qwest.net                          100.732ms asymm  7
```

Each line in the output of **tracepath** represents a router or *hop* that the packet passes through between the source and the final destination. Additional information is provided as available, including the *round trip timing (RTT)* and any changes in the *maximum transmission unit (MTU)* size. The `asymm` indication means traffic reached that router and returned from that router using different (*asymmetric*) routes. The routers shown are the ones used for outbound traffic, not the return traffic.

The **tracepath6** and **traceroute -6** commands are the equivalent to **tracepath** and **traceroute** for IPv6.

```
[user@host ~]$ tracepath6 2001:db8:0:2::451
 1?: [LOCALHOST]                        0.091ms pmtu 1500
```

##### Troubleshooting ports and services

TCP services use sockets as end points for communication and are made up of an IP address, protocol, and port number. Services typically listen on standard ports while clients use a random available port. Well-known names for standard ports are listed in the `/etc/services` file.

The **ss** command is used to display socket statistics. The **ss** command is meant to replace the older tool **netstat**, part of the *net-tools* package, which may be more familiar to some system administrators but which is not always installed.

```
[user@host ~]$ ss -ta
State      Recv-Q Send-Q      Local Address:Port          Peer Address:Port
```

**Table 10.1. Options for ss and netstat**

| Option    | Description                                                  |
| :-------- | :----------------------------------------------------------- |
| `-n`      | Show numbers instead of names for interfaces and ports.      |
| `-t`      | Show TCP sockets.                                            |
| `-u`      | Show UDP sockets.                                            |
| `-l`      | Show only listening sockets.                                 |
| `-a`      | Show all (listening and established) sockets.                |
| `-p`      | Show the process using the sockets.                          |
| `-A inet` | Display active connections (but not listening sockets) for the `inet` address family. That is, ignore local UNIX domain sockets.For **ss**, both IPv4 and IPv6 connections are displayed. For **netstat**, only IPv4 connections are displayed. (**netstat -A inet6** displays IPv6 connections, and **netstat -46** displays IPv4 and IPv6 at the same time.) |

##### Viewing Networking Information

The **nmcli** utility is used to create and edit connection files from the command line.

The **nmcli dev status** command displays the status of all network devices:

```
[user@host ~]$ nmcli dev status
DEVICE  TYPE      STATE         CONNECTION
eno1    ethernet  connected     eno1
ens3    ethernet  connected     static-ens3
eno2    ethernet  disconnected  --
lo      loopback  unmanaged     --
```

The **nmcli con show** command displays a list of all connections. To list only the active connections, add the `--active` option.

```
[user@host ~]$ nmcli con show
NAME         UUID                                  TYPE            DEVICE
eno2         ff9f7d69-db83-4fed-9f32-939f8b5f81cd  802-3-ethernet  --
static-ens3  72ca57a2-f780-40da-b146-99f71c431e2b  802-3-ethernet  ens3
eno1         87b53c56-1f5d-4a29-a869-8a7bdaf56dfa  802-3-ethernet  eno1
[user@host ~]$ nmcli con show --active
NAME         UUID                                  TYPE            DEVICE
static-ens3  72ca57a2-f780-40da-b146-99f71c431e2b  802-3-ethernet  ens3
eno1         87b53c56-1f5d-4a29-a869-8a7bdaf56dfa  802-3-ethernet  eno1
```



##### Adding a network connection

The **nmcli con add** command is used to add new network connections. The following example **nmcli con add** commands assume that the name of the network connection being added is not already in use.

The following command adds a new connection named `eno2` for the interface `eno2`, which gets IPv4 networking information using DHCP and autoconnects on startup. It also gets IPv6 networking settings by listening for router advertisements on the local link. The name of the configuration file is based on the value of the `con-name` option, `eno2`, and is saved to the `/etc/sysconfig/network-scripts/ifcfg-eno2` file.

```
[root@host ~]# nmcli con add con-name eno2 type ethernet ifname eno2
```

The next example creates an `eno2` connection for the `eno2` device with a static IPv4 address, using the IPv4 address and network prefix `192.168.0.5/24` and default gateway `192.168.0.254`, but still autoconnects at startup and saves its configuration into the same file. Due to screen size limitations, terminate the first line with a shell `\` escape and complete the command on the next line.

```
[root@host ~]# nmcli con add con-name eno2 type ethernet ifname eno2 \
ipv4.address 192.168.0.5/24 ipv4.gateway 192.168.0.254
```

This final example creates an `eno2` connection for the `eno2` device with static IPv6 and IPv4 addresses, using the IPv6 address and network prefix `2001:db8:0:1::c000:207/64` and default IPv6 gateway `2001:db8:0:1::1`, and the IPv4 address and network prefix `192.0.2.7/24` and default IPv4 gateway `192.0.2.1`, but still autoconnects at startup and saves its configuration into `/etc/sysconfig/network-scripts/ifcfg-eno2`. Due to screen size limitations, terminate the first line with a shell `\` escape and complete the command on the next line.

```
[root@host ~]# nmcli con add con-name eno2 type ethernet ifname eno2 \
ipv6.address 2001:db8:0:1::c000:207/64 ipv6.gateway 2001:db8:0:1::1 \
ipv4.address 192.0.2.7/24 ipv4.gateway 192.0.2.1
```

##### Controlling network connections

The **nmcli con up name** command activates the connection *name* on the network interface it is bound to. Note that the command takes the name of a *connection*, not the name of the network interface. Remember that the **nmcli con show** command displays the names of *all* available connections.

```
[root@host ~]# nmcli con up static-ens3
```

The **nmcli dev disconnect device** command disconnects the network interface *device* and brings it down. This command can be abbreviated **nmcli dev dis device**:

```
[root@host ~]# nmcli dev dis ens3
```



##### Adding a network connection

The **nmcli con add** command is used to add new network connections. The following example **nmcli con add** commands assume that the name of the network connection being added is not already in use.

The following command adds a new connection named `eno2` for the interface `eno2`, which gets IPv4 networking information using DHCP and autoconnects on startup. It also gets IPv6 networking settings by listening for router advertisements on the local link. The name of the configuration file is based on the value of the `con-name` option, `eno2`, and is saved to the `/etc/sysconfig/network-scripts/ifcfg-eno2` file.

```
[root@host ~]# nmcli con add con-name eno2 type ethernet ifname eno2
```

The next example creates an `eno2` connection for the `eno2` device with a static IPv4 address, using the IPv4 address and network prefix `192.168.0.5/24` and default gateway `192.168.0.254`, but still autoconnects at startup and saves its configuration into the same file. Due to screen size limitations, terminate the first line with a shell `\` escape and complete the command on the next line.

```
[root@host ~]# nmcli con add con-name eno2 type ethernet ifname eno2 \
ipv4.address 192.168.0.5/24 ipv4.gateway 192.168.0.254
```

This final example creates an `eno2` connection for the `eno2` device with static IPv6 and IPv4 addresses, using the IPv6 address and network prefix `2001:db8:0:1::c000:207/64` and default IPv6 gateway `2001:db8:0:1::1`, and the IPv4 address and network prefix `192.0.2.7/24` and default IPv4 gateway `192.0.2.1`, but still autoconnects at startup and saves its configuration into `/etc/sysconfig/network-scripts/ifcfg-eno2`. Due to screen size limitations, terminate the first line with a shell `\` escape and complete the command on the next line.

```
[root@host ~]# nmcli con add con-name eno2 type ethernet ifname eno2 \
ipv6.address 2001:db8:0:1::c000:207/64 ipv6.gateway 2001:db8:0:1::1 \
ipv4.address 192.0.2.7/24 ipv4.gateway 192.0.2.1
```

##### Controlling network connections

The **nmcli con up name** command activates the connection *name* on the network interface it is bound to. Note that the command takes the name of a *connection*, not the name of the network interface. Remember that the **nmcli con show** command displays the names of *all* available connections.

```
[root@host ~]# nmcli con up static-ens3
```

The **nmcli dev disconnect device** command disconnects the network interface *device* and brings it down. This command can be abbreviated **nmcli dev dis device**:

```
[root@host ~]# nmcli dev dis ens3
```

The **nmcli con mod name** command is used to change the settings for a connection. These changes are also saved in the `/etc/sysconfig/network-scripts/ifcfg-*name*` file for the connection. Available settings are documented in the `nm-settings`(5) man page.

To set the IPv4 address to `192.0.2.2/24` and default gateway to `192.0.2.254` for the connection `static-ens3`:

```
[root@host ~]# nmcli con mod static-ens3 ipv4.address 192.0.2.2/24 \
ipv4.gateway 192.0.2.254
```

To set the IPv6 address to `2001:db8:0:1::a00:1/64` and default gateway to `2001:db8:0:1::1` for the connection `static-ens3`:

```
[root@host ~]# nmcli con mod static-ens3 ipv6.address 2001:db8:0:1::a00:1/64 \
ipv6.gateway 2001:db8:0:1::1
```



The **nmcli con mod name** command is used to change the settings for a connection. These changes are also saved in the `/etc/sysconfig/network-scripts/ifcfg-*name*` file for the connection. Available settings are documented in the `nm-settings`(5) man page.

To set the IPv4 address to `192.0.2.2/24` and default gateway to `192.0.2.254` for the connection `static-ens3`:

```
[root@host ~]# nmcli con mod static-ens3 ipv4.address 192.0.2.2/24 \
ipv4.gateway 192.0.2.254
```

To set the IPv6 address to `2001:db8:0:1::a00:1/64` and default gateway to `2001:db8:0:1::1` for the connection `static-ens3`:

```
[root@host ~]# nmcli con mod static-ens3 ipv6.address 2001:db8:0:1::a00:1/64 \
ipv6.gateway 2001:db8:0:1::1
```



##### Deleting a network connection

The **nmcli con del name** command deletes the connection named *name* from the system, disconnecting it from the device and removing the file `/etc/sysconfig/network-scripts/ifcfg-*name*`.

```
[root@host ~]# nmcli con del static-ens3
```



##### Summary of Commands

The following table is a list of key **nmcli** commands discussed in this section.

| Command                         | Purpose                                                      |
| :------------------------------ | :----------------------------------------------------------- |
| **nmcli dev status**            | Show the NetworkManager status of all network interfaces.    |
| **nmcli con show**              | List all connections.                                        |
| **nmcli con show name**         | List the current settings for the connection *name*.         |
| **nmcli con add con-name name** | Add a new connection named *name*.                           |
| **nmcli con mod name**          | Modify the connection *name*.                                |
| **nmcli con reload**            | Reload the configuration files (useful after they have been edited by hand). |
| **nmcli con up name**           | Activate the connection *name*.                              |
| **nmcli dev dis dev**           | Deactivate and disconnect the current connection on the network interface *dev*. |
| **nmcli con del name**          | Delete the connection *name* and its configuration file.     |



##### Describing Connection Configuration Files

By default, changes made with **nmcli con mod name** are automatically saved to `/etc/sysconfig/network-scripts/ifcfg-*name*`. That file can also be manually edited with a text editor. After doing so, run **nmcli con reload** so that NetworkManager reads the configuration changes.

For backward-compatibility reasons, the directives saved in that file have different names and syntax than the `nm-settings`(5) names. The following table maps some of the key setting names to `ifcfg-*` directives.



**Table 10.2. Comparison of nm-settings and ifcfg-\* Directives**

| **nmcli con mod**                             | `ifcfg-* file`                                          | Effect                                                       |
| :-------------------------------------------- | :------------------------------------------------------ | :----------------------------------------------------------- |
| `ipv4.method manual`                          | `BOOTPROTO=none`                                        | IPv4 addresses configured statically.                        |
| `ipv4.method auto`                            | `BOOTPROTO=dhcp`                                        | Looks for configuration settings from a DHCPv4 server. If static addresses are also set, will not bring those up until we have information from DHCPv4. |
| `ipv4.addresses "192.0.2.1/24 192.0.2.254"`   | `IPADDR0=192.0.2.1` `PREFIX0=24` `GATEWAY0=192.0.2.254` | Sets static IPv4 address, network prefix, and default gateway. If more than one is set for the connection, then instead of 0, the `ifcfg-*` directives end with 1, 2, 3 and so on. |
| `ipv4.dns 8.8.8.8`                            | `DNS0=8.8.8.8`                                          | Modify `/etc/resolv.conf` to use this `nameserver`.          |
| `ipv4.dns-search example.com`                 | `DOMAIN=example.com`                                    | Modify `/etc/resolv.conf` to use this domain in the `search` directive. |
| `ipv4.ignore-auto-dns true`                   | `PEERDNS=no`                                            | Ignore DNS server information from the DHCP server.          |
| `ipv6.method manual`                          | `IPV6_AUTOCONF=no`                                      | IPv6 addresses configured statically.                        |
| `ipv6.method auto`                            | `IPV6_AUTOCONF=yes`                                     | Configures network settings using SLAAC from router advertisements. |
| `ipv6.method dhcp`                            | `IPV6_AUTOCONF=no` `DHCPV6C=yes`                        | Configures network settings by using DHCPv6, but not SLAAC.  |
| `ipv6.addresses "2001:db8::a/64 2001:db8::1"` | `IPV6ADDR=2001:db8::a/64` `IPV6_DEFAULTGW=2001:db8::1`  | Sets static IPv6 address, network prefix, and default gateway. If more than one address is set for the connection, `IPV6_SECONDARIES` takes a double-quoted list of space-delimited address/prefix definitions. |
| `ipv6.dns ...`                                | `DNS0= ...`                                             | Modify `/etc/resolv.conf` to use this nameserver. Exactly the same as IPv4. |
| `ipv6.dns-search example.com`                 | `DOMAIN=example.com`                                    | Modify `/etc/resolv.conf` to use this domain in the `search` directive. Exactly the same as IPv4. |
| `ipv6.ignore-auto-dns true`                   | `IPV6_PEERDNS=no`                                       | Ignore DNS server information from the DHCP server.          |
| `connection.autoconnect yes`                  | `ONBOOT=yes`                                            | Automatically activate this connection at boot.              |
| `connection.id ens3`                          | `NAME=ens3`                                             | The name of this connection.                                 |
| `connection.interface-name ens3`              | `DEVICE=ens3`                                           | The connection is bound to the network interface with this name. |
| `802-3-ethernet.mac-address . . .`            | `HWADDR= ...`                                           | The connection is bound to the network interface with this MAC address. |



### Modifying network configuration

It is also possible to configure the network by directly editing the connection configuration files. Connection configuration files control the software interfaces for individual network devices. These files are usually named `/etc/sysconfig/network-scripts/ifcfg-*name*`, where *name* refers to the name of the device or connection that the configuration file controls. The following are standard variables found in the file used for static or dynamic IPv4 configuration.



**Table 10.3. IPv4 Configuration Options for ifcfg File**

| *Static*                                                     | *Dynamic*        | *Either*                                                     |
| :----------------------------------------------------------- | :--------------- | :----------------------------------------------------------- |
| `BOOTPROTO=none``IPADDR0=172.25.250.10``PREFIX0=24``GATEWAY0=172.25.250.254``DEFROUTE=yes``DNS1=172.25.254.254` | `BOOTPROTO=dhcp` | `DEVICE=ens3``NAME="static-ens3"``ONBOOT=yes``UUID=f3e8(...)ad3e``USERCTL=yes` |



In the static settings, variables for IP address, prefix, and gateway have a number at the end. This allows multiple sets of values to be assigned to the interface. The DNS variable also has a number used to specify the order of lookup when multiple servers are specified.

After modifying the configuration files, run **nmcli con reload** to make NetworkManager read the configuration changes. The interface still needs to be restarted for changes to take effect.

```
[root@host ~]# nmcli con reload
[root@host ~]# nmcli con down "static-ens3"
[root@host ~]# nmcli con up "static-ens3"
```

### Changing the system host name

A static host name may be specified in the `/etc/hostname` file. The **hostnamectl** command is used to modify this file and may be used to view the status of the system's fully qualified host name. If this file does not exist, the host name is set by a reverse DNS query once the interface has an IP address assigned.

```
[root@host ~]# hostnamectl set-hostname host@example.com
[root@host ~]# hostnamectl status
   Static hostname: host.example.com
```

### Configuring name resolution

The *stub resolver* is used to convert host names to IP addresses or the reverse. It determines where to look based on the configuration of the `/etc/nsswitch.conf` file. By default, the contents of the `/etc/hosts` file are checked first.

If an entry is not found in the `/etc/hosts` file, by default the stub resolver tries to look up the hostname by using a DNS nameserver. The `/etc/resolv.conf` file controls how this query is performed:

- `search`: a list of domain names to try with a short host name. Both this and `domain` should not be set in the same file; if they are, the last instance wins. See `resolv.conf`(5) for details.
- `nameserver`: the IP address of a nameserver to query. Up to three nameserver directives may be given to provide backups if one is down.

```
[root@host ~]# cat /etc/resolv.conf
# Generated by NetworkManager
domain example.com
search example.com
nameserver 172.25.254.254
```

##### Summary

In this chapter, you learned:

- The TCP/IP network model is a simplified, four-layered set of abstractions that describes how different protocols interoperate in order for computers to send traffic from one machine to another over the Internet.
- IPv4 is the primary network protocol used on the Internet today. IPv6 is intended as an eventual replacement for the IPv4 network protocol. By default, Red Hat Enterprise Linux operates in dual-stack mode, using both protocols in parallel.
- NetworkManager is a daemon that monitors and manages network configuration.
- The **nmcli** command is a command-line tool for configuring network settings with NetworkManager.
- The system's static host name is stored in the `/etc/hostname` file. The **hostnamectl** command is used to modify or view the status of the system's host name and related settings. The **hostname** command displays or temporarily modifies the system's host name.



## Chapter 11. Analyzing and Storing Logs

**Table 11.1. Selected System Log Files**

| Log file            | Type of Messages Stored                                      |
| :------------------ | :----------------------------------------------------------- |
| `/var/log/messages` | Most syslog messages are logged here. Exceptions include messages related to authentication and email processing, scheduled job execution, and those which are purely debugging-related. |
| `/var/log/secure`   | Syslog messages related to security and authentication events. |
| `/var/log/maillog`  | Syslog messages related to the mail server.                  |
| `/var/log/cron`     | Syslog messages related to scheduled job execution.          |
| `/var/log/boot.log` | Non-syslog console messages related to system startup.       |

**Table 11.2. Overview of Syslog Priorities**

| Code | Priority | Severity                         |
| :--- | :------- | :------------------------------- |
| 0    | emerg    | System is unusable               |
| 1    | alert    | Action must be taken immediately |
| 2    | crit     | Critical condition               |
| 3    | err      | Non-critical error condition     |
| 4    | warning  | Warning condition                |
| 5    | notice   | Normal but significant event     |
| 6    | info     | Informational event              |
| 7    | debug    | Debugging-level message          |

The `rsyslog` service uses the facility and priority of log messages to determine how to handle them. This is configured by rules in the `/etc/rsyslog.conf` file and any file in the `/etc/rsyslog.d` directory that has a file name extension of `.conf`. Software packages can easily add rules by installing an appropriate file in the `/etc/rsyslog.d` directory.

Each rule that controls how to sort syslog messages is a line in one of the configuration files. The left side of each line indicates the facility and severity of the syslog messages the rule matches. The right side of each line indicates what file to save the log message in (or where else to deliver the message). An asterisk (`*`) is a wildcard that matches all values.

For example, the following line would record messages sent to the `authpriv` facility at any priority to the file `/var/log/secure`:

```
authpriv.*                  /var/log/secure
```

Other example, create the `/etc/rsyslog.d/debug.conf` file with the necessary entries to redirect all log messages having the `debug` priority to `/var/log/messages-debug`. You may use the **vim /etc/rsyslog.d/debug.conf** command to create the file with the following content.

```
*.debug /var/log/messages-debug
```



**Sending Syslog Messages Manually**

The **logger** command can send messages to the **rsyslog** service. By default, it sends the message to the `user` facility with the `notice` priority (`user.notice`) unless specified otherwise with the `-p` option. It is useful to test any change to the `rsyslog` service configuration.

To send a message to the **rsyslog** service that gets recorded in the `/var/log/boot.log` log file, execute the following **logger** command:

```
[root@host ~]# logger -p local7.notice "Log entry created on host"
```



##### Storing the System Journal Permanently

The `Storage` parameter in the `/etc/systemd/journald.conf` file defines whether to store system journals in a volatile manner or persistently across reboot. Set this parameter to `persistent`, `volatile`, or `auto` 

After editing the configuration file, restart the `systemd-journald` service to bring the configuration changes into effect.

```
[root@host ~]# systemctl restart systemd-journald
```

To limit the output to a specific system boot, use the `-b` option with the **journalctl** command. The following **journalctl** command retrieves the entries limited to the first system boot:

```
[root@host ~]# journalctl -b 1
```

The following **journalctl** command retrieves the entries limited to the current system boot:

```
[root@host ~]# journalctl -b
```

Use the **journalctl** command `--since` and `--until` options to display log events recorded in the previous 30 minutes on `serverb`. To quit **journalctl**, press **q**.

```
journalctl --since 07:01:00 --until 07:31:00
```



##### Setting Local Clocks and Time Zones

Use **tzselect** to determine the appropriate Time Zone for a country.

The **timedatectl** command shows an overview of the current time-related system settings, including current time, time zone, and NTP synchronization settings of the system.

```
[user@host ~]$ timedatectl
```

A database of time zones is available and can be listed with the **timedatectl list-timezones** command.

```
[user@host ~]$ timedatectl list-timezones
```

The superuser can change the system setting to update the current time zone using the **timedatectl set-timezone** command. The following **timedatectl** command updates the current time zone to `America/Phoenix`.

```
[root@host ~]# timedatectl set-timezone America/Phoenix
```

Use the **timedatectl set-timezone UTC** command to set the system's current time zone to `UTC`.

Use the **timedatectl set-time** command to change the system's current time. The time is specified in the *"YYYY-MM-DD hh:mm:ss"* format, where either date or time can be omitted. The following **timedatectl** command changes the time to `09:00:00`.

```
[root@host ~]# timedatectl set-time 9:00:00
```

NOTE: In Red Hat Enterprise Linux 8, the **timedatectl set-ntp** command will adjust whether or not `chronyd` NTP service is operating. Other Linux distributions might use this setting to adjust a different NTP or SNTP service.



##### Configuring and Monitoring Chronyd

The `chronyd` service keeps the usually-inaccurate local hardware clock (RTC) on track by synchronizing it to the configured NTP servers. If no network connectivity is available, `chronyd` calculates the RTC clock drift, which is recorded in the `driftfile` specified in the `/etc/chrony.conf` configuration file.

The first argument of the `server` line is the IP address or DNS name of the NTP server. Following the server IP address or name, a series of options for the server can be listed. It is recommended to use the `iburst` option, because after the service starts, four measurements are taken in a short time period for a more accurate initial clock synchronization.

The following `server classroom.example.com iburst` line in the `/etc/chrony.conf` file causes the `chronyd` service to use the `classroom.example.com` NTP time source.

```
# Use public servers from the pool.ntp.org project.
...output omitted...
server classroom.example.com iburst
...output omitted...
```

After pointing **chronyd** to the local time source, `classroom.example.com`, you should restart the service.

```
[root@host ~]# systemctl restart chronyd
```

The **chronyc** command acts as a client to the `chronyd` service. After setting up NTP synchronization, you should verify that the local system is seamlessly using the NTP server to synchronize the system clock using the **chrony sources** command. For more verbose output with additional explanations about the output, use the **chronyc sources -v** command.

```
[root@host ~]# chronyc sources -v
```



##### Summary

- The `systemd-journald` and `rsyslog` services capture and write log messages to the appropriate files.
- The `/var/log` directory contains log files.
- Periodic rotation of log files prevent them from filling up the file system space.
- The `systemd` journals are temporary and do not persist across reboot.
- The `chronyd` service helps to synchronize time settings with a time source.
- The time zone of the server can be updated based on its location.







## Chapter 12. Implementing Advanced Storage Features

**Create a logical volume**.

Use **lvcreate** to create a new logical volume from the available physical extents in a volume group. At a minimum, the **lvcreate** command includes the `-n` option to set the LV name, either the `-L` option to set the LV size in bytes or the `-l` option to set the LV size in extents, and the name of the volume group hosting this logical volume.

```
[root@host ~]# lvcreate -n lv01 -L 700M vg01
```

You can specify the size using the `-L` option, which expects sizes in bytes, mebibytes (binary megabytes, 1048576 bytes), gibibytes (binary gigabytes), or similar. Alternatively, you can use the `-l` option, which expects sizes specified as a number of physical extents.

*The following list provides some examples of creating LVs:*

- **lvcreate -L 128M**: Size the logical volume to exactly 128 MiB.
- **lvcreate -l 128** : Size the logical volume to exactly 128 extents. The total number of bytes depends on the size of the physical extent block on the underlying physical volume.

**Reducing a Volume Group**

To reduce a volume group, perform the following steps:

**Move the physical extents**.

Use **pvmove PV_DEVICE_NAME** to relocate any physical extents from the physical volume you want to remove to other physical volumes in the volume group. The other physical volumes must have a sufficient number of free extents to accommodate this move. This is only possible if there are enough free extents in the VG and if all of those come from other PVs.

```
[root@host ~]# pvmove /dev/vdb3
```

This command moves the PEs from `/dev/vdb3` to other PVs with free PEs in the same VG.

**Reduce the volume group**.

Use **vgreduce VG_NAME PV_DEVICE_NAME** to remove a physical volume from a volume group.

```
[root@host ~]# vgreduce vg01 /dev/vdb3
```

This removes the `/dev/vdb3` PV from the `vg01` VG and it can now be added to another VG. Alternatively, **pvremove** can be used to permanently stop using the device as a PV.

**Extend the logical volume**.

Use **lvextend LV_DEVICE_NAME** to extend the logical volume to a new size.

```
[root@host ~]# lvextend -L +300M /dev/vg01/lv01
```

This increases the size of the logical volume `lv01` by 300 MiB. Notice the plus sign (+) in front of the size, which means add this value to the existing size; otherwise, the value defines the final size of the LV.

As with **lvcreate**, different methods exist to specify the size: the `-l` option expects the number of physical extents as the argument. The `-L` option expects sizes in bytes, mebibytes, gibibytes, and similar.

*The following list provides some examples of extending LVs.*

**Table 12.1. Extending LVs Examples**

| Command                  | Results                                                      |
| :----------------------- | :----------------------------------------------------------- |
| **lvextend -l 128**      | Resize the logical volume to *exactly* 128 extents in size.  |
| **lvextend -l +128**     | Add 128 extents to the current size of the logical volume.   |
| **lvextend -L 128M**     | Resize the logical volume to *exactly* 128 MiB.              |
| **lvextend -L +128M**    | Add 128 MiB to the current size of the logical volume.       |
| **lvextend -l +50%FREE** | Add 50 percent of the current free space in the VG to the LV. |

**Extend the file system**.

Use **xfs_growfs mountpoint** to expand the file system to occupy the extended LV. The target file system must be mounted when you use the **xfs_growfs** command. You can continue to use the file system while it is being resized.

```
[root@host ~]# xfs_growfs /mnt/data
```

**Extend the ext4 file system**

Use **resize2fs /dev/vgname/lvname** to expand the file system to occupy the new extended LV. The file system can be mounted and in use while the extension command is running. You can include the `-p` option to monitor the progress of the resize operation.

```
[root@host ~]# resize2fs /dev/vg01/lv01
```

**NOTE**

The primary difference between **xfs_growfs** and **resize2fs** is the argument passed to identify the file system. **xfs_growfs** takes the mount point and **resize2fs** takes the logical volume name.

**Extend Swap file system**

Deactivate the swap space

Use `swapoff -v /dev/vgname/lvname` to deactivate the swap space on the logical volume.

Extend the logical volume
`lvextend -l +extents /dev/vgname/lvname` extends the logical volume */dev/vgname/lvname* by the *extents* value.

Format the logical volume as swap space

`mkswap /dev/vgname/lvname` formats the entire logical volume as swap space.

Activate the swap space

Use `swapon -va /dev/vgname/lvname` to activate the swap space on the logical volume.

**Managing Thin-provisioned File Systems**

To manage the thin-provisioned file systems using the Stratis storage management solution, install the *stratis-cli* and *stratisd* packages. The *stratis-cli* package provides the **stratis** command, which translates user requests to the `stratisd` service via the `D-Bus` API. The *stratisd* package provides the `stratisd` service, which implements the `D-Bus` interface, and manages and monitors the elements of Stratis, such as block devices, pools, and file systems. The `D-Bus` API is available if the `stratisd` service is running.

Install and activate Stratis using the usual tools:

- Install *stratis-cli* and *stratisd* using the **yum install** command.

  ```
  [root@host ~]# yum install stratis-cli stratisd
  ...output omitted...
  Is this ok [y/N]: y
  ...output omitted...
  Complete!
  ```

- Activate the `stratisd` service using the **systemctl** command.

  ```
  [root@host ~]# systemctl enable --now stratisd
  ```

The following are common management operations performed using the Stratis storage management solution.

- Create pools of one or more block devices using the **stratis pool create** command.

  ```
  [root@host ~]# stratis pool create pool1 /dev/vdb
  ```

  Each pool is a subdirectory under the `/stratis` directory.

- Use the **stratis pool list** command to view the list of available pools.

  ```
  [root@host ~]# stratis pool list
  Name     Total Physical Size  Total Physical Used
  pool1                  5 GiB               52 MiB
  ```

- Use the **stratis pool add-data** command to add additional block devices to a pool.

  ```
  [root@host ~]# stratis pool add-data pool1 /dev/vdc
  ```

- Use the **stratis blockdev list** command to view the block devices of a pool.

  ```
  [root@host ~]# stratis blockdev list pool1
  Pool Name  Device Node    Physical Size   State  Tier
  pool1      /dev/vdb               5 GiB  In-use  Data
  pool1      /dev/vdc               5 GiB  In-use  Data
  ```

- Use the **stratis filesystem create** command to create a dynamic and flexible file system from a pool.

  ```
  [root@host ~]# stratis filesystem create pool1 filesystem1
  ```

  The links to the Stratis file systems are in the `/stratis/*pool1*` directory.

- Stratis supports file-system snapshotting with the **stratis filesystem snapshot** command. Snapshots are independent of the source file systems.

  ```
  [root@host ~]# stratis filesystem snapshot pool1 filesystem1 snapshot1
  ```

- Use the **stratis filesystem list** command to view the list of available file systems.

  ```
  [root@host ~]# stratis filesystem list
  ...output omitted...
  ```

To ensure that the Stratis file systems are persistently mounted, edit `/etc/fstab` and specify the details of the file system. The following command displays the UUID of the file system that you should use in `/etc/fstab` to identify the file system.

```
[root@host ~]# lsblk --output=UUID /stratis/pool1/filesystem1
UUID
31b9363b-add8-4b46-a4bf-c199cd478c55
```

The following is an example entry in the `/etc/fstab` file to persistently mount a Stratis file system.

```
UUID=31b9...8c55 /dir1 xfs defaults,x-systemd.requires=stratisd.service 0 0
```

The `x-systemd.requires=stratisd.service` mount option delays mounting the file system until after `systemd` starts the `stratisd.service` during the boot process.

**NOTE**

Failure to use the `x-systemd.requires=stratisd.service` mount option in `/etc/fstab` for the Stratis file system will result in the machine booting to `emergency.target` on the next reboot.

##### Implementing Virtual Data Optimizer

**Enabling VDO**

Install the *vdo* and *kmod-kvdo* packages to enable VDO in the system.

```
[root@host ~]# yum install vdo kmod-kvdo
...output omitted...
Is this ok [y/N]: y
...output omitted...
Complete!
```

**Creating a VDO Volume**

To create a VDO volume, run the **vdo create** command.

```
[root@host ~]# vdo create --name=vdo1 --device=/dev/vdd --vdoLogicalSize=50G
...output omitted...
```

If you omit the logical size, the resulting VDO volume gets the same size as its physical device.

When the VDO volume is in place, you can format it with the file-system type of your choice and mount it under the file-system hierarchy on your system.

**Analyzing a VDO Volume**

To analyze a VDO volume, run the **vdo status** command. This command displays a report on the VDO system, and the status of the VDO volume in YAML format. It also displays attributes of the VDO volume. Use the `--name=` option to specify the name of a particular volume. If you omit the name of the specific volume, the output of the **vdo status** command displays the status of all the VDO volumes.

```
[root@host ~]# vdo status --name=vdo1
...output omitted...
```

The **vdo list** command displays the list of VDO volumes that are currently started. You can start and stop a VDO volume using the **vdo start** and **vdo stop** commands, respectively.

##### Summary

In this chapter, you learned:

- Physical volumes, volume groups, and logical volumes are managed by a variety of tools such as pvcreate, vgreduce, and lvextend.
- Logical volumes can be formatted with a file system or swap space, and they can be mounted persistently.
- Additional storage can be added to volume groups and logical volumes can be extended dynamically.
- The Stratis volume-management solution implements flexible file systems that grow dynamically with data.
- Stratis volume-management solution supports thin provisioning, snapshotting, and monitoring.
- The Virtual Data Optimizer (VDO) aims to reduce the cost of data storage.
- The Virtual Data Optimizer applies zero-block eliminiation, data deduplication, and data compression to optimize disk space efficiency.



## Chapter 13. Scheduling Future Tasks

The `/etc/crontab` file has a useful syntax diagram in the included comments.

```
 # For details see man 4 crontabs

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue ...
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed
```

The crontab system also includes repositories for scripts that need to run every hour, day, week, and month. These repositories are directories called `/etc/cron.hourly/`, `/etc/cron.daily/`, `/etc/cron.weekly/`, and `/etc/cron.monthly/`. Again, these directories contain executable shell scripts, not crontab files.

The purpose of `/etc/anacrontab` is to make sure that important jobs always run, and not skipped accidentally because the system was turned off or hibernating when the job should have been executed. For example, if a system job that runs daily was not executed last time it was due because the system was rebooting, the job is executed when the system becomes ready. However, there may be a delay of several minutes in starting the job depending on the value of the `Delay in minutes` parameter specified for the job in `/etc/anacrontab`.

There are different files in `/var/spool/anacron/` for each of the daily, weekly, and monthly jobs to determine if a particular job has run. When `crond` starts a job from `/etc/anacrontab`, it updates the time stamps of those files. The same time stamp is used to determine when a job was last run. The syntax of `/etc/anacrontab` is different from the regular `crontab` configuration files. It contains exactly four fields per line, as follows.

- `Period in days`

  The interval in days for the job that runs on a repeating schedule. This field accepts an integer or a macro as its value. For example, the macro `@daily` is equivalent to the integer `1`, which means that the job is executed on a daily basis. Similarly, the macro `@weekly` is equivalent to the integer `7`, which means that the job is executed on a weekly basis.

- `Delay in minutes`

  The amount of time the `crond` daemon should wait before starting this job.

- `Job identifier`

  The unique name the job is identified as in the log messages.

- `Command`

  The command to be executed.

The `/etc/anacrontab` file also contains environment variable declarations using the syntax `NAME=value`. Of special interest is the variable `START_HOURS_RANGE`, which specifies the time interval for the jobs to run. Jobs are not started outside of this range. If on a particular day, a job does not run within this time interval, the job has to wait until the next day for execution.



**Cleaning Temporary Files with a Systemd Timer**

To ensure that long-running systems do not fill up their disks with stale data, a `systemd` timer unit called `systemd-tmpfiles-clean.timer` triggers `systemd-tmpfiles-clean.service` on a regular interval, which executes the **systemd-tmpfiles --clean** command.

The **systemd** timer unit configuration files have a `[Timer]` section that indicates how often the service with the same name should be started.

Use the following **systemctl** command to view the contents of the `systemd-tmpfiles-clean.timer` unit configuration file.

```
[user@host ~]$ systemctl cat systemd-tmpfiles-clean.timer
# /usr/lib/systemd/system/systemd-tmpfiles-clean.timer
#  SPDX-License-Identifier: LGPL-2.1+
#
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published
#  by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

[Unit]
Description=Daily Cleanup of Temporary Directories
Documentation=man:tmpfiles.d(5) man:systemd-tmpfiles(8)

[Timer]
OnBootSec=15min
OnUnitActiveSec=1d
```

In the preceding configuration the parameter `OnBootSec=15min` indicates that the service unit called `systemd-tmpfiles-clean.service` gets triggered 15 minutes after the system has booted up. The parameter `OnUnitActiveSec=1d` indicates that any further trigger to the `systemd-tmpfiles-clean.service` service unit happens 24 hours after the service unit was activated last.

Based on your requirement, you can change the parameters in the `systemd-tmpfiles-clean.timer` timer unit configuration file. For example, the value `30min` for the parameter `OnUnitActiveSec` triggers the `systemd-tmpfiles-clean.service` service unit 30 minutes after the service unit was last activated. As a result, `systemd-tmpfiles-clean.service` gets triggered every 30 minutes after bringing the changes into effect.

After changing the timer unit configuration file, use the **systemctl daemon-reload** command to ensure that `systemd` is aware of the change. This command reloads the `systemd` manager configuration.

```
[root@host ~]# systemctl daemon-reload
```

After you reload the `systemd` manager configuration, use the following **systemctl** command to activate the `systemd-tmpfiles-clean.timer` unit.

```
[root@host ~]# systemctl enable --now systemd-tmpfiles-clean.timer
```



**Cleaning Temporary Files Manually**

The command **systemd-tmpfiles --clean** parses the same configuration files as the **systemd-tmpfiles --create** command, but instead of creating files and directories, it will purge all files which have not been accessed, changed, or modified more recently than the maximum age defined in the configuration file.

The format of the configuration files for **systemd-tmpfiles** is detailed in the **tmpfiles.d**(5) manual page. The basic syntax consists of seven columns: Type, Path, Mode, UID, GID, Age, and Argument. *Type* refers to the action that **systemd-tmpfiles** should take; for example, `d` to create a directory if it does not yet exist, or `Z` to recursively restore SELinux contexts and file permissions and ownership.

The following are some examples with explanations.

```
d /run/systemd/seats 0755 root root -
```

When creating files and directories, create the `/run/systemd/seats` directory if it does not yet exist, owned by the user `root` and the group `root`, with permissions set to `rwxr-xr-x`. This directory will not be automatically purged.

```
D /home/student 0700 student student 1d
```

Create the `/home/student` directory if it does not yet exist. If it does exist, empty it of all contents. When **systemd-tmpfiles --clean** is run, remove all files which have not been accessed, changed, or modified in more than one day.

```
L /run/fstablink - root root - /etc/fstab
```

Create the symbolic link `/run/fstablink` pointing to `/etc/fstab`. Never automatically purge this line.

**Summary**

In this chapter, you learned:

- Recurring system jobs execute tasks on a repeating schedule.
- Recurring system jobs accomplish administrative tasks on a repeating schedule that have system-wide impact.
- The `systemd` timer units can execute the recurring jobs.



## Chapter 14. Accessing Network-Attached Storage

**Mounting NFS Shares**

Mount temporarily: Mount the NFS share using the **mount** command:

```
[user@host ~]$ sudo mount -t nfs -o rw,sync serverb:/share mountpoint
```

The `-t nfs` option is the file-system type for NFS shares (not strictly required but shown for completeness). The `-o sync` option tells **mount** to immediately synchronize write operations with the NFS server (the default is asynchronous).

Mount persistently: To ensure that the NFS share is mounted at boot time, edit the `/etc/fstab` file to add the mount entry.

```
[user@host ~]$ sudo vim /etc/fstab
...
serverb:/share  /mountpoint  nfs  rw,soft  0 0
```



### The **nfsconf** Tool

Red Hat Enterprise Linux 8 introduces the **nfsconf** tool to manage the NFS client and server configuration files under NFSv4 and NFSv3. Configure the **nfsconf** tool using `/etc/nfs.conf` (the `/etc/sysconfig/nfs` file from earlier versions of the operating system is deprecated now). Use the **nfsconf** tool to get, set, or unset NFS configuration parameters.

Use the **nfsconf --set section key value** to set a value for the key in the specified section.

```
[user@host ~]$ sudo nfsconf --set nfsd vers4.2 y
```

This command updates the `/etc/nfs.conf` configuration file:

**Configure an NFSv4-only Client**

You can configure an NFSv4-only client by setting the following values on the `/etc/nfs.conf` configuration file.

Start by disabling UDP and other NFSv2 and NFSv3 related keys:

```
[user@host ~]$ sudo nfsconf --set nfsd udp n
[user@host ~]$ sudo nfsconf --set nfsd vers2 n
[user@host ~]$ sudo nfsconf --set nfsd vers3 n
```

Enable TCP, and NFSv4, related keys.

```
[user@host ~]$ sudo nfsconf --set nfsd tcp y
[user@host ~]$ sudo nfsconf --set nfsd vers4 y
[user@host ~]$ sudo nfsconf --set nfsd vers4.0 y
[user@host ~]$ sudo nfsconf --set nfsd vers4.1 y
[user@host ~]$ sudo nfsconf --set nfsd vers4.2 y
```

As before, the changes appear in the `/etc/nfs.conf` configuration file



**Create an automount**

Configuring an automount is a multiple step process:

1. Install the *autofs* package.

   ```
   [user@host ~]$ sudo yum install autofs
   ```

   This package contains everything needed to use the automounter for NFS shares.

2. Add a *master map* file to `/etc/auto.master.d`. This file identifies the base directory used for mount points and identifies the mapping file used for creating the automounts.

   ```
   [user@host ~]$ sudo vim /etc/auto.master.d/demo.autofs
   ```

   The name of the master map file is arbitrary (although typically meaningful), but it must have an extension of `.autofs` for the subsystem to recognize it. You can place multiple entries in a single master map file; alternatively, you can create multiple master map files each with its own entries grouped logically.

   Add the master map entry, in this case, for indirectly mapped mounts:

   ```
   /shares  /etc/auto.demo
   ```

   This entry uses the `/shares` directory as the base for indirect automounts. The `/etc/auto.demo` file contains the mount details. Use an absolute file name. The `auto.demo` file needs to be created before starting the `autofs` service.

3. Create the mapping files. Each mapping file identifies the mount point, mount options, and source location to mount for a set of automounts.

   ```
   [user@host ~]$ sudo vim /etc/auto.demo
   ```

   The mapping file-naming convention is `/etc/auto.*name*`, where *name* reflects the content of the map.

   ```
   work  -rw,sync  serverb:/shares/work
   ```

   The format of an entry is *mount point*, *mount options*, and *source location*. This example shows a basic indirect mapping entry. Direct maps and indirect maps using wildcards are covered later in this section.

4. Start and enable the automounter service.

   Use **systemctl** to start and enable the `autofs` service.

```
[user@host ~]$ sudo systemctl enable --now autofs
Created symlink /etc/systemd/system/multi-user.target.wants/autofs.service → /usr/lib/systemd/system/autofs.service.
```

**Direct Maps**

Direct maps are used to map an NFS share to an existing absolute path mount point.

To use directly mapped mount points, the master map file might appear as follows:

```
/-  /etc/auto.direct
```

All direct map entries use `/-` as the base directory. In this case, the mapping file that contains the mount details is `/etc/auto.direct`.

The content for the `/etc/auto.direct` file might appear as follows:

```
/mnt/docs  -rw,sync  serverb:/shares/docs
```

The mount point (or key) is always an absolute path. The rest of the mapping file uses the same structure.

In this example the `/mnt` directory exists, and it is not managed by `autofs`. The full directory `/mnt/docs` will be created and removed automatically by the `autofs` service.

**Indirect Wildcard Maps**

When an NFS server exports multiple subdirectories within a directory, then the automounter can be configured to access any one of those subdirectories using a single mapping entry.

Continuing the previous example, if `serverb:/shares` exports two or more subdirectories and they are accessible using the same mount options, then the content for the `/etc/auto.demo` file might appear as follows:

```
*  -rw,sync  serverb:/shares/&
```

The mount point (or key) is an asterisk character (*), and the subdirectory on the source location is an ampersand character (&). Everything else in the entry is the same.

When a user attempts to access `/shares/work`, the key `*` (which is `work` in this example) replaces the ampersand in the source location and `serverb:/shares/work` is mounted. As with the indirect example, the `work` directory is created and removed automatically by `autofs`.

##### Summary

In this chapter, you learned how to:

- Mount and unmount an NFS export from the command line.
- Configure an NFS export to automatically mount at startup.
- Configure the automounter with direct and indirect maps, and describe their differences.
- Configure NFS clients to use NFSv4 using the new **nfsconf** tool.



## Chapter 15. Managing Network Security

##### **Pre-defined Zones**

Firewalld has pre-defined zones, each of which you can customize. By default, all zones permit any incoming traffic which is part of a communication initiated by the system, and all outgoing traffic. The following table details these initial zone configuration.



**Table 15.1. Default Configuration of Firewalld Zones**

| Zone name  | Default configuration                                        |
| :--------- | :----------------------------------------------------------- |
| `trusted`  | Allow all incoming traffic.                                  |
| `home`     | Reject incoming traffic unless related to outgoing traffic or matching the `ssh`, `mdns`, `ipp-client`, `samba-client`, or `dhcpv6-client` pre-defined services. |
| `internal` | Reject incoming traffic unless related to outgoing traffic or matching the `ssh`, `mdns`, `ipp-client`, `samba-client`, or `dhcpv6-client` pre-defined services (same as the `home` zone to start with). |
| `work`     | Reject incoming traffic unless related to outgoing traffic or matching the `ssh`, `ipp-client`, or `dhcpv6-client` pre-defined services. |
| `public`   | Reject incoming traffic unless related to outgoing traffic or matching the `ssh` or `dhcpv6-client` pre-defined services. *The default zone for newly added network interfaces.* |
| `external` | Reject incoming traffic unless related to outgoing traffic or matching the `ssh` pre-defined service. Outgoing IPv4 traffic forwarded through this zone is *masqueraded* to look like it originated from the IPv4 address of the outgoing network interface. |
| `dmz`      | Reject incoming traffic unless related to outgoing traffic or matching the `ssh` pre-defined service. |
| `block`    | Reject all incoming traffic unless related to outgoing traffic. |
| `drop`     | Drop all incoming traffic unless related to outgoing traffic (do not even respond with ICMP errors). |

For a list of available pre-defined zones and intended use, see **firewalld.zones**(5).

##### **Pre-defined Services**

Firewalld has a number of pre-defined services. These service definitions help you identify particular network services to configure. Instead of having to research relevant ports for the `samba-client` service, for example, specify the pre-built `samba-client` service to configure the correct ports and protocols. The following table lists the pre-defined services used in the initial firewall zones configuration.



**Table 15.2. Selected Pre-defined Firewalld Services**

| Service name    | Configuration                                                |
| :-------------- | :----------------------------------------------------------- |
| `ssh`           | Local SSH server. Traffic to 22/tcp                          |
| `dhcpv6-client` | Local DHCPv6 client. Traffic to 546/udp on the fe80::/64 IPv6 network |
| `ipp-client`    | Local IPP printing. Traffic to 631/udp.                      |
| `samba-client`  | Local Windows file and print sharing client. Traffic to 137/udp and 138/udp. |
| `mdns`          | Multicast DNS (mDNS) local-link name resolution. Traffic to 5353/udp to the 224.0.0.251 (IPv4) or ff02::fb (IPv6) multicast addresses. |



##### **Configuring the Firewall from the Command Line**

The **firewall-cmd** command interacts with the **firewalld** dynamic firewall manager. It is installed as part of the main *firewalld* package and is available for administrators who prefer to work on the command line, for working on systems without a graphical environment, or for scripting a firewall setup.

The following table lists a number of frequently used **firewall-cmd** commands, along with an explanation. Note that unless otherwise specified, almost all commands will work on the *runtime* configuration, unless the `--permanent` option is specified. If the `--permanent` option is specified, you must activate the setting by also running the **firewall-cmd --reload** command, which reads the current permanent configuration and applies it as the new runtime configuration. Many of the commands listed take the `--zone=*ZONE*` option to determine which zone they affect. Where a netmask is required, use CIDR notation, such as 192.168.1/24.

| **firewall-cmd** commands                      | Explanation                                                  |
| :--------------------------------------------- | :----------------------------------------------------------- |
| **--get-default-zone**                         | Query the current default zone.                              |
| **--set-default-zone=ZONE**                    | Set the default zone. This changes both the runtime and the permanent configuration. |
| **--get-zones**                                | List all available zones.                                    |
| **--get-active-zones**                         | List all zones currently in use (have an interface or source tied to them), along with their interface and source information. |
| **--add-source=CIDR [--zone=ZONE]**            | Route all traffic coming from the IP address or network/netmask to the specified zone. If no `--zone=` option is provided, the default zone is used. |
| **--remove-source=CIDR [--zone=ZONE]**         | Remove the rule routing all traffic from the zone coming from the IP address or network/netmask network. If no `--zone=` option is provided, the default zone is used. |
| **--add-interface=INTERFACE [--zone=ZONE]**    | Route all traffic coming from `*INTERFACE*` to the specified zone. If no `--zone=` option is provided, the default zone is used. |
| **--change-interface=INTERFACE [--zone=ZONE]** | Associate the interface with `*ZONE*` instead of its current zone. If no `--zone=` option is provided, the default zone is used. |
| **--list-all [--zone=ZONE]**                   | List all configured interfaces, sources, services, and ports for `*ZONE*`. If no `--zone=` option is provided, the default zone is used. |
| **--list-all-zones**                           | Retrieve all information for all zones (interfaces, sources, ports, services). |
| **--add-service=SERVICE [--zone=ZONE]**        | Allow traffic to `*SERVICE*`. If no `--zone=` option is provided, the default zone is used. |
| **--add-port=PORT/PROTOCOL [--zone=ZONE]**     | Allow traffic to the `*PORT/PROTOCOL*` port(s). If no `--zone=` option is provided, the default zone is used. |
| **--remove-service=SERVICE [--zone=ZONE]**     | Remove `*SERVICE*` from the allowed list for the zone. If no `--zone=` option is provided, the default zone is used. |
| **--remove-port=PORT/PROTOCOL [--zone=ZONE]**  | Remove the `*PORT/PROTOCOL*` port(s) from the allowed list for the zone. If no `--zone=` option is provided, the default zone is used. |
| **--reload**                                   | Drop the runtime configuration and apply the persistent configuration. |

The example commands below set the default zone to `dmz`, assign all traffic coming from the `192.168.0.0/24` network to the `internal` zone, and open the network ports for the `mysql` service on the `internal` zone.

```
[root@host ~]# firewall-cmd --set-default-zone=dmz
[root@host ~]# firewall-cmd --permanent --zone=internal \
--add-source=192.168.0.0/24
[root@host ~]# firewall-cmd --permanent --zone=internal --add-service=mysql
[root@host ~]# firewall-cmd --reload
```

##### Summary

- The `netfilter` subsystem allows kernel modules to inspect every packet traversing the system. All incoming, outgoing or forwarded network packets are inspected.
- The use of **firewalld** has simplified management by classifying all network traffic into zones. Each zone has its own list of ports and services. The `public` zone is set as the default zone.
- The `firewalld` service ships with a number of pre-defined services. They can be listed using the **firewall-cmd --get-services** command.