# RHCSA 8 - Review & Notes

## Performing Basic System Management Tasks

### Chapter 1 Installing Red Hat Enterprise Linux Server

#### Notes

* RHEL 7 by default uses the XFS file system. This file system cannot be
  shrunk; it can only be expanded. Therefore, it is sometimes a better choice to use
  ext4.



___________

### Chapter 2 Using Essential Tools

#### Notes

Standard Input, Output, and Error Overview

|  Name  | Default destination | Use in Redirection | File Descriptor Number |
| :----: | :-----------------: | :----------------: | :--------------------: |
| STDIN  |  Computer keyboard  |   < (same as 0<)   |           0            |
| STDOUT |  Computer monitor   |   > (same as 1>)   |           1            |
| STDERR |  Computer monitor   |         2>         |           2            |

2> Redirects STDERR.
2>&1 Redirects STDERR to the same destination as STDOUT.

 ■ /etc/profile: This is the generic file that is processed by all users upon login.
■ /etc/bashrc: This file is processed when subshells are started.
■ ~/.bash_profile: In this file, user-specific login shell variables can be defined.
■ ~/.bashrc: In this user-specific file, subshell variables can be defined.

* Instead of using man -k , you can use the **apropos** command. This command
  is equivalent to man -k . man -k is to find the correct man page. **mandb** is for update the db

#### “Do I Know This Already?” Quiz

* Which of the following enables you to redirect standard output as well as
  errors to a file?
  a. 1&2> file
  **b. 2>&1 file**
  c. >1&2 file
  d. 1>2& file

  Explanation: Answer B shows the correct syntax. The 2 redirects error mes-
  sages, and &1 makes sure that the same is done for STDOUT.

* Which of the following commands enables you to replace every occurrence of
  old with new in a text file that is opened with vi?
  **a. :%s/old/new/g**
  b. :%r/old/new/
  c. :s/old/new/g
  d. r:/old/new

  Explanation: You need the command :%s/old/new/ to replace all instances
  of old with new . % means that it must be applied on the entire file. s stands for
  substitute. The g option is used to not apply the command to only the first
  occurrence in a line (which is the default behavior) but on all occurrences.

### Review Questions

1. **What can you add to a command to make sure that it does not show any error message, assuming that you do not care about the information that is in the error messages either?**

  2> /dev/null

2. **How do you read the current contents of the PATH variable?**
   echo $PATH



_____________

### Chapter 3 Essential File Management Tools

Chapter 4 Working with Text Files
Chapter 5 Connecting to a RHEL Server
Chapter 6 User and Group Management
Chapter 7 Permissions Management
Chapter 8 Configuring Networking



## Operating Running Systems

Chapter 9 Process Management
Chapter 10 Working with Virtual Machines
Chapter 11 Installing Software Packages
Chapter 12 Scheduling Tasks
Chapter 13 Configuring Logging
Chapter 14 Managing Partitions
Chapter 15 Managing LVM Logical Volumes



## Performing Advanced System Administration Tasks

Chapter 16 Basic Kernel Management
Chapter 17 Configuring a Basic Apache Server
Chapter 18 Managing and Understanding the Boot Procedure
Chapter 19 Essential Boot Procedure Troubleshooting



## Managing Network Services

Chapter 20 Using Kickstart
Chapter 21 Managing SELinux
Chapter 22 Configuring a Firewall
Chapter 23 Configuring Remote Mounts and FTP
Chapter 24 Configuring Time Services