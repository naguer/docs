# Practice Exam from Sanders Videos



1. Reset the root password

   ```
   systemctl reboot
   edit grub -> rd.break
   mount -o remount,rw /sysroot
   chroot /sysroot
   passwd
   touch /.autorelabel
   reboot
   ```

   

2. 
   Configuring a repository. Loop mount the installation disk/ISO that you've used to set up RHEL 8. Configure the loop-mounted ISO as a repository and configure your system to use only this repository

   ```
   mount -t iso9660 -o loop path/to/image.iso /repo
   cd /etc/yum.repos.d && rm -f *
   vim BaseOs.repo
   [BaseOs]
   name=Base Os Files
   baseurl=file:///repo/BaseOs
   gpgcheck=0
   Vim AppStrem.repo
   
   [AppStrem]
   name=AppStrem
   baseurl=file:///repo/AppStrem
   gpgcheck=0
   
   yum repolist
   ```

   

3. In the remaining disk space on your server hard disk, add a 1GiB partition. Do this in such a way that it's possible to add more partitions later. Format this partition with the Ext4 file system and set the label dbfiles on the partition. Configure your system to mount this partition persistently on the directory /dbfiles using the partition label. 

   ```
   gdisk /dev/sda
   p -> n -> e (because i have 3 partitions already) -> Enter -> Enter
   (Created a new partition 4 of type Extended and of size X Gib)
   n -> logical -> Enter -> +1GiB -> l -> t (83) -> w 
   partprobe
   mkfs.ext4 /dev/sda1 -L dbfiles
   mkdir /dbfiles
   fstab -> LABEL=dbfiles /dbfiles ext4 defaults 0 0 
   systemctl daemon-reload
   mount -a
   ```

   

4. Create a 2 GiB LVM volume group with the name vgdata.
   In this vg, create a 1 GiB lv with the name lvdata
   Format this lv with the XFS file system and mount it persistently on the directory /lvdata
   Restart your computer, and after a restar add another 500 MiB to the XFS fs that was created on this logical volume.

   ```
   yum install lvm2
   pvcreate /dev/sda
   vgcreate vgdata /dev/sda
   lvcreate vgdata -n lvdata -L 1G
   mkfs.xfs /dev/mapper/vgdata-lvdata
   blkid
   mkdir /lvdata
   fstab -> UUID=35645... /lvdata xfs defaults 0 0
   systemctl daemon-reload
   mount -a
   reboot
   lvextend -L +500M -r /dev/vgdata/lvdata
   ```

   

5. Set password for users to expire after 90 days. Also ensure that new passwords must have a lenght of at least 6 characters.
   Ensure that while creating new users, an empty file with the name newfile is created in ther home directories.
   Create users victoria and karen. Make them a member of the group students as a secondary group membership.
   Create users anna and amy. Make them a member of the group profs as a secondary group membership.

   ```
   /etc/login.defs -> PASS_MAX_DAYS 90 ; PASS_MIN_LEN 6
   touch /etc/skel/newfile
   useradd victoria
   useradd karen
   useradd anna
   useradd amy
   groupadd students
   groupadd profs
   usermod victoria -aG students
   usermod karen -aG students
   usermod anna -aG profs
   usermod amy -aG profs
   ```

   

6. Create shared group directories /data/students and /data/profs and ensure that members of the group students have full access to /data/students, and members of profs have full access to /data/profs. 
   The others entity should have no access at all.
   Ensure that all new files in these directories are automatically group owned by the group owner of the directory.
   Ensure that only the owner of a file is allowed to remove files.
   User anna is head-master and should be allowed to remove all files.
   All users from the group profs should have read permissions on all files in /data/students.

   ```
   mkdir -p /data/students /data/profs
   chown :students /data/students
   chown :profs /data/profs
   chmod 770 /data/students
   chmod 770 /data/profs
   chmod 2770 /data/students
   chmod 2770 /data/profs
   chmod 3770 /data/students
   chmod 3770 /data/profs
   chmod anna /data/students
   chmod anna /data/profs
   setfacl -m g:profs:rx /data/students (existing files)
   setfacl -m d:g:profs:rx /data/students (new files)
   getfacl students
   ```

   

7. Schedule a cron job to automatically write the text "Hello World" to syslog at every 10 minutes after the hour. Ensure this message is written with the "notice" priority.

   ```
   crontab -e
   10 * * * * logger -p notice "Hello World"
   ```

   