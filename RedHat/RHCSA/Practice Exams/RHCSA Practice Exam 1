# RHCSA Practice Exam 1



This test exam requires the following setup:

- A cleanly installed RHEL 8 virtual machine with the name server3.
- Unless specifically mentioned, all tasks described next should be performed on the virtual machine.

1. Bring down the virtual machine you have pre-installed and, in the KVM software, add an additional 5GiB hard disk to the virtual machine.
2. A repository is available at http://rhatcert.com/repo. Configure your server3 to use this repository and disable usage of any other repositories.
3. Create four users: bill, bob, betty, and belinda. Set their passwords to expire after 120 days and make sure they get a home directory in /home/users. (This means that, for instance, bill has /home/users/bill as his home directory.)
4. Create two groups: consultants and trainers. Make bill and bob members of the group consultants without overwriting any of their current group memberships. Make belinda and betty members of the group trainers without changing any of their current group memberships.
5. Create a shared group environment that meets the following requirements:
   - The group consultants has full read/write access to the directory /groups/consultants.
   - The group trainers has full read/write access to the directory /groups/consultants.
   - bill is head of the consultants department and should be able to remove files that have been created by any user in /groups/consultants. Any other members of the group consultants should have no rights to remove files they haven't created themselves.
   - betty is head of the trainers department and should be able to remove files that have been created by any user in /groups/trainers. Any other members of the group trainers should have no rights to remove files they haven't created themselves.
   - All new files created in /groups/trainers should automatically get group-owned by the group trainers.
   - All new files created in /groups/consultants should automatically get group-owned by the group consultants.
   - Members of the group consultants should be able to read files in /groups/trainers.
6. Create an LVM logical volume with the name lvfiles. This volume should have a size of 1GiB, and it should be allocated from a volume group with the name vgfiles. Format this volume with the ext4 file system and mount it persistently on the directory /files.
7. Enable the performance profile that optimizes your server for best throughput.
8. Create a scheduled job that will send the message "hello" to the system-logging mechanism at the top of each hour.
9. Make the systemd journal persistent.



__________________

**Responses:**

1. **Bring down the virtual machine you have pre-installed and, in the KVM software, add an additional 5GiB hard disk to the virtual machine.**

2. **A repository is available at http://rhatcert.com/repo. Configure your server3 to use this repository and disable usage of any other repositories.**

   ```
   yum repolist
   sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/*.repo
   yum-config-manager --add http://rhatcert.com/repo
   ```

   

3. **Create four users: bill, bob, betty, and belinda. Set their passwords to expire after 120 days and make sure they get a home directory in /home/users. (This means that, for instance, bill has /home/users/bill as his home directory.)**

   ```
   useradd -m -d /home/users/bill bill
   useradd -m -d /home/users/bob bob
   useradd -m -d /home/users/betty betty
   useradd -m -d /home/users/belinda belinda
   chage bob -M 120
   chage bill -M 120
   chage betty -M 120
   chage belinda -M 120
   ```

   

4. **Create two groups: consultants and trainers. Make bill and bob members of the group consultants without overwriting any of their current group memberships. Make belinda and betty members of the group trainers without changing any of their current group memberships.**

   ```
   groupadd consultants
   groupadd trainers
   usermod -aG consultants bill
   usermod -aG consultants bob
   usermod -aG trainers belinda
   usermod -aG trainers betty
   ```

   

5. **Create a shared group environment that meets the following requirements:**

   - **The group consultants has full read/write access to the directory /groups/consultants.**

     ```
     mkdir -p /groups/consultants
     chown :consultants -R /groups/consultants
     chmod 775 /groups/consultants
     ```

     

   - **The group trainers has full read/write access to the directory /groups/trainers.**

     ```
     mkdir -p /groups/trainers
     chown :trainers -R /groups/trainers
     chmod 775 /groups/trainers
     ```

     

   - **bill is head of the consultants department and should be able to remove files that have been created by any user in /groups/consultants. Any other members of the group consultants should have no rights to remove files they haven't created themselves.**

     ```
     chown bill:consultants /groups/consultants
     chmod o+t /groups/consultants
     ```

     

   - **betty is head of the trainers department and should be able to remove files that have been created by any user in /groups/trainers. Any other members of the group trainers should have no rights to remove files they haven't created themselves.**

   - **All new files created in /groups/trainers should automatically get group-owned by the group trainers.**

     ```
     chmod g+s /groups/trainers
     ```

     

   - **All new files created in /groups/consultants should automatically get group-owned by the group consultants.**

     ```
     chmod g+s /groups/consultants/
     ```

     

   - **Members of the group consultants should be able to read files in /groups/trainers.**

     ```
     chmod 775 /group/consultants
     ```

     

6. **Create an LVM logical volume with the name lvfiles. This volume should have a size of 1GiB, and it should be allocated from a volume group with the name vgfiles. Format this volume with the ext4 file system and mount it persistently on the directory /files.**

   ```
   yum install lvm2
   pvcreate /dev/sda
   vgcreate vgfiles /dev/sda
   lvcreate vgfiles -n lvfiles -L 1GiB
   mkfs.xfs /dev/mapper/vgfiles-lvfiles
   udevadm settle
   blkid 
   mkdir /files
   /etc/fstab -> UUID=3432-432423 /files xfs default 0 0
   systemctl daemon-reload
   mount -a
   ```

   

7. **Enable the performance profile that optimizes your server for best throughput.**

   ```
   tuned-adm profile throughput-performance
   ```

   

8. **Create a scheduled job that will send the message "hello" to the system-logging mechanism at the top of each hour.**

   ```
   echo "logger hello" > /etc/cron.hourly/hello.cron
   ```

   

9. **Make the systemd journal persistent.**

   ```
   mkdir /var/log/journal
   echo "persistent|volatile|auto" >> /etc/systemd/journald.conf
systemctl restart systemd-journald
   ```
   
   

