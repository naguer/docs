## RHCSA Practice Exam C

### GENERAL NOTES

Here are some tips to ensure your exam starts with a clean environment:

- You do not need any external servers or resources.
- Do *not* register or connect to any external repositories.
- Install a new VM according to the instructions in each practice exam.
- No sample solutions are provided for these practice exams. On the real exam, you need to be able to verify the solutions for yourself as well.
- You should be able to complete each exam within two hours.

1. Install a RHEL 8 or CentOS 8 virtual machine that meets the following requirements:
   - 2 GB of RAM
   - 20 GB of disk space using default partitioning
   - One additional 20-GB disk that does not have any partitions installed
   - Server with GUI installation pattern
2. Create user **student** with password **password**, and user **root** with password **password**.
3. Configure your system to automatically loop-mount the ISO of the installation disk on the directory **/repo**. Configure your system to remove this loop-mounted ISO as the only repository that is used for installation. Do *not* register your system with **subscription-manager**, and remove all reference to external repositories that may already exist.
4. Reboot your server. Assume that you don’t know the root password, and use the appropriate mode to enter a root shell that doesn’t require a password. Set the root password to **mypassword**.
5. Set default values for new users. Make sure that any new user password has a length of at least six characters and must be used for at least three days before it can be reset.
6. Create users **edwin** and **santos** and make them members of the group sales as a secondary group membership. Also, create users **serene** and **alex** and make them members of the group account as a secondary group.
7. Create shared group directories **/groups/sales** and **/groups/account**, and make sure these groups meet the following requirements:
   - Members of the group sales have full access to their directory.
   - Members of the group account have full access to their directory.
   - Users have permissions to delete only their own files, but Alex is the general manager, so user alex has access to delete all users’ files.
8. Create a 4-GiB volume group, using a physical extent size of 2 MiB. In this volume group, create a 1-GiB logical volume with the name **myfiles** and mount it persistently on /myfiles.
9. Create a group **sysadmins**. Make users edwin and santos members of this group and ensure that all members of this group can run all administrative commands using **sudo**.
10. Optimize your server with the appropriate profile that optimizes throughput.
11. Add a new disk to your virtual machine with a size of 10 GiB. On this disk, create a VDO volume with a size of 50 GiB and mount it persistently.
12. Configure your server to synchronize time with serverabc.example.com, where serverabc is an alias to myserver.example.com. Note that this server does not have to exist to accomplish this exercise.
13. Configure a web server to use the non-default document root /webfiles. In this directory, create a file **index.html** that has the contents **hello world** and then test that it works.