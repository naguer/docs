## RHCSA Practice Exam 3



Interrupt the boot process and reset the root password. Change it to “wander” to gain access to the system.
Repos are available from the repo server at http://repo.eight.example.com/BaseOS and and http://repo.eight.example.com/AppStream for you to use during the exam.
The system time should be set to your (or nearest to you) timezone and ensure NTP sync is configured.
Add the following secondary IP addresses statically to your current running interface. Do this in a way that doesn’t compromise your existing settings:
IPV4 - 10.0.0.5/24
IPV6 - fd01::100/64
Enable packet forwarding on system1. This should persist after reboot.
System1 should boot into the multiuser target by default and boot messages should be present (not silenced).
Create a new 2GB volume group named “vgprac”.
Create a 500MB logical volume named “lvprac” inside the “vgprac” volume group.
The “lvprac” logical volume should be formatted with the xfs filesystem and mount persistently on the /mnt/lvprac directory.
Extend the xfs filesystem on “lvprac” by 500MB.
Use the appropriate utility to create a 10TiB thin provisioned volume.
Configure a basic web server that displays “Welcome to the web server” once connected to it. Ensure the firewall allows the http/https services.
Find all files that are larger than 5MB in the /etc directory and copy them to /find/largefiles
Write a script named awesome.sh in the root directory on client1.
If “me” is given as an argument, then the script should output “Yes, I’m awesome.”
If “them” is given as an argument, then the script should output “Okay, they are awesome.”
If the argument is empty or anything else is given, the script should output “Usage ./awesome.sh me|them”
Create users phil, laura, stewart, and kevin.
All new users should have a file named “Welcome” in their home folder after account creation.
All user passwords should expire after 60 days and be atleast 8 characters in length.
phil and laura should be part of the “accounting” group. If the group doesn’t already exist, create it.
stewart and kevin should be part of the “marketing” group. If the group doesn’t already exist, create it.
Only members of the accounting group should have access to the “/accounting” directory. Make laura the owner of this directory. Make the accounting group the group owner of the “/accounting” directory.
Only members of the marketing group should have access to the “/marketing” directory. Make stewart the owner of this directory. Make the marketing group the group owner of the “/marketing” directory.
New files should be owned by the group owner and only the file creator should have the permissions to delete their own files.
Create a cron job that writes “This practice exam was easy and I’m ready to ace my RHCSA” to /var/log/messages at 12pm only on weekdays.