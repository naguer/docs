d

# Certified Kubernetes Administrator

The Certified Kubernetes Administrator or CKA is a hands-on test and consists of a set of performance-based items (15 to 20 problems) to be solved in a command line and is expected to take approximately 2 hours to complete. The exam is based on Kubernetes v1.20

**My notes are from the Kubernetes official documentation and a part from the KodeKloud Course.** 

https://www.cncf.io/certification/cka/

https://github.com/cncf/curriculum/blob/master/CKA_Curriculum_v1.20.pdf

https://kodekloud.com/p/kubernetes-certification-course

**Exan Objectives:**

| Domain                                             | Weight |
| -------------------------------------------------- | ------ |
| Cluster Architecture, Installation & Configuration | 25%    |
| Workloads & Scheduling                             | 15%    |
| Service & Networking                               | 20%    |
| Storage                                            | 10%    |
| Troubleshooting                                    | 30%    |



## Core Concepts

### ETCD

Consistent and highly-available **key value** store used as Kubernetes' backing store for all cluster data.

**ETCD Basic**
Etcd service runs in port 2379. If we install ETCD and we want to save a key value is very simple

```
./etcdctl set key1 value1
```

ectdctl is the command line for Etcd

If we want to read the data, also is very easy.

```
./etcdctl get key1
value
```

**ETCD on Kubernetes**

Every information we see when we run the `kubectl get` is from the ETCD server (Nodes, PODs, Configs, Secrets, Accounts, Roles, Bindings, others)

* Two methods to deploy on K8S

  * Setup manual: Download binaries, and install it as a service

  * Setup kubeadm: Deployed as a pod in kube-system (etcd-master)
    Get all keys

    ```
    kubectl exec etc-master -n kube-system etcdctl get / --prefix -keys-only
    ```



### Kube API Server

The API server is a component of the Kubernetes [control plane](https://kubernetes.io/docs/reference/glossary/?all=true#term-control-plane) that exposes the Kubernetes API. The API server is the front end for the Kubernetes control plane.
kube-apiserver is designed to scale horizontally—that is, it scales by deploying more instances. You can run several instances of kube-apiserver and balance traffic between those instances.



When we run a `kubectl` command, this command is infact reaching to the kube-apiserver, and when the kube-apiserver validates the access, reach the ETCD for the information. We can also do the same using the API with a post request. 

```
curl -X post /api/v1/namespaces/default/pods... 
```

The Kube-api server is responsible for:

1. Authenticate User 
2. Validate Request
3. Retrieve data
4. Update ETCD
5. Scheduler
6. Kubelet

**Note:** kube-api server is the only component that interacts directly with the ETCD Datastore



### Kube Controller Manager

Control Plane component that runs [controller](https://kubernetes.io/docs/concepts/architecture/controller/) processes.

Logically, each [controller](https://kubernetes.io/docs/concepts/architecture/controller/) is a separate process, but to reduce complexity, they are all compiled into a single binary and run in a single process.

Some types of these controllers are:

- **Node controller:** Responsible for noticing and responding when nodes go down.
- **Replication controller:** Monitoring the status of replica sets and ensuring that the desired number of pods are available at all times within the set. If a pods dies it creates another one.
- **Job controller:** Watches for Job objects that represent one-off tasks, then creates Pods to run those tasks to completion.
- **Endpoints controller:** Populates the Endpoints object (that is, joins Services & Pods).
- **Service Account & Token controllers:** Create default accounts and API access tokens for new namespaces.

**Note:** A controller is a process that continuously monitors the state of various components within the system and works towards bringing the whole system to the desired functioning state



### Kube Scheduler

Control plane component that watches for newly created [Pods](https://kubernetes.io/docs/concepts/workloads/pods/) with no assigned [node](https://kubernetes.io/docs/concepts/architecture/nodes/), and selects a node for them to run on. (Scheduler is only responsible for deciding, Kubelet creates the pod on the nodes)
Factors taken into account for scheduling decisions include: individual and collective resource requirements, hardware/software/policy constraints, affinity and anti-affinity specifications, data locality, inter-workload interference, and deadlines.



### Kubelet

An agent that runs on each [node](https://kubernetes.io/docs/concepts/architecture/nodes/) in the cluster. It makes sure that [containers](https://kubernetes.io/docs/concepts/containers/) are running in a [Pod](https://kubernetes.io/docs/concepts/workloads/pods/).

The kubelet takes a set of PodSpecs that are provided through various mechanisms and ensures that the containers described in those PodSpecs are running and healthy. The kubelet doesn't manage containers which were not created by Kubernetes.

Note: Kubeadm does not deploy Kubelet, we to do that manually, install the kubelet on our worker nodes,
Download the installer, extract, and install.



### Kube-proxy

kube-proxy is a network proxy that runs on each [node](https://kubernetes.io/docs/concepts/architecture/nodes/) in your cluster, implementing part of the Kubernetes [Service](https://kubernetes.io/docs/concepts/services-networking/service/) concept.

[kube-proxy](https://kubernetes.io/docs/reference/command-line-tools-reference/kube-proxy/) maintains network rules on nodes. These network rules allow network communication to your Pods from network sessions inside or outside of your cluster.

kube-proxy uses the operating system packet filtering layer if there is one and it's available. Otherwise, kube-proxy forwards the traffic itself.

**Note:** Its job is to look for new services and every time a new service is created it creates the appropriate
rules on each node to forward traffic to those services to the backend pods, one way it does this is using IPTABLES rules. In this case it creates an IP tables rule on each node in the cluster to forward traffic heading to the IP of the service to the IP of the actual pod

**Note:** Kubeproxy it is deployed as a Daemon Set, so a simple POD is always deployed on each node in the cluster.

```
kubectl get daemonset -n kube-system
```



## Scheduling 

Here we take a closer look at the various options available for customizing and configuring the way the scheduler behaves through different topics.

### Manual Scheduling

If we don't have a Kube Scheduler we can schedule the Pods ourself.

In a pod definition we can configure a `nodeName` (for default isn't set).
Kubernetes with the Kube Scheduler checks if any pods doesn't have this property set, and choose in which node to schedule, and creates a binding object.

If we don't have a Scheduler the pods will maintain in "pending state", in this case we can manually assign pods to node ourself. For example:

```
apiVersion: v1
kind: Pod
...
spec:
	nodeName: node02
	containers:
	- name: nginx
	  image: nginx
```



### Labels and Selector

Labels and selectors are a standard method to group things together.



#### Labels

*Labels* are key/value pairs that are attached to objects, such as pods. Labels are intended to be used to specify identifying attributes of objects that are meaningful and relevant to users. 

Labels allow for efficient queries and watches and are ideal for use in UIs and CLIs. Non-identifying information should be recorded using [annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/).

pod-definitions.yaml

```
apiVersion: v1
kind: Pod
metadata:
	name: simple-webapp
	labels:
		app: App1
		function: Front-end
spec:
	containers:
	- name: simple-webapp
	  image: simple-webapp
	  ports:
	    - containerPort: 8080
```



#### Label selectors

Unlike [names and UIDs](https://kubernetes.io/docs/concepts/overview/working-with-objects/names/), labels do not provide uniqueness. In general, we expect many objects to carry the same label(s).

Via a *label selector*, the client/user can identify a set of objects. The label selector is the core grouping primitive in Kubernetes.

We can select pods with specified labels

```
kubectl get pods --selector app=App1
```



For example, when we use ReplicaSet, we use Labels and Selectors

replicaset-definition.yaml

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
	name: simple-webapp
	labels:
		app: App1
		function: Front-end
spec:
	replicas: 3
	selector:
		matchLabels:
			app: App1
	template:
		metadata:
			labels:
				app: App1
				function: Front-end
			containers:
			- name: simple-webapp
			  image: simple-webapp
```

**Note:** Labels in the template section is for the pods, the labels in the top are the labels for the replica set it self.


Exercises:

* Select pod with multiple Labels
* Select ALL resource with a Label



### Taints and tolerations

*Taints* allow a node to repel a set of pods.

*Tolerations* are applied to pods, and allow (but do not require) the pods to schedule onto nodes with matching taints.

Taints and tolerations work together to ensure that pods are not scheduled onto inappropriate nodes.

**Note:**  Taints are set on nodes, and tolerations are set on pods



#### Taint nodes

The taint-effect defines what would happen to the pods if they do not tolerate the taint, there are three main effects:

- NoSchedule: the pods will not be scheduled on the node

- PreferNoSchedule: the system will try to avoid placing the pod on the node, but that is not guaranteed.

- NoExecute: Pods will not be scheduled on the node and existing pods on the node, if any, will be evicted if they do not tolerate the taint

  ```
  kubectl taint nodes node-name key=value:taint-effect
  ```

  Example:

  ```
  kubectl taint nodes node1 app=blue:NoSchedule
  ```

  

#### Tolerations Pods

pod-definition.yml

```
...
spec:
	containers:
	- name: nginx-container
	  image: nginx
	tolerations:
	- key:"app"
	  operator: "Equal"
	  value: "blue"
	  effect: "NoSchedule"
```

With this "toleration" the pod can be deployed on the `node1` with the taint

Note: A taint is set to the master node and automatically that prevents any pods from being scheduled there on master nodes. We can see this taint

```
kubectl describe node kubemaster | grep Taint
Taints:			node-role.kubernetes.io/master:NoSchedule
```



```
kubectl describe node kubemaster | grep Taint
Taints:			node-role.kubernetes.io/master:NoSchedule
```



### Node Selectors

`nodeSelector` is the simplest recommended form of node selection constraint. `nodeSelector` is a field of PodSpec. It specifies a map of key-value pairs. pods/pod-nginx.yaml

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    env: test
spec:
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
  nodeSelector:
    disktype: ssd
```

The Pod will get scheduled on the node that you attached the label to. You can verify that it worked by running `kubectl get pods -o wide` and looking at the "NODE" that the Pod was assigned to



#### Label nodes

List the [nodes](https://kubernetes.io/docs/concepts/architecture/nodes/) in your cluster, along with their labels:

```shell
$ kubectl get nodes --show-labels
NAME      STATUS    ROLES    AGE     VERSION        LABELS
worker0   Ready     <none>   1d      v1.13.0        ...,kubernetes.io/hostname=worker0
worker1   Ready     <none>   1d      v1.13.0        ...,kubernetes.io/hostname=worker1
```

Chose one of your nodes, and add a label to it:

```shell
kubectl label nodes <your-node-name> disktype=ssd
```

Example:

```
kubectl label nodes node-1 size=large
```



#### Node Selector Limitations

We can't configure complex labels, such as "Large OR Medium" "Not Small" for something like this we need "Node Affinity"



### Node Affinity

Node affinity is conceptually similar to `nodeSelector` -- it allows you to constrain which nodes your pod is eligible to be scheduled on, based on labels on the node.

There are currently two types of node affinity, called `requiredDuringSchedulingIgnoredDuringExecution` and `preferredDuringSchedulingIgnoredDuringExecution`. You can think of them as "hard" and "soft" respectively, in the sense that the former specifies rules that *must* be met for a pod to be scheduled onto a node (just like `nodeSelector` but using a more expressive syntax), while the latter specifies *preferences* that the scheduler will try to enforce but will not guarantee. The "IgnoredDuringExecution" part of the names means that, similar to how `nodeSelector` works, if labels on a node change at runtime such that the affinity rules on a pod are no longer met, the pod will still continue to run on the node. In the future we plan to offer `requiredDuringSchedulingRequiredDuringExecution` which will be just like `requiredDuringSchedulingIgnoredDuringExecution` except that it will evict pods from nodes that cease to satisfy the pods' node affinity requirements.  


pods/pod-with-node-affinity.yaml

```
apiVersion: v1
kind: Pod
metadata:
  name: with-node-affinity
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: kubernetes.io/e2e-az-name
            operator: In
            values:
            - e2e-az1
            - e2e-az2
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 1
        preference:
          matchExpressions:
          - key: another-node-label-key
            operator: In
            values:
            - another-node-label-value
  containers:
  - name: with-node-affinity
    image: k8s.gcr.io/pause:2.0
```



Set Node Affinity to the deployment to place the pods on `node01` only

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: blue
spec:
  replicas: 6
  selector:
    matchLabels:
      run: nginx
  template:
    metadata:
      labels:
        run: nginx
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: nginx
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: color
                operator: In
                values:
                - blue
```



### Resource Requirements

When you specify a [Pod](https://kubernetes.io/docs/concepts/workloads/pods/), you can optionally specify how much of each resource a [Container](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/#why-containers) needs. The most common resources to specify are CPU and memory (RAM); there are others.

When you specify the resource *request* for Containers in a Pod, the scheduler uses this information to decide which node to place the Pod on. When you specify a resource *limit* for a Container, the kubelet enforces those limits so that the running container is not allowed to use more of that resource than the limit you set. The kubelet also reserves at least the *request* amount of that system resource specifically for that container to use.



#### Requests and limits

If the node where a Pod is running has enough of a resource available, it's possible (and allowed) for a container to use more resource than its `request` for that resource specifies. However, a container is not allowed to use more than its resource `limit`.

For example, if you set a `memory` request of 256 MiB for a container, and that container is in a Pod scheduled to a Node with 8GiB of memory and no other Pods, then the container can try to use more RAM.

If you set a `memory` limit of 4GiB for that Container, the kubelet (and [container runtime](https://kubernetes.io/docs/setup/production-environment/container-runtimes)) enforce the limit. The runtime prevents the container from using more than the configured resource limit. For example: when a process in the container tries to consume more than the allowed amount of memory, the system kernel terminates the process that attempted the allocation, with an out of memory (OOM) error.



#### Resource Requests

```
apiVersion:v1
kind: Pod
...
spec:
	containers:
	- name: simple-webapp-color
	  image: simple-webapp-color
	  resources:
	    requests:
	      memory: "1Gi"
	      cpu: 1
```



### DaemonSets



### Static PODs



### Multiple Schedulers



## Logging and Monitoring



### Monitor Cluster Components

This topic is about how we monitor resource consuption on Kubernetes or what we you like to monitor. For example we want to know the numbers of nodes in the cluster, the performance metrics such as CPU, memory and so on. Also we can know POD metrics such as pod memory, cpu, etc. 
K8s doest not come with a built-in monitoring solution, some solutions are: 

* Metric server: The most basic built-in monitoring solution. The metric server retrieces metrics from each Kubernetes nodes and pods, aggregates them and stores them in memory (Does not store in Disk because of that we cannot see historical performance data)
* Prometheus
* Elastic Stack
* Datadog
* Dynatrace

Kubelet Agent runs on each node and is responsible for receiving instructions from the Kubernetes API master server and running PODs on the nodes.
The kubelet also contains a subcomponent know as cAdvisor or Container Advisor. cAdvisor is responsible for retrieving performance metrics from pods, and exposing them through the kubelet API to make the metrics available for the Metrics Server. 

`kubectl top node` provides the CPU and Memory consumption of each of the nodes. Needs the Metric server installed. 

Other good command is  `kubectl top pods` , this command allow us to view CPU/Mem of the pods. 

### Managing Application Logs

The most simple way to view app logs is using 

```
kubectl logs -f POD_NAME CONTAINER_NAME
```

The argument `CONTAINER_NAME` is only necessary if we have more than on container in a pod.

## Application Lifecycle Management

This sections has a lot of topics overlapping with CKAD.



### Rolling Updates and Rollbacks



### Commands and Arguments in Kubernetes



### Configure Environment Variables in Applications



### Configure ConfigMaps in Applications



### Secrets



### Multicontainers PODs



### Init Containers



## Storage

This point is the same that CKAD, for this reason i only put some examples in each topic.

### PV

```
apiVersion: v1
kind: PersistentVolume
metadata:
	name: pv-vol1
spec:
	accessModes:
		- ReadWriteOnce
	capacity:
		storage: 1Gi
	awsElasticBlockStore:
		volumeID: 253jd2du
		fsType: ext4
```

### PVC

Pods consume node resources and PVCs consume PV resources. Pods can request specific levels of resources (CPU and Memory). Claims can request specific size and access modes (e.g., they can be mounted ReadWriteOnce, ReadOnlyMany or ReadWriteMany)

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
	name: myclaim
spec:
	accessModes:
		- ReadWriteOnce
	resources:
		requests:
		storage: 500Mi
```

**Note:** PVCs will automatically bind themselves to a PV that has compatible StorageClass and accessModes



### PVCs in PODs

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
```



### Access Modes

The access modes are:

- ReadWriteOnce -- the volume can be mounted as read-write by a single node (RWO)
- ReadOnlyMany -- the volume can be mounted read-only by many nodes (ROX)
- ReadWriteMany -- the volume can be mounted as read-write by many nodes (RWX)



### Reclaim Policy

- Retain -- manual reclamation
- Recycle -- basic scrub (`rm -rf /thevolume/*`) This is obsolete with Automatic Provisioning
- Delete -- associated storage asset such as AWS EBS, GCE PD, Azure Disk, or OpenStack Cinder volume is deleted

### Storage Class

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
 name: mypvc
 namespace: testns
spec:
 accessModes:
 - ReadWriteOnce
 resources:
   requests:
     storage: 100Gi
 storageClassName: gold
```

**Note:** The default StorageClass is marked by `(default)`



## Networking

### CoreDNS

[CoreDNS](https://coredns.io/) is a flexible, extensible DNS server that can serve as the Kubernetes cluster DNS.
CoreDNS running the kubernetes plugin can be used as a replacement for kube-dns in a kubernetes cluster.



### Networking Namespaces

With network namespaces, you can have different and separate instances of network interfaces and routing tables that operate independent of each other.



### Docker Networking

The type of network a container uses, whether it is a [bridge](https://docs.docker.com/network/bridge/), an [overlay](https://docs.docker.com/network/overlay/), a [macvlan network](https://docs.docker.com/network/macvlan/), or a custom network plugin, is transparent from within the container. From the container’s point of view, it has a network interface with an IP address, a gateway, a routing table, DNS services, and other networking details (assuming the container is not using the `none` network driver). 



### CNI

CNI (Container Network Interface) consists of a specification and libraries for writing plugins to configure network interfaces in Linux containers, along with a number of plugins. CNI concerns itself only with network connectivity of containers and removing allocated resources when the container is deleted.

Kubernetes uses CNI as an interface between network providers and Kubernetes pod networking.

CNI comes with a set of supported plugins already, such as bridger, VLAN, IPVLAN, MACVLAN. 

### Cluster Networking

Networking is a central part of Kubernetes, but it can be challenging to understand exactly how it is expected to work. There are 4 distinct networking problems to address:

1. Highly-coupled container-to-container communications: this is solved by [Pods](https://kubernetes.io/docs/concepts/workloads/pods/) and `localhost` communications.
2. Pod-to-Pod communications: this is the primary focus of this document.
3. Pod-to-Service communications: this is covered by [services](https://kubernetes.io/docs/concepts/services-networking/service/).
4. External-to-Service communications: this is covered by [services](https://kubernetes.io/docs/concepts/services-networking/service/).

Kubernetes is all about sharing machines between applications. Typically, sharing machines requires ensuring that two applications do not try to use the same ports. Coordinating ports across multiple developers is very difficult to do at scale and exposes users to cluster-level issues outside of their control.



### Network Addons

Installing a network plugin in the cluster.

https://kubernetes.io/docs/concepts/cluster-administration/addons/

https://kubernetes.io/docs/concepts/cluster-administration/networking/#how-to-implement-the-kubernetes-networking-model

In the CKA exam, for a question that requires you to deploy a network addon, unless specifically directed, you may use any of the solutions described in the link above.

**However** the documentation currently does not contain a direct reference to the exact command to be used to deploy a third party network addon.

The links above redirect to third party/ vendor sites or GitHub repositories which cannot be used in the exam. This has been intentionally done to keep the content in the Kubernetes documentation vendor neutral.

At this moment in time, there is still one place within the documentation where you can find the exact command to deploy weave network addon:

https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/#steps-for-the-first-control-plane-node (step 2)

## Cluster Architecture, Installation & Configuration

### Manage role based access control (RBAC)

### Use Kubeadm to install a basic cluster

### Manage a highly-available Kubernetes cluster

### Provision underlying infrastructure to deploy a Kubernetes cluster

### Perform a version upgrade on a Kubernetes cluster using Kubeadm

### Implement etcd backup and restore






