# Helm

Helm helps you manage Kubernetes applications — Helm Charts help you define, install, and upgrade even the most complex Kubernetes application.

Charts are easy to create, version, share, and publish — so start using Helm and stop the copy-and-paste.

Helm is a graduated project in the [CNCF](https://cncf.io/) and is maintained by the [Helm community](https://github.com/helm/community).

## Install Helm

Install Helm on Linux

```
> wget https://get.helm.sh/helm-v3.4.2-linux-amd64.tar.gz 
> tar -zxvf helm-v3.4.2-linux-amd64.tar.gz 
> sudo mv linux-amd64/helm /usr/local/bin
> helm version              
version.BuildInfo{Version:"v3.4.2", GitCommit:"23dd3af5e19a02d4f4baa5b2f242645a1a3af629", GitTreeState:"clean", GoVersion:"go1.14.13"}
```



## Charts Concept



## Obtain Charts



## Customizing Charts



## Repositories



## Releases



## Extra

I want to add an good video introduction to Helm by the CNCF

https://www.youtube.com/watch?v=Zzwq9FmZdsU&t=2s



Resources:

* https://helm.sh/
* https://helm.sh/docs/intro/install/