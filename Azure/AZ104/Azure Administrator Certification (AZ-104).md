# Azure Administrator Certification (AZ-104)

https://docs.microsoft.com/en-us/learn/certifications/exams/az-104



## Skills measured

- Manage Azure identities and governance (15-20%)
- Implement and manage storage (10-15%)
- Deploy and manage Azure compute resources (25-30%)
- Configure and manage virtual networking (30-35%)
- Monitor and back up Azure resources (10-15%)



## Azure Concepts 18

* Core Services

  * Virtual Machines

    * Windows or Linux OS
    * Can be remotely connected using RDS or SSH
    * Looks and acts like a real server but isn't
    * Can be placed on a virtual network arranged in "Availability Sets", placed behind "load balancers"
    * Install whatever software you wish
    * A server can be created in a few minutes
    * VM Abstractions
      * Azure Batch
      * Virtual Machine Scale Sets
      * AKS
      * Service Fabric

  * App Services

    * Web apps or container apps
    * Windows or Linux OS
    * Fully-managed servers, no ability to remote control
    * .NET, .NET Core, Java, Ruby, Python, etc
    * Lots of benefits in scaling, CI, deployment slot

  * Virtual Networking

  * Storage

    * Create storage accounts up to 5 PB each
    * Blobs, queues, tables, files
    * Various levels of replication included from local to global
    * Storage tiers (hot, cool, archive)
    * Managed (for VMs) or unmanaged

  * Data Services - SQL Server

    * Azure SQL Database
    * Azure SQL Managed Instance
    * SQL Server on a VM
    * Synapse Analytics (SQL Data Warehouse)

  * Data Services - Other

    * Cosmos DB
    * Azure Database for Mysql
    * Azure Database for PostgreSQL
    * Azure Database for MariaDB
    * Azure Cache for Redis

  * Microservices

    * Service Fabric
    * Azure Functions
    * Azure Logic Apps
    * API Management
    * AKS

  * Networking Connectivity

    * Virtual Network (VNet)
    * Virtual WAN 
    * ExpressRoute
    * VPN Gateway
    * Azure DNS
    * Peering

  * Networking Security

    * Network Security Groups (NSG)
    * Azure Private Link
    * DDoS Protection
    * Azure Firewall
    * WAF
    * Virtual Network Endpoints

  * Networking Delivery

    * CDN
    * Azure Front Door
    * Traffic Manager
    * Application Gateway
    * Load Balancer

  * Networking Monitoring

    * Network Watcher
    * ExpressRoute Monitor
    * Azure Monitor
    * VNet Tap

    

## Powershell and CLI 32



## Manage Resources Groups 11

A resource group is a container that holds related resources for an Azure solution. The resource group can include all the resources for the solution, or only those resources that you want to manage as a group. You decide how you want to allocate resources to resource groups based on what makes the most sense for your organization. Generally, add resources that share the same lifecycle to the same resource group so you can easily deploy, update, and delete them as a group.

**Note:** The resource group stores metadata about the resources. Therefore, when you specify a location for the resource group, you are specifying where that metadata is stored. For compliance reasons, you may need to ensure that your data is stored in a particular region.



**Lock resource group**

Locking prevents other users in your organization from accidentally deleting or modifying critical resources, such as Azure subscription, resource group, or resource. Two types: Read Only and Delete



**Resource group policies**

Some policies can affect RG for example "Apply tag and its default value to RG" or for example "Enforce Tagging" or "Enforcing some regions"



**Move Resources**

You can move the resources in the group to another resource group or move the resource to another subscription

Links:

* https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/manage-resource-groups-portal
* 

## Manage Subscription and Governance 44



## Monitor resources by using Azure Monitor 46



## Create and Configure Storage Accounts 60



