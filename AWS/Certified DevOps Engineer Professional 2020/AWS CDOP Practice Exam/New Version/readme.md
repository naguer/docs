AWS Certified DevOps Engineer - Professional Practice exam

Overall Score: 60%

Topic Level Scoring:
1.0  SDLC Automation: 50%
2.0  Configuration Management and Infrastructure as Code: 50%
3.0  Monitoring and Logging: 66%
4.0  Policies and Standards Automation: 100%
5.0  Incident and Event Response: 50%
6.0  High Availability, Fault Tolerance, and Disaster Recovery : 50%

