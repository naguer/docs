AWS Certified Devops Engineer Professional – Practice Exam 

Overall Score: 70%

Topic Level Scoring:
1.0  Continuous Delivery and Process Automation: 72%
2.0  Monitoring, Metrics, and Logging: 100%
3.0  Security, Governance, and Validation: 0%
4.0  High Availability and Elasticity: 66%
