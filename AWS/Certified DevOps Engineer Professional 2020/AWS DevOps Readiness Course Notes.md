# AWS DevOps Readiness Course Notes



## Domain 1: SDLC Automation



Continuous Integration Goals:

* Automatically kick off a new release when new code is checked in
* Build and test code in a consistent, repeatable environment
* Continually have an artifact ready for deployment
* Continually close feedback loop when builds fails



Continuous Deployment Goals:

* Automatically deploy new changes to staging environment for testing
* Deploy to production safely without impacting customers
* Deliver to customer faster: Increase deployment frequency, and reduce change lead time and change failure rate



Integration Test:

* More complex and are dependent on multiples modules
* Generally shouldn't expose bugs in business logic
  * Should expose bugs in the interaction between the modules



Unit Test:

* Test smaller units and are easier to test
* Should flush out bugs in business logic



* Codestar have the config file template.yml



## Domain 2: Configuration Management and Infrastructure as Code



**CloudFormation Updating Stacks**

**Original Stack** -> Create Change Set -> **Change Set** ->  View Change Set -> **Inspect Changes** -> Execute Change Set -> **AWS CloudFormation**

3 Types of updating Stacks:

* Updates with no interruption:  Without Downtime and Without changing its physical name
* Updates with some interruption: Update with some interruption and Without changing its physical name
* Replacement:  Resource is recreated and a new physical ID is generated for the resources.
  

* **CloudFormation Wait Condition** can be used to ensure required resources are running. WaitCondition pauses the execution of our CNF template and waits for a number of success signals before it continues a stack creation operation.


* **CloudFormation Creation Policy** is used to EC2 and ASC and is helpful if you provision an EC2 instance using user data. By default,  when CNF creates and EC2 instance it will not wait for the operating system and application to be ready. With a creation policy you can ask CloudFormation to wait for an external signal. 


* Waitconditions are external, an additional logical resource, whereas a creation policy is inside that resource.

* **StackPolicies** protect how and who can modified the resources of the stack. All resources are open by default, but if we add a StackPolicy the behavior changes to deny all resources. This provide a very secure mechanism for the updates


* **StackSets** allow the manages of multiple stack on different accounts


* The **cfn-hup** helper is a daemon that detects changes in resource metadata and runs user-specified actions when a change is detected. This allows you to make configuration updates on your running Amazon EC2 instances through the UpdateStack API action. The default interval for check changes is 15 minutes
  

* **ApiGateway** support REST and WebSocket


* **Elastic Beanstalk Worker Environment** is connected to an SQS  queue and running a daemon process on each instance that reads from the queue for you. These worker instances take items from the queue only when they have capacity to run them, preventing them from becoming overwhelmed. 



## Domain 3: Monitoring and Logging

**CloudWatch Key Metrics for ELB:** ELB 5xx, HTTPCODE_ELB_4xx, Latency, SurgeQueueLength, SpilloverCount

**SurgeQueueLength:** Total number of request
**SpilloverCount:** Request dropped from SurgeQueueLength

**Custom Metrics:** To get custom metrics first we need to install the **Amazon CloudWatch Logs agent** (such as disk usage and mem usage)

**Monitor HTTP status code metrics:**

* Increase in **BackendConnectionErrors** errors: The LB may still be warming up
* **HTTPCode_Backend_5xx** errors: Instances might be at capacity, or databases might be overloaded. Check instance/database metrics to verify.
* **HTTPCode_ELB_4xx** errors: Check instance logs connections to instances might be timing out



**CloudTrail Best Practices**

* Enable in all regions
* Enable log file validation
* Encrypted logs
* Integrate with Amazon CloudWatch logs
* Centralize logs from all accounts
* Create additional trails as needed
  

**AWS X-Ray**

* Identify performance bottlenecks and errors
* Pinpoint issues to specific services in your app
* Identify impact of issues on users of the app
* Visualize the service call graph of your application
  

**VPC Flow Logs**

* Captures IP traffic flow details in your VPC
* Accepted, rejected, or all traffic
* Can be enabled for VPCs, subnets, and ENIs
* Logs Published to CloudWatch Logs



## Domain 4: Policies and Standards Automation

**IAM Big picture:**

* Users (Permanent) 
* Group (Best Practice) 
* IAM Policy (Authorization)
* Role (Temporary Authentication)

Role requirements:

* Trust Policy: who is allowed to assume the role
* Access permissions policy: what actions and resources the one assuming the role is allowed to do

USER NEEDS TO USE ROLES EVERY TIME THEY CAN

Amazon GuardDuty: Network Instruction detection using ML

* Protects AWS accounts and workloads
* Monitor your AWS environment for suspicious activity and generates findings
* Allows you to add your own threat lists and trusted IP lists
* Analyzes multiple data sources for default (In background uses machine learning to looking patrons)
  * CloudTrail events, VPC Flow Logs, and DNS Query Logs



**Amazon Inspector:** Analise automatically OS security (Need an agent)

* Automated assessments that help improve security and compliance of applications
* Offers an agent-based solution
* Detects vulnerabilities 
* Verifies security best practices
* Generate findings report
* Pre created rules or you can create your own rules packages



**AWS System Manager features:** 

* Run Command * 

* State Manager

* Inventory

* Maintenance Window

* Patch Manager * 

* Automation

* Parameter Store *

* Session Manager  

  (* Most important features)

  

**AWS Trusted Advisor:** Provides guidance to help you (Is like CloudConformity )

* Reduce cost
* Increase performance
* Improve security



**AWS Service Catalog**: Another way to enforce standards and limits

* Create and manage catalogs of approved IT services
* Limit access to underlying AWS services
* Helps achieve consistent governance and compliance requirements
* Enable turn-key self-service solutions for all end users



## Domain 5: Incident and Event Response

**Suspending Auto Scaling Services**

* Scaling processes
* Detach an instance
  * Move an instance from one Auto Scaling group to another
  * Testing purposes for running your application
* Standby state
  * Remove the instance from service, troubleshoot or make changes to it, put it back into service
  * Doesn't actively handle application traffic until you put them back into service

- Launch
- Terminate
- AlarmNotifications (Accepts notifications form CloudWatch alarms that are associated with the group)
- ReplaceUnhealthy (Terminates instances that are marked as unhealthy and later creates new instances to replace them)
- HealthCheck
- AZRebalance (Balances the number of EC2 instances across the AZ in the region)
- AddToLoadBalancer



**Auto Scaling termination policies** (we can customize this)

* Default
  * Ensures instances span AZ evenly for HA
* Oldest instance
  * Upgrading instances in the ASG to a new EC2 instance type
* Newest instance
  * Testing a new launch configuration but don't want to keep it in production
  * Can be customized
* Allocation Strategy
  * Gradually replace On-Demand instances of a lower priority type with On-Demand instances of a higher priority type
* OldestLaunchConfig
* OldestLaunchTemplate
* ClosesToNextInstanceHour



**Suspending Load Balancing services**

* Disable AZs

  * Targets in that AZ remain registered with the load balancer, but the load balancer will not route requests to them

* Configure slow start mode (Resources start online but in the beginning only a little part of traffic)

* Target groups

  * Deregister targets in a TG
  * Delete a TG

* Delete a LB

  * You can't delete a lob if deletion protection is enabled

  

**Codedeploy lifecycle events in Autoscaling.** 

Start * -> ApplicationStop -> DownloadBundle * -> BeforeInstall -> Install * -> AfterInstall -> ApplicationStart -> ValidateService -> End

The events with * are Events you cannot control



## Domain 6: High Availability, Fault Tolerance, and Disaster Recovery



**Recovery Point Objective (RPO)** How often does data need to be backed up?
Example: The business can recover from losing the last 12 hours of data

**Recovery Time Objective (RTO)** How Long can the application be unavailable? 
Example: The application can be unavailable for a maximum of 1 hour

**Disaster recovery on AWS**

* Backup And Restore: RPO/RTO Hours
  * Lower priority use cases
  * Solutions: S3, EBS
  * Cost: $
* Pilot light: RPO/RTO Minutes
  * Meeting lower RTO & RPO requirements
  * Core services
  * Scale AWS resources in response to a DR event
  * Cost: $$
* Warm Standby: RPO/RTO Seconds
  * Solutions that requiere RTO/RPO in minutes
  * Business critical
  * Cost: $$$
* Hot Standby (with Multi-site): RPO/RTO Real-Time
  * Auto-failover of your environment in AWS
  * Cost: $$$$