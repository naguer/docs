# AWS Certified DevOps Engineer Professional Practice Exams



## Linux Academy

* CodeCommit does not have a repository access policy feature and therefore to protect the master branch you need to set a **Deny policy** on the IAM group that your developers should be assigned to.
  
* **Instance protection** protect for the AUTOMATIC scale in.  
To protect Auto Scaling instances from manual termination, enable **termination protection**.
  If you want configure `Instance protection` for each instance on ASG, you can do in the creating part, set Instance Protection to `Protect from Scale In`
  
  The instance protection begin when the instance state is `InService`
  
*  You can use the CloudWatch Logs agent installer on an existing EC2 instance, after installation is complete, logs automatically flow from the instance to the log stream you create while installing the agent. 
  
  AWS unified things and now offers a "CloudWatch Agent" for metrics and logs.
  
* The CloudWatch Statistics are: Average, Minimum, Maximum, Sum, SampleCount, pNN.NN (The value of the specified percentile.)





* A **stack policy** is a JSON document that defines the AWS **CloudFormation stack** update actions that AWS **CloudFormation** users can perform and the resources that the actions apply to. Can only have one **stack policy** per stack and can protect specific resources from specific update actions
* The cfn-init helper script reads template metadata from the AWS::CloudFormation::Init key and acts accordingly to:
  * Fetch and parse metadata from AWS CloudFormation
  * Install packages
  * Write files to disk
  * Enable/disable and start/stop services
  
* CFN-HUP use to check for updates to metadata and execute custom hooks when changes are detected. The hup daemon checks for changes in intervals, and it can take up to the interval period before the change is reflected. 
* CloudFormation Custom resources enable you to write custom provisioning logic in templates that AWS CloudFormation runs any time you create, update (if you change the custom resource), or delete stacks. For example, you might want to include resources that aren't available as AWS CloudFormation resource types. You can include those resources by using custom resources. That way you can still manage all your related resources in a single stack.
* CloudFormation creation policies work very similarly to the way that WaitConditions work, except they are the preferred method when dealing with EC2 instances and Auto Scaling configurations. With a CreationPolicy, we specify how many success signals we need to receive as well as how long the timeout needs to be. To use a creation policy, we can associate it with a resource in our stack. This will prevent that resource's status from reaching CREATE_COMPLETE until the Creation Policy receives the required number of signals. We can signal back using helper scripts or through the SignalResource API or CLI call. As those signals are coming in, we can see them in the events. CloudFormation only invokes the CreationPolicy when the associated resource gets created
* CFN-signal to signal with a CreationPolicy or WaitCondition, so you can synchronize other resources in the stack when the prerequisite resource or application is ready.



* OpsWorks Lifecycle events, each layer has a set of five lifecycle events (setup, configure, deploy, undeploy, shutdown), each of which has an associated set of recipes that are specific to the layer. When an event occurs on a layer's instance, AWS OpsWorks Stacks automatically runs the appropriate set of recipes. 

* OpsWorks Rollbacks: We can rollback up to four previous versions because OpsWorks stores the five most recent deployments. We can do that using the Rollback command, just like the Deploy command. We also execute the Undeploy command, which triggers the Undeploy lifecycle event and can be used to remove all versions of the app from instances if we want to start from a clean slate.
  You can have more than for previous versions if you use S3 with Versioning
  
* You can associate multiple RDS with one OpsWorks Stack

* You need to create instances before adding them to layers (first you need create the OpsWorks Stack)

* If you need instances with automatically given an IP address you need to configure in the Network tab within the layer
  
* Multicontainer Docker instances on Elastic Beanstalk require a configuration file named `Dockerrun.aws.json`
  
* ALB is capable (currently) of using 25 certificates and allows for SNI so a single IP can be used to support multiple SSL certificates. 
  
* Dynamo GSI's use their own WCU and RCU - every write to the table has a corresponding sparse write to a GSI. If the GSI performance is throttled, so is the tables and can generate an error.
  
* Amazon EC2 uses an instance profile as a container for an IAM role. When you create an IAM role using the IAM console, the console creates an instance profile automatically and gives it the same name as the role to which it corresponds. If you use the Amazon EC2 console to launch an instance with an IAM role or to attach an IAM role to an instance, you choose the role based on a list of instance profile names.
  If you want a instance profile with a different name than the IAM role you need to create using the CLI
  
* ElasticBenstalk Save Current State: You can save your environment's configuration as an object in Amazon S3 that can be applied to other environments during environment creation, or applied to a running environment. Saved configurations are YAML formatted templates that define an environment's platform configuration, tier, configuration option settings, and tags.
  
* To enable access logs for your load balancer, you must specify the name of the Amazon S3 bucket where the load balancer will store the logs. You can use these access logs to analyze traffic patterns and to troubleshoot your back-end applications.

  You must also attach a bucket policy to this bucket that grants Elastic Load Balancing permission to write to the bucket. 
  
  ELB Access Logs: 
  
  - Timestamp
  - Backend:Port
  - Backend-Processing-Time (latencies)
  - Request-Processing-Time (latencies)
  - Received_bytes
  - IP Address
  - Request path
  - server response
  
  


* CloudTrail can store API logs to S3 along with a digest which can be used to validate the authenticity of the logs. The "validate-logs" command will only function if the logs remain in their original location as delivered by AWS.
  
* IAM role credentials are automatically rotated on a frequent basis 
  


* **Containers can make use of IAM roles via task roles**, OR, they can utilize roles applied to the ECS containers instance.
  
* During an immutable environment update, the capacity of your environment doubles for a short time when the instances in the new Auto Scaling group start serving requests and before the original Auto Scaling group's instances are terminated. If your environment has many instances, or instances with a low on-demand limit, ensure that you have enough capacity to perform an immutable environment update. If you are near the limit, consider using rolling updates instead. 
  


* If you implemented a Lifecycle Hook ASG and are viewing a scale-up operation and the new instance remain in a Pending:Wait state for 30 min (more than enough time to provision), we can complete the lifecycle action using the `complete-lifecycle-action` command or the `CompleteLifecycleaction` operation
  
* ELB send metrics to CW only when requests are flowing through the LB, If there are requests flowing through the load balancer, Elastic Load Balancing measures and sends its metrics in 60-second intervals.
  
  Elastic Load Balancing publishes a log file for each load balancer node at the interval you specify. You can specify a publishing interval of either 5 minutes or 60 minutes when you enable the access log for your load balancer. By default, Elastic Load Balancing publishes logs at a 60-minute interval. If the interval is set for 5 minutes, the logs are published at 1:05, 1:10, 1:15, and so on. 
  
* **ASG Default Termination Policy:**
  
  * Determine which Availability Zones have the most instances, and at least one instance that is not protected from scale in.
  * Determine which instances to terminate so as to align the remaining instances to the allocation strategy for the On-Demand or Spot Instance that is terminating.
  * Determine whether any of the instances use the oldest launch template or configuration.
  * After applying all of the above criteria, if there are multiple unprotected instances to terminate, determine which instances are closest to the next billing hour. If there are multiple unprotected instances closest to the next billing hour, terminate one of these instances at random.
  
  
  

____





## Stephane Maarek

* `InsufficientCapabilitiesException` means that the CloudFormation stack is trying to create an IAM role but we haven't provided that capability explicitly to CloudFormation. If for example we need this for Create a Codepipeline resource using Cnf, we need to enable the IAM Capability on the CodePipeline configuration for the Deploy CloudFormation stage Action

* API Gateway APIs can directly invoke an AWS service and pass in a payload. It's a common way to provide a publicly available and secure API for your chosen AWS services.  For example if you need to integrate API gateway and Step Functions together you can, you don't need Lambda in the middle. 
  
* Inspector is leveraged to find security vulnerabilities on EC2 instances, not to get a metadata list of your installed packages. Use SSM Inventory for this.
  
* A DynamoDB table with three use cases for read is an mistake. No more than two processes at most should be reading from the same streams shard at the same time. Having more than two readers per shard can result in throttling.
  One solution is have a Lambda that with read from DynadmoDB stream and pass the payload to SNS, have the 3 use cases read directly from the SNS topic.

* If we need to look for an event on Cloudtrail and trigger a notification we can do this if we enable CloudTrail, create a CloudWatch Event rule to track that API call, and use SNS as target, we don't need send the CloudTrail Logs to CloudWatch Logs (We can but is very expensive)
  
* **The CodeDeploy deployment must be associated with a CloudWatch Alarm for automated rollbacks.**
  
* If we deploy an application onto an ASG using CodeDeploy with Rolling Update and the ASG has five instance running, and at the end of the deployment it seems three instances are running the new version, and two the old version is because If an Amazon EC2 Auto Scaling scale-up event occurs while a deployment is underway, the new instances will be updated with the application revision that was most recently deployed, not the application revision that is currently being deployed. 
  To avoid this problem, we recommend suspending the Amazon EC2 Auto Scaling scale-up processes while deployments are taking place. You can do this through a setting in the common_functions.sh script that is used for load balancing with CodeDeploy. 

* **CodePipeline cannot invoke another CodePipeline. This is something you might be able to achieve using a Custom Action and a Lambda function, but you would need to make sure artifacts are copied locally as well.**
  
* If you have a CICD pipeline orchestrated by CodePipeline with setup on us-east-1 and you want extend to deploy the app in us-east-2 we need and the end of the pipeline include an S3 step to copy the artifacts being used by CodeDeploy to an S3 bucket in us-east-2 and make the CodePipeline in us-east-2 source files from S3
  
* **The best tool for create AMI is AWS Automation document**, we can create in a master account and share the AMI with other accounts. If we want only the last AMI available we can un-share the previous AMI
  
* The `runOrder` value on CloudFormation template is for permit stage actions in parallel when we use CodePipeline.
  The default `runOrder` value for an action is 1. For example, if you want three actions to run in sequence in a stage, you would give the first action the `runOrder` value of 1, the second action the `runOrder` value of 2, and the third the `runOrder` value of 3.
  
* If we need to do Troubleshooting from an Instance running in a ASG, Auto Scaling Hooks may work but they come with a one hour default timeout, if we need more time we need to set the instance in **Standby** right after it has launched. Instances that are on standby are still part of the Auto Scaling group, but they **do not actively handle application traffic.**
  
* **You cannot modify an AWS managed IAM policy, but you can add a new IAM policy and attached to a group.** 

* The cron.yml file must be defined on the beanstalk worker tier (you can't do this in the web tier)
  
* If we need AWS Config in some accounts, and have the information centralize, we need to enable AWS Config in all accounts and create an **AWS Config aggregator for a centralized account.** 
  
* If you use OpsWorks and you have noticed that a lot of instances have been automatically replaced in your stack, and you want a notification, you need to use a CloudWatch Event, and the value of `initiated_by` must be `auto-healing`
  
* If we need a high rage of 10000 objects peer second write on a S3 and encryption, is better use SSE-S3 than SSE-KMS.
  
* **On RDS you can still perform a rolling upgrade (major version of MYSQL) by creating a read replica, upgrading the replica, promoting the replica, and then routing traffic to the promoted replica.**

* In CloudFormation the best practice is to separate stacks into individual, separate logical components that have dependencies on each other. To link through these dependencies, the best is to use Exports and Imports. Each individual CloudFormation template must be a separate file. Use outputs and exports to reference values in the stacks.
  
* **ECS task definitions that include the `awslogs` driver can write to CloudWatch Logs natively.** Also need an IAM instance role on the EC2

* We can integrated directly AWS Config managed rules and CloudWatch Event rule to provide alerting (without SNS topic)
  
* To access DynamoDB table you don't need credential, only need an IAM role with a policy. 
  
* **CodeDeploy always deploys from S3, no matter what the source is.** (NOT USE CodeCommit)

* To perform Blue/Green in Elastic Beanstalk, you need to deploy to a new environment and do a CNAME swap. The CNAME swap feature is not supported by CloudFormation itself, therefore you need to create a custom Lambda function that will perform that API call for you to swap the CNAME of the environment and invoke it as part of a Custom Job in CodePipeline.



____

* Beanstalk differences between `commands` block and `container_commands` are the process they are run:

  **Commands:** These commands are run early in the deployment process, before the web server has been set up, and before your application code has been unpacked. 

  **Container Commands:** These commands are run later in the deployment process, after the web server has been set up, and after your application code has been unpacked to the staging folder, but before your application has been "deployed" (by moving the staging folder to its final location):

  `leader_only` Only run the command on a single instance chosen by Elastic Beanstalk. Leader-only container commands are run before other container commands. A command can be leader-only or have a `test`, but not both (`leader_only` takes precedence).


* `CODEBUILD_SOURCE_VERSION`: The value's format depends on the source repository.
  - For Amazon S3, it is the version ID associated with the input artifact.
  - For CodeCommit, it is the commit ID or branch name associated with the version of the source code to be built.
  - For GitHub, GitHub Enterprise, and Bitbucket it is the commit ID, branch name, or tag name associated with the version of the source code to be built.
* We can send logs from EC2 to S3 using Lambda invoking SSM Run Command (we don't need pass for CloudWatch Logs)
* If we need to make a EB deployment with a unsupported language the best way to do this, is using a Docker-Container configuration, another option is making a custom platform but is very complicated
* A worker tier must be used to asynchronously process these invoices from an SQS queue. SQS size limit is 256KB and therefore the files must be uploaded to S3 and a reference to them should be sent to SQS by the web tier. The worker tier is a separate environment from the web tier. Finally, the `cron.yml` file must be defined on the worker tier, it is not supported by the web tier. Using this strategy we have decoupled our processing tier from our web tier, and CPU usage will go down as a result. The worker tier will also be able to easily scale in case many invoices are uploaded.
* EB immutable Deployment on ASG & ALB creates a new temporary ASG. 



_____

AWS CloudFormation provides the following Python helper scripts that you can use to install software and start services on an Amazon EC2 instance that you create as part of your stack:

- [cfn-init](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-init.html): Use to retrieve and interpret resource metadata, install packages, create files, and start services.
- [cfn-signal](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-signal.html): Use to signal with a CreationPolicy or WaitCondition, so you can synchronize other resources in the stack when the prerequisite resource or application is ready.
- [cfn-get-metadata](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-get-metadata.html): Use to retrieve metadata for a resource or path to a specific key.
- [cfn-hup](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-hup.html): Use to check for updates to metadata and execute custom hooks when changes are detected.

You call the scripts directly from your template. The scripts work in conjunction with resource metadata that's defined in the same template. The scripts run on the Amazon EC2 instance during the stack creation process.



* EB Precedence:
  1) Settings applied directly to the environment via the console, CLIs, or SDKS, 2) saved configurations, 3) .ebextension configuration files.

* ELB Access log is disable by default
* Exists a Jenkins plugins for allows you to deploy into AWS Elastic Beanstalk by Packaging, Creating a new Application Version, and Updating an Environment
* Export and Import function in the Output section are used in cross-stack references (Fn:ImportValue), not Nested Stacks.
* To enable an IAM user to launch an instance with an IAM role or to attach or replace an IAM role for an existing instance, you must grant the user permission to pass the role to the instance. This can be done creating a policy with iam:PassRole.
* You can't MODIFY a Launch Configuration, you need to create a new one.
* CloudFormation ChangeSets
* Service Migration Service (SMS) is to migrate your on-premise virtualized servers to EC2, isn't perfom a P2V (physical to virtual)



## Jon Bonso



* You can set up a solution that uses a simple single-page website, hosted in an Amazon S3 bucket and using Amazon CloudFront. You can create a new Lambda@Edge function and associate it with your CloudFront distribution. Then configure the origin response trigger to execute the Lambda@Edge function add the security headers in the HTTP response.


* When we need capture all events in a CodeCommit repository, such as cloning a repo or creating a branch on a single location, we need to create a Lambda that send event logs to AWS CW Logs and set a **trigger** by selecting the CodeCommit Repository. On CodeCommit, go to the repository settings and select the Lambda function on the Triggers list when creating a new trigger.

  Although you can configure a trigger to use Amazon SNS to send emails about some repository events, those events are limited to operational events, such as creating branches and pushing code to a branch. Triggers do not use CloudWatch Events rules to evaluate repository events. They are more limited in scope.

  

* When we need a canary deployment with serverless we can use Api Gateway or Lambda Function Alias that points to both, the current and new versions. En both we can define the Weight
  
* In CodeDeploy the `BlockTraffic` hook internet traffic is blocked from accessing instances that are currently serving traffic. This event is reserved for the CodeDeploy agent and cannot be used to run scripts.
  
* You can use Amazon CloudWatch Events to detect and react to changes in the status of AWS Personal Health Dashboard (AWS Health) events. Then, based on the rules that you create, CloudWatch Events invokes one or more target actions when an event matches the values that you specify in a rule.
**(You can only create Metric filter for CloudWatch log groups)**
  
* By default, Amazon S3 allows both HTTP and HTTPS requests. To comply with the **s3-bucket-ssl-requests-only** rule, confirm that your bucket policies explicitly deny access to HTTP requests. Bucket policies that allow HTTPS requests without explicitly denying HTTP requests might not comply with the rule (This is not possible using IAM roles)

  To determine HTTP or HTTPS requests in a bucket policy, use a condition that checks for the key **“aws:SecureTransport”**. When this key is **true**, this means that the request is sent through HTTPS. To be sure to comply with the **s3-bucket-ssl-requests-only** rule, create a bucket policy that explicitly denies access when the request meets the condition **“aws:SecureTransport”: “false”**. This policy explicitly denies access to HTTP requests.
  
* *Instance metadata* is data about your instance that you can use to configure or manage the running instance. Instance metadata is divided into categories, for example, host name, events, and security groups.

  You can also use instance metadata to access *user data* that you specified when launching your instance. For example, you can specify parameters for configuring your instance, or include a simple script. You can build generic AMIs and use user data to modify the configuration files supplied at launch time. For example, if you run web servers for various small businesses, they can all use the same generic AMI and retrieve their content from the Amazon S3 bucket that you specify in the user data at launch. To add a new customer at any time, create a bucket for the customer, add their content, and launch your AMI with the unique bucket name provided to your code in the user data.
  
* In **AWS CodePipeline**, you **can add an approval action** to a stage in a pipeline at the point where you want the pipeline execution to stop so that someone with the required AWS Identity and Access Management permissions can approve or reject the action. If the action is approved, the pipeline execution resumes. If the action is rejected—or if no one approves or rejects the action within seven days of the pipeline reaching the action and stopping—the result is the same as an action failing, and the pipeline execution does not continue.
  
*  You have to use the bucket policy to enforce access to the bucket using HTTPS only and not an IAM role.


* Through an integration with Amazon CloudWatch Events, customers can now create events that automatically trigger Amazon Inspector assessments to run against your environments. Within Amazon CloudWatch Events, you can now create event rules that target your Amazon Inspector assessment templates. When that CloudWatch Event occurs, Amazon Inspector will automatically be notified to run the specified assessment. For example, you can create an event which monitors AWS Auto Scaling for new EC2 Instances being launched, or monitors AWS CodeDeploy notifications for when a code deployment has been successfully completed. Once CloudWatch Events have been configured against Amazon Inspector templates, these assessment events will be displayed in the Inspector console as part of your assessment templates so you can see all of the automated triggers for that assessment.
  
* To configure your hybrid servers and VMs for AWS Systems Manager, just follow these provided steps:

  1. Complete General Systems Manager Setup Steps
  2. Create an IAM Service Role for a Hybrid Environment
  3. Install a TLS certificate on On-Premises Servers and VMs
  4. Create a Managed-Instance Activation for a Hybrid Environment
  5. Install SSM Agent for a Hybrid Environment (Windows)
  6. Install SSM Agent for a Hybrid Environment (Linux)
  7. (Optional) Enable the Advanced-Instances Tier
     

* To restrict access to content that you serve from Amazon S3 buckets, you create CloudFront signed URLs or signed cookies to limit access to files in your Amazon S3 bucket, and then you create a special CloudFront user called an origin access identity (OAI) and associate it with your distribution. Then you configure permissions so that CloudFront can use the OAI to access and serve files to your users, but users can’t use a direct URL to the S3 bucket to access a file there.
  
* Decouple the Amazon RDS instance from your Elastic Beanstalk environment using the blue/green deployment strategy to decouple. Take an RDS DB snapshot of the database and enable deletion protection. Set up a new Elastic Beanstalk environment with the necessary information to connect to the Amazon RDS instance. Before terminating the old Elastic Beanstalk environment, remove its security group rule first before proceeding.
  
* **Use Amazon Inspector for automated security assessment to help improve the security and compliance of your applications.** 

**COMPLIANCE = INSPECTOR**

  

* DynamoDB supports two types of secondary indexes:

  – [Global secondary index](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GSI.html) — an index with a partition key and a sort key that can be different from those on the base table. A global secondary index is considered “global” because queries on the index can span all of the data in the base table, across all partitions.

  – [Local secondary index](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/LSI.html) — an index that has the same partition key as the base table, but a different sort key. A local secondary index is “local” in the sense that every partition of a local secondary index is scoped to a base table partition that has the same partition key value.

* When we need X-Ray daemon, we need to create a Docker image that runs this daemon, UDP 2000

  (The xray-daemon.config is used on EB )

  

* **AWS Trusted Advisor** draws upon best practices learned from serving hundreds of thousands of AWS customers. Trusted Advisor inspects your AWS environment, and then makes recommendations when opportunities exist to save money, improve system availability and performance, or help close security gaps. All AWS customers have access to five Trusted Advisor checks. Customers with a Business or Enterprise support plan can view all Trusted Advisor checks.
  AWS Trusted Advisor is integrated with the Amazon CloudWatch Events and Amazon CloudWatch services. You can use Amazon CloudWatch Events to detect and react to changes in the status of Trusted Advisor checks. And you can use Amazon CloudWatch to create alarms on Trusted Advisor metrics for check status changes, resource status changes, and service limit utilization. 

  For example we can Integrate CloudWatch Events rule and AWS Trusted Advisor to detect the EC2 instances with low utilization.  Create a trigger with an AWS Lambda function that filters out the reported data based on tags for each environment, department, and business unit. Create a second trigger that will invoke another Lambda function to terminate the underutilized EC2 instances.
  
* **Global DynamoDB Tables**, it will not automatically create new replica tables on all AWS regions. 

  You have to manually specify and create the replica tables in the specific AWS regions where you want to replicate your data. Take note as well that the DynamoDB Stream option must be enabled in order for the Global DynamoDB Table to work.
  
* **AWS Config** provides you a visual dashboard to help you quickly spot non-compliant resources and take appropriate action. You can use AWS Config rules to evaluate the configuration settings of your AWS resources. When AWS Config detects that a resource violates the conditions in one of your rules, AWS Config flags the resource as noncompliant and sends a notification

* An **Aurora global database** consists of one primary AWS Region where your data is mastered, and one read-only, secondary AWS Region. Aurora replicates data to the secondary AWS Region with typical latency of under a second. The Aurora cluster in the primary AWS Region where your data is mastered performs both read and write operations. The cluster in the secondary region enables low-latency reads. For disaster recovery, you can remove and promote the secondary cluster to allow full read and write operations.

  Only the primary cluster performs write operations. Clients that perform write operations connect to the DB cluster endpoint of the primary cluster.

  

* **We cannot create a snapshot using the Amazon RDS scheduled instance lifecycle events.**


* **Amazon Data Lifecycle Manager (DLM)** for EBS Snapshots provides a simple, automated way to back up data stored on Amazon EBS volumes. You can define backup and retention schedules for EBS snapshots by creating lifecycle policies based on tags.

* **Oracle RAC** is supported via the deployment using Amazon EC2 only since Amazon RDS and Aurora do not support it.

* You cannot create identity pools with guest identities using the AWS Identity and Access Management (IAM) service. You can only implement this using Amazon Cognito.
  
* AWSServiceRoleForOrganizations service-linked allow AWS Organizations to create service-linked roles for other AWS services
  
* Using the **Amazon Kinesis Adapter** is the recommended way to consume streams from Amazon DynamoDB. The DynamoDB Streams API is intentionally similar to that of Kinesis Data Streams, a service for real-time processing of streaming data at massive scale. In both services, data streams are composed of shards, which are containers for stream records.

  You can write applications for Kinesis Data Streams using the Kinesis Client Library (KCL). The KCL simplifies coding by providing useful abstractions above the low-level Kinesis Data Streams API.

  As a DynamoDB Streams user, you can use the design patterns found within the KCL to process DynamoDB Streams shards and stream records. To do this, you use the DynamoDB Streams Kinesis Adapter. The Kinesis Adapter implements the Kinesis Data Streams interface so that the KCL can be used for consuming and processing records from DynamoDB Streams.
  
* On ECS Fargate cluster by default, deployments are not forced but you can use the ***forceNewDeployment*** request parameter (or the –force-new-deployment parameter if you are using the AWS CLI) to force a new deployment of the service. You can use this option to trigger a new deployment with no service definition changes. For example, you can update a service’s tasks to use a newer Docker image with the same image/tag combination (my_image:latest) or to roll Fargate tasks onto a newer platform version.
  
* Nested stacks are stacks created as part of other stacks. You create a nested stack within another stack by using the [`AWS::CloudFormation::Stack`](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-stack.html) resource.
  
* The **VM Import/Export** enables you to easily import virtual machine images from your existing environment to Amazon EC2 instances and export them back to your on-premises environment.
  
* Use the **AWS::CloudFormation::CustomResource** or alternatively, the ***Custom::<User-Defined Resource Name>*** resource type to define custom resources in your templates. Custom resources require one property: the service token, which specifies where AWS CloudFormation sends requests to, such as an Amazon SNS topic.
  When you associate a Lambda function with a custom resource, the function is invoked whenever the custom resource is created, updated, or deleted. AWS CloudFormation calls a Lambda API to invoke the function and to pass all the request data (such as the request type and resource properties) to the function. The power and customizability of Lambda functions in combination with AWS CloudFormation enable a wide range of scenarios, such as dynamically looking up AMI IDs during stack creation, or implementing and using utility functions, such as string reversal functions.

  Normally, you might map AMI IDs to specific instance types and regions. To update the IDs, you manually change them in each of your templates. By using custom resources and AWS Lambda (Lambda), you can create a function that gets the IDs of the latest AMIs for the region and instance type that you’re using so that you don’t have to maintain mappings.
  
* **EC2Rescue** can help you diagnose and troubleshoot problems on Amazon EC2 Linux and Windows Server instances.

* In AWS CodePipeline The default `runOrder` value for an action is 1. The value must be a positive integer (natural number). You cannot use fractions, decimals, negative numbers, or zero. To specify a serial sequence of actions, use the smallest number for the first action and larger numbers for each of the rest of the actions in sequence. To specify parallel actions, use the same integer for each action you want to run in parallel.

  

* You can load [streaming data](http://aws.amazon.com/streaming-data/) into your Amazon Elasticsearch Service (ES) domain from many different sources in AWS. Some sources, like Amazon Kinesis Data Firehose and Amazon CloudWatch Logs, have built-in support for Amazon ES. Others, like Amazon S3, Amazon Kinesis Data Streams, and Amazon DynamoDB, use AWS Lambda functions as event handlers. The Lambda functions respond to new data by processing it and streaming it to your domain.

  You can use subscriptions to get access to a real-time feed of log events from CloudWatch Logs and have it delivered to other services such as a Amazon Kinesis stream, Amazon Kinesis Data Firehose stream, or AWS Lambda for custom processing, analysis, or loading to other systems. To begin subscribing to log events, create the receiving source, such as a Kinesis stream, where the events will be delivered. A subscription filter defines the filter pattern to use for filtering which log events get delivered to your AWS resource, as well as information about where to send matching log events to.
  
* AWS Config cannot detect the utilization of AWS resources. You have to use AWS Trusted Advisor instead.
  
* On AWS ECS Fargate You can update your service with your custom configuration, keep the current settings for your service, and select **Force new deployment**. The new tasks launched by the deployment pull the current image/tag combination from your repository when they start. The **Force new deployment** option is also used when updating a Fargate task to use a more current platform version when you specify `LATEST`. For example, if you specified `LATEST` and your running tasks are using the `1.0.0` platform version and you want them to relaunch using a newer platform version.
  **There is no “Redeploy” deployment strategy option in ECS.**

* AWS Shield is a managed DDoS protection service that is available in two tiers: Standard and Advanced. AWS Shield Standard applies always-on detection and inline mitigation techniques, such as deterministic packet filtering and priority-based traffic shaping, to minimize application downtime and latency. AWS Shield Standard is included automatically and transparently to your Elastic Load Balancing load balancers, Amazon CloudFront distributions, and Amazon Route 53 resources at no additional cost. When you use these services that include AWS Shield Standard, you receive comprehensive availability protection against all known infrastructure layer attacks.

  Customers who have the technical expertise to manage their own monitoring and mitigation of application-layer attacks can use AWS Shield together with AWS WAF rules to create a comprehensive DDoS attack mitigation strategy.
  
* To collect logs from your Amazon EC2 instances and on-premises servers into CloudWatch Logs, AWS offers both a new unified CloudWatch agent, and an older CloudWatch Logs agent. It is recommended to use the unified CloudWatch agent which has the following advantages:

   – You can collect both logs and advanced metrics with the installation and configuration of just one agent.

   – The unified agent enables the collection of logs from servers running Windows Server.

   – If you are using the agent to collect CloudWatch metrics, the unified agent also enables the collection of additional system metrics, for in-guest visibility.

   – The unified agent provides better performance.


* CloudWatch Logs Insights enables you to interactively search and analyze your log data in Amazon CloudWatch Logs. You can perform queries to help you quickly and effectively respond to operational issues. If an issue occurs, you can use CloudWatch Logs Insights to identify potential causes and validate deployed fixes.

  CloudWatch Logs Insights includes a purpose-built query language with a few simple but powerful commands. CloudWatch Logs Insights provides sample queries, command descriptions, query autocompletion, and log field discovery to help you get started quickly. Sample queries are included for several types of AWS service logs.
  
* AWS Systems Manager Maintenance Windows let you define a schedule for when to perform potentially disruptive actions on your instances such as patching an operating system, updating drivers, or installing software or patches. Each Maintenance Window has a schedule, a maximum duration, a set of registered targets
  
* In Cloudfront we can Create an origin group with two origins to set up an origin failover in Amazon CloudFront. Specify one as the primary origin and the other as the second origin. This configuration will cause the CloudFront service to automatically switch to the second origin in the event that the primary origin returns specific HTTP status code failure responses.
  
* You can use Amazon CloudWatch Events to detect and react to changes in the status of Trusted Advisor checks. Then, based on the rules that you create, CloudWatch Events invokes one or more target actions when a check status changes to the value you specify in a rule. Depending on the type of status change, you might want to send notifications, capture status information, take corrective action, initiate events, or take other actions.

  Trusted Advisor only sends the the summary notification every week so this won’t notify you immediately about your non-compliant resources.
  
*  Send all AWS-scheduled maintenance notifications to the Slack:
  Use a combination of AWS Personal Health Dashboard and Amazon CloudWatch Events to track the AWS-initiated activities to your resources. Create an event using CloudWatch Events which can invoke an AWS Lambda function to send notifications to the company's Slack channel.

* With Amazon Inspector, you can automate security vulnerability assessments throughout your development and deployment pipelines or for static production systems. This allows you to make security testing a regular part of development and IT operations.

  Amazon Inspector also offers predefined software called an agent that you can optionally install in the operating system of the EC2 instances that you want to assess. The agent monitors the behavior of the EC2 instances, including network, file system, and process activity. It also collects a wide set of behavior and configuration data (telemetry).

  If you want to set up a recurring schedule for your assessment, you can configure your assessment template to run automatically by creating a Lambda function using the AWS Lambda console. Alternatively, you can select the “Set up recurring assessment runs once every <number_of_days>, starting now” checkbox and specify the recurrence pattern (number of days) using the up and down arrows.

  Important: You have to install the Amazon Inspector agent first to the EC2 instance before you can run the security assessments.



____



* CW Events with AWS Codepipeline: You can configure notifications to be sent when the state changes for:
  * Specified pipelines or all your pipelines. You control this by using `"detail-type":` "CodePipeline **Pipeline** Execution State Change".
  * Specified stages or all your stages, within a specified pipeline or all your pipelines. You control this by using `"detail-type":` "CodePipeline **Stage** Execution State Change".
  * Specified actions or all actions, within a specified stage or all stages, within a specified pipeline or all your pipelines. You control this by using `"detail-type":` "CodePipeline **Action** Execution State Change".
  
* **AWS Config** provides you a visual dashboard to help you quickly spot non-compliant resources and take appropriate action. 
  AWS Config can record all configuration changes and store the data reports to Amazon S3. You can use Amazon QuickSight to analyze the dataset.



*  Amazon Inspector service is primarily used to help you check for unintended network accessibility of your Amazon EC2 instances and for vulnerabilities on those EC2 instances


* **AWS Application Discovery Service** helps you plan your migration to the AWS cloud by collecting usage and configuration data about your on-premises servers. Application Discovery Service is integrated with AWS Migration Hub, which simplifies your migration tracking. After performing discovery, you can view the discovered servers, group them into applications, and then track the migration status of each application from the Migration Hub console. The discovered data can be exported for analysis in Microsoft Excel or AWS analysis tools such as Amazon Athena and Amazon QuickSight.

  Application Discovery Service offers two ways of performing discovery and collecting data about your on-premises servers:

  ​    **– Agentless discovery** can be performed by deploying the AWS Agentless Discovery Connector (OVA file) through your VMware vCenter.

     **– Agent-based discovery** can be performed by deploying the AWS Application Discovery Agent on each of your VMs and physical servers.
  
* **Amazon GuardDuty** offers threat detection that enables you to continuously monitor and protect your AWS accounts and workloads. GuardDuty analyzes continuous streams of meta-data generated from your account and network activity found in AWS CloudTrail Events, Amazon VPC Flow Logs, and DNS Logs. It also uses integrated threat intelligence such as known malicious IP addresses, anomaly detection, and machine learning to identify threats more accurately. This can include issues like escalations of privileges, uses of exposed credentials, or communication with malicious IPs, URLs, or domains.
  For example, GuardDuty can detect compromised EC2 instances serving malware or mining bitcoin.

* AWS Config provides AWS managed rules, which are predefined, customizable rules that AWS Config uses to evaluate whether your AWS resources comply with common best practices. In this scenario, you can use the **approved-amis-by-id** AWS manage rule which checks whether running instances are using specified AMIs. We can also configure AWS Config to stream configuration changes and notifications to an Amazon SNS topic
  
* An aggregator is an AWS Config resource type that collects AWS Config configuration and compliance data from the following:

   – Multiple accounts and multiple regions.

   – Single account and multiple regions.

   – An organization in AWS Organizations and all the accounts in that organization.

  

* AWS Config allows you to remediate noncompliant resources that are evaluated by AWS Config Rules. AWS Config applies remediation using AWS Systems Manager Automation documents (NOT LAMBDA)
  
* We can add a Web Access Control List in front of the API Gateway using AWS WAF to block requests that contain malicious SQL code, and we can have a track of the changes of the web ACLs such as the creation/deletion of rules using AWS Config.
  
* If you plan to launch an Auto Scaling group of EC2 instances, you can configure the `AWS::AutoScaling::AutoScalingGroup` resource type reference in your CloudFormation template to define an Amazon EC2 Auto Scaling group with the specified name and attributes. To configure Amazon EC2 instances launched as part of the group, you can specify a launch template or a launch configuration. It is recommended that you use a launch template to make sure that you can use the latest features of Amazon EC2, such as T2 Unlimited instances.

  You can add an UpdatePolicy attribute to your Auto Scaling group to perform rolling updates (or replace the group) when a change has been made to the group.

  To specify how AWS CloudFormation handles replacement updates for an Auto Scaling group, use the `AutoScalingReplacingUpdate` policy. This policy enables you to specify whether AWS CloudFormation replaces an Auto Scaling group with a new one or replaces only the instances in the Auto Scaling group.

  When you set the `WillReplace` parameter, remember to specify a matching CreationPolicy. If the minimum number of instances (specified by the MinSuccessfulInstancesPercent property) don’t signal success within the Timeout period (specified in the CreationPolicy policy), the replacement update fails and AWS CloudFormation rolls back to the old Auto Scaling group.

  **AutoScalingReplacingUpdate will create a new ASG entirely.**

  **AutoScalingRollingUpdate use the old ASG.** 



_______

## Important

* **Inspector** is only for EC2 instances (need Agent), analyze the running OS against know vulnerabilities or against unintended network accessibility. No own custom rules, only use AWS managed rules. After the assesment you get a report with a list of vulnerabilites. Inspector can't analyze directly AMI

* **AWS Config** helps with auditing and recording **compliance** of AWS resources, helps record configuration and changes over time.
  AWS Config Rules does not prevent actions from happening (no deny). 

  Is a per-region service, can be aggregated across regions and accounts. 

  Integrated with SNS for any changes.
  Questions for Config?

  * Unrestricted SSH to SG?
  * Buckets with any public access?
  * ALB configuration changed over time?

* **AWS Config Rules**

  * Can use AWS managed config rules (over 75)
  * Can make custom config rules (using Lambda)

  Config Rules can trigger CW Events if the rules is non-compliant (And chain with Lambda)
  **Rules can have auto remediations, using SSM Automations**. Not Lambda directly, but SSM Automation can invoke Lambda
  **For alerting need CloudWatch Event, not directly SNS.** 

* **GuardDuty** Intelligent Threat discovery to Protect AWS Account. Use ML for anomaly detection. Can setup CW Event rules to be notified in case of findings, and target to Lambda or SNS
  Input data includes:
  * CloudTrail Logs: unusual Api Call, unauthorized deployments
  * VPC Flow Logs: unusual internal traffic, unusual IP address
  * DNS Logs: compromised Ec2 instances sending encoded data within DNS queries
  
* **AWS Secret Manager** is better than Parameter Store only if you need rotation or a deep integration with RDS.

* **Amazon Macie** is a security service that uses machine learning to automatically discover, classify, and protect sensitive data in AWS. Automatically generates an inventory of S3 buckets and discover sensitive data such a personally identifiable information (PII), and sends the information to CW Events for integration into workflows and remedation actions

* First is how does the unified CloudWatch agent publish
  system-level metrics? These are published as CloudWatch metrics that can be used directly for alarms
  like any other metric,CloudWatch Events is a different feature of CloudWatch that fires events upon
  system events or on a schedule

* **AWS Service Catalog** allows Administrators to publish products and grant IAM users privileges to launch the products without granting those users the ability to launch the underlying services. To launch a CloudFormation stack, the user needs privileges to launch all the underlying infrastructure in the stack.

