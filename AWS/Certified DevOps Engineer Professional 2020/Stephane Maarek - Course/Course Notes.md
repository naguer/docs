# Notes Course



### SDLC Automation

#### CodeCommit

* If you need to restring commits to master, you can do that with a IAM policy wit Deny `codecommit:GitPush` and a Resource with the repository name, and the branch master

* Policy for JR Groups: In IAM any explicit deny takes precedence over anything else, if you have an explicit deny first and after an explicit allow, the deny is what goes into effect

* Review triggers & notifications with SNS and CloudWatchs events

* Notifications & triggers respond to pull request being created or deleted, new commands, new branches, deleted a request, deleted tags.
  
* Repository notifications are different from repository triggers. Although you can configure a trigger to use Amazon SNS to send emails about some repository events, those events are limited to operational events, such as creating branches and pushing code to a branch. Triggers do not use CloudWatch Events rules to evaluate repository events. They are more limited in scope.
  
* **All that happen in your repository can trigger and SNS notification or a lambda function**

  https://docs.aws.amazon.com/codecommit/latest/userguide/how-to-notify-lambda.html



Exercises: 

* Create CodeCommit repository with user and HTTP git keys
* Create a Notification Rule, if we have a new commit or branch, etc, we need to receive a notification email
* Create a CodeCommit trigger to execute a Lambda Function is something happens



#### CodeBuild

* Fully managed build service, alternative to Jenkins, continuous scaling (no build queue)
* Leverages Docker under the hood for reproducible builds. Possibility to extend capabilities leveraging our own base Docker Images
* Secure: Integration with KMS for encryption of build artifacts, IAM for build permissions, and VPC for network security, CloudTrail for API calls logging
* The best way to testing your code (better than Jenkins and EC2 because is Serverless)
* Use CloudWatch Events to detect failed builds and trigger notifications. You can use CloudWatch Alarms to notify if you need "thresholds" for failures
* More important file: *buildspec.yml* (Builds instructions file)

  * Have phases (like steps in Jenkins). For example, install, pre_build, build, post_build
  * *finally* runs in every block (install, pre_build, build), doesn't matter if the before fails or success, is for example for clean. Is optional. 
  * Check Build Syntax https://docs.aws.amazon.com/codebuild/latest/userguide/build-spec-ref.html (check Build Spec Example)
  * Artifacts are the output, for example a compiled jar, you can upload to S3, and use in the future
  * You can use a *cache* for example to caching dependencies
  * With `printenv` you can print all the environments values in your build
  * You can put your environment variables in the GUI in plain text or using with parameter store (ssm)
    For using a parameter from SSM, you need to add the ssm permissions to the Codebuild Role. Use SSM is the way for using secure passwords in the build.
* If you need to login in ECR with CodeBuild using docker, you need to do in the pre_build phase, because, if you can't login, you know this before the build phase.
* With **Cloudwatch events** we can cron the Builds (need a new role and the project ARN of CodeBuild).
Also with Cloudwatch events we can trigger a codebuild job using a event type, for example if the codecommit repository have new commits. For the last with Cloudwatch events we can use Event Types Codebuild changes, for example, if a Codebuild stage fail, we create a new target with a SNS topic, the topics maybe can send a mail.
* **Validating CodeCommit PRs** this is of test our pull request if the code is working before we merge it into master. Read Validating AWS codecommit pull request with aws codebuild and aws lambda.
https://aws.amazon.com/es/blogs/devops/validating-aws-codecommit-pull-requests-with-aws-codebuild-and-aws-lambda/
* Practice: https://docs.aws.amazon.com/codebuild/latest/userguide/sample-docker.html



Exercises: 

* Make a build with a buildspec.yml that check if the word "Congratulations" exists in index.html
* Make a build that use environment variables from the GUI, one in plain text and another with SSM
* Make a build with an artifact
* Configure to run codebuild build every 5 minutes
* Configure if codebuild build phase fail, send a email



#### CodeDeploy

* CodeDeploy is a managed Service to deploy our application automatically to many EC2 instances. 

* Each EC2 Machine (or On-Premise machine) must be running the CodeDeploy Agent (The agent is continuously polling, and CodeDeploy sends appspec.yml)

* Application is pulled from GitHub/S3, EC2 will run the deployments instructions

* CodeDeploy Agent will report of success / failure of deployment on the instance

* CodeDeploy can be chained into CodePipeline and use artifacts from there

* CodeDeploy need an IAM role with read permissions to S3

* We need to install CodeDeploy Agent in the EC2 instances

* CodeDeploy can deploy in Ec2/On-premises, Lamba and ECS

* Deployments Groups can be for example prod, dev, test, etc.

* Deployment configurations are in-place or blue-green, and the strategy can be OneAtTime, HalfAtTime and AllAtOnce (You can create your custom deployment configuration, for example with percentages)

* With blue-green deployments we need to create new instances, is the perfect case for use Auto Scaling Group and ELB

* Appspec.yml

  * `files` is for specified the source and the destination of the files
  * `hooks` is scripts that will be run as the code deploy is happening for your instances, for example ApplicationStop, AfterInstall, BeforeInstall, ApplicationStart and ValidateService

* `hooks` run order example: (without ELB) start -> ApplicationStop -> DownloadBundle -> BeforeInstall -> Install -> AfterInstall -> ApplicationStart -> ValidateService -> End
  The **Start**, **DownloadBundle**, **Install**, and **End** events in the deployment cannot be scripted. However, you can edit the `'files'` section of the AppSpec file to specify what's installed during the **Install** event.

  https://docs.aws.amazon.com/codedeploy/latest/userguide/reference-appspec-file-structure-hooks.html

* You can use Amazon CloudWatch Events to detect and react to changes in the state of an instance or a deployment (an "event") in your CodeDeploy operations.

  https://docs.aws.amazon.com/codedeploy/latest/userguide/monitoring-cloudwatch-events.html

* The Codedeploy agent generates three log files:

  - **Agent log** – Contains information about the agent’s health and overall deployment status (**codedeploy-agent-logs**)
  - **Deployment log** – Contains STDOUT, STDERR, and information specific to the user-defined scripts that run during a deployment (**codedeploy-deployment-logs**)
  - **Updater log** (Linux agents) – Contains agent updater status (**codedeploy-updater-logs**)

  To view these logs in Cloudwatch we need to add permissions to IAM Instance profile and install the CloudWatch logs agent and start the service.

* Trigger for deployment groups, for example trigger if deployment succeeds, fails, stops, ready, etc, the target can be an AWS SNS only, if we want some more complete we need to integrate with CloudWatch Events

* Rollback: CodeDeploy rolls back deployments by redeploying a previously deployed revision of an application as a new deployment. These rolled-back deployments are technically new deployments, with new deployment IDs, rather than restored versions of a previous deployment.

  Deployments can be rolled back automatically or manually.

  Two types of rollbacks: 

  * When a deployment fails
  * When alarm thresholds are met (we need to configure a Cloudwatch alarm before)

  https://docs.aws.amazon.com/codedeploy/latest/userguide/deployments-rollback-and-redeploy.html

* On-Premise CodeDeploy Setup. Deploying a CodeDeploy application revision to an on-premises instance involves two major steps:

  - **Step 1** – Configure each on-premises instance, register it with CodeDeploy, and then tag it.
  - **Step 2** – Deploy application revisions to the on-premises instance.

  For register small number of on-premise services we can use iam-user (one user for each on-premise server), and for a big number of on-premise we can use iam-role but is more complicated. 
  Remember tag the onpremise correctly in CodeDeploy menu

  https://docs.aws.amazon.com/codedeploy/latest/userguide/instances-on-premises.html

* CodeDeploy to AWS Lamba: there are 3 ways traffic can shift during a deployment:

  * Canary is if you need to have traffic being shifted in two increments
  * Linear is if you need something more gradual with equal increments
  * All-at-once is if you want all traffic in one time

  Run order of Hooks in a Lambda Function version deployment:
  Start -> BeforeAllowTraffic -> AllowTraffic -> AfterAllowTraffic -> End

  (All are triggers to other Lambda Functions)

  ```
  hooks:
     - BeforeAllowTraffic: BeforeAllowTrafficHookFunctionName
     - AfterAllowTraffic: AfterAllowTrafficHookFunctionName
  ```

  https://docs.aws.amazon.com/codedeploy/latest/userguide/reference-appspec-file-structure-hooks.html

  

  Exercises:

* Create a deploy to a EC2 instances with index.html, and take the files from S3
* Create a deploy with a Cloudwatch Event, for example, if any step fail, execute a lambda function or send a mail
* Create a Rollback with a Threshold and CloudWatch alarm, for example rollback if pass the %30 cpu utilization.



#### CodePipeline

* Is a Continuous Delivery tools, and is a Visual Workflow
* Sources: GitHub / CodeCommit / Amazon S3
* Build: CodeBuild / Jenkins / etc
* Deploy: AWS CodeDeploy / Beanstalk / CloudFormation / ECS
* Made of stages:
  * Each stage can have sequential actions and or parallel actions
  * Stage examples: Build / Test / Deploy / Load Test / etc...
  * Manual approval can be defined at any stage
* Each pipeline stage create "Artifacts", these are passed stored in S3 and passed on the next stage. For example:
  CodeCommit -> S3 -> CodeBuild -> S3 -> CodeDeploy -> EC2
* CodeCommit & CodeDeploy with CodePipelines
  * We need to create a CodePipeline for each Branch
  * Are two types of `detection options`
    * Amazon CloudWatch Events, recommended way because as soon as a change occurs on CloudCommit then the pipeline will be triggered
    * AWS CodePipeline, this method check for example every 30 seconds, then we have a 30 seconds delay
  * Is possible have CodePipeline in one region, and CodeDeploy in another because CodePipeline is multiregion
* Stages are steps in the pipeline, for example, source -> test -> deploy
  In every stage we have an action group, and we can configure many actions (for example different tests). Every stage generate new artifacs on S3.
* We can add `actions`to an `stage`for generate parallel tasks
* Artifacts are the way for CloudPipeline to communicate with the different stages/services, S3 is the backbone of CodePipeline. 
* Codebuild artifacts and Codepipeline artifacts are different 
* CodeBuild with CodePipelines, remember configure iam role from Codebuild to access to S3 bucket. 
*  Manual Approval Steps are very easy to configure, is an `Action Provider` option in action group, can integrate with a SNS for notification, for the approve we need to go to the CodePipeline interface. 
* With Cloudwatch Events integrations and CodePipeline we can configure for example if something fail send a email or with lambda a notification in Slack
* The stages can be sequential or parallel, in one action group are parallel. The way to define this is using the `runOrder` parameter
* Use cases for CodePipeline
  https://docs.aws.amazon.com/codepipeline/latest/userguide/best-practices.html
* Integrate CodePipeline with Lambda, this is very useful because with Lambda we can extend our pipelines to do anything we want in general.
* CodePipeline can execute Cloudformation templates using the Cnf action provider
* We can create a Pipeline with AWS Cloudformation,
* Codepipeline nested Cloudformation example:
  https://github.com/aws-samples/codepipeline-nested-cfn
* Implementing GitFlow Using AWS CodePipeline, AWS CodeCommit, AWS CodeBuild, and AWS CodeDeploy
  https://aws.amazon.com/es/blogs/devops/implementing-gitflow-using-aws-codepipeline-aws-codecommit-aws-codebuild-and-aws-codedeploy/



Exercises:

* Create a pipeline that deployed a index.html, is something happens in the repository, trigger a new deploy.
* Add a Codebuild stage for test, check if OK exist in index.html, if not, then the pipeline need to fail.



#### CodeStar

* it helps simplify DevOps processes and to do that it brings popular AWS DevOps Services under one roof. Those services are:
  - For building the code you have **AWS CodeBuild**
  - Deployment is taken care by [**AWS CodeDeploy**](https://www.edureka.co/blog/aws-codedeploy/)
  - For version control, we have [**AWS CodeCommit**](https://www.youtube.com/watch?v=ik4VE1O4OBo)
  - **AWS CodePipeline** for building CI/CD Pipelines



#### Jenkins

* Open Source CICD tool, can replace CodeBuild, CodePipeline & CodeDeploy
* Must be deployed in a Master / Slave (Workers) configuration
* All projects must have a "Jenkinsfile" to tell Jenkins what to do
* Ec2-plugin create automate Ec2 instances automatically if jenkins notices that your build cluster is overloaded. When the load goes down, unused ec2 instances will be terminated (Easy for create Jenkins Slaves)
* AWS CodeBuild plugin is similar to ec2 plugin, Jenkins with this plugin send all the build directly yo CodeBuild, with this plugin we don't need Ec2 slaves, we use CodeBuild
* ECS Plugin is for use slaves in ECS service, elastics slaves.
* Aws Codepipeline Plugin is for integration.
* Artifact manager on S3, is for save the artifacs of Jenkins directly on S3.





#### Whitepapers

- **MUST READ -** Blue/Green Deployments on AWS
  https://d1.awsstatic.com/whitepapers/AWS_Blue_Green_Deployments.pdf
- **RECOMMENDED -** Practicing Continuous Integration Continuous Delivery on AWS
  https://d1.awsstatic.com/whitepapers/DevOps/practicing-continuous-integration-continuous-delivery-on-AWS.pdf
- **RECOMMENDED -** Jenkins on AWS
  https://d1.awsstatic.com/whitepapers/DevOps/Jenkins_on_AWS.pdf
- **OPTIONAL -** Introduction to DevOps on AWS
  https://d1.awsstatic.com/whitepapers/AWS_DevOps.pdf
- **OPTIONAL -** Development and Test on AWS
  https://d1.awsstatic.com/whitepapers/aws-development-test-environments.pdf



______________

### Configuration Management and Infrastructure as Code

#### **CloudFormation**

- Templates components:
  
  - **Parameters** are the dynamic inputs for your template (useful to reuse your templates).  `!Ref` to reference a parameter.
    Exist Pseudo Parameters, these can be used at any time, and are enabled by default, such AccountId, Region, NoValue, StackName, etc.
  - **Resources** are the core of your CNF (Mandatory), they represent the different AWS Components that will be created and configured, are declared and can reference each other. Exist 224 types of resources. Dynamic amount of resources in CNF is impossible, you can't perform code generation there.
  - **Mappings** are the fixed variables for your template. For e.g. to different envs, regions, AMI types, etc. To access to mapping values we used `!FindInMap` to return a named value from a specific key. 
  - **Outputs** declares optional outputs values that we can import into other stacks (if you export them first). It's the best way to perform some collaboration cross stack, as you let expert handle their own part of the stack.
    You can't delete a CloudFormation Stack if it outputs are being referenced by another CloudFormation Stack.
    `Export` in the first CNF, and `!ImportValue` in the second CNF are the way to link two templates
  - **Conditions** are used to control the creation of resources or outputs based on a condition. Some commons ones are: Environment, Region, Any parameter value. Each condition can reference another condition, parameter value or mapping. 
    The function we can used are: And, Equals, If, Not, Or
  - **Metadata** is used to include arbitrary JSON or YAML objects that provide details about the template.
  
- **Intrinsic Functions** (all start with Fn:: or !)

  - `!Ref` is for reference parameters, or resources
  - `!GetAtt` this function is useful to retrieve an attribute resources, you need to read the docs if you want to know the attributes of a resource.
    For example you can get the AZ of an EC2 Machine: 
    `!GetAtt EC2Instance.AvailabilityZone`
  - `!FindInMap` is useful to return a named value from a specific key
    This is the structure ->
    `!FindInMap  [ MapName, TopLevelKey, SecondLevelKey]`

  - `!ImportValue` is to get values exported in other templates
  - `!Join` is for join values with a delimiter
    `!join [ delimiter, [coma-delimited list of values] ]`
    For example if we want to create `a:b:c` we use 
    `!join [ :, [a, b, c] ]`
  - `!Sub` is used to substitute variables from a text. It's very handy function that will allow you to fully customize your templates
  - `Conditions Functions` (And, Equals, If, Not, Or etc)
    For example:
    `CreateProdResources: !Equals [ !Ref EnvyType, prod]`

- **User Data** we can have user data at EC2 instance launch through the console and also include it in CloudFormation, the important part is past in the script the function `Fn::Base64` , user data log is in `/var/log/cloud-init-output.log`
  For example:

  ```
  UserData:
  	Fn::Base64: |
  		#!/bin/bash
  		yum update -y
  		yum install -y httpd
  ```

- **cnf-init** is like the User Data but with another more readable language, `AWS::CloudFormation::Init` must be in the Metadata of a resource. Logs go to /var/log/cfn-init.log

- **cfn-signal & wait conditions** this conditions are very useful to know if some resources is configured and created OK. We can run cfn-signal right after cfn-init to tell CloudFormation service to keep on going or fail. We need to define **WaitCondition** this block the template until it receives a signal from cfn-signal, we attach a **CreationPolicy** (also works on EC2, ASG)
  **Important for troubleshooting!** If Wait Condition Didn't receive the required number of signals from an Ec2 Instance, we should check this points:

  * Ensure that the AMI you're using has the AWS CloudFormation helper scripts installed,
  * Verify that the cfn-init & cfn-signal command was successfully run on the instance.
    (/var/log/cloud-init.log and /var/log/cfn-init.log)
  * You can retrieve the logs by logging in to your instances, but you must disable rollback on failure
  * Verify that the instance has a connection to the internet, if the instance is in a VPC, the instance should be able to connect to the Internet thought a NAT, it's in a private subnet thought a IGW.

- **Rollbacks on failures**  Stack creation fails, we had 3 options:

  -  `OnFailure=ROLLBACK`everything roll backs, gets deleted (Default)

  -  `OnFailure=DO_NOTHING` option to disable rollback, and manually troubleshoot

  -  `OnFailure=DELETE` get rid of the stack entirely, do not keep anything

    If the stack update fails, the stack automatically roll back to the previous version

- **Nested Stacks** are stacks as part of other stacks, they allow you to isolate repeated patterns. E.G ELB config that is re used, or SG that is re used. Nested stacks are considered Best practice. 
  **To update a nested stack, always update the parent** 

- **ChangeSets** is useful when you update a stack, you need to know what changes before it happens for rather confidence. You create a change set by submitting changes against the stack you want to update. CloudFormation compares the stack to the new template and/or parameter values and produces a change set that you can review and then choose to apply (execute).

- **Retains Data on Delete**
  
  - DeletionPolicy=Retain:
    - Specify on resources to preserve / backup in case of CloudFormation deletes
    - To keep a resource, specify Retain (works for any resource / nested stack)
  - DeletionPolicy=Snapshot:
    - EBS Volume, ElastiCache Cluster, ElastiCache ReplicationGroup
    - RDS DBInstance, RDS DBCluster, Redshift Cluster
  - DeletePolicy=Delete (default behavior):
    - Note: for AWS::RDS::DBCluster resources, the default policy is Snapshot
    - Note: to delete an S3 bucket, you need to first empty the bucket of its content
  
- **Termination Protection On Stacks** is to prevent accidental deletes of CloudFormation templates, use TerminationProtection, you can activate in the GUI, is similar to the EC2 Protection. 

- **Parameters for SSM** using Parameters Store we are able to centralize the latest value of all parameters in Cloudformation in another place, if a value change in the SSM, when we execute the Cloudformation the parameter changes, we can apply an update of the stack for update the SSM parameters, is not automatically. A good useful example if centralize the Ami ID in the SSM, if you need to change the AMI id you only need to modify the SSM and not need to modify the CloudFormation template. 

- **Public Parameters for SSM**, SSM have public parameters, for example LatestLinuxAmiId, you can use without create a new SSM, you only need to invoke `/aws/service/ami-linux-latest/amzn2-ami-x86`. If you want the list of the public parameters Ami we need to execute `aws ssm get-parameters-by-path --path "/aws/service/ami-amazon-linux-latest"`

- **DependsOn** is a way to configure a resource with a dependence, for example, we can set `DependsOn` in a EC2 resources that depends to a DB resource. This is useful in this case, if we have an application in the EC2 that connect to the DB.

- **Deploying Lambda Functions** we can do this using CloudFormation, we have two methods

  - **Inline** this method have all the code in the CloudFormation, and we can't have dependencies, and we are limited to 4000 characters. This method is fine with small lambda functions
  - **Zip-s3** this method use a Zip from S3 with the Code (and if we have dependencies we need to add them). This method is only for the code, we need to configure the rest of Lambda configuration in CloudFormation Template. 

- **Custom Resources** we can define this in CloudFormation to address any of these use cases:

  - An AWS Resource is yet not covered (New service)
  - An On-Premise Resource
  - Emptying an S3 Bucket before being deleted
  - Fetch an AMI id (old way without SSM)

  This method is a CloudFormation Custom Resource when is created, updated o deleted invoke a Lambda Function (not every time run we run the template), the Lambda Function make an API calls and can invoke whatever you want

- **Drift Detection** is something you can active in your stack, and you can click on "View results" if says IN_SYNC is exactly the same configuration we created, but if we change manual the resources, and we go back to "Detect stack drift", we see "DRIFTED" and we can see the modified resources with the differences.

- **Status Codes Deep Dive**
  https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-describing-stacks.html

- **InsufficienteCapabilitiesException** this error is we don't provided the capability required for our template to work. We need to provide CloudFormation the ability to create IAM resources. We need to configure `CAPABILITY_IAM` value. One solution is only tick the box in the Stack creation with the message "The following resources require capabilities"

- **cfn-hup & cfn-metadata** the cfn-hup helper is a daemon that detects changes in resource metadata and runs user-specified actions when a change is detected. This allows you to make configuration updates on your running Amazon EC2 instances through the UpdateStack API action.

- **Stack Policies** is a JSON document that defines what can be updated as part of a stack update operation. To set or update the policy, your IAM users or roles must first have the ability to call the **cloudformation:SetStackPolicy** action.

  You apply the stack policy directly to the stack. Note that this is not an IAM policy. By default, setting a stack policy protects all stack resources with a **Deny** to deny any updates unless you specify an explicit **Allow**. 



**Reference Links:**

- https://stackoverflow.com/a/45007029/3019499
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/walkthrough-custom-resources-lambda-lookup-amiids.html
- https://github.com/awslabs/aws-cloudformation-templates/tree/master/aws/solutions/lambda-backed-cloudformation-custom-resources/Fetch-AMI-From-Parameter-Store
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-cfn-describing-stacks.html#w2ab1c15c15c17c11
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/troubleshooting.html#troubleshooting-errors-update-rollback-failed
- https://aws.amazon.com/blogs/devops/continue-rolling-back-an-update-for-aws-cloudformation-stacks-in-the-update_rollback_failed-state/
- https://stackoverflow.com/a/41468341/3019499
- https://docs.aws.amazon.com/AWSCloudFormation/latest/APIReference/API_CreateStack.html#API_CreateStack_RequestParameters
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-iam-template.html#using-iam-capabilities
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/protect-stack-resources.html



#### **Beanstalk**

* **EB CLI** is the command line interface for Elastic Beanstalk, we need to install the eb cli `pip install awsebli`.  For create a new project `eb init` and complete with the app name, the default region, and with the platform (also we can complete with the ssh keypair), this command create `.elasticbeanstalk` directory with the `config.yml`file with the information of the project.
  Example create a new project with a dev environment:

  ```
  eb init --profile aws-devops
  echo "Hello World" > index.html
  eb create dev-env
  ```

The `eb create` create a zip of the directory, and upload to S3 and after that upload into Elastic Beanstalk and create the environment (with the sg, elb, asg, cloudwatch alarms). We can be the publish app in the browser with the command `eb open`

If we want to change the index.html and publish, we can modify the file and execute `eb deploy`. This generate a new Application Version, we can go back to the first if we want. With `eb terminate` we can clean the environment. 


* **Saved Configurations** this is the best way for have an EB AS CODE (or backups) and we can easily reproduce an EB configuration in another account, or region, or whatever.
  We can save the configuration 
  `eb config save dev-env --cfg initial-configuration`

  This generate a file .yml with all the configuration (all the NO DEFAULT configuration)
  We can also set an environment variable on the environment
  

And after that we can save the current state of our environment, for example
  `eb config save dev-env --cfg prod`

This generate a new file prod.cfg.yml in our directory. 

If we want to change some parameters, we can modify the cfg file and then update the saved prod configuration (in the local file)

`eb config put prod`

After that we need to update current environments from saved configuration

`eb config dev-env --cfg prod`

* **.ebextensions for configs** this is another way to configure EB with code, we need to create a new directory `ebextensions` with the configuration using yml syntax. 
  For example:

  ```
  option_settings:
    aws:autoscaling:asg:
      MinSize: 1
      MaxSize: 10
      Cooldown: 240
    aws:autoscaling:launchconfiguration:
      InstanceType: t2.micro
  ```

  During environment creation, configuration options are applied from multiple sources from highest to lowest.

  **Settings applied directly to the environment -> Saved Configurations -> Configuration files (.ebextensions) -> Default Values**
  https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options.html

* **.ebextensions for resources** we can add CloudFormation code in a file in the .extensions directory with the code for create Resources, also we can add variables. 

* **RDS in or out of environment?** If we need to create an RDS or DynamoDB for production, the best way is create the DB externally to the EB. When we create the EB environment we need to reference to the external DB directly.

*  **.ebextensions for commands & container commands** we can add a file with commands for the EC2 instance, the commands run before the application and web server are set up and the application version file is extracted. For example:

```
commands:
  create_hello_world_file:
    command: touch hello-world.txt
    cwd: /home/ec2-user
```

Also we can use the container_commands key to execute commands that affect your application source code. Container commands run after the application and web server have been set up and the application version archive has been extracted, but before the application version is deployed.

```
container_commands:
  modify_index_html:
    command: 'echo " - modified content" >> index.html'
```

The third type of command is `database_migration` this run only once, is only for containers

```
  database_migration:
    command: 'echo "do a database migration"'
    # You can use leader_only to only run the command on a single instance
    leader_only: true
```

https://stackoverflow.com/questions/35788499/what-is-difference-between-commands-and-container-commands-in-elasticbean-talk/40096352#40096352

* **Deployment Modes** 

  * Single instance great for dev
  * High Availability with Load balancer, asg, great for prod

* **Rolling Updates Strategies**

  * **All at once** is the fastest, but instances aren't available to serve traffic for a bit (downtime). Great for quick iterations in development environment. No additional cost.
  * **Rolling** update a few instances at a time (bucket), and then move onto the next bucket once the first bucket is healthy. Application is running below capacity during a time. In same point the application is running both versions simultaneously. No additional cost but is a long deployment.
  * **Rolling with additional batches** like rolling, but spins up new instances to move the batch (so that the old application is still available). Application is running at the same capacity all the time, and is running both version simultaneously. Small additional cost, is a long deployment. Good for prod environment. 
  * **Immutable** spins up new instances in a new ASG, deploys version to these instances, and then swaps all the instances when everything is healthy. Zero downtime. New code is deployed to new instances of a temporary ASG. High cost, because the double capacity. Is the longest deployment, and give you a quick rollback in case of failures. Great for prod.
  * **Blue/Green** is not a "Direct feature", is like Immutable, create a new "stage" environment and deploy v2, using beanstalk use "Swap Urls". A lot of manual work. Is a dns change, maybe we need to wait for the TTL. 
  * **Worker Environments** is for example for very intensive CPU consumption, is for *long running workloads* on demand or performs tasks on a schedule. The worker environment creates two queues for decouple the task, one normal y the another for the DLQ, first pull from the queue, if the process fail move the task to the DLQ. Also the Worker Environment have a cron.yml for the scheduled tasks.
  * **Multi Docker Integration** is a web server environment, this contain multiple docker images in a EC2 instance. Docker on beanstalk is good because we are able to standardize our deployments and using any kind of language or any kind of application. `dockerrun.aws.json` is the file that describes how to deploy a set of Docker containers as an Elastic Beanstalk application.

  

**Reference Links**

- https://github.com/aws/aws-elastic-beanstalk-cli-setup
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install-advanced.html
- https://aws.amazon.com/blogs/devops/using-the-elastic-beanstalk-eb-cli-to-create-manage-and-share-environment-configuration/
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html#command-options-general-autoscalingasg
- h[ttps://docs.aws.amazon.com/elasticbeanstalk/latest/dg/ebextensions-optionsettings.html](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/ebextensions-optionsettings.html)
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environment-resources.html
- https://stackoverflow.com/a/40096352/3019499
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_docker_v2config.html



#### **Lambda**

* **Lambda** provide you some customization that is needed to make other services work. More memory you select, more allocated CPU proportional you can use. The timeout default is 3 sec and the maximum is 15 minutes. You can run lambda within a VPC if you need to access resources in a VPC, for example to access to a private database, if you use a VPC, also you need to select a Security Group for the access.

* **Debugging and error handling** if it is in a service that asynchronously invokes the lambda function we can set a DLQ (SNS or SQS), if the function fails three times, then it is possible for us to send an event's payload to SNS or SQS, this will ensure that the event is not lost. 
  Also we have AWS X-Ray integration for active tracing of the function to record timing and error information for a subnet of invocations. With this you can understand where the bottlenecks are in your infrastructure

* **Concurrency** is how many functions can run at the same time. Max is 1000 per account on a region

* **Sources and Uses Cases** we have a lots of trigger for using like source for our Lambda function, for example: 

  * Api Gateway, frontend interface and security
  *  ALB, http/https frontend for the Lambda, but it doesn't add any functionality such as security
  * CloudWatch Events is the glue of all DevOps operations we do. We can react to any events in our infrastructure and then create an event and assign a Lambda function. With CloudWatch Events schedule we can create a cron job that trigger a Lambda Function
  * CloudWatch Logs this trigger a Lambda functions is something happens in ours logs in real time.
  * CodeCommit, is some commit happens trigger a Lambda function, for example we can configure a Lambda function to ensure that no credentials were committed
  * Other are with DynamoDb, Kinesis, S3, SNS and SQS

* **Security, Environment Variables, KMS and SSM** we can add environment variables, in text plain, or using a KMS key to encrypt in transit, or KMS key to encrypt at rest (default aws/lambda). The Lambda function need permissions to KMS to work. Also we can use variables with SSM (text plain, or secret)

*  **Versions, Aliases and Canary Routing**

  * Versions when we work on a Lambda function, we work on $LATEST (mutable)
    When we're ready to publish a Lambda function, we create a version. The first version was V1 (Versions are Immutable and have increasing version numbers). Version get their own ARN (Amazon Resource Name)
    **Version = code + configuration (*nothing can be changed - immutable*)**
    Each version of the lambda function can be accessed
    For create a new version we need to modified LATEST and publish to VERSION
  * Aliases are "pointers" to Lambda function versions
    We can define a "dev", "test", "prod" aliases and have them point at different Lambda versions. ***Aliases are mutable*. Users point to Aliases**
    Alias enable Blue / Green deployment by assigning weights to Lambda Functions. For example Prod Alias point %95 to V1, and point %5 to V2
    Aliases have their own ARNs. Examples:
    Users -> Dev Alias (mutable) -> $Latest (mutable)
    Users -> Prod Alias (mutable) -> V1 (Immutable)

* **SAM Framework** is for deploy applications to Lambda using code.
  SAM make cloudformations code but using a easy syntax.
   For create a environment before install SAM-cli and after execute `sam init --runtime python3.7` for download a sample application. The most important file is `template.yaml` looks like CloudFormation but isn't the same. 
  Sam have the `Transform` parameter. The resource is `AWS::Serverless:Function` 
  Is a way to define for example create a Lambda and an Api Gateway in one part. Other file is `app.py` with the code of the application and `requirements.txt` with the list of the packages to install before the package installation. For the last `event.json` is something we can use to locally test our Lambda Function
  Now we can build the application with `sam build` and after that we can Package our app  and upload to S3`sam package --output-template packaged.yaml --s3-bucket test`

  And for the last we can Deploy our app from S3 to Lambda
  `sam deploy --template-file packaged.yaml --capabilites CAPABILITY_IAM ....`

  Also we can test our lambda function local using docker, and test with an local api gateway, is very easy.

* **SAM and CodeDeploy** we can use SAM to create our Serverless Application, it comes buiit-in with CodeDeploy to help ensure safe Lambda deployments. AWS SAM does the following for us:

  * Deploy new version of our Lambda function, and automatically creates aliases that point to the new version
  * Gradually shifts customer traffic to the new version until you're satisfied that it's working as expected, or you roll back the update.
  * Defines pre-traffic, and post-traffic test functions to verify that the new deployed code
  * Rolls back the deployment if CloudWatch alarm are triggered

  With this we can add for example in `template.yml` the `DeploymentPreference` and configure the type as `Canary10Percent10Minutes`



Exercises:

- Create a Lambda function LATEST pointed to DEV, and Version v1 an Alias PROD
- Create a BlueDeployment using SAM, Lambda and CodeDeploy



**Reference Links**

- https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html
- https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/automating-updates-to-serverless-apps.html



#### **Step Functions**

* Step Functions are used when you have a very **Complex Workflow** and you need to isolate some functions of your Workflow and probably make it more visual and intuitive. Is good for **orchestrate** AWS Batch Jobs or things. Step Functions can execute for one year.



**Reference Links**

- https://aws.amazon.com/step-functions/use-cases/



#### **API Gateway**

* Api Gateway can use two protocols, REST or WebSocket

* **Endpoint Types**

  * **Regional** is the more common, is a public API 
  * **Edge Optimized** this is great if you want to reduce the latency of your API by deploying your API along many edge locations.
  * **Private** APIs are only accessible thought VPC endpoints 

* **Integration types**

  * **Lambda Function** allows you integrate an API with your Lambda function, is the most common integration
  * **HTTP** allows you to proxy the request to others HTTP endpoints, for example if you have an HTTP application and you want an API frontend
  * **Mock** is very useful if you want to mock and API
  * **AWS Service** if you want to make a frontend for an AWS Service
  * **VPC Link** 

* To public an API we need to deploy the API, for example to the stage "Prod", this generate a new URL for invocate the API.

* **Integration with Lambda** we can integrate an API Gateway to an Lambda Alias, because of that is one way to do Blue / Green Testing. The api point to an Lambda Alias, and the alias maybe point to two version (for example %90 to v1, and %10 to v2). Also we can point an API Gateway to an Lambda Version (using this, we can't do blue/gree or canary).
  Mapping templates can be used to modify the content before it goes to Lambda Function (integration request), and also it is possible for the response (Integration response) to modify it using a mapping templates. If we make any change to the api configuration, we need to make a new Deployment of the API.

* **Stages and Deployments** making changes in the API Gateway does not mean they're effective, you need to make a "deployment" for them to be in effect (it's a common source of confusion). Changes are deployed to "Stages" (as many as you want), use the naming you like for stages (dev, test prod). Each stage has its own configuration parameters. Stages can be rolled back as a history of deployments is kept.

* **Stage Variables** are like environment variables for API Gateway, use them to change often changing configuration values. They can be used in Lambda function ARN, HTTP endpoing and Parameter mapping templates.
  Use Cases:

  * Configure HTTP endpoints your stages talk to (dev, test, prod...)
  * Pass configuration parameters to AWS Lambda through mapping templates
  * Stages variables are passed to the "Context" object in AWS Lambda

  Stage Variables & Lambda Aliases, we can create a stage variable to indicate the corresponding Lambda Alias, and with this our API gateway will automatically invoke the right Lambda function. For example:
  API GATEWAY: Stage prod / Alias PROD -> 
  Lambda: Aliases PROD -> Version 2
  API GATEWAY: Stage dev / Alias DEV -> 
  Lambda: Aliases DEV -> Version LATEST

* **Canary Deployment** we have the possibility to enable canary deployments for any stage (usually prod). Choose the % of traffic the canary channel receives (metrics & logs are separate). Possibility to override stage variable for canary.

* **Deployments and Canary Testing**  API deployments will be made to the Canary first before being able to be promoted to the entire stage, in Lambda we can edit an Stage and configure a Canary Deployment. First we need to permit the Canary, and after that we can deploy the TESTING deploy to for example PROD (Canary Enabled), and configure %10 to the canary, and %90 to PROD.  
  If is OK the canary test, we can do a "Promote Canary" for update the stage.
  Exist two ways to do a deployment wit API and Canary:

  * The deployment with Lambda (Api gateway point to a Lambda Alias)
  * The deployment with Api Gateway

* **API Throttling** by default, API Gateway limits the steady-state request rate to 10,000 requests per second (rps). It limits the burst to 5,000 requests across all APIs within an AWS account. In API Gateway the burst limit corresponds to the maximum number of concurrent request submissions that API Gateway can fulfill at any moment without returning "429 Too Many Request" error responses. You can also need to remember you can have throttling in your Lambda function. You can also have a Throttling a User Plan level, for example you can limit an API to 1,000 request per second. 

* **Fronting Step Functions** we can create an API gateway that invokes an Step Function (or any AWS service). Remember to create the role for API Gateway to access the Step Function.



**Reference Links**

- https://aws.amazon.com/blogs/compute/introducing-amazon-api-gateway-private-endpoints/

- https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-request-throttling.html

- https://docs.aws.amazon.com/step-functions/latest/dg/tutorial-api-gateway.html

  

#### **ECS**

* **ECS Clusters** are logical grouping of EC2 instances, this instances run the ECS agent (Docker container), the agents registers the instance to the ECS cluster. 
  The EC2 instances run a special AMI, made specifically for ECS
  Clusters Templates

  * Networking Only
  * EC2 Linux + Networking
  * EC2 Windows + Networking

  The ECS Cluster creates an Auto Scaling group, Launch Configuration, and SGs
  The file `/etc/ecs/ecs.config` have the information of the name cluster and the backed host, this is necessary for the registration to the ECS Cluster, this agent run in a docker container in the same EC2 instance

* **ECS Task Definitions** are metadata in JSON format to tell ECS how to run a Docker Container, it contains crucial information around:

  * Image Name
  * Port Binding for Container and Host
  * Memory and CPU required
  * Environment variables
  * Networking information

  We need a Task Role for the Task Definition, without this maybe cannot pull the image from the ECR, cannot talk with the S3, etc.
  In the Task definition we add the container information, such as the name, the image, the memory/cpu limits, the port mapping, etc. 

* **ECS Service** help define how many tasks should run and how they should be run. They ensure that the number of tasks desired is running across our fleet of EC2 instances. They can be linked to ELB / NLB / ALB if needed. 
  In a service we can configure how many replica we wants (number of tasks), the deployment configuration, for example Rolling update, or Blue/Green deployment, if we want a ELB, the service discovery (optional), and we can configure AutoScaling if we want. 
  If we try to configure 2 replicas in the service, we can't because the 2 containers try to expose the same port. The solution is first scale the ECS instances in a cluster (This scale the ASG), and after that, the task scale automatically, and we can have 2 containers

* **ECS Service with ELB** with this configuration we can mapping different ports, we only specify a container port and the host port becomes random, this feature is called Dynamic Port Forwarding, is how you run multiple same task on the same EC2 instance. If we want configure this, we need to edit the container information and in Port mapping leave the "Host port" empty (this configure the port in random, in the host definition appears port 0), after that we need to update our Service with the new Task Definition Revision, but we can't edit a Service and add an ELB, we need to create a New Service, and for the dynamic host port mapping we need to create the new service with the Application Load balancer option, before this we need to create an ALB, and after that we can finish the Service creation. For the last, we need to configure the ALB to talk with the EC2 instances, for this we need to edit the SG of the EC2 instances, and permit the traffic from the ALB SG on ALL PORTS. 

* **ECR** is a private Docker image repository. Access is controlled through IAM (permission errors -> policy)
  We need to run some commands to push pull:

  * $(aws ecr get-login --no-include-email --region eu-west-1)
  * docker push 123456789.dkr.ecr.eu-west-l.amazonaws.com/demo:last
  * docker pull 123456789.dkr.ecr.eu-west-l.amazonaws.com/demo:last

  When we use ECR in the ECS configuration, we need to put the complete URL in the Image field. The EC2 instances need permissions to read the ECR for download the images (is very common IAM error)

* **Fargate** is the ECS Serverless, we don't need provision EC2 instances and administrate, we just create task definitions, and AWS will run our containers for us. Fargate need a "Task Definition" for Fargate. Fargate will create the Task Definition, the service, and the cluster, we need to edit the values. The cluster template is "Networking Only" (Because we don't need EC2 instances).

* **ECS & Multi Docker Beanstalk** we can run EB in single & multi docker container mode, multi Docker helps run multiple containers (different images) per EC2 instance in EB
  This will create for us:

  * ECS Cluster
  * EC2 instances, configured to use the ECS Cluster
  * Load Balancer (in high availability mode)
  * Task definitions and execution

  Requires a config file `Dockerrun.aws.json` at the root of source code. Your Docker images must be pre-built and stored in ECR for example

* **ECS - IAM Roles** 

  * ECS Classic have two roles:
    * EC2 Role
    * Task Role
  * ECS Fargate only the Task have a Role (because don't use EC2)

* **ECS - Auto Scaling** we can configure a Service Auto Scaling (optional), we need to specified an minimum, desired and maximum number of task and we need to add a Auto Scaling Policy, for example a Target tracking (track ECS service, for example Average Cpu) or a Step Scaling (use an existing alarm, or we can create a new).
  For autoscaling with ECS Classic we need to configure two autoscaling policies, one for the ECS, and the another for the ECS instances (if we use Farget is much easier because we don't need autoscaling with EC2).
  A way to make Auto Scaling easy for ECS Classic is using Beanstalk with multi-container Docker. 

* **ECS - CloudWatch Integrations** we can configure our containers to send the information to CloudWatch logs, we only need to create the IAM permission (use the task definition). If we want the logs from the EC2 os, we need to install the CloudWatch Agent. 

* **ECS - CodePipeline CICD**  we can configure for example:
  Git repository to Codepipeline to CodeBuild to ECR, 
  On parallel: Codepipeline to CloudFormation to ECS



Exercises:

* Creates a ECS Cluster with a Task Definition for a WebServer with a public 80 port, with a service with two task, 
  * First without an ELB
  * Second, create a new revision of Task Definition with an ELB with 4 instances running on 2 EC2 instances. 
  * Third, the same, but with an image in ECR 



**Reference Links**

- https://docs.aws.amazon.com/AmazonECS/latest/developerguide/service-auto-scaling.html

- https://docs.aws.amazon.com/AmazonECS/latest/developerguide/using_awslogs.html

- https://docs.aws.amazon.com/AmazonECS/latest/developerguide/using_cloudwatch_logs.html

- https://ecsworkshop.com/introduction/cicd/

- https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-cd-pipeline.html

- https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-ecs-ecr-codedeploy.html

- https://aws.amazon.com/blogs/devops/build-a-continuous-delivery-pipeline-for-your-container-images-with-amazon-ecr-as-source/

  

#### **Opsworks**

* The only topic important for the exam is OpsWorks Stacks (no ChefAutomate/Puppet Enterprise). 
  OpsWorks Stacks is about using chef cookbooks to deploy our application. With OpsWorks we can managing both, our infrastructure and our configuration with one service.
  **Stack** is a set of layers, instances and related AWS resources whose configuration you want to manage together. In Opsworks we create stacks and each stack will have a set of instances, this instances can be different types like 24/7, Time and Load (we have to define them in advance)
  With one OpsWorks stack we have 3 layers:
  
  * ELB Layer
  * Application Server Layer (this layer can be provided using Cookbook)
  * Amazon RDS Layer
  
  To configure a Deploy we use Lifecycle Events, this is the deploy and shutdown lifecycle (setup, configure, deploy, undeploy, shutdown)



**Reference Links**

- https://docs.aws.amazon.com/opsworks/latest/userguide/welcome_classic.html
- https://docs.aws.amazon.com/opsworks/latest/userguide/workingcookbook-events.html
- https://docs.aws.amazon.com/opsworks/latest/userguide/workingcookbook-json.html



____________

###  Monitoring and Logging 

#### CloudTrail

* **Cloudtrail** is able to track all api calls in our account, for this we need to create `trail` and with this we can save in an S3 bucket and also we can save in CloudWatch Logs. Cloudtrail can save the information in the S3 bucket using a Bucket policy. The file generated in S3 shows; who made the request, when, from where was requested, and also what was a response. **Cloudtrail is not realtime, it is possible for it to have a 15 minutes delay**

* **Cloudtrail Log Integrity** we can view the integrity of the log file (for example if a hacker change the file). We can do this using the AWS CLI and `validate-logs` command. With this we can detect

  * Modification or deletion of CloudTrail log files

  * Modification or deletion of CloudTrail digest files

  * Modification or deletion of both of the above

    **CloudTrail Digest** File Structure. Each **digest** file contains the names of the log files that were delivered to your Amazon S3 bucket during the last hour, the hash values for those log files, and the digital signature of the previous **digest** file.

* **CloudTrail Cross Account Logging** we can configure CloudTrail log Files from multiple accounts into a single S3 bucket, for this we need to:

  * Turn on Cloudtrail in the destination account
  * Update the bucket policy to grant cross-account permissions to CloudTrail
  * Turn on Cloudtrail in the other accounts

  If we want to add read access to the bucket for the other accounts, we need to create an Access Policy attach to an IAM role

#### Kinesis

* **Kinesis** is a managed alternative to Apache Kafka, great for application logs, metrics, IoT, clickstreams. 
  Great for "real-time" big data. Great for streaming processing frameworks (Spark, NiFi, etc). Data is automatically replicated to 3 AZ.
  Services:
  * **Kinesis Streams** low latency streaming ingest at scale. Streams are divided in ordered Shards/Partitions. *Producer -> Shard1, Shard2, etc -> Consumer*. Data retention is 1 day by default, can go up to 7 days. Ability to reprocess/replay data. Multiple applications can consume the same stream. Real-time processing with scale of throughput (more shard you have, more throughput yo have). Once data is inserted in Kinesis, it can't be deleted (immutability)
    
    * **Shards** one stream is made of many different shards, billing is per shard provisioned, can have as many shards as you want. Batching available or per message calls. The number of shards can evolve over time (reshard / merge). Records are ordered per shard
    * **Records** are component by *Data Blog*, *Record Key a*nd *Sequence number*
      * **Data Blog** is data being sent, serialized as bytes. Up to 1 MB. Can represent anything
      * **Record Key** sent alongside a record, helps to group records in Shards. Same key = Same shard. Use a highly distributed key to avoid the "hot partition problem"
      * **Sequence number** is an unique identifier for each records put in shards. Added by Kinesis after ingestion. 
    
    Limits to know: 
    
    * Producer:
      * 1 MB/s or 1000 messages/s at write PER Shard
      * "ProvisionedThroughputException" otherwise
    * Consumer Classic:
      * 2MB/s at read PER SHARD across all consumers
      * = if 3 different applications are consuming, possibility of throttling
    * Data Retention:
      * 24 hours data retention by default
      * Can be extended to 7 days
    
    **Kinesis producers -> Kinesis Streams -> Kinesis Consumer**
    
    **Kinesis Producers** are composed by Kinesis SDK, Kinesis Producer Library (KPL), Kinesis Agent, CloudWatch Logs, 3rd party libraries, all of these can inject data to the Kinesis Strems
    
    **Kinesis Consumers** are composed by Kinesis SDK, Kinesis Client Library (KCL), Kinesis Connector Library, Kinesis Firehose, Lambda, 3rd party libraries such as Spark, Log4j
    
    **Kinesis KCL**, KCL uses DynamoDB to checkpoint offsets, and used DynamoDB to track other workers and share the work among shards
    
  * **Kinesis Analytics** perform real-time analytics on streams using SQL, is a managed service with autoscaling, is real time, only pay for actual consumption rate.
  
  * **Kinesis Firehose** load streams *into* S3, Redshift, ElasticSearch *from* KPL, Kinesis Agent, Kinesis Data Streams, CloudWatch Logs & Events, IOT Rules action
    
    For example Clicks streams, IoT devices or metrics & logs send information to Kinesis Streams, Streams send the data to Kinesis Analytics (real time analytics, aggregations logs, joins, etc), and after that we can deliver that data to Kinesis Firehose, and this services can send the data to S3. We don't need to use the 3 services together. 
    
    Is a fully Managed Service with automatic scaling, Near Real Time (60 seconds latency) we can Load data into Redshift / Amazon S3 / ElasticSearch / Splunk. Data Transformation through AWS Lambda (ex: CSV -> JSON). Supports compression when target is S3 (GZIP, ZIP, and SNAPPY), pay only for the amount of data going through Firehose. 
    
    Difference between Streams and Firehose:
    
     * Streams
       	* Going to write custom code (producer / consumer)
          	* **Real time** ( 200 ms latency for classic)
          	* Must manage scaling (shard splitting / merging)
                  	* Data Storage for 1 to 7 days, replay capability, multi consumers
                  	* **Use with Lambda to insert data in real-time to ElasticSearch** (for example)
    * Firehose
      * Fully managed, send to S3, Splunk, Redshift, ElasticSearch
      * Serverless data transformation with Lambda
      * **Near real time** (lowest buffer time is 1 minute)
      * Automated Scaling
      * No data storage
      
      
  

#### CloudWatch

* **CloudWatch Metrics** contains all the metrics from all the advice services that do track the metrics

  * Basic metric are free, five minutes
  * Detailed metric you will charged, every single minute

  Maximum data retention of 15 months in CloudWatch, but CloudWatch retains metric data as following:

  * Data points with a period of less than 60 seconds are available for 3 hours

  * Data points with a period of 60 seconds are available for 15 days

  * Data points with a period of 300 seconds are available for 63 days

  * Data points with a period of 3600 seconds are available for 455 days (15 months)

    You can for example have full retention of your data over time and you can configure with granularity using ElasticSearch

  **Metrics to Know** 
  We only need to know if the metric exist for default, or we need to create a custom metrics. For example EC2 Ram metric is not available, is a custom metric.

  **Custom Metrics**

  * Standard resolution with data having a one minute granularity
  * High resolution, with data at a granularity of one second

  Metrics produced by AWS services are standard resolution by default, when we publish a custom metric, we can define it as either standard resolution or high resolution

  **CloudWatch Metrics Export** if we want export all the metrics for example into S3 or into ElasticSearch, we can do this using an API call, `aws cloudwatch get-metrics-statistics -Namespace...` this command generate a json output with all the timestamps and the values, we can pipe this output to S3 or ES, we can automate this using CloudWatch Event with a Rule every hour that call a lambda function with the same Api Call and send the output to S3 or ES. 

* **CloudWatch Alarms** allow you to automate your infrastructure as a DevOps based on your behavior of your services or application (using metrics). We can generate an alarm using any metrics, even custom metrics. We can't combine multiple metrics into one alarm. For the alarm condition we can use different threshold types, static or anomaly detection, and we can define the condition for greater, lower, lower-equal, etc. After that we can configure the actions (notification) for example if the alarm is activate use a SNS to receive the notification, also we can add an email endpoint (we don't need to use SNS), we can create the actions for ALARM, OK, INSUFFICIENT DATA. 
  We also can for example configure an AutoScaling action, if some CPU of EC2 is reach, trigger an new EC2 instance. Also we can configure an EC2 Action, for example is the alarm is trigger Stop, terminate or reboot the instance. 
  *We can't use CloudWatch Alarm source in a CloudWatch Event Rule.* 

* **VERY IMPORTANT**
  
  Cloudwatch Alarm only can send notifications, Autoscaling Action or Ec2 Action
Some differences between Alarm an Events are:
  
  - Events can self-trigger based on a schedule; alarms don't do this
- Alarms invoke actions only for sustained changes
  - **Alarms watch a single *metric* and respond to changes in that metric; events can respond to *actions*** (such as a lambda being created or some other change in your AWS environment)
  - **Alarms can be added to CloudWatch dashboards, but events cannot**
  - Events are processed by targets, with many more options than the actions an alarm can trigger
  
  We can use an alarm when we know that the thing we are measuring is all that's needed to trigger an action.

  **For example, if I want to receive an email when my EC2 instance is using 90 percent CPU, I create an alarm. I only need to receive one notice that the CPU use is high. If I used an event stream, I would get hundreds or thousands of emails letting me know that CPU use is high, because the event stream records conditions continuously.**

  **CloudWatch Billing Alarms** we can only configure this in N. Virginia, and we can for example set a monthly alarm if the charges raise the 10 usd, or we can create and billing alarm for an specified service. 

* **CloudWatch Logs** You can use this to monitor, store, and access your log files from EC2, AWS CloudTrail, Route 53, and other sources. Enables you to centralize the logs from all of your systems, applications, and AWS services that you use. 
  This is composed by Logs Groups, this is a giant directory of stuff that contains logs streams and each lo stream will contain the log it self 
  Log Group example /aws/codebuild/myApp
  Log Stream example adb93a72-fb44-4169 (Is arbitrary the naming)

  If we click on the Log Stream we can see the log it self. 
  The logs started like /aws are logs managed by AWS.

* **CloudWatch Unified Agent** is a new kind of agent which does metrics and logs at the same time, we can send both metrics custom metrics and logs into CloudWatch, before use this we need to create a new role with access to CloudWatch from EC2, and attach to the instance, after that we need to install the Unified CloudWatch Agent, (wget,, rpm -ivh, execute the binary). 
  We also save the json config to parameter store, for this we need to add the `CloudWatchAgentAdminPolicy` to the Rol. This is very useful because we configure the CUA in one ec2 instances, save in SSM, and after that we can use in other EC2. We can load the configuration from SSM or a file. This new feature give us metrics like mem used, disk usage, swap, etc.
  
  To deploy CloudWatch monitor agent to the instances we can use: CloudFormation, OpsWorks and Elastic Beanstalk 

* **CloudWatch Logs - Metric Filters & Alarms** we can create Logs metrics filters to monitor events in a log group as they are sent to CloudWatch Logs. We can monitor and count specific terms or extract values from log events and associate the results with a metric. For example we can create a filter patter looking for 404 errors, and assign a *metric*, after that we can configure an Alarm, for example, if we have five 404 in two minutes send an alert.

* **CloudWatch Logs Export To S3** we can export our logs to S3, we can select the period, the prefix, the bucket name, the account, before do this, we need to configure the correct bucket policy. We also can automate the export to S3 using CloudWatch Events (using Lambda Function). We can improve this using CloudWatch Logs Subscriptions (more expensive)

* **CloudWatch Logs Subscriptions** using this we can export the logs to S3 in near realtime, but is more expensive than using CloudWatch Events with Lambda Function. Also we can analyze logs in real-time using Subscriptions from CloudWatch Logs and have it delivered to other services such as a **Kinesis Stream, Or Kinesis Firehose, or Lambda** for custom processing, analysis, or loading to other systems (for example can save to Elastic Search).
  To begin subscribing to log events, create the receiving source, such as a Kinesis stream, where the events will be delivered. A subscription filter defines the filter pattern to use for filtering which log events get delivered to your AWS resource.
  
* **CloudWatch Namespaces** A *namespace* is a container for CloudWatch metrics. Metrics in different namespaces are isolated from each other, so that metrics from different applications are not mistakenly aggregated into the same statistics. The AWS namespaces typically use the following naming convention: `AWS/service`. For example, Amazon EC2 uses the `AWS/EC2` namespace

* **All kind of logs** 
  * Application Logs: Logs that are produced by our application Code. Contains custom log messages, stack traces, etc. Written to a local file on the filesystem. Usually streamed to CloudWatch Logs using CloudWatch Agent on Ec2. Lambda, ECS/Fargate or Elastic Beanstalk also have direct integration with CloudWatch Logs
  * OS Logs (Event Logs, System Logs): Logs that are generated by our OS. Informing you of system behavior (ex: /var/log/messages or /var/log/auth.log). Usually streamed to CloudWatch Logs using a CloudWatch Agent
  * Access Logs: list of all the requests for individual files that people have requested from a website (ex: /var/log/apache/access.log). Usually for load balancers, proxies, web servers, etc. AWS provides some access logs
  * **AWS Managed Logs:**
    * ELB Access Logs (alb, nlb, clb) -> S3
    * CloudTrail Logs -> S3 and CloudWatch Logs (Logs for API calls made within your account)
    * VPC Flow Logs -> S3 and CloudWatch Logs (Information about IP traffic going to and from network interfaces in your VPC)
    * Route 53 Access Logs -> CloudWatch Logs (Log information about the queries that route 53 receives)
    * S3 Access Logs -> S3 (Server access logging provides detailed records for the requests that are made to a bucket)
    * CloudFront Access Logs -> S3 (Detailed information about every user request that CloudFront receives)
  
* **CloudWatch Events** delivers a near real-time stream of system events that describe changes in AWS resources, is very useful to bring all AWS services together and build automation.
  When we create a rule we can set the Event Source from an *Event Pattern* or *Schedule* (similar to cron), after that we need to configure a Target, this is the process to invoke is the Event Pattern or the Schedule happens, the most common targets is a Lambda Function, other can be a SNS topic, SQS queue, SSM Automation, Kinesis Streams, CodeBuild, CodePipeline, Step Functions, create actions to EC2, etc.
  An Event Pattern example is for example for Codepipeline Service, with an Event Type "CodePipeline Execution State Change", we also can modify the Event Type code (is a JSON document)
  
* **CloudWatch Events Integration with CloudTrail API** for example, if we want to create a CloudWatch Event if someone creates an EC2 Image, we can do that using from Event Pattern CloudTrail APi Calls and select service EC2, we need to select "Specific operations" and put "CreateImage" (is the api call name). This combination is very useful for create powerful integrations and intercept any API calls, and for example send a notification using SNS, or call a Lambda Function. 

* **CloudWatch Events VS S3 Events** S3 Events allow us to create notifications based on events (PUT, POST, Permanently deleted, Multipart upload completed, etc) we can send this event to SNS Topic, SQS Queue or a Lambda Function (Before that we need to configure the permissions, for example allow S3 bucket to publish notifications on SQS queue). S3 Events have only a few events, if you want some more complex you need to use CloudWatch Events Rules, also S3 Events don't give us any bucket level operation (only object operation) and CloudWatch Event rules does both. 

* **CloudWatch Dashboards** we can create Dashboards we can add widget such as Line, Stacked Area, Number, Text, Query Results. The Dashboards are multi-region. If in the exam appears something like "How can i *correlate* data?" the answer is CloudWatch Dashboard



#### X-Ray

X-ray makes easy to analyze the behavior of their distributed applications by providing request tracing, exception collection, and profiling capabilities (Is a tool similar to Jagger o New Relic). Is very useful for debug things, or if you need to see traces, debugging distributed application your service is X-Ray, is different from CloudWatch because doesn't publish any metrics or logs.
A good example for X-Ray and automation is using CloudWatch and Amazon SNS to Notify when X-Ray detect elevated levels of latency, errors, and faults in our application (ex: 429 status code-throttle, 5xx status code-faults, 4xx status code-errors)



#### Amazon ES

Amazon ES is a managed version of ElasticSearch, need to run on servers (not a Serverless offering). Use Cases: Log analytics, real time application monitoring, security analytics, full text search. clickstream analytics, indexing. The ES service is composing by ElasticSearch + Logstash + Kibana

* ElasticSearch: provide search and indexing capability. You must specify instance types, multi-AZ, etc
* Kibana: real-time dashboards on top of the data that sits in ES. Alternative to CloudWatch dashboards (more advanced capabilities)
* Logstash: log ingestion mechanism, use the "Logstash Agent". Alternative to CloudWatch Logs (you decide on retention and granularity)

We can also use ES patterns with DynamoDB, for example if we want to search an index in dynamo we need to do a full scan, if we send the writes to -> DynamoDB Stream -> Lambda Function -> Amazon ES.
We can search the index with ELK



#### **Tagging**

The tags are metadata, each tag is simple label with key-value notation, that can make it easier to manage, search for, and filter resources. They are commonly used tagging categories and strategies. 
Tagging strategies are good for cost accounting, we also can use tags for specified deploys using CodeDeploy, another useful use is for security, we can use tags combine with IAM policies and conditions statements to define some users only have access to some AWS resources, for example only access to EC2 instances with tag Dev (this is called Tag Based Control)



**CloudTrail:**

- https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-log-file-validation-cli.html
- https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-receive-logs-from-multiple-accounts.html
- https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-sharing-logs.html

**CloudWatch:**

- https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/cloudwatch_concepts.html#Metric
- https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/metrics-collected-by-CloudWatch-agent.html#linux-metrics-enabled-by-CloudWatch-agent
- https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/Counting404Responses.html
- https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/Subscriptions.html
- https://aws.amazon.com/blogs/big-data/power-data-ingestion-into-splunk-using-amazon-kinesis-data-firehose/
- https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/SubscriptionFilters.html#FirehoseExample
- https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/Create-CloudWatch-Events-CloudTrail-Rule.html
- https://docs.aws.amazon.com/AmazonS3/latest/user-guide/enable-event-notifications.html
- https://linuxacademy.com/community/show/19561-alarm-vs-event/

**X-Ray**:

- https://docs.aws.amazon.com/xray/latest/devguide/aws-xray.html
- https://aws.amazon.com/blogs/devops/using-amazon-cloudwatch-and-amazon-sns-to-notify-when-aws-x-ray-detects-elevated-levels-of-latency-errors-and-faults-in-your-application/

**Amazon ES:**

- https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CWL_ES_Stream.html

**Tagging in AWS**

- https://aws.amazon.com/answers/account-management/aws-tagging-strategies/



______

### Policies and Standards Automation

#### Systems Manager (SSM)

* Helps you manage your EC2 and On-Premise systems at scale, is very useful for **Patching automation for enhanced compliance**. Easily detect problems, works for Windows and Linux. Integrated with CloudWatch metrics/ Dashboards. Integrated with AWS Config. Free service 

* Features

  * Resource Groups
  * Insights
    * Insights Dashboard
    * Inventory: Discover and audit the software installed
    * Compliance
  * Parameter Store
  * Action
    * Automation (shutdown EC2, create AMIs, etc)
    * Run Command
    * Session Manager
    * Patch Manager
    * Maintenance Windows
    * State Manager: Define and maintaining configuration of OS and applications

* We need to install the SSM agent onto the systems we control (Installed by default on Amazon Linux AMI & some Ubuntu AMI). If an instance can't be controlled with SSM, it's probably an issue with the SSM agent. Make sure the EC2 instances have a proper IAM role to allow SSM action.

* **SSM EC2 Setup** we need to install the agent and configure the service `amazon-ssm-agent` with systemctl, and add the IAM  Role with the permissions to SSM, and we need to configure a tag, for example with the environment. 

* **SSM EC2 On-premise setup**  this is the hybrid instances, are distinguished from EC2 instances with the prefix `mi-` (Ec2 use the prefix `i- `)
  Steps:

  ```
  mkdir /tmp/ssm
  curl $URL/ssm-agent.rpm -o /tmp/ssm
  yum install -y ssm-agent.rpm
  systemctl stop amazon-ssm-agent (and edit the code, id and region)
  amazon-ssm-agent -register -code "activation-code" -id "activation-id" -region "region"
  systemctl start amazon-ssm-agent
  ```

  Before this, we need to create an activation code using the GUI, the activation use an IAM role. This generate two codes, the Activation Code and the Activation ID. 
  
* **SSM Resource Groups** with this we can group resources together, with two types:

  * **Tag based** Group resources by specifying tags that are shared by the resources. For example we can group all EC2 with the tag DEV
  * **CloudFormation stack based** use an existing CF stack, the group will have the same logical structure as the stack

* **SSM Run Command** we can run documents ad-hoc, like Ansible/Puppet, etc. We can use and create Documents (is like playbooks in Ansible), for example we can run a Document for "UpdateSSMAgent" or for example we can run "RunShellScript" and upload an script. We can save the output in a Bucket and integrate with SNS notifications.

* **SSM Parameter Store** we can store parameters in plain text or encrypted (using KMS) the parameters are saved in different versions, this is very useful to automated tasks a simplify an architecture or to centralize parameters.

* **SSM Patch Manager** automates the process of patching managed instances. We can use Patch Manager to apply patches for both operating systems and applications. Patch Manager uses *patch baselines*, which include rules for auto-approving patches within days of their release, as well as a list of approved and rejected patches. 

* **SSM Inventory** is used to have a list of all the stuff running on to our instances that is tracked by SSM Agent (Also we can configure only for a specifying tag). Collect inventory data every a specified time (example 30 min), and for example info about applications, network config, windows updates, etc, we can save the information to an S3 buckets, this generate a Dashboard with a lot of information. 

* **SSM Automations** simplifies common maintenance and deployment tasks on EC2 and other AWS resources. Is useful for example to create a Golden AMI

* **SSM Session Manager** lets you manage your EC2 instances using the browser, or throught the AWS Cli. You don't need to open ports, maintain bastion hosts, or manage SSH keys, and generate fully auditable logs with instance access details.



#### AWS Config

* **AWS Config** provides an inventory of your AWS resources and a history of configuration changes to these resources (the output is sending to S3). We can use AWS Config to define rules that evaluate these configurations for compliance. AWS Config make a TRACK of services. IS VERY EXPENSIVE service.  We need to configure the necessary permissions, for example the access from AWS Config to the specified S3 bucket

* **AWS Config Rules** allows us to define rules and maintain COMPLIANCE for all the resources according to these rules and we will get out of it is an audit trail and a compliance status with a dashboard. We can use custom rules, or saved rules (for example restricted-ssh). For a custom rule we need to provide a Lambda function with the rule.

* **AWS Config Automations** if a rule have an issue we can integrate with Cloudwatch Event for take an action, we can configure for different Event Types, for example "Config Rules Compliance Change" "Config Item change", etc. Another method is using AWS Config Remedation, this use SSM Automation to remediate a noncompliant rule. With both we can do the same. <-  **IMPORTANT**

* **AWS Config Multi Account** we can make a *Data Aggregation* for multiple accounts and multiple regions, single account and multiple regions, and an organization in AWS organization

  

#### Others

* **AWS Service Catalog** is a quick *serf-service portal* to launch a set of authorized products pre-defined by admins (includes vms, dbs, storage options, etc). 
  Create and manage catalogs of IT services that are approved on AWS. the "products" are CloudFormation templates. Integrations with "self-service portals" such as ServiceNow.

* **Inspector** enables you to analyze the behavior of your AWS resources and helps you identify potential security issues, for this we need to install the AWS agent on EC2 instances, run an assessment for our assessment target and review our findings and remediate security issues.

  For install the agent, we can do this with the GUI of Inspector, in the background use SSM.
   Two types of assessment:

  * **Network** (Agent is not required) configuration analysis to checks for ports reachable from outside the VPC
  * **Host** (Agent is required) vulnerable software (CVE), host hardening (CIS benchmarks), and security best practices.

  We can automate Amazon Inspector using Cloudwatch Events to automatically run assessments. We can set up a recurring Schedule event or monitor other AWS services for actions to trigger an assesment.

* **EC2 Instance Compliance** 

  * **AWS Config** ensure instance has proper AWS config (not open SSH, etc). Audit and compliance over time
  * **Inspector** is for security vulnerabilities scan from within the OS using the agent or outside network scanning
  * **Systems Manager** run automations, patches, commands, inventory at scale
  * **Service Catalog** restrict how the EC2 instances can be launched to minimize configurations, helpful to onboard beginner AWS user
  * **Configuration Management** SSM, Opsworks, Ansible, Chef, Puppet, User Data. Ensure the EC2 instances have proper configuration files.

* **Health Dashboards**

  * **Service** is a global view status of AWS services for each region
  * **Personal** view status of your personal AWS services, is more personalize, you can configure alerts using Cloudwatch Events

* **Trusted Advisor** gives us recommendations for your accounts on five categories (Cost Optimization, Performance, Security, Fault Tolerance, Service Limits). They have two tiers, free tier, and Support plan. 
  This service is very similar to Cloud Conformity service.

* **Trusted Advisor with Automations** we can integrate with CloudWatch Events to detect and react to changes in the status of Trusted Advisor checks. You can send notifications, capture status information, take corrective action, initiate events or take other actions.  We can select the following types of targets when using CloudWatch Event:

  * AWS Lambda Functions
  * Amazon Kinesis Streams
  * Amazon SQS queues
  * Built-in targets (CloudWatch alarm actions)
  * Amazon Simple Notification Service topics

  Some uses cases:

  * Stop Amazon EC2 instances with low utilization
  * Create snapshots for EBS volumes with no recent backup
  * Delete exposed IAM keys and monitor usage
  * Enable S3 bucket Versioning
  * Service Limit Dashboard 

* **Trusted Advisor with Automation Refreshes** we can do this manual one time each five minutes, or we can automating this using programmatic access and the AWS support API

* **GuardDuty** is an intelligent Threat discovery to protect AWS accounts, run some analysis in the background. Uses Machine Learning algorithms, anomaly detection, 3rd party data. 
  One click to enable (30 days trial), no need to install software

  Input data includes:

  * CloudTrail Logs: unusual API calls, unauthorized deployments
  * VPC Flow Logs: unusual internal traffic, unusual IP address 
  * DNS Logs: compromised EC2 instances sending encoded data within DNS queries

  Notifies you in case of findings something. Integration with AWS Lambda

  GuardDuty with automation, we can create a automation using CloudWatch Events, and for example integrate with Lambda and notified to a Slack Channel. 

* **Macie** is a data visibility security service that helps classify and protect our sensitive and business-critical content (Available only N.Virginia/Oregon). For example analyses sensitive data in a S3 bucket

* **Secrets Manager** easily rotate, manage and retrieve secrets throughout their lifecycle (integrated with MySQL, PostgresSQL, and Aurora). Is better use Parameter store because is free, but SM have the RDS integration (the rotation feature is really just a Lambda Trigger), we can easily recreate this via a Cloudwatch event which trigger the same lambda when a parameter store changes.

* **License Manager** makes it simple to manage licenses from third-party vendors, such as Microsoft, Oracle, and SAP, when you bring them to AWS. You can control license usage and set customized rules on license management and usage for different groups of users. 

* **AWS Cost Allocation Tags** with tags we can track resources that relate to each other. With Cost Allocation Tags we can enable detailed costing reports. Just like Tags, but they show up as columns in reports. Two types of Cost Allocation tags:

  * **AWS Generated,** they are automatically applied to the resource you will create and they will start with a prefix `aws:` (e.g. aws: createdBy)
    They are not applied to resources created before the activation
  * **User tags,** defined by the user, added manually or automatically using the CLI or Console, starts with prefis `user:`

  Cost Allocation Tags just appear in the Billing Console, takes up to 24 hours for the tags to show up in the report

* **Data Protection**

  * TLS for in Transit Encryption, 
    * ACM to manage SSL/TLS certificates
    * Load Balancer, we can add certificates using ACM. ELB, ALB & NLB provide SSL termination (Only HTTPS in the ALB and the EC2 instances uses HTTP)
      Possible to have multiple SSL certificates per ALB, optional SSL/TLS encryption between ALB and EC2 instances
    * CloudFront with SSL
    * All AWS services exposes HTTPS endpoints (s3 also expose HTTP endpoint)
  * At Rest Encryption
    * S3 encryption
      * SSE-S3: Server Side Encryption using AWS key
      * SSE-KMS: Server Side Encryption using our own KMS key
      * SSE-C: Server Side Encryption by providing our own key (AWS won't keep it)
      * Client Side Encryption: send encrypted content to AWS, no knowledge of key
      * Possibility to enable default encryption on S3 through setting
      * Possibility to enforce encryption through S3 bucket policy (x-amz-server-side-encryption)
      * Glacier is encrypted by default
    * One quick setting for: EBS, EFS, RDS, ElastiCache, DynamoDB, etc
      * Usually uses either service encryption key or your own KMS key
    * Category of data: (for this data we need very strong encryption and security)
      * PHI = protected health information
      * PII = Personally identifying information

* **Network Protection**

  * Direct Connect: Private, direct connection between site and AWS 
  * Public internet: use a VPN
    * Site-to-Site VPN supports internet protocol security (IPSsec) VPC connections
  * Network ACL: stateless firewall at the VPC level
  * WAF (Web Application Firewall): web security rules against exploits
  * Security Groups: stateful firewall on the instance's underlying hypervisor

  

Exercises:

* Create 3 ec2, 2 for dev and one for prod, with SSM
* Create one on premise and configure the agent with the permissions.
* Execute a script using SSM Run Command and a EC2 instance with a tag and the script need to retrieved a password from parameter store



**Reference Links**

- https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-managedinstances.html
- https://docs.aws.amazon.com/systems-manager/latest/userguide/activations.html
- https://docs.aws.amazon.com/systems-manager/latest/userguide/patch-manager-approved-rejected-package-name-formats.html
- https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-automation.html
- https://docs.aws.amazon.com/systems-manager/latest/userguide/automation-walk-patch-linux-ami-console.html
- https://docs.aws.amazon.com/systems-manager/latest/userguide/automation-cf.html
- https://d1.awsstatic.com/whitepapers/aws-building-ami-factory-process-using-ec2-ssm-marketplace-and-service-catalog.pdf
- https://github.com/miztiik/AWS-Demos/tree/master/How-To/setup-ami-lifecycle-management-using-ssm



_________________

### Incident and Event Response & HA, Fault Tolerance, and DR

#### AutoScalingGroup

* **ASG from Launch Configuration** is a way to define how to create instances as part of your orders and groups, after that we can create an ASG and use the Launch Configuration

* **ASG from Launch Templates** is an advanced way of defining instances that we can start in different ways, for our instances automatically or our ASG. We can for example create an Launch Template with an ASG with on demand instances and spot instances. In templates we can have different versions, we don't need to create a new Launch Templates each time (like Launch Configuration). From our template we can:

  * Launch an instance from this template
  * Create an ASG from our template
  * Create Spot Fleet

  Launch template are new, and give us the option of launching one type of instance, or a combination of instance types and purchase options, and can be updated and versioned. 
  **If we want to specify a mix of OnDemand and Spot instances and multiple instance types for our ASG we need to use the Launch Templates**

* **ASG Scheduled Actions** we can for example increase the size of our ASG the Monday at 11:00 or increase the fleet because we have a special sale. So, if we know these kind of patterns in advance we may want to schedule some actions for ASG and make scale-in / scale-out. For this we need to create "Scheduled Action", we need to set the Min, Max, Desired Capacity, and the time (we can also use cron syntax)

* **ASG Scaling Policies** some important concepts are: 

  * `Default Cooldown` this is the number of seconds after a scaling activity completes before another can begin (default is 300). 
  * `Warm up` in Scaling Policy is how long the instances metric (for example AverageCpuUtilization) need to wait before the instance metrics goes into the metric AverageCpuUtilization. If the warm up number is higher than the Cooldown it's likely that you'll have more scale up.
  * `scale in` this is terminate instances, we can disable this in the Scaling Policy
  * `Scaling Policy` (normal) In the default SP we don't need to create CloudWatch Alarms, we have metrics such as CPUUtilization, Networking, ALB connections
  * `Simple Scaling Policy` in this we need to create an alarm in advance and we need to define an action, for example "Take the action: Add 2 instances" "And then what: 60 seconds before allowing another scaling activity". This is very useful for use previously created CloudWatch alarms. This give us more flexibility because we can any CloudWatch Alarm based on any metric 
  * `Steps Scaling Policy` this is useful for create steps actions, for example, if the CPUUtilization gets the 70 % add 1 instances, if the CPUUtilization gets the 80% add 3 instances. Is very similar to the simple scaling policy with steps actions. This also need CloudWatch alarms created in advance. 

* **ASG ALB Integration** this is very useful to have one endpoint, health checks and more. 
Slow Start Duration: We can configure an attribute for a warm up, this is useful when you don't want your new instances to be right away taking too many requests
  
* **ASG HTTPS on ALB** for this we need to buy a domain, and request an AWS certificate for the domain, after that we point the domain to the ALB. In the ALB we need to add a new listener for HTTPS with "Default Action" = "Forward to" the target group, and we can configure the "Default SSL Certificate" and we can choose the ACM Certificate. For last, we need to edit the SG and permit the HTTPs.
  We can also force redirection from HTTP to HTTPS, for do this, we need to go to "Listeners" and Enable Redirection from HTTP to HTTPS.

* **ASG Suspending Processes & Troubleshooting** We can select a lot of option to Suspended Processes, such as "Launch" "Terminate" "AZrebalance", "Health-Check", "ReplaceUnHealthy", "AlarmNotification", "ScheduleActions" and "AddToLoadBalancer"

  This is very useful mostly for troubleshooting
  Important: Use the standby feature instead of the suspend-resume feature if you need to troubleshoot or reboot an instance. 

* **ASG Lifecycle Hooks** allow you to control what happens when your Amazon EC2 instances are launched and terminated as you scale out and in. For example, you might download and install software when an instance is launching, and archive instance log files in Amazon Simple Storage Service (S3) when an instance is terminating. For associate actions we can use SNS, SQS and Lambda Using CloudWatch Events (this is the most flexible thing and the most recommended way). SNS and SQS is only configurable using the CLI

* **ASG Termination Policies** is about the priority for the terminate during scale in, for this we have "Default Policy" and "Custom Policy".
  The default termination policy is:

  * Determine which Availability Zones have the most instances, and at least one instance that is not protected from scale in
  * Determine which instances to terminate so as to align the remaining instances to the allocation strategy for the On-Demand or Spot Instance that is terminating
  * Determine whether any of the instances use the oldest launch template or configuration
  * After applying all of the above criteria, if there are multiple unprotected instances to terminate, determine which instances are closest to the next billing hour

* **ASG Integration with SQS** this is very common pattern if you have a SQS queue and you have ASG EC2 instances for processing the queue, for this we need a custom metric. The workflow is: The application read the SQS metrics (Number of messages in the queue) and also read the capacity of the ASG (Processing Servers), the application do the division, numbers of messages / number of servers and emits that number as Custom metric to CloudWatch, and we can create a scaling policy based on the alarm in CloudWatch metrics, this trigger an Auto Scaling Event and launch or terminate instances.
  If we had a large process and we want to protect instances from termination during a scale-in event, we can do this, configure the instance "Protected from" -> "Scale in" whatever the instance is processing, this is possible making a script in the EC2 instances and making an API call and protect for scale in. This also is very useful for a worker in an auto scaled pool that is performing a long-running task.

* **ASG CloudFormation CreationPolicy** is for create ASG using CNF. The "CreationPolicy" is a subpoint of the AutoScalingGroup resource in CNF, this is necessary for waiting for a ResourceSignal and for example Count=3 and Timeout=15m, this is for wait to 3 instances with a timeout of 15m if in 15m don't create 3 instances, CNF send a fail signal and the CNF make a rollback.

* **ASG CloudFormation UpdatePolicy** this is useful for update a ASG. For the update policy we can specified 3 policies:

  * AutoScalingReplacingUpdate: (Example WillReplace: true) Creates a new ASG and then terminates the old, is like an immutable update

  * AutoScalingRollingUpdate: (Example MinInstancesInService:1, MaxBathSize:2, PauseTime: PT1M)
    Creates the new instances in the same ASG.

  * AutoScalingScheduleActions: (Example IgnoreUnmodifiedGroupSizeProperties: true) This is useful for the rolling updates and prevent scheduled actions from modifying min/max/desired for CNF

    *If you don't specify any policy it will just update the LC but won't do anything to the instances*

* **ASG CodeDeploy Integration**

* **ASG CodeDeploy Integration Troubleshooting**

* **Deployment Strategies ASG and ALB**

  * In Place (one LB, one TG, one ASG)
  * Rolling (one LB, one TG, one ASG, new instances)
  * Replace (one LB, one TG, two ASG, new instances)
  * Blue / Green (two LB, two TG, two ASG, new instances, R53)
  
  

**Exercises:**

* Create an ASG HTTPS on ALB
* Create an ASG based on a SQS queue




#### DynamoDB

* **DynamoDB** is a Fully managed NoSQL db, with HA in 3AZ, is made of tables, each table has a PK, and each table can have an infinite number of items (rows), each item has attributes (maximum size of a item 400kb)

  - PK: 

    - Partition Key only: must be unique for each item
    - Partition Key + Sort Key: The combination must be unique, data is grouped by pk, sort key = range key

  - **Queries:** use FilterExpression to client side filter, returns up to 1 MB of data, you can use "Limit" to show only a number of items, able to do pagination on the results, can query a table, a lsi, or a gsi

  - **Provisioned Throughput:** Table must have read and write capacity units, option to setup autoscaling, throughput can be exceeded temporarily using "burst credit", if burst credit are empty, you'll get a "ProvisionedThroughputException", it's then advised to do an exponential back-off retry

  - Difference between eventually consistent and strongly consistent data: 

    - Eventually consistent: A read request immediately after a write operation might not show the latest change but costs less in terms of capacity. Most operations are eventually consistent by default but can usually be changed in your calls.
    - Strongly consistent: The most up-to-date data.

  - **RCU:** A unit of read capacity: 1 strongly consistent read per second or two eventually consistent reads per second for items as large as 4 KB 

    ```
    Strongly Consistent Read
    (ITEM SIZE (rounded up to the next 4KB multiplier) / 4KB) * # of items
    (Round up to the nearest 4 KB multiplier)
    * Eventually consistent reads would cut that in half
    ```

    **WCU:** A unit of write capacity: 1 write per second for items up to 1KB

    ```
    (ITEM SIZE (rounded up to the next 1KB multiplier) / 1KB) * # of items
    (Round up to the nearest 1 KB multiplier)
    ```

    

    **To get the number of strong and eventual consistent read requests that your table can accommodate per second, you simply have to do the following steps:**

    **Step #1 Multiply the value of the provisioned RCU by 4 KB**

    = 10 RCU x 4 KB

     = 40 

    **Step #2 To get the number of strong consistency requests, just divide the result of step 1 to 4 KB**

    Divide the Average Item Size by 4 KB since the scenario requires eventual consistency reads: 

     = 40 / 4 KB

     = 10 strongly consistent reads requests

      **Step #3 To get the number of eventual consistency requests, just divide the result of step 1 to 2 KB**

      = 40 / 2 KB

      = 20 eventually consistent read requests



- **Partitions internal:** Data is divided in partititions, WCU and RCU are spread evenly between partitions
- **DynamoDB Throttling:** If we exceed our RCU or WCU we get provisionedThrouhgputExceededExceptions
  
  - Reasons:
    - Hot keys: One PK is being read to many times
    - Hot partitions
    - Very large items
  - Solutions:
    - Exponential back-off when exception is encountered
    - Distribute pk as much as possible
    - If RCU issue, we can use DAX
- **Scan:** Scan the entire table an then filter out data (inefficient), returns up to 1MB of data, consumes a lot of RCU, limit impact using limit, for faster performance use "parallel scans". Can use a ProjectionExpressions + FilterExpression 

- **DynamoDB Indexes:**

  - Local Secondary Index (LSI): Alternate range key for your table, local to the hash key . LSI must be defined at table creation time. LSI if for efficient query by timestamp. Is called LOCAL because it is local to the Partition key. Max item size 10 GB. LSI has the same partition key as the table, but with different sort key.
  - Global Secondary Index (GSI): Speed up queries on non-key attributes. GSI = pk + optional SK. Basically define a new index, this is a new table an we can project attributes on it, the pk and sk of the original table are always projected (KEY_ONLY). Must define RCU / WCU for the index. Possibility to add / modify GSI (not LSI). Offer only eventual consistency
  - DynamoDB indexes can generate Throttling:
    - GSI, if the writes are throttled on the GSI, then the main table will be throttled. Even if the WCU on the main tables are fine. Choose your GSI pk carefully and assing your WCU capacity carefuly. GSI CAN THROTTILING YOUR MAIN TABLE
    - LSI uses the WCU and RCU of the main table, no special throttling considerations

- DynamoDB is Optimistic Concurrency / Optimistic Locking this mean because multiple clients can access the same object at the same time, DynamoDB for example if we delete something, only do that if a certain condition is validated. This use Conditional Writes.

- **DynamoDB Dax (Accelerator)** is a seamless cache for DynamoDB, no need to modified the application. Solves the Hot Key problem (HK = a key with to many reads). Default TTL 5 minutes, multi AZ (3 nodes minimum for prod). Dax write directly in DynamoDB 

- **DynamoDB streams:** Changes in DynamoDB (Create, update, delete) can end up in a DynamoDB Streams (use Kinesis in the background), this streams can be read by AWS Lambda an we can then do:

  - React to changes in real time
  - Analytics
  - Create derivate tables
  - Streams can implement cross region replication, an has 24 hours of data retention

  DynamoDB streams also enable us to activate Global Tables.

- **DynamoDB TTL:** Automatically delete an item after an expire date, not use WCU-RCU, don't have cost, helps reduce storage, TTL is enabled per row, DynamoDB typically deletes expires items in 48 hras. DynamoDB streams can help recover expired items

- **DynamoDB CLI:**

  - --Projection-expression: select attributes to retrieve (maybe from a DynamoDB scan)
  - --filter-expression: filter results
  - General Cli pagination:
    - Optimization: --page-size: full dataset is still received but each API call will request less data. Allow you to retrieve a subset of the attributes coming from a DynamoDB scan
    - Pagination:  (Useful to paginate the results of a DynamoDB scan in order to minimize the amount of RCU that you will use for that CLI command)
      - --max-items: max number of results returned by the CLI. Returns NextToken
      - --starting-token: specify the last received NextToken to keep on reading

- **DynamoDB transactions** is the ability to create/update/delete multiple rows in different tables at the same time, its an ALL OR NOTHING type of operation. Consumes 2x of WCU / RCU

- **DynamoDB Patterns** they are 2 common patterns

  - **Building a Metada Index for S3**
    Writes -> S3 (Using S3 Events) -> Lambda function -> Write object Metadata in DynamoDb Table (filename, size). DynamoDb allows us to search by date, get the total storage used by a customer, list of all object with an attribute, find all object in a specified date

  - **Elastic Search**

    API to retrieve items <--> DynamoDB > DynamoDB Streams -> Lambda -> Amazon ES  <--> API to search items.



#### Multi

* **Multi AZ services where must be enabled manually:**

  * EFS, ELB, ASG, Beanstalk: Assign AZ
  * RDS, ElastiCache: Multi-AZ (Synchronous standby DB for failovers)
  * Aurora: Data is store automatically across multi-AZ. Can have multi-AZ for the DB itself (same as RDS)
  * ElasticSearch (managed): multi master
  * Jenkins (self deployed): multi master

* **Multi AZ services where is implicitly:**

  * S3 (except OneZone-Infrequent Access)
  * DynamoDB
  * All of AWS proprietary managed services

* **Multi AZ and EBS**

  EBS is tied to a single AZ, but we can "hack" this. We need to configure an ASG with 1 for min/max/desired with Lifecycles hooks for when Terminate make a snapshot of the EBS volume, and the Lifecycles hook for Start, copy the snapshot, create an EBS, attach to the instance. 
  Instance Down -> EBS Volume Snapshot using TerminateHook (Lambda) -> Launch Hook create an EBS volume from the Snapshot in another AZ with an new Instance.

* **Multi Region** 

  * DynamoDB Global Tables (multi-way replication, active-active, enabled by Streams)
  * AWS Config Aggregators (multi region and multi account)
  * RDS Cross Region Read Replicas (used for Read and DR)
  * Aurora Global Database (one region is master, other is for Read and DR. Only 2 regions)
  * EBS volumes snapshots, AMI, RDS snapshots can be copied to other regions
  * VPC peering to allow private traffic between regions
  * Route53 uses a global network of DNS servers
  * S3 Cross Region Replication
  * CloudFront for Global CDN at the Edge Locations
  * Lambda@Edge for Global Lambda function at Edge Locations (A/B testing)

  **Multi Region with Route 53** we can deploy an application with instances, one alb in one region, and the same in another region, and using Route53 records (latency, geoproximity, etc) balancing or switching in these regions. The Route53 records points to Health Checks and this permit automated DNS failovers, 3 health check types:

  * monitor and endpoint (application, server, other AWS resources)
  * monitor other health checks (calculated health checks)
  * monitor CloudWatch Alamars (full control) e.g. throttles DynamoDB, custom metrics, etc. Health Checks are integrated with CW metrics.

* **Multi Region CloudFormation StackSets** this is useful to deploy application on multi-region or multi-account. Is very useful for make a **Global Deployments**

  * With StackSets we can create, update, or delete stacks across multiple accounts and regions with a single operation.
  * We need an Administrator account to create StackSets. 
  * Trusted accounts to create, update, delete stacks instances from StackSets. 
  * When you update a stack set, all associated stack instances are updated throughout all accounts and regions. 
  * Ability to set a maximum concurrent actions on targets (# or %). 
  * Ability to set failure tolerance (# or %)

  For this we need to execute an Cloudformation Template provided for AWS to create the IAM administrator account. After that we need to execute another CloudFormation provides for AWS to create the Target Account Iam Role StackSet. 

  Now we need to execute an example Cloudformation called `Enable AWS Config` this is necessary to track the resources on the accounts. We can deploy this in accounts or organizational units, we need to specified the ID accounts and the regions. Finally we can deploy our CloudFormation Stack in all accounts and regions,
  
* **Multi Region CodePipeline** we can use Codepipeline to perform Multi-Region Deployments. The Codepipeline run in one region and triggers on each others region the CodeDeploy service. The CodePipeline Artifact need also replicate in each region. Also we need to configure the EC2 key on each region. To create the EC2 instances and an CodeDeploy application, we need to launch a CloudFormation template in each region.



#### AWS Organization

Is an Global Service, allows to manage multiple AWS accounts. The main account is the master account (we can't change it). Other accounts are member accounts. Members accounts can only be part of one organization. We get Consolidated Billing across all accounts (single payment method). We get with this pricing benefits from aggregated usage (volume discount for EC2, S3). API is available to automate AWS account creation.

**Organize accounts in Organizational Unit OU:** Can be anything: dev / test or HR / IT. Apply **Service Control Policies (SCPs) to OU, permit or Deny access to AWS services**. Helpful for sandbox account creation, or to separate dev and prod resources, or to only allow/deny services

**Multi Account Services Integration** 

	* Any cross account action requires to define IAM "trust"
 * IAM roles can be assumed cross account
   	* no need to share IAM creds
      	* uses AWS Security Token Service (STS)
* CodePipeline - Cross account invocation of CodeDeploy for example
* AWS Config - Aggregators
  * CloudWatch Events - Event Bus = multi accounts events
  * CloudFormation - Stackset

Common use case: Cloudwatch Logs and centralize logs Multi Account and Multi Region. https://aws.amazon.com/blogs/architecture/stream-amazon-cloudwatch-logs-to-a-centralized-account-for-audit-and-analysis/



#### Disaster Recovery

Any event that has a negative impact on a company's business continuity or finances is a disaster. Disaster Recovery (DR) is about preparing for and recovering from a disaster. 
Kind of DR?

* On-premise -> On-premise (Traditional DR and very expensive)
* On-premise -> AWS Cloud (Hybrid Recovery)
* AWS Cloud Region A -> AWS Cloud Region B

Two important terms:

* **RPO:** Recovery Point Objective. Is how often we run backups. When a Disaster happen the time between the RPO and the Disaster is the Data Loss. Is how match of data loss you accept to loss in case of a disaster happens.
* **RTO:** Recovery Time Objective. Is the Downtime time when you recover from the Disaster. Is the time of downtime you accept.

**Disaster Recovery Strategies:** All have different RTO (Better RTO cost more money)

* **Backup And Restore** High RTO/RPO. Is the cheapest option. We don't manage infrastructure in the middle, would just recreate infrastructure when we need it. 
* **Pilot Light** a small version of the app is always running in the cloud. Useful for the critical core. Similar to Backup and Restore, but faster because critical systems are already up
* **Warm Standby** full system is up and running, but a minimum size. Upon disaster, we can scale to production load.
* **Hot Site / Multi Site Approach** very low RTO (minutes or seconds). Very expensive. Full Production Scale in running AWS and On Premise. Cost a lot of money
* **AWS Multi Region** we can duplicate the resources in the regions, and manage the failover with Route53, and maybe we can use Aurora Global (master/slave) in two regions. 

**Disaster Recovery Tips**

* Backup
  * EBS Snapshots, RDS automated backups / Snapshots, etc.
  * Regular pushes to S3 / S3 IA / Glacier, Lifecycle Policy, Cross Region Replication
  * From On-Premise: Snowball or Storage Gateway
* High Availability
  * Use Route53 to migrate DNS over from Region to Region
  * RDS Multi-AZ, ElastiCache Multi-AZ, EFS, S3
  * Site to Site VPN as a recovery from Direct Connect
* Replication
  * RDS Replication (Cross Region), AWS Aurora + Global Databases
  * Database replication from on-premise to RDS
  * Storage Gateway
* Automation
  * CloudFormation / Elastic Beanstalk to re-create a whole new environment
  * Recover / Reboot EC2 instances with CloudWatch if alarms fail
  * Lambda functions for customized automations
* Chaos
  * Netflix has a "Simian-army" randomly terminating EC2.

**Multi-Region Disaster Recovery Checklist**

* Is my AMI Copied? Is it stored in the parameter store?
* Is my CloudFormation StackSet working and tested to work in another region?
* What's my RPO and RTO?
* Are Route53 Health Checks working correctly? Tied to a CW Alarm?
* How can I automate with CloudWatch Events to trigger some lambda functions and perform a RDS RR promotion? 
* Is my data backed up? RPO and RTO? EBS, AMI, RS, S3 CRR, Global DybamoDB Tables, RDS and Aurora Global RR.

**Backups & Multi-Region DR**

* EFS Backup
  * AWS Backup with EFS (frequency, when, retain, time, lifecycle policy) - managed
  * EFS to EFS backup solution
  * Multi-region idea: EFS -> S3 -> SR CRR -> EFS
* Route 53 Backup
  * Use ListResourceRecordSets API for exports
  * Write your own script for imports into R53 or other DNS provider
* Elastic Beanstalk Backup
  * Saved configuration using the `eb cli` or aws console



#### On-Premise strategy with AWS

* Ability to download Amazon Linux 2 Ami as a VM (.iso format)
  * VMWare, KVM, VirtualBox (Oracle VM), Hyper-V
* VM Import / Export
  * Migrate existing applications into EC2
  * Create a DR repository strategy for your on-premise VMs
  * Can export back the VMs from EC2 to on-premise
* AWS Application Discovery Service
  * Gather information about your on-premise server to plan a migration
  * Server utilization and dependency mappings
  * Track with AWS Migration Hub
* AWS Database Migration Service (DMS)
  * replicate On-premise -> AWS, AWS -> AWS, AWS -> On-premise
  * Works with various database technologies (Oracle, MySQL, DynamoDB, etc)
* AWS Server Migration Service (SMS)
  * Incremental replication on-premise live servers to AWS





**Exercises:**

* Create an ASG with and Scaling Policy based on CPU, and stress the server for the scale out
* Activate DynamoDB Global Table, after that use DynamoDB Streams with lambda and save in a log the new table data.



**Reference Links**

**Auto Scaling**

- https://gist.github.com/mikepfeiffer/d27f5c478bef92e8aff4241154b77e54
- https://aws.amazon.com/about-aws/whats-new/2018/05/application-load-balancer-announces-slow-start-support/
- http://docs.aws.amazon.com/autoscaling/latest/userguide/as-suspend-resume-processes.html
- https://docs.aws.amazon.com/autoscaling/ec2/userguide/lifecycle-hooks.html
- https://github.com/aws-samples/aws-lambda-lifecycle-hooks-function
- https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-instance-termination.html
- https://aws.amazon.com/about-aws/whats-new/2015/12/protect-instances-from-termination-by-auto-scaling/
- https://aws.amazon.com/blogs/devops/use-a-creationpolicy-to-wait-for-on-instance-configurations/
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-attribute-updatepolicy.html
- https://aws.amazon.com/premiumsupport/knowledge-center/auto-scaling-group-rolling-updates/
- https://docs.aws.amazon.com/codedeploy/latest/userguide/tutorials-auto-scaling-group-create-deployment.html
- https://docs.aws.amazon.com/codedeploy/latest/userguide/integrations-aws-auto-scaling.html
- https://d1.awsstatic.com/whitepapers/AWS_Blue_Green_Deployments.pdf



**DynamoDB**

- https://aws.amazon.com/blogs/database/choosing-the-right-dynamodb-partition-key/
- https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/AppendixSampleTables.html#AppendixSampleData.Thread
- https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Streams.html#Streams.Processing
- https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/time-to-live-ttl-how-to.html
- https://aws.amazon.com/blogs/big-data/building-and-maintaining-an-amazon-s3-metadata-index-without-servers/



**Multi-AZ**

- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSEncryption.html



**Multi-Region**

- https://docs.aws.amazon.com/config/latest/developerguide/aggregate-data.html
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/stacksets-getting-started.html
- https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/stacksets-prereqs.html
- https://docs.aws.amazon.com/codepipeline/latest/userguide/actions-create-cross-region.html
- https://aws.amazon.com/blogs/devops/using-aws-codepipeline-to-perform-multi-region-deployments/



**Disaster Recovery**

- https://docs.aws.amazon.com/efs/latest/ug/efs-backup-solutions.html
- https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/hosted-zones-migrating.html
- https://stackoverflow.com/questions/20337749/exporting-dns-zonefile-from-amazon-route-53
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environment-configuration-methods-before.html#configuration-options-before-savedconfig
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environment-configuration-savedconfig.html



**On-Premise**

- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/amazon-linux-2-virtual-machine.html
- https://aws.amazon.com/application-discovery/



**Multi-Account**

- https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CrossAccountSubscriptions.html
- https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/CreateDestination.html
- https://aws.amazon.com/premiumsupport/knowledge-center/streaming-cloudwatch-logs/
- https://aws.amazon.com/blogs/architecture/stream-amazon-cloudwatch-logs-to-a-centralized-account-for-audit-and-analysis/
- https://aws.amazon.com/blogs/architecture/stream-amazon-cloudwatch-logs-to-a-centralized-account-for-audit-and-analysis/
- https://aws.amazon.com/premiumsupport/knowledge-center/streaming-cloudwatch-logs/
- https://aws.amazon.com/about-aws/whats-new/2017/06/cloudwatch-events-adds-cross-account-event-delivery-support/
- https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/CloudWatchEvents-CrossAccountEventDelivery.html



Aurora multi-master has its data replicated across multiple Availability Zones only but not to another AWS Region. You should use Amazon Aurora Global Database instead.