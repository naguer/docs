# SAP-C01 Stephane Maarek Course Notes



### 3. Identify & Federation

* Federation with SAML
* Federation without SAML with a custom IdP (GetFederation Token)
* Federation with SSO for multiple accounts with AWS Organizations
* Web identity Federation (not recommended)
* Cognito for most web and mobile applications (has anonymous mode, MFA)



* SSO 
  
  * AD on AWS:
    * Microsoft AD: standalone or setup trust AD with on-premise, has MFA, seamless join, RDS integration
    * AD Connector: proxy requests to on-premise
    * Simple AD: standalone & cheap AD-compatible with no MFA, no advanced capabilities
  * Single Sign On is used to connect to multiple AWS Accounts (Organization) and SAML apps
  
  

* IAM
  * IAM Policies Anatomy: Json with `Effect`, `Action`, `Resource`, `Conditions`, `Policy Variables`. Explicit DENY has precedence over ALLOW
  * IAM Policies Variables and Tags:
    * AWS Specific: `aws:CurrentTime`, `aws:TokenIssueTime`, `aws:principaltype`, `aws:SecureTranport`
    * Service Specific: `s3:prefix`, `s3:max-keys`, `s3:x-amz-acl`
    * Tag Based: `iam:ResourceTag/key-name`, `aws:PrincipalTag/key-name`
  * IAM Roles vs Resource Based Policies (ex: s3 bucket policy)
    * Attach a policy to a resource (example:s3 bucket policy) vs attaching of a using a role as a proxy
    * When you assume a role (user, application or service), you give up your original permissions and take the permissions assigned to the role
    * When using a resource based policy, the principal doesn't have to give up any permissions
    * Resource Based policies are supported by S3 buckets, SNS topics, SQS queues (don't give up any permissions)
      
* **RAM (Resource Access Manager)** is useful for share VPCs between accounts 



* **STS is very important to Assume a Role**
  * Steps to use
    * Define an IAM Role within your account or cross-account
    * Define which principals can access this IAM Role
    * Use AWS STS to retrieve credentials and impersonate the IAM Role you have access to (AssumeRole API)
    * Temporary credentials can be valid between 15 min to 1 hour
  * Ability to revoke active sessions and credentials for a role (by adding a policy using a time statement - AWSRevokeOlderSessions)
  * When you assume a role, you give up your original permissions
  * **Providing Access to AWS Accounts Owned by Third Parties**
    * We need the 3rd party AWS account ID, and the 3rd party need to chose an External ID (secret between you and the 3rd party), this key must be provided when defining the trust and when assuming the role.
  * **REVIEW THE CONFUSED DEPUTY IN THE COURSE**



### 4. Security

* Cloudtrail
  * Is enabled by default
  * Can put logs from Cloudtrail into CloudWatch logs
  * If a resource is deleted in AWS look into Cloudtrail first
  * CloudTrail console shows the past 90 days
  * The Default UI only shows "Create", "Modify" or "Delete" events
  * Multi Account & Multi Region Logging: We need use CloudTrail from the accounts, and pointing to an another security account with the S3 Bucket, and configure the bucket with a Bucket policy to permit the access from CloudTrail
  * Alert for Api Calls: Cloudtrail -> Stream -> CW Logs with Metric Filters and a CW alarm on top (EX: Api call for terminate instance)
  * CT may take up to 15 min to deliver events
  * CT can deliver to CloudWatch Events (Most reactive way), CloudWatch Logs (We can use with Metric filter) and S3 (Events are delivered every 5 min, possibility analyzing logs integrity deliver cross account, long-term storage)
* KMS, to give access to someone we need to make sure the Key Policy allows the user, and the IAM policy allows the API calls.
* 



### 5. Compute & Load Balancing

- Lambda

  - Limits
    - Ram 128 MB to 3G
    - CPU is linked to RAM, 2 vCPU are allocated after 1.5G of ram
    - /tmp: 512 MB
    - Deployment package limit: 250mb (with layers)
    - Concurrency execution: 1000
  - Lambda in vpc need use an NAT and IGW for connect to a DynamoDB (or use VPC Endpoint)
  - 3 Types invocation
    - Asynchronous: S3, SNS, CloudWatch Events. Make sure the processing is **idempotent**. Lambda attemps to retry on error 3 times. Can define a DLQ (SNS/SQS) for failed processing
    - Synchronous: CLI, SDK, Api Gateway. Result is returned right away. Error handling must happen client side
    - Event Source Mapping: Kinesis Data Streams, SQS, DynamoDB Streams. Common denominator: **records need to be polled from the source**. If your function returns an error, the entire batch is reprocessed until success.
  - Lambda Destinations: Useful to send results to a destination, for Asynchronous can defined destination for successful and failed event (SQS/SNS/Lambda/EventBridge Bus).
    For Event Source mapping is only for discarded event batches (SQS/SNS)
  - Aliases: are pointers to lambda function versions, for example "dev", "test", and that point at different lambda versions, are mutable. Aliases enable Blue / Green. Have their own ARNs
  - AWS Lambda Aliases with API Gateway: API prod stage -> prod Alias 95% -> V1 (5% -> V2)
    No is necessary change the API Gateway for change the version, you only point the Lambda Alias to the new version
  - Lambda & CodeDeploy: CodeDeploy can help you automate traffic shift for Lambda Aliases, using "Linear" grow traffic every N minutes until %100, or "Canary" try X percent, then %100, or "AllAtOnce". Can create Pre & Post Traffic hooks to check the health of the Lambda function. 

  

- ELB

  - CLB supports only one SSL cert, the SSL cert can have many SAN (Subject Alternate Name) but the SSL cert must be changed anytime a SAN is added/edited/removed, better to use ALB with SNI (Server Name Indication)
  - ALB Target Groups:
    - EC2 instances (can be managed by an ASG)
    - EC2 tasks (managed by ECS itself)
    - Lambda functions - HTTP request is translated into a JSON event
    - IP Addresses - Must be private IPs (ex: instances in peered VPC, on-premise)
    - ALB can route to multiple target groups
    - Health checks are at the target group level
  - NLB has one static IP per AZ, and supports assigning EIP (helpful for white listening specific IP). NLB support TLS, WebSockets, and is very important for extreme performance, TCP or UDP traffic. 
    Also very useful with AWS Private Link to expose a service internally. 
  - NLB Target Groups:
    - EC2 Instances (can be managed by an ASG) - TCP
    - ECS  tasks (managed by ECS itself) - TCP
    - IP addresses - Private IP only, even outside your VPC
  - Cross-Zone LB
    - CLB: Disabled by default, no charges for inter AZ data if enabled
    - **ALB: Always on** (can't be disabled), no charges for inter AZ data
    - NLB: Disabled by default, you pay charges ($) for inter AZ data if enabled

### 9. Service Communication FINISH

* SNS + SQS: Fan Out.  the idea is that you're going to push one message in SNS, and receive in many SQS queues. This is more safe than to send your message individually, and you can have different services (such as Fraud Service, Shipping Service) and this services consume different SQS Queue with the same SNS Topic as source. 
* SWF service only is necessary when you are using Amazon Mechanical Turk for other services you need to use Step Functions. SWF is not serverless run on Ec2, 1 year max runtime. 
* SQS Max Message Size of 256 KB (use a pointer to S3 for large messages)
* SQS Could be used as a write buffer for DynamoDB
* IF you use SQS your need to implement IDEMPOTENCY at the consumer level (messages can be processed twice by consumer in case of failures timeouts, etc). This means the same action done twice by the consumer won't duplicate the effect. 
* Step Function build serverless visual Workflow to orchestrate your Lambda functions, represent flow as a Json state machine.



### 11. Monitoring FINISH

Need to review all section

- CloudWatch Metrics
  - Provides by many AWS services
  - EC2 standard: 5 minutes, detailed monitoring: 1 minute
  - EC2 RAM is not a built-in metric
  - Can create custom metrics: standard resolution 1 minute, high resolution 1 sec
- CloudWatch Alarms
  - Can trigger actions: EC2 action (reboot, stop, terminate, recover), Auto Scaling, SNS
  - Alarm events can be intercepted by CloudWatch Events
- CloudWatch Dashboards
  - Display metrics and alarms
  - Can show metrics of multiple regions
- CloudWatch Events
  - Intercepts events from AWS services (Ec2 instance start, CodeBuild Failure)
  - Can intercept any API call with CloudTrail integration
  - Notable targets:
    - Compute: Lambda, Batch, ECS stack
    - Orchestration: Step Function, CodePipeline, CodeBuild
    - Integracion: SQS, SNS, Kinesis Data Streams, Kinesis Data Firehose
    - Maintenance: SSM, EC2 Actions
- CloudWatch Logs Sources
  - SDK, CloudWatch Logs Agent, CloudWatch Unified Agent
  - EB collection of logs from application
  - ECS collection from containers
  - Lambda collection from function logs
  - VPC Flow Logs: VPC specific logs
  - API Gateway
  - CloudTrail based on filter
  - CloudWatch log agents: For example on EC2 machines
  - Route 53 : Log DNS queries
- **CloudWatch Logs S3 Export** must be encrypted with AES-256 (SSE-S3), not SSE-KMS (Log data can take up to 12 hours to become available for export). The API call is `CreateExportTask`. 
  Not near-real time or real-time (use Logs Subscriptions instead)
- CloudWatch Logs Subscriptions: CloudWatch Logs -> Subrscription Filter -> Lambda function -> Amazon ES
- CloudWatch Logs Aggregation Multi-Account & Multi Region:
  All CloudWatch Logs in the different regions/account send the logs to the `Subcription filter` this services send the information to a centralize Kinesis Data streams -> Kinesis Data Forehose -> S3 (Near Real Time)



### 12. Deployment and Instance Management FINISH

* CodeDeploy Deployment Options:
  * In-place on EC2
  * In-place on ASG
  * New instances on ASG
  * Traffic shifting for AWS Lambda (We can configure pre and post traffic hooks)
  * New task set for ECS + traffic shifting (A new task set is created, and traffic is re-routed to the new task test, then if everything is stable for X minutes, the old task set is terminated)
    

* Opsworks can manage ELB and EC2 instances, cannot manage an ASG
  
* SAM if it deploys a Lambda function internally it's going to use CodeDeploy to perform the traffic shifting
  
* Service Catalog is like as a set of CNF templates that your users can use based on their IAM permissions
  
* AWS::RDS:DBCluster resources on CNF the default DeletePolicy is Snapshot

* When you deploy a CNF stack you can use your own IAM permission, or assign an IAM role

* If you create IAM resources using CNF you need to explicity provide a "capability" to CNF `CAPABILITY_IAM` and `CAPABILITY_NAMED_IAM`

* CloudFormation can create Custom Resources using Lambda

  * Emptying an S3 bucket before being deleted
  * An On-Premise Resource
  * An new AWS resource is not yet supported
  * Fetch an AMI id
  * Anything you want.

* CNF Cross Stacks is useful when you need to pass export values to many stacks (VPC id, etc) use Output Export and Fn:ImportValue. 

* CNF Nested Stacks is helpful when components must be re-used (like a module), EX: re-use how to properly configure an ALB. The nested stack only is important to the higher level stack (it's not shared)

* CloudFormer is for create an AWS CNF template from existing AWS resources

* CNF ChangeSets generate & preview the CNF changes before they get applied

* CNF StackSets deploy a CNF stack across multiple accounts and regions

* CNF Stack Policies is for prevent accidental updates / deletes to stack resources (EX: a Stack Policies for never change the RDS database)
  
* SSM Patch Manager Predefined Pach Baselines: Defines which patches should or shouldn't be installed on your instances

  * Linux:
    * AWS-AmazonLinux2DefaultPatchBaseline
    * AWS-CentOSDefaultPatchBaseline
    * AWS-RedHatDefaultPatchBaseline
    * AWS-SuseDefaultPatchBaseline
    * AWS-UbuntuDefaultPatchBaseline
  * Windows: (patches are auto-approved 7 days after the release)
    * AWS-DefaultPatchBaseline: install OS patch CriticalUpdates & SecurityUpdates
    * AWS-WindowsPredefinedPatchBaseline-OS: same as "AWS-DefaultPatchBaseline"
    * AWS-WindowsPredefinedPatchBaseline-OS-Applications: also updates Microsoft Applications
  * Can define your own custom patch baselines as well (OS, classification, severity...)

* **SSM Patch Managers Steps**

  * Define a `patch baseline` to use (or multiple if you have multiple environments)
  * Define patch groups: define based on tags, example different environments (dev, test, prod) - use Tag `Patch Group`
  * Define `Maintenance Windows` (schedule, duration, registered targets/patch groups and registered tasks)
  * Add the `AWS-RunPatchBaseline Run` command as part of the registered tasks of the Maintenance Windows (works cross platform Linux & Windows)
  * Define `Rate Control` (concurrency & error threshold) for the task
  * Monitor `Patch Compliance` using `SSM Inventory`

  

### 13. Cost Control FINISH

- AWS Cost Allocation Tags is useful for enable detailed costing reports they show up as columns in reports. This appear in the Billing Console, takes up to 24 hras for the tags to show up in the report.

  Two types:

   * AWS Generated Cost Allocation Tags (aws:)
   * User Tags (user:)
     

- Trusted Advisor is similar to CloudConformity tool, can enable weekly email notification. 
  With Full TA (Business & Enterprise Support plan) you have the ability to set CW alarms when reaching limits, and programmatic Access using AWS Api. Is good if you need check if an S3 bucket is made public (but cannot check for s3 object that are public inside of your bucket, for this use CW Events/S3 Events) and also is useful for monitoring Service Limits

- **AWS Savings Plan** new pricing model to get a discount based on long-term usage, commit to a certain type of usage: ex $10 per hour for 1 to 3 years. Now we can commit to X money not to instance.

