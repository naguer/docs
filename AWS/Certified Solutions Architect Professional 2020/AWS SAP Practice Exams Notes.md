# AWS Certified Solution Architect Professional Practice Exams



## Linux Academy

* In AWS Organization the master account cannot be impacted by SCPs, so that account should be avoided for normal usage
  
* To integrate CW Logs to stream data to an AWS ES, the first step is create an ES Domain
  
* In AWS Organizations if we want to shared the Reserved Instances, we need to enable on all member accounts, and purchase Reserved Instances in the master account and enable also the option
  
* ElastiCache can be use with RDS, NOSQL as DynamoDB and MongoDB, or with no database tier at all, which is common for distributed computing applications
  
* AWS Direct Connect applies inbound (to your on-premises data center) and outbound (from your AWS Region) routing policies for a public AWS Direct Connect connection. You can also use Border Gateway Protocol (BGP) community tags on advertised Amazon routes and apply BGP community tags on the routes you advertise to Amazon.
  https://docs.aws.amazon.com/directconnect/latest/UserGuide/routing-and-bgp.html
  
* If you need to make a new social media platform on AWS, The default for any social media-style database should be Neptune, which is a managed graph database that can support dynamic relationships between data which are as important as the data itself. Timestream is ideal for the time-based GPS data from the collars (time and location), and S3 can be used for any larger binary objects such as images/videos.
  
* GP2 performance is closely linked with volume size.
  IO1, which can scale IOPS independently to volume size, 
  IO1 is expensive and designed for when IOPS needs to scale separately than volume size.
  
* MODIFY: Another CGW at the customer side will protect against customer hardware failure. Each of the CGWs will need two tunnels: one to each AZ of the VGW. So bring up the second tunnel on the existing connection, and add another dual-tunnel VPN connection to the new second CGW.
  ![1594425072366](/home/naguer/.config/Typora/typora-user-images/1594425072366.png)

* In route53 Alias queries are free if attached to certain products, and can be from domain.com and www.domain.com
  
* No encryption occurs at the physical level across a Direct Connect connection or its VIFs. Communications across the public VIF are encrypted at the application level because they are using public AWS endpoints.
  
* If we have 3 accounts (dev, test and prod) and the administrators from the prod account need to create resources in the other accounts, we need to create cross-account roles in dev and test. 
  
* You can schedule maintenance at a less impact time period. Each AZ will have a different maintenance window to reduce the impact of a maintenance event.
  
* ![1594427107240](/home/naguer/.config/Typora/typora-user-images/1594427107240.png)

____

* To restring a bucket and permit only CloudFront, we need:
  * Configure a bucket policy and set OAI.
  * Add trusted signers to a CF behavior.
  * https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-restricting-access-to-s3.html





## Jon Bonso

#### First test

Result: Failed | 68% correct (51/75). 2 hours 27 minutes.


* If you need scalable storage, you should use S3 instead EBS volumes
  

* When you run `AWS-RunPatchBaseline`, you can target managed instances using their instance ID or tags. SSM Agent and Patch Manager will then evaluate which patch baseline to use based on the patch group value that you added to the instance.

  You create a patch group by using Amazon EC2 tags. Unlike other tagging scenarios across Systems Manager, a patch group *must* be defined with the tag key: **Patch Group**. Note that the key is case-sensitive. You can specify any value, for example, "web servers," but the key must be **Patch Group**.

  The `AWS-DefaultPatchBaseline` baseline is primarily used to approve all Windows Server operating system patches that are classified as "CriticalUpdates" or "SecurityUpdates" and that have an MSRC severity of "Critical" or "Important". Patches are auto-approved seven days after release.
  

*  Global Tables replicates your Amazon DynamoDB tables automatically across your choice of AWS regions. Any changes made in one of the replica tables will be automatically replicated across all other tables.
  The following is a conceptual overview of how a global table is created.

  1. Create an ordinary DynamoDB table
  2. Choose the **Global Tables** tab, and then choose **Enable streams**.
  3. Choose **Add region**, and choose **Global table 2017.11.29 mode**. You can now choose another Region 
     

* By default, an SCP named `FullAWSAccess` is attached to every root, OU, and account. This default SCP allows all actions and all services. So in a new organization, until you start creating or manipulating the SCPs, all of your existing IAM permissions continue to operate as they did. As soon as you apply a new or modified SCP to a root or OU that contains an account, the permissions that your users have in that account become filtered by the SCP. Permissions that used to work might now be denied if they're not allowed by the SCP at every level of the hierarchy down to the specified account.
  

* **SCPs DO NOT affect any service-linked role. Service-linked roles enable other AWS services to integrate with AWS Organizations and can't be restricted by SCPs.**


* By default, the data in a Redis node on ElastiCache resides only in memory and is not persistent. If a node is rebooted, or if the underlying physical server experiences a hardware failure, the data in the cache is lost.

  If you require data durability, you can **enable the Redis append-only file feature (AOF)**. When this feature is enabled, the node writes all of the commands that change cache data to an append-only file. When a node is rebooted and the cache engine starts, the AOF is "replayed"; the result is a warm Redis cache with all of the data intact.

  AOF is disabled by default. To enable AOF for a cluster running Redis, you must create a parameter group with the `appendonly` parameter set to yes, and then assign that parameter group to your cluster. You can also modify the `appendfsync` parameter to control how often Redis writes to the AOF file.

  
* You can configure various Docker networking modes that will be used by containers in your ECS task. The valid values are `none`, `bridge`, `awsvpc`, and `host`. The default Docker network mode is `bridge`.

  * If the network mode is set to `**none**`, the task's containers do not have external connectivity and port mappings can't be specified in the container definition.

  * If the network mode is `**bridge**`, the task utilizes Docker's built-in virtual network which runs inside each container instance.

  * If the network mode is `**host**`, the task bypasses Docker's built-in virtual network and maps container ports directly to the EC2 instance's network interface directly. In this mode, you can't run multiple instantiations of the same task on a single container instance when port mappings are used.

  * If the network mode is `**awsvpc**`, the task is allocated an elastic network interface, and you must specify a `NetworkConfiguration` when you create a service or run a task with the task definition. When you use this network mode in your task definitions, every task that is launched from that task definition gets its own elastic network interface (ENI) and a primary private IP address. The task networking feature simplifies container networking and gives you more control over how containerized applications communicate with each other and other services within your VPCs.
    

* AWS Server Migration Service (SMS) is an agentless service which makes it easier and faster for you to migrate thousands of on-premises workloads to AWS. AWS SMS allows you to automate, schedule, and track incremental replications of live server volumes, making it easier for you to coordinate large-scale server migrations.
  **AWS Server Migration Service is a significant enhancement of the EC2 VM Import/Export service.** The AWS Server Migration Service provides automated, live incremental server replication and AWS Console support, unlike the VM Import/Export service.

* To connect to AWS public endpoints such as an Amazon Elastic Compute Cloud (Amazon EC2) or Amazon Simple Storage Service (Amazon S3) with dedicated network performance, use a public virtual interface.
  
* AWS Direct Connect provides two types of virtual interfaces: public and private. 

  * **Public Virtual Interface**

    To connect to AWS public endpoints, such as an Amazon Elastic Compute Cloud (Amazon EC2) or Amazon Simple Storage Service (Amazon S3), with dedicated network performance, use a public virtual interface.

    A public virtual interface allows you to connect to all AWS public IP spaces globally. Direct Connect customers in any Direct Connect location can create public virtual interfaces to receive Amazon’s global IP routes, and they can access publicly routable Amazon services in any AWS Regions (except the AWS China Region).

  * **Private Virtual Interface**

    To connect to private services, such as an Amazon Virtual Private Cloud (Amazon VPC), with dedicated network performance, use a private virtual interface.

    A private virtual interface allows you to connect to your VPC resources (for example, EC2 instances, load balancers, RDS DB instances, etc.) on your private IP address or endpoint. A private virtual interface can connect to a Direct Connect gateway, which can be associated with one or more virtual private gateways in any AWS Regions (except the AWS China Region). A virtual private gateway is associated with a single VPC, so you can connect to multiple VPCs in any AWS Regions (except the AWS China Region) using a private virtual interface. For a private virtual interface, AWS only advertises the entire VPC CIDR over the Border Gateway Protocol (BGP) neighbor.

    ![1594516440364](/home/naguer/.config/Typora/typora-user-images/1594516440364.png)

* AWS Resource Access Manager (AWS RAM) enables you to share specified AWS resources that you own with other AWS accounts. To enable trusted access with AWS Organizations:

  From the AWS RAM CLI, use the `enable-sharing-with-aws-organizations` command.

  Name of the IAM service-linked role that can be created in accounts when trusted access is enabled: *AWSResourceAccessManagerServiceRolePolicy*.
  

* AWS CloudHSM is a cloud-based hardware security module (HSM) that enables you to easily generate and use your own encryption keys on the AWS Cloud. If we need CloudHSM highly available we need to deploy on least 2 AZ.
  

* Using AWS Direct Connect, you can establish private connectivity between AWS and your datacenter, office, or colocation environment, which in many cases can reduce your network costs, increase bandwidth throughput, and provide a more consistent network experience than Internet-based connections.

  Using industry standard 802.1q VLANs, this dedicated connection can be partitioned into multiple virtual interfaces. Virtual interfaces can be reconfigured at any time to meet your changing needs. You can use an **AWS Direct Connect gateway** to connect your AWS Direct Connect connection over a private virtual interface to one or more VPCs in your account that are located in the same or different Regions. You associate a Direct Connect gateway with the virtual private gateway for the VPC. Then, create a private virtual interface for your AWS Direct Connect connection to the Direct Connect gateway. You can attach multiple private virtual interfaces to your Direct Connect gateway.

  With Direct Connect Gateway, you no longer need to establish multiple BGP sessions for each VPC; this reduces your administrative workload as well as the load on your network devices.
  

* You can only use the same SSL certificate from ACM in more than one AWS Region if you are attaching it to your CloudFront distribution only, and not to your Application Load Balancer. To use a certificate with Elastic Load Balancing for the same site (the same fully qualified domain name, or FQDN, or set of FQDNs) in a different Region, you must request a new certificate for each Region in which you plan to use it.
  

* The member account of a Organization doesn't have the capability to turn off RI (reserved instance) sharing on their account. For make this, you need to turn off in the master account for member accounts (you can select the accounts to disable the RI discounts)
  

* A VPC peering connection is a networking connection between two VPCs that enables you to route traffic between them privately. Instances in either VPC can communicate with each other as if they are within the same network. You can create a VPC peering connection between your own VPCs, with a VPC in another AWS account, or with a VPC in a different AWS Region.
  

* AWS Shield is better than AWS WAF for DDOS attacks
  

* RDS does not support RMAN (recovery manager) backup utility (Oracle)
  

* If you don't use Amazon Cognito, then you choose to write a custom code or app that interacts with a web IdP (Login with Amazon, Facebook, Google, or any other OIDC-compatible IdP) and then call the `AssumeRoleWithWebIdentity` API to trade the authentication token you get from those IdPs for AWS temporary security credentials. 
  

* https://tutorialsdojo.com/aws-cheat-sheet-aws-storage-gateway
  

* CloudFront Signed URL provides a way to distribute private content but it doesn't encrypt the sensitive credit card information, if we want something like that we need to use HTTPS and field-level encryption on CloudFront.

  Field-level encryption adds an additional layer of security along with HTTPS that lets you protect specific data throughout system processing so that only certain applications can see it. Field-level encryption allows you to securely upload user-submitted sensitive information to your web servers. The sensitive information provided by your clients is encrypted at the edge closer to the user and remains encrypted throughout your entire application stack, ensuring that only applications that need the data—and have the credentials to decrypt it—are able to do so.
  

* Amazon Route 53 supports DNSSEC for domain registration. However, Route 53 does not support DNSSEC for DNS service, regardless of whether the domain is registered with Route 53. If you want to configure DNSSEC for a domain that is registered with Route 53, you must either use another DNS service provider or set up your own DNS server.
  

* If you are migrating an AMI to another region, you can choose the *"Proceed without a* keypair*"* option when you are about to launch a new EC2 instance using the migrated AMI, to use your original PEM key (the public key)



____

#### Second test

Result: Passed | 82% correct (62/75). 1 hours 55 minutes.



* Amazon EC2 now allows peering relationships to be established between Virtual Private Clouds (VPCs) across different AWS regions. **Inter-Region VPC Peering** allows VPC resources like EC2 instances, RDS databases, and Lambda functions running in different AWS regions to communicate with each other using private IP addresses, without requiring gateways, VPN connections or separate network appliances. Share resources between regions.
  
* ECS use **Service Auto Scaling** (NO Auto Scaling Group)
  
* **NACL** control inbound and outbound traffic for your **subnets** (Not VPC)

* If we have files in a S3 bucket, and we configure the versioning, the Version ID for theses files is `null`, the next file version of each file will be the `1 version`
  
* **RDS Multi-AZ** when automatic failover occurs, your application can remain unaware of what's happening behind the scenes. The CNAME record for your DB instance will be altered to point to the newly promoted standby.

* A **stateless** application is an application that does not need knowledge of previous interactions and does not store session information. For example, an application that, given the same input, provides the same response to any end user, is a stateless application.
  
* **A pre-signed URL** gives you access to the object identified in the URL, provided that the creator of the pre-signed URL has permissions to access that object (used for creator, mean to upload new objects). **Are only available in S3**
  
**A signed URL** includes additional information, for example, an expiration date and time, that gives you more control over access to your content (about accessing existing objects)
  **Are only available in CloudFront**
  
* **in-place vs disposable deployments:** in-place upgrade involves performing application updates on live Amazon EC2 instances. A disposable upgrade, on the other hand, involves rolling out a new set of EC2 instances by terminating older instances.

* **Oracle RAC**  is only possible on EC2, not on RDS

* **Gateway Stored Volume** Stored **by locally** as primary data storage. Support volumes of 512TV
  **Gateway Cached Volume** Stored **by Amazon S3** as primary data storage. Support volumes of 1,024TB
  (the frequently accessed data is stored on the on-premises server while the entire data is backed up over AWS)

* **Storage Gateway can uses CHAP to authenticate iSCSI this provides protection against playback attacks by requiring authentication.**
  
* ADD S3 ENCRYPT TYPES
  
* OLTP = RDS, OLAP = Redshift
  
* In OpsWorks, you will be provisioning a stack and layers. The stack is the top-level AWS OpsWorks Stacks entity. It represents a set of instances that you want to manage collectively, typically because they have a common purpose such as serving PHP applications. In addition to serving as a container, a stack handles tasks that apply to the group of instances as a whole, such as managing applications and cookbooks.

  Every stack contains one or more layers, each of which represents a stack component, such as a load balancer or a set of application servers. As you work with AWS OpsWorks Stacks layers, keep the following in mind:

  Each layer in a stack must have at least one instance and can optionally have multiple instances.

  Each instance in a stack must be a member of at least one layer, except for [registered instances](https://docs.aws.amazon.com/opsworks/latest/userguide/registered-instances.html). You cannot configure an instance directly, except for some basic settings such as the SSH key and hostname. You must create and configure an appropriate layer, and add the instance to the layer.

  For example, if we need two group of servers in one stack, we need two layers
  
  ![1594589611173](/home/naguer/.config/Typora/typora-user-images/1594589611173.png)

* Server-side encryption with Amazon S3-managed encryption keys (SSE-S3) use strong multi-factor encryption. Amazon S3 encrypts each object with a unique key. As an additional safeguard, it encrypts the key itself with a master key that it rotates regularly.

* If you have a VPC peered with multiple VPCs that have overlapping or matching CIDR blocks, ensure that your route tables are configured to avoid sending response traffic from your VPC to the incorrect VPC. 

* To restrict access by using CloudFront

  - Require that your users access your private content by using special CloudFront signed URLs or signed cookies (signed cookies feature is primarily used if you want to provide access to multiple restricted files)

    - Use **signed URLs** for the following cases:

      You want to use an RTMP distribution. Signed cookies aren't supported for RTMP distributions.

      You want to restrict access to individual files, for example, an installation download for your application.

      Your users are using a client (for example, a custom HTTP client) that doesn't support cookies.

    - Use **signed cookies** for the following cases:

      You want to provide access to multiple restricted files, for example, all of the files for a video in HLS format or all of the files in the subscribers' area of a website.

      You don't want to change your current URLs.

  - Require that your users access your Amazon S3 content by using CloudFront URLs, not Amazon S3 URLs. Requiring CloudFront URLs isn't necessary, but it is recommended to prevent users from bypassing the restrictions that you specify in signed URLs or signed cookies.

  https://tutorialsdojo.com/aws-cheat-sheet-s3-pre-signed-urls-vs-cloudfront-signed-urls-vs-origin-access-identity-oai/

* If you have multiple VPN connections, you can provide secure communication between sites using the **AWS VPN CloudHub**. This enables your remote sites to communicate with each other, and not just with the VPC. The VPN CloudHub operates on a simple hub-and-spoke model that you can use with or without a VPC. This design is suitable for customers with multiple branch offices and existing internet connections who'd like to implement a convenient, potentially low-cost hub-and-spoke model for primary or backup connectivity between these remote offices.

* Service control policy (SCP) simply determines what services and actions can be delegated by administrators to the users and roles in the accounts that the SCP is applied to. It does not grant any permissions, unlike an IAM Policy.



____

#### Third test

Result: Passed | 81% correct (61/75). 2 hours 7 minutes.

* A network device that supports Border Gateway Protocol (BGP) and BGP MD5 authentication is needed to establish a Direct Connect link from your data center to your VPC.
  https://tutorialsdojo.com/aws-cheat-sheet-aws-direct-connect/

* By using cached volumes, you can use Amazon S3 as your primary data storage, while retaining frequently accessed data locally in your storage gateway. Cached volumes minimize the need to scale your on-premises storage infrastructure, while still providing your applications with low-latency access to frequently accessed data. You can create storage volumes up to 32 TiB in size and afterwards, attach these volumes as iSCSI devices to your on-premises application servers. When you write to these volumes, your gateway stores the data in Amazon S3. (Low latency access)

  https://tutorialsdojo.com/aws-cheat-sheet-aws-storage-gateway/
  

* A listener is a process that checks for connection requests. It is configured with a protocol and a port for front-end (client to load balancer) connections, and a protocol and a port for back-end (load balancer to back-end instance) connections.

  Elastic Load Balancing supports the following protocols: HTTP, HTTPS (secure HTTP), TCP and SSL (secure TCP).
  

* Amazon supports Internet Protocol security (IPsec) VPN connections. IPsec is a protocol suite for securing Internet Protocol (IP) communications by authenticating and encrypting each IP packet of a data stream. Data transferred between your VPC and datacenter routes over an encrypted VPN connection to help maintain the confidentiality and integrity of data in transit. An Internet gateway is not required to establish a hardware VPN connection.
  

* Amazon AppStream 2.0 is a fully managed application streaming service. You centrally manage your desktop applications on AppStream 2.0 and securely deliver them to any computer. 
  

* In CloudFront, there are 3 options that you can choose as the value for your Origin Protocol Policy: HTTP Only, HTTPS Only and Match Viewer.
  if the Origin Protocol Policy is set to Match Viewer, the CloudFront communicates with the origin using HTTP or HTTPS depending on the protocol of the viewer request.

* SNI Custom SSL relies on the SNI extension of the Transport Layer Security protocol, which allows multiple domains to serve SSL traffic over the same IP address by including the hostname which the viewers are trying to connect to.
  You can host multiple TLS secured applications, each with its own TLS certificate, behind a single load balancer. In order to use SNI, all you need to do is bind multiple certificates to the same secure listener on your load balancer.

* SCPs are similar to IAM permission policies except that they don't grant any permissions. Instead, SCPs specify the maximum permissions for an organization, organizational unit (OU), or account. When you attach an SCP to your organization root or an OU, the SCP limits permissions for entities in member accounts. Even if a user is granted full administrator permissions with an IAM permission policy, any access that is not explicitly allowed or that is explicitly denied by the SCPs affecting that account is blocked.
  By default, an SCP named **FullAWSAccess** is attached to every root, OU, and account. This default SCP allows all actions and all services. So in a new organization, until you start creating or manipulating the SCPs, all of your existing IAM permissions continue to operate as they did. As soon as you apply a new or modified SCP to a root or OU that contains an account, the permissions that your users have in that account become filtered by the SCP. Permissions that used to work might now be denied if they're not allowed by the SCP at every level of the hierarchy down to the specified account.

  By removing the default FullAWSAccess SCP, all actions for all services are now implicitly denied. To use SCPs as a whitelist, you must replace the AWS-managed **FullAWSAccess** SCP with an SCP that explicitly permits only those services and actions that you want to allow.
  

* Remember that only one virtual private gateway (VGW) can be attached to a VPC at a time,
  

* You cannot use the LDAP credentials to log into IAM.
  

* **Amazon DynamoDB** is integrated with **AWS Lambda** so that you can create *triggers*—pieces of code that automatically respond to events in DynamoDB Streams.
  If you enable DynamoDB Streams on a table, you can associate the stream ARN with a Lambda function that you write. Immediately after an item in the table is modified, a new record appears in the table's stream. AWS Lambda polls the stream and invokes your Lambda function synchronously when it detects new stream records.

* You can setting up AWS generated tags by activating it in the Billing and Cost Management console of the master account (NOT IN A MEMBER ACCOUNT)



____

#### Four test

Result: Fail | 70% correct (53/75). 2 hours 24 minutes.

* Raid 1 = Mirror, Raid 0 = Performance
  

* https://tutorialsdojo.com/aws-cheat-sheet-amazon-ebs/
  

* Elastic Load Balancing creates a cookie, named `AWSELB`, that is used to map the session to the instance, this is useful for use sticky. 
  

* You can integrate Amazon Mechanical Turk and SWF to implement a set of workflows for batch processing.
  

* **Amazon Redshift workload management (WLM)** enables users to flexibly manage priorities within workloads so that short, fast-running queries won't get stuck in queues behind long-running queries.

* We can store custom SSL certificate in ACM or IAM. 
  

* In RDS For production application that requires fast and consistent I/O performance, **AWS recommends Provisioned IOPS (input/output operations per second) storage**
  

* For update instances on Opsworks AWS recommend:

  * Create and start new instances to replace your current online instances. Then delete the current instances. The new instances will have the latest set of security patches installed during setup
  * Linux-based instances in Chef 11.10 or older stacks, run the Update Dependencies stack command, which installs the current set of security patches and other updates on the specified instances.
    

* It is recommended by AWS to use the Server Migration Service (SMS) to migrate VMs from a vCenter environment to AWS. SMS automates the migration process by replicating on-premises VMs incrementally and converting them to Amazon machine images (AMIs). You can continue using your on-premises VMs while migration is in progress. The Server Migration Connector is a FreeBSD VM that you install in your on-premises virtualization environment
  

* When automated snapshots are enabled for a cluster, Amazon Redshift periodically takes snapshots of that cluster, usually every eight hours or following every 5 GB per node of data changes, or whichever comes first. Automated snapshots are enabled by default when you create a cluster.

  When you enable Amazon Redshift to automatically copy snapshots to another region, you specify the destination region where you want snapshots to be copied. In the case of automated snapshots, you can also specify the retention period that they should be kept in the destination region. 
  

* Amazon WorkDocs is a fully managed, secure content creation, storage, and collaboration service, like google docs
  

* If your VPC does not have sufficient ENIs or subnet IPs, your Lambda function will not scale as requests increase, and you will see an increase in invocation errors with EC2 error types like `EC2ThrottledException`
  

*  You can choose the following options to improve the data durability of your ElastiCache cluster:

  \- Daily automatic backups
  \- Manual backups using Redis append-only file (AOF)
  \- Setting up a Multi-AZ with Automatic Failover

* For reference an IAM role in CNF to an application we need to use `InstanceProfileName`
  The instance profile contains the role and can provide the role's temporary credentials to an application that runs on the instance.

* If you got your certificate from a third-party CA, import the certificate into ACM or upload it to the IAM certificate store (Not to cloudfront)
  ACM lets you import third-party certificates from the ACM console, as well as programmatically. If ACM is not available in your region, use AWS CLI to upload your third-party certificate to the IAM certificate store.
  **CloudFront** is incorrect because although you can upload certificates to CloudFront, it doesn't mean that you can import SSL certificates on it. You would not be able to export the certificate that you have loaded in CloudFront nor assign them to your EC2 or ELB instances as it would be tied to a single Cloudfront distribution.

  Cloudfront certificates

  * Default: If we don't need to use a specified domain name
  * Custom: If we need to use a specified domain name

____

## Important

* **Inspector** is only for EC2 instances (need Agent), analyze the running OS against know vulnerabilities or against unintended network accessibility. No own custom rules, only use AWS managed rules. After the assesment you get a report with a list of vulnerabilites. Inspector can't analyze directly AMI

* **AWS Config** helps with auditing and recording **compliance** of AWS resources, helps record configuration and changes over time.
  AWS Config Rules does not prevent actions from happening (no deny). 

  Is a per-region service, can be aggregated across regions and accounts. 

  Integrated with SNS for any changes.
  Questions for Config?

  * Unrestricted SSH to SG?
  * Buckets with any public access?
  * ALB configuration changed over time?

* **AWS Config Rules**

  * Can use AWS managed config rules (over 75)
  * Can make custom config rules (using Lambda)

  Config Rules can trigger CW Events if the rules is non-compliant (And chain with Lambda)
  **Rules can have auto remediations, using SSM Automations**. Not Lambda directly, but SSM Automation can invoke Lambda
  **For alerting need CloudWatch Event, not directly SNS.** 

* **GuardDuty** Intelligent Threat discovery to Protect AWS Account. Use ML for anomaly detection. Can setup CW Event rules to be notified in case of findings, and target to Lambda or SNS
  Input data includes:
  * CloudTrail Logs: unusual Api Call, unauthorized deployments
  * VPC Flow Logs: unusual internal traffic, unusual IP address
  * DNS Logs: compromised Ec2 instances sending encoded data within DNS queries
  
* **AWS Secret Manager** is better than Parameter Store only if you need rotation or a deep integration with RDS.

* **Amazon Macie** is a security service that uses machine learning to automatically discover, classify, and protect sensitive data in AWS. Automatically generates an inventory of S3 buckets and discover sensitive data such a personally identifiable information (PII), and sends the information to CW Events for integration into workflows and remedation actions

* First is how does the unified CloudWatch agent publish
  system-level metrics? These are published as CloudWatch metrics that can be used directly for alarms
  like any other metric,CloudWatch Events is a different feature of CloudWatch that fires events upon
  system events or on a schedule

* **AWS Service Catalog** allows Administrators to publish products and grant IAM users privileges to launch the products without granting those users the ability to launch the underlying services. To launch a CloudFormation stack, the user needs privileges to launch all the underlying infrastructure in the stack.

* **Multi-AZ** is Synchronous replication

* **Amazon Connect** is an easy to use omnichannel cloud contact center that helps companies provide superior customer service at a lower cost.

* **VM Import/Export** enables you to import virtual machine (VM) images from your existing virtualization environment to Amazon EC2, and then export them back. Also we can generate AMIs

* Amazon Route 53 supports DNSSEC for domain registration. However, **Route 53 does not support DNSSEC for DNS service**, regardless of whether the domain is registered with Route 53. If you want to configure DNSSEC for a domain that is registered with Route 53, you must either use another DNS service provider or set up your own DNS server.

* **Capture website Clickstream data** = Kineses

* AWS Organizations has two available feature sets:

  - [All features](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_getting-started_concepts.html#feature-set-all) – This feature set is the preferred way to work with AWS Organizations, and it includes consolidating billing features. When you create an organization, enabling all features is the default. With all features enabled, you can use the advanced account management features available in AWS Organizations such as [service control policies (SCPs)](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies_type-auth.html#orgs_manage_policies_scp) and [tag policies](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_manage_policies_tag-policies.html).
  - [Consolidated billing features](https://docs.aws.amazon.com/organizations/latest/userguide/orgs_getting-started_concepts.html#feature-set-cb-only) – All organizations support this subset of features, which provides basic management tools that you can use to centrally manage the accounts in your organization.

* When you enable users to access the AWS Management Console with a long session duration time (such as 12 hours), their temporary credentials do not expire as quickly. If users inadvertently expose their credentials to an unauthorized third party, that party has access for the duration of the session. However, you can immediately revoke all permissions to the role's credentials issued before a certain point in time if you need to. For doing this, you need to go to IAM console, choose Roles and "Revoke active sessions", IAM immediately attaches a policy named `AWSRevokeOlderSessions` to the role. The policy denies all access to users who assumed the role before the moment you chose **Revoke active sessions**. Any user who assumes the role **after** you chose **Revoke active sessions** is **not** affected.

  When you apply a new policy to a user or a resource, it may take a few minutes for policy updates to take effect.

* https://tutorialsdojo.com/aws-cheat-sheet-service-control-policies-scp-vs-iam-policies/

* ElastiCache for Memcached = Multithreaded 

* Sticky Session cookie name `AWSELB`

**1. Rehost (“lift and shift”)** - In a large legacy migration scenario where the organization is looking to quickly implement its migration and scale to meet a business case, we find that the majority of applications are rehosted. Most rehosting can be automated with tools such as AWS SMS although you may prefer to do this manually as you learn how to apply your legacy systems to the cloud.

You may also find that applications are easier to re-architect once they are already running in the cloud. This happens partly because your organization will have developed better skills to do so and partly because the hard part - migrating the application, data, and traffic - has already been accomplished.

**2. Replatform (“lift, tinker and shift”)** -This entails making a few cloud optimizations in order to achieve some tangible benefit without changing the core architecture of the application. For example, you may be looking to reduce the amount of time you spend managing database instances by migrating to a managed relational database service such as Amazon Relational Database Service (RDS), or migrating your application to a fully managed platform like AWS Elastic Beanstalk.

**3. Repurchase (“drop and shop”)** - This is a decision to move to a different product and likely means your organization is willing to change the existing licensing model you have been using. For workloads that can easily be upgraded to newer versions, this strategy might allow a feature set upgrade and smoother implementation.

**4. Refactor / Re-architect** - Typically, this is driven by a strong business need to add features, scale, or performance that would otherwise be difficult to achieve in the application’s existing environment. If your organization is looking to boost agility or improve business continuity by moving to a service-oriented architecture (SOA) this strategy may be worth pursuing - even though it is often the most expensive solution.

**5. Retire** - Identifying IT assets that are no longer useful and can be turned off will help boost your business case and direct your attention towards maintaining the resources that are widely used.

**6. Retain** -You may want to retain portions of your IT portfolio because there are some applications that you are not ready to migrate and feel more comfortable keeping them on-premises, or you are not ready to prioritize an application that was recently upgraded and then make changes to it again.

![1595718509750](/home/naguer/.config/Typora/typora-user-images/1595718509750.png)

* **Recovery time objective (RTO)** - The time it takes after a disruption to restore a business process to its service level, as defined by the operational level agreement (OLA). For example, if a disaster occurs at 12:00 PM (noon) and the RTO is eight hours, the DR process should restore the business process to the acceptable service level by 8:00 PM.

* **Recovery point objective (RPO)** - The acceptable amount of data loss measured in time. For example, if a disaster occurs at 12:00 PM (noon) and the RPO is one hour, the system should recover all data that was in the system before 11:00 AM. Data loss will span only one hour, between 11:00 AM and 12:00 PM (noon).

* Direct Connect from one Datacenter to two regions. 

  You can use an AWS Direct Connect gateway to connect your AWS Direct Connect connection over a private virtual interface to one or more VPCs in your account that are located in the same or different regions. 
  You associate a Direct Connect gateway with the virtual private gateway for the VPC, and then create a private virtual interface for your AWS Direct Connect connection to the Direct Connect gateway. You can attach multiple private virtual interfaces to your Direct Connect gateway. A Direct Connect gateway is a globally available resource. You can create the Direct Connect gateway in any public region and access it from all other public regions.

  ![1595810559743](/home/naguer/.config/Typora/typora-user-images/1595810559743.png)

____

* Secure Sockets Layer (SSL) VPN** is an emerging technology that provides remote-access VPN capability, using the SSL function that is already built into a modern web browser. SSL VPN allows users from any Internet-enabled location to launch a web browser to establish remote-access VPN connections, thus promising productivity enhancements and improved availability, as well as further IT cost reduction for VPN client software and support.

  Always keep in mind to install the SSL VPN software in your EC2 instances which are deployed in the public subnet. This will be where your users can connect to the Internet to be able to access the business applications, which is deployed in the private subnet of your VPC.



* Inter-Region VPC Peering provides a simple and cost-effective way to share resources between regions or replicate data for geographic redundancy.
* NAT gateways are not supported for IPv6 traffic—use an outbound-only (egress-only) internet gateway instead.

* [AWS Database Migration Service](https://aws.amazon.com/dms/) (DMS) and the [AWS Schema Conversion Tool](https://aws.amazon.com/dms/#sct) (SCT) now allow you to convert and migrate IBM Db2 databases on Linux, UNIX and Windows (Db2 LUW) to any [DMS supported target](https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Target.html). 
* https://tutorialsdojo.com/amazon-fsx/
  ![1596128723217](/home/naguer/.config/Typora/typora-user-images/1596128723217.png)

* For some AWS services, you can grant cross-account access to your resources. To do this, you attach a policy directly to the resource that you want to share, instead of using a role as a proxy. The resource that you want to share must support resource-based policies. Unlike a user-based policy, a resource-based policy specifies who (in the form of a list of AWS account ID numbers) can access that resource.

  Cross-account access with a resource-based policy has some advantages over a role. With a resource that is accessed through a resource-based policy, the user still works in the trusted account and does not have to give up his or her user permissions in place of the role permissions. In other words, the user continues to have access to resources in the trusted account at the same time as he or she has access to the resource in the trusting account. This is useful for tasks such as copying information to or from the shared resource in the other account. FILTRAR

* `**enableDnsHostnames** - `Indicates whether the instances launched in the VPC get public DNS hostnames. If this attribute is `true`, instances in the VPC get public DNS hostnames, but only if the `enableDnsSupport` attribute is also set to `true`.

  `**enableDnsSupport** - `Indicates whether the DNS resolution is supported for the VPC. If this attribute is `false`, the Amazon-provided DNS server in the VPC that resolves public DNS hostnames to IP addresses is not enabled. If this attribute is `true`, queries to the Amazon provided DNS server at the 169.254.169.253 IP address, or the reserved IP address at the base of the VPC IPv4 network range plus two ( *.*.*.2 ) will succeed.

* AWS Firewall Manager is a security management tool that makes it easier for you to configure your AWS WAF rules across your accounts. With Firewall Manager, security administrators of large organizations can write company-wide rules from one place, enforce them across applications protected by [AWS WAF](https://aws.amazon.com/waf/), and get the central visibility of attacks against your Application Load Balancers and Amazon CloudFront infrastructure.

* if only Spot instances are used for data processing then the average performance could be affected as Spot instances may be interrupted any time by AWS. Although this set up is the most cost-effective, it affects the system performance which is not acceptable. It would be better to use a combination of Spot and Reserved Instances instead.

* You can use the same SSL certificate from ACM in more than one AWS Region but it depends on whether you’re using Elastic Load Balancing or Amazon CloudFront. To use a certificate with Elastic Load Balancing for the same site (the same fully qualified domain name, or FQDN, or set of FQDNs) in a different Region, you must request a new certificate for each Region in which you plan to use it. To use an ACM certificate with Amazon CloudFront, you must request the certificate in the US East (N. Virginia) region. ACM certificates in this region that are associated with a CloudFront distribution are distributed to all the geographic locations configured for that distribution.

* Volume Storage Gateway restore to an EBS volume, is impossible to restore directly to an EC2

## Experience from exams

#### One

As far as the types of questions I had see below:

Mechanical Turk - Know which services it supports SQS,SNS,SWF

Transit Gateways - multiple questions

Direct Connect - lots of these on cost efficiency, performance and HA with VPN. DirectConnect Gateway

AWS Organizations - SCPs, member accounts, root account not affected by SCPs, invite member accounts

IAM - understand when and how roles are used

Snowball,Snowball Edge, Snowmobile - 50-80 TB vs 100 TB and usable space

DynamoDB - Global Tables, Lambda, DAX

API Gateway - Lambda, timeouts

Autoscaling Groups - EC2 vs. ELB

Cloudfront - Lambda@Edge, OAI

Route53 - AAAA records vs. CNAME vs. Alias records

RDS - Aurora and Aurora Serverless, MySQL and PostGreSQL

ElasticSearch

ElastiCache - know the differences in memcached and Redis

MultiAZ deployments

Tons of HA,fault tolerant, cost effective and easiest solution to implement

Systems Manager - Patch Manager, Run Command, Maintenance Windows

S3 - ACLs, bucket policies, signed urls, durable storage

ElasticBeanstalk - All at Once, Rolling with batch, immutable etc.

OpsWorks - stacks

EFS vs. GP2 and cost effectiveness

FsX and FsX for Lustre - enterprise workloads vs. high performance workloads

Migration from Oracle to RDS

Server Migration Service

VPC Peering(verbatim) - https://docs.aws.amazon.com/vpc/latest/peering/peering-configurations-partial-access.html#one-to-two-vpcs-simple-hub

Justin Davies Direct Connect Deep Dive https://www.youtube.com/watch?v=DXFooR95BYc



_________

#### Two

So I just passed my AWS SA Pro (SAP) Certification yesterday (no score yet), but boy was it tough. For everyone who will also be taking it soon, here are some tips:
- Know the difference of Service Catalog and Cloudformation. Like for enforcement of services and tags and such across different AWS accounts
- Know how you can use AWS Organizations, Cloudformation StackSets and IAM/SCP to manage what infrastructure resources your team can launch
- Know how to do blue/green deployments in Elastic Beanstalk
- Know how to create a CI/CD pipeline using code* tool and integrate this with ECR/S3
- Know how you can speed up DynamoDB performance (do you use auto scaling? Or put an elasticache in front? Or use DAX?)
- **Migrations were a HUGE part of my exam. Know when to rehost or replatform or even re-architect a legacy infrastructure with a database. Brush up on your math because some scenarios will ask what is the best migration scheme (S3 acceleration with fast internet speed? Use existing DX line but with slower speed? Use snowball even though the size of the data is a bit low). You'll either choose your best answer depending on cost or urgency of the task.**
- **the longest prefix for vpc peering question came up**
- **When connecting multiple VPCs to a central VPC using a dedicated DX line, know when you have to create VIFs or create a DX gateway or create a transit gateway. These question really made it difficult for me since I have little experience with transit gateway and DX. Automatically rule out public VIF choices though.**
- Know how to combine Cloudfront and load balancers to rereoute traffic to different origins. (tip: it's via the headers i think)
- Does NAT Gateway support IPv6? (thankfully I knew this) Nat gateway vs Nat instance vs egress only internet gateway for your private subnet?
- how to mitigate ddos and application layer attacks (cloudfront + waf, NLB +waf, autoscaling with waf, cloudwatch with shield, combinations like that)
- weirdly enough, i dont recall any questions about authentication using ldap or sso or sts
- for migrating small applications, would you rather use SMS or VM import or just reinstall it in beanstalk (supports the app's prog language) as a cost efficient measure?
- some question regarding IBM db2 that you're supposed to migrate cost efficiently, but I cant exactly remember the details
- connect, lex and alexa question also came up
- this one was tough. So you have a desktop application + some other stuff, and you need to boost its performance. its a multi answer question, and you had to pick between a service that doesnt really boost performance + workspaces vs cloudfront + appstream
- workdocs also came up
- how do you properly migrate a local data analytics server to redshift? This question involved so many steps and I wasnt sure on the answer since I didnt have experience on this
- if you have a small RTO window, like 5 minutes but an RPO of one hour, what is your best but most cost efdective DR plan? Do you go pilot light or warm standby or multi AZ?
- **know what is the difference between ecs task execution role and ecs task role**
- know how to resolve lambda timeout errors in a severless stack. Increase the memory, increase the timeout, etc
- this one was a bit tricky. If you have an app that runs scripts on a dataset and it runs for X mins on a day's worth of data, how do you optimize the setup so that the script instead runs as data comes in. Compare EC2 auto scaling, lambda, and ECS containers.
- how do you quickly test your newly deployed app and quickly revert to the old version if errors are encountered? (Hint: CI/CD)
- i also had questions that included fargate, but only for general use cases like using it for container management and the likes
- **know how yo be cost effective with redshift master node and task nodes. Yes you will use reserved and spot fleet combination, but it's more of deciding HOW and WHEN to do it**
- there was a question on using cloud development kit. Other option I could remember is aws amplify. 



____

#### Three

"Some of topics from exam that I can remember:

- EC2Rescue tool with System status checks
- Company own orchestration tools to manage more than 100 ec2 which all are not updated and have security vulnerabilities. They needed to be patched and do so regularly and record changes of configurations. Options will be related to SSM Patch and maintenance window and AWS Config. (Tip: You need to know what functions does each components in SSM does)
- Patching EC2 running Windows and need to patch in two different time so to avoid downtime.(Tip: Patch baselines and maintenance windows... from SSM)
- Using backup connection line for DirectConnect. Answers will be between VPN or second directConnect, you need to see if question says cheap(VPN) or company requires sensitive latency and highly available.
- **Related to connect 2 regions with total 5 VPC to company data center. (Tip: Check if required cheap model and answer were between using directconnect gateway or VPN to each VPCs or something similar)**
- **There was a 500mbps DX bought from partner network and has separate 1gbps internet line always available. Some ~20 TB files needed to transfer and which will be best way? Options had Storage Gateway on DX, S3 upload, S3 transfer acceleration, Snowball... (Tip: In your mind calculate how much each bandwidth can transfer in a day before picking snowball as for some questions transferring large data with Snowball is always not good option).**
- One team has access to other OrganizationUnit using lambda with hardcoded IAM credentials and plaintext DB password in lambda. Need to make it secure.(Tip: Check options for Secrets or SSM Parameters, roles or assume role).
- Million request per minute to consume and store data less than 4kb each. There were no options for Kenisis but where answers each on DynamoDB, RDS, S3 and with using SQS or so on.
- **Lambda didn't worked after moving to VPC to connect third party but ec2 was working fine in that VPC. (Tip: check subnet of lambda has route to NAT gateway)**
- **There was question about Network load balancer infront of EC2 and was related to proxy protocol**
- Using tags to manage resource like ec2 for business units, so in same account different unit cant control EC2 of other unit.(This is discussed in Re-invent 2018 Become an IAM Policy Master in 60 Minutes or Less (SEC316-R1)
- **Two VPC having same subnet peering with central VPC(third) and need to connect to DB with an IP in one of them. (Tip: Use longest prefix to match DB ip in third VPC)**
- There is Route53 pointed to Autoscaling. Autoscaling uses product catalogue page as health-check and keeps removing EC2 as there is bottleneck in backend. How to fix this situation (Tip: Use Route53 health to check product catalogue page and ELB to check /health with simple html page)
- Migrate 100Vms app server to AWS but they have to talk back to application on-premises. There is also desktop app to connects to servers which user complains of experiencing high latencies. (Tip: Was something related to Use DX from AWS to on-premises and AppStream for user instead of desktop app.)
- Connecting 2 separate region with one datacenter
- Reserved instance buying scenario with demand(Mixed options with spot, on-demand or buy up to max Autoscale value)
- **Ecs task is not consuming from SQS and DynamoDB is not populated with results. Logs complains that can't access SQS. What have been changes. (Tip: Know about ECS different levels of roles for tasks)**
- One billion weather forecast generated every 15minutes and consumed by almost 5 to 10 million users. Which storage and delivery services to use cost effectively. (Tip: options will be different combination of EFS, S3, RDS, Cloudfront, API Gateway, Lambda, EC2... Eliminate wrong first and focus to pick best one).
- A company wants to setup roles for users where they can only create limited resource only from Cloudformation. After the resource is created with Cloudformation users can use the resource. (Tip: there was no option of service catalog. There were confusing answers that template will also create new permission for dev to access or something like that)
- Using Rekognition to process video and save metadata and infos on DB and something related store videos around S3
- There will be some question related to SSO, DR with normal RDS or Aurora or Redshift, backing up your infrastructure in other region for DA...."
  

_________



## Direct Connect



![1595839398900](/home/naguer/.config/Typora/typora-user-images/1595839398900.png)

https://docs.aws.amazon.com/directconnect/latest/UserGuide/Welcome.html



##### Direct Connect Virtual Interfaces (VIF)

* Public VIF: enables access to public services, such as S3, Ec2, Glacier, etc (green color in the image). 
* Private VIF: A private virtual interface enables access to your VPC (EC2 instances, ALB, etc)
* Transit Virtual Interface: To connect to resources in a VPC using a Transit Gateway

​	Note: VpC endpoints cannot be acceded through Private VIF (we don't need them because we can do that using a Public VIF)



##### Direct Connect Connection Types

* Dedicated Connections: 1 Gbps and 10 Gbps capacity

  * Physical ethernet port dedicated to a customer
  * Request made to AWS first, then completed by AWS Direct Connect Partners

* Hosted Connections: 50 Mbps, 500 Mbps, to 10 Gbps

  * Connection requests are made via AWS Direct Connect Partners
  * Capacity can be added or removed on demand
  * 1, 2, 5, 10 Gbps availble at select AWS Direct Connect Partners

  Note: Lead times are often longer than 1 month to establish a new connection



##### Direct Connect Encryption

* Data in transit is not encrypted but is private
* AWS Direct Connect + VPN provides an IPsec-encrypted private connection
* Good for an extra level of security, but slightly more complex to put in place



##### Direct Connect Link Aggregation Groups (LAG)

* Get increased speed and failover by summing up existing Direct Connect connections into a logical one (allowing you to treat them as a single, managed connection.)

* Can aggregate up to 4 (active active mode, all active)

* Can add connections over time to the LAG

* All the connections in the LAG must have the same bandwidth

* All connections in the LAG must terminate at the same AWS Direct Connect Endpoint

* Can set a minimum number of connections for the LAG to function


  In the following diagram, you have four connections, with two connections to each location. You can create a LAG for the connections that terminate in the same location, and then use the two LAGs instead of the four connections for configuration and management.

  ![1595840353720](/home/naguer/.config/Typora/typora-user-images/1595840353720.png)

  https://docs.aws.amazon.com/directconnect/latest/UserGuide/lags.html

##### Direct Connect Gateways

Useful if we want to setup a DX to one or more VPC in different regions (same account, cross acount)

Uses a Private VIF because we need to connect to the vpc resources. 

![1595840659694](/home/naguer/.config/Typora/typora-user-images/1595840659694.png)



https://docs.aws.amazon.com/directconnect/latest/UserGuide/direct-connect-gateways-intro.html



## Transit VPC

* Uses a specified Software VPN
* Not an AWS offering, newer managed solution is Transit Gateway
* Uses the public internet with a software VPN solution
* Allows for transitive connectivity between VPC & locations
* Software VPN running on EC2, problem with HA, you need to maintain, problem with bandwidth 

![1596079851171](/home/naguer/.config/Typora/typora-user-images/1596079851171.png)



## Transit Gateway

* For having transitive peering between thousands of VPC and on-premise, hub-and-spoke (star) connection
* Regional resource, can work cross-region
* Share cross-account using RAM (TG is one of the principal reasons for use RAM)
* You can peer Transit Gateway across regions
* Route Tables: limit which VPC can talk with other VPC
* Work with DX Gateway, VPN connections
* Supports **IP Multicast** (Not supported by any other AWS service)
* Instances in a VPC can access a NAT Gateway, NLB, and EFS in others VPC attached to the AWS TG

![1596080042450](/home/naguer/.config/Typora/typora-user-images/1596080042450.png)

## Private Link (VPC Endpoint Services)

* Most secure & Scalable way to expose a service to 1000s of vpc (own or other accounts)

* Does not require VPC peering, IGW, NAT, Routes tables

* Is different to VPC peering because if we use VPC peering, we connect ALL services/applications in the two VPCs

* Requires a NLB (service VPC) and ENI (customer VPC)

* If the NLB is in multiple AZ, and the ENI in multiple AZ the solution is fault tolerant

  ![1596080833462](/home/naguer/.config/Typora/typora-user-images/1596080833462.png)