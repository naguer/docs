AWS Certified Solutions Architect Professional – Practice (SAP-C01)

Overall Score: 75%

Topic Level Scoring:
1.0  Design for Organizational Complexity: 0%
2.0  Design for New Solutions: 60%
3.0  Migration Planning: 100%
4.0  Cost Control: 50%
5.0  Continuous Improvement for Existing Solutions: 100%

