# Exam Readiness: AWS Certified Solutions Architect – Professional



### Complex organizations should design for three areas:



#### Sub-domain: cross-account authentication and access strategies



* User Federation
  * External user identities have permission to use AWS resources in your account. 
  * Your AWS account is more secure, because long-term security credentials are not distributed
  
* AWS Directory Service: AWS provides multiple ways to use MS AD with other AWS services.
  Active Directiory/LDAP applications:
  * AWS Managed MS AD: A Microsoft Active Directory is need in the AWS cloud
  * AD connector: On-premises users need to access AWS services via AD
  * Simple AD: Low-scale, low-cost basic AD capability (Does not support MFA)
    

* Types of policies
  * AWS managed
  * Customer managed
  * Inline
  
* Federated Users are users (or applications) who do not have AWS accounts.
  
* Roles for federated users: With roles, you can give federated users access to your AWS resources for a limited amount of time. This is useful if you have non-AWS users who you can authenticate with an external service, such as Microsoft Active Directory, LDAP, or Kerberos.
  
* Temporary AWS credentials: The temporary AWS credentials used with roles provide identity federation between AWS and non-AWS users in your corporate identity and authorization system. 



#### Sub-domain: networks

- Hybrid - VPN Connections

  - **AWS Managed VPN:** A hardware VPN connection from your on-premises network equipment on a remote network to AWS managed network equipment attached to your Amazon VPC
  - **AWS VPN CloudHub:** A hub-and-spoke model for connecting multiple remote branch offices to an Amazon VPC
  - **Software VPN:** A VPN connection from your equipment on a remote network to a user-managed software VPC appliance running inside your VPC

- Important to know how Direct connect (DX) work from "Customer router" on the Corporate data center, to the "Customer/Partner Cage" and AWS Cage on DX location, to the Virtual private gateway in your VPC.

  * DX Can be combined with AWS managed VPC to provide an IPsec-encrypted private connection.
  * DX supported private virtual interfaces.

  * Single DX connection can connect to two or more AWS Regions
    

- Hybrid - AWS Storage Gateway

  - **File Gateway:** Enables the storage and retrieval of objects in S3 and Glacier using file protocols such as NFS. Configure file shares that are mapped to selected S3 buckets using IAM roles.

  - **Tape Gateway:** Provides backup application with an iSCSI VTL interface consisting of a virtual media changer, virtual tape drives and virtual tapes:

    - Virtual tape data is stored in S3 or Glacier
    - Monitor the status of data transfer and storage interfaces using the AWS Management console
    - Additionally, use the API or SDK to manage and app interaction with the gateway

  - **Stored-Volume Gateway:** Data written to a stored volume gateway is saved on on-premises storage hardware and asynchronously backed up to S3 in the form of EBS snapshots. 
    Storage volumes can be created up to 16 TB in size and are mounted as iSCSI devices from on-premises application servers.

  - **Cached-Volume Gateway:** In S3 with only a cached of recently written and recently read data store locally on on-premises storage hardware.

    Point-in-time snapshots can be taken of volume data in S3 in the form of EBS snapshots, this provides space-efficient versioned copies of volumes for data protection and various data reuse needs. 
    To prepare for upload to S3, a gateway stores incoming data in a staging area called an upload buffer. 

- **VPC Endpoints:** Virtual devices that enable instances using private IPs to connect to services without an internet or virtual gateway. Manage access to VPC endpoints via IAM policies. 

  - **Interface endpoints:** An interface VPC endpoint is an elastic network interface with a private IP address that serves as an entry point for traffic destined to services powered by AWS PrivateLink.
  - **Gateway endpoints:** A gateway endpoint is a gateway that is a target for a specified route in your route table. This type of endpoint is used for traffic destined to a supported AWS service, such as Amazon S3 or Amazon DynamoDB.

  

#### **Sub-domain: multi-account AWS environments**

AWS Organizations:

* Manage policies across accounts and filter out unwanted access
* Automate creation of new accounts through APIs
* Organize accounts into Organizational Units (OU)
* Offers consolidated billing



### New Solutions – Part 1

This module focuses on:

- Implementation strategies for reliability requirements
- Ensuring business continuity
- Meeting performance objectives



Important topics to review: * RECOMMEND WATCHING AGAIN VIDEO *

* For tiers separation a good solution is using Amazon SQS, this give us:
  * Asynchronous tasks
  * Single direction only (ALB is multidirection)
  * Unordered (This is the standard, also possible fifo)
  * "At least once" delivery
* Amazon SNS:
  * Fan out to Amazon SQS
  * Asynchronous
  * Batch processing
* Kinesis Stream:
  * Asynchronous tasks
  * Single direction only
  * Ordered within a shard 
  * "At least once" semantics
  * Independent stream position
* Kineses Streams vs Kineses Firehose
  * Streams:
    * Custom processing per incoming record
    * Sub-1 second processing latency
    * Choice of stream processing frameworks
  * Firehose:
    * Zero administration
    * Processing latency of 60 seconds or higher
    * Ability to use existing analytics tools based on S3, Redshit and Amazon ES
* SQS for responses: We can use SQS for queues in each direction, need identifier for return messages.
  We need at least 2 queues.
* Data scaling using S3: Users -> CloudFront -> S3 and User -> Ec2 -> EBS
  * Put static content in S3
  * Randomize key names
  * Use appropriate storage classes
  * Larger object results in fewer Get and Put operations
* Data scaling using CloudFront: Users -> CloudFront -> S3 and User -> Ec2 -> EBS
  * Reduce traffic costs
  * Increase performance 
  * Origin can be from AWS or from on-premises
  * Access to S3 buckets can be restricted to Origin Access Identites
* Data scaling, Amazon EBS and instance stores: Users -> CloudFront -> S3 and User -> Ec2 -> EBS
  * EBS volume size can be increased while attached to an Ec2
  * EBS volume type and throughput can be changed while attached to an instance
  * EC2 instances have a maximum EBS throughput rate
  * Consider OS-based RAID sets
* Scaling and RDS, two options:
  * Increase instance size (power processor)
  * Increase storage
* Read Replicas:
  * Improves read performance only
  * Asynchronous
  * Unique/different endpoints
  * Cross-region
  * CloudWatch metric: ReplicaLag
* **ElastiCache: Redis** 
  * Advanced data structures
  * Persistent
  * Automatic failover with Multi-AZ deployments
  * Can scale using read replicas
  * Can scale up, but not out. Once scaled up, cannot scale down
  * Supports backup and restore operations
  * AOF (append only file) log can be enabled for recovery of nodes
* ElastiCache: Memcached
  * Simple key-value storage
  * Non-persistent, pure cache
  * Can scale both up and out
  * Scales out using multiple nodes
  * Does not support backup and restore operations
  * Supports multi-threaded operations
* Amazon ElastiCache, common requests:
  * Applications read from write to the cache
  * Create appropriate cache timeouts
  * Redis replication groups (read replicas)
* DynamoDB caching
  * Elasticache
  * DynamoDB Accelerator (DAX) Read-Through Cache
  * DynamoDB Accelerator (DAX) Write-Through Cache
* SQS And DynamoDB autoscaling, we can improve the autoscaling and minimize the costs usings a SQS before DynamoDB for the peaks moments, IF we application can handle the latency
* DynamoDB Global Tables, fully managed replication:
  * Globally distributed
  * Low latency reads/writes
  * Multi-region redundancy



### New Solutions – Part 2

This module focuses on two sub-domains:

- Security requirements and controls
- Deployment strategies for business requirements




Cognito: A fully managed solution providing access control and authentication for web/mobile apps.

* Supports MFA
* Data at-rest and in-transit encryption
* Log in via social identity providers (Facebook, Google, Amazon)
* Support to SAML




User Pools: 

* Provides a directory profile for all user which you can access through an SDK

* Supports user federation through a third-party identity provider

* Signed users receive authentication tokens

* Tokens can be exchanged for AWS access via Amazon Cognito identity pools

  **Note:** Is useful for users that need login using their mobiles and write a DynamoDB table using temporary credentials, and each login has its own directory profile (They exchange token generated by User Pools for credentials generated by Identity Pool)


Identity Pools:

* Authenticates users with web identity providers, including Amazon Cognito user pools
* Assigns temporary AWS credentials via AWS STS
* **Supports anonymous guest users (unsupport no anonymous users)**



Best Practices Cloudtrail

* Send all CloudTrail logs to S3
* Centralize collecting of logs to a dedicated account
* Enforce least-privilege principle
* Enforce MFA delete on the log S3 bucket
* Enable CloudTrail log integrity validation



### Migration Planning



**This module focuses on selecting:**

- Existing workloads and processes for potential migration to the cloud
- Migration tools or services for new and migrated solutions based on detailed AWS knowledge
- Strategies for migrating existing on-premises workloads to the cloud
- New cloud architectures for existing solutions



**Migration**

* Retain: Leave on-premise and revisit it in the future
* Re-host: Lift-and-shift
* Refactor: re-architect the infrastructure to be Cloud Native
* Re-platform: Lift, modify, and shift (minor changes)
* Replace: Buy a new cloud solution, such a SaaS service (Example Gitlab/Jira cloud)
* Retire: Delete the app, end of life, new solutions to replace the old one
  

**Application migration process**

**Plan** (About discovery, know about the app, dependencies, make a migration plan, estimate effort)  -> 
**Build** (Transform: Migrate, Deploy, Validate. Transition: Pilot testing, transtion to support, cutover and decommission) -> 
**Run** (Operate: Staff training, monitoring, incident management. Optimize: CI/CD, WAR, Monitoring driven optimization)

Tools to help migration:

* AWS Application Discovery Service (ADS)
* AWS Database Migration Service (DMS)

**Data migration**

Consider: 

* Downtime/orchestration

Methods: 

* Image backup/restore
* File copy
* Replication



### Cost Control

**This module focuses on:** 

- Cost-effective pricing models
- Designing and implementing controls to ensure cost optimization
- Opportunities to reduce cost in an existing solution



**Types of tags**

* Resource Tags
  * Provide the ability to organize and search within and across resources
  * Filterable and searchable
  * Do no appear in detailed billing report
* Cost Allocation Tags
  * Map AWS charges to organizational attributes for accounting purposes
  * Information presented in the detailed billing report and Cost Explorer
  * Only available on certain services or limited to components within a service (for example, s3 buckets but not objects)
    

**Best practices of cost management**

- Only allow specific groups or teams to deploy chosen AWS resources.
- Create policies for each environment.
- Require tags in order to instantiate resources.
- Monitor and send alerts or shut down instances that are improperly tagged.
- Use CloudWatch to send alerts when billing thresholds are met.
- Analyze spend using AWS or partner tools.



### Improving Architectures – Part 1

**This module focuses on:**

- Troubleshooting solution architectures
- Determining a strategy to improve an existing solution for operational excellence
- Determining a strategy to improve the reliability of an existing solution



**Troubleshooting on AWS**

* **S3 Access Logs:** Contain details about data requests, such as the request type, the resources requested, and the date and time. 
  Use when we need to troubleshoot bucket access issues and data request
* **ELB Access Logs:** Capture detailed information about each request send to your ELB, including IP, latencies and server responses.
  Use when we need to analyze traffic patterns and troubleshoot network issues
* **CloudTrail:** Provides a history of API calls
  Use when you need to audit and determine who did what, when and from. 
* **VPC Flow Logs:** Capture information about the IP traffic going into or out of your network interfaces and subnets
  Use when you need to verify network access rules are properly configured and troubleshoot connectivity and security issues
* **CW Logs:** Monitor, store and access app and systems using log data from EC2 instances and on-premises servers
  Use when you need to monitor and troubleshoot OS and apps running
* **AWS Config:** Provides an inventory of your AWS resource and records changes to the configuration those resources
  Use when you need to troubleshoot outages and conduct security attack analyses



### Improving Architectures – Part 1

**This module focuses on:**

- Performance 
- Security
- Deployment



**Performance**

https://docs.aws.amazon.com/AmazonElastiCache/latest/mem-ug/Strategies.html



**Security**

**SSL termination at the load balancer**

* Certificates are stored in IAM
* Single certificate per ELB
* Offload decryption work to the load balancer
* Re-encryption between ELB and instances
* Only in ALB and CLB

**SSL termination in Cloudfront**

* SNI or non-SNI certificates
* SSL connections to Load balancer

**AWS Certificate Manager**

* Manages and deploy public/privates certificates
* Establish website identity
* Verify identity of resources within a company



### **Additional resources**

As you continue to prepare for the AWS Certified Solutions Architect – Professional exam, it's important to have the right information to help you to continue learning. Below is a short list of helpful resources.

- [Whitepaper: AWS Security Best Practices](https://d0.awsstatic.com/whitepapers/Security/AWS_Security_Best_Practices.pdf?refid=em_)
- [Whitepaper: AWS Well-Architected Framework](https://d0.awsstatic.com/whitepapers/architecture/AWS_Well-Architected_Framework.pdf)
- [Whitepaper: Architecting for the Cloud: AWS Best Practices](https://d1.awsstatic.com/whitepapers/AWS_Cloud_Best_Practices.pdf)
- [Whitepaper: Practicing Continuous Integration and Continuous Delivery on AWS: Accelerating Software Delivery with DevOps](https://d1.awsstatic.com/whitepapers/DevOps/practicing-continuous-integration-continuous-delivery-on-AWS.pdf)
- [Whitepaper: Microservices on AWS](https://d1.awsstatic.com/whitepapers/microservices-on-aws.pdf)
- [Whitepaper: Amazon Web Services: Overview of Security Processes](https://d1.awsstatic.com/whitepapers/Security/AWS_Security_Whitepaper.pdf)
- [AWS Documentation](https://docs.aws.amazon.com/)
- [AWS Architecture Center](https://aws.amazon.com/architecture/)