# Quizzes Reviews & Notes



#### QUIZ: AWS Fundamentals Quiz for Developers

1 Attempt: %100



______________

#### QUIZ: AWS Identity and Access Management Concepts for Developers

1 Attempt: %75



**When requested through an STS API call, credentials are returned with what three components?**

* Security Token, Access Key ID, Signed URL
* **Security Token, Access Key ID, Secret Access Key**
* Signed URL, Security Token, Username
* Security Token, Secret Access Key, Personal Pin Code



**IAM Policies, at a minimum, contain what elements?**

* Id
* Sid
* **Effects**
* **Actions**
* Principal
* **Resources**



____________

#### QUIZ: AWS EC2 Essentials for Developers

1 Attempt: %62.5



**Which API call would best be used to describe an Amazon Machine Image?**

* ami-describe-image
* ami-describe-images
* DescribeImage
* **DescribeImages**

 In general, API actions stick to the PascalCase style with the first letter of every word capitalized.



__________

#### QUIZ: AWS VPC Fundamental Concepts for Developers

1 Attempt: %90

EC2 Instances in a public subnet (with a IGW) ALWAYS need an EIP or Public Ip assigned to it in order to connect to the internet, without a public ip can't download data.



_________

#### QUIZ: AWS Lambda Concepts for Developers

1 Attempt: %73

**A Lambda deployment package contains:**

* Function code, libraries, and runtime binaries
* Only Function Code
* **Function code and libraries not included within the runtime environment**
* Only libraries not included within the runtime

Lambda function packages or 'deployment packages' include function code and all the dependencies required for the function to work.



**When referencing the remaining time left for a Lambda function to run within the function's code you would use:**

* The event object
* The timeLeft object
* The remains object
* **The context object**

The context object will give you access to a method to get the remaining time left in function execution. For example, in Python this would return the remaining time in milliseconds: `context.get_remaining_time_in_millis()`



___________

#### QUIZ: AWS ECS Concepts for Developers

1 Attempt: %80

**The ECS Task Agent:**

*  **Is responsible for starting and stopping tasks**
*  Is responsible for managing the cluster of EC2 instances
* **Runs inside the EC2 instance and reports on information like running tasks and resource utilization**
* Runs inside each container and reports on metrics like open connections to that container and current processes

The ECS agent runs in each EC2 instance and helps manage the tasks on the instance



_____________________

#### QUIZ: AWS Elastic Beanstalk

1 Attempt: %80

**When deploying a simple Python web application with Elastic Beanstalk which of the following AWS resources will be created and managed for you by Elastic Beanstalk?**

* **An ELB**
* **An S3 Bucket**
* A Lambda Function
* **An EC2 Instance**

Elastic Beanstalk will configure an Elastic Load Balancer, a Ec2,  an S3 bucket to store some data for the web application.



___________

#### QUIZ: AWS S3

1 Attempt: %90

**Bucket policies are only attached to S3 buckets, ACLs can be attached to S3 Buckets OR S3 Objects. Bucket Policies are written in JSON, ACLs are written in XML.**



____________

#### QUIZ: AWS DynamoDB Concepts for Developers



_____________

#### QUIZ: AWS Database Concepts for Developers

1 Attempt: %80

**Which of the following best describes the Lazy Loading caching strategy?**

* Every time the underlying database is written to or updated the cache is updated with the new information.
* Every miss to the cache is counted and when a specific number is reached a full copy of the database is migrated to the cache
* A specific amount of time is set before the data in the cache is marked as expired. After expiration, a request for expired data will be made through to the backing database.
* **Data is added to the cache when a cache miss occurs (when there is no data in the cache and the request must go to the database for that data)**



_________________________

#### QUIZ: AWS SNS Essentials for Developers

1 Attempt: %40



______________

#### QUIZ: AWS SQS Essentials for Developers

1 Attempt: %82

**Which of the following is true if long polling is enabled?**

*  If long polling is enabled, then each poll only polls a subset of SQS servers; in order for all messages to be received, polling must continuously occur
* The reader will listen to the queue until timeout
* Increases costs because each request lasts longer
* The reader will listen to the queue until a message is available or until timeout

Long polling waits for messages to come into the queue until there are messages or the ReceiveMessageWaitTimeSeconds attribute limit is reached. 



_________________

#### QUIZ: AWS App Services Essentials for Developers (SWF, Step Functions, and API Gateway)

REPEAT ALL!!!



________________

#### QUIZ: AWS CICD

REPEAT ALL!!!



