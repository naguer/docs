### Report Card

Expectations

Score: 69.23

1. Demonstrate an Ability to Monitor and Troubleshoot within the AWS Environment: 53.85 %

2. Demonstrate an Ability to Deploy in the AWS Environment: 61.54 %
3. Demonstrate an Ability to Secure AWS Environments: 84.62 %

4. Demonstrate an Ability to Develop with AWS Services: 46.15 %

5. Demonstrate an Ability to Optimize New and Existing Applications to use AWS Services: 100 %



**Your team is using CodeDeploy to deploy an application that uses secure parameters that are stored in the AWS Systems Manager Parameter Store. What two options must be completed so that CodeDeploy can deploy the application?**

- Add permissions using AWS access keys.
- **Use `ssm get-parameters` with the `--with-decryption` option.**
- **Add permissions using an AWS IAM role.**
- Use `ssm get-parameters` with the `--with-no-decryption` option.



**CodeBuild projects have configuration settings that determine things like:**

-  What actions to take on the build artifacts after creation
- **Where the project's source code is located**
- **Where to store the output of the build**
- What AWS user is responsible for manually approving the build



**AWS CodeBuild allows you to compile your source code, run unit tests, and produce deployment artifacts by:**

* Allowing you to provide an Amazon Machine Image to take these actions within

- Allowing you to select an Amazon Machine Image and provide a User Data bootstrapping script to prepare an instance to take these actions within
- **Allowing you to provide a container image to take these actions within**
- **Allowing you to select from pre-configured environments to take these actions within**

Correct! You can provide your own custom container image to build your deployment artifacts.



**How can you control access to the API Gateway in your environment?**

- **Cognito User Pools**
- **Lambda Authorizers**
- API Methods
- API Stages



You have just developed a new application to be deployed on an EC2 instance. The EC2 instance will store data on a separate EBS volume. How would you make sure the data on the EBS volume is encrypted at rest?

- Create an EBS encryption key
- **Use KMS to create a customer master key**
- **Ensure encryption is enable when creating the volume**
- Use Provisioned IOPS to secure encryption



Your company policy is that all secrets must be encrypted. You are developing a Lambda function that needs access to one of your RDS instances and will share a connection string that contains the database's credentials which must remain secure. Which option below will allow the Developer to write the least amount of code?

- Use Lambda environment variables to include the secrets
- Use a common DynamoDB table to store the secrets
- Use another RDS instance to store the secrets
- **Use System Manager Parameter store secure strings**



**One of your requirements is to setup an S3 bucket to store your files like documents and images. However, those objects should not be directly accessible via the S3 URL, they should ONLY be accessible from pages on your website so that only your paying customers can see them. How could you implement this?**

- You can't. The S3 URL must be public in order to use it on your website.
- **You can use a bucket policy and check for the aws:Referer key in a condition, where that key matches your domain**
- You can use server-side and client-side encryption, where only your application can decrypt the objects
- Use HTTPS endpoints to encrypt your data

You could use a bucket policy like this: { "Version": "2012-10-17", "Id": "example", "Statement": [ { "Sid": "Allow get requests referred by [www.example.com](http://www.example.com/) and example.com.", "Effect": "Allow", "Principal": "*", "Action": "s3:GetObject", "Resource": "arn:aws:s3:::examplebucket/*", "Condition": { "StringLike": {"aws:Referer": ["[http://www.example.com/*","http://example.com/*"\]}](http://www.example.com/*%22,%22http://example.com/*%22]%7D) } }, { "Sid": "Explicit deny to ensure requests are allowed only from specific referer. Remember that explicit denies override all other permissions.", "Effect": "Deny", "Principal": "*", "Action": "s3:*", "Resource": "arn:aws:s3:::examplebucket/*", "Condition": { "StringNotLike": {"aws:Referer": ["http://www.example.com/*","[http://example.com/*"\]}](http://example.com/*%22]%7D) } } ] }

<https://docs.aws.amazon.com/AmazonS3/latest/dev/example-bucket-policies.html#example-bucket-policies-use-case-4>



**Your application instance takes 60 seconds to process instructions received in an SQS message. Assuming the SQS queue is configured with the default Visibility Timeout, what is the best way to configure your application to ensure that no other instances retrieve a message that has already been processed or is currently being processed?**

- Use the ReceiveMessage API call to retrieve the message and the DeleteMessage API call to delete the message when processing completes.

- Use the ReceiveMessage API call to retrieve the message. The message will automatically delete when processing completes.

- Use the ReceiveMessage API call to retrieve the message, the ChangeMessageVisibility API call to lower the visibility timeout. The message will automatically delete when processing completes.

- **Use the ReceiveMessage API call to retrieve the message, the ChangeMessageVisibility API call to increase the visibility timeout, and the DeleteMessage API call to delete the message when processing completes.**

  The message queue is using the default Visibility Timeout of 30 seconds, but the application takes 60 seconds to process the instructions from the message. It is therefore necessary to increase the Visibility Timeout of the message to prevent it from becoming visible in the queue for other instances to process while it is still being processed by the first instance. (Another solution could be to increase the visibility timeout of the entire queue.) It is also necessary for the instance to delete the message from the queue once it has finished processing it, otherwise the message will become visible in the queue after the Visibility Timeout expires.

  

**Which DynamoDB API call does not consume capacity units?**

- GetItem
- **UpdateTable**
- DeleteItem
- UpdateItem

The UpdateTable API call is used to change the required provisioned throughput capacity.



**If you have an item that is 4KB in size and you want to provision read capacity units for 100 requests per second, using strongly consistent reads how many read capacity units do you need to provision?**

- 20

- 50

- 90

- **100**

  100 x (4/4) = 100. Read capacity units are 4KB in size, and our data is 4KB in size. We divide 4KB/4KB = 1, and multiply by the number of requests per second. <https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/HowItWorks.ProvisionedThroughput.html>