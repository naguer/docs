- VPC Endpoint enables you to privately connect your VPC to supported AWS services and VPC endpoint services powered by PrivateLink without requiring an internet gateway, NAT device, VPN connection, or AWS Direct Connect connection. Instances in your VPC do not require public IP addresses to communicate with resources in the service. Traffic between your VPC and the other service does not leave the Amazon network. 

- VPC Endpoints: Differences between Interface Endpoints and Gateway Endpoints 

  Enables you to connect to services powered by AWS PrivateLink. *AWS PrivateLink* simplifies the security of data shared with cloud-based applications by eliminating the exposure of data to the public Internet. AWS PrivateLink provides private connectivity between VPCs, AWS services, and on-premises applications, securely on the Amazon network. 

[Interface Endpoints](https://docs.aws.amazon.com/vpc/latest/userguide/vpce-interface.html) support 20+ AWS services. (Charged per hour and per GB)

 [Gateway Endpoints](https://docs.aws.amazon.com/vpc/latest/userguide/vpce-gateway.html) are used only for S3 and DynamoDB.  (FREE)

Interface Endpoint is an ENI (think network card) within your VPC. It uses DNS record to direct your traffic to the private IP address of the interface. Gateway Endpoint uses route prefix in your route table to direct traffic meant for S3 or DynamoDB to the Gateway Endpoint (think 0.0.0.0/0 -> igw).

So to secure your Interface Endpoint, use Security Groups. But to secure Gateway Endpoint, use VPC Endpoint Policies.



- Difference between Internet Gateway (IGW) and NAT Gateway in vpc: 
  An Internet Gateway (IGW) allows resources within your VPC to access the internet, and vice versa. In order for this to happen, there needs to be a routing table entry allowing a subnet to access the IGW.

  That is to say - an IGW allows resources within your public subnet to access the internet, and the internet to access said resources.

  A NAT Gateway does something similar, but with two main differences:

  1. It allows resources in a private subnet to access the internet (think yum updates, external database connections, wget calls, etc), and
  2. it only works one way. The internet at large cannot get through your NAT to your private resources unless you explicitly allow it.



* To enable access to or from the Internet for instances in a VPC subnet, you must do the following:

  \- Attach an internet gateway to your VPC.

  \- Ensure that your subnet's route table points to the Internet gateway.

  \- Ensure that instances in your subnet have a globally unique IP address (public IPv4 address, Elastic IP address, or IPv6 address).

  \- Ensure that your network access control lists and security groups allow the relevant traffic to flow to and from your instance.



* Amazon EMR BIG DATA HADOOP (use for ETL and ML)
* AWS Appsync sync web and mobile
* SQS retention default 4 days, max 14 days. The default visibility timeout for a message is 30 seconds. The maximum is 12 hours.

* You can set up a SAML-Based Federation for API Access to your AWS cloud. In this way, you can easily connect to AWS using the login credentials of your on-premises network.

* (Business intelligence = Redshift) OLAP

* AWS Key Management Service (AWS KMS) is a managed service that makes it easy for you to create and control the encryption keys used to encrypt your data. 
* Products with SQL queries to s3, S3 Select, Athena, Redshift Spectrum
* SQS and SNS are messaging services and do not perform asynchronous function calls.

* Snowball 80TB (72 TB) max, for max capacity you need to use Snowball Edge

* DB parameter groups act as a *container* for engine configuration values that are applied to one or more DB instances. Useful if you  want to apply a group of database-specific settings.

* The lifecycle storage class transitions have two constraints: Dont smaller than 128kb and at least 30 days (this not apply to intelligent tiering, glacier and deep)

* Enhanced networking provides higher bandwidth, higher packet per second (PPS) performance, and consistently lower inter-instance latencies. There is no additional charge for using enhanced networking (only in supported instace types)

* AWS Organizations offers policy-based management for multiple AWS accounts. With Organizations, you can create groups of accounts, automate account creation, apply and manage policies for those groups.

* Vertical scaling means running the same software on bigger machines which is limited by the capacity of the individual server. Horizontal scaling is adding more servers to the existing pool and doesn’t run into limitations of individual servers.

* KSM vs CloudHSM: KSM is shared hardware tenancy - you keys are in their own partition of an encryption module shared with other AWS customers, each with their own isolated partition. Cloud HSM gives you your own hardware module, so the most likely reason to choose Cloud HSM is if you had to ensure your keys were isolated on their own encryption module for compliance purposes. Also, AWS KSM only uses symmetric keys, while Cloud HSM allows symmetric and asymmetric keys.

* Nat Gateway = Ipv4, Egress-only Internet Gateaway = Ipv6 (Both to acess to internet, but internet cant access to the instances)

* First enable Enhanced Vpc Routing, after you can create a new flow log that tracks the traffic.

* SAML enable you to sign in using your corporate directory credentials, such as your user name and password from Microsoft Active Directory. With SAML, you can use single sign-on (SSO) to sign in to all of your SAML-enabled applications by using a single set of credentials. You can enable SAML authentication for your AWS accounts by using [AWS Identity and Access Management](https://aws.amazon.com/iam/) (IAM). You can add SAML support to your web and mobile apps running on the AWS Cloud by using [Amazon Cognito](https://aws.amazon.com/cognito/). If you use Microsoft Active Directory which implements Security Assertion Markup Language (SAML), you can set up a SAML-Based Federation for API Access to your AWS cloud. In this way, you can easily connect to AWS using the login credentials of your on-premises network.

* To increase the write performance of the database (throughput to the database needs to be improved) hosted in an EC2 instance. You can achieve this by either setting up a standard RAID 0 configuration or simply by increasing the size of the EC2 instance.

* AWS Storage Gateway & Glacier encrypts data at rest by default

* CloudFront and Elastic Load Balancing are the two AWS services that support Perfect Forward Secrecy. 

* Public IP addresses are dynamic - i.e. if you stop/start your instance you get reassigned a new public IP.

  Elastic IPs get allocated to your account, and stay the same - it's up to you to attach them to an instance or not. You could say they are static public IP addresses.

* Lambda@Edge lets you run Lambda functions to customize the content that CloudFront delivers, executing the functions in AWS locations closer to the viewer. 

* *AWS Glue* is a fully managed extract, transform, and load (ETL) service that makes it easy for customers to prepare and load their data for analytics.

* Steps functions for microservises or distributed apps (coordinate the components)