# Route53

#### Dns 101

***Exam Tips***

- ELBs do not have pre-defined IPv4 addresses; you resolve to them using a DNS name
- Understand the difference between an Alias Record and  a CNAME
- Given the choice, always choose an Alias Record over a CNAME
- Common DNS Types
  - SOA Records
  - NS Records
  - A Records
  - CNAMES
  - MX Records
  - PTR Records



____________

#### Route53 - Register A Domain Name Lab

***Exam Tips***

- You can buy domain names directly with AWS
- It can take up to 3 days to register depending on the circumstances



_________

#### Route53 Routing Policies Available On AWS

***Exam Tips***

Routing Policies

- Simple
- Weighted
- Latency-bases
- Failover
- Geolocation
- Geo-proximity (Traffic Flow Only)
- Multivalue Answer 



________

#### Simple Routing Policy Lab

***Exam Tips***

- If you choose the simple routing policy you can only have one record with multiple IP addresses. If you specify multiple values in a record, Route53 returns all value to the user in a random order.



____________

#### Weighted Routing Policy Lab

***Exam Tips***

- You can put weighted points to ips for make routing, for example %40 to ip in sidney and %60 to ip in irland
- Health Checks
  - You can set HC on individual records sets
  - If a record set fails a HC it will be removed from Route53 until it passes the health check
  - You can set SNS notifications to alert you if a HC is failed



______________

#### Latency Routing Policy 

***Exam Tips***

- Automatically Route53 select lower latency endpoint to send the traffic



_______________

#### Failover Routing Policy 

***Exam Tips***

- Is an active/passive set up, for e.g. primary site to be in EU-WEST-2 and your secondary DR site in AP-SOUTHEAST-2
- Route53 will monitor your primary site using a health check
- You can only set one passive-secondary location
- Is mostly for DR not for availability-performance.



------

#### Geolocation Routing Policy 

***Exam Tips***

- You can redirect traffic from one location (like ue) to a specified  Servers (in ue), for e.g. user from UE will be view the website from ue. Basically route traffic based in the location.
- Isn't the same than Latency Routing Policy because yo choose the location and the final endpoint server.



------

#### Geoproximity Routing Policy (Traffic Flow Only)

***Exam Tips***

- Its really complicated topic and not is in the exam
- To use geoproximity routing, you must use Route53 traffic flow



------

#### Multivalue Answer 

***Exam Tips***

- Is similar to Simple Routing however it allows you to put Health Checks on each record set, basically is Single Routing with Health Checks 



------

#### Route53 Quiz

First Attempt: %56

First Attempt: %100



***Check:***

**True or False: There is a limit to the number of domain names that you can manage using Route 53.**

* True and False. With Route 53, there is a default limit of 50 domain names. However, this limit can be increased by contacting AWS support.



**You have an enterprise solution that operates Active-Active with facilities in Regions US-West and India. Due to growth in the Asian market you have been directed by the CTO to ensure that only traffic in Asia (between Turkey and Japan) is directed to the India Region. Which of these will deliver that result? (Choose 2)**

* R53 - Geolocation routing policy
* R53 - Geoproximity routing policy

The instruction from the CTO is clear that that the division is based on geography. Latency based routing will approximate geographic balance only when all routes and traffic evenly supported which is rarely the case due to infrastructure and day night variations. You cannot combine Blacklist & Whitelist in CloudFront. Weighted routing is randomized and will not respect Geo boundaries. Geolocation is based on national boundaries and will meet the needs well. Geoproximity is based on Latitude & Longitude and will also provide a good approximation with potentially less configuration. Further information: <https://aws.amazon.com/about-aws/global-infrastructure/><https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/routing-policy.html#routing-policy-geo><https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/georestrictions.html>



**In AWS Route53, which of the following are true? (Choose 2)**

* A CNAME record assigns an Alias name to a Canonical Name.
* Alias Records provide a Route 53–specific extension to DNS functionality

A CNAME record assigns an Alias name to a Canonical Name. This is a normal DNS capability as defined in the RFCs. By design CNAMEs are not intended to point at IP addresses. Further information: <https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/ResourceRecordTypes.html><https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/resource-record-sets-choosing-alias-non-alias.html><https://tools.ietf.org/html/rfc2181><https://tools.ietf.org/html/rfc1034>



**Your company hosts 10 web servers all serving the same web content in AWS. They want Route 53 to serve traffic to random web servers. Which routing policy will meet this requirement, and provide the best resiliency?**

* Multivalue Routing

Multivalue answer routing lets you configure Amazon Route 53 to return multiple values, such as IP addresses for your web servers, in response to DNS queries. Route 53 responds to DNS queries with up to eight healthy records and gives different answers to different DNS resolvers. The choice of which to use is left to the requesting service effectively creating a form or randomisation. Further information: <https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/routing-policy.html#routing-policy-multivalue>