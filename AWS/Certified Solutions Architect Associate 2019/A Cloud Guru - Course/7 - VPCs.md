# VPCs

#### Introduction To VPCs

***Exam Tips***

- Think of a VPC as a logical datacenter in AWS
- Consists of IGWs (Or Virtual Private Gateways), Route Tables, Network Access Control Lists, Subnets, and Security Groups
- 1 Subnet = 1 AZ, you cannot have a subnet in multiple AZ, however you can have multiple subnets in the same AZ
- Security groups are stateful: This means any changes applied to an incoming rule will be automatically applied to the outgoing rule. e.g. If you allow an incoming port 80, the outgoing port 80 will be automatically opened.
- Network ACLs are stateless: This means any changes applied to an incoming rule will not be applied to the outgoing rule. e.g. If you allow an incoming port 80, you would also need to apply the rule for outgoing traffic.
- Security group support allow rules only (by default all rules are denied). e.g. You cannot deny a certain IP address from establishing a connection.
- Network ACL support allow and deny rules. By deny rules, you could explicitly deny a certain IP address to establish a connection example: Block IP address 123.201.57.39 from establishing a connection to an EC2 Instance.
- Internet Gateway allows access to out to the Internet and back, both directions
- Securty Groups are tied to an instance, Network ACLs are tied to the subnet
- No Transitive peering. If you have VPC A connect to VPC B, and VPC B connect to VPC C, A can't connect to C, you have to create a peering connection between A and C



***Resources***

[Diff Between SG and Network ACLs]: https://medium.com/awesome-aws/aws-difference-between-security-groups-and-network-acls-adc632ea29ae
[VPC Desing and New Capabilities]: https://www.youtube.com/watch?v=rQvl-V3tLiQ



_____

#### Build A Custom VPC 

***Exam Tips***

- When you create a VPC a default Route Table, Network ACLs (NACL) and a default SG its created
- It won't create any subnets, or will it create a default internet gateway
- UE-East-1A in your AWS account can be a completely different availability zone to US-East-1A in another AWS account. The AZ's are randomized
- Amazon always reserve 5 IP addresses within your subnets
- You can only have one internet gateway per VPC
- Security Groups can't span VPCs



------

#### Network Address Translation / NAT Gateways

***Exam Tips***

* Nat Instances:
  * When a creating a NAT instance, Disable Source/Destination Check on the instance
  * Nat instances must be in a public subnet
  * There must be a route out of the private subnet to the NAT instance, in order for this to work
  * The amount of traffic that NAT instances can support depends on the instance size, if you are bottlenecking, increase the instance size.
  * You can create HA using Autoscaling Groups, multiple subnets in different AZs, and a script to automate failover
* Nat Gateway:
  * Redundant inside the AZ
  * Preferred by the enterprise
  * Starts at 5Gbps and scales currently to 45Gbps
  * No need to patch
  * Not associated with SGs
  * Automatically assigned a public ip address
  * Remember to update your route tables
  * No need to disable Source/Destination Checks
  * if you have resources in multiple AZ and they share one NAT Gateway, in the event that the NAT gateway's AZ is down, resources in the other AZ lose internet access, to create an AZ independent architecture, create a NAT gateway in each AZ and configure your routing to ensure that resources use the NAT gateway in the same AZ.

***Resources***

[NAT Instances]: https://docs.aws.amazon.com/vpc/latest/userguide/VPC_NAT_Instance.html



------

#### Access Control Lists (ACL)

***Exam Tips***

- Your VPC automatically comes a default network ACL, and by default it allows all outbound and inbound traffic
- You can create custom network ACLs, by default each custom network ACL denies all inbound and outbound traffic until you add rules
- Each subnet in your VPC must be associated with a network ACL, if you don't explicitly associate a subnet with a network ACL, the subnet is automatically associated with the default network ACL
- You can associate a network ACL with multiple subnets, however, a subnet can be associated with only one network ACL at a time. When you associate a network ACL with a subnet, the previous association is removed
- Network ACLs contain a numbered list of rules that is evaluated in order, starting with the lowest numbered rule
- Network ACLs have separate inbound and outbound rules, and each rule can either allow or deny traffic
- With NACL you can deny access to a resource
- Network ACLs are stateless, responses to allowed inbound traffic are subject to the rules for outbound traffic (and viceversa) - (If you create a inbound rules, you need the create the outbound rules for the same ports)

***Resources***

[DOC: AWS VPC Ephemeral Ports]: https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html#nacl-ephemeral-ports
[VPC Security]: https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Security.html



------

#### Custom VPCs and ELBs

***Exam Tips***

- For Application LB and Classic LB -> at least 2 subnets
- For Network LB -> just 1 subnet (Load balance TCP traffic)

***Resources***

[Network LB Overview]: https://docs.aws.amazon.com/elasticloadbalancing/latest/network/introduction.html#network-load-balancer-overview



------

#### VPC Flow Logs

***Exam Tips***

- You cannot enable flow logs for VPCs that are peered with your VPC unless the peer VPC  is in your account
- You cannot tag a flow log
- After you're created a flow log, you cannot change his configuration, for example, you can't associate a different IAM role with the flow log.
- VPC Flow Logs can be created at the VPC, subnet, and network interface levels.
- Not all IP Traffic is monitored:
  - Traffic generated by instances when they contact the Amazon DNS server. if you use your own DNS server, then all traffic to that DNS server is logged.
  - Traffic generated by a Windows instance for Amazon Windows license activation
  - Traffic to and from 169.254.169.254 for instance metadata.



------

#### Bastions

***Exam Tips***

- A NAT Gateway or Nat instances is used to provide internet traffic to EC2 instances in a private subnets.
- A Bastion is used to securely administer EC2 instances (using ssh or rdp), Bastions are called Jump Boxes in Australia.
- You cannot use a NAT Gateway as a Bastion host.

***Resources***

[SSH Agent Forwarding]: https://aws.amazon.com/blogs/security/securely-connect-to-linux-instances-running-in-a-private-amazon-vpc/



------

#### Direct Connect

***Exam Tips***

- Direct Connect directly connects your data center to AWS
- Useful for high throughput workloads (ie lots of network traffic)
- Or if you need a stable and reliable secure connection



------

#### VPC EndPoints

***Exam Tips***

- A VPC endpoint enables you to privately connect your VPC to supported AWS services and VPC endpoint services powered by PrivateLink without requiring an internet gateway, NAT device, VPC connection, or AWS Direct Connect connection. Instances in your BPC don't require public IP addresses to communicate with resources in the service. Traffic between your VPC and the other service does not leave the Amazon network.
- Endpoints are virtual devices, they are horizontally scaled, redundant, and highly available VPC components that allow communication between instances in your VPC and services without imposing availability risks or bandwidth constraint on your network traffic. 
- Traffic between your VPC and the other service does not leave the Amazon network
- There are two types of VPC endpoints:
  - Interface Endpoints
  - Gateway Endpoints
- Currently Gateway Endpoints Support:
  - Amazon S3
  - DynamoDB



------

#### VPCs Quiz

First Attempt: %68

Second Attempt: %88 (2 Fails)



**A VPN connection consists of which of the following components? (Choose 2)**

* Customer Gateway
* Virtual Private Gateway

The correct answers are "Customer Gateway" and "Virtual Private Gateway". When connecting a VPN between AWS and a third party site, the Customer Gateway is created within AWS, but it contains information about the third party site e.g. the external IP address and type of routing. The Virtual Private Gateway has the information regarding the AWS side of the VPN and connects a specified VPC to the VPN. "Direct Connect Gateway" and "Cross Connect" are both Direct Connect related terminology and have nothing to do with VPNs. Further information: <https://docs.aws.amazon.com/vpc/latest/userguide/VPC_VPN.html><https://docs.aws.amazon.com/directconnect/latest/UserGuide/direct-connect-gateways.html><https://docs.aws.amazon.com/directconnect/latest/UserGuide/Colocation.html>



**At which of the following levels can VPC Flow Logs be created? (Choose 3)**

* Network Interface Level
* Subnet Level
* VPC Level

VPC Flow Logs can be created at the VPC, subnet, and network interface levels.



**Having just created a new VPC and launching an instance into its Public Subnet, you realise that you have forgotten to assign a Public IP to the instance during creation. What is the simplest way to make your instance reachable from the outside world?**

* Create an Elastic IP address and associate it with your instance

NO "Create an Elastic IP and new Network Interface. Associate the Elastic IP to the new Network Interface, and the new Network Interface to your instance."

Although creating a new NIC & associating an EIP also results in your instance being accessible from the internet, it leaves your instance with 2 NICs & 2 private IPs as well as the Public Address and is therefore not the simplest solution. By default, any user-created VPC subnet WILL NOT automatically assign Public IPv4 Addresses to instances – the only subnet that does this is the “Default” VPC subnets automatically created by AWS in your account. Further information: <https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html#using-instance-addressing-eips-associating><https://docs.aws.amazon.com/vpc/latest/userguide/vpc-ip-addressing.html#subnet-public-ip>


**What is the purpose of an Egress-Only Internet Gateway? (Choose 2)**

* Prevents IPv6 based Internet resources initiating a connection into a VPC

* Allows VPC based IPv6 traffic to communicate to the Internet

The purpose of an "Egress-Only Internet Gateway" is to allow IPv6 based traffic within a VPC to access the Internet, whilst denying any Internet based resources the possibility of initiating a connection back into the VPC. Further information: <https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html><https://docs.aws.amazon.com/vpc/latest/userguide/egress-only-internet-gateway.html><https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat.html>



**By default, instances in new subnets in a custom VPC can communicate with each other across Availability Zones.**

* True

In a custom VPC with new subnets in each AZ, there is a Route that supports communication across all subnets/AZs. Plus a Default SG with an allow rule 'All traffic, All protocols, All ports, from anything using this Default SG'. Further information: <https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/default-vpc.html><https://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_SecurityGroups.html>



**When you create a custom VPC, which of the following are created automatically? (Choose 3)**

* Route Table
* SG
* ACL

When you create a custom VPC, a default Security Group, Access control List, and Route Table are created automatically. You must create your own subnets, Internet Gateway, and NAT Gateway (if you need one.) Further information: <https://docs.aws.amazon.com/cli/latest/reference/ec2/create-vpc.html>



**When peering VPCs, you may peer your VPC only with another VPC in your same AWS account.**

* False

You may peer a VPC to another VPC that's in your same account, or to any VPC in any other account.



**Select the incorrect statement.**

* In Amazon VPC, an instance does not retain its private IP.

Because dont have a EIP, when you terminate the EC2 instance, the private IP it's lost.