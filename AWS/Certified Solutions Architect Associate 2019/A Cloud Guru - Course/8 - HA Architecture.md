# HA Architecture

#### Load Balancers Theory

***Exam Tips***



_______________

#### Load Balancers And Health Checks Lab

***Exam Tips***

* 3 Load Balancers:
  * Application Load Balancers for intelligent routing
  * Network Load Balancers for very high performance
  * Classic Load Balancers, if you dont want intelligent routing and want mantain cost down.
* 504 Error means the gateway has time out. This means that the application not responding within the idle timeout period (you need troubleshotting the app, ws or db)
* If you need the IPv4 address of your end user, look for the X-Forwarded-For header
* Instances monitored by ELB are reported as: Inservice or OutofService
* HealthChecks check the instance health by talking to it
* LB have their own DNS name, you are never given an IP address
* Read the ELB FAQ for Classic LB

***Resources***

[Registering Instances Classic LB]: https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-backend-instances.html
[Elastic Load Balancing]: https://docs.aws.amazon.com/elasticloadbalancing/latest/userguide/what-is-load-balancing.html



------

#### Advanced Load Balancer Theory

***Exam Tips***

* Sticky Sessions enable your users to stick to the same EC2 Instances, can be useful if you are storing information locally to that instance
* Cross Zone Load Balancing enables you to load balance across multiple AZ (ALB has CZLB enabled by default)
* Path patters allow you to direct traffic to different EC2 instances bases on the URL contained in the request



------

#### Autoscaling Groups Lab

***Exam Tips***

***Resources***

[Controlling Access to Your Amazon EC2 Auto Scaling Resources]: https://docs.aws.amazon.com/autoscaling/ec2/userguide/control-access-using-iam.html



------

#### HA Architecture

***Exam Tips***

* Always Design for failure
* Use Multiple AZ's and Multiple Regions where ever you can
* Know the difference between Multi-AZ (DR) and RR for RDS (Performance)
* Know the difference between scaling out and scaling up
* Read the question carefully and always consider the cost element
* Know the different S3 storage classes

***Resources***

[Netflix Simian Army]: https://medium.com/netflix-techblog/the-netflix-simian-army-16e57fbab116



------

#### Building A Fault Tolerant WP - HA Wordpress Site - Lab1

***Resources***

[AWS REFARCH WP]: https://github.com/aws-samples/aws-refarch-wordpress

[WP Best Practices on AWS]: https://d1.awsstatic.com/whitepapers/wordpress-best-practices-on-aws.pdf



------

#### Building A Fault Tolerant WP - Setting Up EC2 - Lab2



------

#### Building A Fault Tolerant WP - Adding Resilience And Autoscaling - Lab3



------

#### Building A Fault Tolerant WP - Cleaning Up - Lab4

***Exam Tips***



------

#### Building A Fault Tolerant WP - CloudFormation - Lab5

***Exam Tips***

* Is a way of completely scripting your cloud environment
* Quick Start is a bunch of CloudFormation templates already built by AWS Solutions Architects allowing you to create complex environments very quickly

***Resources***

[AWS Quick Starts]: https://aws.amazon.com/quickstart



------

#### Elastic Beanstalk

***Exam Tips***

* With Elastic Beanstalk you can quickly deploy and manage applications in the AWS Cloud without worrying about the infrastructure that runs those apps, you simply upload your app and Elastic Beanstalk automatically handles the details of capacity provisioning, load balancing, scaling, and application health monitoring.



------

#### HA Symmary

***Resources***

[AWS Cloud Best Practices]: https://d1.awsstatic.com/whitepapers/AWS_Cloud_Best_Practices.pdf

[AWS Operational Excellence Pillar]: https://d1.awsstatic.com/whitepapers/architecture/AWS-Operational-Excellence-Pillar.pdf



------

#### HA Architecture Quiz

First Attempt: %76

**You work for a major news network in Europe. They have just released a new mobile app that allows users to post their photos of newsworthy events in real time. Your organization expects this app to grow very quickly, essentially doubling its user base each month. The app uses S3 to store the images, and you are expecting sudden and sizable increases in traffic to S3 when a major news event takes place (as users will be uploading large amounts of content.) You need to keep your storage costs to a minimum, and you are happy to temporally lose access to up to 0.1% of uploads per year. With these factors in mind, which storage media should you use to keep costs as low as possible?**

* S3 Standard-IA (Because you can loose 0.1%, you need 99,9%, for that is Standard-ia)

The key drivers here are availability and cost, so an awareness of cost is necessary to answer this. Full S3 is quite expensive at around $0.023 per GB for the lowest band. S3 standard IA is $0.0125 per GB, S3 OneZone-IA is $0.01 per GB, and Legacy S3-RRS is around $0.024 per GB for the lowest band. Of the offered solutions S3 One Zone-IA is the cheapest suitable option. Glacier cannot be considered as it is not intended for direct access, however it comes in at around $0.004 per GB. S3 has an availability of 99.99%, S3-IA has an availability of 99.9% while S3-1Zone-IA only has 99.5%Further information: <https://aws.amazon.com/s3/pricing/><https://aws.amazon.com/s3/storage-classes/?nc=sn&loc=3><https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-class-intro.html>



**Following an unplanned outage you have been called into a planning meeting. You are asked what can be done to reduce the risk of a single bad deployment taking the whole site down. (The selected options do not necessarily need to work together) (Choose 4)**

* Use several Target groups or auto scaling groups under each Load Balancers.
* Use Route53 with health checks to distribute load across multiple ELBs.
* Use multiple auto scaling groups and boundaries for a staged or 'canary' deployment process.
* Use a Classic Load Balancer to spread the load over several availability zones.

Using R53 to distribute work direct to compute resources can work, but is hard to manage, and is not a recommended AWS pattern. ELB can spread load across AZs not regions. Deploying updates to all groups simultaneously will not reduce risk. Using R53 in combination with ELBs is a good pattern to distribute regionally as well as across AZs. Although the methods vary, you can place multiple autoscaling or target groups behind ELBs. Further information: <https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-add-availability-zone.html><https://docs.aws.amazon.com/elasticloadbalancing/latest/network/target-group-register-targets.html><https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/dns-failover-complex-configs.html>



**You have a web site with three distinct services each hosted by different web server autoscaling groups. Which AWS service should you use.**

* Application Load Balancers (ALB)

The ALB has functionality to distinguish traffic for different targets (mysite.co/accounts vs. mysite.co/sales vs. mysite.co/support) and distribute traffic based on rules for; target group, condition, and priority. Further information: <https://aws.amazon.com/elasticloadbalancing/><https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html>



**Placement Groups can either be of the type 'Cluster', 'Spread', or 'Partition'. Choose options from below which are only specific to Spread Placement Groups.**

* A spread placement group is a group of instances that are each placed on distinct underlying hardware

There is only one answer that is specific to Spread Placement Groups, and that is the final option. Whilst some of these answers are correct for either Cluster Placement Groups only, or for both Cluster and Spread Placement Groups, the question stated that only options specific to Spread Placement Groups should be chosen. This would rule out two options as they are true for both Spread & Cluster type placement groups. The Logical grouping of instances within a single Availability Zone is only true of Cluster Placement Groups and is also incorrect. Further information: <https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/placement-groups.html>



**A product manager walks into your office and advises that the simple single node MySQL RDS instance that has been used for a pilot needs to be upgraded for production. She also advises that they may need to alter the size of the instance once they see how many people use the system during peak periods. The key concern is that there can not be any outages of more than a few seconds during the go-live period. Which of the following might you recommend, (Choose 2)**

* Convert the RDS instance to a multi-AZ implementation.
* Consider replacing it with Aurora before go live.

There are two issues to be addressed in this question. Minimizing outages, whether due to required maintenance or unplanned failures. Plus the possibility of needing to scale up or down. Read-replicas can help you with high read loads, but are not intended to be a solution to system outages. Multi-AZ implementations will increase availability because in the event of a instance outage one of the instances in another AZs will pick up the load with minimal delay. Aurora provided the same capability with potentially higher availability and faster response. Further information: <https://aws.amazon.com/blogs/database/scaling-your-amazon-rds-instance-vertically-and-horizontally/>
