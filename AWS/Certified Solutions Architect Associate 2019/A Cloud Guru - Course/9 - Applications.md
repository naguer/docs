# Applications

#### SQS

***Exam Tips***

- SQS is pull based, not pushed based
- Messages are 256 KB in size
- Messages can be kept in the queue from 1 min to 14 days, the default retention period is 4 days
- Visibility TimeOut is the amount of time that the message is invisible in the SQS queue after a reader picks up that message. Provided the jobs is processed before the visibility time out expires, the message will then be deleted from the queue, if the job is not processed within that time the message will become visible again and another reader will process it, this could result in the same message being delivered twice
- Visibility timeout maximum is 12 hours for ec2 time processing
- SQS guarantees that your messages will be processed at least once
- Amazon SQS long polling is a way to retrieve messages from your Amazon SQS queues, while the regular short polling return immediately (even if the message queue being polled is empty) long polling doesn't return a response until a message arrives in the message queue, or the long poll times out (with long polling you can save money because your Ec2 instances don't be all the time doing polling)

***Resources***

[How Amazon SQS Works]: https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-how-it-works.html



------

#### SWF

***Exam Tips***

* SWF vs SQS:
  * SQS has a retention period of up to 14 days, with SWF workflow executions can last up to 1 year
  * Amazon SWF presents a task-oriented API, whereas Amazon SQS offers a message-oriented API (Task = SWF, Decoupling architecture = SQS)
  * Amazon SWF ensures that a task is assigned only once and is never duplicated, with Amazon SQS, you need to handle duplicated messages and may also need to ensure that a message is processed only once
  * SWF keeps track of all the tasks and events in an application, with amazon SQS you need to implement your own application-level tracking especially if your app uses multiple queues.

- SWF Actors:

  - Workflow Starters: An app that can initiate (start) a workflow, could be your e-commerce website following the placement of an order, or a mobile app searching for bus times.
  - Deciders: Control the flow of activity tasks in a workflow execution, if something has finished (or failed) in a workflow, a Decider decides what to do next.
  - Activity Workers: Carry out the activity tasks 

  

------

#### SNS

***Exam Tips***

- SNS Benefits:
  - Instantaneous, push-based delivery (no polling)
  - Simple APIs and easy integration with apps
  - Flexible message delivery over multiple transport protocols
  - Inexpensive, pay-as-you-go model with no up-front costs
  - Web-bases AWS Management Console offers the simplicity of a point-and-click interface
- SNS vs SQS:
  - Both MEssaging Services in AWS
  - SNS - Push
  - SQS - Poll (pulls)



------

#### Elastic Transcoder

***Exam Tips***

- Elastic Trancoder is a media transcoder in the cloud, it converts media files from their original source format in to differents formats that will play on smartphones, tablets, PCs, etc

------

#### Api Gateway

***Exam Tips***

- Api Gateway is essential a door to your AWS Environment
- Api Gateway has caching capabilities to increase performance
- Api Gateway is low cost and scales automatically
- You can throttle API Gateway to prevent attacks
- You can log results to CloudWatch
- If you are using Javascripts/AJAX that uses multiple domains with API Gateway, ensure that you have enabled CORS on API Gateway
- CORS is enforced by the client

***Resources***

[Api Gateway REST API Reference]: https://docs.aws.amazon.com/apigateway/api-reference/



------

#### Kineses

***Exam Tips***

- Kinesis Streams, use shards and persistence
- Kinesis Firehose, don't use persistence, analyze on the fly
- Kinesis Analytics if for analyze your data inside Kinesis (Streams and Firehose)



------

#### Web Identity Federation & Cognito

***Exam Tips***

- Federation allows users to authenticate with a Web Identity Provider (Google, Facebook, Amazon)
- The user authenticates first with the Web ID provider and receives and authentication token, which is exchanged for temporary AWS credentials allowing them to assume an IAM Role
- Cognito is an Identity Broker which handles interaction between your applications and the web id provider (you don't need to write your own code to do this)
- User pool is user based, it handles things like user registration, authentication, and account recovery
- Identity pools authorize access to your AWS resources



------

####  Applications Quiz

First Atempt: 91 (one fail)

**Amazon SWF is designed to help users ____.**

- Coordinate synchronous and asynchronous tasks

Similar to SQS SWF manages queues of work, however unlike SQS it can have out-of-band parallel and sequential task to be completed by humans and non AWS services Further information: <https://aws.amazon.com/swf/>



**In SWF, what does a "domain" refer to?**

- A collection of related workflows



**What is the difference between SNS and SQS?**

- SNS is a push notification service, whereas SQS is message system that requires worker nodes to poll a queue.

SNS is a Notification service for sending text based communication of different types to different destinations. SQS is a Queue system for asynchronously manages tasks (called messages) Further information: <https://aws.amazon.com/sqs/><https://aws.amazon.com/sns/>

