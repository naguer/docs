# Serverless

#### Lambda Concepts

***Exam Tips***

* Lambda scales out (not up) automatically, 5 triggers -> triggers 5 separate lambda functions. Each time your function is triggered, a new, separate instance of that function is started.
* Lambda functions are independent, 1 event = 1 function
* Lambda is Serverless
* Know what services are Serverless! (RDS not serveless)
* Only Aurora Serverless is Serverless
* Lambda functions can trigger other lambda functions, 1 event can = x functions if functions trigger other functions
* Architectures can get extremely complicated, AWS x-ray allows you to debug what is happening
* Lambda can do things globally, you can use it to backup S3 buckets to other S3 buckets, etc.
* Know your triggers
* Normally is a api gateway -> Lambda function -> DynamoDb
* ApiGateway, DynamoDb and S3 can invoke Lambda function directly
* Lambda pricing is based on the amount of memory, and the duration of execution billed in factions of seconds

***Resources***

[Lambda Scaling]: https://docs.aws.amazon.com/lambda/latest/dg/scaling.html
[Lambda Pricing]: https://aws.amazon.com/lambda/pricing/



__________

#### Serverless Quiz

First Attempt: %67

Second Attempt: %100


**On Friday morning your marketing manager calls an urgent meeting to celebrate that they have secured a deal to run a coordinated national promotion on TV, radio, and social media over the next 10 days. They anticipate a 500x increase on site visits and trial registrations. After the meeting you throw some ideas around with your team about how to ensure that your current 1 server web site will survive. Which of these best embody the AWS design strategy for this situation. (Choose 2)**

* Work with your web design team to create some web pages with embedded java scripts to emulate your 5 most popular information web pages and sign up web pages.
* Create a duplicate sign up page that stores registration details in DynamoDB for asynchronous processing using SQS & Lambda.

A 500x increase is beyond the scope of a well designed single server system to absorb unless it is already hugely overspecialised to accommodate this sort of burst load. An AWS solution for this situation might include S3 static web pages with client side scripting to meet high demand of information pages. Plus use of a noSQL database to collect customer registration for asynchronous processing, and SQS backed by scalable compute to keep up with the requests. Lightsail does provide a scalable provisioned service solutions, but these still need to be designed an planned by you and so offer no significant advantage in this situation. A standby server is a good idea, but will not help with the anticipated 500x load increase. Further information: https://aws.amazon.com/architecture/well-architected/



**Which of the following services can invoke Lambda function directly? (Choose 3)**

* Api-Gateway
* DynamoDb
* S3

An Application Load Balancer (ALB) can invoke a Lambda function directly, but not the ELB (Classic Load Balancer)

ELB contains 3 different types of load balancers. Not all are supported for invoking Lambda functions.