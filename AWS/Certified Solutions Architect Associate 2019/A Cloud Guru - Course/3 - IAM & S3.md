# IAM & S3

#### IAM 101:

***Exam Tips***

IAM offers the following features:

* Manage users and their level of access to AWS console.

* Centralized control of your AWS account
* Shred Access to your AWS account
* Granular Permissions
* Identity Federation (Including AD, FB, Linkedin etc)
* Multifactor Authentication
* Provide Temporary Access for users/devices
* Allow you to set up your own password rotation policy
* Integrates with many different AWS services
* Supports PCI DSS Compliance

* Key Terminology you need to know: Users, Groups, Policies, Roles

Plans: 


Basic Plan: Free

- Included with all accounts
- 24/7 self-service access to forums and resources
- Best practice checks to help improve security and performance
- Access to health status and notifications
  

Developer Plan: From $29/month

- For early adoption, testing and development
- Email access to AWS Support during business hours
- 1 primary contact can open an unlimited number of support cases
- 12-hour response time for nonproduction systems

Business Plan: From $100/month

- For production workloads & business-critical dependencies
- 24/7 chat, phone, and email access to AWS Support
- Unlimited contacts can open an unlimited number of support cases
- 1-hour response time for production systems



------

#### IAM Lab:

***Exam Tips***

* IAM Is Universal. It does not apply to region at this time.
* The "Root Account", is the first account, the admin account.
* New Users have NO permissions when first created.
* New User are assigned Access Key ID & Secret Access Key when first created.
* These are not the same as a password. The AKID and SAK is only to access AWS via the APIs and command line, you cannot use to login in to the console.
* You only get to view these once, if you lose them, you have to regenerate them.
* Power Users Access allows to all aws services except the management of groups and users within IAM

***What Have We Learn so Far?***

* Always setup multi actor Auth on your root account
* You can create and customize your own password rotation policies



_____

#### Create a Billing Alarm:

***Resources***

[Create a Billing Alarm]: https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/monitor_estimated_charges_with_cloudwatch.html
[Create a Free Tier Billing Alert]: https://aws.amazon.com/about-aws/whats-new/2017/12/aws-free-tier-usage-alerts-automatically-notify-you-when-you-are-forecasted-to-exceed-your-aws-service-usage-limits/



------

#### S3 101:

***S3 has the following features:***

* Tiered Storage Available
* Life-cycle Management (move the files after X days automatically)
* Versioning
* Encryption
* MFA Delete (if you activate this. you only can delete files with MFA)
* Secure your data using ACL and Bucket Policies
  

***S3 Charges***

- Storage
- Requests
- Storage Management Pricing (storage classes)
- Data Transfer Pricing
- Transfer Acceleration (Using Cloudfront, its faster over long distances between your end users and an S3 bucket)
- Cross Region Replication Pricing (Automate copy between regions)
  

***Exam Tips***

- it is object-bases (Just as files) storage, allows you to upload files
- Files can be from 0 bytes to 5 TB
- There is unlimited store
- Files are stored in Buckets (Buckets are like directories in Linux)
- S3 is a universal namespace, that is, names must be unique. The bucket have a unique url, for example https://s3-ue-west-1.amazonaws.com/nahuelhernandez
- Not suitable to install an OS or a DB
- Successful uploads will generate a HTTP 200 status code
- You  can turn on MFA Delete
- S3 has eventual consistency for HTTP Methods: overwrite PUTS and DELETES
- Per account by default you can have 100 buckets



***Objects consist of the following:***

- Key (The name)
- Value (Data, sequence of bytes)
- Version ID (Important for versioning)
- Metadata (Data about data you are storing)
- Subresources (ACL and Torrent)



***How Does data consistency work for S3?***

- Read after write consistency for PUTS of new Objects
- Eventual Consistency for overwrite PUTS and DELETES (can take time to propagate)

In others words:
If you write a new file and read it immediately after reads, you will be able to view that data. If you update AN EXISTING file or delete a file and read immediately, you may get the older version, or you may not. 



***S3 Storage Classes (6)***

- S3 Standard 99,99% availability, 99,11^9 durability, stored redundantly across multiple devices in multiples facilities, and is designed to sustain the loss of 2 facilities concurrently.
- S3 IA (Infrequently Accessed) data IA, but requires rapid access when needed. Lower fee than S3, but you are charged a retrieval fee
- S3 ONE ZONE IA 99,5% Lower cost than IA but dont require data resilience (is the update for S3 RRS)
- S3 INTELLIGENT TIERIENG Designed to optimize cost by automatically moving data to the most cost-effective access tier without performance impact or operational overhead (Using Machine Learning)
- S3 Glacier is secure, durable, low-cost, but retrieval times configurable from minutes to hours (Is for years) is super cheaps.
- S3 Glacier Deep archive is the lowerst cost storage class where a retrieval time of 12 hours is acceptable
- S3 RRS DEPRECATED

For the exam you need to know "First byte latency", in S3 miliseconds, in S3 Glacier minuts or hours



**Remember Read the S3 FAQs before Taking the examen:**
https://aws.amazon.com/s3/faqs/

------

#### Let's Create An S3 Bucket:

***Examen Tips***

* Buckets are a universal name space
* Upload an object to S3 Receive a HTTP 200 Code
* S3, S3-IA, S3-IA (One Zone), Glacier
* Control Access to buckets using either a bucket ACL or using Bucket Polices



------

#### S3 Security And Encryption:

***Exam Tips***

All newly created bucket are PRIVATE

* Bucket Policies
* ACL (Files level)

S3 buckets can be configured to create access logs which log all request to another bucket, and even to another bucket in another account.

* Encryption in transit is achieved by SSL/TLS

* Encryption at Rest (Server Side) is achieved by
  * S3 Managed Keys - SSE-S3 (only amazon)
  * AWS Key Management Service, Managed Keys - SSE-KMS (you and amazon)
  * Server Side Encryption With Customer Provided Keys - SSE-C (only you)
* Client Side Encryption



------

#### S3 Versioning - LAB

***Exam Tips***

* Store all versions of an object (including all writes and even if you delete an object)
* Great backup tool
* Once enabled, versioning cannot be disabled, only suspended
* Integrates with Lifecycle rules
* Versioning MFA Delete capability, which uses multi-factor authentication.



***Resources***

[AWS S3 Versioning]: https://docs.aws.amazon.com/AmazonS3/latest/dev/ObjectVersioning.html

 

******

#### Lifecycle Management with S3 - LAB

***Exam Tips***

* Automates moving your objects between the different storage tiers
* Can be used in conjunction with versioning
* Can be applied to current versions and previous versions



------

####   Cross Region Replication (CRR) - LAB

***Exam Tips***

* Versioning must be enabled on both the source and destination buckets
* Regions be must unique
* Files in an existing bucket are not replicated automatically
* All subsequent updated files will be replicated automatically
* Delete markers are no replicated
* Deleting individual versions or delete markers will not be replicated



***Resources***

[Docs: AWS S3 Replication Rules]: https://docs.aws.amazon.com/AmazonS3/latest/dev/crr-what-is-isnot-replicated.html
[APP: CRR Monitor]: https://aws.amazon.com/solutions/cross-region-replication-monitor/
[Docs: AWS S3 Region Codes]: https://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region

[DOCS: AWS S3 Replication Configuration]: https://docs.aws.amazon.com/AmazonS3/latest/dev/crr-add-config.html


------

#### S3 Transfer Acceleration

S3 Transfer Acceleration utilizes the CloudFront Edge Network to accelerate your uploads to S3. Instead of uploading directly to your S3 bucket, you can use a distinct URL to upload directly to an edge location which will then transfer that file to S3. You will get a distinct URL to upload.

***Resources***

[S3 Transfer Acceleation Speed Comparison]: https://s3-accelerate-speedtest.s3-accelerate.amazonaws.com/en/accelerate-speed-comparsion.html
[Turn On Trasfer Acceleration]: https://docs.aws.amazon.com/AmazonS3/latest/dev/transfer-acceleration.html#transfer-acceleration-getting-started



------

#### CloudFront Overview

***Exam Tips***

* Edge Location: This is the location where content will be cached. This is separate to an AWS Region/AZ
* Origin: This is the origin of all the files that the CDN will distribute. This can be either an S3 Bucket, an EC2 Instance, an Elastic Load Balancer, or Route53
* Distribution: This is the name given the CDN which consists of a collection of Edge Locations
* Web Distribution: Typically used for Websites
* RTMP: Used for Media Streaming
* Edge locations are not just READ only, yo can write to them to (ie put an object on to them, for example with Transfer Acceleration)
* Objects are cached for the life of the TTL (Time To Live)
* You can clear cached objects, but you will be charged

***Resources***

[DOCS: Cloudfront Key Features]: https://aws.amazon.com/cloudfront/features/



------

#### CloudFront Lab

https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/Invalidation.html



------

#### Snowball Overview

Snowball is for moving a lot of data into (or export) AWS Cloud 

***Exam Tips***

* Snowball can import/export to S3



------

#### Snowball DEMO

Optional, i can skip this lesson. 



------

#### Storage Gateway 

This part is mostly in sysops exam.

***Exam Tips***

* Storage Gateway It is a physical or virtual appliance that can be used to cache S3 locally at a customer's site.

* File Gateway

  * File Gateway: For flat files, stored directly on S3

* Volume Gateway
  * Stored Volumes: Entire Dataset is stored on site and is asynchronously backed up to S3
  * Cached Volumes: Entire Dataset is stored on S3 and the most frequently accessed data is cached on site

* Gateway Virtual Tape Library

  * Used for backup and uses popular backup applications like NetBackup, Exec, Veeam etc.

  

------

#### IAM & S3 Summary

Repeat this video.

Read the S3 FAQs before taking the exam, it comes up A LOT!
https://aws.amazon.com/s3/faqs/



------

#### IAM & S3 Quiz

1 Attempt -> %88

2 Attempt -> %95

**One of your users is trying to upload a 7.5GB file to S3. However, they keep getting the following error message: "Your proposed upload exceeds the maximum allowed object size.". What solution to this problem does AWS recommend?**

* Design your application to use the Multipart Upload API for all objects.

https://docs.aws.amazon.com/AmazonS3/latest/dev/UploadingObjects.html



**S3 has eventual consistency for which HTTP Methods?**

* overwrite PUTS and DELETES



**What is the availability of S3-OneZone-IA?**

99.50%

OneZone-IA is only stored in one Zone. While it has the same Durability, it may be less Available than normal S3 or S3-IA. Further information: https://aws.amazon.com/s3/storage-classes/?nc=sn&loc=3



**Which of the following options allows users to have secure access to private files located in S3? (Choose 3)**

* CloudFront Origin Access Identity
* CloudFront Signed Cookies
* CloudFront Signed URLs

There are three options in the question which can be used to secure access to files stored in S3 and therefore can be considered correct. Signed URLs and Signed Cookies are different ways to ensure that users attempting access to files in an S3 bucket can be authorised. One method generates URLs and the other generates special cookies but they both require the creation of an application and policy to generate and control these items. An Origin Access Identity on the other hand, is a virtual user identity that is used to give the CloudFront distribution permission to fetch a private object from an S3 bucket. Public S3 buckets should never be used unless you are using the bucket to host a public website and therefore this is an incorrect option. Further information: https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/PrivateContent.htmlhttps://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-signed-urls.htmlhttps://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-signed-cookies.htmlhttps://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-restricting-access-to-s3.html



**Using SAML (Security Assertion Markup Language 2.0), you can give your federated users single sign-on (SSO) access to the AWS Management Console.**

* True



**You run a popular photo sharing website that depends on S3 to store content. Paid advertising is your primary source of revenue. However, you have discovered that other websites are linking directly to the images in your buckets, not to the HTML pages that serve the content. This means that people are not seeing the paid advertising, and you are paying AWS unnecessarily to serve content directly from S3. How might you resolve this issue?**

* Remove the ability for images to be served publicly to the site and then use signed URLs with expiry dates.


**Power User Access allows ____.**

* Access to all AWS services except the management of groups and users within IAM.


**What is AWS Storage Gateway?**

* It is a physical or virtual appliance that can be used to cache S3 locally at a customer's site.

At its heart it is a way of using AWS S3 managed storage to supplement on-premise storage. It can also be used within a VPC in a similar way. Further information: https://aws.amazon.com/storagegateway/faqs/https://docs.aws.amazon.com/storagegateway/latest/userguide/WhatIsStorageGateway.htmlhttps://docs.aws.amazon.com/storagegateway/latest/userguide/HardwareAppliance.html


**You have been asked to advise on a scaling concern. The client has a elegant solution that works well. As the information base grows they use CloudFormation to spin up another stack made up of an S3 bucket and supporting compute instances. The trigger for creating a new stack is when the PUT rate approaches 100 PUTs per second. the problem is that as the business grows that number of buckets is growing into the hundreds and will soon be in the thousands. You have been asked what can be done to reduce the number of buckets without changing the basic architecture.**

* Change the trigger level to around 3000 as S3 can now accommodate much higher PUT and GET levels.

Until 2018 there was a hard limit on S3 puts of 100 PUTs per second. To achieve this care needed to be taken with the structure of the name Key to ensure parallel processing. As of July 2018 the limit was raised to 3500 and the need for the Key design was basically eliminated. Disk IOPS is not the issue with the problem. The account limit is not the issue with the problem. Further information: https://aws.amazon.com/about-aws/whats-new/2018/07/amazon-s3-announces-increased-request-rate-performance/https://docs.aws.amazon.com/AmazonS3/latest/dev/request-rate-perf-considerations.htmlhttps://aws.amazon.com/s3/storage-classes/



**How many S3 buckets can I have per account by default?**

100