18:40

What is the maximum VisibilityTimeout of an SQS message in a FIFO queue?

* 12 hours

The visibility timeout controls how long a message is invisible in the queue while it is being worked on by a processing instance. This interval should not be confused with how long the message can remain in the queue. Further information: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/APIReference/API_ChangeMessageVisibility.html



**When editing permissions (policies and ACLs), to whom does the concept of the "Owner" refer?**

* The "Owner" refers to the identity and email address used to create the AWS account.

The Owner concept comes into play especially when setting or locking down access to various objects. Further information: http://docs.aws.amazon.com/AmazonS3/latest/dev/UsingBucket.htmlhttp://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.htmlhttp://docs.aws.amazon.com/AmazonS3/latest/dev/amazon-s3-policy-keys.htmlhttps://blogs.aws.amazon.com/security/post/Tx2255RUMJGC96Y/Don-t-Forget-to-Enable-Access-to-the-Billing-Consolehttp://docs.aws.amazon.com/cli/latest/reference/ec2/describe-instances.html



**What is the maximum response time for a Business Level 'production down' Support Case?**

* 1 Hour

 https://aws.amazon.com/premiumsupport/

https://aws.amazon.com/premiumsupport/plans/



**Your company likes the idea of storing files on AWS. However, low-latency service of the majority of files is important to customer service. Which Storage Gateway configuration would you use to achieve both of these ends? (Choose 2)**

* Gateway-Stored
* File Gateways

Gateway-Stored volumes store your primary data locally, while asynchronously backing up that data to AWS. Depending on the Cache allocated you can achieve the same with File Gateway 

https://aws.amazon.com/storagegateway/details/

https://docs.aws.amazon.com/storagegateway/latest/userguide/resource-gateway-limits.html



**Your company has decided to set up a new AWS account for test and dev purposes. They already use AWS for production, but would like a new account dedicated for test and dev so as to not accidentally break the production environment. You launch an exact replica of your production environment using a CloudFormation template that your company uses in production. However, CloudFormation fails. You use the exact same CloudFormation template in production, so the failure is something to do with your new AWS account. The CloudFormation template is trying to launch 60 new EC2 instances in a single availability zone. After some research you discover that the problem is ____.**

* For all new AWS accounts, there is a soft limit of 20 EC2 instances per region. You should submit the limit increase form and retry the template after your limit has been increased.



**Which of the following strategies does AWS use to deliver the promised levels of DynamoDB performance? (Choose 2)**

* Data is stored on Solid State Disks.

* The Database is partitioned across a number of nodes.



**How is the Public IP address managed in an instance session via the instance GUI/RDP or Terminal/SSH session?**

* The Public IP address is not managed on the instance: It is, instead, an alias applied as a network address translation of the Private IP address.

AWS networking is implemented differently from most conventional data centers. Further information: http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-instance-addressing.html



**Which of the following are valid S3 data encryption options? (Choose 4)**

SSE-KMS.

Server Side Encryption (SSE)-S3.

SSE-C.

A client library such as Amazon S3 Encryption Client.

The valid ways of encrypting data on S3 are Server Side Encryption (SSE)-S3, SSE-C, SSE-KMS or a client library such as Amazon S3 Encryption Client. Further information: https://docs.aws.amazon.com/AmazonS3/latest/dev/UsingEncryption.html



**Your server logs are full of what appear to be application-layer attacks, so you deploy AWS Web Application Firewall. Which of the following conditions may you set when configuring AWS WAF? (Choose 3)**

* IP Match Conditions
* String Match Conditions
* Size Constraint Conditions

http://docs.aws.amazon.com/waf/latest/developerguide/getting-started.html#getting-started-wizard-create-web-acl