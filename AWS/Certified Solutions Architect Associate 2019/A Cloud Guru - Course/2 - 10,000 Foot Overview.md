### AWS - 10,000 Foot Overview

Region recomendada es US East (N. Virginia) porque es la region mas antigua, y donde salen las nuevas funcionalidades, tambien por eso es la que mas se cae, evitar usarla en prod.

Los temas criticos a ver son:

Compute
Storage
Network & Content Delivery
Databases
Security, Identity & compliance

**Exam Tips:**

`Diferencias entre "Region" "Availability Zone" (AZ) y "Edge Location"`

`Una region  es un lugar fisicio en el mundo que se componen de 2 o mas AZ`

`Un AZ es uno o mas data centers (cercanos), cada uno con power redundante, networking y conectividad,`

`Edge Location son endpoints de AWS que son usados para cachear contenido, tipicamente consiste en cloudfront, Amazon Content Delivery Network (CDN)`