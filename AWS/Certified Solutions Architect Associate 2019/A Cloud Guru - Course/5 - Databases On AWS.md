# Databases On AWS

#### Databases 101

***Exam Tips***

* RDS (OLTP)

  * SQL
  * MySQL
  * PostgresSQL
  * Oracle
  * Aurora
  * MariaDB

* DynamoDB (No SQL)

* Red Shift OLAP (Business Intelligence or Data Warehousing)

* Elasticache to speed up performance of existing dbs (frequent identical queries)

  * Memcache
  * Redis

* Two ways to increase your performance, add RR or add Elasticache Service

  

__________

#### Let's Create our first RDS Instance Lab

***Exam Tips***

* RDS runs on VMs
* You cannot log in to these OS 
* Patching of the RDS OS and DB is Amazon's responsibility
* RDS is NOT serverless
* Aurora Serverless IS Serverless



__________

#### RDS Backups, Multi-AZ & Read Replicas

***Exam Tips***

- Multi-AZ is for DR Only, for more performance you need read replicas
- To improve performance you need to add read replicas (o add elastic cache)
- Read replicas are only used for scaling, not for DR
- Must have automatic backups turned on in order to deploy a read replica
- You can have up to 5 read replica copies of any database
- You can have RR of RR (but watch out for latency)
- Each RR wil have its own DNS end point
- You can have RR that have Multi-AZ
- You can create RR of Multi-AZ source dbs
- RR can be promoted to be their own db, this breaks the replication
- You can have a RR in a second region



______________________________

#### RDS Backups, Multi-AZ & Read Replicas - Lab

***Exam Tips***

* Backups of RDS: Automated Backups, Databases Snapshots
* Read Replicas
  * Can Be Multi-AZ
  * Used to increase performance
  * Must have backups turned on
  * Can be in different regions
  * Can be Aurora or MySQL
  * Can be promoted to master, this will break the RR
* MUltiAZ
  * Used for DR.
  * You can force a failover from one AZ to another by rebooting the RDS instance
* Encryption at rest is supported for MySQL, Oracle, SQL Server, PostgresSQL, MariaDB & Aurora, encryption is done using the AWS Key Management Service (KMS), if the RDS instance is encrypted the data stored at rest in the underlying storage is encrypted, as are its automated backups, read replicas and snapshots.



________________________

#### DynamoDB

***Exam Tips***

- Stored on SSD storage
- Spread across 3 geographically distinct data centres
- Eventual Consistent Reads (Defaults, updated in more that one second)
- Strongly Consistent Reads (if app need to updated in less that one second)



___________

#### Redshift

***Exam Tips***

- Redshift is used for Business Intelligent
- Only used in one AZ
- Backups are enabled by default with a 1 day (maximum 35)
- Redshift always attempts to maintain at least three copies of your data (the original and replica on the compute nodes and a backup in S3)
- Redshift can also asynchronously replicate your snapshots to S3 in another region for DR.



________

#### Amazon Aurora

***Exam Tips***

- 2 copies of your data is contained in each AZ, with minimum of 3 AZ, 6 copies of your data
- You can share Aurora Snapshots with other AWS accounts
- 2 types of replicas available. Aurora Replicas and MySQL replicas. Automated fail-over is only available with Aurora Replicas.
- Aurora has automated backups turned on by default. You can also take Snapshots with Aurora. You can share these snapshots with other AWS accounts.



__________

#### Elasticache

***Exam Tips***

- Is Memcached or Redis (More datatypes) 
- Use Elasticache to increase database and web application performance
- Redis is Multi-AZ
- You can do backups and restores of Redis
- If you need to scale horizontally, and something simple, use Memcached.

***Resources***

[DOC: Elasticache]: https://aws.amazon.com/elasticache/



------

#### Databases On AWS Quiz

First Attempt: %85

Second Attempt: %85



***Check:***

**Check differences between OLTP and OLAP**

Online Analytical Processing, a category of software tools which provide analysis of data for business decisions. OLAP systems allow users to analyze database information from multiple database systems at one time. REDSHIFT

Online transaction processing shortly known as OLTP supports transaction-oriented applications in a 3-tier architecture. OLTP administers day to day transaction of an organization. RDS



**Under what circumstances would I choose provisioned IOPS over standard storage when creating an RDS instance?**

* If you use online transaction processing in your production environment.

NO "If you have workloads that are not sensitive to latency/lag."



**Which of the following data formats does Amazon Athena support? (Choose 3)**

* Apache Parquet
* Apache ORC
* JSON

Amazon Athena is an interactive query service that makes it easy to analyse data in Amazon S3, using standard SQL commands. It will work with a number of data formats including "JSON", "Apache Parquet", "Apache ORC" amongst others, but "XML" is not a format that is supported. Further information: <https://aws.amazon.com/athena/faqs/>



**If you want your application to check RDS for an error, have it look for an "___" node in the response from the Amazon RDS API.**

* Error



**With new RDS DB instances, automated backups are enabled by default?**

* True



**What data transfer charge is incurred when replicating data from your primary RDS instance to your secondary RDS instance?**

* There is no charge associated with this action.



**In RDS, what is the maximum value I can set for my backup retention period?**

* 35 Days




**Which of the following DynamoDB features are chargeable, when using a single region? (Choose 2)**

* Storage of Data
* Read and Write Capacity

There will always be a charge for provisioning read and write capacity and the storage of data within DynamoDB, therefore these two answers are correct. There is no charge for the transfer of data into DynamoDB, providing you stay within a single region (if you cross regions, you will be charged at both ends of the transfer.) There is no charge for the actual number of tables you can create in DynamoDB, providing the RCU and WCU are set to 0, however in practice you cannot set this to anything less than 1 so there always be a nominal fee associated with each table. Further information: <https://aws.amazon.com/dynamodb/pricing/>



**If you are using Amazon RDS Provisioned IOPS storage with a Microsoft SQL Server database engine, what is the maximum size RDS volume you can have by default?**

* 16TB

https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Storage.html#USER_PIOPS



**How many copies of my data does RDS - Aurora store by default?**

* 6