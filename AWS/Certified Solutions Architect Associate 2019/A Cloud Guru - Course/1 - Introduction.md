### The Exam Blue Print

* 130 minutos de duracion

* Aprox 65 preguntas

* Multiple Choice

* Las preguntas son en base a posibles escenarios

* Necesitas al menos el %70 para aprobar

* Puntaje entre 100 - 1000, aprobas con 720

* Dura 3 años la certificacion

Info sobre el examen oficial:

https://d1.awsstatic.com/training-and-certification/docs-sa-assoc/AWS_Certified_Solutions_Architect_Associate_Feb_2018_%20Exam_Guide_v1.5.2.pdf

