# AWS Certified SysOps Administrator Associate Practice Tests

65 questions | 2 hours 10 minutes | 72% correct required to pass



## AWS Certified SysOps Administrator Associate Practice Test 1 - Results

Attempt 1: Failed 70% correct (46/65) 1 hour 24 minutes September 1, 2019 1:10 AM

* AWS EB CLI cannot create the instance profile for your beanstalk environment if your IAM role has no access to creating roles. **You don't need associate a EB role to your CLI because assumes the role in your account**



* **vault lock policy** (or simple Vault Lock) is different than a **vault access policy**. Both policies govern access controls to your vault, however, a **vault lock policy** can be locked to prevent future changes, providing strong enforcement for your compliance controls compared to a regular vault access policy.
  In contrast, you use a vault access policy to implement access controls that **are not compliance related**, temporary, and subject to frequent modification. 



* **IPv6 is supported on all current generation instance types and the C3, R3, and I2 previous generation instance types** If your instance type does not support IPv6, you must resize the instance to a supported instance type. In the example above, the database instance is an `m3.large` instance type, which does not support IPv6. You must resize the instance to a supported instance type, for example, `m4.large`.



* Create an Internet-facing load balancer and register the web servers with it. Create an internal load balancer and register the database servers with it. The web servers receive requests from the Internet-facing load balancer and send requests for the database servers to the internal load balancer. The database servers receive requests from the internal load balancer.



* If you need to generate a report on the replication and encryption status of all of the obhects stored in S3 use **Amazon S3 inventory** is one of the tools Amazon S3 provides to help manage your storage. You can use it to audit and report on the replication and encryption status of your objects for business, compliance, and regulatory needs. In contrast, **S3 Select** is only used to retrieve specific data from the contents of an object using simple SQL expressions without having to retrieve the entire object. It does not generate a detailed report, unlike S3 Inventory.



* **Geoproximity routing** lets Amazon Route 53 route traffic to your resources based on the geographic location of your users and your resources. You can force, is not like Latency. Geoproximity is different that Geolocation. Latency routing is when you have **resources in multiple AWS Regions** and you want to route traffic to the region that provides the best latency.

  **Geographic proximity not always grantee low latency.**

* https://tutorialsdojo.com/aws-cheat-sheet-latency-routing-vs-geoproximity-routing-vs-geolocation-routing/

  

* The `terminate-instance-in-auto-scaling-group` CLI command terminates the specified instance and optionally adjusts the desired group size. This call simply makes a termination request so the instance is not terminated immediately.

  **This command has a required parameter** which indicates whether terminating the instance also decrements the size of the Auto Scaling group:

  ```
  --should-decrement-desired-capacity | --no-should-decrement-desired-capacity (boolean)
  ```



* A flow log record is a space-separated string that has the following format:

   `<version> <account-id> <interface-id> <srcaddr> <dstaddr> <srcport> <dstport> <protocol> <packets> <bytes> <start> <end> <action> <log-status>`

  

* Check Disk Classes https://tutorialsdojo.com/aws-cheat-sheet-amazon-ebs/



* ELB and the Auto Scaling group need to create in the same Network. Make sure that both are in VPC or in EC2-Classic (Is the original VPC Used by Amazon RDS you can have a LB in it, EC2-Classic run in a single flat network that is shared with other customers)
  
* Snowmobile is more suitable in migrating large datasets of 10PB or more in a single location. For datasets less than 10PB or distributed in multiple locations, you should use Snowball. For example if you need migrate 200 TB, you need to use Snowball. 



* Elastic Load Balancing provides access logs that capture detailed information about requests sent to your load balancer. Each log contains information such as the time the request was received, the client's IP address, latencies, request paths, and server responses. You can use these access logs to analyze traffic patterns and troubleshoot issues. When you enable access logging, you must specify an S3 bucket for the access logs. The bucket must be located in the same region as the load balancer.



* Check Disk Provisioned IOPS https://tutorialsdojo.com/aws-cheat-sheet-amazon-ebs/



* DynamoDB Auto Scaling uses the AWS Application Auto Scaling service to dynamically adjust provisioned throughput capacity on your behalf, in response to actual traffic patterns. This enables a table or a global secondary index to increase its provisioned read and write capacity to handle sudden increases in traffic, without throttling. When the workload decreases, Application Auto Scaling decreases the throughput so that you don’t pay for unused provisioned capacity.



* Web identity federation – You can let users sign in using a well-known third party identity provider such as Login with Amazon, Facebook, Google, or any OpenID Connect (OIDC) 2.0 compatible provider. You can exchange the credentials from that provider for temporary permissions to use resources in your AWS account. This is known as the web identity federation approach to temporary access.

  When you use web identity federation for your mobile or web application, you don't need to create custom sign-in code or manage your own user identities. Using web identity federation helps you keep your AWS account secure, because you don't have to distribute long-term security credentials, such as IAM user access keys, with your application.

  

* The Internet gateway is used in AWS to connect your VPC to the outside world, the Internet. The virtual private gateway is used to connect via VPN connection to your on-premises area. 



## AWS Certified SysOps Administrator Associate Practice Test 2 - Results

Attempt 1: Failed 69% correct (45/65) 1 hour 33 minutes September 1, 2019 11:17 AM



* For an EC2 instance to be able to communicate to the Internet over IPv6, the following configuration should be done in the VPC:

  - -Associate a /56 IPv6 CIDR block with the VPC. The size of the IPv6 CIDR block is fixed (/56) and the range of IPv6 addresses is automatically allocated from Amazon's pool of IPv6 addresses (you cannot select the range yourself).
  - -Create a subnet with a /64 IPv6 CIDR block in your VPC. The size of the IPv6 CIDR block is fixed (/64).
  - -Create a custom route table, and associates it with your subnet, so that traffic can flow between the subnet and the Internet gateway.

  

* Every subnet that you create is automatically associated with the main route table for the VPC. Because of this If you have 2 subnet in the same VPC you don't need configure the route tables. it does not matter whether the subnet is public or private as long as they both reside in one VPC. Take note that a public subnet basically means that it has a route to the Internet Gateway and a private subnet does not. 



* Amazon S3 ACL: Enable you to manage access to buckets and objects. The `WRITE` ACL permission allows grantee to create, overwrite, and delete any object in the bucket and `WRITE_ACP` allows grantee to write the ACL for the applicable bucket.



* You can aggregate the metrics for AWS resources across multiple resources. Amazon CloudWatch cannot aggregate data across Regions. Metrics are completely separate between Regions. Take note that you can monitor AWS resources in multiple Regions using a single CloudWatch dashboard, but you cannot **aggregate** the data across Regions.



* You can use an AWS Direct Connect gateway to connect your AWS Direct Connect connection over a private virtual interface to one or more VPCs in your account that are located in the same or different regions. You associate a Direct Connect gateway with the virtual private gateway for the VPC, and then create a private virtual interface for your AWS Direct Connect connection to the Direct Connect gateway. 



* S3 analytics is a useful tool for analyzing storage access patterns to help you determine when to transition less frequently accessed Standard storage to the IA storage class. Once you see the access patterns in the data, you can then set a lifecycle policy which will transfer the contents to Glacier.



* **DB security groups are for database instances not in a VPC, but in an EC2-Classic platform** (VPC Security Group is the correct)



* Auto Scaling group can't contain EC2 instances from multiple Regions. Only in the same Region with multi AZ

* You can create a load balancer with the following security features (SSL):

  - -SSL Server Certificates
  - -SSL Negotiation
  - -Back-End Server Authentication

  

* You can associate secondary IPv4 CIDR blocks with your VPC to increase its size. When you associate a CIDR block with your VPC, a route is automatically added to your VPC route tables to enable routing within the VPC (the destination is the CIDR block and the target is local).



* The first entry is the default entry for local IPv4 routing in the VPC; this entry enables the instances in this VPC to communicate with each other. The second entry routes all other IPv4 subnet traffic to the Internet gateway (igw-1a2b3c4d) which should have a value of `0.0.0.0/0` for its destination.



* Classic LB have the option for cross-zone load balancing



* In AWS, you assign a single Classless Internet Domain Routing (CIDR) IP address range as the primary CIDR block when you create a VPC and can add up to four (4) secondary CIDR blocks after creation of the VPC. In this scenario, you already have 4 secondary CIDRs and all of the IP addresses are already allocated which means that you cannot create secondary CIDRs anmore.



* Auto Scaling group cannot scale out to other AWS regions. **Important!**



## AWS Certified SysOps Administrator Associate Practice Test 3 - Results

Attempt 1: Passed 75% correct (49/65) 2 hour 30 minutes September 2, 2019 12:40 PM



* Access log en ELB is disable by default. After you enable access logging for your load balancer, Elastic Load Balancing captures the logs and stores them in the Amazon S3 bucket that you specify



* For Redshift, by default, automated backups are enabled for the data warehouse cluster with a 1-day retention period. It provides free storage for snapshots that is equal to the storage capacity of your cluster until you delete the cluster. After you reach the free snapshot storage limit, you are charged for any additional storage at the normal rate. **S3 has a lifecycle policy but not a backup policy. Standard S3 is already very durable and AWS has no options for automatic backup on S3.** 



* Instance store volumes are only temporary storages, which is useful for frequently changing information.



* If you have an HTTPS listener, you deployed an SSL server certificate on your load balancer when you created the listener. Each certificate comes with a validity period. You must ensure that you renew or replace the certificate before its validity period ends. You can replace the certificate deployed on your load balancer with a certificate provided by ACM or a certificate uploaded to IAM. 

  To replace an SSL certificate with a certificate uploaded to IAM:

  1. Use the [get-server-certificate](https://docs.aws.amazon.com/cli/latest/reference/iam/get-server-certificate.html) command to get the ARN of the certificate:

  ```
  aws iam get-server-certificate --server-certificate-name my-new-certificate
  ```

  2. Use the [set-load-balancer-listener-ssl-certificate](https://docs.aws.amazon.com/cli/latest/reference/elb/set-load-balancer-listener-ssl-certificate.html) command to set the certificate. For example:

  ```
  aws elb set-load-balancer-listener-ssl-certificate --load-balancer-name my-load-balancer --load-balancer-port 443 --ssl-certificate-id arn:aws:iam::123456789012:server-certificate/my-new-certificate
  ```



* Amazon Cognito identity pools assign your authenticated users a set of temporary, limited privilege credentials to access your AWS resources. The permissions for each user are controlled through IAM roles that you create. You can define rules to choose the role for each user based on claims in the user's ID token. You can define a default role for authenticated users. You can also define a separate IAM role with limited permissions for guest users who are not authenticated. With this users can log in to their profile using any social media account, and for example use an application that write on a DynamoDB table.



* AWS Personal Health Dashboard alerts are triggered by changes in the health of AWS resources and  give you remediation guidance to help quickly diagnose and resolve issues when AWS is experiencing events that may impact you. While the Service Health Dashboard displays the general status of AWS services.



* CloudWatch only monitors the health of the resources that you own based on certain metrics but it does not check the underlying hardware that hosts the AWS resources.



* https://tutorialsdojo.com/aws-cheat-sheet-aws-systems-manager/



* When you need to make changes to a stack's settings or change its resources, you update the stack instead of deleting it and creating a new stack. For example, if you have a stack with an EC2 instance, you can update the stack to change the instance's AMI ID. When you update a stack, you submit changes, such as new input parameter values or an updated template. AWS CloudFormation compares the changes you submit with the current state of your stack and updates only the changed resources.



* You can create a CloudWatch alarm that watches a single metric. The alarm performs one or more actions based on the value of the metric relative to a threshold over a number of time periods. The action can be an Amazon EC2 action, an Amazon EC2 Auto Scaling action, or a notification sent to an Amazon SNS topic. You can also add alarms to CloudWatch dashboards 


  **Important:**

*  TCO is for comparing the cost of your applications in an on-premises or traditional hosting environment to AWS. 



* AWS Simple Monthly Calculator only helps customers and prospects estimate their monthly AWS bill more efficiently, but does not generate reports as detailed as the cost optimization monitor. It also does not monitor current costs that you are incurring.



* The Cost Optimization Monitor can help you generate reports that provide insight into service usage and costs as you deploy and operate cloud architecture. They include detailed billing reports, which you can access in the AWS Billing and Cost Management console.



## AWS Certified SysOps Administrator Associate Practice Test 4 - Results

Attempt 1: Passed 73% correct (48/65) 1 hour 17 minutes September 3, 2019 2:17 AM



* The `delete-vpc` CLI command deletes the specified VPC. You must detach or delete all gateways and resources that are associated with the VPC before you can delete it.

  For example, you must terminate all instances running in the VPC, delete all security groups associated with the VPC (except the default one), delete all route tables associated with the VPC (except the default one), and so on.

  

* **Provisioned IOPS SSD** volumes are designed to meet the needs of I/O-intensive workloads, particularly database workloads, that are sensitive to storage performance and consistency. It is the highest-performance SSD volume for mission-critical low-latency or **high-throughput workloads.**



* Temporary credentials in AWS. Temporary credentials are useful in scenarios that involve identity federation, delegation, cross-account access, and IAM roles. In this example, it is called **enterprise identity federation** considering that you also need to set up a single sign-on (SSO) capability.



* With step scaling policies, you can specify the number of seconds that it takes for a newly launched instance to warm up. Until its specified warm-up time has expired, an instance is not counted toward the aggregated metrics of the Auto Scaling group. While scaling out, AWS also does not consider instances that are warming up as part of the current capacity of the group.



* You can configure Amazon Redshift to automatically copy snapshots (automated or manual) for a cluster to another region. When a snapshot is created in the cluster’s primary region, it will be copied to a secondary region; these are known respectively as the source region and destination region.
  **Is no Cross-Region Replication (CRR) feature that is readily available in Redshift**. Take note that the CRR feature is quite different from Cross-Region Snapshots Copy in which the former refers to an S3 feature while the latter is the one for Redshift



* To determine whether a log file was modified, deleted, or unchanged after CloudTrail delivered it, you can use CloudTrail log file integrity validation. This feature is built using industry standard algorithms: SHA-256 for hashing and SHA-256 with RSA for digital signing. This makes it computationally infeasible to modify, delete or forge CloudTrail log files without detection. You can use the AWS CLI to validate the files in the location where CloudTrail delivered them.



* **ELB does not support load balancing across multiple region**. You have to use Route53 to route requests across multiple AWS regions.



* **Copy automatically snapshots EBS to another region:** Amazon EBS emits notifications based on Amazon CloudWatch Events for a variety of snapshot and encryption status changes. With CloudWatch Events, you can establish rules that trigger programmatic actions in response to a change in snapshot or encryption key state. For example, when a snapshot is created, you can trigger an AWS Lambda function to share the completed snapshot with another account or copy it to another region for disaster-recovery purposes



* If you need to remove a file from CloudFront edge caches before it expires, you can do one of the following:

  - Invalidate the file from edge caches. The next time a viewer requests the file, CloudFront returns to the origin to fetch the latest version of the file.
  - Use file versioning to serve a different version of the file that has a different name.

  

* AWS Storage Gateway supports the Amazon S3 Standard, Amazon S3 Standard-Infrequent Access, Amazon S3 One Zone-Infrequent Access and Amazon Glacier storage classes. When you create or update a file share, you have the option to select a storage class for your objects. You can either choose the Amazon S3 Standard or any of the infrequent access storage classes such as S3 Standard IA or S3 One Zone IA. Objects stored in any of these storage classes can be transitioned to Amazon Glacier using a Lifecycle Policy.

  Although you can write object directly from a file share to the S3-Standard-IA or S3-One Zone-IA storage class, it is recommended you use a Lifecycle Policy to transition your objects rather than write directly from the file share, especially if you're expecting to update or delete the object within 30 days of archiving it.

  

* You manage access in AWS by creating policies and attaching them to IAM identities or AWS resources. A policy is an object in AWS that, when associated with an entity or resource, defines their permissions. AWS evaluates these policies when a principal, such as a user, makes a request. Permissions in the policies determine whether the request is allowed or denied.



* Aurora Auto Scaling dynamically adjusts the number of Aurora Replicas provisioned for an Aurora DB cluster. Aurora Auto Scaling enables your Aurora DB cluster to handle sudden increases in connectivity or workload



## AWS Certified SysOps Administrator Associate Practice Test 5 - Results

Attempt 1: Fail 67% correct (44/65) 1 hour 26 minutes September 3, 2019 9:17 PM



* **The following limitations exist for Amazon RDS encrypted DB instance:**

  - You can only enable encryption for an Amazon RDS DB instance when you create it, not after the DB instance is created.

    However, because you can encrypt a copy of an unencrypted DB snapshot, you can effectively add encryption to an unencrypted DB instance. That is, you can create a snapshot of your DB instance, and then create an encrypted copy of that snapshot. You can then restore a DB instance from the encrypted snapshot, and thus you have an encrypted copy of your original DB instance. For more information, see [Copying a Snapshot](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_CopySnapshot.html).

  - DB instances that are encrypted can't be modified to disable encryption.

  - You can't have an encrypted Read Replica of an unencrypted DB instance or an unencrypted Read Replica of an encrypted DB instance.

  - Encrypted Read Replicas must be encrypted with the same key as the source DB instance.

  - You can't restore an unencrypted backup or snapshot to an encrypted DB instance.

  - To copy an encrypted snapshot from one AWS Region to another, you must specify the KMS key identifier of the destination AWS Region. This is because KMS encryption keys are specific to the AWS Region that they are created in.

  

* If after creating an AWS user account for the vendor, you want to restrict their access to specific AWS resources that they only need using an IAM policy and only for 2 weeks. You need to use **An inline policy** best for individual users, for groups the best is **Aws Managed Policy**



* If you need to create a EBS snapshot an move to another region you need to do manual. 
  Although the Amazon Data Lifecycle Manager (DLM) for EBS Snapshots provides a simple, automated way to back up data stored on Amazon EBS volumes, it does not have the capability to create a snapshot and automatically move it to another region. 



* **Sticky Sessions:** Elastic Load Balancing creates a cookie, named `AWSELB`, that is used to map the session to the instance.



* You can restore a DB instance to a specific point in time, creating a new DB instance. RDS uploads transaction logs for DB instances to Amazon S3 every 5 minutes. The maximum retention period possible for RDS databases is only 35 days 



*  You can create an Auto Scaling group directly from an EC2 instance. When you use this feature, Amazon EC2 Auto Scaling automatically creates a launch configuration for you as well.



* There are several reasons why you may need to set the backup retention period to 0. For example, you can disable automatic backups immediately by setting the retention period to 0. If you set the value to 0 and receive a message saying that the retention period must be between 1 and 35, check to make sure you haven't setup a read replica for the instance. Read replicas require backups for managing read replica logs, thus, you can't set the retention period of 0.



* Amazon Redshift logs information in the following log files:

  - **Connection log:** logs authentication attempts, and connections and disconnections.
  - **User log:** logs information about changes to database user definitions.
  - **User activity log:** logs each query before it is run on the database.

  

* Proxy Protocol is an Internet protocol used to carry connection information from the source requesting the connection to the destination for which the connection was requested. Elastic Load Balancing uses Proxy Protocol version 1, which uses a human-readable header format.

  By default, when you use Transmission Control Protocol (TCP) for both front-end and back-end connections, your Classic Load Balancer forwards requests to the instances without modifying the request headers. If you enable Proxy Protocol, a human-readable header is added to the request header with connection information such as the source IP address, destination IP address, and port numbers. The header is then sent to the instance as part of the request.

  

+ You cannot enable flow logs for network interfaces that are in the EC2-Classic platform.



+ You can simply check the Amazon RDS console to view if your RDS instance needs OS patching. Periodically, Amazon RDS performs maintenance on Amazon RDS resources. 



+ If you add a VPN connection between on premises and amazon VPC you need to add the route for the virtual private gateway in the amazon VPC subnet.

 