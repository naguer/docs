AWS Certified SysOps Administrator - Associate - Practice exam. 

Overall Score: 95%

Topic Level Scoring:
1.0  Monitoring and Reporting: 100%
2.0  High Availability: 100%
3.0  Deployment and Provisioning: 100%
4.0  Storage and Data Management: 100%
5.0  Security and Compliance: 75%
6.0  Networking: 100%
7.0  Automation and Optimization: 100%
