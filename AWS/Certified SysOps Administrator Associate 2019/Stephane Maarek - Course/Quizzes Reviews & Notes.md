

# Quizzes Reviews & Notes

### 3 EC2 for Sysops

#### Notes

* **Placement Groups**

  * **Cluster:** Low latency in a single AZ, Same Rack, (10 Gbps bandwidth between instances). Use case, Bigdata, or something needs High Network Throughput
  * **Spread:** Different HW (max 7 instances per AZ per partition groups), can span across Multiple AZs. Is for Critical Applications.
  * **Partition:** Spreads instances across many different partition, within an AZ. scale to 100S of EC2 instances per group (Hadoop, Cassandra, Kafka) (up to 7 partitions per AZ). A partition failure can effect many EC2 but wont affect other partitions

* **Termination Protection:** If we have an instance where shutdown behavior in "terminate" and enable termination protection, if we shutdown the instance from the OS the instance will still be terminated.

* **EC2 Launch Troubleshooting:**

  * **InstanceLimitExceeded error :** if you get this error, it means that you
    have reached your limit of max number of instances per region (default limit 20)
  * **InsufficientInstanceCapacity :** if you get this error, it means AWS does
    not have that much On-Demand capacity in the particular AZ to which
    the instance is launched.
  * **Instance Terminates Immediately** (goes from pending to terminated)
    * You've reached your EBS volume limit (The AWS Limits page forEBS lists the default limit being 5,000 EBS volumes and 10,000 EBS snapshots. Total storage limit is 20 TB per type (gp2, io12, st1, sc1))
    * An EBS snapshot is corrupt.
    * The root EBS volume is encrypted and you do not have permissions to access the KMS key for decryption.
    * The instance store-backed AMI that you used to launch the instance is missing a required part (an image.part.xx file).

* **EC2 SSH Troubleshooting**

  * "Unprotected Private Key File Error" problem with permission in the ssh files
  * "Host Key not found" (Or Permission Denied), incorrect OS user
  * "connection timeout", problem with the SG, or CPU load High

* **Instance Types:**

  * R: applications that needs a lot of RAM – in-memory caches
  * C: applications that needs good CPU – compute / databases
  * M: applications that are balanced (think “medium”) – general / web app
  * I: applications that need good local I/O (instance storage) – databases
  *   G: applications that need a GPU – video rendering / machine learning
  * T2 / T3: burstable instances (up to a capacity)
  * T2 / T3 - unlimited: unlimited burst

* **Cross  Account AMI Copy**

  * You can share an AMI with another AWS account.

  * Sharing an AMI does not affect the ownership of the AMI.

  * If you copy an AMI that has been shared with your account, you are the owner of the target AMI in your account.

    To copy an AMI that was shared with you from another account, the owner of the source AMI must grant you read permissions for the storage that backs the AMI, either the associated EBS snapshot
(for an Amazon EBS-backed AMI) or an associated S3 bucket (for an instance store-backed AMI).
    
  * Limits:
  
  * You can't copy an encrypted AMI that was shared with you from another account. Instead, if the
      underlying snapshot and encryption key were shared with you, you can copy the snapshot while re-encrypting it with a key of your own. You own the copied snapshot, and can register it as a new AMI.
  * You can't copy an AMI with an associated billingProduct code that was shared with you from another account. This includes Windows AMIs and AMIs from the AWS Marketplace. To copy a shared AMI with a billingProduct code, launch an EC2 instance in your account using the shared AMI and then create an AMI from the instance.
    
    
    
  
* **Elastic IPs**

  * When you stop and then start an EC2 instance, it changes its public IP, If you need to have a fixed public IP, you need an Elastic IP. An Elastic IP is a public IPv4 IP you own as long as you don’t delete it. You can attach it to one instance at a time. You can remap it across instances. You don’t pay for the Elastic IP if it’s attached to a server. **You pay for the Elastic IP if it’s not attached to a server Elastic IPs** 

  * With an Elastic IP address, you can mask the failure of an instance or software by rapidly remapping the address to another instance in your account.

  * You can only have 5 Elastic IP in your account (you can ask AWS to increase
    that).

  * Overall, try to avoid using Elastic IP:

    * Always think if other alternatives are available to you
    * You could use a random public IP and register a DNS name to it
    * Or use a Load Balancer with a static hostname

    

* **CloudWatch Metrics for EC2**

  * **AWS Provided Metrics (AWS pushes them)**
    * Basic Monitoring (default): metrics are collected at a 5 minute internal
    * Detailed Monitoring (paid): metrics are collected at a 1 minute interval
    * Includes CPU, Network, Disk and Status Check Metrics

  * **Custom metric (yours to push):**

    * Basic Resolution: 1 minute resolution
    * High Resolution: all the way to 1 second resolution
    * Include RAM, application level metrics
    * Make sure the IAM permissions on the EC2 instance role are correct

  * **EC2 included metrics**

    * CPU: Cpu Utilization + Credit Usage / Balance
    * Network: In / Out
    * Status Check:
      * Instance status = Check the EC2 VM
      * System status = Check the underlying Hardware
    * Disk: Read / Write for Ops / Bytes (only for instance store)
    * **RAM ins't included in the AWS Metrics**

  * **EC2 Custom Metrics**

    * Ram and Swap usage
    * Any custom metric for your app 

    

* **CloudWatch Logs for EC2**

  * By default no logs from your EC2 machine will go to Cloudwatch, you need to run a CloudWatch agent on Ec2 with te correct IAM permissions, useful for example to view a access.log in Cloudwatch, also work on premise.

  

______________

### 4 Managing EC2 at Scale - Systems Manager
**SSM & Opswork**

#### Notes

- **SSM:** Helps you automate management tasks such as collecting system inventory, applying operating system (OS) patches, automating the creation of Amazon Machine Images (AMIs), and configuring operating systems (OSs) and applications at scale. Systems Manager lets you remotely and securely manage the configuration of your managed instances. A *managed instance* is any Amazon EC2 instance or on-premises machine in your hybrid environment that has been configured for Systems Manager.
  - The most important features are **Parameter Store**, **Run Command and Patch Manager**
  - You need to install the Agent and the correct IAM role
  - **Parameter Store:** Secure storage for configs and secrets, optional encryption using KMS. Serverless, scalable, free. Version tracking of configuration and secrets. Notification with CloudWatch Events.
  - You can use Resource Groups to manage a logical group, (prod, dev, webapp, etc), for this you use AWS Tags. Resource Groups are a Regional service
  - Documents can be in JSON or YAML (is like playbooks in Ansible), 
  - **Run Command:** Execute a script or just run a command in one or multiple instances (using resource groups), integrated with IAM & CloudTrail, no need for SSH
  - **Patch:** 
    - Inventory: List software on an instance
    - Inventory + Run Command: Patch Software
    - Patch manager + Maintenance Windows: Patch OS
    - Patch manager: Gives you compliance
    - State manager: Ensure instances are in a consist state (compliance)
- **Session Manager:** Allows you to start a secure shell on your VM, *Does not use SSH access and bastion hosts* . Only on EC2 for now, not on Premise. Log actions done through secure shells to S3 and CloudWatch Logs. CloudTrail can intercept StartSession events. All connection are with User Iam, not SSH keys
- **AWS Opsworks:** Managed Chef & Puppet, its an alternative to SSM. Work cross-cloud



__________________

### 5 EC2 High Availability and Scalability

#### Notes

- **Load Balancer:** All LB (clb, alb, nlb) has a static host name, do not resolve directly IP
  - LBs can scale but not instantaneously, you need to open a ticket to the AWS support for a "warm-up". Prewarm your LB is for ALB, CLB (Not for NLB)
  - **SSL Termination:** Encryption of the connection is between the clients and the ELB, and later the ELB talk directly to your instances in HTTP traffic
  - If the LB cant connect to your app check your SG
  - **Stickiness:** For CLB and ALB, the cookie used for this has an expiration date you control (60 segs default, minimum 1 seg, max 7 days). Enabling stickiness may bring imbalance to the load over the EC2 instances 
  - **Supporting SSL for Old Browsers:** If you have Legacy browsers that has an old TLS (as TLS 1.0), you need to change the policy to allow for **weaker cipher**
  - **Erros:** 4xx errors are client induced errors, 5xx errors are app induced erros. LB errors 503 means at capacity or no registered target
  - All Load Balancers are integrated with CloudWatch metrics, that you can enable access logs that go directly into S3 encrypted, and that there is some kind of tracing enabled through headers, for this you need added custom header ‘X-Amzn-Trace-Id’
  - **LB Monitoring:**
    - **SurgeQueueLength:** The total number of requests (HTTP listener) or connections (TCP listener) that are pending routing to a healthy instance. Help to scale out ASG. Max value is 1024
    - **SpilloverCount:** The total number of requests that were rejected because the surge queue is full.
  - **ALB**: are V2 and Layer7. Alb can LB across multiple apps on the same machine (ex containers). ALb are great for microservices. Stickiness are generated by the ALB (not the application). Support Http, https, & Websockets. The application servers don't see the ip of the client (true ip of the client is inserted in the header **X-Forwarded-For**). Alb can route on based on hostname / path.
    **If you need in your ALB put a static ips, you need to combine with the NLB.**
  - **NLB**: are V2 and Layer4. Forward TCP traffic to your instances, handle millions of requests per seconds, support for static IP or EIP. Less latency (100 ms vs 400ms for ALB). Use for extreme performance. NLB directly see the client IP. **No pre-warming needed.** 1 static IP per subnet. **No SSL termination (ssl must be enabled by the application itself)**
- **ASG:** it is possible to scale an ASG based on CloudWatch alarms, auto scaling new rules (target average cpu usage, number of request on the Elb per instance, net in-out), also we can auto scale based on a custom metric (ex: number of connected users). Iam roles attached to an ASG will get assigned to EC2 instances. ASG can terminate instances marked as unhealthy by an LB.
  **Scaling Process in ASG:**
  - Launch: Add a new EC2 to the group, increasing the capacity
  - Terminate: Removes an EC2 instance from the group, decreasing its capacity.
  - HealthCheck: Checks the health of the instances
  - ReplaceUnhealthy: Terminate unhealthy instances and re-create them
  - AZRebalance: Balancer the number of EC2 instances across AZ
  - AlarmNotification: Accept notification from CloudWatch
  - ScheduledActions: Performs scheduled actions that you create.
  - AddToLoadBalancer: Adds instances to the load balancer or target group
- **AZRebalance:** Launch new instance then terminate old instance. If you suspend the launch process, AZRebalance won't launch or terminate instances. If you **suspend the terminate** process, the **ASG can grow up to 10% of this size** (it’s allowed during rebalances). The ASG could remain at the increased capacity as it can’t terminate instances.
  If you suspend the Terminate process, your Auto Scaling group can grow up to ten percent larger than its maximum size, because this is allowed temporarily during rebalancing activities. If the scaling process cannot terminate instances, your Auto Scaling group could remain above its maximum size until you resume the Terminate process.
- To make sure you have high availability, means you have least 2 instances
  running across 2 AZ in your ASG (must configure multi AZ ASG)
- Health Checks Available: Ec2 status checks, or ELB health checks
- Good to Know CLI: set-instance-health, terminate-instance-in-auto-scaling-group



_____________

### 6 Elastic Beanstalk for SysOps

#### Notes

**Three architecture models:**

* Single Instance deployment: good for dev

* LB + ASG: great for production or pre-production web applications
* ASG only: great for non-web apps in production (workers, etc..)

Has tree components:

* Application
* Application Version: Each deployment get assigned a version
* Environment name (dev, test, prod, etc)

**You create an Application -> Create an environment ->  Upload a version (Giving an alias) -> Release to environment from Alias**

**Deployments options for updates:**

• All at once (deploy all in one go) – fastest, but instances aren't available to serve traffic for a bit (downtime)
• Rolling: update a few instances at a time (bucket), and then move onto the next bucket once the first bucket is healthy
• Rolling with additional batches: like rolling, but spins up new instances to move the batch (so that the old application is still available)
• Immutable: spins up new instances in a new ASG, deploys version to these instances, and then swaps all the instances when everything is healthy

Blue/Green: Not a "Direct feature", is like Immutable, create a new "stage" environment and deploy v2, using beanstalk use "Swap Urls". A lot of manual work.

***Beanstalk for Sysops*** if you have a application taking a **long time resolving** dependencies, you need to **create a Golden Ami** (Os dependencies, apps dependencies, software base)



________

### 7 CloudFormation for Sysops

#### Notes

- Templates components (one course section for each):
  - Resources: your AWS resources declared in the template (MANDATORY)
  - Parameters: the dynamic inputs for your template (useful to reuse your templates) !Ref
  - Mappings: the static variables for your template. For e.g. to different envs. 
  - Outputs: References to what has been created
  - Conditionals: List of conditions to perform resource creation
  - Metadata
- **Pseudo Parameters:** these can be used at any time (accountid, region, etc)
- **Functions:** !Sub is used to substitute variables from a test
- **Sysops Functions:**
  
  - We can have user data at EC2 instance launch through the console, the important part is past in the script the function **Fn::Base64** , user data log is in **/var/log/cloud-init-output.log**
  - **cnf-init** : is like the User Data but with another more readable language, **AWS::CloudFormation::Init** must be in the **Metadata** of a resource. Logs go to **/var/log/cfn-init.log**
  - **cfn-signal & wait conditions:** We can run cfn-signal after cfn-init, this tell CloudFormation service to keep on going or fail. We need to define **WaitCondition** this block the template until it receives a singal from cfn-signal, we attach a **CreationPolicy** (also works on EC2, ASG)
  - **Rollbacks on failures:** Stack creation fails, 3 options, Rollback (default), Do_nothing (useful to manually troubleshot), delete
  - **Nested Stacks:** This are stacks as part of other stacks, they allow you to isolate repeated patterns. E.G lb config that is re used, or sg thatis re used. Nested stacks are considered Best practice. 
    **To update a nested stack, always update the parent** 
  - **ChangeSets:** When you update a stack, you need to know what changes before it happens for rather confidence. You create a change set by submitting changes against the stack you want to update. CloudFormation compares the stack to the new template and/or parameter values and produces a change set that you can review and then choose to apply (execute).
  - **Retains Data on Delete:**
    
    - DeletionPolicy=Retain:
      
      - Specify on resources to preserve / backup in case of CloudFormation deletes
      - To keep a resource, specify Retain (works for any resource / nested stack)
      
    - DeletionPolicy=Snapshot:
      - EBS Volume, ElastiCache Cluster, ElastiCache ReplicationGroup
      - RDS DBInstance, RDS DBCluster, Redshift Cluster
      
    - DeletePolicy=Delete (default behavior):
      - Note: for AWS::RDS::DBCluster resources, the default policy is Snapshot
      - Note: to delete an S3 bucket, you need to first empty the bucket of its content
      
      

______________

### 8 EC2 Storage and Data Management - EBS and EFS
#### Notes

* An EBS is a network drive you can attach to your instances. It's locked to an AZ. To move a volume across, you first need to snapshot it. Have a provisioned capacity (GBs and IOPS)

  * **GP2 (SSD):** General purpose SSD volume that balances price and performance for a wide variety of workloads. **Can be boot. Development and test envs.** 
    1 GiB to 16 TiB, can burst IOPS to 3000. Max IOPS is 16k
  * **IO1 (SSD):** Highest-performance SSD volume for mission-critical low-latency or high-throughput workloads. **Can be boot. Is useful for example in DBs.** 
    4 GiB to 16 TiB. MIN 100 - MAX 64,000 (Nitro instances) else MAX 32,000 (other instances). Max ratio in GiB is 50:1 for IOPS
  * **ST1 (HDD):** Low cost HDD volume designed for frequently accessed, throughput-intensive workloads. Useful for streaming workloads, big data, data warehouses, log processing, kafka, etc. 500 GiB to 16 TiB, Max IOPS is 500. Max Throughput 500 MiB/s, can burst.
  * **SC1 (HDD):** Lowest cost HDD volume designed for less frequently accessed workloads. Scenarios where the lowest storage cost is important. 500 GiB to 16 TiB, Max IOPS is 250. Max Throughput 250 MiB/s, can burst.

* **Gp2 Volume I/O Burst**: If your gp2 volume is less than 1000 GiB (means IOPS less than 3000) it can “burst” to 3000 IOPS performance. Burst concept also Applies to ST1 or SC1 (for increase in throughput)

* **Computing MB/s based on IOPS**

  * GP2:
    * Throughput in MiB/s = (Volume size in GiB) × (IOPS per GiB) × (I/O size in KiB)
    * ex: 300 I/O operations per second * 256 KiB per I/O operation = 75 MiB/s
    * Limit to a max of 250 MiB/s (means volume >= 334 GiB won’t increase throughput)
  * IO1:
    * Throughput in MiB/s = (Provisioned IOPS) × (I/O size in KiB)
    * The throughput limit of io1 volumes is 256 KiB/s for each IOPS provisioned
    * Limit to a max of 500 MiB/s (at 32,000 IOPS) and 1000 MiB/s (at 64,000 IOPS)

* **EBS Resizing** you can only increase, size in any volume, IOPS only in IO1

* **EBS Snapshots**, EBS volumes restored by snapshots need to be pre-warmed (using fio or dd command to read the entire volume). **Snapshots can be automated using Amazon Data Lifecycle Manager**

* **Encrypt an unencrypted EBS volume** for this you need to create an EBS snapshot -> Encrypt the EBS snapshot **Using Copy** -> create new EBS volume -> Attach the volume

* **Instance Store**: Is physically attached to the machine, better I/O, good for buffer, cache, tmp content, **data survice reboots, but dont survive termination**. You can't resize, and backup are manuals.

* If you use EBS for high performance, use EBS-optimized instance types

* **Troubleshotting:** High wait time or slow response for SSD => increase IOPS

* **Raids** :

  * **0 Increase performance:** Using this we can have a lot of IOPS, For example. 

    Two 500 GiB Amazon EBS io1 volumes with 4,000
    provisioned IOPS each will create a -> 
    1000 GiB RAID 0 array with an available bandwidth of 8,000
    IOPS and 1,000 MB/s of throughput

  * **1 increase fault tolerance:** Mirroring, we have to send the data to 2 EBS volume at the same time **(2x network).**

* **CloudWatch & EBS**: gp2 volumes: 5 min interval, io1 volumes: 1 min interval

* **EFS:** Work in multi-az, scalable, pay per use. Uses NFSv4. Uses SG to access EFS. EFS file sync to sync from on premise fs to EFS.  Backup EFS-to-EFS. Encryption at rest using KMS.



_______________________

### 10 S3 Storage and Data Management - For SysOps (incl Glacier, Athena & Snowball)
#### Notes

* **S3 Versioning** creates a new version each time you change a file, that includes when you encrypt a file. Deleting a file in the S3 Buckets just adds a delete marker on the versioning.

* **MFA**: You will need MFA to permanently delete an object, or suspend versioning on the bucket. Only the bucket owner (root account) can enable/disable MFA. Only enable using the CLI.

* **S3 Access Logs** Any request made to S3 from any account are logged into another S3 bucket

* **S3 Cross Region Replication**: Must enable versioning (Source and destination), buckets must be in different regions, can be in different account. Copying is asynchronous. Must give proper IAM permissions to S3. 

* **S3 pre-signed URLs**: Can generate pre-signed URLs using SDK or CLI. Valid for a default of 3600 seconds, can change timeout with --expires-in [TIME_BY_SECONDS] argument.

* Cloudfront is Popular with S3 but works with EC2 Load Balancing

* **CloudFront Troubleshooting**

  * 4xx error code indicates that user doesn't have access to the underlying bucket (403) or the object user is requesting is not found (404)
  * 5xx error codes indicates Gateway issues

  

* **S3 Inventory** helps manage your storage with Audit and reports on the replication and encryption status of your objects. You can query the data using Athena, redshift, etc. Data goes from a source bucket to a target bucket.



* **S3 Lifecycle Rules** Example: General Purpose => Infrequent Access => Glacier

* **S3 Analytics:** You can setup analytics to help determine when to transition objects from Standard to Standard_IA (Does not work for ONEZONE_IA or Glacier). Take about 24h to 48h hours to first start.

* **Glacier:** 

  * Each item in Glacier is called “Archive” (up to 40TB), archives are stored in ”Vaults”
  * Vault Access Policy is similar to bucket policy (restrict user / account permissions)
  * Vault Lock Policy is a policy you lock, for regulatory and compliance requirements.
    * The policy is immutable, it can never be changed (that’s why it’s call LOCK)
    * Example 1: forbid deleting an archive if less than 1 year old
    * Example 2: implement WORM policy (write once read many)
  * **3 retrieval options:**
    * Expedited (1 to 5 minutes retrieval) – $0.03 per GB and $0.01 per request
    * Standard (3 to 5 hours) - $0.01 per GB and 0.05 per 1000 requests
    * Bulk (5 to 12 hours) - $0.0025 per GB and $0.025 per 1000 requests

* **Snowball:** If it takes more than a week to transfer over the network, use Snowball devices.

* **Hybrid Cloud for Storage:**

  * Storage Cloud Native Options:

    * Block: Ebs, Ec2 instance store
    * File: Efs
    * Object: S3, Glacier

  * Storage Gateway:

    * File Gateway: File access / NFS (backed S3)

    * Volume Gateway: Volumes / Block Storage / iSCSI (backed by S3 with EBS snapshots)

    * Tape Gateway: VTL Tape solution / Backup with iSCSI (backed by S3 and Glacier)

      

* **Athena:** Serverless service to perform analytics directly against S3 files, uses SQL language to query the files. Has a JDBC / ODBC driver. Supports CSV, JSON, ORC, Avro, and Parquet (built on Presto)
  Use cases: Business intelligence / analytics / reporting, analyze & query
  VPC Flow Logs, ELB Logs, CloudTrail trails, etc...
  **Exam Tip: Analyze data directly on S3 => use Athena**



_____________

### Databases for SysOps

#### Notes

* **RDS:**
  * **Read Replicas:** up to 5 ReadReplicas, Cross AZ or CrossRegion, **replication is ASYNC**, so read are eventually consistent. Replicas can be promoted to their own DB, app must update the connection string. Each RR has a DNS endpoint. RR can be Multi-AZ. RR help with DR by using a cross region RR. 
    Read Replicas can be used to run BI / Analytics Reports for example
  * **Multi AZ:** **SYNC replication. One DNS name**, automatic app failover to standby. 
    Is only in a simple region. Backups are created from the standby
  * **Backups:** Are automatically enabled in RDS. You can configure db Snapshots manually, retention of backup for as long as you want.
  * **Encryption:** At rest with KMS (AES-256). SSL certs to RDS in flight.
    To enforce SSL: 
    * PostgreSQL: rds.force_ssl=1 in the AWS RDS Console (Parameter Groups)
    * MySQL: Within the DB: GRANT USAGE ON *.* TO 'mysqluser'@'%' REQUIRE SSL;
  * **Security:** Iam users can now be used for Mysql/Aurora (new)
  * **API:**
    * DescribeDBInstances API: Helps to get a list of all the DB Instances you have deployed including Read Replicas and Helps to get the DB version
    * CreateDBSnapshot API: Make a snapshot of a DB
    * DescribeEvents API: Helps to return information about events related to your DB Instance
    * RebootDBInstance API: Helps to initiate a ‘forced’ failover by rebooting DB instance
  * **Backups vs Snapshots:** Manual snapshots don't expire, backups have a retention period between 0 and 35 days
  * **RDS with CloudWatch:**
    * DatabaseConnections, SwapUsage, ReadIOPS/WriteLatency, ReadThrougput/WriteThroughPut, DiskQueueDepth, FreeStorageSpace
    * **Enhanced Monitoring**: Useful when you need to see how different processes or threads use the CPU, access to over 50 new CPU, memory, fs and disk I/O metrics
  * **Performance Insights:** **Visualize your database performance and analyze any issues that affect it, is like NewRelic, by Waits, SQL statements, Hosts, users, etc.**
* **Aurora:** Is propietary, support Postgres and MySQL. Is “AWS cloud optimized” and claims 5x performance improvement over MySQL on RDS, over 3x the performance of Postgres on RDS. Storage automatically grows in increments of 10GB, up to 64 TB. Aurora can have 15 replicas while MySQL has 5, and the replication process is faster (sub 10 ms replica lag). 
  Failover in Aurora is instantaneous. It’s HA native.
  **6 Copies of your data across 3 AZ**. Automate failover for master in less than 30 sec. Support cross Region replication. 
  
  * Serverless
  * No need to choose an instance size
  * Only supports MySQL 5.6 (as of Jan 2019) & Postgres (beta)
  * Helpful when you can’t predict the workload
  * DB cluster starts, shutdown and scales automatically based on CPU / connections
  * Can migrate from Aurora Cluster to Aurora Serverless and vice versa
  * Aurora Serverless usage is measured in ACU (Aurora Capacity Units)
  * Billed in 5 minutes increment of ACU
  * Note: some features of Aurora aren’t supported
* **DB Parameter Groups:** You can configure the DB enfine using Parameter Groups, dynamic parameters are applied immediately. Static parameters are applied after instance reboot.
  **Must-know parameters:**
  • PostgreSQL / SQL Server: rds.force_ssl=1 => force SSL connections 
  • Reminder: for SSL on MySQL / MariaDB, you must run: 
  GRANT SELECT ON mydatabase.* TO 'myuser'@'%' IDENTIFIED BY '....' REQUIRE SSL;
  **(This is Mysql Statement, not parameter group)**
* **ElastiCache:** Help make your app stateless. Apps queries ElastiCache, if not availabe, get from RDS and store in ElastiCache.
  * **Redis Overview:** In memory, cache survive reboots, key-value store
  * **Memcache Overview:** In memory, doesn't survive reboots. Object store
  
  

__________________

### Monitoring, Auditing and Performance

CloudWatch, CloudTrail & Config

#### Notes

* **AWS CloudWatch:**
  
  * **Metrics:** Collect and track key metrics
  * **Logs:** Collect, monitor, analyze and store log files. Can go to S3 for archival or stream to ElasticSearch. Using the AWS CLI can tail CloudWatch logs. Can use filter expressions
  * **Events:** Send notifications when certain events happen in your AWS
    Source + Rule => Target
    Schedule: Cron jobs
    Triggers to Lambda functions, SQS/SNS/Kinesis Messages
    Event Pattern: Event rules to react to a service doing something,  Ex: CodePipeline state changes!
  * **Alarms:** Trigger notifications for any metrics.
    Can go to Auto Scaling, EC2 Actions, SNS notificationes
    States: OK, INSUFFICIENT_DATA, ALARM
    High resolution custom metrics: can only choose 10 sec or 30 sec
    Alarms can be created based on CloudWatch Logs Metrics Filters
    CloudWatch doesn't test or validate the actions that is assigned
    **To test alarms and notifications, set the alarm state to Alarm using CLI** (Set-alarm-state)
  * Performance monitoring (metrics, CPU, network, etc...) & dashboards
  * Events & Alerting
  * Log Aggregation & Analysis
* **AWS CloudTrail:**
  
  * Provides governance, compliance and audit for your AWS Account, is Enabled by default, can put logs from CloudTrail into CloudWatch logs. **Can be region specific or global. Show the past 90 days of activity.** 
* **AWS Config:** (COMPLIANCE)
  
  * Track configuration of resources over time
  * Evaluate configuration compliance based on rules
  * Record configuration changes
  * Possibility of storing AWS Config data into S3 (can be queried by Athena)
  * You can receive alerts (SNS notifications) for any changes
  * **Config Rules:** Rules can be evaluated triggered for each config change, or regular time intervals. Can make custom config rules e.g evaluate if each EC2 instance is t2.micro
  
  

________________

### Account Management

### Notes

* **Service Health Dashboard:** Shows all regions, all services health. Show historical information for each day http://status.aws.amazon.com
  **Personal Health Dashboard:** Global service, show how AWS outages directly impact you and your resources. **List issues and actions you can do to remediate them** http://phd.aws.amazon.com
* **AWS Organizations:** Global service, allows to manage multiple AWS accounts. The main account is the master account. Other accounts are members accounts. **Consolidated Billing Across all accounts**, pricing benefits from all.
* **Organize accounts in Organizational Unit OU:** Can be anything: dev / test or HR / IT. Apply **Service Control Policies (SCPs) to OU, permit or Deny access to AWS services**. Helpful for sandbox account creation, or to separate dev and prod resources, or to only allow/deny services
* **Service Catalog:** Quick Self-service portal to launch a set of authorized products pre-defined by admins, incluide vms, dbs, storage options, etc. The "Products" are CF templates. Give user access to launching products without requiring AWS knowledge.
* **Billing Alarms:** Is stored in CloudWatch **us-east-1**, it's for actual cost, not for project cost.
* **Cost Explorer:** Graphical tool to view and analize your costs and usage, forecast spending for the next 3 months. Get recommendations for which EC2 Reserved Instances to purchase.
* **AWS Budget:** Create budget and send alarms when costs exceeds the budget, **3 types: Usage, Cost, Reservation.**
* **Cost Allocation Tags:** With tags we can track resources that relate to each other, with Cost Allocation Tags we can enable detailed costing reports. **Just like Tags, but they show up as columns in Reports**. Cost Allocation Tags just appear in the Billing Console.
* **KMS:** integrated to ebs, s3, redshit, rds, ssm. Only encrypt up to 4KB of data per call. 
  Fully manage the keys: Create, rotation, disable, enable. **Only Symmetric encryption**
* **Cloud HSM:** Provision encryption Hardware. **You manage your own encryption keys** (not AWS). Cluster are spread across multi AZ. **Support both, symmetric and asymmetric encryption.** 
* **IAM PassRole:** In order to assign a role to an EC2 instance, you need IAM:PassRole. You can used for any service, not only EC2
* **Security Token Service:** Allows to grant limited and temporary access to AWS resources. Cross account access. Federation (active directory). Federation with 3rd party providers/cognito -> Used mainly in web and mobile apps, makes use of fb/google/amazon etc to federate them.
* **Cross Account Access:** Define an IAM role for another account to access, define which accounts can access this IAM Role. Use AWS STS to retrieve credentials and impersonate the IAM Role you have access to. Temp credential valid 15 min to 1 hour.



__________________

### Security and Compliance for SysOps

#### Notes

* **DDOS protection:**
  * AWS Shield Standard: protects against DDOS attack for your website and applications, for all customers at no additional costs
  * AWS Shield Advanced: premium DDoS protection, access to response team 24/7. 
  * AWS WAF: Protects your web apps from common web exploits. Define customizable web security rules. Deploy on CloudFront, ALB or API gateway.
* **Penetration testing:** You need permission from AWS with root credentials, cannot test nano/micro/small instances. take 2 business days to be approved. For EC2, ELB, RDS, Aurora, CloudFront, API Gateway, Lambda, Lightsail
* **AWS Inspector:** **ONLY FOR EC2 Instances**. Analyze against know vulnerabilities, or unintended network access. You need install a agent, you define template, no own custom rules possible. After the assessment you get a report with a list of vulnerabilities. 
* **Loggin in AWS:** Aws provides many service-specific security and audit logs
  * CloudTrail trails - trace all API calls
  * Config Rules - for config & compliance over time
  * CloudWatch Logs - for full data retention
  * VPC Flow Logs - IP traffic within your VPC
  * ELB Access Logs - metadata of requests made to your load balancers
  * CloudFront Logs - web distribution access logs
  * WAF Logs - full logging of all requests analyzed by the service
    * Logs can be analyzed using AWS Athena if they’re stored in S3
      You should encrypt logs in S3, control access using IAM & Bucket Policies, MFA
* **GuardDuty:** Intelligent Threat discovery to protect AWS account, use **Machine Learning algorithms for detecting anomalies.** One click to enable (30 days trial), no need to install software. Notifies you in case of findings
* **Trusted Advisor:** No need to install anything, **high level AWS account assessment. Analize your accounts and provides recommendation**: Cost Optimization, performance, security, fault tolerance, service limits. **Can enable weekly notification from the console** 
* **KMS:** Integrated with EBS, S3, Redshift, RDS, SSM. Can create, rotate, disable, and enable keys.
* **Cloud HSM:** Provisions encryption Hardware. You manage your own encryption keys (not AWS). Clusters are spread across multi AZ. Support **Symmetric** and **asymmetric**. If you want to use *asymmetric* encryption, use AWS CloudHSM
* **PassRole Option:** Assign a role to an EC2 instance, you need **IAM:PassRole** not only EC2, for any service.
* **Security Token Service:** Allows to grant limited and temporary access to AWS resources. Token is valid for up to one hour. Cross Account Access. Federation (Active Directory) or Federation with third party providers / Cognito, used mainly in web and mobile apps, use of Facebook/Google/amazon etc to federate them
* **Cross Account Access:** IAM Role for another account to access, define which accounts can access this IAM Role. Temp credentials can be valid between 15 min to 1 hour
* **Identity Federation:** Federation lets users outside of AWS to assume temp role for accessing AWS resources. Federation assumes a form of 3rd party auth: Ldap, Active directory (SAML), SSO, Open ID, Cognito. Using federation, you don't need to create IAM users (user management is outside of AWS)
* **SAML Federation for Enterprise:** To integrate Active Directory or any SAML 2.0. Provides access to AWS console or cli. No need to create an IAM user for each of your employees.
* **Custom identity Broker Application For Enterprises:** Only if identity provider is not compatible with SAML 2.0. The identity broker must determine the appropiate IAM policy.
* **Cognito Federated Identity Pools:** (For public Apps) Provide direct access to AWS resources from the Client Side. For e.g. provide (temporary) access to write to S3 bucket using Facebook Login. **Web identity Federation is an alternative to using Cognito but AWS recommends against it**
* **AWS Artifact:** Portal that provides customers with on-demand access to AWS compliance documentation and AWS agreements



https://d1.awsstatic.com/whitepapers/Security/DDoS_White_Paper.pdf

https://d0.awsstatic.com/whitepapers/compliance/AWS_Security_at_Scale_Logging_in_AWS_Whitepaper.pdf



_______________________

### Route 53

#### Notes:

* In AWS, the most common records are:
  * A: URL to IPv4
  * AAAA: URL to IPv6
  * CNAME: URL to URL
  * Alias: URL to AWS resource.
* **Simple Routing Policy:** Maps domain to one URL. You can't attach health checks 
* **Weighted Routing Policy:** Control the % of the requests. Helpful to split traffic between two regions. Can be associated with Health Checks. 
* **Latency Routing Policy:** Has the least latency close to us. Helpful when latency of users is a priority.
* **Geo Location Routing Policy:** This is routing based of user location. For e.g. we specify traffic from the UK should go to this specified IP
* **Multi Value Routing Policy:** Use when routing traffic to multiple resources. Multi Value is not a substitute for having an ELB. 



____________________

### 16 Networking

#### Notes:

* CIDRs:
  * • /32 – no IP number can change
    • /24 - last IP number can change (256 ips) 
    • /16 – last IP two numbers can change (65,536 IP)
    • /8 – last IP three numbers can change
    • /0 – all IP numbers can change
  * Private IP can only allow certain values
    • 10.0.0.0 – 10.255.255.255 (10.0.0.0/8) <= in big networks
    • 172.16.0.0 – 172.31.255.255 (172.16.0.0/12) <= default AWS one
    • 192.168.0.0 – 192.168.255.255 (192.168.0.0/16) <= example: home networks
* **Default VPC:** All new accounts have a default VPC, new instances are launched into default VPC if no subnet is specified. **Have Internet connectivity and all instances have public IP.**
* **VPC:** Max 5 vpc per region, min size is /28, max size is /16. Max CIDR per VPC is 5. You can add another CIDR in your VPC
* **Subnet:** Aws Reserves 5 Ips in each subnet. **Exam tip:** If you need 29 IP addresses for EC2 instances, you can’t choose a Subnet of size /28 (32 IP)
* **Internet Gateways:** Help our VPC instances connect with the Internet. Must be create separately from VPC. **One VPC can only be attached to one IGW and viceversa** . IGW is also a NAT for the instances that have a public IPv4. Internet Gateways on their own do not allow internet access. **You also need configure your routes tables.** 
* **NAT Instances:** Allows instances in the private subnets to connect to the Internet, must be launched in a public subnet. Must have Elastic IP attached to it. Route table must be configured to route traffic from private subnets to NAT instances.
* **NAT Gateway:** Nat is created in a specific AZ, uses an EIP. Cannot be used by an instance in that subnet (only from other subnets). Requires an IGW. 
  **Private Subnet -> NAT -> IGW. No sg to manage/required**
* **DNS Resolution in VPC:** 
  * enableDnsSupport: (= DNS Resolution setting). Default True.
  * enableDnsHostname: (= DNS Hostname setting). False by default (true default vpc)
  * If you use custom DNS domain names in a private zone in Route 53, you must set both these attributes to true
* **Network ACLs:** Are like firewall which control traffic from and to subnet. Default NACL allows everything outbound and everything inbound. **One NACL per Subnet, new Subnets are assigned the Default NACL**. NACL are a great way of blocking a specific IP at the subnet level. 
  **Newly created NACL will deny everything**
  * Define NACL rules:
    * Rules have a number (1-32766) and higher precedence with a lower number
    * E.g. If you define #100 ALLOW <IP> and #200 DENY <IP> , IP will be allowed
    * Last rule is an asterisk (*) and denies a request in case of no rule match
* **VPC Peering:** Connect two VPC, privately using AWS network. **Is not transitive** . You can do VPC Peering with another AWS account. **You must update route tables in each VPC’s subnets to ensure instances can communicate** 
* **VPC Endpoints:** Endpoints allow you to connect to AWS Services using a private network instead of the public www network. **They remove the need of IGW, NAT, etc to access AWS Services**.
  * **Interface Endpoint:** provisions an ENI (private IP address) as an entry point (must attach security group) – most AWS services
  * **Gateway Endpoint** : provisions a target and must be used in a route table – **S3 and**
    **DynamoDB**
* **Flow Logs:** Capture information about IP traffic going into your interfaces. Is for VPC, Subnets, and ENI. Helps for connectivity issues. Logs data can go to S3 or CloudWatch Logs. 
  **Query VPC flow logs using Athena on S3 or CloudWatch Logs Insights**
* **Site to Site VPN:** 
  * **Virtual Private Gateway:**
    * VPN concentrator on the AWS side of the VPN connection
    * VGW is created and attached to the VPC from which you want to create the Site-to- Site VPN connection
    * Possibility to customize the ASN
  * **Customer Gateway**: Software application or physical device on customer side of the VPN connection
* **Direct Connect:** Provides a dedicated private connection from a remote network to your VPC. You need to setup a Virtual Private Gateway on your VPC. Access public resources (s3) and private (ec2) on same connection. **If you want to setup a Direct Connect to one or more VPC in many different regions (same account), you must use a Direct Connect Gateway** 
* **Egress Only Internet Gateway:** Egress only Internet Gateway is for IPv6 only, similar to NAT, but NAT is for IPv4. Good to know: IPv6 are all public addresses



____________________

## Practice Test

65 questions | 2 hours 10 minutes | 70% correct required to pass

Attempt 1: Passed 78% correct (51/65) 1 hour 18 minutes August 27, 2019 11:29 PM



* For share an AMI with other AWS accounts that belong to the same organization, you need to edit the account list that can see the AMI from the AMI console UI



* To perform an analysis to see your most active AWS users, you need use CloudTrail and Athena 



* If you need to scale your app based on the number of users, yo need to config auto scale based on the number of connections CloudWatch Metrics, NO to a custom script expose the number of users with custom Cloudwath Metric (is more complicate)



* Cfn-signal fail to send a signal to Cloudformation if the subnet where the application is deployed does not have a network route to the CloudFormation service through a Nat Gateway or Internet Gateway 



* If you have a instance in a private subnet with a Nat Gateway, you need to add a route with a target 0.0.0.0/0 to the NAT Gateway



* You can suspend ASG Launch process, with this no scaling out actions have taken place



* If you suspect some of your employees try to access files in S3 that they don't have access to, you need to enable S3 Access Logs and analyze them using Athena



* If you need best deploying speed on Elastic Beanstalk with an app with OS dependencies, you need to create a Golden Image and create a new Beanstalk environment for each application and apply blue/green deployment patterns 



* If you had a website on S3 with Cloudfront, and some users experience a lot of 501 errors, you need to analyze the CloudFront access logs using Athena. 
  The S3 logs will not give you any chance to look at the user IP and other crucial information, as the requests are proxied through CloudFront. Additionally, results are cached in CloudFront and the S3 access logs won't contain a lot of information



* Under the shared responsibility model you NOT are responsible for S3 SSE in Amazon S3, yes for versioning, ACLs and Bucket policies





