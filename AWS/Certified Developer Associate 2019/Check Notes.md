#### S3

There are 4 methods of encrypting objects in S3, all uses AES-256 for Encryption. All in rest:

- SSE-S3: encrypts S3 objects using keys handled & managed by AWS, ServerSide AES-256 (HTTP/S+Header). AWS manages both data key and master key 

- SSE-KMS: leverage AWS KMS to manage encryption keys, ServerSide, Adventages: UserControl + Audit trail (HTTP/S+Header). AWS manages data key and you manage master key 

- SSE-C: when you want to manage your own encryption keys outside of AWS, S3 doesn't store the key, HTTPS must be used, encryption key must provided in HTTP headers for every HTTP request made (HTTPS Only+Data Key in Header). You manage both data key and master key 

- Client Side Encryption, customer fully manages the keys and encryption cycle

- Bucket policies are only attached to S3 buckets, ACLs can be attached to S3 Buckets OR S3 Objects. Bucket Policies are written in JSON, ACLs are written in XML.

  ------

- Encryption in transit (SSL)

  - AWS S3 exposes:
    • HTTP endpoint: non encrypted
    • HTTPS endpoint: encryption in flight

    • You’re free to use the endpoint you want, but HTTPS is recommended
    • HTTPS is mandatory for SSE-C
    • Encryption in flight is also called SSL / TLS

_________

#### AWS CLI, SDK, IAM Roles & Policies

* **AWS CLI STS Decode Errors**, is for when you run API calls and they fail, you can get a long error message, this error message can be decoded using the STS command line **sts decode-authorization-message**
* IAM Policies minimun elements: Effects, Actions, Resources
* **AWS Security Token Service (STS)** is a web service that enables you to request temporary, limited-privilege credentials for IAM users or for users that you authenticate (federated users).
* STS Api call return with **Security Token, Access Key ID, Secret Access Key**



____

#### Elastic Beanstalk

Three architecture models:
• Single Instance deployment: good for dev
• LB + ASG: great for production or pre-production web applications
• ASG only: great for non-web apps in production (workers, etc..)

Has tree components:

- Application
- Application Version: Each deployment get assigned a version
- Environment name (dev, test, prod, etc)

You create an Application -> Create an environment ->  Upload a version (Giving an alias) -> Release to environment from Alias

* An *environment* is a collection of AWS resources running an application version. Each environment runs only one application version at a time, however, you can run the same application version or different application versions in many environments simultaneously.

**EB Extensions**, this use a zip file, requirements: 

- .ebextensions/ in the root of source code 
- yaml/json
- admit .config extensions (e.g. logging.config)
- Ability to add resources such as RDS, ElastiCache, DynamoDB, etc



________________

#### CodeCommit, CodePipeline, CodeBuild, CodeDeploy

- **CodeCommit**
  - Authentication in Git:
    - SSH Keys: AWS Users can configure SSH keys in their IAM Console
    - HTTPS: Done through the AWS CLI Authentication helper or Generating HTTPS credentials
    - MFA (multi factor authentication) can be enabled for extra safety
  - Authorization in Git:
    - IAM Policies manage user / roles rights to repositories
  - *You can trigger notifications in CodeCommit using AWS SNS (Simple*
    *Notification Service) or AWS Lambda or AWS CloudWatch Event Rules*
  - SNS / Lambda notifications: Deletion of branches, trigger for push in master
  - *CloudWatch Event Rules: Trigger for PR, Commit comments events.*

- **Codepipline**

  - Automating our pipeline from code to ElasticBeanstalk
  - Made of stages, each stage can have sequential actions or parallel, examples: build, test, deploy, etc. Manual Approval can be defined at any stage
  - Each pipleine stage can create "artifacts", this are passed store in Amazon S3 and passed on to the next stage
  - If a pipeline file for send mail you need Cloudwatch Event Rule
  - CodePipeline state changes happen in AWS CloudWatch Events, which
    can in return create SNS notifications.

- **Codebuild**

  - You can use our own Docker images
  - Source code From GitHub - CodeCommit - CodePipeline - S3
  - Build instructions can be defined in code (buildspec.yml)
  - You can reproduce Codebuild Locally for troubleshoot (after install docker)
  - Pipelines can be defined within CodePipeline or Codebuild itself
  - Save logs in CloudWatch or S3
  - S3 Cache Bucket (optional)
  - BuildSpec
    - Must be at the root of your code
    - Define env variables, can use secrets with SSM Parameter Store
    - Phases: Install (dependencies), Pre Build (commands), Build, Post build (for example zip)
    - Artifacts: For example upload to S3
    - Cache: Files to cache (usually dependencies) in s3

- **CodeDeploy**

  - Deploy to EC2, these instances arent managed by Elastic Beanstalk
  - Ec2 or On premise need the CodeDeploy Agent, the agent is continuosly polling AWS CodeDeploy, Codedeploy sends appspec.yml
  - Application is pulled from github or S3. 
  - Ec2 Run the deployment instructions
  - CodeDeploy Agent will report of success  / failure of deployment on the instance
  - Ec2 instances are grouped by Deployment Group (dev-test-prod)
  - CodeDeploy can be chained into CodePipeline and use artifacts from there
  - Blue-green only works with EC2 (not on premise)
  - Deployment type: In place Deployment or Blue/Green Deployment
  - Hooks: Set of instructions to do to deploy the new version: The order is:
    ApplicationStop, DownloadBundle, BeforeInstall, install, AfterInstall, ApplicationStart, ValidateService, BeforeAllowTraffic, AllowTraffic, AfterAllowTraffic.

  

______________

#### Cloudformation

- Pseudo Parameters, these can be used at any time (accountid, region, etc)
- Parameters: the dynamic inputs for your template (useful to reuse your templates) !Ref



_________________

#### CloudWatch, X-Ray 

* CloudWatch

  * High resolution custom metrics can only choose 10 or 30 sec. 
  * You need use detailed monitoring if you want to more prompt scale your ASG
  * Custom Metrics: Standard resolution: 1 min, High resolution: up to 1 seconds, use api call PutMetricData, use exponential back off in case of throttle errors

* X-Ray

  * Segments: each application / service will send them
  * Trace: segments collected together to form an end-to-end trace
  * Sampling: decrease the amount of requests sent to X-Ray, reduce cost
  * You can send traces across accounts if you create a role on another account, and allow a role in your account to assume that role

  

_____________

#### SQS, SNS & Kinesis

* SQS: Producer -> Send Messages -> SQS Queue -> Poll Messages -> Consumer
* SQS Standard Queue: Default retention: 4 days maximum of 14, no limit messages in the queue, maybe duplicate messages, limitation 256kb per message
* SQS Delay Queue: Delay a message up to 15 min, default is 0, can override the default using the DelaySeconds parameters
* SQS poll receive up to 10 messages at a time
* SQS Visibility Timeout, the message is invisible to others consumers for a defined period. Default 30 seconds (0 sec to 12 hours). DeleteMessage Api to tell SQS the message was successfully processed
* ReceiveMessageWaitTimeSeconds, when set to greater than zero, enables long polling. 
* SQS FIFO, name of the queue must end in .fifo, lower throughput, no per message dealy. Provide a MessageDeduplicationId with your message, deduplication is 5 min. For ensure strict ordening between messages specify a MessageGroupId, message with different Group ID may be received out of order. Messages with the same Group Id are deliverd to one consumer at a time.
* SQS Extended Client is for send message greater than 256kb using a amazon S3 bucket, in the SQS queue only store the metadata message
* *You can use the ReceiveMessage API call to retrieve the message, the ChangeMessageVisibility API call to increase the visibility timeout, and the DeleteMessage API call to delete the message when processing completes*



* SNS event producer only sends messages to one SNS topics, as many "Event Receivers" (subscriptions) as we want to listen to the SNS topic notifications. Each subscriber to the topic will get all the messages.
* SNS How to publish: Create a topic, create a subscription (or many), publish to the topic
* SNS + SQS (FAN OUT): Push one in SNS, receive in many SQS, no data loss



* Kineses is managed alternative to Kafka, great for logs, metrics, IoT, realtime bigdata, data is automatically replicated to 3 AZ
* Kinesis Streams: low latency streaming ingest at scale. Streams are divided in ordered Shards/Partitions. Data retention is 1 day default to 7 days, can reprocess data. Once data is inserted in Kinesis, it cant be deleted. One stream is made of many shards
* Kinesis Analytics: perform real-time analytics on streams using SQL. Managed service no administration
* Kinesis Firehose: load streams into S3, Redshift, ElasticSearch. Near Real Time (60 segs latency). Managed service no administration



__________________

#### Lambda

- Max resources of 3GB of ram (128 min), increase RAM improve CPU and Network
- Execution time 3 secs, max of 5 min (New limit 15 minuts)
- Deployment size max: Compressed .zip 50mb, uncompressed 250mb, env variables 4kb
- Concurrency up to 1000 executions, invocation over the concurrency will trigger a "Throttle"
- Versions is code and confgs are immutable, have increasing version numbers (v1, v2)
- Aliases are pointers to versions, is usefull to dev, test, prod envs, aliases are mutable.
- Lambda@Edge is for use lambda globally, or for request filtering before reaching your app, is deploy lambda alongside your CloudFront CDN. You can use to change CloudFront requests and responses
- The format of a Lambda handler is <filename>.<function name>.
- Environment variables for Lambda functions enable you to dynamically pass settings to your function code and libraries, without making changes to your code. 

____________

#### DynamoDB

* DynamoDB is a Fully managed NoSQL db, with HA in 3AZ, is made of tables, each table has a PK, and each table can have an infinite number of items (rows), each item has attributes (maximum size of a item 400kb)
* Querys: use FilterExpression to client side filter, returns up to 1 MB of data, you can use "Limit" to show only a number of items, able to do pagination on the results, can query a table, a lsi, or a gsi
* Partitions internal: Data is divided in partititions, wcu and rcu are spread evenly between partitions
* DynamoDB Throttling: If we exceed our RCU or WCU we get ProvisionedThrouhgputExceededExceptions
  - Reasons:
    - Hot keys: One PK is being read to many times
    - Hot partitions:
    - Very large items
  - Solutions:
    - Exponential back-off when exception is encountered
    - Distribute pk as much as possible
    - If rcu issue, we can use DAX

* GSI Offer only eventual consistency
* DynamoDB is Optimistic Concurrency / Optimistic Locking this mean because multiple clients can access the same object at the same time, DynamoDB for example if we delete something, only do that if a certain condition is validated. This use Conditional Writes.
* DynamoDB Dax (Accelerator) is a seamless cache for DynamoDB, no need to modified the application. Solves the Hot Key problem (HK = a key with to many reads). Default TTL 5 minutes, multi AZ (3 nodes minimum for prod). Dax write directly in DynamoDB 
* DynamoDB CLI:
  - --Projection-expression: select attributes to retrieve (maybe from a DynamoDB scan)
  - --filter-expression: filter results
  - General Cli pagination:
    - Optimization: --page-size: full dataset is still received but each API call will request less data. Allow you to retrieve a subset of the attributes coming from a DynamoDB scan
    - Pagination:  (Useful to paginate the results of a DynamoDB scan in order to minimize the amount of RCU that you will use for that CLI command)
      - --max-items: max number of results returned by the CLI. Returns NextToken
      - --starting-token: specify the last received NextToken to keep on reading

* DynamoDB transactions conmes 2X of WCU / RCU
* The UpdateTable API call is used to change the required provisioned throughput capacity, and does not consume capacity units.



_______________

#### Api Gateway & Cognito

* ApiGateway

  * To make a change in the Api Gateway you need to make a "Deployment" for them to be in effect

  * Changes are deployed to "Stages" (e.g. dev, test, prod). Each stage has its own configs

  * Stages Variables are like envs variables for Api Gateway,

  * Api gateway stages (variables) (dev, prod)-> Lambda Aliases (dev, prod) -> Lambda version (1,2,3)

  * Mapping Templates can be used to modify request / responses, rename parameters, filter output results.

  * Endpoint – A URL that responds to HTTP requests, 
    Resource – A named entity that exists within an endpoint, 
    Stage - To deploy an API, you create an API deployment and associate it with a stage. Each stage is a snapshot of the API and is made available for the client to call.

  * API security: 

    - IAM Permissions: rest api use sig v4, where IAM credential are in headers. Great for users/roles already within your AWS account. For e.g useful to authorize an EC2 to use your internal API. Handle Authentication + Authorization
    - Custom Authorizer: Use AWS Lambda to validate the token in header being passed. Great for 3rd party tokens. Handle Authentication + Authorization
    - Cognito Users Pools: Cognito fully manages user lifecycle. Api Gateway verifies identity automatically from AWS Cognito. You manage your own user pool (can be backed by Facebook, Google login etc...). No need custom code. Cognito only helps with authentication, not authorization
    - You can control access to the API Gateway with Cognito Users Pools and Lambda Authorizers

    

* Cognito User Pools:

  - Sign in functionality for app users
  - Integrate with API Gateway

* Cognito Identity Pools (Federated Identity):

  - Provide AWS credentials to users so they can access AWS resources directly
  - Integrate with Cognito User Pools as an identity provider

* Cognito Sync: (Deprecated – use AWS AppSync now)

  - Synchronize data from device to Cognito.
  - May be deprecated and replaced by AppSync



_______________

#### ECS

- ECS

  - If we need to scale, we need to add EC2 instances. We must configure the file /etc/ecs/ecs.config with the cluster name

  - Task Definitions are metadata in JSON to tell ECS how to run a Docker (image name, port binding, memory and cpu required, envs, iam role, etc)

  - EC2 instances can run multiple containers on the same type, you must not specify a host port (only container port), and use a ALB with dynamic port mapping, the Ec2 sg must allow traffic from the ALB on al ports. To enable random host port, set host port = 0 (or empty), which allows multiple containers of the same type to launch on the same instance

  - ECS + Xray: two types: As Daemon (on daemon for EC2), "Side Car" (on xray per app container).-Fargate also use side car integration. 

    For the Task definition in EC2, we need the next line:
    name : AWS_XRAY_DAEMON_ADDRESS Value: xray-daemon:2000

  - ECS does integrate with CloudWatch Logs, you need to setup login at the task definition level, each container will have a different log stream

