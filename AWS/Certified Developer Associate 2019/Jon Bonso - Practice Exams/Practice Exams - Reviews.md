# AWS Certified Developer Associate Practice Tests

65 questions | 2 hours 10 minutes | 72% correct required to pass



## AWS Certified Developer Associate Practice Test 1 - Results

Attempt 1: Failed 56% correct (37/65) 1 hour 11 minutes August 5, 2019 4:08 AM

Attempt 1: Failed 70% correct (42/65) 1 hour 12 minutes August 7, 2019 2:25 AM



* Multithreaded event-based key/value cache store = ElastiCache Memcached

  **You can choose Memcached over Redis if you have the following requirements:**

  \- You need the simplest model possible.

  \- You need to run large nodes with multiple cores or threads.

  

* Setup HTTPS communication between the viewers and Cloudfront: 
  You can change the **Viewer Protocol Policy** setting for one or more cache behaviors to require HTTPS communication by setting it as either `Redirect HTTP to HTTPS`, or `HTTPS Only`.



* AWS Lambda uses environment variables to facilitate communication with the X-Ray daemon and configure the X-Ray SDK.

  **_X_AMZN_TRACE_ID:** Contains the tracing header, which includes the sampling decision, trace ID, and parent segment ID. If Lambda receives a tracing header when your function is invoked, that header will be used to populate the _X_AMZN_TRACE_ID environment variable. If a tracing header was not received, Lambda will generate one for you.

  **AWS_XRAY_CONTEXT_MISSING:** The X-Ray SDK uses this variable to determine its behavior in the event that your function tries to record X-Ray data, but a tracing header is not available. Lambda sets this value to `LOG_ERROR `by default.

  **AWS_XRAY_DAEMON_ADDRESS:** This environment variable exposes the X-Ray daemon's address in the following format: *IP_ADDRESS***:***PORT*. You can use the X-Ray daemon's address to send trace data to the X-Ray daemon directly, without using the X-Ray SDK.

  

* API Gateway and CloudWatch, the metrics reported by API Gateway provide information that you can analyze in different ways. The list below shows some common uses for the metrics. These are suggestions to get you started, not a comprehensive list.

   \- Monitor the **IntegrationLatency** metrics to measure the responsiveness of the backend.

   \- Monitor the **Latency** metrics to measure the overall responsiveness of your API calls.

   \- Monitor the **CacheHitCount** and **CacheMissCount** metrics to optimize cache capacities to achieve a desired performance.

  

* **S3 is not part of your VPC**, unlike your EC2 instances, EBS volumes, ELBs, and other services which typically reside within your private network. An EC2 instance needs to have access to the Internet, via the Internet Gateway or a NAT Instance/Gateway in order to access S3. Alternatively, you can also create a VPC endpoint so your private subnet would be able to connect to S3.



* API Gateway integration types:

  * Custom HTTP endpoint
    * HTTP Proxy:  You do not set the integration request or the integration response. API Gateway passes the incoming request from the client to the HTTP endpoint and passes the outgoing response from the HTTP endpoint to the client.
    * HTTP: you must configure both the integration request and integration response. You must set up necessary data mappings from the method request to the integration request, and from the integration response to the method response.
  * Lambda Custom Integrations:
    * AWS_PROXY: This integration relies on direct interactions between the client and the integrated Lambda function. With this type of integration, also known as the Lambda proxy integration, you do not set the integration request or the integration response. API Gateway passes the incoming request from the client as the input to the backend Lambda function.	
    * AWS: All other integrations

  

* KMS APIs call: The **`GenerateDataKeyWithoutPlaintext` API generates a unique data key**. This operation returns a data key that is encrypted under a customer master key (CMK) that you specify. `GenerateDataKeyWithoutPlaintext` is identical to `GenerateDataKey `except that it returns only the encrypted copy of the data key.

  Like `GenerateDataKey`, `GenerateDataKeyWithoutPlaintext` returns a unique data key for each request. The bytes in the key are not related to the caller or CMK that is used to encrypt the data key.
  ***GenerateDataKey*** this operation also **returns a plaintext copy of the data key along with the copy of the encrypted data key** under a customer master key (CMK) that you specified. 

  

* Lambda Functions:  **MAX 3000 concurrent executions**

  ```
  concurrent executions = (invocations per second) x (average execution duration in seconds)
  ```

  if the Lambda function takes an average of 100 seconds for every execution with 50 requests per second. Using the formula above, the function will have 5,000 concurrent executions.

  ​      = **(50 events per second) x (100 seconds average execution duration)**
  ​      =  **5,000 concurrent executions**

  AWS Lambda dynamically scales function execution in response to increased traffic, up to your [concurrency limit](https://docs.aws.amazon.com/lambda/latest/dg/limits.html). Under sustained load, your function's concurrency bursts to an initial level between 500 and 3000 concurrent executions that varies per region. After the initial burst, the function's capacity increases by an additional 500 concurrent executions each minute until either the load is accommodated, or the total concurrency of all functions in the region hits the limit.

  By default, AWS Lambda limits the total concurrent executions across all functions within a given region to 1000. This limit can be raised by requesting for AWS to increase the limit of the concurrent executions of your account. 

  

* **Secrets Manager** enables you to replace hardcoded credentials in your code (including passwords), with an API call to Secrets Manager to retrieve the secret programmatically. This helps ensure that the secret can't be compromised by someone examining your code, because the secret simply isn't there. Also, you can configure Secrets Manager to **automatically rotate** the secret for you according to a schedule that you specify. You can store and control access to these secrets centrally by using the Secrets Manager console, the Secrets Manager command line interface (CLI), or the Secrets Manager API and SDKs.



## AWS Certified Developer Associate Practice Test 2 - Results

Attempt 1: Failed 64% correct (42/65) 1 hour 12 minutes August 7, 2019 2:25 AM

Attempt 2: Passed! 81% correct (53/65) 1 hour 2 minutes August 11, 2019 10:52 AM



* If you are using a CloudFormation template, you can configure the `AWS::Lambda::Function` resource which creates a Lambda function. To create a function, you need a deployment package and an execution role. The deployment package contains your function code. The execution role grants the function permission to use AWS services, such as Amazon CloudWatch Logs for log streaming and AWS X-Ray for request tracing.
  **You can including your function source inline in the ZipFile parameter in the CF template, without need a zip file or use S3**



* A Lambda authorizer is an API Gateway feature that uses a Lambda function to control access to your API. When a client makes a request to one of your API's methods, API Gateway calls your Lambda authorizer, which takes the caller's identity as input and returns an IAM policy as output. 
  Two types of Lambda authorizers:

   \- A **token-based** Lambda authorizer (also called a TOKEN authorizer) receives the caller's identity in a bearer token, such as a JSON Web Token (JWT) or an OAuth token.

   \- A **request parameter-based** Lambda authorizer (also called a REQUEST authorizer) receives the caller's identity in a combination of headers, query string parameters, stageVariables, and $context variables.

  

* SSE-C: Allows you to set your own encryption. Amazon S3 manages both the encryption, as it writes to disks, and decryption, when you access your objects.

  you **must** provide encryption key information using the following request headers:

  **x-amz-server-side-encryption-customer-algorithm** - This header specifies the encryption algorithm. The header value must be "AES256".

  **x-amz-server-side-encryption-customer-key** - This header provides the 256-bit, base64-encoded encryption key for Amazon S3 to use to encrypt or decrypt your data.

  **x-amz-server-side-encryption-customer-key-MD5** - This header provides the base64-encoded 128-bit MD5 digest of the encryption key according to RFC 1321. Amazon S3 uses this header for a message integrity check to ensure the encryption key was transmitted without error. 



* X-Ray compiles and processes segment documents to generate queryable **trace summaries** and **full traces** that you can access by using the [`GetTraceSummaries`](https://docs.aws.amazon.com/xray/latest/api/API_GetTraceSummaries.html) and [`BatchGetTraces`](https://docs.aws.amazon.com/xray/latest/api/API_BatchGetTraces.html) APIs, respectively. In addition to the segments and subsegments that you send to X-Ray, the service uses information in subsegments to generate **inferred segments** and adds them to the full trace. Inferred segments represent downstream services and resources in the service map.



* Iam permissions in multiple Accounts:

  You can use a role to delegate access to resources that are in different AWS accounts that you own (Production and Testing). You can share resources in one account with users in a different account by setting up **cross-account access**. In this way, you don't need to create individual IAM users in each account and the users don't have to sign out of one account and sign into another in order to access resources that are in different AWS accounts. 
  

* To ensure efficient tracing and provide a representative sample of the requests that your application serves, the X-Ray SDK applies a **sampling** algorithm to determine which requests get traced. By default, the X-Ray SDK records the first request each second, and five percent of any additional requests.
  

* AWS X-RAY Samples:
  To ensure efficient tracing and provide a representative sample of the requests that your application serves, the X-Ray SDK applies a **sampling** algorithm to determine which requests get traced. By default, the X-Ray SDK records the first request each second, and five percent of any additional requests.
  Reservoir size is the target number of traces to record per second before applying the fixed rate. The reservoir applies across all services cumulatively, so you can't use it directly. However, if it is non-zero, you can borrow one trace per second from the reservoir until X-Ray assigns a quota. Before receiving a quota, record the first request each second, and apply the fixed rate to additional requests. The fixed rate is a decimal between 0 and 1.00 (100%).
  The default rule that will record the first request each second, and five percent of any **additional** requests. One request each second is referred to as the reservoir, which ensures that at least one trace is recorded each second. The five percent of additional requests is what we refer to as the fixed rate. **Both the reservoir and the fixed rate are configurable.** 

   **total sampled requests per second**`= reservoir size + ( (incoming requests per second - reservoir size) * fixed rate)`



* The unit of scale for AWS Lambda is a concurrent execution. However, scaling indefinitely is not desirable in all scenarios. For example, you may want to control your concurrency for cost reasons or to regulate how long it takes you to process a batch of events, or to simply match it with a downstream resource. To assist with this, Lambda provides a concurrent execution limit control at both the account level and the function level.

  The *concurrent executions* refers to the number of executions of your function code that are happening at any given time. You can estimate the concurrent execution count, but the concurrent execution count will differ depending on whether or not your Lambda function is processing events from a poll-based event source.

  If you set the concurrent execution limit for a function, the value is deducted from the unreserved concurrency pool. For example, if your account's concurrent execution limit is 1000 and you have 10 functions, you can specify a limit on one function at 200 and another function at 100. The remaining 700 will be shared among the other 8 functions.

  AWS Lambda will keep the unreserved concurrency pool at a minimum of 100 concurrent executions, so that functions that do not have specific limits set can still process requests. So, in practice, if your total account limit is 1000, you are limited to allocating 900 to individual functions.

  By default, an AWS account's concurrent execution limit is 1000 which will be shared by all Lambda functions.

## AWS Certified Developer Associate Practice Test 3 - Results

Attempt 1: Passed 73% correct (48/65) 1 hour 37 minutes August 9, 2019 6:05 PM



* To enable HTTPS connections to your website or application in AWS, you need an SSL/TLS *server certificate*. For certificates in a Region supported by **AWS Certificate Manager (ACM)**, it is recommended that you use ACM to provision, manage, and deploy your server certificates. In unsupported Regions, you must use **IAM as a certificate manager**. 
  ACM is the preferred tool to provision, manage, and deploy your server certificates. With ACM you can request a certificate or deploy an existing ACM or external certificate to AWS resources. Certificates provided by ACM are free and automatically renew. In a [supported Region](https://docs.aws.amazon.com/general/latest/gr/rande.html#acm_region), you can use ACM to manage server certificates from the console or programmatically. 
  Use IAM as a certificate manager only when you must support HTTPS connections in a Region that is not [supported by ACM](https://docs.aws.amazon.com/general/latest/gr/rande.html#acm_region).



* Lamba invocations running concurrently in Kineses or DynanamoDBStreams,  the number of shards is the unit of concurrency. If your stream has 100 active shards, there will be at most 100 Lambda function invocations running concurrently. This is because Lambda processes each shard’s events in sequence.
  
* AWS CodeCommit requires credentials. Those credentials must have permissions to access AWS resources, such as CodeCommit repositories, and your IAM user, which you use to manage your Git credentials or the SSH public key that you use for making Git connections. AWS CodeCommit requires credentials. Those credentials must have permissions to access AWS resources, such as CodeCommit repositories, and your IAM user, which you use to manage your Git credentials or the SSH public key that you use for making Git connections.
  

* You can set up a gateway response for a supported response type at the API level. Whenever API Gateway returns a response of the type, the header mappings and payload mapping templates defined in the gateway response are applied to return the mapped results to the API caller.

  The following are the Gateway response types which are associated with the **HTTP 504 error in API Gateway**: 

  **INTEGRATION_FAILURE** - The gateway response for an integration failed error. If the response type is unspecified, this response defaults to the DEFAULT_5XX type.

  **INTEGRATION_TIMEOUT** - The gateway response for an integration timed out error. If the response type is unspecified, this response defaults to the DEFAULT_5XX type.
  
  For the integration timeout, the range is from 50 milliseconds to 29 seconds for all integration types, including Lambda, Lambda proxy, HTTP, HTTP proxy, and AWS integrations.
  
  (In other scenario throttling generate a large number of incoming requests will most likely produce an HTTP 502 or 429 error)
  
  
* **AWS CloudFormation StackSets** extends the functionality of stacks by enabling you to create, update, or delete stacks **across multiple accounts and regions** with a single operation. Using an administrator account, you define and manage an AWS CloudFormation template, and use the template as the basis for provisioning stacks into selected target accounts across specified regions. A *stack set* lets you create stacks in AWS accounts across regions by using a single AWS CloudFormation template.



* ECS and CQL: **Cluster queries** are expressions that enable you to group objects. For example, you can group container instances by attributes such as Availability Zone, instance type, or custom metadata. You can add custom metadata to your container instances, known as *attributes*. Each attribute has a name and an optional string value. You can use the built-in attributes provided by Amazon ECS or define custom attributes. After you have defined a group of container instances, you can customize Amazon ECS to place tasks on container instances based on group.



* The AWS CLI ([aws s3 commands](https://docs.aws.amazon.com/cli/latest/userguide/cli-services-s3-commands.html#using-s3-commands-managing-objects)), AWS SDKs, and many third-party programs automatically perform a multipart upload when the file is large. To perform a multipart upload with encryption using an AWS KMS key, the requester must have permission to the **kms:Decrypt** action on the key. This permission is required because Amazon S3 must decrypt and read data from the encrypted file parts before it completes the multipart upload. 



* **Systems Manager** parameters are a unique type that is different from existing parameters because they refer to actual values in the Parameter Store. The value for this type of parameter would be the Systems Manager (SSM) parameter key instead of a string or other value. CloudFormation will fetch values stored against these keys in Systems Manager in your account and use them for the current stack operation. e.g. **This is useful to retrieve the latest AMI IDs in CF.**



* CodeDeploy provides two deployment type options:

  **In-place deployment**: The application on each instance in the deployment group is stopped, the latest application revision is installed, and the new version of the application is started and validated. You can use a load balancer so that each instance is deregistered during its deployment and then restored to service after the deployment is complete. **Only deployments that use the EC2/On-Premises compute platform can use in-place deployments**. AWS Lambda compute platform deployments cannot use an in-place deployment type.

  **Blue/green deployment**: The behavior of your deployment depends on which compute platform you use:

  \- **Blue/green on an EC2/On-Premises** compute platform: The instances in a deployment group (the original environment) are replaced by a different set of instances (the replacement environment). If you use an EC2/On-Premises compute platform, be aware that blue/green deployments work with Amazon EC2 instances only.

  \- **Blue/green on an AWS Lambda** compute platform: Traffic is shifted from your current serverless environment to one with your updated Lambda function versions. You can specify Lambda functions that perform validation tests and choose the way in which the traffic shift occurs. All AWS Lambda compute platform deployments are blue/green deployments. For this reason, you do not need to specify a deployment type.

  \- **Blue/green on an Amazon ECS** compute platform: Traffic is shifted from the task set with the original version of a containerized application in an Amazon ECS service to a replacement task set in the same service. The protocol and port of a specified load balancer listener are used to reroute production traffic. During deployment, a test listener can be used to serve traffic to the replacement task set while validation tests are run.

  

## AWS Certified Developer Associate Practice Test 4 - Results

Attempt 1: Passed 69% correct (45/65) 1 hour 44 minutes August 13, 2019 12:45 AM



* Amazon DynamoDB **transactions** simplify the developer experience of making coordinated, all-or-nothing changes to multiple items both within and across tables. Transactional read requests require two read capacity units to perform one read per second for items up to 4 KB. **(Divide / 2KB)** 
  ECR * 2 = SCR 
  SCR * 2 = Transactional Read Requests



* DynamoDB Report Write Capacity Units consumed (put, update, delete)

  To return the number of write capacity units consumed by any of these operations, set the `ReturnConsumedCapacity` parameter to one of the following:

  `**TOTAL** `— returns the total number of write capacity units consumed.

  `**INDEXES** `— returns the total number of write capacity units consumed, with subtotals for the table and any secondary indexes that were affected by the operation.

  `**NONE** `— no write capacity details are returned. (This is the default.)



* IAM instance profile is just a container for an IAM role that you can use to pass role information to an EC2 instance when the instance starts. This is different from an AWS CLI **profile**, which you can use for switching to various profiles. In addition, an instance profile is associated with the instance and not configured in the AWS CLI.



* Amazon RDS supports using Transparent Data Encryption (TDE) to encrypt stored data on your DB instances running Microsoft SQL Server. TDE automatically **encrypts data before** it is written to storage, and automatically decrypts data when the data is read from storage. 
  Instead **RDS Encryption** encrypts your Amazon RDS DB instances and snapshots **at rest**. It doesn't automatically encrypt data before it is written to storage, nor automatically decrypt data when it is read from storage.



* Amazon CloudWatch Events is a web service that monitors your AWS resources and the applications you run on AWS. You can use Amazon CloudWatch Events to detect and react to changes in the state of a pipeline, stage, or action. Then, based on rules you create, CloudWatch Events invokes one or more target actions when a pipeline, stage, or action enters the state you specify in a rule. 



* The `sam package` command zips your code artifacts, uploads them to Amazon S3, and produces a packaged AWS SAM template file that's ready to be used. The `sam deploy` command uses this file to deploy your application.

  Take note that both the `sam package` and `sam deploy` commands are identical to their AWS CLI equivalent commands which are [`aws cloudformation package`](http://docs.aws.amazon.com/cli/latest/reference/cloudformation/package.html) and [`aws cloudformation deploy`](http://docs.aws.amazon.com/cli/latest/reference/cloudformation/deploy/index.html), respectively.

  ***sam publish*** instead this command publishes an AWS SAM application to the AWS Serverless Application Repository, and does not generate the template file. It takes a packaged AWS SAM template and publishes the application to the specified region.

  

* AWS Secret Manager VS System Manager Parameter Store

Secrets Manager allows you to automatically rotate RDS secrets.

Both get you an API-accessible, encrypted spot to stash credentials and config data and control access to it via IAM. For basically everything except RDS, you will want to use Parameter Store if for no other reason than it's free.

What about key rotation for services other than RDS? We can use another valuable tool in the toolbox: Lambda! You can use AWS Lambda to write a function to rotate your keys and this is integrated directly in the Secrets Manager console.

Another huge difference, and again, a win for Secrets Manager is the **ability to generate random secrets**. You can randomly generate passwords in [CloudFormation](https://wpengine.linuxacademy.com/amazon-web-services-2/cloudformation-deep-dive/) and store the password in Secrets Manager. And this is not just functionality for CloudFormation. The SDK can be used to do this in your application code. A final difference, and another win for Secrets Manager, is that secrets can be shared across accounts.



* Which API call would I use to give an application access to a S3 bucket via a role that has been created?

  a. IAM:AccessRole

  b. STS:GetSessionToken

  c. IAM:GetRoleAccess

  d. STS:AssumeRole

  It's always AssumeRole because you are temporarily assuming an IAM role...there are a few different permeations available within the STS API:

* AssumeRole: (Used by IAM users and Cross-Account access)

  https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRole.html

  AssumeRoleWithSAML (used for users authenticated by a SAML compliant directory like Active Directory)

  https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRoleWithSAML.html

  AssumeRoleWithWebIdentity (used when authenticating users with Cognito, or web ID Providers like FaceBook, Google, Amazon.com)

  https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRoleWithWebIdentity.html