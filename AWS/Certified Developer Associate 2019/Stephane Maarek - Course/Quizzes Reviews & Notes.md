# Quizzes Reviews & Notes

### AWS Fundamentals: IAM + EC2

#### Notes

* EBS support encryption in flight SSL and do support encryption at rest using KMS



______________

### AWS Fundamentals: ELB + ASG + EBS

#### Quiz Reviews

**Your instance in us-east-1a just got terminated, and the attached EBS volume is now available. Your colleague tells you he can't seem to attach it to your instance in us-east-1b.**

* He is missing IAM permissions
* EBS volumes are region locked
* **EBS volumes are AZ locked**



____________

### AWS Fundamentals: Route53 + RDS + ElastiCache + VPC

#### Notes

* Caching Strategy with ElastiCache Lazy Loading would only cache data that is actively requested from the database

  

__________

### AWS Fundamental: S3

#### Notes

There are 4 methods of encrypting objects in S3, all uses AES-256 for Encryption. All in rest:

* SSE-S3: encrypts S3 objects using keys handled & managed by AWS, ServerSide AES-256 (HTTP/S+Header). S3 Managed Data Key

* SSE-KMS: leverage AWS KMS to manage encryption keys, ServerSide, Adventages: UserControl + Audit trail (HTTP/S+Header). KMS Customer Master Key (CMK)

* SSE-C: when you want to manage your own encryption keys outside of AWS, S3 doesn't store the key, HTTPS must be used, encryption key must provided in HTTP headers for every HTTP request made (HTTPS Only+Data Key in Header). Client-provided data key

* Client Side Encryption, customer fully manages the keys and encryption cycle

  ****

* Encryption in transit (SSL)

  * AWS S3 exposes:
    • HTTP endpoint: non encrypted
    • HTTPS endpoint: encryption in flight

    • You’re free to use the endpoint you want, but HTTPS is recommended
    • HTTPS is mandatory for SSE-C
    • Encryption in flight is also called SSL / TLS

  #### Quiz Reviews

**Your client wants to make sure the encryption is happening in S3, but wants to fully manage the encryption keys and never store them in AWS. You recommend**

* SSE-S3
* SSE-KMS
* **SSE-C**
* Client Side Encryption

**Your company wants data to be encrypted in S3, and maintain control of the rotation policy for the encryption keys. You recommend**

- SSE-S3
- **SSE-KMS**
- SSE-C
- Client Side Encryption

**Your company does not trust S3 for encryption and wants it to happen on the application. You recommend**

- SSE-S3
- SSE-KMS
- SSE-C
- **Client Side Encryption**

**Which encryption method requires HTTPS?**

- SSE-S3
- SSE-KMS
- **SSE-C**
- Client Side Encryption



__________

### AWS CLI, SDK, IAM Roles & Policies

#### Notes

* **AWS CLI STS Decode Errors**, is for when you run API calls and they fail, you can get a long error message, this error message can be decoded using the STS command line **sts decode-authorization-message**
* AWS SDK performs actions directly from your applications code, you have to use for example when coding against AWS Services such as DynamoDB
* Exponential Backoff is when any api fails because of too many calls, these apply to rate limited API, incluided in SDK API calls
* IAM Policy Simulator is for test and troubleshoot IAM and resource-based policies 
* **AWS Security Token Service (STS)** is a web service that enables you to request temporary, limited-privilege credentials for AWS Identity and Access Management (IAM) users or for users that you authenticate (federated users).



___________________

### Elastic Beanstalk

#### Notes

- Elastic Beanstalk is a developer centric view of deploying an application on AWS, just need the application code in a repository
- Three architecture models:
  • Single Instance deployment: good for dev
  • LB + ASG: great for production or pre-production web applications
  • ASG only: great for non-web apps in production (workers, etc..)
- Elastic Beanstalk has three components
  • Application
  • Application version: each deployment gets assigned a version
  • Environment name (dev, test, prod...): free naming
- Deployments options for Updates
  - All at once (deploy all in one go) – fastest, but instances aren’t available to
    serve traffic for a bit (downtime)
  - Rolling: update a few instances at a time (bucket), and then move onto the
    next bucket once the first bucket is healthy
  - Rolling with additional batches: like rolling, but spins up new instances to
    move the batch (so that the old application is still available)
  - Immutable: spins up new instances in a new ASG, deploys version to these
    instances, and then swaps all the instances when everything is healthy
- **EB Extensions**, this use a zip file, requirements: 
  * .ebextensions/ in the root of source code 
  * yaml/json
  * admit .config extensions (e.g. logging.config)
  * Ability to add resources such as RDS, ElastiCache, DynamoDB, etc
- EB Cli, cli for working with Beanstalk from the CLI easier, helpful for your automated deployments pipelines
- EB Deployment Mechanism
  - Describe dependencies, for e.g. requirements.txt for python
  - Package Code as Zip, Zip file is uploaded to each EC2 machine
  - For long deployments you can add your dependencies in the package to improve speed
- Beanstalk with HTTPS

  * Idea: Load the SSL certificate onto the Load Balancer
  * Can be done from the Console (EB console, load balancer configuration)
  * Can be done from the code: .ebextensions/securelistener-alb.config
  * SSL Certificate can be provisioned using ACM (AWS Certificate Manager) or CLI
  * Must configure a security group rule to allow incoming port 443 (HTTPS port)
-  Beanstalk redirect HTTP to HTTPS
  - Configure your instances to redirect HTTP to HTTPS:
    https://github.com/awsdocs/elastic-beanstalk-samples/tree/master/configuration-files/aws-provided/security-configuration/https-redirect
  - OR configure the Application Load Balancer (ALB only) with a rule
  - Make sure health checks are not redirected (so they keep giving 200 OK)
- Lifecycle policy is for remove olders versions

https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/using-features-managing-env-tiers.html

#### Quiz Reviews

**You are looking to perform a set of repetitive and scheduled tasks asynchronously. Which Elastic Beanstalk environment should you setup?**

* Setup a WS environment and a .ebextensions file
* Setup a WS environment and a cron.yaml file
* Setup a Worker environment and a .ebextensions file
* **Setup a Worker environment and a cron.yaml file**



________

### CICD: CodeCommit, CodePipeline, CodeBuild, CodeDeploy

#### Notes

* **CodeCommit**
  * Authentication in Git:
    * SSH Keys: AWS Users can configure SSH keys in their IAM Console
    * HTTPS: Done through the AWS CLI Authentication helper or Generating HTTPS credentials
    * MFA (multi factor authentication) can be enabled for extra safety
  * Authorization in Git:
    * IAM Policies manage user / roles rights to repositories
  * *You can trigger notifications in CodeCommit using AWS SNS (Simple*
    *Notification Service) or AWS Lambda or AWS CloudWatch Event Rules*
  * SNS / Lambda notifications: Deletion of branches, trigger for push in master
  * *CloudWatch Event Rules: Trigger for PR, Commit comments events.*

* **Codepipline**
  * Automating our pipeline from code to ElasticBeanstalk
  * Made of stages, each stage can have sequential actions or parallel, examples: build, test, deploy, etc. Manual Approval can be defined at any stage
  * Each pipleine stage can create "artifacts", this are passed store in Amazon S3 and passed on to the next stage
  * If a pipeline file for send mail you need Cloudwatch Event Rule
  * CodePipeline state changes happen in AWS CloudWatch Events, which
    can in return create SNS notifications.
* **Codebuild**
  * You can use our own Docker images
  * Source code From GitHub - CodeCommit - CodePipeline - S3
  * Build instructions can be defined in code (buildspec.yml)
  * You can reproduce Codebuild Locally for troubleshoot (after install docker)
  * Pipelines can be defined within CodePipeline or Codebuild itself
  * Save logs in CloudWatch or S3
  * S3 Cache Bucket (optional)
  * BuildSpec
    * Must be at the root of your code
    * Define env variables, can use secrets with SSM Parameter Store
    * Phases: Install (dependencies), Pre Build (commands), Build, Post build (for example zip)
    * Artifacts: For example upload to S3
    * Cache: Files to cache (usually dependencies) in s3
* **CodeDeploy**
  * Deploy to EC2, these instances arent managed by Elastic Beanstalk
  * Ec2 or On premise need the CodeDeploy Agent, the agent is continuosly polling AWS CodeDeploy, Codedeploy sends appspec.yml
  * Application is pulled from github or S3. 
  * Ec2 Run the deployment instructions
  * CodeDeploy Agent will report of success  / failure of deployment on the instance
  * Ec2 instances are grouped by Deployment Group (dev-test-prod)
  * CodeDeploy can be chained into CodePipeline and use artifacts from there
  * Blue-green only works with EC2 (not on premise)
  * Deployment type: In place Deployment or Blue/Green Deployment
  * Hooks: Set of instructions to do to deploy the new version: The order is:
    ApplicationStop, DownloadBundle, BeforeInstall, install, AfterInstall, ApplicationStart, ValidateService, BeforeAllowTraffic, AllowTraffic, AfterAllowTraffic.



_________

### Cloudformation

#### Notes

* Templates components (one course section for each):
  * Resources: your AWS resources declared in the template (MANDATORY)
  * Parameters: the dynamic inputs for your template (useful to reuse your templates) !Ref
  * Mappings: the static variables for your template. For e.g. to different envs. 
  * Outputs: References to what has been created
  * Conditionals: List of conditions to perform resource creation
  * Metadata
* Pseudo Parameters, these can be used at any time (accountid, region, etc)
* Functions
  
  * !Sub is used to substitute variables from a test
  
  

#### Quiz Reviews

**The `!Ref` function can be used to reference the following except...**

* Parameters
* Resources
* **Conditions**



_______

### Monitoring & Audit: CloudWatch, X-Ray and CloudTrail
#### Notes

* AWS CloudWatch

  * Metrics: Collect and track key metrics, overtime for monitoring

  * Logs: Collect, monitor, analyze and store log files. Can go to s3 for archival or streams to ElasticSearch. Can use filter expressions. Can define expiration policies. To send logs to Cloudwatch check IAM permissions. They don't expire by default, and expiration policy should be defined at log groups level.

  * Events: Send notifications when certain events happen in your AWS. Use for cron jobs. Triggers to Lambda, SQS/SNS/Kinesis

  * Alarms: React in real-time to metrics / events. Can go to ASG, EC2 Actions, SNS Notifications. Alarms have 3 states: OK, Insufficient_data, alarm. 
    High resolution custom metrics can only choose 10 or 30 sec. 
    Alarms is for send notifications in case of unexpected metrics

  * You need use detailed monitoring if you want to more prompt scale your ASG

  * Custom Metrics: Standard resolution: 1 min, High resolution: up to 1 seconds, use api call PutMetricData, use exponential back off in case of throttle errors

    

* AWS X-Ray:

  * Troubleshooting application performance and errors
  * Distributed tracing of microservices and distributed systems
  * Is for Latency, Errors and Fault analysis,
  * • Segments: each application / service will send them
    • Trace: segments collected together to form an end-to-end trace
    • Sampling: decrease the amount of requests sent to X-Ray, reduce cost
    • Annotations: Key Value pairs used to index traces and use with filters
    • Metadata: Key Value pairs, not indexed, not used for searching
  * You can send traces across accounts if you create a role on another account, and allow a role in your account to assume that role

  

* AWS CloudTrail:

  * Internal monitoring of API calls being made, is enabled by default
  * Audit changes to AWS Resources by your users
  * Can put logs from CloudTrail into CloudWatch logs
  * If a resource is deleted, look into Cloudtrail first.

  

________

### AWS Integration & Messaging: SQS, SNS & Kinesis
#### Notes

* Application Communication

  * Synchronous communications (application to application)
  * Asynchronous / Event based (application to queue to application)

  

* SQS: Producer -> Send Messages -> SQS Queue -> Poll Messages -> Consumer

* SQS Standard Queue: Default retention: 4 days maximum of 14, no limit messages in the queue, maybe duplicate messages, limitation 256kb per message

* SQS Delay Queue: Delay a message up to 15 min, default is 0, can override the default using the DelaySeconds parameters

* SQS poll receive up to 10 messages at a time

* SQS Visibility Timeout, the message is invisible to others consumers for a defined period. Default 30 seconds (0 sec to 12 hours). DeleteMessage Api to tell SQS the message was successfully processed

* SQS DLQ if a consumer fails to process a message within the visibility timeout, the message goes back to the queue. We have to create a DQL fiest and then designate it dlq. We need to make sure to process the messages in the DLQ before they expire.

* SQS Long Polling is when a consumer what for messages to arrive, this decreases the number of API calls made to SQS while increasing the efficiency and latency of your app. The wait time can be between 1 sec to 20 secs. Long polling can be enabled at the queue level or at the API level using WaitTimeSeconds. ReceiveMessageWaitTimeSeconds, when set to greater than zero, enables long polling. Long polling allows the Amazon SQS service to wait until a message is available in the queue before sending a response.

* SQS FIFO, name of the queue must end in .fifo, lower throughput, no per message dealy. Provide a MessageDeduplicationId with your message, deduplication is 5 min. For ensure strict ordening between messages specify a MessageGroupId, message with different Group ID may be received out of order. Messages with the same Group Id are deliverd to one consumer at a time.

* SQS Extended Client is for send message greater than 256kb using a amazon S3 bucket, in the SQS queue only store the metadata message

* SQS Security, in flight using https endpoints. Can enablee SSE using KMS

* SQS Use Cases, decouple applications, buffer writes to a database, handle large loads of messages coming in.



* SNS is for send one message to many receivers.
* SNS event producer only sends messages to one SNS topics, as many "Event Receivers" (subscriptions) as we want to listen to the SNS topic notifications. Each subscriber to the topic will get all the messages.
* SNS susbribers can be sqs, http/https, lambda, emails, sms, mobile notifications
* SNS How to publish: Create a topic, create a subscription (or many), publish to the topic
* SNS + SQS (FAN OUT): Push one in SNS, receive in many SQS, no data loss



* Kineses is managed alternative to Kafka, great for logs, metrics, IoT, realtime bigdata, data is automatically replicated to 3 AZ
* Kinesis Streams: low latency streaming ingest at scale. Streams are divided in ordered Shards/Partitions. Data retention is 1 day default to 7 days, can reprocess data. Once data is inserted in Kinesis, it cant be deleted. One stream is made of many shards
* Kinesis Analytics: perform real-time analytics on streams using SQL. Managed service no administration
* Kinesis Firehose: load streams into S3, Redshift, ElasticSearch. Near Real Time (60 segs latency). Managed service no administration
* Kinesis Client Library (KCL) is Java library that helps read record from a K Streams with distributed apps sharing the read workload. Each shard is be read by only KCL instance. e.g. 4 shards = max 4 kcl instances. 
* In KCL, you can have a maximum of EC2 instances running in parallel equal to the number of shards in your Kinesis Stream.
* Partition Key is to specified a Shard a generated a ordered in Kineses.



_________

### Serverless Lambda

#### Notes

- Services Serverless: Lamba, Step Functions, DynamoDB, Cognito, Api Gateway, S3, SNS & SQS, Kineses, Aurora Serverless
- Max resources of 3GB of ram (128 min), increase RAM improve CPU and Network
- Execution time 3 secs, max of 5 min (New limit 15 minuts)
- Disk capacity in /tmp: 512 mb
- Deployment size max: Compressed .zip 50mb, uncompressed 250mb, env variables 4kb
- Ability to deploy in a VPC and assign sc (less performant)
- Concurrency up to 1000 executions, invocation over the concurrency will trigger a "Throttle"
- Versions are immutable, have increasing version numbers (v1, v2), version is code + config
- Aliases are pointers to versions, is usefull to dev, test, prod envs, aliases are mutable.
- Lambda Best practices: 
  - Perform Heavy-duty work outside of your function handler (connect db, initialice AWS SDK, pull deps)
  - Use envs variables for dbs connections strings, s3 buckets, etc.
  - Passwords they can be encrypted using KMS
  - Avoid recursive code
  - Don't use lambda function in a VPC unless you have to
- Lambda@Edge is for use lambda globally, or for request filtering before reaching your app, is deploy lambda alongside your CloudFront CDN. You can use to change CloudFront requests and responses
- The format of a Lambda handler is <filename>.<function name>.
- Environment variables for Lambda functions enable you to dynamically pass settings to your function code and libraries, without making changes to your code. Environment variables are key-value pairs that you create and modify as part of your function configuration, using either the AWS Lambda Console, the AWS Lambda CLI or the AWS Lambda SDK.



________________

### DynamoDB 

#### Notes

* DynamoDB is a Fully managed NoSQL db, with HA in 3AZ, is made of tables, each table has a PK, and each table can have an infinite number of items (rows), each item has attributes (maximum size of a item 400kb)

* PK: 

  * Partition Key only: must be unique for each item
  * Partition Key + Sort Key: The combination must be unique, data is grouped by pk, sort key = range key

* Querys: use FilterExpression to client side filter, returns up to 1 MB of data, you can use "Limit" to show only a number of items, able to do pagination on the results, can query a table, a lsi, or a gsi

* Provisioned Throughput: Table must have read and write capacity units, option to setup autoscaling, throughput can be exceeded temporarily using "burst credit", if burst credit are empty, you'll get a "ProvisionedThroughputException", it's then advised to do an exponential back-off retry

* Difference between eventually consistent and strongly consistent data: 

  * Eventually consistent: A read request immediately after a write operation might not show the latest change but costs less in terms of capacity. Most operations are eventually consistent by default but can usually be changed in your calls.
  * Strongly consistent: The most up-to-date data.

* RCU: A unit of read capacity: 1 strongly consistent read per second or two eventually consistent reads per second for items as large as 4 KB 

  ```
  Strongly Consistent Read
  (ITEM SIZE (rounded up to the next 4KB multiplier) / 4KB) * # of items
  (Round up to the nearest 4 KB multiplier)
  * Eventually consistent reads would cut that in half
  ```

  WCU: A unit of write capacity: 1 write per second for items up to 1KB

  ```
  (ITEM SIZE (rounded up to the next 1KB multiplier) / 1KB) * # of items
  (Round up to the nearest 1 KB multiplier)
  ```

  

  **To get the number of strong and eventual consistent read requests that your table can accommodate per second, you simply have to do the following steps:**

  **Step #1 Multiply the value of the provisioned RCU by 4 KB**

  = 10 RCU x 4 KB

   = 40 

  **Step #2 To get the number of strong consistency requests, just divide the result of step 1 to 4 KB**

  Divide the Average Item Size by 4 KB since the scenario requires eventual consistency reads: 

   = 40 / 4 KB

   = **10 strongly consistent reads requests**


  **Step #3 To get the number of eventual consistency requests, just divide the result of step 1 to 2 KB**

  = 40 / 2 **KB**

  = **20 eventually consistent read requests**



* Partitions internal: Data is divided in partititions, wcu and rcu are spread evenly between partitions

* DynamoDB Throttling: If we exceed our RCU or WCU we get ProvisionedThrouhgputExceededExceptions
    * Reasons:
      * Hot keys: One PK is being read to many times
      * Hot partitions:
      * Very large items
    * Solutions:
      * Exponential back-off when exception is encountered
      * Distribute pk as much as possible
      * If rcu issue, we can use DAX

* Scan: Scan the entire table an then filter out data (inefficient), returns up to 1MB of data, consumes a lot of RCU, limit impact using limit, for faster performance use "parallel scans". Can use a ProjectionExpressions + FilterExpression 

- DynamoDB Indexes:
  - Local Secondary Index (LSI): Alternate range key for your table, local to the hash key . LSI must be defined at table creation time. LSI if for efficient query by timestamp. Is called LOCAL because it is local to the Partition key. Max item size 10 GB. LSI has the same partition key as the table, but with different sort key.
  - Global Secondary Index (GSI): Speed up queries on non-key atrributes. GSI = pk + optional SK. Basically define a new index, this is a new table an we can project attributes on it, the pk and sk of the original table are always projected (KEY_ONLY). Must define RCU / WCU for the index. Possibilit to add / modify GSI (not LSI). Offer only eventual consistency
  - DynamoDB indexes can generate Throttling:
    - GSI, if the writes are throttled on the GSI, then the main table will be throttled. Even if the WCU on the main tables are fine. Choose your GSI pk carefully and assing your WCU capacity carefuly. GSI CAN THROTTILING YOUR MAIN TABLE
    - LSI uses the WCU and RCU of the main table, no special throttling considerations
- DynamoDB is Optimistic Concurrency / Optimistic Locking this mean because multiple clients can access the same object at the same time, DynamoDB for example if we delete something, only do that if a certain condition is validated. This use Conditional Writes.
- DynamoDB Dax (Accelerator) is a seamless cache for DynamoDB, no need to modified the application. Solves the Hot Key problem (HK = a key with to many reads). Default TTL 5 minutes, multi AZ (3 nodes minimum for prod). Dax write directly in DynamoDB 
- DynamoDB streams: Changes in DynamoDB (Create, update, delete) can end up in a DynamoDB Streams, this streams can be read by AWS Lambda an we can then do:
  - React to changes in real time
  - Analitycs
  - Create derivate tables
  - Streams can implement cross region replication, an has 24 hours of data retention
- DynamoDB TTL: Automatically delete an item after an expire date, not use WCU-RCU, don't have cost, helps reduce storage, TTL is enabled per row, DynamoDB typically deletes expires items in 48 hras. DynamoDB streams can help recover expired items
- DynamoDB CLI:
  - --Projection-expression: select attributes to retrieve (maybe from a DynamoDB scan)
  - --filter-expression: filter results
  - General Cli pagination:
    - Optimization: --page-size: full dataset is still received but each API call will request less data. Allow you to retrieve a subset of the attributes coming from a DynamoDB scan
    - Pagination:  (Useful to paginate the results of a DynamoDB scan in order to minimize the amount of RCU that you will use for that CLI command)
      - --max-items: max number of results returned by the CLI. Returns NextToken
      - --starting-token: specify the last received NextToken to keep on reading
- DynamoDB transactions is the ability to create/update/delete multiple rows in different tables at the same time, its an ALL OR NOTHING type of operation. Consumes 2x of WCU / RCU

https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Query.html

____________

### Serverless: Api Gateway & Cognito

#### Notes

- To make a change in the Api Gateway you need to make a "Deployment" for them to be in effect

- Changes are deployed to "Stages" (e.g. dev, test, prod). Each stage has its own configs

- Stages Variables are like envs variables for Api Gateway,

- Api gateway stages (variables) (dev, prod)-> Lambda Aliases (dev, prod) -> Lambda version (1,2,3)

- Mapping Templates can be used to modify request / responses, rename parameters, filter output results.

- Swagger allows you to import and export your API.

- Caching API responses: Default TTL 5min, max 60min. Capacity between 0.5GB to 237GB. Able to flush the entire cache (invalidate it) immediately. Client can invalidate the cache with header: Cache-Control: max-age=0

- Stage Cache is por enable api cache.

- API security: 

  - IAM Permissions: rest api use sig v4, where IAM credential are in headers. Great for users/roles already within your AWS account. For e.g useful to authorize an EC2 to use your internal API. Handle Authentication + Authorization
  - Custom Authorizer: Use AWS Lambda to validate the token in header being passed. Great for 3rd party tokens. Handle Authentication + Authorization
  - Cognito Users Pools: Cognito fully manages user lifecycle. Api Gateway verifies identity automatically from AWS Cognito. You manage your own user pool (can be backed by Facebook, Google login etc...). No need custom code. Cognito only helps with authentication, not authorization

  

- Cognito User Pools:

  - Sign in functionality for app users
  - Integrate with API Gateway

- Cognito Identity Pools (Federated Identity):

  - Provide AWS credentials to users so they can access AWS resources directly
  - Integrate with Cognito User Pools as an identity provider

- Cognito Sync: (Deprecated – use AWS AppSync now)

  - Synchronize data from device to Cognito.
  - May be deprecated and replaced by AppSync

  

#### Quiz Reviews

**You would like to provide a Facebook login before your users call your API hosted by API Gateway. You need seamlessly authentication integration, you will use**

- Cognito Sync
- DynamoDB user tables with Lambda Authorizer
- **Cognito User Pools**
- Cognito Federated identity pools



____________

### Serverless Application Model (SAM)

#### Notes

* Framework for developing and deploying serverless applications, all config in YAML. Generate complex CloudFormation from simple Yaml. SAM can use CodeDeploy to deploy Lambda functions
* SAM Recipe:
  * Transform Header indicates it’s SAM template: Transform: 'AWS::Serverless-2016-10-31'
  * Package & Deploy:
    • aws cloudformation package / sam package
    • aws cloudformation deploy / sam deploy
  * Write Code
    • AWS::Serverless::Function
    • AWS::Serverless::Api
    • AWS::Serverless::SimpleTable
* Important examples:
  • S3ReadPolicy: Gives read only permissions to
  objects in S3
  • SQSPollerPolicy: Allows to poll an SQS queue
  • DynamoDBCrudPolicy: CRUD = create read update delete



________________

### ECS, ECR & Fargate

#### Notes

- TO manage containers we have 3 choices:

  - ECS: Amazon's own platform. Logical grouping of EC2, instances run the ECS agent. The Agent registers the instance to the ECS cluster, run special AMI for ECS. If we need to scale, we need to add EC2 instances. We must configure the file /etc/ecs/ecs.config with the cluster name
  - Fargate: Amazon's own Serverless platform, we dont provision EC2 instances, we just create task definitions, to scale just increase the task number.  The containers are provisioned by the container spec (CPU/RAM)
  - EKS: Amazon's managed Kubernetes

- ECS Task Definitions are metadata in JSON to tell ECS how to run a Docker (image name, port binding, memory and cpu required, envs, iam role, etc)

- A **task placement strategy** is an algorithm for selecting instances for task placement or tasks for termination. Task placement strategies can be specified when either running a task or creating a new service.

  Amazon ECS supports the following task placement strategies:

  **binpack** - Place tasks based on the least available amount of CPU or memory. This minimizes the number of instances in use.

  **random** - Place tasks randomly.

  **spread** - Place tasks evenly based on the specified value. Accepted values are attribute key-value pairs, *instanceId*, or *host*. 

- ECR is a private Docker image repository, you need to do login before push/pull
  $(aws ecr get-login --no-include-email --region eu-west-1)
  sent over HTTPS (encrypt in flight) and encrypted at rest

- EC2 instances can run multiple containers on the same type, you must not specify a host port (only container port), and use a ALB with dynamic port mapping, the Ec2 sg must allow traffic from the ALB on al ports. To enable random host port, set host port = 0 (or empty), which allows multiple containers of the same type to launch on the same instance

- ECS + Xray: two types: As Daemon (on daemon for EC2), "Side Car" (on xray per app container).-Fargate also use side car integration. 
  

For the Task definition in EC2, we need the next line:
  name : AWS_XRAY_DAEMON_ADDRESS Value: xray-daemon:2000

- ECS does integrate with CloudWatch Logs, you need to setup login at the task definition level, each container will have a different log stream



______

AWS Secrets Manager is an AWS service that makes it easier for you to manage secrets. *Secrets* can be database credentials, passwords, third-party API keys, and even arbitrary text. You can store and control access to these secrets centrally by using the Secrets Manager console, the Secrets Manager command line interface (CLI), or the Secrets Manager API and SDKs.

Secrets Manager enables you to replace hardcoded credentials in your code (including passwords), with an API call to Secrets Manager to retrieve the secret programmatically. 

_________



## Practice Test

65 questions | 2 hours 10 minutes | 70% correct required to pass

Attempt 1: Passed 78% correct (51/65) 50 minutes August 9, 2019 1:38 AM

Attempt 2: Passed 90% correct (59/65) 34 minutes August 14, 2019 0:48 AM



**You configured your S3 Bucket called "myimages2019" to trigger a Lambda function with each upload. The lambda function creates thumbnails. However, it throws an error about authorization when it tries to download the main picture. What is the potential reason of that?**

* **You didn't assign a role t the Lambda function with a policy that allows access to S3 buckets**
* You didn't assign a user to the Lambda function
* You didn't assign a policy to the Lambda function with Read-Write access of S3 buckets
* You didn't assign a policy to the Lambda function with Read-Only of S3 buckets.

AWS services (like Lambda) assumes a role and not policy. Then, you can attach policies on that role



**Your company decides to adopt AWS S3 as a vault system for storing all the application secrets. For compliance purposes, this vault bucket must be encrypted. Your company wants to avoid the hassle of managing all encryption keys, what type of encryption suits your needs ?**

* Client-side Encryption
* SSE-C
* **SSE-S3**
* SSE-KMS

SSE-S3: AWS manages both data key and master key 
SSE-KMS: AWS manages data key and you manage master key 
SSE-C: You manage both data key and master key | Read more: http://amzn.to/2iVsGvM



**Which terminology does NOT belong to the API Gateway**

- **Counters**
- Resources
- Stages
- Endpoint

Endpoint – A URL that responds to HTTP requests, 
Resource – A named entity that exists within an endpoint, 
Stage - To deploy an API, you create an API deployment and associate it with a stage. Each stage is a snapshot of the API and is made available for the client to call.



**Using Elastic Beanstalk, one environment includes**

- **One and only one app version**
- Unlimited app versions
- Two (Blue and Green)
- Nothing, environments is only a concept

https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/concepts.html



**CodePipeline contains a set of actions and each action contains a set of stages**

- Yes
- **No**

each stage contains a set of actions



**The data should be distributed evenly across DynamoDB partitions. What does this depend on?**

* Items
* **The Partition Key**
* The Range Key
* The Sort Key

When choosing the primary key of the table, you should consider this rule for performance purposes



**You want to retrieve some items from a DynamoDB table using the primary key. What API call will provide better performance amongst the 4 below ? (select two)**

* **Query**
* **GetItem**
* Filter
* Scan

Filter is executed after retrieving some data, and Scan will retrieve an entire table, which is not efficient to find an item using a Primary Key. GetItem directly uses PrimaryKey and Query can help sort through more data.



* Beanstalk Configuration Files:
  Configuration files are YAML- or JSON-formatted documents with a .config file extension that you place in a folder named .ebextensions and deploy in your application source bundle. Read More: https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/ebextensions.html



* AWS Lambda is a fully managed service. AWS periodically releases new Runtimes with the new versions. Yo can't use a new runtime version, you need to wait a AWS. 



* Step Functions use JSON



* AWS IAM and AWS Cloudfront are global services. AWS EC2, AWS Lambda are regional services. AWS DynamoDB is regional too and providing "Global tables" does not mean it is a global service. 