AWS Certified Developer Associate – Practice (DVA-P01)

Overall Score: 90%

Topic Level Scoring:
1.0  Deployment: 75%
2.0  Security: 100%
3.0  Development with AWS Services: 100%
4.0  Refactoring: 100%
5.0  Monitor and Troubleshooting: 50%
